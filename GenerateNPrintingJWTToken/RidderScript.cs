﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Cryptography;
using Ridder.Common.Login;
using System.Text;
using System.Threading.Tasks;

namespace GenerateNPrintingJWTToken
{
    public class RidderScript : CommandScript
    {
        public async void Execute()
        {
            // RK
            // 12-04-2023
            // Ophalen en opslaan van NPrinting JWT token

            if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("U_BISHARPPORTALUSER", StringComparison.OrdinalIgnoreCase))
            {
                MessageBox.Show("Dit script mag alleen vanaf een bisharp portal user aangeroepen worden.", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            IRecord[] records = FormDataAwareFunctions.GetSelectedRecords();
            if (records.Length > 1)
            {
                MessageBox.Show("Dit script is voor één Bisharp portal user tegelijk aan te roepen.", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int id = (int)records[0].GetPrimaryKeyValue();
            var rsUser = GetRecordset("U_BISHARPPORTALUSER", "PORTALUSERNAME, JWTTOKEN", $"PK_U_BISHARPPORTALUSER = {id}", "");

            string username = rsUser.DataTable.AsEnumerable().First().Field<string>("PORTALUSERNAME");
            if (string.IsNullOrEmpty(username))
            {
                MessageBox.Show("Geen gebruikersnaam gevonden voor huidige gebruiker", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string jwtToken = await GetJwtTokenFromPortal(username);
            if (string.IsNullOrEmpty(jwtToken))
            {
                return;
            }

            rsUser.MoveFirst();
            rsUser.SetFieldValue("JWTTOKEN", jwtToken);
            var result = rsUser.Update2();

            if (result.Any(x => x.HasError))
            {
                MessageBox.Show($"Fout bij het opslaan van gegenereerde JWT token: {result.First(x => x.HasError).GetResult()}", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private async Task<string> GetJwtTokenFromPortal(string email)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://biportal.bisharp.nl/api/BisharpApi/GenerateJwtToken");

            var formData = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("email", email)
            });

            try
            {
                var response = await client.PostAsync(client.BaseAddress, formData);

                response.EnsureSuccessStatusCode();

                string result = await response.Content.ReadAsStringAsync();
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Geen JWT token ontvangen van portal: {ex.Message}", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }
    }
}
