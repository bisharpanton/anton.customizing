﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using Ridder.Client.SDK.SDKParameters;

public class CreateGoodsreceiptFromFinalInstallment_RidderScript : WorkflowCommandScript
{
    public void Execute()
    {
        // SP
        // 9-1-2025
        // Bij slottermijn vragen of inkooporder volledig ontvangen mag worden

        var installmentId = (int)RecordIds.First();

        var installment = GetRecordset("R_PURCHASEINSTALLMENT", "FK_PURCHASEORDER, INSTALLMENTPERCENTAGE",
            $"PK_R_PURCHASEINSTALLMENT = {installmentId}", "").DataTable.AsEnumerable();

        var purchaseOrderId = installment.First().Field<int>("FK_PURCHASEORDER");
        var percentage = installment.First().Field<double>("INSTALLMENTPERCENTAGE");

        var percentageOtherInstallments = GetRecordset("R_PURCHASEINSTALLMENT", "INSTALLMENTPERCENTAGE",
            $"FK_PURCHASEORDER = {purchaseOrderId} AND FK_PURCHASEINVOICE IS NOT NULL", "").DataTable.AsEnumerable().Sum(x => x.Field<double>("INSTALLMENTPERCENTAGE"));

        if (percentage + percentageOtherInstallments != 1.0)
        {
            return;//Dit is niet de slottermijn
        }

        DialogResult dialogResult2 = MessageBox.Show("De geselecteerde termijn is de laatste termijn. Wil je de inkooporder volledig ontvangen?",
            "Inkooporder ontvangen", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

        if (dialogResult2 == DialogResult.Cancel)
        {
            return;
        }

        var result = EventsAndActions.Purchase.Events.CreateGoodsReceiptFromPurchaseOrder(purchaseOrderId, 0, 0,
                BlockOrSplitParameter.BlockJournalize, true, DateTime.Now, string.Empty);
        if (result != null && result.HasError)
        {
            MessageBox.Show($"Inkooporder ontvangen mislukt, oorzaak: {result.GetResult()}", $"{GetRecordTag("R_PURCHASEORDER", purchaseOrderId)}", MessageBoxButtons.OK,
                MessageBoxIcon.Information);
            return;
        }
    }
}
