﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Communication.Script;
using System.Security.AccessControl;

public class R_ROLEUSER_Delete_User : IDeleteScript
{
    private ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeDelete(RowData data, ref string reason)
    {
        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterDelete(RowData data, ref string reason)
    {
        //DB
        //19-10-2023
        //Bij ACW aanmaken KPI declarable uren
        if (!DeleteKpiDeclarableUren(data, ref reason))
        {
            return false;
        }

        return true;
    }

    private bool DeleteKpiDeclarableUren(RowData data, ref string reason)
    {
        var abbreviationCompany = _script.GetRecordset("R_CRMSETTINGS", "ABBREVIATIONCOMPANY", "", "")
            .DataTable.AsEnumerable().First().Field<string>("ABBREVIATIONCOMPANY");

        if (!abbreviationCompany.Equals("ACW"))
        {
            return true;
        }

        var applicableRoles = _script.GetRecordset("C_STARTUPROLE", "FK_ROLE", "COMMANDNAME = 'KPI Declarabele uren'", "")
            .DataTable.AsEnumerable().Select(x => x.Field<int>("FK_ROLE")).ToList();

        if (!applicableRoles.Contains((int)data["FK_R_ROLE"]))
        {
            return true;
        }

        var otherRoleLinked = _script.GetRecordset("R_ROLEUSER", "PK_R_ROLEUSER",
            $"FK_R_ROLE IN ({string.Join(",", applicableRoles)}) AND FK_R_USER = {data["FK_R_USER"]}", "").RecordCount > 0;

        if(otherRoleLinked)
        {
            return true;
        }

        var rsStartup = _script.GetRecordset("R_STARTUP", "", 
            $"FK_USER = {data["FK_R_USER"]} AND COMMAND = '66945eda-ce05-43a8-86d1-dded00c28dbf'", "");

        if(rsStartup.RecordCount == 0)
        {
            return true;
        }

        rsStartup.MoveFirst();
        rsStartup.Delete();

        return true;
    }
}
