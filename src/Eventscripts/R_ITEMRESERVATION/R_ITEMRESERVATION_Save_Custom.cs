﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Communication.Script;

public class R_ITEMRESERVATION_Save_Custom : ISaveScript
{
    private ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //28-02-2018
        //Bij het inkopen van een bonregel, kopieer het werkadres van de order over naar het bestemmingsadres van de inkooporder

        if(!CopyWorkAdressToPurchaseOrders(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    private bool CopyWorkAdressToPurchaseOrders(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true;
        }

        if (!(bool)GetCrmSetting("COPYWORKADDRESSORDERTOPURCHASEORDER"))
        {
            return true;
        }

        if ((data["FK_PURCHASEORDERDETAILITEM"] as int?).HasValue && (data["FK_JOBORDERDETAILITEM"] as int?).HasValue)
        {
            var rsJobOrderDetail = _script.GetRecordset("R_JOBORDERDETAILITEM", "FK_ORDER",
                $"PK_R_JOBORDERDETAILITEM = {data["FK_JOBORDERDETAILITEM"]}", "");
            rsJobOrderDetail.MoveFirst();

            var rsOrder = _script.GetRecordset("R_ORDER", "FK_DESTINATIONADDRESS",
                $"PK_R_ORDER = {rsJobOrderDetail.Fields["FK_ORDER"].Value}", "");
            rsOrder.MoveFirst();

            var rsPurchaseOrderDetail = _script.GetRecordset("R_PURCHASEORDERDETAILITEM", "FK_PURCHASEORDER",
                $"PK_R_PURCHASEORDERDETAILITEM = {data["FK_PURCHASEORDERDETAILITEM"]}", "");
            rsPurchaseOrderDetail.MoveFirst();

            var rsPurchaseOrder = _script.GetRecordset("R_PURCHASEORDER", "FK_WORKADRESSORDER",
                $"PK_R_PURCHASEORDER = {rsPurchaseOrderDetail.Fields["FK_PURCHASEORDER"].Value}", "");
            rsPurchaseOrder.MoveFirst();

            if (!(rsPurchaseOrder.Fields["FK_WORKADRESSORDER"].Value as int?).HasValue)
            {
                rsPurchaseOrder.Fields["FK_WORKADRESSORDER"].Value = rsOrder.Fields["FK_DESTINATIONADDRESS"].Value;
                rsPurchaseOrder.Update();
            }
            else
            {
                //DB
                //07-08-2018 - Het is wel gewenst om meerdere orders te combineren. In dit geval wordt het laatste werkadres gepakt. Op de inkooporder onderdrukken we
                //dan het werkadres.
                /*
                //Check of de order die gekoppeld wordt hetzelfde werkadres heeft als momenteel in de inkooporder staat
                if ((int)rsPurchaseOrder.Fields["FK_WORKADRESSORDER"].Value != (int)rsOrder.Fields["FK_DESTINATIONADDRESS"].Value)
                {
                    reason = "Er worden behoeftes van verschillende werkadressen gecombineerd. Dit is niet mogelijk.";
                    return false;
                }*/
            }
        }

        return true;
    }

    public object GetCrmSetting(string s)
    {
        var rsCRM = _script.GetRecordset("R_CRMSETTINGS", s, "", "");
        rsCRM.MoveFirst();

        return rsCRM.Fields[s].Value;
    }
}
