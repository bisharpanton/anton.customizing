﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using Ridder.Communication.Script;
using System.Linq;

public class R_JOBORDERDETAILITEM_Save_User : ISaveScript
{
    private ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (!CopyPricePerKgFromAssemblyDetail(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //2-5-2024
        //Bij LBO is ooit intelligentie gemaakt om het veld 'Beton centrale' te vullen obv de toegewezen leverancier in de bonregel artikel. 
        //Ik denk dat dit is omgevallen bij het verspreiden van de scripts vanuit de Anton Groep
        if (!FillBetonInfoAtJoborder(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }


    private bool FillBetonInfoAtJoborder(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if(saveType == SaveType.AfterUpdateSave && oldData["FK_ITEM"].ToString().Equals(data["FK_ITEM"].ToString()) && oldData["FK_SUPPLIER"].ToString().Equals(data["FK_SUPPLIER"].ToString()))
        {
            return true;
        }

        var item = _script.GetRecordset("R_ITEM", "FK_ITEMGROUP, DESCRIPTION", $"PK_R_ITEM = {data["FK_ITEM"]}", "")
            .DataTable.AsEnumerable().First();

        var itemGroupId = item.Field<int>("FK_ITEMGROUP");

        var itemGroupIsConcrete = _script.GetRecordset("R_ITEMGROUP", "ISCONCRETE", $"PK_R_ITEMGROUP = {itemGroupId}", "")
            .DataTable.AsEnumerable().First().Field<bool>("ISCONCRETE");

        if(!itemGroupIsConcrete)
        {
            return true;
        }

        /*
        if (!(data["FK_SUPPLIER"] as int?).HasValue)
        {
            return true;
        }*/

        var joborderChanged = false;

        var rsJobrder = _script.GetRecordset("R_JOBORDER", "FK_BETONCENTRALE, BETONSOORT", $"PK_R_JOBORDER = {data["FK_JOBORDER"]}", "");
        rsJobrder.MoveFirst();

        if((data["FK_SUPPLIER"] as int?).HasValue && (rsJobrder.GetField("FK_BETONCENTRALE").Value as int? ?? 0) != (int)data["FK_SUPPLIER"])
        {
            rsJobrder.SetFieldValue("FK_BETONCENTRALE", data["FK_SUPPLIER"]);
            joborderChanged = true;
        }

        if(!rsJobrder.GetField("BETONSOORT").Value.ToString().Equals(item.Field<string>("DESCRIPTION")))
        {
            rsJobrder.SetFieldValue("BETONSOORT", item.Field<string>("DESCRIPTION"));
            joborderChanged = true;
        }

        if(joborderChanged)
        {
            rsJobrder.Update();
        }

        return true;
    }


    private bool CopyPricePerKgFromAssemblyDetail(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (oldData != null)
            return true;

        if (!(data["FK_ASSEMBLYDETAILITEM"] as int?).HasValue)
            return true;

        var pricePerKgAssemblyDetail = _script.GetRecordset("R_ASSEMBLYDETAILITEM", "PRIJSPERKG", $"PK_R_ASSEMBLYDETAILITEM = {data["FK_ASSEMBLYDETAILITEM"]}", "")
            .DataTable.AsEnumerable().First().Field<double>("PRIJSPERKG");

        if(pricePerKgAssemblyDetail == 0.0)
        {
            return true;
        }

        data["PRIJSPERKG"] = pricePerKgAssemblyDetail;

        return true;
    }
}
