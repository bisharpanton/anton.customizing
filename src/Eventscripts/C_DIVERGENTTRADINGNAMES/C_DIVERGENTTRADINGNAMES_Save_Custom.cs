﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Communication.Script;

public class C_DIVERGENTTRADINGNAMES_Save_Custom : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //27-5-2020
        //Zorg dat er altijd maar 1 record geselecteerd kan worden

        if(!UndoOtherSelectedDetails(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }

    private bool UndoOtherSelectedDetails(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterInsertSave && !(bool)data["DEFAULTTRADINGNAME"])
        {
            return true;
        }

        if (saveType == SaveType.AfterUpdateSave && (bool)oldData["DEFAULTTRADINGNAME"] == (bool)data["DEFAULTTRADINGNAME"])
        {
            return true;
        }

        if (!(bool)data["DEFAULTTRADINGNAME"])
        {
            return true;
        }

        var currentDetailId = (int)data["PK_C_DIVERGENTTRADINGNAMES"];
        var relationId = (int)data["FK_RELATION"];

        var rsDetailsNowSelected = _script.GetRecordset("C_DIVERGENTTRADINGNAMES", 
            "DEFAULTTRADINGNAME",
            $"FK_RELATION = {relationId} AND PK_C_DIVERGENTTRADINGNAMES <> {currentDetailId} AND DEFAULTTRADINGNAME = 1", "");

        if (rsDetailsNowSelected.RecordCount == 0)
        {
            return true;
        }

        rsDetailsNowSelected.MoveFirst();
        rsDetailsNowSelected.SetFieldValue("DEFAULTTRADINGNAME", false);
        rsDetailsNowSelected.Update();

        return true;
    }
}
