﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;

public class C_VEILIGHEIDSMELDINGEN_Save_Custom : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if(!CheckIfIntercompanyRelationIsCurrentAdministration(data, oldData, saveType, ref reason))
        {
            return false;
        }
          
        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }

    private bool CheckIfIntercompanyRelationIsCurrentAdministration(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        var chosenIntercompanyDesc = _script.GetRecordset("M_CHOICEVALUE", "CHOICETEXT",
            $"CHOICENUMBER = '{data["INTERCOMPANYRELATION"]}' AND FK_CHOICETYPE = '06f64738-17bb-4d09-814a-c458ed132b22'", "").DataTable.AsEnumerable().First().Field<string>("CHOICETEXT");

        var currentAdmin = _script.GetUserInfo().CompanyName;

        if(_script.GetUserInfo().CurrentUserName.Equals("SDK_APPREOWEBSERVICE"))
        {
            return true; //Toevoegen van veiligheidsmelding via Appreo dan de koppeling niet onderuit laten gaan op deze situatie
        }

        if((string)chosenIntercompanyDesc == (string)currentAdmin)
        {
            reason = $"Error: De gekozen intercompany relatie ({chosenIntercompanyDesc}) is gelijk aan de huidige administratie ({currentAdmin}). Kies een andere intercompany relatie.";
            return false;
        }

        return true;
    }

}
