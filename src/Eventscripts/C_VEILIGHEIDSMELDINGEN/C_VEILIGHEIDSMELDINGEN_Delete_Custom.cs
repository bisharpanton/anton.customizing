﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Communication.Script;

public class C_VEILIGHEIDSMELDINGEN_Delete_Custom : IDeleteScript
{
    private ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeDelete(RowData data, ref string reason)
    {
        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterDelete(RowData data, ref string reason)
    {
        //DB
        //19-2-2020
        //Onthoud welke records verwijderd worden. Dit wil de Appreo App weten.
        if (!SaveDeletedRecordIds(data, ref reason))
        {
            return false;
        }

        return true;
    }

    private bool SaveDeletedRecordIds(RowData data, ref string reason)
    {
        var rsDeletedRecords = _script.GetRecordset("C_DELETEDRECORDS", "",
            "PK_C_DELETEDRECORDS = -1", "");
        rsDeletedRecords.AddNew();
        rsDeletedRecords.Fields["TABLENAME"].Value = data.TableName;
        rsDeletedRecords.Fields["RECORDID"].Value = data[$"PK_{data.TableName}"];
        rsDeletedRecords.Update();

        return true;
    }
}
