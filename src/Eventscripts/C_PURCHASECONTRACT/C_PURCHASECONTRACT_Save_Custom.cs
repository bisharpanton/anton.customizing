﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;

public class C_PURCHASECONTRACT_Save_Custom : ISaveScript
{
    private static string _language;
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {

        // HVE
        // 9-2-2021
        // Set amount in letters
        if (!SetAmountInLetters(data, oldData, saveType, ref reason))
        {
            return false;
        }

        if (!SetHourlyRateInLetters(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        // HVE
        // 9-2-2021
        // Set contract text
        /*if (!SetContractTextByContracttype(data, oldData, saveType, ref reason))
        {
            return false;
        }*/

        return true;
    }


    private bool SetContractTextByContracttype(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterInsertSave && (int)data["REVISION"] > 0)
        {
            return true;
        }
        
        var contractType = (int)data["FK_CONTRACTTYPE"];
        if (saveType == SaveType.AfterUpdateSave)
        {
            var oldContractType = (int)oldData["FK_CONTRACTTYPE"];
            if (contractType == oldContractType)
            {
                return true;
            }
        }

        var languageId = DetermineLanguageId((int)data["FK_SUPPLIER"]);

        var rsDefaultText = _script.GetRecordset("C_CONTRACTDEFAULTTEXT", "", $"FK_CONTRACTTYPE = {contractType}", "");
        if (rsDefaultText.RecordCount == 0)
        {
            return true;
        }

        var contractId = (int)data["PK_C_PURCHASECONTRACT"];
        var rsContractText = _script.GetRecordset("C_CONTRACTTEXT", "", $"FK_PURCHASECONTRACT = {contractId}",
            "FK_REF_CONTRACTTEXT DESC");
        rsContractText.UseDataChanges = false;

        if (rsContractText.RecordCount > 0)
        {
            rsContractText.MoveFirst();
            while (!rsContractText.EOF)
            {
                rsContractText.Delete();
                rsContractText.MoveNext();
            }

            rsContractText.MoveFirst();
            rsContractText.Update();
        }

        var defaultTexts = rsDefaultText
            .DataTable
            .AsEnumerable();

        rsDefaultText.MoveFirst();
        while (!rsDefaultText.EOF)
        {
            rsContractText.AddNew();
            rsContractText.SetFieldValue("FK_PURCHASECONTRACT", contractId);
            rsContractText.SetFieldValue("FK_CHAPTER", (int)rsDefaultText.Fields["FK_CONTRACTCHAPTER"].Value);

            var text = (string)rsDefaultText.Fields["TEXT"].Value;
            if (languageId > 0)
            {
                var id = (int)rsDefaultText.Fields["PK_C_CONTRACTDEFAULTTEXT"].Value;
                var transText = _script.GetTranslatableFieldValue("C_CONTRACTDEFAULTTEXT", "TEXT", languageId, id);
                if (!string.IsNullOrWhiteSpace(transText))
                {
                    text = transText;
                }
            }
            rsContractText.SetFieldValue("MEMO", text);
            rsContractText.SetFieldValue("SEQUENCE", (int)rsDefaultText.Fields["SEQUENCE"].Value);
            rsContractText.SetFieldValue("CONTACTS", (bool)rsDefaultText.Fields["CONTACTS"].Value);
            rsContractText.SetFieldValue("FINANCIAL", (bool)rsDefaultText.Fields["FINANCIAL"].Value);

            var refText = rsDefaultText.Fields["FK_REF_CONTRACTTEXT"].Value as int? ?? 0;
            if (refText > 0)
            {
                var chapterId = defaultTexts
                    .Where(y => y.Field<int>("PK_C_CONTRACTDEFAULTTEXT") == refText)
                    .Select(x => x.Field<int>("FK_CONTRACTCHAPTER"))
                    .FirstOrDefault();
                var sequence = defaultTexts
                    .Where(y => y.Field<int>("PK_C_CONTRACTDEFAULTTEXT") == refText)
                    .Select(x => x.Field<int>("SEQUENCE"))
                    .FirstOrDefault();
                var refTextId = rsContractText
                    .DataTable
                    .AsEnumerable()
                    .Where(y => (y.Field<int>("FK_CHAPTER") == chapterId) 
                                && (y.Field<int>("SEQUENCE") == sequence ))
                    .Select(x => x.Field<int?>("PK_C_CONTRACTTEXT") ?? 0)
                    .FirstOrDefault();
                if (refTextId > 0)
                {
                    rsContractText.SetFieldValue("FK_REF_CONTRACTTEXT", refTextId);
                }

            }
            rsContractText.Update();
            rsDefaultText.MoveNext();
        }

      
       

        return true;
    }

    private int DetermineLanguageId(int supplierId)
    {
        return _script.GetRecordset("R_RELATION", "FK_LANGUAGE",
                $"PK_R_RELATION = {supplierId}", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int?>("FK_LANGUAGE") ?? 0)
            .FirstOrDefault();
    }


    private bool SetAmountInLetters(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            var fieldsToCompare = new[] {"AMOUNT", "FK_SUPPLIER"};
            var changed = false;
            foreach (var field in fieldsToCompare)
            {
                if (!object.Equals(oldData[field], data[field]))
                {
                    changed = true;
                    break;
                }
            }

            if (!changed)
            {
                return true;
            }
        }

        var supplierId = (int)data["FK_SUPPLIER"];
        _language = DetermineLanguage(supplierId);

        var amount = (double)data["AMOUNT"];

        var text = "";
        if (_language == "NL")
        {
            text = ConvertWholeNumberNL(amount.ToString());
        }
        else
        {
            text = ConvertWholeNumber(amount.ToString());
        }
        data["AMOUNT_TEXT"] = text;
        return true;

    }

    private bool SetHourlyRateInLetters(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            var fieldsToCompare = new[] { "HOURLYRATE", "FK_SUPPLIER" };
            var changed = false;
            foreach (var field in fieldsToCompare)
            {
                if (!object.Equals(oldData[field], data[field]))
                {
                    changed = true;
                    break;
                }
            }

            if (!changed)
            {
                return true;
            }
        }

        var supplierId = (int)data["FK_SUPPLIER"];
        _language = DetermineLanguage(supplierId);

        var amount = (double)data["HOURLYRATE"];

        var text = "";
        if (_language == "NL")
        {
            text = ConvertWholeNumberNL(amount.ToString());
        }
        else
        {
            text = ConvertWholeNumber(amount.ToString());
        }
        data["HOURLYRATETEXT"] = text;
        return true;

    }

    private string DetermineLanguage(int supplierId)
    {
        var languageId = _script.GetRecordset("R_RELATION", "FK_LANGUAGE",
                $"PK_R_RELATION = {supplierId}", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int>("FK_LANGUAGE"))
            .FirstOrDefault();

        return _script.GetRecordset("R_LANGUAGE", "CODE", $"PK_R_LANGUAGE = {languageId}", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<string>("CODE"))
            .FirstOrDefault();
    }

    private static string ConvertWholeNumberNL(string Number)
    {
        {
            string word = "";
            try
            {
                bool beginsZero = false;//tests for 0XX    
                bool isDone = false;//test if already translated    
                double dblAmt = (Convert.ToDouble(Number));
                //if ((dblAmt > 0) && number.StartsWith("0"))    
                if (dblAmt > 0)
                {//test for zero or digit zero in a nuemric    
                    beginsZero = Number.StartsWith("0");

                    int numDigits = Number.Length;
                    int pos = 0;//store digit grouping    
                    String place = "";//digit grouping name:hundres,thousand,etc...    
                    switch (numDigits)
                    {
                        case 1://ones' range    
                        case 2:
                            int _Number = Convert.ToInt32(Number);
                            if (_Number < 20)
                            {
                                word = UnderTwenty(_Number);
                            }
                            else
                            {
                                word = TwentyOrHeigher(_Number);
                            }
                            isDone = true;
                            break;
                        case 3://hundreds' range    
                            pos = (numDigits % 3) + 1;
                            place = " Honderd ";
                            break;
                        case 4://thousands' range    
                        case 5:
                        case 6:
                            pos = (numDigits % 4) + 1;
                            place = " Duizend ";
                            break;
                        case 7://millions' range    
                        case 8:
                        case 9:
                            pos = (numDigits % 7) + 1;
                            place = " Miljoen ";
                            break;
                        case 10://Billions's range    
                        case 11:
                        case 12:
                            pos = (numDigits % 10) + 1;
                            place = " Miljard ";
                            break;
                        //add extra case options for anything above Billion...    
                        default:
                            isDone = true;
                            break;
                    }
                    if (!isDone)
                    {//if transalation is not done, continue...(Recursion comes in now!!)    
                        if (Number.Substring(0, pos) != "0" && Number.Substring(pos) != "0")
                        {
                            try
                            {
                                word = ConvertWholeNumberNL(Number.Substring(0, pos)) + place + ConvertWholeNumberNL(Number.Substring(pos));
                            }
                            catch { }
                        }
                        else
                        {
                            word = ConvertWholeNumberNL(Number.Substring(0, pos)) + ConvertWholeNumberNL(Number.Substring(pos));
                        }

                        //check for trailing zeros    
                        //if (beginsZero) word = " and " + word.Trim();    
                    }
                    //ignore digit grouping names    
                    if (word.Trim().Equals(place.Trim()))
                        word = "";
                }
            }
            catch { }
            return word.Trim();
        }
    }

    private static string TwentyOrHeigher(int number)
    {
        var word1 = UnitsFromTwenty(number % 10);
        var word2 = TensOverTwenty(number / 10);

        return $"{word1} {word2}";
    }


    private static String ConvertWholeNumber(string Number)
    {
        string word = "";
        try
        {
            bool beginsZero = false;//tests for 0XX    
            bool isDone = false;//test if already translated    
            double dblAmt = (Convert.ToDouble(Number));
            //if ((dblAmt > 0) && number.StartsWith("0"))    
            if (dblAmt > 0)
            {//test for zero or digit zero in a nuemric    
                beginsZero = Number.StartsWith("0");

                int numDigits = Number.Length;
                int pos = 0;//store digit grouping    
                String place = "";//digit grouping name:hundres,thousand,etc...    
                switch (numDigits)
                {
                    case 1://ones' range    

                        word = ones(Number);
                        isDone = true;
                        break;
                    case 2://tens' range    
                        word = tens(Number);
                        isDone = true;
                        break;
                    case 3://hundreds' range    
                        pos = (numDigits % 3) + 1;
                        place = " Hundred ";
                        break;
                    case 4://thousands' range    
                    case 5:
                    case 6:
                        pos = (numDigits % 4) + 1;
                        place = " Thousand ";
                        break;
                    case 7://millions' range    
                    case 8:
                    case 9:
                        pos = (numDigits % 7) + 1;
                        place = " Million ";
                        break;
                    case 10://Billions's range    
                    case 11:
                    case 12:

                        pos = (numDigits % 10) + 1;
                        place = " Billion ";
                        break;
                    //add extra case options for anything above Billion...    
                    default:
                        isDone = true;
                        break;
                }
                if (!isDone)
                {//if transalation is not done, continue...(Recursion comes in now!!)    
                    if (Number.Substring(0, pos) != "0" && Number.Substring(pos) != "0")
                    {
                        try
                        {
                            word = ConvertWholeNumber(Number.Substring(0, pos)) + place + ConvertWholeNumber(Number.Substring(pos));
                        }
                        catch { }
                    }
                    else
                    {
                        word = ConvertWholeNumber(Number.Substring(0, pos)) + ConvertWholeNumber(Number.Substring(pos));
                    }

                    //check for trailing zeros    
                    //if (beginsZero) word = " and " + word.Trim();    
                }
                //ignore digit grouping names    
                if (word.Trim().Equals(place.Trim()))
                    word = "";
            }
        }
        catch { }
        return word.Trim();
    }

    #region numbers
    private static string UnitsFromTwenty(int number)
    {
        String name = "";
        switch (number)
        {
            case 1:
                name = "eenen";
                break;
            case 2:
                name = "tweeën";
                break;
            case 3:
                name = "drieën";
                break;
            case 4:
                name = "vieren";
                break;
            case 5:
                name = "vijfen";
                break;
            case 6:
                name = "zesen";
                break;
            case 7:
                name = "zevenen";
                break;
            case 8:
                name = "achten";
                break;
            case 9:
                name = "negenen";
                break;
        }

        return name;
    }

    private static string TensOverTwenty(int number)
    {
        String name = "";
        switch (number)
        {
            case 2:
                name = "twintig";
                break;
            case 3:
                name = "dertig";
                break;
            case 4:
                name = "veertig";
                break;
            case 5:
                name = "vijftig";
                break;
            case 6:
                name = "zestig";
                break;
            case 7:
                name = "zeventig";
                break;
            case 8:
                name = "tachtig";
                break;
            case 9:
                name = "negentig";
                break;
        }

        return name;
    }

    private static string UnderTwenty(int number)
    {
        String name = "";
        switch (number)
        {
            case 1:
                name = "Een";
                break;
            case 2:
                name = "Twee";
                break;
            case 3:
                name = "Drie";
                break;
            case 4:
                name = "Vier";
                break;
            case 5:
                name = "Vijf";
                break;
            case 6:
                name = "Zes";
                break;
            case 7:
                name = "Zeven";
                break;
            case 8:
                name = "Acht";
                break;
            case 9:
                name = "Negen";
                break;
            case 10:
                name = "Tien";
                break;
            case 11:
                name = "Elf";
                break;
            case 12:
                name = "Twaalf";
                break;
            case 13:
                name = "Dertien";
                break;
            case 14:
                name = "Veertien";
                break;
            case 15:
                name = "Vijftien";
                break;
            case 16:
                name = "Zestien";
                break;
            case 17:
                name = "Zeventien";
                break;
            case 18:
                name = "Achttien";
                break;
            case 19:
                name = "Negentien";
                break;
        }
        return name;
    }

    private static String ones(String Number)
    {
        int _Number = Convert.ToInt32(Number);
        String name = "";
        switch (_Number)
        {

            case 1:
                name = "One";
                break;
            case 2:
                name = "Two";
                break;
            case 3:
                name = "Three";
                break;
            case 4:
                name = "Four";
                break;
            case 5:
                name = "Five";
                break;
            case 6:
                name = "Six";
                break;
            case 7:
                name = "Seven";
                break;
            case 8:
                name = "Eight";
                break;
            case 9:
                name = "Nine";
                break;
        }
        return name;
    }

    private static String tens(String Number)
    {
        int _Number = Convert.ToInt32(Number);
        String name = null;
        switch (_Number)
        {
            case 10:
                name = "Ten";
                break;
            case 11:
                name = "Eleven";
                break;
            case 12:
                name = "Twelve";
                break;
            case 13:
                name = "Thirteen";
                break;
            case 14:
                name = "Fourteen";
                break;
            case 15:
                name = "Fifteen";
                break;
            case 16:
                name = "Sixteen";
                break;
            case 17:
                name = "Seventeen";
                break;
            case 18:
                name = "Eighteen";
                break;
            case 19:
                name = "Nineteen";
                break;
            case 20:
                name = "Twenty";
                break;
            case 30:
                name = "Thirty";
                break;
            case 40:
                name = "Fourty";
                break;
            case 50:
                name = "Fifty";
                break;
            case 60:
                name = "Sixty";
                break;
            case 70:
                name = "Seventy";
                break;
            case 80:
                name = "Eighty";
                break;
            case 90:
                name = "Ninety";
                break;
            default:
                if (_Number > 0)
                {
                    name = tens(Number.Substring(0, 1) + "0") + " " + ones(Number.Substring(1));
                }
                break;
        }
        return name;
    }
    #endregion


}
