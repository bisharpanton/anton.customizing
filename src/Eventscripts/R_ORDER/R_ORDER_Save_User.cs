﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.IO;
using System.Linq;
using Ridder.Common.ItemManagement;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using Ridder.Communication.Script;
using Ridder.Communication.Service;
using System.Text;
using System.Net;
using System.Diagnostics;

public class R_ORDER_Save_User : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //Bij het omzetten van een offerte naar order worden de kolommen 'Geplande startdatum productie' en 'Geplande einddatum productie' niet gevuld. 
        //Dit willen we wel voor de shopfloor.

        if (oldData == null && (int)data["ORDERTYPE"] == 4 && (data["PLANNEDPRODUCTIONSTARTDATE"] == DBNull.Value || data["PLANNEDPRODUCTIONSTARTDATE"] == null))
        {
            data["PLANNEDPRODUCTIONSTARTDATE"] = (DateTime)data["ORDERDATE"];
            data["PLANNEDPRODUCTIONENDDATE"] = ((DateTime)data["ORDERDATE"]).AddDays(1);
        }

        //Bij aanmaken van een order, automatisch ordermappen aanmaken vanuit ingestelde template
        if (saveType == SaveType.AfterInsertSave)
        {
            string subPath = GetCrmSetting("ORDERLOCATION") + "\\" + data["ORDERNUMBER"].ToString();
            bool IsExists = Directory.Exists(subPath);

            //Indien er al files bestaan voor dit ordernummer omdat die bv in het verleden zijn aangemaakt. Verwijder deze dan.
            if (IsExists)
            {
                clearFolder(subPath);
                Directory.Delete(subPath);
            }

            

            //HVE
            //20-12-2018
            //Instellingen CRM voorzien van standaard locatie
            string templateFolder = GetCrmSetting("ORDERTEMPLATELOCATION").ToString();
            bool templateFolderExists = Directory.Exists(templateFolder);
            if (templateFolderExists)
            {
                Copy(templateFolder, subPath);
            }
        }

        //DB
        //08-01-2019
        //Bij TATA-orders wordt de standaard leverdatum onzichtbaar op de layout en komt het veld 'TATA geplande opleverdatm' zichtbaar. Houd in dit geval 
        //de standaard leverdatum gelijk aan het veld 'TATA geplande opleverdatum'. 
        //Deze functionaliteit is alleen van toepassing voor Anton Constructiewerken
        if (!CopyTataDeliveryDate(data, oldData, saveType, ref reason))
        {
            return false;
        }

       return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //Automatisch bonnen aanmaken
        if (!CreateJoborders(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //DB
        //13-11-2018
        //Kopieer werkadres naar inkooporders
        if (!CopyWorkAdressToPurchaseOrders(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //DB
        //15-12-2020
        //Automatisch updaten CreditSafe info bij de relatie indien relatie nu niet in CreditSafe portfolio valt
        if (!UpdateCreditSafeInfo(data, oldData, saveType, ref reason))
        {
            //DB - 28-11-2023 - Uitgezet dat het aanmaken van offertes/orders geblokeerd wordt als de creditsafe koppeling eruit ligt. Dat merken we ook wel bij de relatie import.
            //return false;
        }

        //DB
        //1-2-2023
        //Genereer een Outlook mailfolder in de Postvak In per order
        if (!CreateOrUpdateOutlookFolder(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    private bool CreateOrUpdateOutlookFolder(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        var createOutlookOrderFolders = _script.GetRecordset("R_CRMSETTINGS", "CREATEOUTLOOKORDERFOLDERS", "", "")
            .DataTable.AsEnumerable().FirstOrDefault()?.Field<bool>("CREATEOUTLOOKORDERFOLDERS") ?? false;

        if(!createOutlookOrderFolders)
        {
            return true;
        }

        if(saveType == SaveType.AfterUpdateSave && oldData["DESCRIPTION"].ToString().Equals(data["DESCRIPTION"].ToString()))
        {
            //Alleen doorgaan bij aanmaken order of wijzigen omschrijving
            return true;
        }

        var wfCreateOrUpdateOutlookOrderFolder = new Guid("3aaf506f-cbf5-4fc1-9e09-4f64beae48fc");

        var wfParameters = new Dictionary<string, object>();
        wfParameters.Add("OrderWordtAfgesloten", false);
        wfParameters.Add("OrderAfsluitenWordtTeruggedraaid", false);

        var wfResult = _script.ExecuteWorkflowEvent("R_ORDER", (int)data["PK_R_ORDER"], wfCreateOrUpdateOutlookOrderFolder, wfParameters);

        if(wfResult.HasError)
        {
            reason = wfResult.GetResult();
            return false;
        }

        return true;
    }

    private bool UpdateCreditSafeInfo(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true; //Alleen van toepassing bij aanmaken order
        }
        
        var relation = _script.GetRecordset("R_RELATION", "CREDITSAFEID, COCNUMBER",
            $"PK_R_RELATION = {data["FK_RELATION"]}", "").DataTable.AsEnumerable().First();

        if (string.IsNullOrEmpty(relation.Field<string>("CREDITSAFEID")))
        {
            return true; //Geen CreditSafe relatie
        }

        var relationInCreditSafePortfolio = _script.GetRecordset("C_CREDITSAFECHECKRELATIONINPORTFOLIO",
            "PK_C_CREDITSAFECHECKRELATIONINPORTFOLIO",
            $"FK_RELATION = {data["FK_RELATION"]}", "").RecordCount > 0;

        if (relationInCreditSafePortfolio)
        {
            return true;
        }

        var relationTodayCreatedOrUpdated = _script.GetRecordset("C_CREDITSAFEREQUESTS", "PK_C_CREDITSAFEREQUESTS",
            $"FK_RELATION = {data["FK_RELATION"]} AND DATECREATED >= '{DateTime.Now:yyyyMMdd}'", "").RecordCount > 0;

        if (relationTodayCreatedOrUpdated)
        {
            return true;
        }

        if (!UpdateCreditSafeInfoCore(data, oldData, saveType, ref reason, relation))
        {
            return false;
        }

        return true;
    }

    private bool UpdateCreditSafeInfoCore(RowData data, RowData oldData, SaveType saveType, ref string reason,
        DataRow relation)
    {
        var createdCreditSafeRequest =
            CreateCreditSafeRequest((int)data["FK_RELATION"], relation.Field<string>("CREDITSAFEID"), relation.Field<string>("COCNUMBER"), ref reason);

        if (createdCreditSafeRequest == 0)
        {
            return false;
        }

        if (!ProcessCreditSafeRequest(createdCreditSafeRequest, ref reason))
        {
            return false;
        }

        var creditSafeRequest = _script.GetRecordset("C_CREDITSAFEREQUESTS", "FK_RELATION, IMPORTMESSAGE, FK_WORKFLOWSTATE",
                $"PK_C_CREDITSAFEREQUESTS = {createdCreditSafeRequest}", "")
            .DataTable.AsEnumerable().First();

        var wfStateCancelled = new Guid("c30fde4d-db6a-4e8f-af2f-9b18b3c88dc0");

        if ((Guid)creditSafeRequest.Field<Guid>("FK_WORKFLOWSTATE") == wfStateCancelled)
        {
            reason = $"Updaten CreditSafe info mislukt {creditSafeRequest.Field<string>("IMPORTMESSAGE")}";
            return false;
        }

        return true;
    }

    private bool ProcessCreditSafeRequest(int createdCreditSafeRequest, ref string reason)
    {
        var wfImportRelationFromCreditSafe = new Guid("2b0d3015-d70b-4fb4-a143-2b456e060397");

        var wfResult = _script.ExecuteWorkflowEvent("C_CREDITSAFEREQUESTS", createdCreditSafeRequest,
            wfImportRelationFromCreditSafe, null);

        if (wfResult.HasError)
        {
            reason =
                $"Uitvoeren workflow 'Importeer relatie vanuit CreditSafe' mislukt, oorzaak: {wfResult.GetResult()}";

            return false;
        }

        return true;
    }

    private int CreateCreditSafeRequest(int relationId, string creditSafeId, string cocNumber, ref string reason)
    {
        var rsCreditSafeRequests = _script.GetRecordset("C_CREDITSAFEREQUESTS", "",
            "PK_C_CREDITSAFEREQUESTS = -1", "");
        rsCreditSafeRequests.UseDataChanges = false;

        rsCreditSafeRequests.AddNew();

        rsCreditSafeRequests.SetFieldValue("FK_RELATION", relationId);
        rsCreditSafeRequests.Fields["CREDITSAFEID"].Value = creditSafeId;
        rsCreditSafeRequests.Fields["COCNUMBER"].Value = cocNumber;

        rsCreditSafeRequests.SetFieldValue("ISUPDATECREDITSAFEINFO", true);

        var updateResult = rsCreditSafeRequests.Update2();

        if (updateResult != null && updateResult.Any(x => x.HasError))
        {
            reason = $"Aanmaken nieuw CreditSafe request is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}";

            return 0;
        }

        return (int)updateResult.First().PrimaryKey;
    }

    private int GetDefaultTradingNameId(int relationId)
    {
        return _script.GetRecordset("C_DIVERGENTTRADINGNAMES", "",
                $"FK_RELATION = {relationId} AND DEFAULTTRADINGNAME = TRUE", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int?>("PK_C_DIVERGENTTRADINGNAMES") ?? 0)
            .FirstOrDefault();
    }

    private bool CopyTataDeliveryDate(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (_script.GetUserInfo().CompanyName != "Anton Constructiewerken")
        {
            return true;
        }

        //Bij de relatie TATA en als het veld 'TATA geplande opleverdatum' wordt ingevuld of wordt gewijzigd
        if ((int)data["FK_RELATION"] == 3739 && (data["TATADELIVERYDATE"] as DateTime?).HasValue &&
            (oldData == null || oldData != null && (!(oldData["TATADELIVERYDATE"] as DateTime?).HasValue || ((oldData["TATADELIVERYDATE"] as DateTime?).HasValue && (DateTime)oldData["TATADELIVERYDATE"] != (DateTime)data["TATADELIVERYDATE"]))))
        {
            data["DELIVERYDATE"] = data["TATADELIVERYDATE"];
        }

        return true;
    }

    private bool CreateJoborders(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true; //Alleen doorgaan bij toevoegen order
        }

        if ((OrderType)data["ORDERTYPE"] == OrderType.Service)
        {
            return true;
        }

        var tableNameJobordersToCreate = (OrderType)data["ORDERTYPE"] == OrderType.Time_Material
            ? "C_JOBORDERSTOCREATE"
            : "C_JOBORDERSTOCREATESALESORDER";

        var jobordersToCreate = _script.GetRecordset(tableNameJobordersToCreate,
            "JOBORDERNUMBER, DESCRIPTION, EXPORTTOAPPREO", "", "").DataTable.AsEnumerable();

        if (!jobordersToCreate.Any())
        {
            return true;
        }

        var rsJoborders = _script.GetRecordset("R_JOBORDER", "", "PK_R_JOBORDER = -1", "");
        rsJoborders.UseDataChanges = true;
        rsJoborders.UpdateWhenMoveRecord = false;

        foreach (var joborder in jobordersToCreate)
        {
            rsJoborders.AddNew();
            rsJoborders.Fields["FK_ORDER"].Value = data["PK_R_ORDER"];
            rsJoborders.Fields["JOBORDERNUMBER"].Value = joborder.Field<int>("JOBORDERNUMBER");
            rsJoborders.Fields["DESCRIPTION"].Value = joborder.Field<string>("DESCRIPTION");
            rsJoborders.Fields["MINIMALPRODUCTIONSTARTDATE"].Value = data["MINIMALPRODUCTIONSTARTDATE"];
            rsJoborders.Fields["MAXIMALPRODUCTIONFINISHDATE"].Value = data["MAXIMALPRODUCTIONFINISHDATE"];
            rsJoborders.Fields["TIMEANDMATERIAL"].Value = true;

            rsJoborders.Fields["EXPORTTOAPPREO"].Value = joborder.Field<bool>("EXPORTTOAPPREO");
        }

        var updateResult = rsJoborders.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            reason =
                $"Het automatisch aanmaken van bonnen is mislukt, oorzaak {updateResult.First(x => x.HasError).GetResult()}";
            return false;
        }

        var createdJoborders = rsJoborders.DataTable.AsEnumerable().ToList();

        foreach (var createdJoborder in createdJoborders)
        {
            var joborderId = createdJoborder.Field<int>("PK_R_JOBORDER");
            var joborderNumber = createdJoborder.Field<int>("JOBORDERNUMBER");

            var jobOrderToCreatePrimaryKeyColumn = (OrderType)data["ORDERTYPE"] == OrderType.Time_Material
                ? "PK_C_JOBORDERSTOCREATE"
                : "PK_C_JOBORDERSTOCREATESALESORDER";

            var jobOrderToCreatePrimaryKey = jobordersToCreate.First(x => x.Field<int>("JOBORDERNUMBER") == joborderNumber).Field<int>(jobOrderToCreatePrimaryKeyColumn);

            var tableNameJobOrderWorkActivitesToCreate = (OrderType)data["ORDERTYPE"] == OrderType.Time_Material
                ? "U_JOBORDERDETAILWORKACTIVITYTOCREATE"
                : "U_JOBORDERDETAILWORKACTIVITYTOCREATESALESORDER";

            var columnNameFkJobOrderToCreate = (OrderType)data["ORDERTYPE"] == OrderType.Time_Material
                ? "FK_JOBORDERSTOCREATE"
                : "FK_JOBORDERSTOCREATESALESORDER";

            var rsJobOrderDetailWorkActivitiesToCreate = _script.GetRecordset(tableNameJobOrderWorkActivitesToCreate,
                    $"{columnNameFkJobOrderToCreate}, FK_WORKACTIVITY, SETUPTIME, OPERATIONTIME", $"{columnNameFkJobOrderToCreate} = {jobOrderToCreatePrimaryKey}", "")
                    .DataTable.AsEnumerable();

            if(!rsJobOrderDetailWorkActivitiesToCreate.Any())
            {
                continue;
            }

            var rsJobOrderDetailWorkActivities = _script.GetRecordset("R_JOBORDERDETAILWORKACTIVITY", "", "PK_R_JOBORDERDETAILWORKACTIVITY = -1", "");
            rsJobOrderDetailWorkActivities.UseDataChanges = true;
            rsJobOrderDetailWorkActivities.UpdateWhenMoveRecord = false;

            foreach (var jobOrderDetailWorkActivityToCreate in rsJobOrderDetailWorkActivitiesToCreate)
            {
                rsJobOrderDetailWorkActivities.AddNew();
                rsJobOrderDetailWorkActivities.Fields["FK_ORDER"].Value = data["PK_R_ORDER"];
                rsJobOrderDetailWorkActivities.Fields["FK_JOBORDER"].Value = joborderId;
                rsJobOrderDetailWorkActivities.Fields["FK_WORKACTIVITY"].Value = jobOrderDetailWorkActivityToCreate.Field<int>("FK_WORKACTIVITY");
                rsJobOrderDetailWorkActivities.Fields["SETUPTIME"].Value = jobOrderDetailWorkActivityToCreate.Field<long?>("SETUPTIME") ?? 0;
                rsJobOrderDetailWorkActivities.Fields["OPERATIONTIME"].Value = jobOrderDetailWorkActivityToCreate.Field<long?>("OPERATIONTIME") ?? 0;
            }

            var updateResult2 = rsJobOrderDetailWorkActivities.Update2();

            if (updateResult2.Any(x => x.HasError))
            {
                reason =
                    $"Het automatisch aanmaken van bonnen bewerking regels is mislukt, oorzaak {updateResult2.First(x => x.HasError).GetResult()}";
                return false;
            }
        }

        return true;
    }

    private bool CopyWorkAdressToPurchaseOrders(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterInsertSave)
        {
            return true;
        }

        if (!(bool)GetCrmSetting("COPYWORKADDRESSORDERTOPURCHASEORDER"))
        {
            return true;
        }

        if (!(data["FK_DESTINATIONADDRESS"] as int?).HasValue)
        {
            return true;
        }

        if (oldData["FK_DESTINATIONADDRESS"].ToString() == data["FK_DESTINATIONADDRESS"].ToString())
        {
            return true; //Alleen doorgaan indien werkadres wordt gewijzigd in de order
        }

        ScriptRecordset rsMaterialState = _script.GetRecordset("R_MATERIALSTATE", "FK_PURCHASEORDER",
            $"FK_ORDER = {data["PK_R_ORDER"]} AND FK_PURCHASEORDER IS NOT NULL", "");

        if (rsMaterialState.RecordCount == 0)
        {
            return true;
        }

        List<int> purchaseOrderIds = rsMaterialState.DataTable.AsEnumerable()
            .Select(x => x.Field<int>("FK_PURCHASEORDER")).Distinct().ToList();

        var rsPurchaseOrders = _script.GetRecordset("R_PURCHASEORDER", "FK_WORKADRESSORDER",
            $"PK_R_PURCHASEORDER IN ({string.Join(",", purchaseOrderIds)}) AND (FK_WORKADRESSORDER IS NULL OR FK_WORKADRESSORDER == {oldData["FK_DESTINATIONADDRESS"] as int? ?? 0}) AND FINANCIALDONE = 0",
            "");

        if (rsPurchaseOrders.RecordCount == 0)
        {
            return true;
        }

        rsPurchaseOrders.UpdateWhenMoveRecord = false;
        rsPurchaseOrders.UseDataChanges = false;

        rsPurchaseOrders.MoveFirst();
        while (!rsPurchaseOrders.EOF)
        {
            rsPurchaseOrders.SetFieldValue("FK_WORKADRESSORDER", data["FK_DESTINATIONADDRESS"]);

            rsPurchaseOrders.MoveNext();
        }

        rsPurchaseOrders.MoveFirst();
        rsPurchaseOrders.Update();

        return true;
    }

    public void clearFolder(string FolderName)
    {
        DirectoryInfo dir = new DirectoryInfo(FolderName);

        foreach (FileInfo fi in dir.GetFiles())
        {
            fi.Delete();
        }

        foreach (DirectoryInfo di in dir.GetDirectories())
        {
            clearFolder(di.FullName);
            di.Delete();
        }
    }

    void Copy(string sourceDir, string targetDir)
    {
        if (!Directory.Exists(sourceDir))
        {
            return;
        }

        Directory.CreateDirectory(targetDir);

        foreach (var file in Directory.GetFiles(sourceDir))
            File.Copy(file, Path.Combine(targetDir, Path.GetFileName(file)));

        foreach (var directory in Directory.GetDirectories(sourceDir))
            Copy(directory, Path.Combine(targetDir, Path.GetFileName(directory)));
    }


    public object GetCrmSetting(string s)
    {
        var rsCRM = _script.GetRecordset("R_CRMSETTINGS", s, "", "");
        rsCRM.MoveFirst();

        return rsCRM.Fields[s].Value;
    }

    private enum JobordersCreate : int
    {
        AtTimeAndMaterial = 1,
        Always = 2
    }
}
