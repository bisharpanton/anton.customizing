﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;

public class C_PURCHASECONTRACTDOCUMENT_Delete_User : IDeleteScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeDelete(RowData data, ref string reason)
    {
        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterDelete(RowData data, ref string reason)
    {
        //SP
        //8-12-2022
        //Update chapter numbers of other chapters
        if (!UpdateAttachmentNumberOfOtherAttachments(data, ref reason))
        {
            return false;
        }

        return true;
    }

    private bool UpdateAttachmentNumberOfOtherAttachments(RowData data, ref string reason)
    {
        var attachmentNumber = (int)data["ATTACHMENTNUMBER"];
        var purchaseContractId = (int)data["FK_PURCHASECONTRACT"];

        var rsDocumentsToUpdate = _script.GetRecordset("C_PURCHASECONTRACTDOCUMENT", "ATTACHMENTNUMBER",
            $"FK_PURCHASECONTRACT = {purchaseContractId} AND ATTACHMENTNUMBER > {attachmentNumber}", "ATTACHMENTNUMBER");
        var update = false;

        rsDocumentsToUpdate.UpdateWhenMoveRecord = false;
        rsDocumentsToUpdate.MoveFirst();
        while (!rsDocumentsToUpdate.EOF)
        {
            var oldChapter = (int)rsDocumentsToUpdate.Fields["ATTACHMENTNUMBER"].Value;
            rsDocumentsToUpdate.SetFieldValue("ATTACHMENTNUMBER", oldChapter-1);
            update = true;

            rsDocumentsToUpdate.MoveNext();
        }

        if (update)
        {
            rsDocumentsToUpdate.MoveFirst();
            rsDocumentsToUpdate.Update();
        }

        return true;
    }
}
