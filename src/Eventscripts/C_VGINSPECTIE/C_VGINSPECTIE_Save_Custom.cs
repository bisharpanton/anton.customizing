﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Communication.Script;

public class C_VGINSPECTIE_Save_Custom : ISaveScript
{
    private ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //18-04-2017
        //Indien een onderdeel op 'Onvoldoende' staat, dan is de memo verplicht.
		
        //1. Haal alle keuze kolommen op
		
        Guid pkChoiceType = new Guid("48481a61-5c39-49af-891d-83a1622b76d4");
		
        ScriptRecordset rsTableInfo = new ScriptRecordset(ScriptRecordset.GetDataSet("M_TABLEINFO", "PK_M_TABLEINFO", 
            string.Format("TABLENAME = '{0}'", data.TableName ), ""));
        rsTableInfo.MoveFirst();
        Guid pkTableInfo = (Guid) rsTableInfo.Fields["PK_M_TABLEINFO"].Value;
		
        ScriptRecordset rsColumnInfo = new ScriptRecordset(ScriptRecordset.GetDataSet("M_COLUMNINFO", "COLUMNNAME, DESCRIPTION", 
            string.Format("FK_TABLEINFO = '{0}' AND FK_CHOICETYPE = '{1}'", pkTableInfo , pkChoiceType), ""));
        rsColumnInfo.MoveFirst();
		
        //2. Loop alle kolommen langs van het huidige record
        while(!rsColumnInfo.EOF)
        {
            string columnnameCheck = rsColumnInfo.Fields["COLUMNNAME"].Value.ToString();
            string columnnameMemo = columnnameCheck.Replace('1', '2');
			
            if( (int) data[columnnameCheck] == 2 && data[columnnameMemo].ToString() == "" ) //Onvoldoende en memo niet ingevuld
            {
                reason = string.Format("Bij de kolom '{0}' is 'Onvoldoende' ingegeven, maar de memo is niet ingevuld.", rsColumnInfo.Fields["DESCRIPTION"].Value );
                return false;
            }
			
            rsColumnInfo.MoveNext();
        }

        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }
}
