﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;

public class U_WKAREQUIREDDOCUMENTTYPESPERWKAEMPLOYEE_Save_User : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //22-6-2023
        //Check of WKA dossier compleet is

        var wkaFileComplete = CheckIfWkaFileIsComplete((int)data["FK_WKAEMPLOYEE"]);
        UpdateBoolAtWkaEmployee((int)data["FK_WKAEMPLOYEE"], wkaFileComplete);

        return true;
    }

    private void UpdateBoolAtWkaEmployee(int wkaEmployeeId, bool wkaFileComplete)
    {
        var rsWkaEmployee = _script.GetRecordset("U_WKAEMPLOYEE", "WKAFILECOMPLETE", $"PK_U_WKAEMPLOYEE = {wkaEmployeeId}", "");
        rsWkaEmployee.MoveFirst();
        rsWkaEmployee.SetFieldValue("WKAFILECOMPLETE", wkaFileComplete);
        rsWkaEmployee.Update();
    }

    private bool CheckIfWkaFileIsComplete(int wkaEmployeeId)
    {
        var requiredDocumentTypeIds = _script.GetRecordset("U_WKAREQUIREDDOCUMENTTYPESPERWKAEMPLOYEE", "FK_WKADOCUMENTTYPE",
            $"FK_WKAEMPLOYEE = {wkaEmployeeId}", "").DataTable.AsEnumerable().Select(x => x.Field<int>("FK_WKADOCUMENTTYPE")).ToList();

        var wkaEmployeeDocuments = _script.GetRecordset("U_WKAEMPLOYEEDOCUMENT", "FK_WKADOCUMENTTYPE, EXPIRATIONDATE",
            $"FK_WKAEMPLOYEE = {wkaEmployeeId} AND FK_WKADOCUMENTTYPE IS NOT NULL AND EXPIRATIONDATE IS NOT NULL", "").DataTable.AsEnumerable().ToList();

        foreach (var documentId in requiredDocumentTypeIds)
        {
            var employeeDocumentsThisDocumentId = wkaEmployeeDocuments.Where(x => x.Field<int>("FK_WKADOCUMENTTYPE") == documentId).ToList();

            if(!employeeDocumentsThisDocumentId.Any())
            {
                return false;
            }

            if(!employeeDocumentsThisDocumentId.Any(x => x.Field<DateTime?>("EXPIRATIONDATE").HasValue && x.Field<DateTime>("EXPIRATIONDATE").Date >= DateTime.Now.Date))
            {
                return false;
            }
        }

        return true;
    }
}
