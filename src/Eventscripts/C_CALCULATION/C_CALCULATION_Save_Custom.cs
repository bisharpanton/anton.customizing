﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Communication.Script;

public class C_CALCULATION_Save_Custom : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (!CopyDescriptionToSalesDetails(data, oldData, saveType, ref reason))
        {
            return false;
        }


        return true;
    }

    private bool CopyDescriptionToSalesDetails(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterInsertSave)
        {
            return true; //Alleen bij wijzigen omschrijving van toepassing
        }

        if (oldData["DESCRIPTION"].ToString().Equals(data["DESCRIPTION"].ToString()))
        {
            return true; //Omschrijving wijzigt niet
        }

        if(!(data["FK_OFFER"] as int?).HasValue || !(data["FK_ASSEMBLY"] as int?).HasValue)
        {
            return true; //Geen offerte of stuklijst gekoppeld
        }

        var rsOfferDetailAssembly = _script.GetRecordset("R_OFFERDETAILASSEMBLY", 
            "DESCRIPTION",
            $"FK_OFFER = {data["FK_OFFER"]} AND FK_ASSEMBLY = {data["FK_ASSEMBLY"]}", "");

        if (rsOfferDetailAssembly.RecordCount == 0)
        {
            return true;
        }

        rsOfferDetailAssembly.MoveFirst();
        rsOfferDetailAssembly.SetFieldValue("DESCRIPTION", data["DESCRIPTION"]);
        rsOfferDetailAssembly.Update();

        return true;
    }
}
