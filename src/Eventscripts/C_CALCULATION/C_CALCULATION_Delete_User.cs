﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;

public class C_CALCULATION_Delete_User : IDeleteScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeDelete(RowData data, ref string reason)
    {
        if (!DeleteCalclulationRules(data, ref reason))
        {
            return false;
        }

        if (!DeleteFlatCalclulations(data, ref reason))
        {
            return false;
        }

        if (!DeleteSubCalclulations(data, ref reason))
        {
            return false;
        }


        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterDelete(RowData data, ref string reason)
    {
        return true;
    }

    private bool DeleteCalclulationRules(RowData data, ref string reason)
    {
        var rsCalculationRules = _script.GetRecordset("C_CALCULATIONRULE", "",
            $"FK_CALCULATION = {data["PK_C_CALCULATION"]}", "");

        if (rsCalculationRules.RecordCount == 0)
        {
            return true;
        }

        rsCalculationRules.UpdateWhenMoveRecord = false;
        rsCalculationRules.MoveFirst();
        while (!rsCalculationRules.EOF)
        {
            rsCalculationRules.Delete();
            rsCalculationRules.MoveNext();
        }

        rsCalculationRules.MoveFirst();
        rsCalculationRules.Update();

        return true;
    }

    private bool DeleteFlatCalclulations(RowData data, ref string reason)
    {
        var rsFlatCalculations = _script.GetRecordset("C_FLATCALCULATION", "",
            $"FK_CALCULATION = {data["PK_C_CALCULATION"]}", "");

        if (rsFlatCalculations.RecordCount == 0)
        {
            return true;
        }

        rsFlatCalculations.UpdateWhenMoveRecord = false;
        rsFlatCalculations.MoveFirst();
        while (!rsFlatCalculations.EOF)
        {
            rsFlatCalculations.Delete();
            rsFlatCalculations.MoveNext();
        }

        rsFlatCalculations.MoveFirst();
        rsFlatCalculations.Update();

        return true;
    }

    private bool DeleteSubCalclulations(RowData data, ref string reason)
    {
        var rsSubCalculations = _script.GetRecordset("C_CALCULATION", "",
            $"FK_MAINCALCULATION = {data["PK_C_CALCULATION"]}", "");

        if(rsSubCalculations.RecordCount == 0)
        {
            return true;
        }

        rsSubCalculations.UpdateWhenMoveRecord = false;
        rsSubCalculations.MoveFirst();
        while(!rsSubCalculations.EOF)
        {
            rsSubCalculations.Delete();
            rsSubCalculations.MoveNext();
        }

        rsSubCalculations.MoveFirst();
        rsSubCalculations.Update();

        return true;
    }

}
