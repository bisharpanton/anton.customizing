﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;

public class U_WKAEMPLOYEE_Save_User : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //31-7-2023
        //Check of BSN of Registratienummer al bij een andere WKA werknemer voorkomt

        if (!CheckUniqueBsnOrRegno(data, oldData, saveType, ref reason))
        {
            return false;
        }


        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //22-6-2023
        //Indien een WKA werknemer een KVK nummer heeft dan is het een ZZP'er en dan moet automatisch het KvK uittreksel als document worden gekoppeld

        if(!AutomaticLinkKvk(data, oldData, saveType, ref reason))
        {
            return false;
        }


        return true;
    }

    private bool CheckUniqueBsnOrRegno(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if(saveType == SaveType.AfterUpdateSave && oldData["BSN"].ToString().Equals(data["BSN"].ToString()) && oldData["REGNO"].ToString().Equals(data["REGNO"].ToString()))
        {
            return true; //BSN of registratienummer wordt niet gewijzigd
        }

        if(!string.IsNullOrEmpty(data["BSN"].ToString()))
        {
            var otherWkaEmployeeSameBsn = _script.GetRecordset("U_WKAEMPLOYEE", "PK_U_WKAEMPLOYEE",
                $"BSN = '{data["BSN"]}' AND PK_U_WKAEMPLOYEE <> {data["PK_U_WKAEMPLOYEE"]}", "");

            if(otherWkaEmployeeSameBsn.RecordCount > 0)
            {
                reason = $"BSN '{data["BSN"]}' komt al voor bij een andere WKA werknemer. Dit is niet toegestaan.";
                return false;
            }
        }

        if (!string.IsNullOrEmpty(data["REGNO"].ToString()))
        {
            var otherWkaEmployeeSameRegno = _script.GetRecordset("U_WKAEMPLOYEE", "PK_U_WKAEMPLOYEE",
                $"REGNO = '{data["REGNO"]}' AND PK_U_WKAEMPLOYEE <> {data["PK_U_WKAEMPLOYEE"]}", "");

            if (otherWkaEmployeeSameRegno.RecordCount > 0)
            {
                reason = $"Registratienummer '{data["REGNO"]}' komt al voor bij een andere WKA werknemer. Dit is niet toegestaan.";
                return false;
            }
        }

        return true;
    }

    private bool AutomaticLinkKvk(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if(saveType == SaveType.AfterInsertSave && !string.IsNullOrEmpty(data["COCNUMBER"].ToString()) ||
            saveType == SaveType.AfterUpdateSave && string.IsNullOrEmpty(oldData["COCNUMBER"].ToString()) && !string.IsNullOrEmpty(data["COCNUMBER"].ToString()))
        {
            var documentTypeIdKvkUittreksel = _script.GetRecordset("U_WKADOCUMENTTYPE", "PK_U_WKADOCUMENTTYPE",
                $"DESCRIPTION = 'KvK uittreksel'", "").DataTable.AsEnumerable().FirstOrDefault()?.Field<int>("PK_U_WKADOCUMENTTYPE") ?? 0;

            if(documentTypeIdKvkUittreksel == 0)
            {
                reason = "Geen documenttype 'KvK uittreksel' gevonden.";
                return false;
            }

            var rsRequiredDocumentTypesPerEmployee = _script.GetRecordset("U_WKAREQUIREDDOCUMENTTYPESPERWKAEMPLOYEE", "",
                $"FK_WKAEMPLOYEE = {data["PK_U_WKAEMPLOYEE"]} AND FK_WKADOCUMENTTYPE = {documentTypeIdKvkUittreksel}", "");

            if(rsRequiredDocumentTypesPerEmployee.RecordCount > 0)
            {
                return true; //Document is al gekoppeld
            }

            rsRequiredDocumentTypesPerEmployee.AddNew();
            rsRequiredDocumentTypesPerEmployee.SetFieldValue("FK_WKAEMPLOYEE", data["PK_U_WKAEMPLOYEE"]);
            rsRequiredDocumentTypesPerEmployee.SetFieldValue("FK_WKADOCUMENTTYPE", documentTypeIdKvkUittreksel);
            rsRequiredDocumentTypesPerEmployee.Update();
        }

        return true;
    }
}
