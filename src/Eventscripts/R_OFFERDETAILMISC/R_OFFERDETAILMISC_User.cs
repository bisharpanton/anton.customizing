﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;

public class R_OFFERDETAILMISC_Save_User : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //SP
        //11-8-2022
        //Bij aanmaken regel, vul de tabel meerwerk opslagpercentages vanuit het de standaard opslagpercentages van het project
        if (!CopyFormatAdditionalWorkMarkup(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //SP
        //29-3-2022
        //Wanneer er een productgroep wijzigt, vul opnieuw de memovelden
        if (!FillTextFields(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }
    private bool CopyFormatAdditionalWorkMarkup(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        var projectModuleActive = _script.GetRecordset("R_CRMSETTINGS", "PROJECTMODULEACTIVE",
            "", "").DataTable.AsEnumerable().First().Field<bool>("PROJECTMODULEACTIVE");

        if (!projectModuleActive)
        {
            return true;//Alleen van toepassing indien vinkje 'Projectenmodule actief' aanstaat
        }

        if (saveType == SaveType.AfterUpdateSave)
        {
            return true;//Alleen doorgaan bij eerste keer aanmaken
        }

        var mainprojectId = _script.GetRecordset("R_OFFER", "FK_MAINPROJECT",
            $"PK_R_OFFER = {data["FK_OFFER"]}", "").DataTable.AsEnumerable().FirstOrDefault().Field<int?>("FK_MAINPROJECT") as int? ?? 0;

        if(mainprojectId == 0)
        {
            return true;//Alleen doorgaan indien project aanwezig
        }

        var additionalWorkFormats = _script.GetRecordset("U_ADDITIONALWORKMARKUPPROJECT", "SEQUENCENUMBER, DESCRIPTION, PERCENTAGE, PROJECTTEAM, NOCOSTPRICENEEDED",
              $"FK_MAINPROJECT = {mainprojectId}", "")
          .DataTable.AsEnumerable().Select(x => new Format()
          {
              SequenceNumber = x.Field<int>("SEQUENCENUMBER"),
              Description = x.Field<string>("DESCRIPTION"),
              Percentage = x.Field<double>("PERCENTAGE"),
              Projectteam = x.Field<bool>("PROJECTTEAM"),
              NoCostpriceNeeded = x.Field<bool>("NOCOSTPRICENEEDED")

          }).ToList();


        var rsAdditionalWorkMarkup = _script.GetRecordset("U_ADDITIONALWORKMARKUP", "", "PK_U_ADDITIONALWORKMARKUP = -1", "");
        rsAdditionalWorkMarkup.MoveFirst();
        rsAdditionalWorkMarkup.UseDataChanges = false;
        rsAdditionalWorkMarkup.UpdateWhenMoveRecord = false;

        foreach (Format additionalWorkFormat in additionalWorkFormats)
        {
            rsAdditionalWorkMarkup.AddNew();
            rsAdditionalWorkMarkup.Fields["FK_OFFERDETAILMISC"].Value = data["PK_R_OFFERDETAILMISC"];
            rsAdditionalWorkMarkup.Fields["FK_OFFER"].Value = data["FK_OFFER"];
            rsAdditionalWorkMarkup.Fields["SEQUENCENUMBER"].Value = additionalWorkFormat.SequenceNumber;
            rsAdditionalWorkMarkup.Fields["DESCRIPTION"].Value = additionalWorkFormat.Description;
            rsAdditionalWorkMarkup.Fields["PERCENTAGE"].Value = additionalWorkFormat.Percentage;
            rsAdditionalWorkMarkup.Fields["PROJECTTEAM"].Value = additionalWorkFormat.Projectteam;
            rsAdditionalWorkMarkup.Fields["NOCOSTPRICENEEDED"].Value = additionalWorkFormat.NoCostpriceNeeded;
        }

        var updateResult = rsAdditionalWorkMarkup.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            reason = $"Het inladen van het format opslagpercentages meerwerk is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}";
            return false;
        }

        //SP 12-1-2023
        //Vul nu ook de kolom 'Rapport' in de offerte, zodat het meerwerk rapport wordt gebruikt bij het mailen/printen
        Guid originalReport = new Guid("4f62734f-5159-4410-b2f7-bf6fdea61e73");
        Guid substituteReport = new Guid("a6a53726-ee96-4771-8a7b-60a85327e6a6");

        var rsReportSubstitute = _script.GetRecordset("R_REPORTSUBSTITUTE", "PK_R_REPORTSUBSTITUTE",
            $"FK_ORIGINALREPORT = '{originalReport}' AND FK_USERSUBSTITUTE = '{substituteReport}'", "");

        if(rsReportSubstitute.RecordCount == 0)
        {
            return true;
        }

        var reportSubstitute = rsReportSubstitute.DataTable.AsEnumerable().FirstOrDefault().Field<int>("PK_R_REPORTSUBSTITUTE");

        var rsOffer = _script.GetRecordset("R_OFFER", "FK_REPORT",
            $"PK_R_OFFER = {data["FK_OFFER"]}", "");
        rsOffer.MoveFirst();
        rsOffer.UseDataChanges = false;
        rsOffer.UpdateWhenMoveRecord = false;

        rsOffer.SetFieldValue("FK_REPORT", reportSubstitute);

        var updateResult2 = rsOffer.Update2();

        if (updateResult2.Any(x => x.HasError))
        {
            reason = $"Het aanpassen van het offertetemplate naar het meerwerk template is mislukt, oorzaak: {updateResult2.First(x => x.HasError).GetResult()}";
            return false;
        }

        return true;
    }

    private bool FillTextFields(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if ((data["FK_PRODUCTGROUP"] as int? ?? 0) == 0)
        {
            return true;//productgroep niet gevuld
        }

        if (saveType == SaveType.AfterUpdateSave && oldData["FK_PRODUCTGROUP"].ToString().Equals(data["FK_PRODUCTGROUP"].ToString()))
        {
            return true; //Alleen doorgaan bij aanmaken of wijzigen productgroep
        }

        var rsOffer = _script.GetRecordset("R_OFFER",
            "HEADERMEMO, FOOTERMEMO, NIETINVOORSTELMEEGENOMEN, MEMOALGEMENEVOORWAARDEN",
            $"PK_R_OFFER = {data["FK_OFFER"]}", "");
        rsOffer.MoveFirst();

        var productGroups = _script.GetRecordset("R_OFFERDETAILMISC", "FK_PRODUCTGROUP",
            $"FK_OFFER = {data["FK_OFFER"]} AND FK_PRODUCTGROUP IS NOT NULL", "").DataTable.AsEnumerable().Select(x => x.Field<int>("FK_PRODUCTGROUP")).ToList().Distinct();

        if (!productGroups.Any())
        {
            return true;
        }

        string headertext = "";
        string generaltext = "";
        string nietmeegenomentext = "";
        string meegenomentext = "";

        foreach (var productgroup in productGroups)
        {
            var productgroupTexts = _script.GetRecordset("U_PRODUCTGROUP", "FK_HEADER, FK_GENERAL, FK_NIETINONSVOORSTELMEEGENOMEN, FK_INONSVOORSTELMEEGENOMEN",
                $"PK_U_PRODUCTGROUP = {productgroup}", "").DataTable.AsEnumerable();

            var headerId = productgroupTexts.First().Field<int?>("FK_HEADER");
            var generalId = productgroupTexts.First().Field<int?>("FK_GENERAL");
            var nietmeegenomenId = productgroupTexts.First().Field<int?>("FK_NIETINONSVOORSTELMEEGENOMEN");
            var meegenomenId = productgroupTexts.First().Field<int?>("FK_INONSVOORSTELMEEGENOMEN");

            if ((headerId as int? ?? 0) != 0)
            {
                var rsTekstblok = _script.GetRecordset("R_TEXTSNIPPET", "SNIPPET",
                    $"PK_R_TEXTSNIPPET = {headerId}", "");
                if (rsTekstblok.RecordCount > 0)
                {
                    rsTekstblok.MoveFirst();
                    var header = (string)rsTekstblok.Fields["SNIPPET"].Value;

                    headertext = _script.RtfOperations.MergeRtf(
                        headertext,
                        header);
                }
            }

            if ((generalId as int? ?? 0) != 0)
            {
                var rsTekstblok = _script.GetRecordset("R_TEXTSNIPPET", "SNIPPET",
                    $"PK_R_TEXTSNIPPET = {generalId}", "");
                if (rsTekstblok.RecordCount > 0)
                {
                    rsTekstblok.MoveFirst();
                    var general = (string)rsTekstblok.Fields["SNIPPET"].Value;

                    generaltext = _script.RtfOperations.MergeRtf(
                        generaltext,
                        general);
                }
            }

            if ((nietmeegenomenId as int? ?? 0) != 0)
            {
                var rsTekstblok = _script.GetRecordset("R_TEXTSNIPPET", "SNIPPET",
                    $"PK_R_TEXTSNIPPET = {nietmeegenomenId}", "");
                if (rsTekstblok.RecordCount > 0)
                {
                    rsTekstblok.MoveFirst();
                    var nietmeegenomen = (string)rsTekstblok.Fields["SNIPPET"].Value;

                    nietmeegenomentext = _script.RtfOperations.MergeRtf(
                        nietmeegenomentext,
                        nietmeegenomen);
                }
            }

            if ((meegenomenId as int? ?? 0) != 0)
            {
                var rsTekstblok = _script.GetRecordset("R_TEXTSNIPPET", "SNIPPET",
                    $"PK_R_TEXTSNIPPET = {meegenomenId}", "");
                if (rsTekstblok.RecordCount > 0)
                {
                    rsTekstblok.MoveFirst();
                    var meegenomen = (string)rsTekstblok.Fields["SNIPPET"].Value;

                    meegenomentext = _script.RtfOperations.MergeRtf(
                        meegenomentext,
                        meegenomen);
                }
            }

        }

        rsOffer.Fields["HEADERMEMO"].Value = headertext;
        rsOffer.Fields["MEMOALGEMENEVOORWAARDEN"].Value = generaltext;
        rsOffer.Fields["NIETINVOORSTELMEEGENOMEN"].Value = nietmeegenomentext;
        rsOffer.Fields["FOOTERMEMO"].Value = meegenomentext;

        rsOffer.Update();

        return true;
    }
    class Format
    {
        public int SequenceNumber { get; set; }
        public string Description { get; set; }
        public double Percentage { get; set; }
        public bool Projectteam { get; set; }
        public bool NoCostpriceNeeded { get; set; }
    }
}
