﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Communication.Script;

public class R_ORDERDOCUMENT_Delete_User : IDeleteScript
{
    private ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeDelete(RowData data, ref string reason)
    {
        //DB
        //28-9-2023
        //Indien er gekoppelde bewerkingen zijn tbv Appreo, markeer deze bonregels bewerking dan als verwijderd
        if (!CreateDeletedDocumentsRecords(data, ref reason))
        {
            return false;
        }

        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterDelete(RowData data, ref string reason)
    {

        return true;
    }

    private bool CreateDeletedDocumentsRecords(RowData data, ref string reason)
    {
        var linkedWorkactivities = _script.GetRecordset("U_WORKACTIVITIESPERORDERDOCUMENT", "FK_WORKACTIVITY",
            $"FK_ORDERDOCUMENT = {data["PK_R_ORDERDOCUMENT"]}", "").DataTable.AsEnumerable().Select(x => x.Field<int>("FK_WORKACTIVITY")).ToList();

        if(!linkedWorkactivities.Any())
        {
            return true;
        }

        var joborderDetailWorkactivities = _script.GetRecordset("R_JOBORDERDETAILWORKACTIVITY", "PK_R_JOBORDERDETAILWORKACTIVITY",
            $"FK_ORDER = {data["FK_ORDER"]} AND FK_WORKACTIVITY IN ({string.Join(",", linkedWorkactivities)})", "")
            .DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_JOBORDERDETAILWORKACTIVITY")).ToList();

        if (!joborderDetailWorkactivities.Any())
        {
            return true;
        }

        var rs = _script.GetRecordset("U_DELETEDDOCUMENTS", "", $"PK_U_DELETEDDOCUMENTS = NULL", "");

        rs.UpdateWhenMoveRecord = false;

        foreach (var joborderDetailWorkactivityId in joborderDetailWorkactivities)
        {
            rs.AddNew();
            rs.SetFieldValue("DOCUMENTID", data["FK_DOCUMENT"]);
            rs.SetFieldValue("TABLENAME", "R_JOBORDERDETAILWORKACTIVITY");
            rs.SetFieldValue("RECORDID", joborderDetailWorkactivityId);
        }

        rs.MoveFirst();
        rs.Update();

        return true;
    }
}
