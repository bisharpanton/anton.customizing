﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;

public class U_ADDITIONALWORKMARKUP_Delete_User : IDeleteScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeDelete(RowData data, ref string reason)
    {
        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterDelete(RowData data, ref string reason)
    {
        //SP
        //11-8-2022
        //Update bij een wijziging het netto verkoopbedrag van de offerteregel
        if (!UpdateNetsalespriceOfferdetail(data, ref reason))
        {
            return false;
        }
        return true;
    }
    private bool UpdateNetsalespriceOfferdetail(RowData data, ref string reason)
    {
        var projectModuleActive = _script.GetRecordset("R_CRMSETTINGS", "PROJECTMODULEACTIVE",
            "", "").DataTable.AsEnumerable().First().Field<bool>("PROJECTMODULEACTIVE");

        if (!projectModuleActive)
        {
            return true;//Alleen van toepassing indien vinkje 'Projectenmodule actief' aanstaat
        }

        var additionalWorkMarkups = _script.GetRecordset("U_ADDITIONALWORKMARKUP", "SEQUENCENUMBER, DESCRIPTION, PERCENTAGE, PROJECTTEAM",
               $"FK_OFFERDETAILMISC = {data["FK_OFFERDETAILMISC"]}", "SEQUENCENUMBER ASC")
           .DataTable.AsEnumerable().Select(x => new Markup()
           {
               SequenceNumber = x.Field<int>("SEQUENCENUMBER"),
               Description = x.Field<string>("DESCRIPTION"),
               Percentage = x.Field<double>("PERCENTAGE"),
               Projectteam = x.Field<bool>("PROJECTTEAM")

           }).ToList();

        var mainMarkups = additionalWorkMarkups.Where(x => x.Projectteam == false);
        var projectTeamMarkups = additionalWorkMarkups.Where(x => x.Projectteam == true);

        var newTotalList = new List<TotalList>();

        foreach (var mainMarkup in mainMarkups)
        {
            newTotalList.Add(new TotalList
            {
                SequenceNumber = mainMarkup.SequenceNumber,
                Description = mainMarkup.Description,
                Percentage = mainMarkup.Percentage

            });
            ;
        }

        if (projectTeamMarkups.Any())
        {
            newTotalList.Add(new TotalList
            {
                SequenceNumber = projectTeamMarkups.First().SequenceNumber,
                Description = "Projectteam",
                Percentage = projectTeamMarkups.Sum(x => x.Percentage)

            });
        }

        var sortedList = newTotalList.OrderBy(x => x.SequenceNumber);

        var subTotal = _script.GetRecordset("U_ADDITIONALWORKDETAILS", "NETSALESPRICE",
            $"FK_OFFERDETAILMISC = {data["FK_OFFERDETAILMISC"]}", "").DataTable.AsEnumerable().Sum(x => x.Field<double>("NETSALESPRICE"));

        var endTotal = subTotal;

        foreach (TotalList markup in sortedList)
        {
            var amountToAdd = endTotal * markup.Percentage;
            endTotal += amountToAdd;
        }

        var rsOfferdetailMisc = _script.GetRecordset("R_OFFERDETAILMISC", "NETSALESAMOUNT",
            $"PK_R_OFFERDETAILMISC = {data["FK_OFFERDETAILMISC"]}", "");
        rsOfferdetailMisc.MoveFirst();
        rsOfferdetailMisc.SetFieldValue("NETSALESAMOUNT", endTotal);

        var updateResult = rsOfferdetailMisc.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            reason = $"Doorvoeren prijs naar offerteregel mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}";
            return false;
        }

        return true;
    }
    class Markup
    {
        public int SequenceNumber { get; set; }
        public string Description { get; set; }
        public double Percentage { get; set; }
        public bool Projectteam { get; set; }
    }

    public class TotalList
    {
        public int SequenceNumber { get; set; }
        public string Description { get; set; }
        public double Percentage { get; set; }
        public bool Projectteam { get; set; }
    }
}
