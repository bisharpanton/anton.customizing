﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Common.ItemManagement;
using Ridder.Communication.Script;
using Ridder.Common.ProductionManagement;
using System.Diagnostics;

public class R_JOBORDER_Save_User : ISaveScript
{
    private ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //SP
        //31-10-2024
        //Bij aanmaken nieuwe bon bij regie/verkoop wordt het memo veld gevuld met template tekstblok.
        //Dit is ingericht voor De Leeuw

        if(_script.GetUserInfo().DatabaseName == "De Leeuw Protection Systems")
        {
            if (!FillMemoWithTemplateSnippetDeLeeuw(data, oldData, saveType, ref reason))
            {
                return false;
            }
        }

        //HVE
        //22-11-2018
        //Bij aanmaken nieuwe bon wordt het memo veld gevuld met std_tekstblok.
        if (!FillDefaultMemoWithSnippet(data, oldData, saveType, ref reason))
        {
            return false;
        }

        // HVE
        // 4-8-2020
        // Omschrijving bepalen igv service-interval of service object
        if (!SetDescriptionIfService(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //DB
        //4-2-2015
        //Bij aanmaken bon moet kostprijs vd verkoopregel gekopieerd worden naar de kolom 'Budget'.
        if (!CopyCostpriceFromSalesorderDetail(data, oldData, saveType, ref reason))
        {
            return false;
        }
        
        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //14-4-2015
        //Bij aanmaken van regie-order automatisch bewerkingen toevoegen
        if (!AutomaticCreateWorkactivitiesAtTimeAndMaterialJoborder(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //DB
        //25-5-2021
        //Bij servicebonnen, kopieer omschrijving en order referentie naar de order
        if(!CopyServiceFieldsToServiceOrder(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //DB
        //25-5-2021
        //Kopieer object locatie naar werkadres order bij servicebonnen
        if(!CopyObjectLocationToDestinationAddressOrder(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //DB
        //5-10-2023
        //Genereer voorrrijkosten bij storingen bij De Leeuw
        if(!GenerateVoorrijKosten(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //DB
        //2-11-2023
        //Aanmaken standaard artikelen per service object
        if (!CreateDefaultItems(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //RK
        //20-12-2023
        //Invullen bewerking bonregel bij aanmaken servicebon aan de hand van serviceinterval
        if (!InvullenBonRegelMetStandaardBewerkingUren(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    private bool FillMemoWithTemplateSnippetDeLeeuw(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true;
        }

        var orderType = _script.GetRecordset("R_ORDER", "ORDERTYPE",
            $"PK_R_ORDER = {data["FK_ORDER"]}", "").DataTable.AsEnumerable().First().Field<int>("ORDERTYPE");

        if(orderType == 7 || orderType == 6)
        {
            return true;//Niet van toepassing bij Service en Intern
        }


        if ((string)data["DESCRIPTION"] == "Werkvoorbereiding" || (string)data["DESCRIPTION"] == "Inkopen"
            || (string)data["DESCRIPTION"] == "Productie" || (string)data["DESCRIPTION"] == "Logistiek")
        {
            return true;//Alleen bij bon Uitvoering of handmatig aangemaakte bonnen
        }

        var rsTekstblok = _script.GetRecordset("R_TEXTSNIPPET", "SNIPPET", "DESCRIPTION = 'Template Werkbon Uitvoering'", "");

        if (rsTekstblok.RecordCount == 0)
        {
            return true;
        }

        rsTekstblok.MoveFirst();
        data["MEMO"] = (string)rsTekstblok.Fields["SNIPPET"].Value;

        return true;
    }

    private bool CreateDefaultItems(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true; //Bij aanmaken van toepassing
        }

        if ((PlanSetting)data["PLANSETTING"] != PlanSetting.Service)
        {
            return true;
        }

        if (!(data["FK_SERVICEOBJECT"] as int?).HasValue)
        {
            return true;
        }

        var defaultItems = _script.GetRecordset("U_SERVICEOBJECTDEFAULTITEMS", "", 
            $"FK_SERVICEOBJECT = {data["FK_SERVICEOBJECT"]} AND FK_ITEM IS NOT NULL AND QUANTITY > 0.0", "")
            .DataTable.AsEnumerable().ToList();

        if(!defaultItems.Any())
        {
            return true;
        }

        var rsJoborderDetailItem = _script.GetRecordset("R_JOBORDERDETAILITEM", "", "PK_R_JOBORDERDETAILITEM = NULL", "");
        rsJoborderDetailItem.UseDataChanges = true;
        rsJoborderDetailItem.UpdateWhenMoveRecord = false;

        foreach (var defaultItem in defaultItems)
        {
            rsJoborderDetailItem.AddNew();
            rsJoborderDetailItem.SetFieldValue("FK_ORDER", data["FK_ORDER"]);
            rsJoborderDetailItem.SetFieldValue("FK_JOBORDER", data["PK_R_JOBORDER"]);
            rsJoborderDetailItem.SetFieldValue("FK_ITEM", defaultItem.Field<int>("FK_ITEM"));
            rsJoborderDetailItem.SetFieldValue("QUANTITY", defaultItem.Field<double>("QUANTITY"));
        }

        var updateResult = rsJoborderDetailItem.Update2();

        if (updateResult != null && updateResult.Any(x => x.HasError))
        {
            reason = $"Het aanmaken van de bonregels artikel obv service object standaard artikelen is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}";
            return false;
        }

        return true;
    }

    private bool GenerateVoorrijKosten(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if(saveType == SaveType.AfterUpdateSave)
        {
            return true; //Bij aanmaken van toepassing
        }

        if ((PlanSetting)data["PLANSETTING"] != PlanSetting.Service)
        {
            return true;
        }

        var serviceTypeCode = _script.GetRecordset("R_SERVICETYPE", "CODE", $"PK_R_SERVICETYPE = {data["FK_SERVICETYPE"]}", "")
            .DataTable.AsEnumerable().First().Field<string>("CODE");

        if (!serviceTypeCode.Equals("STORING"))
        {
            return true; //Alleen van toepassing bij storingen, voor als de bon vanuit een object wordt aangemaakt
        }

        var abbreviationCompany = _script.GetRecordset("R_CRMSETTINGS", "ABBREVIATIONCOMPANY", "", "")
            .DataTable.AsEnumerable().First().Field<string>("ABBREVIATIONCOMPANY");

        if(!abbreviationCompany.Equals("LPS"))
        {
            return true;
        }

        var rsOhwDetailItem = _script.GetRecordset("R_ORDERWIPDETAILITEM", "", "PK_R_ORDERWIPDETAILITEM IS NULL", "");
        rsOhwDetailItem.UseDataChanges = true;
        rsOhwDetailItem.UpdateWhenMoveRecord = false;

        rsOhwDetailItem.AddNew();
        rsOhwDetailItem.SetFieldValue("FK_ORDER", data["FK_ORDER"]);
        rsOhwDetailItem.SetFieldValue("FK_JOBORDER", data["PK_R_JOBORDER"]);
        rsOhwDetailItem.SetFieldValue("FK_ITEM", 16361); //Artikel 00001 - Voorrijkosten
        rsOhwDetailItem.SetFieldValue("QUANTITY", 1.0);
        rsOhwDetailItem.SetFieldValue("COSTPRICE", 0.0);
        
        rsOhwDetailItem.Update();

        return true;
    }

    private bool CopyObjectLocationToDestinationAddressOrder(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true;
        }

        if ((PlanSetting)data["PLANSETTING"] != PlanSetting.Service)
        {
            return true;
        }

        if (!(data["FK_SERVICEOBJECT"] as int?).HasValue)
        {
            return true;
        }


        var objectLocationId = _script.GetRecordset("R_SERVICEOBJECT", "FK_OBJECTLOCATION",
            $"PK_R_SERVICEOBJECT = {data["FK_SERVICEOBJECT"]}", "").DataTable.AsEnumerable().First().Field<int?>("FK_OBJECTLOCATION") ?? 0;

        if (objectLocationId == 0)
        {
            return true;
        }

        var rsOrder = _script.GetRecordset("R_ORDER", "FK_DESTINATIONADDRESS",
            $"PK_R_ORDER = {data["FK_ORDER"]}", "");
        rsOrder.MoveFirst();
        rsOrder.SetFieldValue("FK_DESTINATIONADDRESS", objectLocationId);
        rsOrder.Update();

        return true;
    }

    private bool CopyServiceFieldsToServiceOrder(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if(saveType == SaveType.AfterInsertSave)
        {
            return true;
        }

        if((PlanSetting)data["PLANSETTING"] != PlanSetting.Service)
        {
            return true;
        }

        var rsJoborder = _script.GetRecordset("R_JOBORDER", "PK_R_JOBORDER",
            $"FK_ORDER = {data["FK_ORDER"]}", "");

        if(rsJoborder.RecordCount > 1)
        {
            return true;//Alleen doorgaan als dit de eerste bon is
        }

        if(oldData["DESCRIPTION"].ToString().Equals("DESCRIPTION") && oldData["REFERENCE"].ToString().Equals("REFERENCE"))
        {
            return true; //Service bon velden worden niet gewijzigd
        }

        var rsOrder = _script.GetRecordset("R_ORDER", "DESCRIPTION, REFERENCE",
            $"PK_R_ORDER = {data["FK_ORDER"]}", "");
        rsOrder.MoveFirst();
        rsOrder.SetFieldValue("DESCRIPTION", data["DESCRIPTION"]);
        rsOrder.SetFieldValue("REFERENCE", data["REFERENCE"]);
        rsOrder.Update();

        return true;
    }

    private bool SetDescriptionIfService(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true;
        }

        var planSettingService = 2;
        var planSetting = (int)data["PLANSETTING"];
        if (planSetting != planSettingService)
        {
            return true;
        }

        var serviceobjectId = data["FK_SERVICEOBJECT"] as int? ?? 0;
        var serviceInterval = data["FK_SERVICEINTERVAL"] as int? ?? 0;

        if (serviceInterval == 0 && serviceobjectId == 0)
        {
            return true;
        }
        else if (serviceInterval > 0 && serviceobjectId > 0)
        {
            data["DESCRIPTION"] = GetIntervalDescription();
        }
        else if (serviceInterval == 0 && serviceobjectId > 0)
        {
            data["DESCRIPTION"] = GetServiceObjectDescription(serviceobjectId);
        }

        return true;
    }

    private string GetServiceObjectDescription(int serviceobjectId)
    {
        return _script.GetRecordset("R_SERVICEOBJECT", "DESCRIPTION", $"PK_R_SERVICEOBJECT = {serviceobjectId}", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<string>("DESCRIPTION"))
            .First();
    }

    private string GetIntervalDescription()
    {
        return _script.GetRecordset("R_CRMSETTINGS", "DESCRIPTIONSERVICEINTERVAL", "", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<string>("DESCRIPTIONSERVICEINTERVAL"))
            .First();
    }

    private bool CopyCostpriceFromSalesorderDetail(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true;
        }

        if (!(data["FK_ORDERBASE"] as int?).HasValue || !(data["FK_ASSEMBLY"] as int?).HasValue)
        {
            return true;
        }

        //Haal basisregel op
        var rsOrderBase = _script.GetRecordset("R_ORDERBASE", "FK_SALESORDERDETAILASSEMBLYCUSTOM",
            $"PK_R_ORDERBASE = {data["FK_ORDERBASE"]}", "");
        rsOrderBase.MoveFirst();

        if (!(rsOrderBase.Fields["FK_SALESORDERDETAILASSEMBLYCUSTOM"].Value as int?).HasValue)
        {
            return true;
        }

        var rsSalesorderDetailAssembly = _script.GetRecordset("R_SALESORDERDETAILASSEMBLY",
            "QUANTITY, COSTPRICE, FK_OFFERDETAILASSEMBLY",
            $"PK_R_SALESORDERDETAILASSEMBLY = {rsOrderBase.Fields["FK_SALESORDERDETAILASSEMBLYCUSTOM"].Value}", "");
        rsSalesorderDetailAssembly.MoveFirst();

        var calculationId = GetCalculation((int)data["FK_ASSEMBLY"]);

        var costprice = 0.0;

        if (calculationId != 0)
        {
            costprice = GetCostpriceThisAssemblyFromCalculation(calculationId);
        }
        else if ((int)data["LEVEL"] == 1)
        {
            costprice = (double)rsSalesorderDetailAssembly.Fields["COSTPRICE"].Value;
        }

        //Bij Loos wordt één verkoopregel stuklijst in delen omgezet naar basisregels.
        //In dat geval berekenen we hier de kostprijs voor het aantal dat omgezet is naar productie
        var costpriceSubquantity =
            (Math.Abs((double)data["QUANTITYPLANNED"] - (double)rsSalesorderDetailAssembly.Fields["QUANTITY"].Value) > 0.1)
                ? (costprice / (double)rsSalesorderDetailAssembly.Fields["QUANTITY"].Value) *
                  (double)data["QUANTITYPLANNED"]
                : costprice;

        data["BUDGET"] = costpriceSubquantity;
        data["OORSPRONKELIJKBUDGET"] = costpriceSubquantity;

        return true;
    }

    private double GetCostpriceThisAssemblyFromCalculation(int calculationId)
    {
        return _script.GetRecordset("C_CALCULATIONRULE",
                "PRICETOTAL",
                $"FK_CALCULATION = {calculationId} AND FK_SUBCALCULATION IS NULL", "")
            .DataTable.AsEnumerable().Sum(x => x.Field<double>("PRICETOTAL"));
    }

    private int GetCalculation(int assemblyId)
    {
        return _script.GetRecordset("C_CALCULATION", "PK_C_CALCULATION",
                $"FK_ASSEMBLY = {assemblyId}", "").DataTable.AsEnumerable().FirstOrDefault()
            ?.Field<int>("PK_C_CALCULATION") ?? 0;
    }

    private bool AutomaticCreateWorkactivitiesAtTimeAndMaterialJoborder(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if(saveType == SaveType.AfterUpdateSave)
        {
            return true;
        }

        var rsCRM = _script.GetRecordset("R_CRMSETTINGS",
            "CREATEMANUFACTUREWORKACTIVITIES, CREATEMANUFACTUREWORKACTIVITIESDESCRIPTION, CREATEDRAWINGSWORKACTIVITIES, CREATEDRAWINGSWORKACTIVITIESDESCRIPTION, CREATEPREPARATIONWORKACTIVITIES, CREATEPREPARATIONWORKACTIVITIESDESCRIPTION",
            "", "");
        rsCRM.MoveFirst();

        //SP - 25-4-2024
        //Onderstaande is vervangen door CreateJoborders in de R_ORDER_Save_User
        //Check 'Fabricage'
        /*if ((AutomatischAanmakenBewerkingen)rsCRM.Fields["CREATEMANUFACTUREWORKACTIVITIES"].Value ==
            AutomatischAanmakenBewerkingen.Altijd ||
            ((AutomatischAanmakenBewerkingen)rsCRM.Fields["CREATEMANUFACTUREWORKACTIVITIES"].Value ==
             AutomatischAanmakenBewerkingen.BijSpecifiekeBonOmschrijving
             && data["DESCRIPTION"].ToString() != ""
             && data["DESCRIPTION"].ToString()
                 .Equals(rsCRM.Fields["CREATEMANUFACTUREWORKACTIVITIESDESCRIPTION"].Value.ToString())
             && (bool)data["TIMEANDMATERIAL"]))
        {
            if (!CreateWorkactivities(data, oldData, saveType, ref reason, "AUTOMATISCHAANMAKENBIJREGIE", false))
            {
                return false;
            }
        }

        //Check 'Tekenwerk'
        if ((AutomatischAanmakenBewerkingen)rsCRM.Fields["CREATEDRAWINGSWORKACTIVITIES"].Value ==
            AutomatischAanmakenBewerkingen.Altijd ||
            ((AutomatischAanmakenBewerkingen)rsCRM.Fields["CREATEDRAWINGSWORKACTIVITIES"].Value ==
             AutomatischAanmakenBewerkingen.BijSpecifiekeBonOmschrijving
             && data["DESCRIPTION"].ToString() != ""
             && data["DESCRIPTION"].ToString()
                 .Equals(rsCRM.Fields["CREATEDRAWINGSWORKACTIVITIESDESCRIPTION"].Value.ToString())
             && (bool)data["TIMEANDMATERIAL"]))
        {
            if (!CreateWorkactivities(data, oldData, saveType, ref reason, "AUTOMATISCHAANMAKENBIJREGIE_TEKENWERK", false))
            {
                return false;
            }
        }

        //Check 'WVB'
        if ((AutomatischAanmakenBewerkingen)rsCRM.Fields["CREATEPREPARATIONWORKACTIVITIES"].Value ==
            AutomatischAanmakenBewerkingen.Altijd ||
            ((AutomatischAanmakenBewerkingen)rsCRM.Fields["CREATEPREPARATIONWORKACTIVITIES"].Value ==
             AutomatischAanmakenBewerkingen.BijSpecifiekeBonOmschrijving
             && data["DESCRIPTION"].ToString() != ""
             && data["DESCRIPTION"].ToString()
                 .Equals(rsCRM.Fields["CREATEPREPARATIONWORKACTIVITIESDESCRIPTION"].Value.ToString())))
        {
            if (!CreateWorkactivities(data, oldData, saveType, ref reason, "PREPARATIONWORKACTIVITY", false))
            {
                return false;
            }
        }*/

        //Check 'Service'
        if ((PlanSetting)data["PLANSETTING"] == PlanSetting.Service)
        {
            if (!CreateWorkactivities(data, oldData, saveType, ref reason, "CREATEAUTOMATICATSERVICEJOBORDER", true))
            {
                return false;
            }
        }

        return true;
    }

    private bool CreateWorkactivities(RowData data, RowData oldData, SaveType saveType, ref string reason, string columnNameWorkactivity, bool isService)
    {
        var rsWorkactivitiesToCreate = _script.GetRecordset("R_WORKACTIVITY", "PK_R_WORKACTIVITY, DEFAULTTIME, DEFAULTTIMEATSERVICE", 
            $"{columnNameWorkactivity} = 1", "");

        if (rsWorkactivitiesToCreate.RecordCount == 0)
        {
            return true;
        }
        
        var rsJoborderDetailWorkactivity =
            _script.GetRecordset("R_JOBORDERDETAILWORKACTIVITY", "", "PK_R_JOBORDERDETAILWORKACTIVITY = 1", "");
        rsJoborderDetailWorkactivity.UpdateWhenMoveRecord = false;
        rsJoborderDetailWorkactivity.UseDataChanges = true;
        rsJoborderDetailWorkactivity.MoveFirst();

        rsWorkactivitiesToCreate.MoveFirst();
        while (!rsWorkactivitiesToCreate.EOF)
        {
            rsJoborderDetailWorkactivity.AddNew();
            rsJoborderDetailWorkactivity.Fields["FK_ORDER"].Value = data["FK_ORDER"];
            rsJoborderDetailWorkactivity.Fields["FK_JOBORDER"].Value = data["PK_R_JOBORDER"];
            rsJoborderDetailWorkactivity.Fields["FK_WORKACTIVITY"].Value = rsWorkactivitiesToCreate.Fields["PK_R_WORKACTIVITY"].Value;
            rsJoborderDetailWorkactivity.Fields["OPERATIONTIME"].Value = isService
                ? rsWorkactivitiesToCreate.Fields["DEFAULTTIMEATSERVICE"].Value as long? ?? 0
                : rsWorkactivitiesToCreate.Fields["DEFAULTTIME"].Value as long? ?? 0;

            rsWorkactivitiesToCreate.MoveNext();
        }

        rsJoborderDetailWorkactivity.MoveFirst();
        var updateResult = rsJoborderDetailWorkactivity.Update2();

        if (updateResult != null && updateResult.Any(x => x.HasError))
        {
            reason = $"Het aanmaken van de bewerkingen is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}";
            return false;
        }

        return true;
    }

    private bool FillDefaultMemoWithSnippet(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true;
        }

        var rsTekstblok = _script.GetRecordset("R_TEXTSNIPPET", "SNIPPET", "DESCRIPTION = 'Werkbon_std_memoveld'", "");

        if (rsTekstblok.RecordCount == 0)
        {
            return true;
        }

        rsTekstblok.MoveFirst();
        data["MEMO"] = (string)rsTekstblok.Fields["SNIPPET"].Value;

        return true;
    }

    private bool InvullenBonRegelMetStandaardBewerkingUren(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if(saveType == SaveType.AfterUpdateSave)
            return true;

        if (!(data["PK_R_JOBORDER"] as int?).HasValue)
            return true;

        if ((PlanSetting)data["PLANSETTING"] != PlanSetting.Service)
            return true;

        if (!(data["FK_SERVICEINTERVAL"] as int?).HasValue)
            return true;

        var abbreviationCompany = _script.GetRecordset("R_CRMSETTINGS", "ABBREVIATIONCOMPANY", "", "")
            .DataTable.AsEnumerable().First().Field<string>("ABBREVIATIONCOMPANY");

        if (!abbreviationCompany.Equals("LPS"))
            return true;

        var serviceIntervalId = (int)data["FK_SERVICEINTERVAL"];
        var serviceInterval = _script.GetRecordset("R_SERVICEINTERVAL", "PK_R_SERVICEINTERVAL, EXPECTEDHOURS", $"PK_R_SERVICEINTERVAL = {serviceIntervalId}", "").DataTable.AsEnumerable().First();

        if((serviceInterval.Field<long?>("EXPECTEDHOURS") ?? 0) == 0)
            return true;

        var rsJobOrderDetailWorkActivity = _script.GetRecordset("R_JOBORDERDETAILWORKACTIVITY", "", "PK_R_JOBORDERDETAILWORKACTIVITY = 0", "");
        rsJobOrderDetailWorkActivity.UseDataChanges = true;

        rsJobOrderDetailWorkActivity.AddNew();
        rsJobOrderDetailWorkActivity.SetFieldValue("FK_ORDER", data["FK_ORDER"]);
        rsJobOrderDetailWorkActivity.SetFieldValue("FK_JOBORDER", data["PK_R_JOBORDER"]);
        rsJobOrderDetailWorkActivity.SetFieldValue("FK_WORKACTIVITY", 145); // 145 = Montage op locatie
        rsJobOrderDetailWorkActivity.SetFieldValue("OPERATIONTIME", serviceInterval.Field<long>("EXPECTEDHOURS"));

        var updateResult = rsJobOrderDetailWorkActivity.Update2();

        if (updateResult != null && updateResult.Any(x => x.HasError))
        {
            reason = $"Het aanmaken van de bewerkingen is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}";
            return false;
        }

        return true;
    }

    private enum AutomatischAanmakenBewerkingen : int
    {
        Nooit = 1,
        Altijd = 2,
        BijSpecifiekeBonOmschrijving = 3
    }
}
