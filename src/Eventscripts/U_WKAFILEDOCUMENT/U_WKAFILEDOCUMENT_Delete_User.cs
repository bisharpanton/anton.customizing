﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;

public class U_WKAFILEDOCUMENT_Delete_User : IDeleteScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeDelete(RowData data, ref string reason)
    {
        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterDelete(RowData data, ref string reason)
    {
        //DB
        //22-6-2023
        //Check of WKA dossier compleet is

        var wkaTypeId = _script.GetRecordset("U_WKAFILE", "FK_WKATYPE",
                $"PK_U_WKAFILE = {(int)data["FK_WKAFILE"]}", "").DataTable.AsEnumerable().First().Field<int>("FK_WKATYPE");

        var wkaFileComplete = CheckIfWkaFileIsComplete(wkaTypeId, (int)data["FK_WKAFILE"]);

        UpdateBoolAtWkaEmployee((int)data["FK_WKAFILE"], wkaFileComplete);

        return true;
    }

    private void UpdateBoolAtWkaEmployee(int wkaFileId, bool wkaFileComplete)
    {
        var rsWkaEmployee = _script.GetRecordset("U_WKAFILE", "WKAFILECOMPLETE", $"PK_U_WKAFILE = {wkaFileId}", "");
        rsWkaEmployee.MoveFirst();
        rsWkaEmployee.SetFieldValue("WKAFILECOMPLETE", wkaFileComplete);
        rsWkaEmployee.Update();
    }

    private bool CheckIfWkaFileIsComplete(int wkaTypeId, int wkaFileId)
    {
        var requiredDocumentTypeIds = _script.GetRecordset("U_WKAREQUIREDDOCUMENTTYPESPERWKATYPE", "FK_WKADOCUMENTTYPE",
            $"FK_WKATYPE = {wkaTypeId}", "").DataTable.AsEnumerable().Select(x => x.Field<int>("FK_WKADOCUMENTTYPE")).ToList();

        var wkaEmployeeDocuments = _script.GetRecordset("U_WKAFILEDOCUMENT", "FK_WKADOCUMENTTYPE, EXPIRATIONDATE",
            $"FK_WKAFILE = {wkaFileId} AND FK_WKADOCUMENTTYPE IS NOT NULL AND EXPIRATIONDATE IS NOT NULL", "").DataTable.AsEnumerable().ToList();

        foreach (var documentId in requiredDocumentTypeIds)
        {
            var employeeDocumentsThisDocumentId = wkaEmployeeDocuments.Where(x => x.Field<int>("FK_WKADOCUMENTTYPE") == documentId).ToList();

            if (!employeeDocumentsThisDocumentId.Any())
            {
                return false;
            }

            if (!employeeDocumentsThisDocumentId.Any(x => x.Field<DateTime?>("EXPIRATIONDATE").HasValue && x.Field<DateTime>("EXPIRATIONDATE").Date >= DateTime.Now.Date))
            {
                return false;
            }
        }

        return true;
    }
}
