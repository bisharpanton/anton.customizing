﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using System.Globalization;

public class R_PROJECTTIME_Save_User : ISaveScript
{
    private ScriptHelper _script = new ScriptHelper();
    private bool _CreateNewProjectTimeInAfterSave = false;
    private long _TimeEmployeeNewProjectTime = 0;
    private int _SequenceOverTime = 1;

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //28-10-2015
        //Anton wil af en toe urenboekingen maken van meer dan 8 uur. De extra overuur-boeking krijgt dan overuursoort 'Normaal'. Standaard in iQ
        //moet men twee boekingen maken. Hieronder een script wat de urenboeking automatisch opsplitst indien er meer geboekt wordt dan 'nog te boeken vandaag'.
        if (!SplitProjectTime(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //Check of discontinuiteit van toepassing is
        var crmSettings = GetCrmSettings();
        var discontinuityApplicableInCRM = crmSettings.ThresholdValueDiscontinuity != null
            && crmSettings.ThresholdValueDiscontinuity.Value > 0
            && Math.Abs(crmSettings.PercentageTakeDiscontinuity) > 0.01
            && Math.Abs(crmSettings.PercentageBuildUpDiscontinuity) > 0.01;

        //Alleen controleren op werknemer indien CRM correct is ingevuld
        var discontinuityApplicableAtEmployee = (discontinuityApplicableInCRM)
            && _script.GetRecordset("R_EMPLOYEE", "DISCONTINUITYAPPLICABLE", $"PK_R_EMPLOYEE = {data["FK_EMPLOYEE"]}", "")
                        .DataTable.AsEnumerable().First().Field<bool>("DISCONTINUITYAPPLICABLE");

        //DB
        //1-8-2019
        //Bereken of er discontinuïteit van toepassing is.
        //In dat geval vullen we een andere overuursoort in
        if (discontinuityApplicableInCRM && discontinuityApplicableAtEmployee && !CalculateDiscontiuityBuildUp(data, oldData, saveType, ref reason, crmSettings))
        {
            return false;
        }

        //DB
        //7-8-2019
        //Bereken opname van discontinuïteit
        if (discontinuityApplicableInCRM && discontinuityApplicableAtEmployee && !CalculateTakeDiscontinuity(data, oldData, saveType, ref reason, crmSettings))
        {
            return false;
        }

        //DB
        //13-10-2020
        //Indien urenboeking vanuit shopfloor wordt aangemaakt wordt de code niet ingevuld. Zorg hiervoor
        if (!FillEmployeeCode(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //DB
        //28-04-2021
        //Log fiatteer info
        if (!SaveApprovalInfo(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //SP
        //03-05-2022
        //Vul de kolommen 'uren dag' en 'uren week' in huidige urenboeking
        //TODO: Nog te plaatsen als de nieuwe kolommen overal aanwezig zijn
		if (!FillTotalHoursDayAndWeek(data, oldData, saveType, ref reason))
		{
		    return false;
		}

        //SP
        //21-6-2022
        //Bij boeken van uren op een project, controleer of dit op een arbeid bon wordt gedaan
        if (!CheckJoborderTypeProjects(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //SP
        //7-12-2023
        //Bij boeken van uren op een project, controleer of dit op een arbeid bon wordt gedaan
        if (!FillFkProjecttimeDayAndWeek(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //07-08-2019
        //Indien we de drempelwaarde van de discontinuiteit overschrijden willen we de nieuwe
        //urenboeking in de aftersave maken. Anders gaat de berekening van het saldo discontinuiteit mis
        if (_CreateNewProjectTimeInAfterSave)
        {
            var workdayInfo = GetWorkdayFromTimeSchedule((int)data["FK_EMPLOYEE"], (DateTime)data["DATE"], ref reason);

            var newOverTimes = workdayInfo.WorkDayOverTimes;

            if (newOverTimes == null)
            {
                return false;
            }

            CreateNewProjectTime(data, _TimeEmployeeNewProjectTime, newOverTimes, _SequenceOverTime);
        }

        return true;
    }

    private bool CheckJoborderTypeProjects(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        var projectModuleActive = _script.GetRecordset("R_CRMSETTINGS", "PROJECTMODULEACTIVE",
            "", "").DataTable.AsEnumerable().First().Field<bool>("PROJECTMODULEACTIVE");

        if (!projectModuleActive)
        {
            return true;//Alleen van toepassing indien vinkje 'Projectenmodule actief' aanstaat
        }

        if (!(data["FK_ORDER"] as int?).HasValue)
        {
            return true;//Alleen doorgaan bij directe uren
        }

        if (saveType == SaveType.AfterUpdateSave && !(oldData["FK_JOBORDER"] as int?).HasValue || !(data["FK_JOBORDER"] as int?).HasValue)
        {
            return true;//Niet doorgaan indien er bij de oude of nieuwe data geen bon is ingevuld
        }

        if (saveType == SaveType.AfterUpdateSave && (int)oldData["FK_JOBORDER"] == (int)data["FK_JOBORDER"])
        {
            return true;//Alleen door bij invoegen urenboeking of wijzigen van de bon
        }

        if ((data["FK_ORDER"] as int?).HasValue && !(data["FK_JOBORDER"] as int?).HasValue)
        {
            reason = "Bon is verplicht.";
            return false;
        }

        Guid tablePhase = new Guid("7db2fca6-1cb4-4a2c-9f96-3b4376a5d37b");
        Guid tableJoborder = new Guid("7de0ac3e-9dfa-47e0-b113-63d3cac55f90");
        Guid tableBudget = new Guid("51e8daf8-b92c-4852-9518-433679420946");

        var rsWbPhase = _script.GetRecordset("R_WORKBREAKDOWNINFO", "PARENTRECORDID",
            $"RECORDID = {data["FK_JOBORDER"]} AND FK_TABLEINFO = '{tableJoborder}' AND FK_PARENTTABLEINFO = '{tablePhase}'", "");

        if (rsWbPhase.RecordCount == 0)
        {
            return true;//geen projectfase gevonden
        }

        var phaseRecordId = rsWbPhase.DataTable.AsEnumerable().FirstOrDefault().Field<int>("PARENTRECORDID");

        var rsWbBudget = _script.GetRecordset("R_WORKBREAKDOWNINFO", "RECORDID",
            $"PARENTRECORDID = {phaseRecordId} AND FK_TABLEINFO = '{tableBudget}' AND FK_PARENTTABLEINFO = '{tablePhase}'", "");

        if (rsWbBudget.RecordCount == 0)
        {
            return true;//geen budgetdetails gevonden
        }

        var budgetdetailId = rsWbBudget.DataTable.AsEnumerable().FirstOrDefault().Field<int>("RECORDID");

        var rsBudgetdetail = _script.GetRecordset("R_PROJECTBUDGETDETAIL", "PK_R_PROJECTBUDGETDETAIL",
            $"PK_R_PROJECTBUDGETDETAIL = {budgetdetailId} AND (LABOR = 1 OR FK_WORKACTIVITY IS NOT NULL OR FK_WORKACTIVITYGROUP IS NOT NULL OR FK_DEPARTMENT IS NOT NULL)", "");

        if (rsBudgetdetail.RecordCount == 0)
        {
            reason = "Bij projecten mogen uren alleen geboekt worden op arbeidsbonnen.";
            return false;
        }

        return true;
    }

    private bool FillTotalHoursDayAndWeek(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave && (long)oldData["TIMEEMPLOYEE"] == (long)data["TIMEEMPLOYEE"])
        {
            return true; //Berekenen dag- en weektotaal alleen van toepassing bij aanmaken urenboeking of wijzigen tijd werknemer urenboeking.
        }

        var projectTimesThisWeekWithoutCurrentProjecttime = GetProjecttimesThisWeek((int)data["FK_EMPLOYEE"], (DateTime)data["DATE"], (int)data["PK_R_PROJECTTIME"]);
        var projectTimesThisDayWithoutCurrentProjecttime = projectTimesThisWeekWithoutCurrentProjecttime
            .Where(x => x.Field<DateTime>("DATE").Date == ((DateTime)data["DATE"]).Date).ToList();

        var dayTotalWithoutCurrentProjecttime = projectTimesThisDayWithoutCurrentProjecttime.Sum(x => x.Field<long>("TIMEEMPLOYEE"));
        var weekTotalWithoutCurrentProjecttime = projectTimesThisWeekWithoutCurrentProjecttime.Sum(x => x.Field<long>("TIMEEMPLOYEE"));

        var dayTotal = dayTotalWithoutCurrentProjecttime + (long)data["TIMEEMPLOYEE"];
        var weekTotal = weekTotalWithoutCurrentProjecttime + (long)data["TIMEEMPLOYEE"];

        data["TOTALHOURSDAY"] = dayTotal;
        data["TOTALHOURSWEEK"] = weekTotal;

        return true;
    }

    private bool FillFkProjecttimeDayAndWeek(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave && (DateTime)oldData["DATE"] == (DateTime)data["DATE"] && (int)oldData["FK_EMPLOYEE"] == (int)data["FK_EMPLOYEE"])
        {
            return true; //Alleen van toepassing bij aanmaken of wijzigen datum/werknemer
        }

        var yearWeek = DetermineYearWeek((DateTime)data["DATE"]);

        var projectTimeEmployeeWeekId = _script.GetRecordset("U_PROJECTTIMEEMPLOYEEWEEK", "PK_U_PROJECTTIMEEMPLOYEEWEEK",
               $"FK_EMPLOYEE = {data["FK_EMPLOYEE"]} AND YEARWEEKNR = {yearWeek}", "").DataTable.AsEnumerable().FirstOrDefault()?.Field<int>("PK_U_PROJECTTIMEEMPLOYEEWEEK") ?? 0;

        if(projectTimeEmployeeWeekId == 0)
        {
            data["FK_PROJECTTIMEEMPLOYEEWEEK"] = DBNull.Value;
            data["FK_PROJECTTIMEEMPLOYEEDAY"] = DBNull.Value;

            return true;//Urenboeking is verder dan een jaar vanaf nu, dit wordt later ingevuld door de taakplanner
        }
        else
        {
            data["FK_PROJECTTIMEEMPLOYEEWEEK"] = projectTimeEmployeeWeekId;
        }

        var date = (DateTime)data["DATE"];

        var projectTimeEmployeeDayId = _script.GetRecordset("U_PROJECTTIMEEMPLOYEEDAY", "PK_U_PROJECTTIMEEMPLOYEEDAY",
            $"FK_PROJECTTIMEEMPLOYEEWEEK = {projectTimeEmployeeWeekId} AND DATE = '{date:yyyyMMdd}'", "").DataTable.AsEnumerable().FirstOrDefault()?.Field<int>("PK_U_PROJECTTIMEEMPLOYEEDAY") ?? 0;

        if (projectTimeEmployeeDayId == 0)
        {
            data["FK_PROJECTTIMEEMPLOYEEDAY"] = DBNull.Value;//Urenboeking is verder dan een jaar vanaf nu, dit wordt later ingevuld door de taakplanner
        }
        else
        {
            data["FK_PROJECTTIMEEMPLOYEEDAY"] = projectTimeEmployeeDayId;
        }

        return true;
    }

    private object DetermineYearWeek(DateTime date)
    {
        var day = (int)CultureInfo.CurrentCulture.DateTimeFormat.Calendar.GetDayOfWeek(date);
        day = day == 0 ? 7 : day;

        var weekStart = date.AddDays(-((day - 1) % 7));

        var jan4 = new DateTime(date.Year, 1, 4);
        var day4Jan = (int)CultureInfo.CurrentCulture.DateTimeFormat.Calendar.GetDayOfWeek(jan4);
        day4Jan = day4Jan == 0 ? 7 : day4Jan;

        var isoYearWeekStart = jan4.AddDays(1 - day4Jan);

        string yearWeek;
        if (weekStart < isoYearWeekStart)
        {
            yearWeek = (date.Year - 1).ToString() + "52";
            if (day4Jan == 4)
            {
                yearWeek = (date.Year - 1).ToString() + "53";
            }
        }
        else
        {
            var weekNum = (weekStart - isoYearWeekStart).Days / 7 + 1;

            if (weekNum >= 52 && date.Month == 12 && date.Day - day > 28)
            {
                yearWeek = (date.Year + 1).ToString() + "01";
            }
            else
            {
                yearWeek = String.Format("{0:0000}{1:00}", date.Year, weekNum);
            }
        }

        return yearWeek;
    }

    private List<DataRow> GetProjecttimesThisWeek(int employee, DateTime date, int currentProjectTimeId)
    {
        int delta = DayOfWeek.Monday - date.DayOfWeek;

        if (delta > 0)
        {
            delta -= 7;
        }

        DateTime weekstart = date.AddDays(delta);
        DateTime weekend = weekstart.AddDays(6);

        return _script.GetRecordset("R_PROJECTTIME", "DATE, TIMEEMPLOYEE",
            $"FK_EMPLOYEE = {employee} AND DATE >= '{weekstart.Date:yyyyMMdd}' AND DATE <= '{weekend.Date:yyyyMMdd}' AND PK_R_PROJECTTIME <> {currentProjectTimeId}"
            , "").DataTable.AsEnumerable().ToList();
    }

    private bool SaveApprovalInfo(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if ((saveType == SaveType.AfterInsertSave && (bool)data["CLOSED"]) ||
            (saveType == SaveType.AfterUpdateSave && !(bool)oldData["CLOSED"] && (bool)data["CLOSED"]))
        {
            var currentEmployeeId = _script.GetRecordset("R_USER", "FK_EMPLOYEE",
                $"PK_R_USER = {CurrentUserInfo.CurrentUserId}", "").DataTable.AsEnumerable()
                .First().Field<int?>("FK_EMPLOYEE") ?? 0;

            if (currentEmployeeId == 0)
            {
                reason = $"Geen werknemer gevonden bij huidige gebruiker.";
                return false;
            }

            data["FK_APPROVEDBY"] = currentEmployeeId;
            data["DATEAPPROVED"] = DateTime.Now;

        }
        else if (saveType == SaveType.AfterUpdateSave && (bool)oldData["CLOSED"] && !(bool)data["CLOSED"])
        {
            data["FK_APPROVEDBY"] = DBNull.Value;
            data["DATEAPPROVED"] = DBNull.Value;
        }

        return true;
    }

    private bool FillEmployeeCode(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true;
        }

        if (!string.IsNullOrEmpty(data["WERKNEMERSCODE"].ToString()))
        {
            return true;
        }

        if (!(data["FK_EMPLOYEE"] as int?).HasValue)
        {
            return true;
        }

        data["WERKNEMERSCODE"] = _script.GetRecordset("R_EMPLOYEE", "CODE",
            $"PK_R_EMPLOYEE = {data["FK_EMPLOYEE"]}", "")
        .DataTable.AsEnumerable().First().Field<string>("CODE");

        return true;
    }


    private bool CalculateTakeDiscontinuity(RowData data, RowData oldData, SaveType saveType, ref string reason, CrmSettings crmSettings)
    {
        if (!(data["FK_WORKACTIVITY"] as int?).HasValue || (int)data["FK_WORKACTIVITY"] == 0)
        {
            return true;
        }

        //Herbereking is van toepassing bij toevoegen en bij wijzigen van werknemer, datum, tijd werknemer en/of bewerking
        if (saveType == SaveType.AfterUpdateSave && oldData["FK_EMPLOYEE"].ToString() == data["FK_EMPLOYEE"].ToString()
            && oldData["DATE"].ToString() == data["DATE"].ToString() &&
            oldData["TIMEEMPLOYEE"].ToString() == data["TIMEEMPLOYEE"].ToString()
            && oldData["FK_WORKACTIVITY"].ToString() == data["FK_WORKACTIVITY"].ToString())
        {
            return true;
        }

        var workactivityType = _script.GetRecordset("R_WORKACTIVITY", "WORKACTIVITYTYPE",
            $"PK_R_WORKACTIVITY = {data["FK_WORKACTIVITY"]}", "").DataTable.AsEnumerable().First()
            .Field<int>("WORKACTIVITYTYPE");

        //Uurtype 8 = 'Discontinuiteit' (Oorspronkelijk = ADV)
        if (workactivityType != 8)
        {
            return true;
        }

        var takeDiscontinuity =
            (long)Math.Round(Convert.ToDouble((long)data["TIMEEMPLOYEE"]) * crmSettings.PercentageTakeDiscontinuity);

        //Check discontinuïteitspotje van werknemer
        var discontinuityBalance = CalculateDiscontinuityBalance((int)data["FK_EMPLOYEE"]);

        if (discontinuityBalance < takeDiscontinuity)
        {
            var timeSpanDiscontinuityBalance = TimeSpan.FromTicks(discontinuityBalance);
            var timeSpanTakeDiscontinuity = TimeSpan.FromTicks(takeDiscontinuity);
            reason =
                $"Huidig saldo discontinuït is {timeSpanDiscontinuityBalance.Hours}u {timeSpanDiscontinuityBalance.Minutes}m, {timeSpanTakeDiscontinuity.Hours}u {timeSpanTakeDiscontinuity.Minutes}m aan discontinuïteit opnemen is niet mogelijk.";
            return false;
        }

        data["TAKEDISCONTINUITY"] = takeDiscontinuity;

        return true;
    }

    private bool CalculateDiscontiuityBuildUp(RowData data, RowData oldData, SaveType saveType, ref string reason, CrmSettings crmSettings)
    {
        if (!(data["FK_WORKACTIVITY"] as int?).HasValue || (int)data["FK_WORKACTIVITY"] == 0)
        {
            return true;
        }

        //Herbereking is van toepassing bij toevoegen en bij wijzigen van werknemer, datum, tijd werknemer en/of overuursoort
        if (saveType == SaveType.AfterUpdateSave && oldData["FK_EMPLOYEE"].ToString() == data["FK_EMPLOYEE"].ToString()
            && oldData["DATE"].ToString() == data["DATE"].ToString() &&
            oldData["TIMEEMPLOYEE"].ToString() == data["TIMEEMPLOYEE"].ToString()
            && oldData["FK_OVERTIMECODE"].ToString() == data["FK_OVERTIMECODE"].ToString())
        {
            return true;
        }

        if (!(data["FK_OVERTIMECODE"] as int?).HasValue)
        {
            return true; //Discontinuïteit alleen van toepassing bij overuren
        }

        var timeCodeDiscontinuity = _script.GetRecordset("R_TIMECODE", "FK_TIMECODEDISCONTINUITY",
            $"PK_R_TIMECODE = {data["FK_OVERTIMECODE"]}", "").DataTable.AsEnumerable().First()
            .Field<int?>("FK_TIMECODEDISCONTINUITY");

        if (!timeCodeDiscontinuity.HasValue)
        {
            return true; //Discontinuïteit niet ingesteld bij deze overuursoort. 
        }

        var buildUpDiscontinuity =
            (long)Math.Round(Convert.ToDouble((long)data["TIMEEMPLOYEE"]) * crmSettings.PercentageBuildUpDiscontinuity);

        //Check discontinuïteitspotje van werknemer
        var discontinuityBalance = CalculateDiscontinuityBalance((int)data["FK_EMPLOYEE"]);

        if (discontinuityBalance >= crmSettings.ThresholdValueDiscontinuity.Value)
        {
            return true; //Potje discontinuïteit zit al vol
        }
        else if (discontinuityBalance + buildUpDiscontinuity > crmSettings.ThresholdValueDiscontinuity.Value)
        {
            //Met de huidige boeking wordt de drempelwaarde bereikt.
            //Maak een nieuwe urenboeking aan voor het restant wat uitbetaald moet worden
            var timeToReachThresholdValue = crmSettings.ThresholdValueDiscontinuity.Value - discontinuityBalance;
            var newTimeEmployee = (long)Math.Round(Convert.ToDouble(timeToReachThresholdValue) / crmSettings.PercentageBuildUpDiscontinuity);

            _CreateNewProjectTimeInAfterSave = true;
            _TimeEmployeeNewProjectTime = (long)data["TIMEEMPLOYEE"] - newTimeEmployee;
            _SequenceOverTime = 1;

            data["TIMEEMPLOYEE"] = newTimeEmployee;
            data["TIMEDEVICE"] = newTimeEmployee;


            buildUpDiscontinuity = timeToReachThresholdValue;
        }

        data["FK_OVERTIMECODE"] = timeCodeDiscontinuity.Value;
        data["BUILDUPDISCONTINUITY"] = buildUpDiscontinuity;
        data["TIMEOFINLIEU"] = 0;

        return true;
    }

    private long CalculateDiscontinuityBalance(int employee)
    {
        //Workactivity type 8 = Discontinuiteit
        var workactivityTypeBalanceStartSaldo = _script.GetRecordset("R_WORKACTIVITYTYPEBALANCE", "TIMESPAN",
            $"FK_EMPLOYEE = {employee} AND WORKACTIVITYTYPE = 8"
            , "").DataTable.AsEnumerable().Sum(x => x.Field<long>("TIMESPAN"));

        var hoursDiscontinuity = _script.GetRecordset("R_PROJECTTIME", "BUILDUPDISCONTINUITY, TAKEDISCONTINUITY",
            $"FK_EMPLOYEE = {employee} AND ((BUILDUPDISCONTINUITY IS NOT NULL AND BUILDUPDISCONTINUITY <> 0) OR (TAKEDISCONTINUITY IS NOT NULL AND TAKEDISCONTINUITY <> 0))"
            , "").DataTable.AsEnumerable();

        var totalBuildUpDiscontinuity = hoursDiscontinuity.Where(x => x.Field<long?>("BUILDUPDISCONTINUITY").HasValue)
            .Sum(x => x.Field<long>("BUILDUPDISCONTINUITY"));

        var totalTakeDiscontinuity = hoursDiscontinuity.Where(x => x.Field<long?>("TAKEDISCONTINUITY").HasValue)
            .Sum(x => x.Field<long>("TAKEDISCONTINUITY"));

        return workactivityTypeBalanceStartSaldo + totalBuildUpDiscontinuity - totalTakeDiscontinuity;
    }

    public CrmSettings GetCrmSettings()
    {
        var CRM = _script.GetRecordset("R_CRMSETTINGS",
            "THRESHOLDVALUEDISCONTINUITY, PERCENTAGETAKEDISCONTINUITY, PERCENTAGEBUILDUPDISCONTINUITY", "", "")
            .DataTable.AsEnumerable().First();

        return new CrmSettings()
        {
            ThresholdValueDiscontinuity = CRM.Field<long?>("THRESHOLDVALUEDISCONTINUITY"),
            PercentageTakeDiscontinuity = CRM.Field<double>("PERCENTAGETAKEDISCONTINUITY"),
            PercentageBuildUpDiscontinuity = CRM.Field<double>("PERCENTAGEBUILDUPDISCONTINUITY"),
        };
    }

    public class CrmSettings
    {
        public long? ThresholdValueDiscontinuity { get; set; }
        public double PercentageTakeDiscontinuity { get; set; }
        public double PercentageBuildUpDiscontinuity { get; set; }
    }

    private bool SplitProjectTime(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true;
        }

        if (!(data["FK_WORKACTIVITY"] as int?).HasValue || (int)data["FK_WORKACTIVITY"] == 0)
        {
            reason = "Bewerking is verplicht.";
            return false;
        }

        //Niet van toepassing bij Shopfloor-boekingen
        if (data["FK_STARTREGISTRATION"] != DBNull.Value && data["FK_STARTREGISTRATION"] != null)
        {
            return true;
        }

        if (!(data["DATE"] as DateTime?).HasValue)
        {
            reason = "Datum is verplicht.";
            return false;
        }

        if ((data["FK_OVERTIMECODE"] as int?).HasValue)
        {
            //Er wordt een overuursoort ingegeven. Maak deze leidend.
            return true;
        }

        DateTime datum = (DateTime)data["DATE"];

        var workdayInfo = GetWorkdayFromTimeSchedule((int)data["FK_EMPLOYEE"], datum, ref reason);

        if (workdayInfo == null)
        {
            return false;
        }

        //Bereken resterend aantal te boeken uren
        var urenVandaag = _script.GetRecordset("R_PROJECTTIME", "TIMEEMPLOYEE, FK_OVERTIMECODE",
            $"PK_R_PROJECTTIME <> {data["PK_R_PROJECTTIME"]} AND DATE >= '{datum.Date:yyyyMMdd}' AND DATE < '{datum.Date.AddDays(1):yyyyMMdd}' AND FK_EMPLOYEE = {data["FK_EMPLOYEE"]}",
        "").DataTable.AsEnumerable();

        long stdUren = urenVandaag.Where(x => !x.Field<int?>("FK_OVERTIMECODE").HasValue).Sum(x => x.Field<long>("TIMEEMPLOYEE"));

        long werkdag = workdayInfo.DefaultHours;

        long nogTeBoeken = werkdag - stdUren;

        //Als het ingeboekte urentotaal groter is dan het nog te boeken aantal dan moet de boeking opgesplitst worden.
        if ((long)data["TIMEEMPLOYEE"] > nogTeBoeken)
        {
            long overTime = (long)data["TIMEEMPLOYEE"] - nogTeBoeken;

            if (overTime < TimeSpan.FromMinutes(1).Ticks)
            {
                return true;
            }

            //CreateNewProjectTime(data, overTime, overTimeCodes, 1);
            _CreateNewProjectTimeInAfterSave = true;
            _TimeEmployeeNewProjectTime = overTime;

            data["TIMEEMPLOYEE"] = nogTeBoeken;
            data["TIMEDEVICE"] = nogTeBoeken;
            data["ENDTIME"] = Convert.ToDateTime(data["STARTTIME"]).AddTicks(nogTeBoeken);
        }

        return true;
    }

    private int GetTimeCodeTravel()
    {
        var rsOvertimeTravel = _script.GetRecordset("R_TIMECODE", "PK_R_TIMECODE", "CODE = 'REIS'", "");

        if (rsOvertimeTravel.RecordCount == 0)
        {
            return 0;
        }

        rsOvertimeTravel.MoveFirst();
        return (int)rsOvertimeTravel.Fields["PK_R_TIMECODE"].Value;
    }

    private void CreateNewProjectTime(RowData data, long newTimeEmployee, List<WorkDayOverTime> overTimeCodes, int dummySequence)
    {
        var remainingTimeToBook = newTimeEmployee;

        ScriptRecordset rsUren = _script.GetRecordset("R_PROJECTTIME", "", "PK_R_PROJECTTIME = -1", "");
        rsUren.UseDataChanges = true;

        while (remainingTimeToBook > 0)
        {
            var overtimeInfo = overTimeCodes.First(x => x.SequenceNumber == dummySequence);

            if (overtimeInfo.Time.Ticks == 0)
            {
                overtimeInfo.Time = TimeSpan.FromHours(24); //0u in het overuursoort is een dummy. Boek het overuur dan gewoon volledig op deze overuursoort
            }

            if (IsTravelWorkactivity((int)data["FK_WORKACTIVITY"]))
            {
                overtimeInfo.Time = TimeSpan.FromHours(24); //We misbruiken de gevonden overuursoort, bij de bewerking reisen willen we alle uren op overuursoort REIS 
                overtimeInfo.TimeCode = GetTimeCodeTravel();
            }

            var timeToBook = (remainingTimeToBook < overtimeInfo.Time.Ticks) ? remainingTimeToBook : overtimeInfo.Time.Ticks;

            rsUren.AddNew();
            rsUren.Fields["DATE"].Value = data["DATE"];
            rsUren.Fields["FK_EMPLOYEE"].Value = data["FK_EMPLOYEE"];
            rsUren.Fields["FK_ORDER"].Value = data["FK_ORDER"];
            rsUren.Fields["FK_JOBORDER"].Value = data["FK_JOBORDER"];

            if ((data["FK_JOBORDERDETAILWORKACTIVITY"] as int? ?? 0) > 0)
            {
                rsUren.Fields["FK_JOBORDERDETAILWORKACTIVITY"].Value = data["FK_JOBORDERDETAILWORKACTIVITY"];
            }

            rsUren.Fields["FK_WORKACTIVITY"].Value = data["FK_WORKACTIVITY"];
            rsUren.Fields["OPERATIONORSETUP"].Value = data["OPERATIONORSETUP"];
            rsUren.Fields["FK_OVERTIMECODE"].Value = overtimeInfo.TimeCode;
            rsUren.Fields["FK_WAGEGROUP"].Value = data["FK_WAGEGROUP"];
            rsUren.Fields["VERKORTEMEMO"].Value = data["VERKORTEMEMO"];

            rsUren.Fields["STARTTIME"].Value = data["ENDTIME"];
            rsUren.Fields["ENDTIME"].Value = Convert.ToDateTime(data["ENDTIME"]).AddTicks(timeToBook);

            rsUren.Fields["TIMEEMPLOYEE"].Value = timeToBook;
            rsUren.Fields["TIMEDEVICE"].Value = timeToBook;

            rsUren.Fields["ORIGINPROJECTTIMEID"].Value = data["PK_R_PROJECTTIME"];

            remainingTimeToBook -= timeToBook;
            dummySequence++;
        }

        rsUren.Update();
    }

    private bool IsTravelWorkactivity(int workactivityId)
    {
        var workactivity = _script.GetRecordset("R_WORKACTIVITY", "WORKACTIVITYTYPE, DESCRIPTION",
            $"PK_R_WORKACTIVITY = {workactivityId}", "")
            .DataTable.AsEnumerable().First();

        return workactivity.Field<WorkActivityType>("WORKACTIVITYTYPE") == WorkActivityType.TravellingHours || workactivity.Field<string>("DESCRIPTION").ToLower().Contains("reis");
    }

    private WorkdayInfo GetWorkdayFromTimeSchedule(int employee, DateTime date, ref string reason)
    {
        var timeTableOffset = _script.GetRecordset("R_TIMETABLEOFFSET", "",
            $"FK_EMPLOYEE = {employee} AND STARTDATE < '{date.Date.AddDays(1):yyyyMMdd}'", "STARTDATE DESC")
            .DataTable.AsEnumerable().ToList();

        if (!timeTableOffset.Any())
        {
            reason = "Geen rooster gevonden bij deze werknemer";
            return null;
        }

        var applicableTimeTableOffset = timeTableOffset.First();

        var timeTableId = applicableTimeTableOffset.Field<int>("FK_TIMETABLE");
        var timeTableStartDate = applicableTimeTableOffset.Field<DateTime>("STARTDATE");
        var timeTableStartDay = applicableTimeTableOffset.Field<int>("FK_TIMETABLEWORKDAY");

        var timeTableWorkdays = _script.GetRecordset("R_TIMETABLEWORKDAY",
            "", $"FK_TIMETABLE = {timeTableId}", "SEQUENCENR ASC").DataTable.AsEnumerable().ToList();

        var sequenceNumberStartDate =
            timeTableWorkdays.First(x => x.Field<int>("PK_R_TIMETABLEWORKDAY") == timeTableStartDay).Field<int>("SEQUENCENR");
        var numberOfStartDateInTimeTable =
            timeTableWorkdays.Count(x => x.Field<int>("SEQUENCENR") <= sequenceNumberStartDate);

        var numberOfWorkdaysInSchedule = timeTableWorkdays.Count();
        var numberOfIntervals = Math.Floor((date - timeTableStartDate).TotalDays / numberOfWorkdaysInSchedule);

        var lastStartDateTimeTable = timeTableStartDate.AddDays(numberOfIntervals * numberOfWorkdaysInSchedule);
        var numberOfDaysBetweenCurrentDateAndLastStartDate =
            Convert.ToInt32(Math.Round((date.Date - lastStartDateTimeTable.Date).TotalDays));

        var daysToLoop = numberOfDaysBetweenCurrentDateAndLastStartDate + numberOfStartDateInTimeTable;
        var indexWorkdayInTimeTable = daysToLoop <= numberOfWorkdaysInSchedule
            ? daysToLoop
            : daysToLoop - numberOfWorkdaysInSchedule;

        var workdayId = timeTableWorkdays[indexWorkdayInTimeTable - 1].Field<int>("FK_WORKDAY");

        var workDay = _script.GetRecordset("R_WORKDAY", "",
            $"PK_R_WORKDAY = {workdayId}", "").DataTable.AsEnumerable().First();

        var workdayOverTimes = _script.GetRecordset("R_WORKDAYOVERTIME", "",
            $"FK_WORKDAY = {workdayId}", "").DataTable.AsEnumerable().Select(x => new WorkDayOverTime()
            {
                SequenceNumber = x.Field<int>("SEQUENCENR"),
                Time = TimeSpan.FromTicks(x.Field<long>("TIMESPAN")),
                TimeCode = x.Field<int>("FK_TIMECODE"),
            }).ToList();

        return new WorkdayInfo()
        {
            DefaultHours = workDay.Field<long>("DEFAULTHOURS"),
            WorkDayOverTimes = workdayOverTimes,
        };
    }

    class WorkdayInfo
    {
        public long DefaultHours { get; set; }
        public List<WorkDayOverTime> WorkDayOverTimes { get; set; }
    }

    class WorkDayOverTime
    {
        public TimeSpan Time { get; set; }
        public int TimeCode { get; set; }
        public int SequenceNumber { get; set; }
    }
}
