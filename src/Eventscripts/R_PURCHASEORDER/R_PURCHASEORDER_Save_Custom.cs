﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Communication.Script;

public class R_PURCHASEORDER_Save_Custom : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        // HVE
        // 23-6-2020
        // Determine Default Divergent Trading Name
        if (!SetDefaultDivergentTradingName(data, oldData, saveType, ref reason))
        {
            return false;
        }

        // HVE
        // 18-5-2021
        // Set WKA from Supplier
        if (!SetWKAFromSupplier(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }


    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //SP
        //13-6-2023
        //Maak opstartopdracht aan voor inkoper
        if (!CreateStartUpForPurchaser(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    private bool CreateStartUpForPurchaser(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave && (int)oldData["FK_PURCHASER"] == (int)data["FK_PURCHASER"])
        {
            return true;//geen wijziging
        }

        var employeeId = (int)data["FK_PURCHASER"];

        var userIds = _script.GetRecordset("R_USER", "PK_R_USER",
               $"FK_EMPLOYEE = {employeeId}", "").DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_USER")).ToList();

        if(!userIds.Any())
        {
            return true;
        }

        Guid command = GetCommandFromCrmSettings();
            
        var rsStartup = _script.GetRecordset("R_STARTUP", "", "PK_R_STARTUP = -1", "");
        rsStartup.UseDataChanges = false;
        rsStartup.UpdateWhenMoveRecord = false;

        foreach (var userId in userIds)
        {
            if(!CommandExists(userId, command))
            {
                rsStartup.AddNew();
                rsStartup.SetFieldValue("FK_USER", userId);
                rsStartup.SetFieldValue("COMMAND", command);
            }            
        }

        if(rsStartup.RecordCount > 0)
        {
            var updateResult = rsStartup.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                reason = $"Aanmaken opstartopdracht mislukt {updateResult.First(x => x.HasError).GetResult()}";
                return false;
            }
        }

        return true;
    }

    private Guid GetCommandFromCrmSettings()
    {
        return _script.GetRecordset("R_CRMSETTINGS", "STARTUPCOMMANDPURCHASEORDERS", "", "")
            .DataTable.AsEnumerable().First().Field<Guid>("STARTUPCOMMANDPURCHASEORDERS");
    }

    private bool CommandExists(int userId, Guid command)
    {
        return _script.GetRecordset("R_STARTUP", "FK_USER, COMMAND",
            $"FK_USER = {userId} AND COMMAND = '{command}'", "").RecordCount > 0;
    }

    private bool SetWKAFromSupplier(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            var fieldsToCompare = new[] { "FK_SUPPLIER" };
            var changed = false;
            foreach (var field in fieldsToCompare)
            {
                if (!object.Equals(oldData[field], data[field]))
                {
                    changed = true;
                    break;
                }
            }
            if (!changed)
            {
                return true;
            }
        }

        var manualCreated = (bool)data["MANUALCREATED"];
        if (manualCreated)
        {
            return true;
        }

        var supplierId = (int)data["FK_SUPPLIER"];
        var wkaSupplier = GetWKAsupplier(supplierId);
        data["WKA"] = wkaSupplier;

        return true;
    }

    private bool GetWKAsupplier(int supplierId)
    {
        return _script.GetRecordset("R_RELATION", "WKA",
            $"PK_R_RELATION = {supplierId}", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<bool>("WKA"))
            .FirstOrDefault();
    }

    private bool SetDefaultDivergentTradingName(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if ((bool)data["MANUALCREATED"])
        {
            return true; // handmatig aangemaakt
        }

        if (saveType == SaveType.AfterUpdateSave && (int)oldData["FK_SUPPLIER"] == (int)data["FK_SUPPLIER"])
        {
            return true; // geen wijziging
        }

        var relationId = (int)data["FK_SUPPLIER"];

        var tradingNameId = GetTradingNameId(relationId);
        if (tradingNameId == 0)
        {
            data["FK_DIVERGENTTRADINGNAME"] = DBNull.Value;
        }
        else
        {
            data["FK_DIVERGENTTRADINGNAME"] = tradingNameId;
        }

        return true;

    }

    private int GetTradingNameId(int relationId)
    {
        return _script.GetRecordset("C_DIVERGENTTRADINGNAMES", "",
            $"FK_RELATION = {relationId} AND DEFAULTTRADINGNAME = TRUE", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int?>("PK_C_DIVERGENTTRADINGNAMES") ?? 0)
            .FirstOrDefault();
    }

}

