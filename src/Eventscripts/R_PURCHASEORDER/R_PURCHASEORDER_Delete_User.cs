﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;

public class R_PURCHASEORDER_Delete_User : IDeleteScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeDelete(RowData data, ref string reason)
    {
        //SP
        //17-5-22
        //Maak voor verwijderen het veld 'original purchaseorder' leeg
        if (!ClearOriginalPurchaseOrder((int)data["PK_R_PURCHASEORDER"], ref reason))
        {
            return false;
        }

        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterDelete(RowData data, ref string reason)
    {
        return true;
    }

    private bool ClearOriginalPurchaseOrder(int id, ref string reason)
    {
        var rsPurchaseorder = _script.GetRecordset("R_PURCHASEORDER", "FK_ORIGNALPURCHASEORDER",
            $"FK_ORIGNALPURCHASEORDER = {id}", "");
        rsPurchaseorder.UpdateWhenMoveRecord = false;

        if (rsPurchaseorder.RecordCount == 0)
        {
            return true;
        }

        rsPurchaseorder.MoveFirst();
        while (!rsPurchaseorder.EOF)
        {
            rsPurchaseorder.SetFieldValue("FK_ORIGNALPURCHASEORDER", DBNull.Value);
            rsPurchaseorder.MoveNext();
        }

        rsPurchaseorder.MoveFirst();
        rsPurchaseorder.Update();

        var updateResult = rsPurchaseorder.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            reason = $"Leegmaken van originele inkooporder mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}";
            return false;
        }

        return true;
    }
}
