﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Communication.Script;
using Ridder.Recordset.Extensions;


public class C_BETONKWALITEIT_Save_User : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //25-4-2024
        //Blokkeer wijzigen indien gebruikt in offertes
        if (!BlockChangingWhenUsed(data, oldData, saveType, ref reason))
        {
            return false;
        }



        return true;
    }



    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }

    private bool BlockChangingWhenUsed(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if(saveType == SaveType.AfterInsertSave)
        {
            return true; //Alleen bij wijzigen 
        }

        if(saveType == SaveType.AfterUpdateSave && oldData["KWALITEIT"].ToString().Equals(data["KWALITEIT"].ToString()))
        {
            return true; //Veld kwaliteit wordt niet gewijzigd
        }

        var offerIds = _script.GetRecordset("R_OFFERDETAILASSEMBLY", "FK_OFFER",
            $"FK_BETONKWALITEIT = {data["PK_C_BETONKWALITEIT"]}", "").DataTable.AsEnumerable()
            .Select(x => x.Field<int>("FK_OFFER")).ToList();

        if(!offerIds.Any())
        {
            return true;
        }

        if(offerIds.Count > 1)
        {
            reason = $"Deze betonkwaliteit is aan meerdere offerteregels gekoppeld. Wijzigen is niet toegestaan. Maak een nieuwe aan.";
            return false;
        }

        var offerState = _script.GetRecordset("R_OFFER", "FK_WORKFLOWSTATE", $"PK_R_OFFER = {offerIds.First()}", "")
            .DataTable.AsEnumerable().First().Field<Guid>("FK_WORKFLOWSTATE");

        var wfStateNew = new Guid("f8751fe5-2c2c-491b-b1b7-60ae603e487d");

        if(offerState != wfStateNew)
        {
            reason = $"Offerte heeft niet meer de status Nieuw. Wijzigen niet toegestaan.";
            return false;
        }

        return true;
    }
}
