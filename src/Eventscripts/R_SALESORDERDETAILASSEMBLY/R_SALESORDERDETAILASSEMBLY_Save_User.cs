﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.IO;
using System.Linq;
using Ridder.Communication.Script;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;

public class R_SALESORDERDETAILASSEMBLY_Save_User : ISaveScript
{
    private ScriptHelper _script = new ScriptHelper();
    private bool _MergeFooterMemo = false;

    string offerDetailTableName = "R_OFFERDETAILASSEMBLY";
    string offerDetailPKRKey = "PK_R_OFFERDETAILASSEMBLY";
    string offerDetailFkKey = "FK_OFFERDETAILASSEMBLY";

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //10-9-2019
        //Check of er een verkoopregel wordt overgenomen van een nieuwe offerte. In dat geval; merge dan de order-memos
        if (saveType == SaveType.AfterInsertSave)
        {
            var mergeMemo = (bool)GetCrmSetting("MERGEORDERMEMO");

            if (mergeMemo)
            {
                CheckIfNewOfferIsConvertedToOrder(data[offerDetailFkKey] as int?, (int)data["FK_ORDER"]);
            }
        }

        //DB
        //15-10-2020
        //Controleer of overrulen krediet check is toegestaan door huidige werknemer
        if (!CheckOverrulenCreditCheckIsAllowed(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }
    
    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //SP
        //29-6-2023
        //Update bij een wijziging van het netto verkoopbedrag de invulvelden van de resultaatneming
        if (!UpdateResultLogFields(data, oldData, saveType, ref reason))
        {
            return false;
        }

        if (saveType == SaveType.AfterUpdateSave)
        {
            return true;
        }

        if (!(data[offerDetailFkKey] as int?).HasValue)
        {
            return true;
        }

        //hier de gebruikersvelden van offerteregel toevoegen
        int offerid = GetOfferId((int)data[offerDetailFkKey]);

        int offerdetailId = (int)data["FK_OFFERDETAILASSEMBLY"];

        FillOrder(offerid, (int)data["FK_ORDER"]);

        FillSalesOrderDetail(data, offerdetailId);

        return true;
    }

    public void FillSalesOrderDetail(RowData data, int offerdetailId)
    {
        var rsOfferdetail = _script.GetRecordset("R_OFFERDETAILASSEMBLY", "FK_COREBUSINESS, FK_PRODUCTGROUP",
            $"PK_R_OFFERDETAILASSEMBLY = {offerdetailId}", "");
        rsOfferdetail.MoveFirst();

        if ((rsOfferdetail.Fields["FK_COREBUSINESS"].Value as int?).HasValue)
        {
            data["FK_COREBUSINESS"] = rsOfferdetail.Fields["FK_COREBUSINESS"].Value;
        }

        if ((rsOfferdetail.Fields["FK_PRODUCTGROUP"].Value as int?).HasValue)
        {
            data["FK_PRODUCTGROUP"] = rsOfferdetail.Fields["FK_PRODUCTGROUP"].Value;
        }
    }

    private bool UpdateResultLogFields(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (oldData != null && (double)oldData["NETSALESAMOUNT"] == (double)data["NETSALESAMOUNT"])
        {
            return true;//Netto verkoopbedrag wordt niet gewijzigd
        }

        var rsOrder = _script.GetRecordset("R_ORDER", "TOTALNETAMOUNT, EXPECTEDMARGINPERC, EXPECTEDMARGINAMOUNT, EXPECTEDTOTALCOSTS, FK_MAINPROJECT",
            $"PK_R_ORDER = {data["FK_ORDER"]}", "");

        var mainprojectId = rsOrder.DataTable.AsEnumerable().FirstOrDefault().Field<int?>("FK_MAINPROJECT") ?? 0;

        if (mainprojectId > 0)
        {
            return true;//Indien er een project is gekoppeld worden de velden hier vandaan gevuld.
        }

        var totalNetAmount = _script.GetRecordset("R_SALESORDERALLDETAIL", "NETSALESAMOUNT",
            $"FK_ORDER = {data["FK_ORDER"]} AND SOURCE_TABLENAME <> 'R_SALESORDERDETAILTEXT'", "").DataTable.AsEnumerable().Sum(x => x.Field<double>("NETSALESAMOUNT"));

        var marginPerc = rsOrder.DataTable.AsEnumerable().First().Field<double>("EXPECTEDMARGINPERC");

        var margin = totalNetAmount * marginPerc;
        var totalExpectedCosts = totalNetAmount - margin;

        rsOrder.MoveFirst();
        rsOrder.SetFieldValue("EXPECTEDMARGINAMOUNT", margin);
        rsOrder.SetFieldValue("EXPECTEDTOTALCOSTS", totalExpectedCosts);

        var updateResult = rsOrder.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            reason = $"Updaten van kolommen t.b.v. resultaatneming mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}";
            return false;
        }

        return true;

    }

    private bool CheckOverrulenCreditCheckIsAllowed(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (!(bool)data["OVERRULECREDITCHECK"])
        {
            return true; //Vinkje staat uit. Check niet van toepassing
        }

        if (saveType == SaveType.AfterUpdateSave &&
            (bool)oldData["OVERRULECREDITCHECK"] == (bool)data["OVERRULECREDITCHECK"])
        {
            return true; //Niets doen; vinkje wordt niet gewijzigd
        }

        //We komen hier als vinkje gelijk aan staat bij aanmaken verkoopregel of als vinkje wordt aangezet

        var netSalesAmountSalesOrderDetailAssembly = (double)data["NETSALESAMOUNT"];
        
        var relationId = GetRelationFromOrder((int)data["FK_ORDER"]);
        var onderhandenOrders = GetOnderhandenOrders(relationId);

        var totalOrderAmountOnderhanden = CalculateTotalOrderAmount(onderhandenOrders);
        var outstandingAmountCreditCheck = GetOutstandingAmountCreditCheck(relationId);
        var invoicedAmountOrderOnderhanden = CalculateInvoicedAmount(onderhandenOrders);
        var totalOutstandingOrderAmount =
            totalOrderAmountOnderhanden - outstandingAmountCreditCheck - invoicedAmountOrderOnderhanden;

        var outstandingInvoicedAmount = GetTotalOutstandingInvoicedAmount(relationId);

        var totalOutstanding = netSalesAmountSalesOrderDetailAssembly + totalOutstandingOrderAmount +
                               outstandingInvoicedAmount;

        var creditLimitRelation = GetCreditLimitRelation(relationId);
        var currentEmployeeInfo = GetCreditMandateCurrentEmployee();

        if (totalOutstanding > (creditLimitRelation + currentEmployeeInfo.CreditMandate))
        {
            reason =
                $"Totaal openstaand is {totalOutstanding:C2}. Kredietlimiet relatie plus kredietmandaat werknemer is niet toereikend om kredietlimiet te overrulen.";

            return false;
        }

        data["FK_CREDITCHECKOVERRULEDBY"] = currentEmployeeInfo.EmployeeId;
        data["DATECREDITCHECKOVERRULED"] = DateTime.Now;

        return true;
    }

    private double GetOutstandingAmountCreditCheck(int relationId)
    {
        return _script.GetRecordset("C_OUTSTANDINGCREDITCHECK", "NETSALESAMOUNT",
                $"FK_RELATION = {relationId}", "")
            .DataTable.AsEnumerable().Sum(x => (double)x["NETSALESAMOUNT"]);
    }

    private double GetCreditLimitRelation(int relationId)
    {
        var rsRelation = _script.GetRecordset("R_RELATION",
            "FK_PARENTCOMPANY, SALESCREDITLIMIT, SALESCREDITLIMITSCORE",
            $"PK_R_RELATION = {relationId}", "");
        rsRelation.MoveFirst();

        if ((rsRelation.GetField("FK_PARENTCOMPANY").Value as int?).HasValue &&
            rsRelation.GetField("SALESCREDITLIMITSCORE").Value.ToString().Contains("403"))
        {
            //Bij 403 verklaring, haal dan het kredietlimiet van het moederbedrijf op
            return _script.GetRecordset("R_RELATION",
                    "SALESCREDITLIMIT",
                    $"PK_R_RELATION = {rsRelation.GetField("FK_PARENTCOMPANY").Value}", "")
                .DataTable.AsEnumerable().First().Field<double>("SALESCREDITLIMIT");
        }

        return (double)rsRelation.GetField("SALESCREDITLIMIT").Value;
    }

    private double GetTotalOutstandingInvoicedAmount(int relationId)
    {
        return _script.GetRecordset("R_SALESINVOICE", "OUTSTANDINGAMOUNT",
                $"FK_INVOICERELATION = {relationId}", "")
            .DataTable.AsEnumerable().Sum(x => (double)x["OUTSTANDINGAMOUNT"]);
    }

    private double CalculateInvoicedAmount(List<int> onderhandenOrders)
    {
        if (!onderhandenOrders.Any())
        {
            return 0.0;
        }

        var salesInvoiceAllDetails = _script.GetRecordset("R_SALESINVOICEALLDETAIL",
            "FK_SALESINVOICE, NETSALESAMOUNT",
            $"FK_ORDER IN ({string.Join(",", onderhandenOrders)})", "").DataTable.AsEnumerable().ToList();

        if (!salesInvoiceAllDetails.Any())
        {
            return 0.0;
        }

        var wfStateDone = new Guid("49978ba1-7725-459e-8a4f-934c050fa8c1");
        var salesInvoiceIdsWithStateDone = _script.GetRecordset("R_SALESINVOICE", "PK_R_SALESINVOICE",
                $"FK_WORKFLOWSTATE = '{wfStateDone}' AND PK_R_SALESINVOICE IN ({string.Join(",", salesInvoiceAllDetails.Select(x => x.Field<int>("FK_SALESINVOICE")).ToList())})", 
                "").DataTable.AsEnumerable().Select(x => (int)x["PK_R_SALESINVOICE"]).ToList();

        if (!salesInvoiceIdsWithStateDone.Any())
        {
            return 0.0;
        }

        return salesInvoiceAllDetails
            .Where(x => salesInvoiceIdsWithStateDone.Contains(x.Field<int>("FK_SALESINVOICE")))
            .Sum(x => (double)x["NETSALESAMOUNT"]);
    }

    private double CalculateTotalOrderAmount(List<int> onderhandenOrders)
    {
        if (!onderhandenOrders.Any())
        {
            return 0.0;
        }

        var totalOrderAmount = _script.GetRecordset("R_ORDER",
                "TOTALNETAMOUNT",
                $"PK_R_ORDER IN ({string.Join(",", onderhandenOrders)})", "")
            .DataTable.AsEnumerable().Sum(x => (double)x["TOTALNETAMOUNT"]);

        return totalOrderAmount;
    }

    private List<int> GetOnderhandenOrders(int relationId)
    {
        var wfStateNew = new Guid("6205d9d2-72d7-49df-8b4b-6e5c1c3d4927");
        var wfStateOnderhanden = new Guid("e83694f3-b6a8-4c2f-8c65-20107edf22bc");

        return _script.GetRecordset("R_ORDER", "PK_R_ORDER",
            $"FK_RELATION = {relationId} AND (FK_WORKFLOWSTATE = '{wfStateNew}' OR FK_WORKFLOWSTATE = '{wfStateOnderhanden}')",
            "").DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_ORDER")).ToList();
    }

    private int GetRelationFromOrder(int orderId)
    {
        return _script.GetRecordset("R_ORDER", "FK_RELATION", 
            $"PK_R_ORDER = {orderId}", "")
            .DataTable.AsEnumerable().First().Field<int>("FK_RELATION");
    }

    private CurrentEmployeeInfo GetCreditMandateCurrentEmployee()
    {
        var currentUser = _script.GetUserInfo().CurrentUserId;

        var employeeId = _script.GetRecordset("R_USER", "FK_EMPLOYEE",
                $"PK_R_USER = {currentUser}", "")
            .DataTable.AsEnumerable().FirstOrDefault()?.Field<int?>("FK_EMPLOYEE") ?? 0;

        if (employeeId == 0)
        {
            return new CurrentEmployeeInfo(){EmployeeId = employeeId, CreditMandate = 0.0};
        }

        var creditMandate = _script.GetRecordset("R_EMPLOYEE", "CREDITMANDATE",
                $"PK_R_EMPLOYEE = {employeeId}", "")
            .DataTable.AsEnumerable().First().Field<double>("CREDITMANDATE");

        return new CurrentEmployeeInfo(){EmployeeId = employeeId, CreditMandate = creditMandate};
    }
   

    private void CheckIfNewOfferIsConvertedToOrder(int? offerDetail, int order)
    {
        if (!offerDetail.HasValue)
        {
            return;
        }

        var currentOfferId = GetOfferId(offerDetail.Value);
        var existingOfferIds = new List<int>();

        var sources = new string[]{"ASSEMBLY", "ITEM", "MISC"};

        foreach (var source in sources)
        {
            var offerDetails = _script.GetRecordset($"R_SALESORDERDETAIL{source}", $"FK_OFFERDETAIL{source}",
                    $"FK_ORDER = {order} AND FK_OFFERDETAIL{source} IS NOT NULL", "").DataTable.AsEnumerable()
                .Select(x => x.Field<int>($"FK_OFFERDETAIL{source}"))
                .ToList();

            if (!offerDetails.Any())
            {
                continue;
            }

            existingOfferIds.AddRange(_script.GetRecordset($"R_OFFERDETAIL{source}", "FK_OFFER",
                    $"PK_R_OFFERDETAIL{source} IN ({string.Join(",", offerDetails)})", "")
                .DataTable.AsEnumerable().Select(x => x.Field<int>("FK_OFFER")).ToList());
        }

        if (existingOfferIds.Count > 0 && !existingOfferIds.Contains(currentOfferId))
        {
            _MergeFooterMemo = true;
        }
    }

    public void FillOrder(int PkrOffer, int PkrOrder)
    {
        var rsOffer = _script.GetRecordset("R_OFFER",
            "EXCHANGEFROMSERVICE, FK_COREBUSINESS, FK_PRODUCTGROUP, FK_INDUSTRY, FOOTERMEMO, PLAINTEXT_FOOTERMEMO, NIETINVOORSTELMEEGENOMEN, PLAINTEXT_NIETINVOORSTELMEEGENOMEN, MEMOALGEMENEVOORWAARDEN, PLAINTEXT_MEMOALGEMENEVOORWAARDEN, VGPLAN, FK_UITGANGSNORM, EXECUTIEKLASSE, CORROSIEKLASSE, VOORBEWERKINGSGRAAD, FK_ALTERNATIVEREPORTLAYOUT, FK_DIVERGENTTRADINGNAME, AWARDBASEDONCO2PERFORMANCELADDER",
            $"PK_R_OFFER = {PkrOffer}", "");
        rsOffer.MoveFirst();

        var rsOrder = _script.GetRecordset("R_ORDER",
            "OMGEZET, EXCHANGEFROMSERVICE, FK_COREBUSINESS, FK_PRODUCTGROUP, FK_INDUSTRY, FOOTERMEMO, PLAINTEXT_FOOTERMEMO, NIETINVOORSTELMEEGENOMEN, PLAINTEXT_NIETINVOORSTELMEEGENOMEN, MEMOALGEMENEVOORWAARDEN, PLAINTEXT_MEMOALGEMENEVOORWAARDEN, VGPLAN, FK_UITGANGSNORM, EXECUTIEKLASSE, CORROSIEKLASSE, VOORBEWERKINGSGRAAD, FK_ALTERNATIVEREPORTLAYOUT, FK_DIVERGENTTRADINGNAME, AWARDBASEDONCO2PERFORMANCELADDER",
            $"PK_R_ORDER = {PkrOrder}", "");
        rsOrder.MoveFirst();

        if (!(bool)rsOrder.Fields["OMGEZET"].Value)
        {
            rsOrder.Fields["FK_INDUSTRY"].Value = rsOffer.Fields["FK_INDUSTRY"].Value;
            rsOrder.Fields["VGPLAN"].Value = rsOffer.Fields["VGPLAN"].Value;

            //NEN-info
            rsOrder.Fields["FK_UITGANGSNORM"].Value = rsOffer.Fields["FK_UITGANGSNORM"].Value;
            rsOrder.Fields["EXECUTIEKLASSE"].Value = rsOffer.Fields["EXECUTIEKLASSE"].Value;
            rsOrder.Fields["CORROSIEKLASSE"].Value = rsOffer.Fields["CORROSIEKLASSE"].Value;
            rsOrder.Fields["VOORBEWERKINGSGRAAD"].Value = rsOffer.Fields["VOORBEWERKINGSGRAAD"].Value;

            rsOrder.Fields["MEMOALGEMENEVOORWAARDEN"].Value = rsOffer.Fields["MEMOALGEMENEVOORWAARDEN"].Value;
            rsOrder.Fields["NIETINVOORSTELMEEGENOMEN"].Value = rsOffer.Fields["NIETINVOORSTELMEEGENOMEN"].Value;

            rsOrder.Fields["EXCHANGEFROMSERVICE"].Value = rsOffer.Fields["EXCHANGEFROMSERVICE"].Value;

            rsOrder.Fields["AWARDBASEDONCO2PERFORMANCELADDER"].Value = rsOffer.Fields["AWARDBASEDONCO2PERFORMANCELADDER"].Value;

            //Bij LBR is de projectleider altijd Tim Ursem
            if (_script.GetUserInfo().DatabaseName == "Loos Betonreparaties")
            {
                rsOrder.Fields["FK_PLANNER"].Value = 150;
            }

            // HVE
            // 30-4-2020
            // alternative report lay-out overnemen indien gevuld
            if ((rsOffer.Fields["FK_ALTERNATIVEREPORTLAYOUT"].Value as int? ?? 0) > 0)
            {
                rsOrder.Fields["FK_ALTERNATIVEREPORTLAYOUT"].Value = rsOffer.Fields["FK_ALTERNATIVEREPORTLAYOUT"].Value;
            }

            // HVE
            // 7-7-2020
            // Handelsnaam overnemen van offerte
            if ((rsOffer.Fields["FK_DIVERGENTTRADINGNAME"].Value as int? ?? 0) == 0)
            {
                rsOrder.Fields["FK_DIVERGENTTRADINGNAME"].Value = DBNull.Value;
            }
            else
            {
                rsOrder.Fields["FK_DIVERGENTTRADINGNAME"].Value = rsOffer.Fields["FK_DIVERGENTTRADINGNAME"].Value;
            }

            //SP
            //29-3-2022
            //Bij DLP maken we gebruik van kernactiviteiten op offerte/order niveau. Wanneer we van een offerte een order maken moet deze worden overgenomen
            //DB - 25-1-2024 - Toevoeging gemaakt
            if ((rsOffer.Fields["FK_COREBUSINESS"].Value as int?).HasValue)
            {
                rsOrder.Fields["FK_COREBUSINESS"].Value = rsOffer.Fields["FK_COREBUSINESS"].Value;
            }

            if ((rsOffer.Fields["FK_PRODUCTGROUP"].Value as int?).HasValue)
            {
                rsOrder.Fields["FK_PRODUCTGROUP"].Value = rsOffer.Fields["FK_PRODUCTGROUP"].Value;
            }

            //DB
            //6-5-2015
            KopieerOfferteDocumentenNaarOrdermap(PkrOffer, PkrOrder);
        }


        //Footermemo = 'In ons voorstel opgenomen' - Merge indien meerdere offertes tot één order worden omgezet
        if (_MergeFooterMemo)
        {
            rsOrder.Fields["FOOTERMEMO"].Value = _script.RtfOperations.MergeRtf(
                rsOrder.Fields["FOOTERMEMO"].Value.ToString(),
                rsOffer.Fields["FOOTERMEMO"].Value.ToString());

            //DB
            //6-5-2015
            KopieerOfferteDocumentenNaarOrdermap(PkrOffer, PkrOrder);
        }



        rsOrder.Fields["OMGEZET"].Value = 1;

        rsOrder.Update();
    }

    public int GetOfferId(int PkrOfferDetail)
    {
        return _script.GetRecordset(offerDetailTableName, "FK_OFFER",
                $"{offerDetailPKRKey} = {PkrOfferDetail}", "")
            .DataTable.AsEnumerable().First().Field<int>("FK_OFFER");
    }
    
    public void KopieerOfferteDocumentenNaarOrdermap(int PkrOffer, int PkrOrder)
    {
        DataSet dtOffer = ScriptRecordset.GetDataSet("R_OFFER", "OFFERNUMBER", "PK_R_OFFER = " + PkrOffer, "");
        ScriptRecordset rsOffer = new ScriptRecordset(dtOffer);
        rsOffer.MoveFirst();

        DataSet dtOrder = ScriptRecordset.GetDataSet("R_ORDER", "ORDERNUMBER", "PK_R_ORDER = " + PkrOrder, "");
        ScriptRecordset rsOrder = new ScriptRecordset(dtOrder);
        rsOrder.MoveFirst();

        string offerLocation =
            $"{GetCrmSetting("OFFERLOCATION").ToString()}\\{rsOffer.Fields["OFFERNUMBER"].Value.ToString()}";

        if (Directory.Exists(offerLocation))
        {
            string orderLocation =
                $"{GetCrmSetting("ORDERLOCATION").ToString()}\\{rsOrder.Fields["ORDERNUMBER"].Value.ToString()}";

            if(!Directory.Exists(orderLocation))
            {
                Directory.CreateDirectory(orderLocation);
            }


            string targetDir = @"" + orderLocation;

            IShellLink link = (IShellLink)new ShellLink();         // setup shortcut information
            link.SetDescription("My Description");
            link.SetPath($"{offerLocation}");         // save it
            IPersistFile file = (IPersistFile)link;
            //string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            file.Save(Path.Combine($"{targetDir}", $"Offerte_{rsOffer.Fields["OFFERNUMBER"].Value.ToString()}.lnk"), false);

            /*Directory.CreateDirectory(targetDir);

            DirectoryInfo sourceLocation = new DirectoryInfo(offerLocation);
            DirectoryInfo targetLocation = new DirectoryInfo(targetDir);

            CopyFolder(sourceLocation, targetLocation);*/
        }

    }

    [ComImport]
    [Guid("00021401-0000-0000-C000-000000000046")]
    internal class ShellLink
    {
    }
    [ComImport]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [Guid("000214F9-0000-0000-C000-000000000046")]
    internal interface IShellLink
    {
        void GetPath([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszFile, int cchMaxPath, out IntPtr pfd, int fFlags);
        void GetIDList(out IntPtr ppidl);
        void SetIDList(IntPtr pidl);
        void GetDescription([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszName, int cchMaxName);
        void SetDescription([MarshalAs(UnmanagedType.LPWStr)] string pszName);
        void GetWorkingDirectory([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszDir, int cchMaxPath);
        void SetWorkingDirectory([MarshalAs(UnmanagedType.LPWStr)] string pszDir);
        void GetArguments([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszArgs, int cchMaxPath);
        void SetArguments([MarshalAs(UnmanagedType.LPWStr)] string pszArgs);
        void GetHotkey(out short pwHotkey);
        void SetHotkey(short wHotkey);
        void GetShowCmd(out int piShowCmd);
        void SetShowCmd(int iShowCmd);
        void GetIconLocation([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszIconPath, int cchIconPath, out int piIcon);
        void SetIconLocation([MarshalAs(UnmanagedType.LPWStr)] string pszIconPath, int iIcon);
        void SetRelativePath([MarshalAs(UnmanagedType.LPWStr)] string pszPathRel, int dwReserved);
        void Resolve(IntPtr hwnd, int fFlags);
        void SetPath([MarshalAs(UnmanagedType.LPWStr)] string pszFile);
    }


    public object GetCrmSetting(string columnName)
    {
        var rsCRMSettings = _script.GetRecordset("R_CRMSETTINGS", columnName, "", "");
        rsCRMSettings.MoveFirst();

        return rsCRMSettings.Fields[columnName].Value;
    }

    public static void CopyFolder(DirectoryInfo source, DirectoryInfo target)
    {
        foreach (DirectoryInfo dir in source.GetDirectories())
        {
            var newDirectoryFullName = Path.Combine(target.FullName, dir.Name);

            if (newDirectoryFullName.Length >= 248)
            {
                continue; //Dit is een max van Windows. Sla de kopieer actie over anders komt er een foutmelding
            }

            CopyFolder(dir, target.CreateSubdirectory(dir.Name));
        }

        foreach (FileInfo file in source.GetFiles())
        {
            var newFileName = Path.Combine(target.FullName, file.Name);

            if (newFileName.Length >= 260)
            {
                continue; //Dit is een max van Windows. Sla de kopieer actie over anders komt er een foutmelding
            }

            file.CopyTo(Path.Combine(target.FullName, file.Name));
        }
    }

    class CurrentEmployeeInfo
    {
        public int EmployeeId { get; set; }
        public double CreditMandate { get; set; }
    }

    private enum CreditCheck : int
    {
        _ = 1,
        Akkoord = 2,
        Geblokkeerd = 3
    }
}
