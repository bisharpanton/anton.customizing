﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Communication.Script;

public class R_ORDERBASE_Save_User : ISaveScript
{
    private ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //21-12-2017
        //Indien er voor het eerst een orderbasis uit een verkoopregel wordt gegenereerd dan wordt de standaard koppeling naar de verkoopregel 
        //opgeslagen, vul dan ook de custom koppeling
        if (saveType == SaveType.AfterInsertSave && (data["FK_SALESORDERDETAILASSEMBLY"] as int?).HasValue)
        {
            data["FK_SALESORDERDETAILASSEMBLYCUSTOM"] = data["FK_SALESORDERDETAILASSEMBLY"];
        }

        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }
}
