﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;

public class C_CONTRACTTEXT_Delete_Custom : IDeleteScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeDelete(RowData data, ref string reason)
    {
        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterDelete(RowData data, ref string reason)
    {
        // HVE
        // 9-3-2021
        // Update the sequence number of the other sections
        if (!UpdateSequenceWithinTheChapter(data, ref reason))
        {
            return false;
        }

        //SP
        //8-12-2022
        //Update chapter numbers of other chapters
        if (!UpdateChapterNumberOfOtherChapters(data, ref reason))
        {
            return false;
        }

        return true;
    }

    private bool UpdateChapterNumberOfOtherChapters(RowData data, ref string reason)
    {
        var chapterNumber = (int)data["CHAPTERNUMBER"];
        var contractId = (int)data["FK_PURCHASECONTRACT"];

        var rsTexts = _script.GetRecordset("C_CONTRACTTEXT", "CHAPTERNUMBER",
            $"FK_PURCHASECONTRACT = {contractId} AND CHAPTERNUMBER = {chapterNumber}", "CHAPTERNUMBER");

        if (rsTexts.RecordCount > 0)
        {
            return true;//Stoppen als er nog andere teksten zijn met hetzelfde hoofdstuknummer
        }

        var rsTextsToUpdate = _script.GetRecordset("C_CONTRACTTEXT", "CHAPTERNUMBER",
            $"FK_PURCHASECONTRACT = {contractId} AND CHAPTERNUMBER > {chapterNumber}", "CHAPTERNUMBER");
        var update = false;

        rsTextsToUpdate.UpdateWhenMoveRecord = false;
        rsTextsToUpdate.MoveFirst();
        while (!rsTextsToUpdate.EOF)
        {
            var oldChapter = (int)rsTextsToUpdate.Fields["CHAPTERNUMBER"].Value;
            rsTextsToUpdate.SetFieldValue("CHAPTERNUMBER", oldChapter-1);
            update = true;

            rsTextsToUpdate.MoveNext();
        }

        if (update)
        {
            rsTextsToUpdate.MoveFirst();
            rsTextsToUpdate.Update();
        }

        return true;
    }

    private bool UpdateSequenceWithinTheChapter(RowData data, ref string reason)
    {
        var chapterId = (int)data["FK_CHAPTER"];
        var contractId = (int)data["FK_PURCHASECONTRACT"];

        var rsTexts = _script.GetRecordset("C_CONTRACTTEXT", "SEQUENCE",
            $"FK_CHAPTER = {chapterId} AND FK_PURCHASECONTRACT = {contractId}", "SEQUENCE");
        
        if (rsTexts.RecordCount == 0)
        {
            return true;
        }
        
        var oldSequence = (int)data["SEQUENCE"];
        var index = 1;
        var update = false;

        rsTexts.UpdateWhenMoveRecord = false;
        rsTexts.MoveFirst();
        while (!rsTexts.EOF)
        {
            var sequence = (int)rsTexts.Fields["SEQUENCE"].Value;
            if (sequence > oldSequence)
            {
                rsTexts.SetFieldValue("SEQUENCE", index);
                update = true;
            }

            index++;
            rsTexts.MoveNext();

        }

        if (update)
        {
            rsTexts.MoveFirst();
            rsTexts.Update();
        }

        return true;

    }
}
