﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;

public class C_ORDERFINANCIALSTATE_Save_Custom : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        // HVE
        // 22-10-2021
        // If Rendement_perc changed, inform the user.
        if (!RedementChangedIsInformUser(data, oldData, saveType, ref reason))
        {
            return false;
        }
        
        return true;
    }

    private bool RedementChangedIsInformUser(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterInsertSave)
        {
            return true;
        }

        var rendement = data["RENDEMENT_PERC"] as double? ?? 0d;
        var oldRendement = oldData["RENDEMENT_PERC"] as double? ?? 0d;

        if (rendement == 0d || rendement == oldRendement)
        {
            return true;
        }

        var orderId = (int)data["FK_ORDER"];
        var wf_ShowMessage = new Guid("94179d39-bf4c-4bae-9160-625cdf55978c");

        _script.ExecuteWorkflowEvent("R_ORDER", orderId, wf_ShowMessage, null);

        return true;
    }
}
