﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Communication.Script;


public class R_JOBORDERDETAILOUTSOURCED_Save_User : ISaveScript
{
    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //07-03-2018
        //We willen dat de kolom 'Leverancier' altijd gevuld is bij uitbesteed werk die aangemerkt zijn als 'Pomp'. 
        //In de calculatietool vangen we deze verplichting af. Totdat deze controle nog niet ingebouwd is bij 
        //de calculatietool vullen we gewoon default de hoofdleverancier in als de leverancier niet doorkomt vanuit de calculatietool

        if (!CopyMainSupplier(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //07-03-2018
        //Bij aanmaken pomp-bonregel ubw of bij wijzigen leverancier, kopieer de waarde door naar de bon
        if (!UpdatePumpSupplierToJoborder(data, oldData, saveType, ref reason))
        {
            return false;
        }


        //DB
        //07-03-2018
        //Bij aanmaken pomp-regel of wijzigen ubw, kopieer code door naar pomptype op de bon
        if (!UpdatePumpCodeToJoborder(data, oldData, saveType, ref reason))
        {
            return false;
        }



        return true;
    }

    public bool CopyMainSupplier(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (oldData == null && !(data["FK_SUPPLIER"] as int?).HasValue)
        {
            ScriptRecordset rsOutsourcedActivity = new ScriptRecordset(ScriptRecordset.GetDataSet("R_OUTSOURCEDACTIVITY", "ISPUMP",
                string.Format("PK_R_OUTSOURCEDACTIVITY = {0}", data["FK_OUTSOURCEDACTIVITY"]), ""));
            rsOutsourcedActivity.MoveFirst();

            if (!(bool)rsOutsourcedActivity.Fields["ISPUMP"].Value)
            {
                return true;
            }

            ScriptRecordset rsMainSupplier = new ScriptRecordset(ScriptRecordset.GetDataSet("R_OUTSOURCEDPURCHASEPRICE", "FK_SUPPLIER",
                string.Format("FK_OUTSOURCEDACTIVITY = {0} AND MAINSUPPLIER = 1", data["FK_OUTSOURCEDACTIVITY"]), ""));

            if (rsMainSupplier.RecordCount == 0)
            {
                return true;
            }

            rsMainSupplier.MoveFirst();

            data["FK_SUPPLIER"] = rsMainSupplier.Fields["FK_SUPPLIER"].Value;
        }

        return true;
    }


    public bool UpdatePumpSupplierToJoborder(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if ((data["FK_SUPPLIER"] as int?).HasValue && (oldData == null || (oldData != null && (oldData["FK_SUPPLIER"].ToString() != data["FK_SUPPLIER"].ToString()))))
        {
            ScriptRecordset rsOutsourcedActivity = new ScriptRecordset(ScriptRecordset.GetDataSet("R_OUTSOURCEDACTIVITY", "ISPUMP",
                string.Format("PK_R_OUTSOURCEDACTIVITY = {0}", data["FK_OUTSOURCEDACTIVITY"]), ""));
            rsOutsourcedActivity.MoveFirst();

            if (!(bool)rsOutsourcedActivity.Fields["ISPUMP"].Value)
            {
                return true;
            }

            //Check op afwijkende leverancier in huidige bon, dan veld leegmaken
            ScriptRecordset rsOutsourcedActivitiesPump = new ScriptRecordset(ScriptRecordset.GetDataSet("R_OUTSOURCEDACTIVITY", "PK_R_OUTSOURCEDACTIVITY", "ISPUMP = 1", ""));
            var ubwPompIds = rsOutsourcedActivitiesPump.DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_OUTSOURCEDACTIVITY")).ToList();

            ScriptRecordset rsBonregelsUbwPomp = new ScriptRecordset(ScriptRecordset.GetDataSet("R_JOBORDERDETAILOUTSOURCED", "FK_SUPPLIER",
                string.Format("FK_JOBORDER = {0} AND FK_OUTSOURCEDACTIVITY IN ({1}) AND FK_SUPPLIER IS NOT NULL", data["FK_JOBORDER"], string.Join(",", ubwPompIds)), ""));
            var bonregelsUbwPomp = rsBonregelsUbwPomp.DataTable.AsEnumerable();

            int huidigePompLeverancier = (int)data["FK_SUPPLIER"];
            bool anderePompLeverancierAanwezig = bonregelsUbwPomp.Max(x => x.Field<int>("FK_SUPPLIER")) != huidigePompLeverancier
                || bonregelsUbwPomp.Min(x => x.Field<int>("FK_SUPPLIER")) != huidigePompLeverancier;

            //Verwerk door naar bon
            ScriptRecordset rsBon = new ScriptRecordset(ScriptRecordset.GetDataSet("R_JOBORDER", "FK_POMPLEVERANCIER",
                string.Format("PK_R_JOBORDER = {0}", data["FK_JOBORDER"]), ""));
            rsBon.MoveFirst();
            rsBon.Fields["FK_POMPLEVERANCIER"].Value = (anderePompLeverancierAanwezig) ? DBNull.Value : data["FK_SUPPLIER"];
            rsBon.Update();
        }

        return true;
    }

    public bool UpdatePumpCodeToJoborder(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if ((data["FK_OUTSOURCEDACTIVITY"] as int?).HasValue && (oldData == null || (oldData != null && (oldData["FK_OUTSOURCEDACTIVITY"].ToString() != data["FK_OUTSOURCEDACTIVITY"].ToString()))))
        {
            ScriptRecordset rsOutsourcedActivity = new ScriptRecordset(ScriptRecordset.GetDataSet("R_OUTSOURCEDACTIVITY", "ISPUMP, CODE",
                string.Format("PK_R_OUTSOURCEDACTIVITY = {0}", data["FK_OUTSOURCEDACTIVITY"]), ""));
            rsOutsourcedActivity.MoveFirst();

            if (!(bool)rsOutsourcedActivity.Fields["ISPUMP"].Value)
            {
                return true;
            }

            ScriptRecordset rsOutsourcedActivitiesPump = new ScriptRecordset(ScriptRecordset.GetDataSet("R_OUTSOURCEDACTIVITY", "PK_R_OUTSOURCEDACTIVITY", "ISPUMP = 1", ""));
            var ubwPompIds = rsOutsourcedActivitiesPump.DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_OUTSOURCEDACTIVITY")).ToList();

            ScriptRecordset rsBonregelsUbwPomp = new ScriptRecordset(ScriptRecordset.GetDataSet("R_JOBORDERDETAILOUTSOURCED", "FK_OUTSOURCEDACTIVITY",
                string.Format("FK_JOBORDER = {0} AND FK_OUTSOURCEDACTIVITY IN ({1})", data["FK_JOBORDER"], string.Join(",", ubwPompIds)), ""));
            var ubwIdsOpBonregel = rsBonregelsUbwPomp.DataTable.AsEnumerable().Select(x => x.Field<int>("FK_OUTSOURCEDACTIVITY")).ToList();

            ScriptRecordset rsUbwCodesBetonOpBon = new ScriptRecordset(ScriptRecordset.GetDataSet("R_OUTSOURCEDACTIVITY", "DESCRIPTION",
                string.Format("PK_R_OUTSOURCEDACTIVITY IN ({0})", string.Join(",", ubwIdsOpBonregel)), ""));
            var codes = rsUbwCodesBetonOpBon.DataTable.AsEnumerable().Select(x => x.Field<string>("DESCRIPTION")).ToList();

            //Verwerk door naar bon
            ScriptRecordset rsBon = new ScriptRecordset(ScriptRecordset.GetDataSet("R_JOBORDER", "PUMPTYPE",
                string.Format("PK_R_JOBORDER = {0}", data["FK_JOBORDER"]), ""));
            rsBon.MoveFirst();

            string result = string.Join(" / ", codes);
            rsBon.Fields["PUMPTYPE"].Value = (result.Length > 250) ? result.Substring(0, 250) : result;
            rsBon.Update();

        }

        return true;
    }
}
