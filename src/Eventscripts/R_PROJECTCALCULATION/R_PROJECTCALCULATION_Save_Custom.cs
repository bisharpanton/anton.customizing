﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Ridder.Communication.Script;

public class R_PROJECTCALCULATION_Save_Custom : ISaveScript
{
    private ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //07-05-2019
        //Copy user column 'Total net amount' from project
        if (!CopyUserColumns(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //DB
        //07-05-2019
        //Bepaal voorgaande project calculatie
        if (!DeterminePreviousProjectCalculation(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }


    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //07-05-2019
        //Kopieer de huidige verkoopregels naar deze projectcalculatie
        if (!CopySalesOrderDetails(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //DB
        //07-05-2019
        //Kopieer de huidige uitgebrachte offerteregels naar deze projectcalculatie
        // 24-8-2021 HVE
        // waarde = bedrag * scoringskans

        if (!CopyOfferDetails(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    private bool DeterminePreviousProjectCalculation(RowData data, RowData oldData, SaveType saveType,
        ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true; //Alleen doorgaan bij invoegen
        }

        var projectCalculations = _script.GetRecordset("R_PROJECTCALCULATION", "PK_R_PROJECTCALCULATION",
                $"FK_MAINPROJECT = {data["FK_MAINPROJECT"]}", "")
            .DataTable.AsEnumerable();

        if (!projectCalculations.Any())
        {
            return true;
        }

        data["PREVIOUSPROJECTCALCULATION"] = projectCalculations.Max(x => x.Field<int>("PK_R_PROJECTCALCULATION"));

        return true;
    }

    private bool CopyOfferDetails(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true; //Alleen doorgaan bij invoegen
        }

        var offers = _script.GetRecordset("R_OFFER", "PK_R_OFFER, OFFERNUMBER, REVISION, PROBABILITYOFSALE, FK_WORKFLOWSTATE",
                $"FK_MAINPROJECT = {data["FK_MAINPROJECT"]} AND " +
                $"(FK_WORKFLOWSTATE IN ('{OfferWfStates.SubMitted}','{OfferWfStates.New}','{OfferWfStates.TeFiatteren}','{OfferWfStates.Gefiatteerd}'))", "")
            .DataTable.AsEnumerable();

        if (!offers.Any())
        {
            return true;
        }

        var offerIds = offers.Select(x => x.Field<int>("PK_R_OFFER")).ToList();
        var offerDetailsMisc = _script.GetRecordset("R_OFFERDETAILMISC",
                "FK_OFFER, LINENUMBER, DESCRIPTION, QUANTITY, NETSALESAMOUNT",
                $"FK_OFFER IN ({string.Join(",", offerIds)}) AND KOSTENINPROJECT = 1", "")
            .DataTable.AsEnumerable();

        if (!offerDetailsMisc.Any())
        {
            return true;
        }

        CopyOfferDetailsToProjectCalculation((int)data["PK_R_PROJECTCALCULATION"], offers, offerDetailsMisc);

        return true;
    }

    private void CopyOfferDetailsToProjectCalculation(int projectCalculation, EnumerableRowCollection<DataRow> offers, 
        EnumerableRowCollection<DataRow> offerDetailsMisc)
    {
        var rsCalculationOfferDetails = _script.GetRecordset("C_PROJECTCALCULATIONOFFERDETAIL", "",
            "PK_C_PROJECTCALCULATIONOFFERDETAIL = -1", "");
        rsCalculationOfferDetails.UseDataChanges = false;
        rsCalculationOfferDetails.UpdateWhenMoveRecord = false;

        foreach (var offerDetail in offerDetailsMisc)
        {
            rsCalculationOfferDetails.AddNew();

            rsCalculationOfferDetails.Fields["FK_PROJECTCALCULATION"].Value = projectCalculation;

            var offer = offers.First(x => x.Field<int>("PK_R_OFFER") == offerDetail.Field<int>("FK_OFFER"));

            rsCalculationOfferDetails.Fields["OFFERNUMBER"].Value =
                $"{offer.Field<int>("OFFERNUMBER")} Rev. {offer.Field<int>("REVISION")}";

            rsCalculationOfferDetails.Fields["LINENUMBER"].Value = offerDetail.Field<int>("LINENUMBER");
            rsCalculationOfferDetails.Fields["DESCRIPTION"].Value = offerDetail.Field<string>("DESCRIPTION");
            rsCalculationOfferDetails.Fields["QUANTITY"].Value = offerDetail.Field<double>("QUANTITY");
            rsCalculationOfferDetails.Fields["NETSALESAMOUNT"].Value = offerDetail.Field<double>("NETSALESAMOUNT") * offer.Field<double>("PROBABILITYOFSALE");
            rsCalculationOfferDetails.Fields["PROBABILITYOFSALE"].Value = offer.Field<double>("PROBABILITYOFSALE");

            rsCalculationOfferDetails.Fields["OFFERWORKFLOWSTATE"].Value = _script.GetTranslatableFieldValue("R_WORKFLOWSTATE", "STATE", 1, offer.Field<Guid>("FK_WORKFLOWSTATE"));
        }

        rsCalculationOfferDetails.MoveFirst();
        rsCalculationOfferDetails.Update();
    }

    private bool CopySalesOrderDetails(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true; //Alleen doorgaan bij invoegen
        }

        var orders = _script.GetRecordset("R_ORDER", "PK_R_ORDER, ORDERNUMBER",
                $"FK_MAINPROJECT = {data["FK_MAINPROJECT"]}", "")
            .DataTable.AsEnumerable();

        if (!orders.Any())
        {
            return true;
        }

        var orderIds = orders.Select(x => x.Field<int>("PK_R_ORDER")).ToList();
        var salesOrderDetails = _script.GetRecordset("R_SALESORDERALLDETAIL",
                "FK_ORDER, LINENUMBER, DESCRIPTION, QUANTITY, NETSALESAMOUNT, ADDITIONALWORK",
                $"FK_ORDER IN ({string.Join(",", orderIds)})", "")
            .DataTable.AsEnumerable();

        if (!salesOrderDetails.Any())
        {
            return true;
        }

        CopySalesOrderDetailsToProjectCalculation((int)data["PK_R_PROJECTCALCULATION"], orders, salesOrderDetails);

        return true;
    }

    private void CopySalesOrderDetailsToProjectCalculation(int projectCalculation, EnumerableRowCollection<DataRow> orders,
        EnumerableRowCollection<DataRow> salesOrderDetails)
    {
        var rsCalculationSalesDetails = _script.GetRecordset("C_PROJECTCALCULATIONSALESORDERDETAIL", "",
            "PK_C_PROJECTCALCULATIONSALESORDERDETAIL = -1", "");
        rsCalculationSalesDetails.UseDataChanges = false;
        rsCalculationSalesDetails.UpdateWhenMoveRecord = false;

        foreach (var salesOrderDetail in salesOrderDetails)
        {
            rsCalculationSalesDetails.AddNew();

            rsCalculationSalesDetails.Fields["FK_PROJECTCALCULATION"].Value = projectCalculation;
            rsCalculationSalesDetails.Fields["ORDERNUMBER"].Value =
                orders.First(x => x.Field<int>("PK_R_ORDER") == salesOrderDetail.Field<int>("FK_ORDER"))
                    .Field<int>("ORDERNUMBER");
            rsCalculationSalesDetails.Fields["LINENUMBER"].Value = salesOrderDetail.Field<int>("LINENUMBER");
            rsCalculationSalesDetails.Fields["DESCRIPTION"].Value = salesOrderDetail.Field<string>("DESCRIPTION");
            rsCalculationSalesDetails.Fields["QUANTITY"].Value = salesOrderDetail.Field<double>("QUANTITY");
            rsCalculationSalesDetails.Fields["NETSALESAMOUNT"].Value = salesOrderDetail.Field<double>("NETSALESAMOUNT");
            rsCalculationSalesDetails.Fields["ADDITIONALWORK"].Value = salesOrderDetail.Field<bool>("ADDITIONALWORK");

        }

        rsCalculationSalesDetails.MoveFirst();
        rsCalculationSalesDetails.Update();
    }

    private bool CopyUserColumns(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true; //Alleen doorgaan bij invoegen
        }

        var mainOrder = _script.GetRecordset("R_MAINPROJECT", "FK_ORDER",
                $"PK_R_MAINPROJECT = {data["FK_MAINPROJECT"]}", "")
            .DataTable.AsEnumerable().First().Field<int?>("FK_ORDER");

        if (!mainOrder.HasValue)
        {
            return true;
        }

        var orders = _script.GetRecordset("R_ORDER", "TOTALNETAMOUNT, FINANCIALDONE",
                $"PK_R_ORDER = {mainOrder.Value}", "")
            .DataTable.AsEnumerable().ToList();

        data["TOTALNETAMOUNT"] = orders.Sum(x => (double)x["TOTALNETAMOUNT"]);

        var orderIdsNotFinancialDone = orders.Where(x => !(bool)x["FINANCIALDONE"]).Select(x => x.Field<int>("PK_R_ORDER")).ToList();

        data["TOTALRESULTTAKINGS"] = !orderIdsNotFinancialDone.Any()
            ? 0.0
            : _script.GetRecordset("C_RESULTAATNEMING", "AMOUNT", $"FK_ORDER IN ({string.Join(",", orderIdsNotFinancialDone)})", "")
            .DataTable.AsEnumerable().Sum(x => x.Field<double>("AMOUNT"));


        return true;
    }

    public static class OfferWfStates
    {
        //  var wfStateSubmitted = new Guid("5b0811e8-171f-49fd-a4bf-8bbd4a088421");
        public static Guid SubMitted = new Guid("5b0811e8-171f-49fd-a4bf-8bbd4a088421");
        public static Guid New = new Guid("f8751fe5-2c2c-491b-b1b7-60ae603e487d");
        public static Guid Gefiatteerd = new Guid("1daafbf0-d541-46e5-b91e-35bcd3790cd2");
        public static Guid TeFiatteren = new Guid("1b5d76cc-c089-42f7-bfde-3ab3503ccbac");
    }
}
