﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Communication.Script;

public class R_SALESINVOICEDETAILASSEMBLY_Save_Custom : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();
 

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        // RK
        // 14-12-2023
        if (!CopySalesInvoiceRelationFromOrderToSalesInvoice(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        // HVE
        // 30-4-2020
        // Indien aanwezig neem Alternative Reports over van order
        if (!SetValuesFromOrder(data, oldData, saveType, ref reason))
        {
            return false;
        }
        
        return true;
    }

    private bool CopySalesInvoiceRelationFromOrderToSalesInvoice(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
            return true;

        var orderId = data["FK_ORDER"] as int? ?? 0;
        if (orderId == 0)
            return true;

        var order = _script.GetRecordset("R_ORDER", "FK_SALESINVOICERELATION", $"PK_R_ORDER = {orderId}", "").DataTable.AsEnumerable().First();

        if (order["FK_SALESINVOICERELATION"] == DBNull.Value || order.Field<int>("FK_SALESINVOICERELATION") == 0)
            return true;

        var orderSalesRelationId = order.Field<int>("FK_SALESINVOICERELATION");

        var salesInvoiceId = data["FK_SALESINVOICE"];
        var rsSalesInvoice = _script.GetRecordset("R_SALESINVOICE", "FK_INVOICERELATION", $"PK_R_SALESINVOICE = {salesInvoiceId}", "");

        // Checken of relatie al ingevuld is
        if ((int)rsSalesInvoice.GetField("FK_INVOICERELATION").Value == orderSalesRelationId)
            return true;

        rsSalesInvoice.MoveFirst();
        rsSalesInvoice.SetFieldValue("FK_INVOICERELATION", orderSalesRelationId);

        var result = rsSalesInvoice.Update2();

        if (result.Any(x => x.HasError))
        {
            reason = result.First(x => x.HasError).GetResult();
            return false;
        }

        return true;
    }

    private bool SetValuesFromOrder(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true;
        }
        var orderId = data["FK_ORDER"] as int? ?? 0;

        if (orderId == 0)
        {
            return true;
        }

        var rsOrder = _script.GetRecordset("R_ORDER", "FK_ALTERNATIVEREPORTLAYOUT, FK_DIVERGENTTRADINGNAME", $"PK_R_ORDER = {orderId}", "")
            .DataTable
            .AsEnumerable();

        var alternativeReportId = rsOrder
            .Select(x => x.Field<int?>("FK_ALTERNATIVEREPORTLAYOUT") ?? 0)
            .FirstOrDefault();
        var divergentTradingNameId = rsOrder
            .Select(x => x.Field<int?>("FK_DIVERGENTTRADINGNAME") ?? 0)
            .FirstOrDefault();

        if (alternativeReportId == 0 && divergentTradingNameId == 0)
        {
            return true;
        }

        var salesInvoiceId = (int)data["FK_SALESINVOICE"];
        var rsSalesInvoice = _script.GetRecordset("R_SALESINVOICE", "OMGEZET, FK_ALTERNATIVEREPORTLAYOUT, FK_DIVERGENTTRADINGNAME", $"PK_R_SALESINVOICE = {salesInvoiceId}", "");
        rsSalesInvoice.MoveFirst();

        if (!(bool)rsSalesInvoice.Fields["OMGEZET"].Value)
        {

            if (alternativeReportId == 0)
            {
                rsSalesInvoice.SetFieldValue("FK_ALTERNATIVEREPORTLAYOUT", DBNull.Value);
            }
            else
            {
                rsSalesInvoice.SetFieldValue("FK_ALTERNATIVEREPORTLAYOUT", alternativeReportId);
            }

            if (divergentTradingNameId == 0)
            {
                rsSalesInvoice.SetFieldValue("FK_DIVERGENTTRADINGNAME", DBNull.Value);
            }
            else
            {
                rsSalesInvoice.SetFieldValue("FK_DIVERGENTTRADINGNAME", divergentTradingNameId);
            }

            rsSalesInvoice.SetFieldValue("OMGEZET", true);
            rsSalesInvoice.Update();
        }

        return true;
    }

}
