﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Communication.Script;
using Ridder.Recordset.Extensions;


public class R_TODO_MaakSupportTicketTaak_Save_User : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {

        //DB
        //4-3-2025
        //Maak of update support ticket taak

        if(!_script.GetUserInfo().DatabaseName.Equals("Bisharp"))
        {
            return true;
        }

        if(!(data["FK_EMPLOYEE"] as int?).HasValue)
        {
            return true;
        }

        if (saveType == SaveType.AfterInsertSave || saveType == SaveType.AfterUpdateSave && oldData["FK_EMPLOYEE"].ToString().Equals(data["FK_EMPLOYEE"].ToString()))
        {
            return true; //Werknemer wordt niet gewijzigd
        }

        var rsTodo = _script.GetRecordset("R_TODO", "", $"FK_ACTIONSCOPE = {data["PK_R_ACTIONSCOPE"]} AND ISSUPPORTTICKETTODO = 1", "");

        if (rsTodo.RecordCount == 0)
        {
            //Maak nieuwe taak aan

            rsTodo.UseDataChanges = true;

            rsTodo.AddNew();

            rsTodo.SetFieldValue("FK_ACTIONSCOPE", data["PK_R_ACTIONSCOPE"]);
            rsTodo.SetFieldValue("FK_RELATION", data["FK_RELATION"]);
            rsTodo.SetFieldValue("FK_TODOTYPE", 46);

            if ((data["FK_BISHARPPORTALUSER"] as int?).HasValue)
            {
                var contactId = _script.GetRecordset("U_BISHARPPORTALUSER", "FK_CONTACT", $"PK_U_BISHARPPORTALUSER = {data["FK_BISHARPPORTALUSER"]}", "")
                    .DataTable.AsEnumerable().First().Field<int?>("FK_CONTACT");

                if(contactId.HasValue)
                    rsTodo.SetFieldValue("FK_CONTACT", contactId);
            }

            rsTodo.SetFieldValue("DESCRIPTION", data["DESCRIPTION"].ToString());
            rsTodo.SetFieldValue("FK_ASSIGNEDTO", data["FK_EMPLOYEE"]);
            rsTodo.SetFieldValue("DUEDATE", DateTime.Now.AddDays(1));


            var memo = $"Memo support ticket:\n\n" + data["MEMO"].ToString();

            rsTodo.SetFieldValue("INTERNALMEMO", memo);
            rsTodo.SetFieldValue("ISSUPPORTTICKETTODO", true);

            rsTodo.Update();
        }
        else
        {
            rsTodo.MoveFirst();
            rsTodo.SetFieldValue("FK_ASSIGNEDTO", data["FK_EMPLOYEE"]);
            rsTodo.Update();
        }


        return true;
    }
}
