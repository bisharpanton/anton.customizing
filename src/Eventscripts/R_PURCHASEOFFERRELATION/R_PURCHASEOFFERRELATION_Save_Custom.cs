﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;

public class R_PURCHASEOFFERRELATION_Save_Custom : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        // HVE
        // 30-11-2021
        // Determine Default Divergent Trading Name
        if (!SetDefaultDivergentTradingName(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }


    private bool SetDefaultDivergentTradingName(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if ((bool)data["MANUALCREATED"])
        {
            return true; // handmatig aangemaakt
        }

        if (saveType == SaveType.AfterUpdateSave && (int)oldData["FK_SUPPLIER"] == (int)data["FK_SUPPLIER"])
        {
            return true; // geen wijziging
        }

        var relationId = (int)data["FK_SUPPLIER"];

        var tradingNameId = GetTradingNameId(relationId);
        if (tradingNameId == 0)
        {
            data["FK_DIVERGENTTRADINGNAME"] = DBNull.Value;
        }
        else
        {
            data["FK_DIVERGENTTRADINGNAME"] = tradingNameId;
        }

        return true;
    }

    private int GetTradingNameId(int relationId)
    {
        return _script.GetRecordset("C_DIVERGENTTRADINGNAMES", "",
                $"FK_RELATION = {relationId} AND DEFAULTTRADINGNAME = TRUE", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int?>("PK_C_DIVERGENTTRADINGNAMES") ?? 0)
            .FirstOrDefault();
    }
}
