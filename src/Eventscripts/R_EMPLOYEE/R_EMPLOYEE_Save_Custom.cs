﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;

public class R_EMPLOYEE_Save_Custom : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //12-05-2021
        //Het vinkje 'Niet in bedrijfsdeclarabiliteit' wordt in rapportages/filters/dashboards/Appreo gebruikt 
        //om indirecte AIO werknemers te filteren. Dit vinkje is echter niet zo netjes. Nu we ook ZZP'ers gaan registeren
        //als werknemer maken we een Werknemer categorie aan. Als deze op 'Algemeen' staat moet het vinkje 
        //'Niet in bedrijfsdeclarabliteit' uit staat en bij Indirect/ZZP/Uitzendkracht moet deze aan staan.

        if(!UpdateBoolNietInBedrijfsdeclarabliteit(data, oldData, saveType, ref reason))
        {
            return false;
        }


        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }

    private bool UpdateBoolNietInBedrijfsdeclarabliteit(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if(saveType == SaveType.AfterUpdateSave && oldData["EMPLOYEECATEGORY"].ToString().Equals(data["EMPLOYEECATEGORY"].ToString()))
        {
            //Werknemer categorie wordt niet gewijzigd
            return true; 
        }

        data["NIETDECLARABEL"] = (EmployeeCategory)data["EMPLOYEECATEGORY"] != EmployeeCategory.Algemeen && (EmployeeCategory)data["EMPLOYEECATEGORY"] != EmployeeCategory.Oproepkracht;        

        return true;
    }

    enum EmployeeCategory
    {
        Algemeen = 1,
        Indirect = 2,
        IndirectAIO = 3,
        ZZP = 4,
        Uitzendkracht = 5,
        Oproepkracht = 6
    }
}
