﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;

public class R_PURCHASEINVOICEDETAILMISC_Save_Custom : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (!DetermineWkaState(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    private bool DetermineWkaState(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true; //Alleen doorgaan bij aanmaken inkoopfactuur
        }

        var invoiceId = (int)data["FK_PURCHASEINVOICE"];
        var rsInvoice = _script.GetRecordset("R_PURCHASEINVOICE", "", $"PK_R_PURCHASEINVOICE = {invoiceId}", "");
        var invoice = rsInvoice
            .DataTable
            .AsEnumerable()
            .First();

        var currentWKAState = invoice.Field<int>("WKASTATE");
        if (currentWKAState == (int)WkaState.NogTeControleren)
        {
            return true;
        }

        var column = "MISC";
        var purchaseOrderDetailId = data[$"FK_PURCHASEORDERDETAIL{column}"] as int? ?? 0;
        if (purchaseOrderDetailId == 0)
        {
            return true;
        }
        var wka = GetWKAfromPurchaseorder(column, purchaseOrderDetailId);
        if (!wka)
        {
            return true;
        }

        rsInvoice.MoveFirst();
        rsInvoice.SetFieldValue("WKASTATE", (int)WkaState.NogTeControleren);
        var updateResult = rsInvoice.Update2();
        if (updateResult != null && updateResult.Any(x => x.HasError))
        {
            reason =
                $"Het opslaan is mislukt, oorzaak: " +
                $"{string.Join("\r\n", updateResult.Where(y => y.HasError).Select(x => x.GetResult()))}";
            return false;
        }


        return true;

    }

    private bool GetWKAfromPurchaseorder(string column, int purchaseOrderDetailId)
    {
        var purchaseOrderId = _script.GetRecordset($"R_PURCHASEORDERDETAIL{column}", "FK_PURCHASEORDER",
                $"PK_R_PURCHASEORDERDETAIL{column} = {purchaseOrderDetailId}", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int>("FK_PURCHASEORDER"))
            .FirstOrDefault();

        return _script.GetRecordset("R_PURCHASEORDER", "WKA",
                $"PK_R_PURCHASEORDER = {purchaseOrderId}", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<bool>("WKA"))
            .FirstOrDefault();
    }


    private enum WkaState : int
    {
        Nvt = 1,
        NogTeControleren = 2,
        Akkoord = 3,
        NietAkkoord = 4
    }
}
