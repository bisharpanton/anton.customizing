﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Communication.Script;
using Ridder.Recordset.Extensions;


public class R_SALESINVOICEDETAILSERVICECONTRACT_Save_User : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //23-1-2025
        //Kopieer facturatie memo

        if(saveType == SaveType.AfterInsertSave && (data["FK_SERVICECONTRACTDETAIL"] as int?).HasValue)
        {
            data["MEMO"] = _script.GetRecordset("R_SERVICECONTRACTDETAIL", "INVOICEMEMO",
                $"PK_R_SERVICECONTRACTDETAIL = {data["FK_SERVICECONTRACTDETAIL"]}", "").DataTable.AsEnumerable().First().Field<string>("INVOICEMEMO");
        }


        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }
}
