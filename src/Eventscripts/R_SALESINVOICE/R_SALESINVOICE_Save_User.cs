﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;

public class R_SALESINVOICE_Save_User : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //28-12-2021
        //Zet bisharp start- en eind periode op huidige maand

        if (oldData == null)
        {
            data["BISHARPCONTRACTSTARTPERIOD"] = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            data["BISHARPCONTRACTENDPERIOD"] = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
        }

        //DB
        //9-11-2022
        //Bepaal Anton boekingsdatum
        if (!DetermineAntonBookdate(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }

    private bool DetermineAntonBookdate(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //Alleen doorgaan bij aanmaken inkoopfactuur, als factuurdatum wijzigt of als de factuur gejournaliseerd wordt
        if (saveType == SaveType.AfterUpdateSave && ((DateTime)oldData["INVOICEDATE"]).Equals((DateTime)data["INVOICEDATE"])
            && ((bool)oldData["DUMMYTRIGGERANTONBOOKDATE"]).Equals((bool)data["DUMMYTRIGGERANTONBOOKDATE"]))
        {
            return true;
        }

        var closedMonths = _script.GetRecordset("U_ANTONCLOSEDMONTHS", "CLOSEDMONTH", "CLOSEDMONTH IS NOT NULL", "")
            .DataTable.AsEnumerable().Select(x => new
            {
                StartMonth = new DateTime(x.Field<DateTime>("CLOSEDMONTH").Year, x.Field<DateTime>("CLOSEDMONTH").Month, 1),
                EndMonth = new DateTime(x.Field<DateTime>("CLOSEDMONTH").Year, x.Field<DateTime>("CLOSEDMONTH").Month, DateTime.DaysInMonth(x.Field<DateTime>("CLOSEDMONTH").Year, x.Field<DateTime>("CLOSEDMONTH").Month))
            }).ToList();

        var antonBookDate = (DateTime)data["INVOICEDATE"];
        var availableMonthFound = false;

        while (!availableMonthFound)
        {
            var foundClosedMonth = closedMonths.FirstOrDefault(x => antonBookDate.Date >= x.StartMonth.Date && antonBookDate.Date <= x.EndMonth.Date);
            if (foundClosedMonth != null)
            {
                //Anton Boekdatum valt in afgesloten maand. Zet volgende boekdatum op eerste van de volgende maand en zoek opnieuw
                antonBookDate = foundClosedMonth.EndMonth.AddDays(1);
            }
            else
            {
                //Anton Boekdatum valt niet in afgesloten maand. 
                availableMonthFound = true;
            }
        }

        data["ANTONBOOKDATE"] = antonBookDate;

        return true;
    }
}
