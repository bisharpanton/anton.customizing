﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;

public class R_MAINPROJECT_Save_User : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //SP
        //11-8-2022
        //Bij aanmaken project, vul de tabel standaard meerwerk opslagpercentages vanuit het format
        if (!CopyFormatAdditionalWorkMarkup(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    private bool CopyFormatAdditionalWorkMarkup(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        var projectModuleActive = _script.GetRecordset("R_CRMSETTINGS", "PROJECTMODULEACTIVE",
            "", "").DataTable.AsEnumerable().First().Field<bool>("PROJECTMODULEACTIVE");

        if (!projectModuleActive)
        {
            return true;//Alleen van toepassing indien vinkje 'Projectenmodule actief' aanstaat
        }

        if (saveType == SaveType.AfterUpdateSave)
        {
            return true;//Alleen doorgaan bij eerste keer aanmaken
        }

        var additionalWorkFormats = _script.GetRecordset("U_ADDITIONALWORKMARKUPFORMAT", "SEQUENCENUMBER, DESCRIPTION, PERCENTAGE, PROJECTTEAM, NOCOSTPRICENEEDED",
               "", "")
           .DataTable.AsEnumerable().Select(x => new Format()
           {
               SequenceNumber = x.Field<int>("SEQUENCENUMBER"),
               Description = x.Field<string>("DESCRIPTION"),
               Percentage = x.Field<double>("PERCENTAGE"),
               Projectteam = x.Field<bool>("PROJECTTEAM"),
               NoCostpriceNeeded = x.Field<bool>("NOCOSTPRICENEEDED")

           }).ToList();

        if(!additionalWorkFormats.Any())
        {
            return true;//Geen format gevonden
        }

        var rsStandardAdditionalWorkMarkupProject = _script.GetRecordset("U_ADDITIONALWORKMARKUPPROJECT", "", "PK_U_ADDITIONALWORKMARKUPPROJECT = -1", "");
        rsStandardAdditionalWorkMarkupProject.MoveFirst();
        rsStandardAdditionalWorkMarkupProject.UseDataChanges = false;
        rsStandardAdditionalWorkMarkupProject.UpdateWhenMoveRecord = false;

        foreach (Format additionalWorkFormat in additionalWorkFormats)
        {
            rsStandardAdditionalWorkMarkupProject.AddNew();
            rsStandardAdditionalWorkMarkupProject.Fields["FK_MAINPROJECT"].Value = data["PK_R_MAINPROJECT"];
            rsStandardAdditionalWorkMarkupProject.Fields["SEQUENCENUMBER"].Value = additionalWorkFormat.SequenceNumber;
            rsStandardAdditionalWorkMarkupProject.Fields["DESCRIPTION"].Value = additionalWorkFormat.Description;
            rsStandardAdditionalWorkMarkupProject.Fields["PERCENTAGE"].Value = additionalWorkFormat.Percentage;
            rsStandardAdditionalWorkMarkupProject.Fields["PROJECTTEAM"].Value = additionalWorkFormat.Projectteam;
            rsStandardAdditionalWorkMarkupProject.Fields["NOCOSTPRICENEEDED"].Value = additionalWorkFormat.NoCostpriceNeeded;
        }

        var updateResult = rsStandardAdditionalWorkMarkupProject.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            reason = $"Het inladen van het format opslagpercentages meerwerk is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}";
            return false;
        }


        return true;
    }

    class Format
    {
        public int SequenceNumber { get; set; }
        public string Description { get; set; }
        public double Percentage { get; set; }
        public bool Projectteam { get; set; }
        public bool NoCostpriceNeeded { get; set; }
    }
}
