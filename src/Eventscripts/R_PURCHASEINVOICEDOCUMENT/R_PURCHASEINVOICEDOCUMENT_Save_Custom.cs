﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Communication.Script;

public class R_PURCHASEINVOICEDOCUMENT_Save_Custom : ISaveScript
{
    private ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if(!SetPurchaseInvoiceToAccordFromScanSys(data, oldData, saveType, ref reason))
        {
            return false;
        }

        
        //DB
        //13-5-2020
        //Indien er aan een declaratie inkoopfactuur een document wordt gekoppeld,
        //kopieer dit document dan door naar de declaratie taak
        if (!CopyDocumentToDeclarationTodo(data, oldData, saveType, ref reason))
        {
            return false;
        }


        return true;
    }

    private bool CopyDocumentToDeclarationTodo(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true;
        }

        var purchaseInvoiceDeclarationType = _script.GetRecordset("R_PURCHASEINVOICE", "DECLARATIONTYPE",
                $"PK_R_PURCHASEINVOICE = {data["FK_PURCHASEINVOICE"]}", "")
            .DataTable.AsEnumerable().First().Field<int>("DECLARATIONTYPE");

        if (purchaseInvoiceDeclarationType == 1) //Declaratie type 1 = Nvt
        {
            return true; //Niets doen
        }

        var declarationTodo = GetDeclarationTodo((int)data["FK_PURCHASEINVOICE"]);

        if (declarationTodo == 0)
        {
            return true;
        }

        var rsTodoDocuments = _script.GetRecordset("R_TODODOCUMENT", "",
            "PK_R_TODODOCUMENT = -1", "");
        rsTodoDocuments.AddNew();
        rsTodoDocuments.Fields["FK_TODO"].Value = declarationTodo;
        rsTodoDocuments.Fields["FK_DOCUMENT"].Value = data["FK_DOCUMENT"];
        rsTodoDocuments.Update();

        return true;
    }

    private int GetDeclarationTodo(int purchaseInvoiceId)
    {
        var rsDeclarationTodo = _script.GetRecordset("R_TODO", "PK_R_TODO",
            $"FK_PURCHASEINVOICE = {purchaseInvoiceId} AND ISDECLARATIONTODO = 1", "");

        if(rsDeclarationTodo.RecordCount == 0)
        {
            return 0; //Geen declaratie taak gevonden
        }

        rsDeclarationTodo.MoveFirst();
        return (int)rsDeclarationTodo.Fields["PK_R_TODO"].Value;
    }

    private bool SetPurchaseInvoiceToAccordFromScanSys(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        var rsInkoopfactuur = _script.GetRecordset("R_PURCHASEINVOICE",
            "PK_R_PURCHASEINVOICE, FK_INVOICEAUDITOR, AKKOORDVANUITIC, FK_WORKFLOWSTATE, PURCHASEINVOICERECEIPTSTATE",
            $"PK_R_PURCHASEINVOICE = {data["FK_PURCHASEINVOICE"]}", "");
        rsInkoopfactuur.MoveFirst();

        Guid wfStateNew = new Guid("466c4d1e-21ce-47bb-a2c6-8b96c6ec0d23");

        if ((bool)rsInkoopfactuur.Fields["AKKOORDVANUITIC"].Value && (Guid)rsInkoopfactuur.Fields["FK_WORKFLOWSTATE"].Value == wfStateNew && _script.GetUserInfo().CurrentUserName == "SCANSYS")
        {
            var rsUser = _script.GetRecordset("R_USER", "FK_EMPLOYEE",
                $"PK_R_USER = {_script.GetUserInfo().CurrentUserId}", "");
            rsUser.MoveFirst();

            rsInkoopfactuur.Fields["FK_INVOICEAUDITOR"].Value = rsUser.Fields["FK_EMPLOYEE"].Value;
            rsInkoopfactuur.Update();

            var numberOfPurchaseInvoiceDetailInstallment = _script.GetRecordset("R_PURCHASEINVOICEDETAILINSTALLMENT", "PK_R_PURCHASEINVOICEDETAILINSTALLMENT",
                $"FK_PURCHASEINVOICE = {data["FK_PURCHASEINVOICE"]}", "").RecordCount;

            if ((int)rsInkoopfactuur.GetField("PURCHASEINVOICERECEIPTSTATE").Value != 1 || numberOfPurchaseInvoiceDetailInstallment > 0)
            {
                //1 = Ontvangsten koppelen, dit is van toepassing als we in scan sys een inkoopregel hebben gekoppeld. Dan de factuur nog niet naar akkoord zetten.

                Guid wfSetToAccord = new Guid("0075174d-1a1a-4525-b8f4-06c267d803d2");

                var parameters = new Dictionary<string, object>();
                parameters.Add("ExecutedFromSetDeclarationTodoDone", false);

                var result = _script.ExecuteWorkflowEvent("R_PURCHASEINVOICE",
                    (int)rsInkoopfactuur.Fields["PK_R_PURCHASEINVOICE"].Value, wfSetToAccord, parameters);

                if (result.HasError)
                {
                    reason = $"Accorderen inkoopfactuur vanuit Scan Sys is mislukt, oorzaak: {result.GetResult()}";
                    return false;
                }
            }

        }

        return true;
    }
}
