﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;

public class C_LEVERANCIERSBEOORDELINGRELATIE_Save_User : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //SP
        //5-1-2023
        //Converteer de relatienaam naar een naam zonder speciale karakters
        if (!ConvertRelationNameToNameWithoutSpecialCharacters(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
         return true;
    }

    private bool ConvertRelationNameToNameWithoutSpecialCharacters(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true;
        }

        if (!(data["FK_RELATION"] as int?).HasValue || (int)data["FK_RELATION"] == 0)
        {
            return true;
        }

        var relationName = _script.GetRecordset("R_RELATION", "NAME",
            $"PK_R_RELATION = {data["FK_RELATION"]}", "").DataTable.AsEnumerable().First().Field<string>("NAME");

        Regex charactersToReplace = new Regex("[-(),.;:=\"\']");

        string nameWithoutSpecialCharacters = Regex.Replace($"{relationName}", $"{charactersToReplace}", "");

        data["RELATIONNAMEWITHOUTSPECIALCHARACTERS"] = nameWithoutSpecialCharacters;

        return true;
    }
}
