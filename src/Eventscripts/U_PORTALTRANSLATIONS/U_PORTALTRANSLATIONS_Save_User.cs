﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Communication.Script;
using Ridder.Recordset.Extensions;


public class U_PORTALTRANSLATIONS_Save_User : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //26-8-2024
        //Indien controller of view wijzigt, kopieer de namen zodat we de vertalingen met één query kunnen ophalen vanuit het portaal

        if (saveType == SaveType.AfterInsertSave || saveType == SaveType.AfterUpdateSave && !oldData["FK_PORTALCONTROLLER"].ToString().Equals(data["FK_PORTALCONTROLLER"].ToString()))
        {
            data["CONTROLLERNAME"] = !(data["FK_PORTALCONTROLLER"] as int?).HasValue
                ? string.Empty
                : _script.GetRecordset("U_PORTALCONTROLLER", "NAME", $"PK_U_PORTALCONTROLLER = {data["FK_PORTALCONTROLLER"]}", "")
                    .DataTable.AsEnumerable().First().Field<string>("NAME");
        }

        if (saveType == SaveType.AfterInsertSave || saveType == SaveType.AfterUpdateSave && !oldData["FK_PORTALVIEW"].ToString().Equals(data["FK_PORTALVIEW"].ToString()))
        {
            data["VIEWNAME"] = !(data["FK_PORTALVIEW"] as int?).HasValue
                ? string.Empty
                : _script.GetRecordset("U_PORTALVIEW", "NAME", $"PK_U_PORTALVIEW = {data["FK_PORTALVIEW"]}", "")
                    .DataTable.AsEnumerable().First().Field<string>("NAME");
        }

        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }
}
