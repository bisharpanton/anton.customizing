﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;

public class R_APPOINTMENTACTION_Save_User : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //4-8-2022
        //Bij het slepen van een bon uit de serviceplanning moet de omschrijving van de afspraakactie gevuld worden met gewenste informatie.

        if(!FillDescriptionAndMemoOfJoborder(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }

    private bool FillDescriptionAndMemoOfJoborder(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if(saveType == SaveType.AfterUpdateSave)
        {
            return true;
        }

        if(!(data["FK_ORDER"] as int?).HasValue || !(data["FK_JOBORDER"] as int?).HasValue)
        {
            return true;
        }

        var order = _script.GetRecordset("R_ORDER", "FK_RELATION, ORDERNUMBER, DESCRIPTION, FK_DESTINATIONADDRESS",
            $"PK_R_ORDER = {data["FK_ORDER"]}", "").DataTable.AsEnumerable().First();

        var result = $"{order.Field<int>("ORDERNUMBER")} - {order.Field<string>("DESCRIPTION")}";

        data["DESCRIPTION"] = (result.Length > 80) ? result.Substring(0, 80) : result;

        var recordLabelAddress = GetAddressRecordlabel(order.Field<int?>("FK_DESTINATIONADDRESS"));

        if(!string.IsNullOrEmpty(recordLabelAddress))
        {
            data["MEMO"] = recordLabelAddress;
        }

        return true;
    }

    private string GetAddressRecordlabel(int? relationDeliveryAddressId)
    {
        if((relationDeliveryAddressId ?? 0) == 0)
        {
            return string.Empty;
        }

        var adressId = _script.GetRecordset("R_RELATIONDELIVERYADDRESS", "FK_ADDRESS",
            $"PK_R_RELATIONDELIVERYADDRESS = {relationDeliveryAddressId}", "")
            .DataTable.AsEnumerable().First().Field<int?>("FK_ADDRESS") ?? 0;

        if(adressId == 0)
        {
            return string.Empty;
        }

        var result = string.Empty;
        try
        {
            result = _script.GetRecordTag("R_ADDRESS", adressId);
        }
        catch(Exception e)
        {
            return string.Empty;
        }

        return result;
    }
}
