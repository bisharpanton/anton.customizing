﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Communication.Script;

public class R_EMPLOYEEPRIVATEDATA_Save_Custom : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //HVE
        //23-6-2020
        //Opslaan datum wanneer foto voor het laatst gewijzigd is
        if (!SaveDatePictureChanged(data, oldData, saveType, ref reason))
        {
            return false;
        }
            
        return true;
    }


    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }


    private bool SaveDatePictureChanged(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterInsertSave && data["PICTURE"] == DBNull.Value)
        {
            return true;
        }

        if (saveType == SaveType.AfterUpdateSave)
        {
            var fieldsToCompare = new[] { "PICTURE" };
            var changed = false;
            foreach (var field in fieldsToCompare)
            {
                if (!object.Equals(oldData[field], data[field]))
                {
                    changed = true;
                    break;
                }
            }

            if (!changed)
            {
                return true;
            }
        }

        if (data["PICTURE"] != DBNull.Value)
        {
            //HV - 2021-07-13 Deze check is ingericht voor het Intranet. Deze kan niet goed omgaan met hele grote foto's
            var picture = (byte[])data["PICTURE"];
            var size = picture.Length / 1024;

            if (size > 500)
            {
                reason = $"Afbeelding is groter dan de maximale 500kb.";
                return false;

            }
        }


        data["DATEPICTURECHANGED"] = DateTime.Now;
        return true;

    }

    public bool SameByteArray(byte[] a, byte[] b)
    {
        if (a == b)
        {
            return true;
        }

        if ((a != null) && (b != null))
        {
            if (a.Length != b.Length)
            {
                return false;
            }

            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] != b[i])
                {
                    return false;
                }
            }

            return true;
        }

        return false;
    }
}

