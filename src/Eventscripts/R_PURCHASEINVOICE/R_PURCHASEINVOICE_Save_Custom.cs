﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Communication.Script;

public class R_PURCHASEINVOICE_Save_Custom : ISaveScript
{
    private ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //9-11-2022
        //Bepaal Anton boekingsdatum
        if(!DetermineAntonBookdate(data, oldData, saveType, ref reason))
        {
            return false;
        }



        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //13-5-2020
        //Maak bij een declaratie inkoopfactuur ook een declaratie taak aan
        if (!CreateDeclarationTodo(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //DB
        //13-5-2020
        //Indien de factuurcontroleur wijzigt en er is een declaratie taak, werk deze dan bij
        if (!UpdateInvoiceAuditorAtDeclarationTodo(data, oldData, saveType, ref reason))
        {
            return false;
        }


        return true;
    }

    private bool DetermineAntonBookdate(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //Alleen doorgaan bij aanmaken inkoopfactuur, als factuurdatum wijzigt of als de factuur gejournaliseerd wordt
        if(saveType == SaveType.AfterUpdateSave && ((DateTime)oldData["INVOICEDATE"]).Equals((DateTime)data["INVOICEDATE"]) 
            && ((bool)oldData["ISJOURNALIZED"]).Equals((bool)data["ISJOURNALIZED"]))
        {
            return true;
        }

        var closedMonths = _script.GetRecordset("U_ANTONCLOSEDMONTHS", "CLOSEDMONTH", "CLOSEDMONTH IS NOT NULL", "")
            .DataTable.AsEnumerable().Select(x => new
            {
                StartMonth = new DateTime(x.Field<DateTime>("CLOSEDMONTH").Year, x.Field<DateTime>("CLOSEDMONTH").Month, 1),
                EndMonth = new DateTime(x.Field<DateTime>("CLOSEDMONTH").Year, x.Field<DateTime>("CLOSEDMONTH").Month, DateTime.DaysInMonth(x.Field<DateTime>("CLOSEDMONTH").Year, x.Field<DateTime>("CLOSEDMONTH").Month))
            }).ToList();

        var antonBookDate = (DateTime)data["INVOICEDATE"];
        var availableMonthFound = false;

        while(!availableMonthFound)
        {
            var foundClosedMonth = closedMonths.FirstOrDefault(x => antonBookDate.Date >= x.StartMonth.Date && antonBookDate.Date <= x.EndMonth.Date);
            if (foundClosedMonth != null)
            {
                //Anton Boekdatum valt in afgesloten maand. Zet volgende boekdatum op eerste van de volgende maand en zoek opnieuw
                antonBookDate = foundClosedMonth.EndMonth.AddDays(1);
            }
            else
            {
                //Anton Boekdatum valt niet in afgesloten maand. 
                availableMonthFound = true;
            }
        }

        data["ANTONBOOKDATE"] = antonBookDate;

        return true;
    }

    private bool UpdateInvoiceAuditorAtDeclarationTodo(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if(saveType == SaveType.AfterInsertSave)
        {
            return true;
        }

        if((int)data["DECLARATIONTYPE"] == 1) //Declaratie type 1 = Nvt
        {
            return true;
        }

        if(string.Equals(oldData["FK_INVOICEAUDITOR"].ToString(), data["FK_INVOICEAUDITOR"].ToString()))
        {
            return true; //Factuurcontroleur wijzigt niet
        }

        if(!(data["FK_INVOICEAUDITOR"] as int?).HasValue)
        {
            return true; //Factuurcontroleur is leeg gemaakt
        }

        var rsDeclarationTodo = _script.GetRecordset("R_TODO", "FK_ASSIGNEDTO",
            $"FK_PURCHASEINVOICE = {data["PK_R_PURCHASEINVOICE"]} AND ISDECLARATIONTODO = 1", "");

        if(rsDeclarationTodo.RecordCount == 0)
        {
            return true; //Geen declaratie taak gevonden
        }

        rsDeclarationTodo.MoveFirst();
        rsDeclarationTodo.Fields["FK_ASSIGNEDTO"].Value = data["FK_INVOICEAUDITOR"];
        rsDeclarationTodo.Update();

        return true;
    }

    private bool CreateDeclarationTodo(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if(saveType == SaveType.AfterUpdateSave)
        {
            return true;
        }

        if((int)data["DECLARATIONTYPE"] == 1) //Declaratie type 1 = Nvt
        {
            return true;
        }

        if (!(data["FK_INVOICEAUDITOR"] as int?).HasValue)
        {
            return true; //Er is geen factuurcontroleur. Dan niets doen
        }

        var todoTypeDeclarationTodo = _script.GetRecordset("R_CRMSETTINGS", "FK_TODOTYPEDECLARATIONTODO", "", "")
            .DataTable.AsEnumerable()
            .Select(x => x.Field<int?>("FK_TODOTYPEDECLARATIONTODO")).First() ?? 0;


        if (todoTypeDeclarationTodo == 0)
        {
            return true;
        }

        var rsTodo = _script.GetRecordset("R_TODO", "",
            "PK_R_TODO = -1", "");
        rsTodo.UseDataChanges = true;
        
        rsTodo.AddNew();
        rsTodo.Fields["FK_PURCHASEINVOICE"].Value = data["PK_R_PURCHASEINVOICE"];
        rsTodo.Fields["FK_TODOTYPE"].Value = todoTypeDeclarationTodo;

        rsTodo.Fields["DESCRIPTION"].Value = $"Fiatteren declaratie {GetNameSupplier((int)data["FK_SUPPLIER"])} - {data["EXTERNALINVOICENUMBER"]}";

        rsTodo.Fields["FK_RELATION"].Value = data["FK_SUPPLIER"];
        rsTodo.Fields["FK_ASSIGNEDTO"].Value = data["FK_INVOICEAUDITOR"];

        rsTodo.Fields["ISDECLARATIONTODO"].Value = true;

        rsTodo.Fields["DUEDATE"].Value = DateTime.Now.DayOfWeek == DayOfWeek.Friday
            ? DateTime.Now.AddDays(3) //Zet verloopdatum naar maandag
            : DateTime.Now.AddDays(1);
            
        var updateResult = rsTodo.Update2();

        if (updateResult != null && updateResult.Any(x => x.HasError))
        {
            reason = $"Aanmaken declaratie taak is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}";
            return false;
        }

        return true;
    }

    private string GetNameSupplier(int relationId)
    {
        return _script.GetRecordset("R_RELATION", "NAME",
                $"PK_R_RELATION = {relationId}", "")
            .DataTable.AsEnumerable().First().Field<string>("NAME");
    }
}
