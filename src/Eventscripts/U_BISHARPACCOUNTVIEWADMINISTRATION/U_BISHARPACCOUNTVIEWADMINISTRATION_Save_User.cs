﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;

public class U_BISHARPACCOUNTVIEWADMINISTRATION_Save_User : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //1-7-2022
        //Vinkje 'Is actuele administratie' mag maar 1 keer aan staan per relatie

        if(!BlockSetIsActualAdministration(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }

    private bool BlockSetIsActualAdministration(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if(saveType == SaveType.AfterInsertSave && (bool)data["ISMAINADMINISTRATION"] 
            || saveType == SaveType.AfterUpdateSave && !(bool)oldData["ISMAINADMINISTRATION"] && (bool)data["ISMAINADMINISTRATION"])
        {
            var otherActuelAdministrationExists = _script.GetRecordset("U_BISHARPACCOUNTVIEWADMINISTRATION", "PK_U_BISHARPACCOUNTVIEWADMINISTRATION",
                $"FK_RELATION = {data["FK_RELATION"]} AND PK_U_BISHARPACCOUNTVIEWADMINISTRATION <> {data["PK_U_BISHARPACCOUNTVIEWADMINISTRATION"]} AND ISMAINADMINISTRATION = 1", "")
                .RecordCount > 0;

            if(otherActuelAdministrationExists)
            {
                reason = "Er is al een andere AccountView administratie ingesteld als actuele administratie bij deze relatie. Zet eerst die administratie inactief alvorens deze administratie als actuele admninistratie aan te wijzen.";
                return false;
            }
        }

        return true;
    }

}
