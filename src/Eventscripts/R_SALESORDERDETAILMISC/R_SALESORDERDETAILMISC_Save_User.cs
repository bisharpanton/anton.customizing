﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.IO;
using System.Linq;
using Ridder.Communication.Script;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;

public class R_SALESORDERDETAILMISC_Save_User : ISaveScript
{
    private ScriptHelper _script = new ScriptHelper();
    private bool _MergeFooterMemo = false;

    string offerDetailTableName = "R_OFFERDETAILMISC";
    string offerDetailPKRKey = "PK_R_OFFERDETAILMISC";
    string offerDetailFkKey = "FK_OFFERDETAILMISC";

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //10-9-2019
        //Check of er een verkoopregel wordt overgenomen van een nieuwe offerte. In dat geval; merge dan de order-memos
        if (saveType == SaveType.AfterInsertSave)
        {
            var mergeMemo = (bool)GetCrmSetting("MERGEORDERMEMO");

            if (mergeMemo)
            {
                CheckIfNewOfferIsConvertedToOrder(data[offerDetailFkKey] as int?, (int)data["FK_ORDER"]);
            }
        }

        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //SP
        //29-6-2023
        //Update bij een wijziging van het netto verkoopbedrag de invulvelden van de resultaatneming
        if (!UpdateResultLogFields(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //SP
        //20-2-2025
        //Hier aparte functie van gemaakt
        if (!CopyInfoToOrderAndOfferDetails(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //SP
        //29-3-2022
        //Bij invullen of wijzigen productgroep, vul de memovelden
        //if (!FillTextFields(data, oldData, saveType, ref reason))
        //{
        //   return false;
        //}

        return true;

    }

    private bool CopyInfoToOrderAndOfferDetails(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true;
        }

        if (!(data[offerDetailFkKey] as int?).HasValue)
        {
            return true;
        }

        //hier de gebruikersvelden van offerteregel toevoegen
        int offerid = GetOfferId((int)data[offerDetailFkKey]);

        int offerdetailId = (int)data["FK_OFFERDETAILMISC"];

        FillOrder(offerid, (int)data["FK_ORDER"]);

        FillSalesOrderDetail(data, offerdetailId);

        return true;
    }

    private bool UpdateResultLogFields(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (oldData != null && (double)oldData["NETSALESAMOUNT"] == (double)data["NETSALESAMOUNT"])
        {
            return true;//Netto verkoopbedrag wordt niet gewijzigd
        }

        var rsOrder = _script.GetRecordset("R_ORDER", "TOTALNETAMOUNT, EXPECTEDMARGINPERC, EXPECTEDMARGINAMOUNT, EXPECTEDTOTALCOSTS, FK_MAINPROJECT",
            $"PK_R_ORDER = {data["FK_ORDER"]}", "");

        var mainprojectId = rsOrder.DataTable.AsEnumerable().FirstOrDefault().Field<int?>("FK_MAINPROJECT") ?? 0;

        if (mainprojectId > 0)
        {
            return true;//Indien er een project is gekoppeld worden de velden hier vandaan gevuld.
        }

        var totalNetAmount = _script.GetRecordset("R_SALESORDERALLDETAIL", "NETSALESAMOUNT",
            $"FK_ORDER = {data["FK_ORDER"]} AND SOURCE_TABLENAME <> 'R_SALESORDERDETAILTEXT'", "").DataTable.AsEnumerable().Sum(x => x.Field<double>("NETSALESAMOUNT"));

        var marginPerc = rsOrder.DataTable.AsEnumerable().First().Field<double>("EXPECTEDMARGINPERC");

        var margin = totalNetAmount * marginPerc;
        var totalExpectedCosts = totalNetAmount - margin;

        rsOrder.MoveFirst();
        rsOrder.SetFieldValue("EXPECTEDMARGINAMOUNT", margin);
        rsOrder.SetFieldValue("EXPECTEDTOTALCOSTS", totalExpectedCosts);

        var updateResult = rsOrder.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            reason = $"Updaten van kolommen t.b.v. resultaatneming mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}";
            return false;
        }

        return true;

    }

    private bool FillTextFields(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if ((data["FK_PRODUCTGROUP"] as int? ?? 0) == 0)
        {
            return true;//productgroep niet gevuld
        }

        if (saveType == SaveType.AfterUpdateSave && oldData["FK_PRODUCTGROUP"] == data["FK_PRODUCTGROUP"])
        {
            return true; //Alleen doorgaan bij aanmaken of wijzigen productgroep
        }

        var rsOffer = _script.GetRecordset("R_OFFER",
            "HEADERMEMO, FOOTERMEMO, NIETINVOORSTELMEEGENOMEN, MEMOALGEMENEVOORWAARDEN",
            $"PK_R_OFFER = {data["FK_OFFER"]}", "");
        rsOffer.MoveFirst();

        var productGroups = _script.GetRecordset("R_OFFERDETAILMISC", "FK_PRODUCTGROUP",
            $"FK_OFFER = {data["FK_OFFER"]}", "").DataTable.AsEnumerable().Select(x => x.Field<int>("FK_PRODUCTGROUP")).Distinct().ToList();

        if (!productGroups.Any())
        {
            return true;
        }

        string headertext = "";
        //string generaltext = "";
        //string nietmeegenomentext = "";

        foreach (var productgroup in productGroups)
        {
            var productgroupTexts = _script.GetRecordset("U_PRODUCTGROUP", "FK_HEADER, FK_GENERAL, FK_NIETINONSVOORSTELMEEGENOMEN",
                $"PK_R_PRODUCTGROUP = {productgroup}", "").DataTable.AsEnumerable();

            var headerId = productgroupTexts.First().Field<int?>("FK_HEADER");

            if (headerId.HasValue)
            {
                var rsTekstblok = _script.GetRecordset("R_TEXTSNIPPET", "SNIPPET",
                    $"PK_R_TEXTSNIPPET = {headerId}", "");
                if (rsTekstblok.RecordCount > 0)
                {
                    rsTekstblok.MoveFirst();
                    var header = (string)rsTekstblok.Fields["SNIPPET"].Value;
                    headertext += _script.RtfOperations.MergeRtf(
                        headertext,
                        header);
                }
            }

            rsOffer.Fields["HEADERMEMO"].Value = headertext;

            //var generalId = productgroupTexts.First().Field<int?>("FK_GENERAL");

            //var nietmeegenomenId = productgroupTexts.First().Field<int?>("FK_NIETINONSVOORSTELMEEGENOMEN");

        }


        return true;
    }

    private void CheckIfNewOfferIsConvertedToOrder(int? offerDetail, int order)
    {
        if (!offerDetail.HasValue)
        {
            return;
        }

        var currentOfferId = GetOfferId(offerDetail.Value);
        var existingOfferIds = new List<int>();

        var sources = new string[] { "ASSEMBLY", "ITEM", "MISC" };

        foreach (var source in sources)
        {
            var offerDetails = _script.GetRecordset($"R_SALESORDERDETAIL{source}", $"FK_OFFERDETAIL{source}",
                    $"FK_ORDER = {order} AND FK_OFFERDETAIL{source} IS NOT NULL", "").DataTable.AsEnumerable()
                .Select(x => x.Field<int>($"FK_OFFERDETAIL{source}"))
                .ToList();

            if (!offerDetails.Any())
            {
                continue;
            }

            existingOfferIds.AddRange(_script.GetRecordset($"R_OFFERDETAIL{source}", "FK_OFFER",
                    $"PK_R_OFFERDETAIL{source} IN ({string.Join(",", offerDetails)})", "")
                .DataTable.AsEnumerable().Select(x => x.Field<int>("FK_OFFER")).ToList());
        }

        if (existingOfferIds.Count > 0 && !existingOfferIds.Contains(currentOfferId))
        {
            _MergeFooterMemo = true;
        }
    }
    public void FillSalesOrderDetail(RowData data, int offerdetailId)
    {
        var rsOfferdetail = _script.GetRecordset("R_OFFERDETAILMISC", "FK_COREBUSINESS, FK_PRODUCTGROUP",
            $"PK_R_OFFERDETAILMISC = {offerdetailId}", "");
        rsOfferdetail.MoveFirst();

        if ((rsOfferdetail.Fields["FK_COREBUSINESS"].Value as int?).HasValue)
        {
            data["FK_COREBUSINESS"] = rsOfferdetail.Fields["FK_COREBUSINESS"].Value;
        }

        if ((rsOfferdetail.Fields["FK_PRODUCTGROUP"].Value as int?).HasValue)
        {
            data["FK_PRODUCTGROUP"] = rsOfferdetail.Fields["FK_PRODUCTGROUP"].Value;
        }
    }

    public void FillOrder(int PkrOffer, int PkrOrder)
    {
        var rsOffer = _script.GetRecordset("R_OFFER",
            "EXCHANGEFROMSERVICE, FK_COREBUSINESS, FK_PRODUCTGROUP, FK_INDUSTRY, FOOTERMEMO, PLAINTEXT_FOOTERMEMO, NIETINVOORSTELMEEGENOMEN, PLAINTEXT_NIETINVOORSTELMEEGENOMEN, MEMOALGEMENEVOORWAARDEN, PLAINTEXT_MEMOALGEMENEVOORWAARDEN, VGPLAN, FK_UITGANGSNORM, EXECUTIEKLASSE, CORROSIEKLASSE, VOORBEWERKINGSGRAAD, FK_ALTERNATIVEREPORTLAYOUT, FK_DIVERGENTTRADINGNAME, AWARDBASEDONCO2PERFORMANCELADDER",
            $"PK_R_OFFER = {PkrOffer}", "");
        rsOffer.MoveFirst();

        var rsOrder = _script.GetRecordset("R_ORDER",
            "OMGEZET, EXCHANGEFROMSERVICE, FK_COREBUSINESS, FK_PRODUCTGROUP, FK_INDUSTRY, FOOTERMEMO, PLAINTEXT_FOOTERMEMO, NIETINVOORSTELMEEGENOMEN, PLAINTEXT_NIETINVOORSTELMEEGENOMEN, MEMOALGEMENEVOORWAARDEN, PLAINTEXT_MEMOALGEMENEVOORWAARDEN, VGPLAN, FK_UITGANGSNORM, EXECUTIEKLASSE, CORROSIEKLASSE, VOORBEWERKINGSGRAAD, FK_ALTERNATIVEREPORTLAYOUT, FK_DIVERGENTTRADINGNAME, AWARDBASEDONCO2PERFORMANCELADDER",
            $"PK_R_ORDER = {PkrOrder}", "");
        rsOrder.MoveFirst();

        if (!(bool)rsOrder.Fields["OMGEZET"].Value)
        {
            rsOrder.Fields["FK_INDUSTRY"].Value = rsOffer.Fields["FK_INDUSTRY"].Value;
            rsOrder.Fields["VGPLAN"].Value = rsOffer.Fields["VGPLAN"].Value;

            //NEN-info
            rsOrder.Fields["FK_UITGANGSNORM"].Value = rsOffer.Fields["FK_UITGANGSNORM"].Value;
            rsOrder.Fields["EXECUTIEKLASSE"].Value = rsOffer.Fields["EXECUTIEKLASSE"].Value;
            rsOrder.Fields["CORROSIEKLASSE"].Value = rsOffer.Fields["CORROSIEKLASSE"].Value;
            rsOrder.Fields["VOORBEWERKINGSGRAAD"].Value = rsOffer.Fields["VOORBEWERKINGSGRAAD"].Value;

            rsOrder.Fields["MEMOALGEMENEVOORWAARDEN"].Value = rsOffer.Fields["MEMOALGEMENEVOORWAARDEN"].Value;
            rsOrder.Fields["NIETINVOORSTELMEEGENOMEN"].Value = rsOffer.Fields["NIETINVOORSTELMEEGENOMEN"].Value;

            rsOrder.Fields["EXCHANGEFROMSERVICE"].Value = rsOffer.Fields["EXCHANGEFROMSERVICE"].Value;

            rsOrder.Fields["AWARDBASEDONCO2PERFORMANCELADDER"].Value = rsOffer.Fields["AWARDBASEDONCO2PERFORMANCELADDER"].Value;

            //Bij LBR is de projectleider altijd Tim Ursem
            if (_script.GetUserInfo().DatabaseName == "Loos Betonreparaties")
            {
                rsOrder.Fields["FK_PLANNER"].Value = 150;
            }

            //DB
            //6-5-2015
            KopieerOfferteDocumentenNaarOrdermap(PkrOffer, PkrOrder);


            // HVE
            // 30-4-2020
            // alternative report lay-out overnemen indien gevuld
            if ((rsOffer.Fields["FK_ALTERNATIVEREPORTLAYOUT"].Value as int? ?? 0) > 0)
            {
                rsOrder.Fields["FK_ALTERNATIVEREPORTLAYOUT"].Value = rsOffer.Fields["FK_ALTERNATIVEREPORTLAYOUT"].Value;
            }

            // HVE
            // 7-7-2020
            // Handelsnaam overnemen van offerte
            if ((rsOffer.Fields["FK_DIVERGENTTRADINGNAME"].Value as int? ?? 0) == 0)
            {
                rsOrder.Fields["FK_DIVERGENTTRADINGNAME"].Value = DBNull.Value;
            }
            else
            {
                rsOrder.Fields["FK_DIVERGENTTRADINGNAME"].Value = rsOffer.Fields["FK_DIVERGENTTRADINGNAME"].Value;
            }

            //SP
            //29-3-2022
            //Bij DLP maken we gebruik van kernactiviteiten op offerte/order niveau. Wanneer we van een offerte een order maken moet deze worden overgenomen
            //DB - 25-1-2024 - Toevoeging gemaakt
            if ((rsOffer.Fields["FK_COREBUSINESS"].Value as int?).HasValue)
            {
                rsOrder.Fields["FK_COREBUSINESS"].Value = rsOffer.Fields["FK_COREBUSINESS"].Value;
            }

            if ((rsOffer.Fields["FK_PRODUCTGROUP"].Value as int?).HasValue)
            {
                rsOrder.Fields["FK_PRODUCTGROUP"].Value = rsOffer.Fields["FK_PRODUCTGROUP"].Value;
            }
        }

        //Footermemo = 'In ons voorstel opgenomen' - Merge indien meerdere offertes tot één order worden omgezet
        if (_MergeFooterMemo)
        {
            rsOrder.Fields["FOOTERMEMO"].Value = _script.RtfOperations.MergeRtf(
                rsOrder.Fields["FOOTERMEMO"].Value.ToString(),
                rsOffer.Fields["FOOTERMEMO"].Value.ToString());

            //DB
            //6-5-2015
            KopieerOfferteDocumentenNaarOrdermap(PkrOffer, PkrOrder);
        }

        rsOrder.Fields["OMGEZET"].Value = 1;

        rsOrder.Update();
    }

    public int GetOfferId(int PkrOfferDetail)
    {
        return _script.GetRecordset(offerDetailTableName, "FK_OFFER",
                $"{offerDetailPKRKey} = {PkrOfferDetail}", "")
            .DataTable.AsEnumerable().First().Field<int>("FK_OFFER");
    }

    public void KopieerOfferteDocumentenNaarOrdermap(int PkrOffer, int PkrOrder)
    {
        DataSet dtOffer = ScriptRecordset.GetDataSet("R_OFFER", "OFFERNUMBER", "PK_R_OFFER = " + PkrOffer, "");
        ScriptRecordset rsOffer = new ScriptRecordset(dtOffer);
        rsOffer.MoveFirst();

        DataSet dtOrder = ScriptRecordset.GetDataSet("R_ORDER", "ORDERNUMBER", "PK_R_ORDER = " + PkrOrder, "");
        ScriptRecordset rsOrder = new ScriptRecordset(dtOrder);
        rsOrder.MoveFirst();

        string offerLocation =
            $"{GetCrmSetting("OFFERLOCATION").ToString()}\\{rsOffer.Fields["OFFERNUMBER"].Value.ToString()}";

        if (Directory.Exists(offerLocation))
        {
            string orderLocation =
                $"{GetCrmSetting("ORDERLOCATION").ToString()}\\{rsOrder.Fields["ORDERNUMBER"].Value.ToString()}";

            if (!Directory.Exists(orderLocation))
            {
                Directory.CreateDirectory(orderLocation);
            }

            string targetDir = @"" + orderLocation;

            IShellLink link = (IShellLink)new ShellLink();         // setup shortcut information
            link.SetDescription("My Description");
            link.SetPath($"{offerLocation}");         // save it
            IPersistFile file = (IPersistFile)link;
            //string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            file.Save(Path.Combine($"{targetDir}", $"Offerte_{rsOffer.Fields["OFFERNUMBER"].Value.ToString()}.lnk"), false);

            /*Directory.CreateDirectory(targetDir);

            DirectoryInfo sourceLocation = new DirectoryInfo(offerLocation);
            DirectoryInfo targetLocation = new DirectoryInfo(targetDir);

            CopyFolder(sourceLocation, targetLocation);*/
        }

    }

    [ComImport]
    [Guid("00021401-0000-0000-C000-000000000046")]
    internal class ShellLink
    {
    }
    [ComImport]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [Guid("000214F9-0000-0000-C000-000000000046")]
    internal interface IShellLink
    {
        void GetPath([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszFile, int cchMaxPath, out IntPtr pfd, int fFlags);
        void GetIDList(out IntPtr ppidl);
        void SetIDList(IntPtr pidl);
        void GetDescription([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszName, int cchMaxName);
        void SetDescription([MarshalAs(UnmanagedType.LPWStr)] string pszName);
        void GetWorkingDirectory([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszDir, int cchMaxPath);
        void SetWorkingDirectory([MarshalAs(UnmanagedType.LPWStr)] string pszDir);
        void GetArguments([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszArgs, int cchMaxPath);
        void SetArguments([MarshalAs(UnmanagedType.LPWStr)] string pszArgs);
        void GetHotkey(out short pwHotkey);
        void SetHotkey(short wHotkey);
        void GetShowCmd(out int piShowCmd);
        void SetShowCmd(int iShowCmd);
        void GetIconLocation([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszIconPath, int cchIconPath, out int piIcon);
        void SetIconLocation([MarshalAs(UnmanagedType.LPWStr)] string pszIconPath, int iIcon);
        void SetRelativePath([MarshalAs(UnmanagedType.LPWStr)] string pszPathRel, int dwReserved);
        void Resolve(IntPtr hwnd, int fFlags);
        void SetPath([MarshalAs(UnmanagedType.LPWStr)] string pszFile);
    }

    public object GetCrmSetting(string columnName)
    {
        var rsCRMSettings = _script.GetRecordset("R_CRMSETTINGS", columnName, "", "");
        rsCRMSettings.MoveFirst();

        return rsCRMSettings.Fields[columnName].Value;
    }

    public static void CopyFolder(DirectoryInfo source, DirectoryInfo target)
    {
        foreach (DirectoryInfo dir in source.GetDirectories())
        {
            var newDirectoryFullName = Path.Combine(target.FullName, dir.Name);

            if (newDirectoryFullName.Length >= 248)
            {
                continue; //Dit is een max van Windows. Sla de kopieer actie over anders komt er een foutmelding
            }

            CopyFolder(dir, target.CreateSubdirectory(dir.Name));
        }

        foreach (FileInfo file in source.GetFiles())
        {
            var newFileName = Path.Combine(target.FullName, file.Name);

            if (newFileName.Length >= 260)
            {
                continue; //Dit is een max van Windows. Sla de kopieer actie over anders komt er een foutmelding
            }

            // HVE | 2021-11-01 
            // Check if file doesnt already exist
            if (File.Exists(newFileName))
            {
                continue;
            }

            file.CopyTo(Path.Combine(target.FullName, file.Name));
        }
    }
}
