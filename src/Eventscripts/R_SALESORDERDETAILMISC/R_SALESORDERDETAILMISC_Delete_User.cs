﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;

public class R_SALESORDERDETAILMISC_Delete_User : IDeleteScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeDelete(RowData data, ref string reason)
    {
        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterDelete(RowData data, ref string reason)
    {
        //SP
        //29-6-2023
        //Update bij een wijziging van het netto verkoopbedrag de invulvelden van de resultaatneming
        if (!UpdateResultLogFields(data, ref reason))
        {
            return false;
        }
        return true;
    }

    private bool UpdateResultLogFields(RowData data, ref string reason)
    {
        var rsOrder = _script.GetRecordset("R_ORDER", "TOTALNETAMOUNT, EXPECTEDMARGINPERC, EXPECTEDMARGINAMOUNT, EXPECTEDTOTALCOSTS, FK_MAINPROJECT",
        $"PK_R_ORDER = {data["FK_ORDER"]}", "");

        var mainprojectId = rsOrder.DataTable.AsEnumerable().FirstOrDefault().Field<int?>("FK_MAINPROJECT") ?? 0;

        if (mainprojectId > 0)
        {
            return true;//Indien er een project is gekoppeld worden de velden hier vandaan gevuld.
        }

        var totalNetAmount = _script.GetRecordset("R_SALESORDERALLDETAIL", "NETSALESAMOUNT",
            $"FK_ORDER = {data["FK_ORDER"]}", "").DataTable.AsEnumerable().Sum(x => x.Field<double>("NETSALESAMOUNT"));

        var marginPerc = rsOrder.DataTable.AsEnumerable().First().Field<double>("EXPECTEDMARGINPERC");

        var margin = totalNetAmount * marginPerc;
        var totalExpectedCosts = totalNetAmount - margin;

        rsOrder.MoveFirst();
        rsOrder.SetFieldValue("EXPECTEDMARGINAMOUNT", margin);
        rsOrder.SetFieldValue("EXPECTEDTOTALCOSTS", totalExpectedCosts);

        var updateResult = rsOrder.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            reason = $"Updaten van kolommen t.b.v. resultaatneming mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}";
            return false;
        }

        return true;

    }
}
