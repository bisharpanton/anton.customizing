﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;

public class R_OFFER_Delete_Custom : IDeleteScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeDelete(RowData data, ref string reason)
    {
        //SP
        //25-1-2022
        //Verwijder koppelde documenten van taak
        if (!DeleteLinkedTodosFromOffer((int)data["PK_R_OFFER"], ref reason))
        {
            return true;
        }
        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterDelete(RowData data, ref string reason)
    {
        return true;
    }

    private bool DeleteLinkedTodosFromOffer(int offerId, ref string reason)
    {
        var rsTodosFromOffer = _script.GetRecordset("R_TODO", "",
          $"FK_OFFER = {offerId}", "");

        if (rsTodosFromOffer.RecordCount == 0)
        {
            return true;
        }

        rsTodosFromOffer.UseDataChanges = false;
        rsTodosFromOffer.UpdateWhenMoveRecord = false;

        rsTodosFromOffer.MoveFirst();
        while (!rsTodosFromOffer.EOF)
        {
            rsTodosFromOffer.Delete();
            rsTodosFromOffer.MoveNext();
        }

        rsTodosFromOffer.MoveFirst();

        var deleteResult = rsTodosFromOffer.Update2();

        if (deleteResult.Any(x => x.HasError))
        {
            reason = $"Verwijderen documenten van taak mislukt, oorzaak: {deleteResult.First(x => x.HasError).GetResult()}";
            return false;
        }

        return true;
    }
}
