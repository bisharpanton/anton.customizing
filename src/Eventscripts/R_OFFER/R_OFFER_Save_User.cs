﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.IO;
using System.Linq;
using Ridder.Communication.Script;

public class R_OFFER_Save_User : ISaveScript
{
    private ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //20-7-2015
        //Controleer of de relatie een debiteur is. Dit werkt niet via de blockade designer, daarom hier het script geplaatst. 
        if (!CheckRelationIsDebtor(data, oldData, saveType, ref reason))
        {
            return false;
        }

        
        //DB
        //17-11-2014
        //De workflow 'Wijzig relatie' gooit het veld 'Verkoper' leeg. Dit willen we niet

        if (oldData != null && ((int)oldData["FK_RELATION"] != (int)data["FK_RELATION"]))
        {
            data["FK_SALESPERSON"] = oldData["FK_SALESPERSON"];
        }


        //DB
        //20-10-2014
        //Als de calculatie wordt opgeslagen moet het calculatiescherm hierna gereset worden
        if (!ResetOldCalculationTables(data, oldData, saveType, ref reason))
        {
            return false;
        }
        
        return true;
    }


    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //01-03-2017
        //Bij aanmaken van een offerte met revisie nul en de offertemap bestaat nog niet, maak deze dan aan
        if (oldData == null && (int)data["REVISION"] == 0)
        {
            string subPath = GetCrmSetting("OFFERLOCATION") + "\\" + data["OFFERNUMBER"].ToString();
            bool IsExists = Directory.Exists(subPath);

            //Indien er al files bestaan voor dit offertenummer omdat die bv in het verleden zijn aangemaakt. Verwijder deze dan.
            if (IsExists)
            {
                clearFolder(subPath);
                Directory.Delete(subPath);
            }


            //HVE
            //20-12-2018
            //Instellingen CRM voorzien van standaard locatie
            string templateFolder = GetCrmSetting("OFFERTEMPLATELOCATION").ToString();
            bool templateFolderExists = Directory.Exists(templateFolder);
            if (templateFolderExists)
            {
                Copy(templateFolder, subPath);
            }
        }

        //DB
        //19-03-2019
        //Check of offerte vanuit taak wordt aangemaakt
        if (!LinkAutomaticTodoToThisOffer(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //DB
        //15-12-2020
        //Automatisch updaten CreditSafe info bij de relatie indien relatie nu niet in CreditSafe portfolio valt
        if (!UpdateCreditSafeInfo(data, oldData, saveType, ref reason))
        {
            //DB - 28-11-2023 - Uitgezet dat het aanmaken van offertes/orders geblokeerd wordt als de creditsafe koppeling eruit ligt. Dat merken we ook wel bij de relatie import.
            //return false;
        }


        return true;
    }

    private bool UpdateCreditSafeInfo(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true; //Alleen van toepassing bij aanmaken offerte
        }

        var relation = _script.GetRecordset("R_RELATION", "CREDITSAFEID, COCNUMBER",
            $"PK_R_RELATION = {data["FK_RELATION"]}", "").DataTable.AsEnumerable().First();

        if (string.IsNullOrEmpty(relation.Field<string>("CREDITSAFEID")))
        {
            return true; //Geen CreditSafe relatie
        }

        var relationInCreditSafePortfolio = _script.GetRecordset("C_CREDITSAFECHECKRELATIONINPORTFOLIO",
            "PK_C_CREDITSAFECHECKRELATIONINPORTFOLIO",
            $"FK_RELATION = {data["FK_RELATION"]}", "").RecordCount > 0;

        if (relationInCreditSafePortfolio)
        {
            return true;
        }

        var relationTodayCreatedOrUpdated = _script.GetRecordset("C_CREDITSAFEREQUESTS", "PK_C_CREDITSAFEREQUESTS",
            $"FK_RELATION = {data["FK_RELATION"]} AND DATECREATED >= '{DateTime.Now:yyyyMMdd}'", "").RecordCount > 0;

        if (relationTodayCreatedOrUpdated)
        {
            return true;
        }

        if (!UpdateCreditSafeInfoCore(data, oldData, saveType, ref reason, relation))
        {
            return false;
        }

        return true;
    }

    private bool UpdateCreditSafeInfoCore(RowData data, RowData oldData, SaveType saveType, ref string reason,
        DataRow relation)
    {
        var createdCreditSafeRequest =
            CreateCreditSafeRequest((int)data["FK_RELATION"], relation.Field<string>("CREDITSAFEID"), relation.Field<string>("COCNUMBER"), ref reason);

        if (createdCreditSafeRequest == 0)
        {
            return false;
        }

        if (!ProcessCreditSafeRequest(createdCreditSafeRequest, ref reason))
        {
            return false;
        }

        var creditSafeRequest = _script.GetRecordset("C_CREDITSAFEREQUESTS", "FK_RELATION, IMPORTMESSAGE, FK_WORKFLOWSTATE",
                $"PK_C_CREDITSAFEREQUESTS = {createdCreditSafeRequest}", "")
            .DataTable.AsEnumerable().First();

        var wfStateCancelled = new Guid("c30fde4d-db6a-4e8f-af2f-9b18b3c88dc0");

        if ((Guid)creditSafeRequest.Field<Guid>("FK_WORKFLOWSTATE") == wfStateCancelled)
        {
            reason = $"Updaten CreditSafe info mislukt {creditSafeRequest.Field<string>("IMPORTMESSAGE")}";
            return false;
        }

        return true;
    }

    private bool ProcessCreditSafeRequest(int createdCreditSafeRequest, ref string reason)
    {
        var wfImportRelationFromCreditSafe = new Guid("2b0d3015-d70b-4fb4-a143-2b456e060397");

        var wfResult = _script.ExecuteWorkflowEvent("C_CREDITSAFEREQUESTS", createdCreditSafeRequest,
            wfImportRelationFromCreditSafe, null);

        if (wfResult.HasError)
        {
            reason =
                $"Uitvoeren workflow 'Importeer relatie vanuit CreditSafe' mislukt, oorzaak: {wfResult.GetResult()}";

            return false;
        }

        return true;
    }

    private int CreateCreditSafeRequest(int relationId, string creditSafeId, string cocNumber, ref string reason)
    {
        var rsCreditSafeRequests = _script.GetRecordset("C_CREDITSAFEREQUESTS", "",
            "PK_C_CREDITSAFEREQUESTS = -1", "");
        rsCreditSafeRequests.UseDataChanges = false;

        rsCreditSafeRequests.AddNew();

        rsCreditSafeRequests.SetFieldValue("FK_RELATION", relationId);
        rsCreditSafeRequests.Fields["CREDITSAFEID"].Value = creditSafeId;
        rsCreditSafeRequests.Fields["COCNUMBER"].Value = cocNumber;

        rsCreditSafeRequests.SetFieldValue("ISUPDATECREDITSAFEINFO", true);

        var updateResult = rsCreditSafeRequests.Update2();

        if (updateResult != null && updateResult.Any(x => x.HasError))
        {
            reason = $"Aanmaken nieuw CreditSafe request is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}";

            return 0;
        }

        return (int)updateResult.First().PrimaryKey;
    }

    private bool LinkAutomaticTodoToThisOffer(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true;
        }

        if ((int)data["DUMMYORIGINTODO"] == 0)
        {
            return true;
        }

        var rsTodo = _script.GetRecordset("R_TODO", "FK_OFFER",
            $"PK_R_TODO = {data["DUMMYORIGINTODO"]}", "");
        rsTodo.MoveFirst();
        rsTodo.Fields["FK_OFFER"].Value = data["PK_R_OFFER"];
        rsTodo.Update();

        return true;
    }

    private bool ResetOldCalculationTables(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterInsertSave)
        {
            return true; //Niets doen bij toevoegen offerte
        }

        if (!(bool)oldData["RESETCALCULATIESCHERM"] && (bool)data["RESETCALCULATIESCHERM"])
        {
            data["CALCULATIEOMSCHRIJVING"] = string.Empty;
            data["FK_BASISCALCULATIE"] = DBNull.Value;
            data["CALCULATIETYPE"] = 1;
            data["AFRONDENCALCULATIE"] = false;
            data["RESETCALCULATIESCHERM"] = false;

            //Calculatietabel Artikel resetten
            DataSet dsCalculatieTabelArtikel = ScriptRecordset.GetDataSet("U_CALCULATIETABELARTIKEL", "PK_U_CALCULATIETABELARTIKEL", "FK_VERKOOPOFFERTE = " + data["PK_R_OFFER"], "");
            ScriptRecordset rsCalculatieTabelArtikel = new ScriptRecordset(dsCalculatieTabelArtikel);
            rsCalculatieTabelArtikel.MoveFirst();
            
            while (rsCalculatieTabelArtikel.RecordCount > 0 && !rsCalculatieTabelArtikel.EOF)
            {
                rsCalculatieTabelArtikel.Delete();
                rsCalculatieTabelArtikel.MoveFirst();
            }

            //Calculatietabel Bewerking resetten
            DataSet dsCalculatieTabelBewerking = ScriptRecordset.GetDataSet("U_CALCULATIETABELBEWERKING", "PK_U_CALCULATIETABELBEWERKING", "FK_VERKOOPOFFERTE = " + data["PK_R_OFFER"], "");
            ScriptRecordset rsCalculatieTabelBewerking = new ScriptRecordset(dsCalculatieTabelBewerking);
            rsCalculatieTabelBewerking.MoveFirst();

            while (rsCalculatieTabelBewerking.RecordCount > 0 && !rsCalculatieTabelBewerking.EOF)
            {
                rsCalculatieTabelBewerking.Delete();
                rsCalculatieTabelBewerking.MoveFirst();
            }

            //Calculatietabel UBW resetten
            DataSet dsCalculatieTabelUBW = ScriptRecordset.GetDataSet("U_CALCULATIETABELUBW", "PK_U_CALCULATIETABELUBW", "FK_VERKOOPOFFERTE = " + data["PK_R_OFFER"], "");
            ScriptRecordset rsCalculatieTabelUBW = new ScriptRecordset(dsCalculatieTabelUBW);
            rsCalculatieTabelUBW.MoveFirst();

            while (rsCalculatieTabelUBW.RecordCount > 0 && !rsCalculatieTabelUBW.EOF)
            {
                rsCalculatieTabelUBW.Delete();
                rsCalculatieTabelUBW.MoveFirst();
            }
        }

        return true;
    }

    private bool CheckRelationIsDebtor(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave && (int)oldData["FK_RELATION"] == (int)data["FK_RELATION"])
        {
            return true; //Niets doen indien relatie niet wordt gewijzigd
        }

        var checkDebtorAtOffer = (bool)GetCrmSetting("CHECKDEBTORATOFFER");

        if (!checkDebtorAtOffer)
        {
            return true;
        }

        if (_script.GetRecordset("R_DEBTOR", "PK_R_DEBTOR", $"FK_RELATION = {data["FK_RELATION"]}", "")
                .RecordCount == 0)
        {
            reason = "Relatie is geen debiteur.";
            return false;
        }

        return true;
    }
    
    public void clearFolder(string FolderName)
    {
        DirectoryInfo dir = new DirectoryInfo(FolderName);

        foreach (FileInfo fi in dir.GetFiles())
        {
            fi.Delete();
        }

        foreach (DirectoryInfo di in dir.GetDirectories())
        {
            clearFolder(di.FullName);
            di.Delete();
        }
    }

    void Copy(string sourceDir, string targetDir)
    {
        if (!Directory.Exists(sourceDir))
        {
            return;
        }

        Directory.CreateDirectory(targetDir);

        foreach (var file in Directory.GetFiles(sourceDir))
            File.Copy(file, Path.Combine(targetDir, Path.GetFileName(file)));

        foreach (var directory in Directory.GetDirectories(sourceDir))
            Copy(directory, Path.Combine(targetDir, Path.GetFileName(directory)));
    }


    public object GetCrmSetting(string s)
    {
        DataSet dsCRMSettings = ScriptRecordset.GetDataSet("R_CRMSETTINGS", s, "", "");
        ScriptRecordset rsCRMSettings = new ScriptRecordset(dsCRMSettings);
        rsCRMSettings.MoveFirst();

        return rsCRMSettings.Fields[s].Value;
    }
}
