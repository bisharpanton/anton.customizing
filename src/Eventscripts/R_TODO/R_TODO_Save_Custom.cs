﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.IO;
using System.Linq;
using Ridder.Client.SDK.SDKParameters;
using Ridder.Communication.Script;

public class R_TODO_Save_Custom : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //1-4-2020
        //Indien er een nabeltaak voor een verkoopfactuur wordt gemaakt willen het factuurdocument gekoppeld hebben.
        //Dan kan in de Appreo app het factuur document worden mee gegeven

        if (!LinkSalesInvoiceDocument(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    private bool LinkSalesInvoiceDocument(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true; //Alleen van toepassing bij aanmaken taak. Wijzigen van factuur ondervangen we niet
        }

        var salesInvoiceId = data["FK_SALESINVOICE"] as int? ?? 0;

        if (salesInvoiceId == 0)
        {
            return true;
        }

        var salesInvoice = _script.GetRecordset("R_SALESINVOICE", "FK_JOURNALENTRY, INVOICEDATE",
            $"PK_R_SALESINVOICE = {salesInvoiceId}", "").DataTable.AsEnumerable().First();

        if (!salesInvoice.Field<int?>("FK_JOURNALENTRY").HasValue)
        {
            reason = "Verkoopfactuur is nog niet verzonden. Aanmaken nabel-taak nog niet mogelijk.";
            return false;
        }

        var archiveLocation = GetArchiveLocation(salesInvoiceId, salesInvoice.Field<DateTime>("INVOICEDATE"),
            salesInvoice.Field<int>("FK_JOURNALENTRY"));

        if (archiveLocation == "")
        {
            return true;
        }

        if (!ExportAndLinkSalesInvoiceReport((int)data["PK_R_TODO"], salesInvoiceId, archiveLocation, ref reason))
        {
            return false;
        }

        if (!CopySalesInvoiceDocuments((int)data["PK_R_TODO"], salesInvoiceId, ref reason))
        {
            return false;
        }

        return true;
    }

    private bool CopySalesInvoiceDocuments(int todoId, int salesInvoiceId, ref string reason)
    {
        var salesInvoiceDocuments = _script.GetRecordset("R_SALESINVOICEDOCUMENT", "FK_DOCUMENT",
                $"FK_SALESINVOICE = {salesInvoiceId}", "").DataTable.AsEnumerable()
            .Select(x => x.Field<int>("FK_DOCUMENT"))
            .ToList();

        if (!salesInvoiceDocuments.Any())
        {
            return true;
        }

        var rsTodoDocuments = _script.GetRecordset("R_TODODOCUMENT", "FK_TODO, FK_DOCUMENT",
            "PK_R_TODODOCUMENT = -1", "");
        rsTodoDocuments.UseDataChanges = false;
        rsTodoDocuments.UpdateWhenMoveRecord = false;

        foreach (var salesInvoiceDocument in salesInvoiceDocuments)
        {
            rsTodoDocuments.AddNew();
            rsTodoDocuments.Fields["FK_TODO"].Value = todoId;
            rsTodoDocuments.Fields["FK_DOCUMENT"].Value = salesInvoiceDocument;
        }

        rsTodoDocuments.MoveFirst();
        var updateResult = rsTodoDocuments.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            reason =
                $"Kopiëren factuurdocumenten is mislukt; oorzaak: {updateResult.First(x => x.HasError).GetResult()}";
            return false;
        }

        return true;
    }

    private bool ExportAndLinkSalesInvoiceReport(int todoId, int salesInvoiceId, string archiveLocation, ref string reason)
    {
        var systemInvoiceReport = new Guid("e59c272e-7cc6-4f07-a7f6-fafab96014a3");
        var reportColumn = new Guid("2bbd7056-7610-4350-a58a-a06eea85dad6");

        var exportReportResult = _script.ExportReport(salesInvoiceId, "R_SALESINVOICE", DesignerScope.System,
            systemInvoiceReport, reportColumn, archiveLocation, ExportType.Pdf, false);

        var addDocumentResult = _script.EventsAndActions.CRM.Actions.AddDocument(archiveLocation, "0", true,
            "R_TODO", todoId, Path.GetFileNameWithoutExtension(archiveLocation));

        if(addDocumentResult.HasError)
        {
            reason =
                $"Toevoegen verkoopfactuur bij huidige taak mislukt, oorzaak: {addDocumentResult.GetResult()}";
            return false;
        }

        return true;
    }

    private string GetArchiveLocation(int salesInvoiceId, DateTime invoiceDate, int journalEntryId)
    {
        var locationAppreoAppDocuments = _script.GetRecordset("R_CRMSETTINGS", "LOCATIONAPPREOAPPDOCUMENTS",
            "", "").DataTable.AsEnumerable().First().Field<string>("LOCATIONAPPREOAPPDOCUMENTS");

        if (string.IsNullOrEmpty(locationAppreoAppDocuments))
        {
            return "";
        }

        var archiveDirectory = Path.Combine(locationAppreoAppDocuments,
            $"Verkoopfacturen\\{invoiceDate.Year}");

        if (!Directory.Exists(archiveDirectory))
        {
            Directory.CreateDirectory(archiveDirectory);
        }

        return Path.Combine(archiveDirectory, $"Verkoopfactuur {GetJournalEntryNumber(journalEntryId)}.pdf");
    }

    private int GetJournalEntryNumber(int journalEntryId)
    {
        return _script.GetRecordset("R_JOURNALENTRY", "JOURNALENTRYNUMBER",
                $"PK_R_JOURNALENTRY = {journalEntryId}", "").DataTable.AsEnumerable().First()
            .Field<int>("JOURNALENTRYNUMBER");
    }
}
