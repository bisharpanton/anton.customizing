﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Communication.Script;

public class R_TODO_Delete_Custom : IDeleteScript
{
    private ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeDelete(RowData data, ref string reason)
    {
        //SP
        //25-1-2022
        //Verwijder koppelde documenten van taak
        if(!DeleteLinkedDocumentsFromTodo((int)data["PK_R_TODO"], ref reason))
        {
            return true;
        }
        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterDelete(RowData data, ref string reason)
    {
        //DB
        //19-2-2020
        //Onthoud welke records verwijderd worden. Dit wil de Appreo App weten.
        if (!SaveDeletedRecordIds(data, ref reason))
        {
            return false;
        }

        return true;
    }

    private bool SaveDeletedRecordIds(RowData data, ref string reason)
    {
        var rsDeletedRecords = _script.GetRecordset("C_DELETEDRECORDS", "",
            "PK_C_DELETEDRECORDS = -1", "");
        rsDeletedRecords.AddNew();
        rsDeletedRecords.Fields["TABLENAME"].Value = data.TableName;
        rsDeletedRecords.Fields["RECORDID"].Value = data[$"PK_{data.TableName}"];
        rsDeletedRecords.Update();

        return true;
    }
    private bool DeleteLinkedDocumentsFromTodo(int todoId, ref string reason)
    {
        var rsDocumentsFromTodo = _script.GetRecordset("R_TODODOCUMENT", "",
          $"FK_TODO = {todoId}", "");

        if (rsDocumentsFromTodo.RecordCount == 0)
        {
            return true;
        }

        rsDocumentsFromTodo.UseDataChanges = false;
        rsDocumentsFromTodo.UpdateWhenMoveRecord = false;

        rsDocumentsFromTodo.MoveFirst();
        while (!rsDocumentsFromTodo.EOF)
        {
            rsDocumentsFromTodo.Delete();
            rsDocumentsFromTodo.MoveNext();
        }

        rsDocumentsFromTodo.MoveFirst();

        var deleteResult = rsDocumentsFromTodo.Update2();

        if (deleteResult.Any(x => x.HasError))
        {
            reason = $"Verwijderen documenten van taak mislukt, oorzaak: {deleteResult.First(x => x.HasError).GetResult()}";
            return false;
        }

        return true;
    }
}
