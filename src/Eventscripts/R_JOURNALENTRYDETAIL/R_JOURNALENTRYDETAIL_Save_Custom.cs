﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;

public class R_JOURNALENTRYDETAIL_Save_Custom : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //22-06-2021
        //Standaard kan je bij inkoopfactuurregel termijn geen grootboekrekening ingeven.
        //Dat is voor Anton wel belangrijk.
        //Standaard wordt nl bij de factuur een tussenrekening 'Vooruitontvangen termijnfacturen' opgehoogd
        //en bij het financieel gereed melden wordt deze rekening weer afgenomen. Deze tweede boeking gebeurt bij Anton niet.
        //Bij Anton wordt er alleen bij de factuur een boeking gemaakt, maar daar kan je standaard geen grootboekrekning wijzigen/verbijzonderen.

        //We doen dit in de AfterSave omdat we obv volgorde aanmaken bepalen welk journaalpostregel bij welke factuurregel hoort

        if (!CopyGeneralLedgerAccountFromPurchaseInvoiceDetailInstallment(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    private bool CopyGeneralLedgerAccountFromPurchaseInvoiceDetailInstallment(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if(saveType == SaveType.AfterUpdateSave)
        {
            return true;
        }

        var generalledLedgerAccountNumber = _script.GetRecordset("R_GENERALLEDGERACCOUNT", "GENERALLEDGERACCOUNTNUMBER",
            $"PK_R_GENERALLEDGERACCOUNT = {data["FK_GENERALLEDGERACCOUNT"]}", "").DataTable.AsEnumerable().First().Field<string>("GENERALLEDGERACCOUNTNUMBER");

        if(generalledLedgerAccountNumber == "1530" || generalledLedgerAccountNumber == "1531")
        {
            return true;//Bij de automatische regels voor BTW verlegd is dit niet van toepassing
        }

        var journalEntryTypePurchaseInvoice = new Guid("76d84e6b-9954-481a-8f59-57cd36b39832");
        var glaAccountTypePurchaseInstallment = new Guid("c13996eb-6b32-4589-8a10-726c790f1840");

        var journalEntryType = _script.GetRecordset("R_JOURNALENTRY", "FK_JOURNALENTRYTYPE",
            $"PK_R_JOURNALENTRY = {data["FK_JOURNALENTRY"]}", "").DataTable.AsEnumerable().First().Field<Guid>("FK_JOURNALENTRYTYPE");

        if(journalEntryType != journalEntryTypePurchaseInvoice || (Guid)data["FK_GLACCOUNTTYPE"] != glaAccountTypePurchaseInstallment)
        {
            //Alleen doorgaan indien de journaalpost van het type inkoopfactuur is en het rekeningtype van deze regel is 'Vooruitontvangen termijnfacturen'
            return true;  
        }

        var purchaseInvoiceId = _script.GetRecordset("R_PURCHASEINVOICE", "PK_R_PURCHASEINVOICE",
            $"FK_JOURNALENTRY = {data["FK_JOURNALENTRY"]}", "").DataTable.AsEnumerable().FirstOrDefault()?.Field<int>("PK_R_PURCHASEINVOICE") ?? 0;

        if(purchaseInvoiceId == 0)
        {
            reason = "Geen inkoopfactuur gevonden. Kopiëren grootboek voor inkoopfactuurregel termijn mislukt.";
            return false;
        }

        var purchaseInvoiceDetailsInstallment = _script.GetRecordset("R_PURCHASEINVOICEDETAILINSTALLMENT",
            "NETPURCHASEPRICE, FK_GENERALLEDGERACCOUNT", $"FK_PURCHASEINVOICE = {purchaseInvoiceId}", "")
            .DataTable.AsEnumerable().Select(x => new
            {
                PurchaseInvoiceDetailInstallmentId = x.Field<int>("PK_R_PURCHASEINVOICEDETAILINSTALLMENT"),
                NetPurchasePrice = x.Field<double>("NETPURCHASEPRICE"),
                GeneralLedgerAccount = x.Field<int?>("FK_GENERALLEDGERACCOUNT") ?? 0,
            }).OrderBy(x => x.PurchaseInvoiceDetailInstallmentId).ToList();

        //Er is geen directe koppeling tussen de inkoopfactuurregel en de journaalpostregel. We gaan er vanuit dat 
        //de journaalpostregels in de volgorde van inkoopfactuurregel id worden aangemaakt. We sorteren daarom
        //de inkoopfactuurregels en kijken welke plaats de huidige journaalpostregel heeft in de volgorde. 
        //Zo komen we bij de juiste inkoopfactuurregel uit.

        var numberOfPreviousCreatedJournalEntryDetailsInstallment = _script.GetRecordset("R_JOURNALENTRYDETAIL",
            "PK_R_JOURNALENTRYDETAIL", 
            $"FK_JOURNALENTRY = {data["FK_JOURNALENTRY"]} AND PK_R_JOURNALENTRYDETAIL < {data["PK_R_JOURNALENTRYDETAIL"]} AND FK_GLACCOUNTTYPE = '{glaAccountTypePurchaseInstallment}'", 
            "").RecordCount;

        var correspondingPurchaseInvoiceDetailInstallment = purchaseInvoiceDetailsInstallment[numberOfPreviousCreatedJournalEntryDetailsInstallment];

        if(correspondingPurchaseInvoiceDetailInstallment.GeneralLedgerAccount == 0)
        {
            return true; //Geen grootboek gevonden
        }

        if(Math.Abs(correspondingPurchaseInvoiceDetailInstallment.NetPurchasePrice - (double)data["AMOUNT"]) > 0.01)
        {
            reason = $"Opzoeken factuurregel termijn om grootboekrekening te kopiëren is mislukt. Bedrag factuurregel is {correspondingPurchaseInvoiceDetailInstallment.NetPurchasePrice:n2} en bedrag huidige journaalpostregel is {(double)data["AMOUNT"]:n2}";
            return false;
        }

        if((int)data["FK_GENERALLEDGERACCOUNT"] == correspondingPurchaseInvoiceDetailInstallment.GeneralLedgerAccount)
        {
            return true; //Grootboekrekeing is al op de juiste ingesteld
        }

        UpdateCurrentJournalEntryDetail((int)data["PK_R_JOURNALENTRYDETAIL"], correspondingPurchaseInvoiceDetailInstallment.GeneralLedgerAccount);

        return true;
    }

    private void UpdateCurrentJournalEntryDetail(int journalEntryDetailId, int generalLedgerAccount)
    {
        var rsJournalEntryDetail = _script.GetRecordset("R_JOURNALENTRYDETAIL", "FK_GENERALLEDGERACCOUNT",
            $"PK_R_JOURNALENTRYDETAIL = {journalEntryDetailId}", "");
        rsJournalEntryDetail.MoveFirst();
        rsJournalEntryDetail.SetFieldValue("FK_GENERALLEDGERACCOUNT", generalLedgerAccount);
        rsJournalEntryDetail.Update();
    }
}
