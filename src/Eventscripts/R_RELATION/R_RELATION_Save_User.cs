﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.IO;
using Ridder.Communication.Script;
using Ridder.Common.MetaData;
using System.Data.SqlClient;

public class R_RELATION_Save_User : ISaveScript
{
    private ScriptHelper _script = new ScriptHelper();

    //private readonly string SQLServerInfo = "LAPTOP-PDBHPOEJ"; //Lokaal bij DB
    private readonly string SQLServerInfoDwh = "SRV-APP06\\BISHARP"; //Live bij Anton

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //SP
        //12-4-2022
        //Blokkeer het opslaan van de relatie indien relatienaam meer dan 50 tekens heeft of telefoonnummer meer dan 25 tekens
        if (!BlockLengthRelationNameTelephone(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //DB
        //10-12-2018
        //Houdt het veld 'FK_INDUSTRY' gelijk aan 'FK_INDUSTRYCUSTOM'
        if (saveType == SaveType.AfterInsertSave ||
           saveType == SaveType.AfterUpdateSave && oldData["FK_INDUSTRYCUSTOM"].ToString() != data["FK_INDUSTRYCUSTOM"].ToString())
        {
            data["FK_INDUSTRY"] = data["FK_INDUSTRYCUSTOM"];
        }

        //DB
        //10-1-2024
        //Bij Bisharp, controleer of er data in DWH aanwezig is bij wijzigen Ridder Get Data From
        if (!BlockChangeGetDataFromWhenDataExistsInDwh(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }



    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (!SaveVisitingAddressAsDeliveryAddress(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //DB
        //28-4-2021
        //Log relatie financiele gegevens
        if (!CreateLogsRelationFinancialInfo(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //SP
        //9-3-2023
        //Maak of update het WKA-dossier
        if (!CreateOrUpdateWkaFile(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //SP
        //5-1-2022
        //Blokkeer wanneer IBAN al voorkomt bij andere relatie
        if (!BlockIfIBANexistsInOtherRelation(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //SP
        //5-1-2022
        //Blokkeer wanneer IBAN G-rekening al voorkomt bij andere relatie
        if (!BlockIfIBANgRekExistsInOtherRelation(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //DB
        //13-1-2025
        //Log changes sales funnel status
        if (!LogSalesFunnelState(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //DB
        //13-1-2025
        //Genereer automatisch nurture taak bij geen interesse
        if (!GenerateNurtureTodoAtNoInteresseInBisharp(data, oldData, saveType, ref reason))
        {
            return false;
        }


        return true;
    }

    private bool GenerateNurtureTodoAtNoInteresseInBisharp(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (!_script.GetUserInfo().DatabaseName.Equals("Bisharp"))
        {
            return true; //Alleen doorgaan bij Bisharp
        }

        if (saveType == SaveType.AfterInsertSave ||
            saveType == SaveType.AfterUpdateSave && oldData["FK_SALESFUNNELSTATE"].ToString().Equals(data["FK_SALESFUNNELSTATE"].ToString()))
        {
            return true; //sales funnel state wordt niet gewijzigd
        }

        if (!(data["FK_SALESFUNNELSTATE"] as int?).HasValue)
        {
            return true;
        }

        var descriptionSalesFunnelState = _script.GetRecordset("U_SALESFUNNELSTATE", "DESCRIPTION",
            $"PK_U_SALESFUNNELSTATE = {data["FK_SALESFUNNELSTATE"]}", "").DataTable.AsEnumerable().First()
            .Field<string>("DESCRIPTION");

        if(!descriptionSalesFunnelState.StartsWith("Geen interesse"))
        {
            return true;
        }

        if (!(data["FK_REJECTIONMOTIVE"] as int?).HasValue)
        {
            reason = $"Bij sales funnel status 'Geen interesse' dient er een 'Reden geen interesse' ingevuld te worden.";
            return false;
        }

        var salesTodoTypeIds = _script.GetRecordset("R_TODOTYPE", "PK_R_TODOTYPE", "BISHARPTODOTYPE = 2", "")
            .DataTable.AsEnumerable().Select(x => (int)x.Field<int>("PK_R_TODOTYPE")).ToList();

        var wfStateClosed = new Guid("43ea659f-14bb-473a-869b-819c63ef43ac");

        var numberOpenSalesTodos = _script.GetRecordset("R_TODO", "PK_R_TODO",
            $"FK_RELATION = {data["PK_R_RELATION"]} AND FK_TODOTYPE IN ({string.Join(",", salesTodoTypeIds)}) AND FK_WORKFLOWSTATE <> '{wfStateClosed}'", "").RecordCount;

        if(numberOpenSalesTodos > 0)
        {
            reason = "Sluit alle sales taken af bij 'Geen interesse'. Er wordt automatisch een nurture taak gegenereerd over een half jaar.";
            return false;
        }

        var rsTodo = _script.GetRecordset("R_TODO", "", "PK_R_TODO = NULL", "");
        rsTodo.UseDataChanges = true;

        rsTodo.AddNew();
        rsTodo.SetFieldValue("FK_RELATION", data["PK_R_RELATION"] );

        if ((data["MAINCONTACTDEBTOR"] as int?).HasValue)
        {
            rsTodo.SetFieldValue("FK_CONTACT", data["MAINCONTACTDEBTOR"]);
        }

        var newDate = DateTime.Now.AddMonths(6);

        if(newDate.DayOfWeek == DayOfWeek.Saturday)
        {
            newDate = newDate.AddDays(2);
        }
        else if (newDate.DayOfWeek == DayOfWeek.Sunday)
        {
            newDate = newDate.AddDays(1);
        }

        rsTodo.SetFieldValue("FK_TODOTYPE", 52); //Nurture
        rsTodo.SetFieldValue("DUEDATE", newDate);

        var employeeId = GetCurrentEmployeeId(_script.GetUserInfo().CurrentUserId);
        rsTodo.SetFieldValue("FK_ASSIGNEDTO", employeeId);

        rsTodo.Update();

        return true;
    }

    private bool LogSalesFunnelState(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if(!_script.GetUserInfo().DatabaseName.Equals("Bisharp"))
        {
            return true; //Alleen doorgaan bij Bisharp
        }

        if(saveType == SaveType.AfterInsertSave && !(data["FK_SALESFUNNELSTATE"] as int?).HasValue ||
            saveType == SaveType.AfterUpdateSave && oldData["FK_SALESFUNNELSTATE"].ToString().Equals(data["FK_SALESFUNNELSTATE"].ToString()))
        {
            return true; //sales funnel state wordt niet gewijzigd
        }

        var rsLog = _script.GetRecordset("U_LOGSALESFUNNELSTATE", "", "PK_U_LOGSALESFUNNELSTATE = NULL", "");
        rsLog.AddNew();

        rsLog.SetFieldValue("FK_RELATION", data["PK_R_RELATION"]);
        rsLog.SetFieldValue("DATE", DateTime.Now);

        var employeeId = GetCurrentEmployeeId(_script.GetUserInfo().CurrentUserId);
        if(employeeId != 0)
            rsLog.SetFieldValue("FK_EMPLOYEE", employeeId);

        if (oldData != null && (oldData["FK_SALESFUNNELSTATE"] as int?).HasValue)
        {
            rsLog.SetFieldValue("FK_OLDSTATE", (int)oldData["FK_SALESFUNNELSTATE"]);
        }

        if ((data["FK_SALESFUNNELSTATE"] as int?).HasValue)
        {
            rsLog.SetFieldValue("FK_NEWSTATE", (int)data["FK_SALESFUNNELSTATE"]);
        }

        rsLog.Update();

        return true;
    }

    private int GetCurrentEmployeeId(int currentUserId)
    {
        return _script.GetRecordset("R_USER", "FK_EMPLOYEE", $"PK_R_USER = {currentUserId}", "")
            .DataTable.AsEnumerable().FirstOrDefault()?.Field<int?>("FK_EMPLOYEE") ?? 0;
    }

    private bool BlockChangeGetDataFromWhenDataExistsInDwh(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if(saveType == SaveType.AfterInsertSave)
        {
            return true;
        }

        var companyAbbreviaton = _script.GetRecordset("R_CRMSETTINGS", "ABBREVIATIONCOMPANY", "", "")
            .DataTable.AsEnumerable().First().Field<string>("ABBREVIATIONCOMPANY");

        if (!companyAbbreviaton.Equals("BIS"))
        {
            return true; //Alleen doorgaan bij Bisharp
        }

        if (oldData["RIDDERGETDATAFROM"].ToString().Equals(data["RIDDERGETDATAFROM"].ToString()))
        {
            return true;
        }

        if (string.IsNullOrEmpty(data["BISHARPCOMPANYNUMBER"].ToString()))
        {
            reason = "Geen Bisharp company number gevonden.";
            return false;
        }

        var numberOfRecords = GetNumberOfRecordsThisCompanyInRidderDwh(data["BISHARPCOMPANYNUMBER"].ToString(), ref reason);

        if(numberOfRecords > 0)
        {
            reason = "Hee tjappie, er zijn nog records voor deze klant aanwezig in het Ridder DWH. Het wijzigen van de startdatum is niet toegestaan. Anders gaat de bepaling van de verwijderde records mis. Verwijder eerst alle data van deze klant uit het DWH als je de startdatum wilt veranderen.";
            return false;
        }

        return true;
    }

    private int? GetNumberOfRecordsThisCompanyInRidderDwh(string bisharpCompanyNumber, ref string reason)
    {
        var connectionString = $"Server={SQLServerInfoDwh};Database=Bisharp Ridder DWH;User Id=bisharp;Password=Welkom@bisharp;;Trusted_Connection=True;MultipleActiveResultSets=true";
        var queryString = $"SELECT ISNULL(SUM([RECORDCOUNT]),0) AS [TOTALRECORDCOUNT] FROM B_TABLERECORDINFO WHERE COMPANY = '{bisharpCompanyNumber}';";

        DataTable result;

        try
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                connection.Open();

                SqlDataAdapter da = new SqlDataAdapter(command);
                DataTable dtSelectTemplate = new DataTable();
                da.Fill(dtSelectTemplate);

                result = dtSelectTemplate;

                da = null;
                command = null;

                connection.Close();
            }
        }
        catch (Exception e)
        {
            reason = $"Uitvoeren SQL query om te bepalen aantal records in Ridder DWH is mislukt, oorzaak: {e}";
            return null;
        }

        return result.AsEnumerable().First().Field<int>("TOTALRECORDCOUNT");
    }

    private bool CreateOrUpdateWkaFile(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        var wkaActive = _script.GetRecordset("R_CRMSETTINGS", "WKAACTIVE",
            "", "").DataTable.AsEnumerable().First().Field<bool>("WKAACTIVE");

        if (!wkaActive)
        {
            return true;//Alleen van toepassing indien vinkje 'WKA functionaliteit actief' aanstaat
        }

        if (!(bool)data["WKA"])
        {
            return true; 
        }

        if (saveType == SaveType.AfterUpdateSave && (bool)oldData["WKA"] == (bool)data["WKA"] && (oldData["FK_WKATYPE"] as int? ?? 0) == (data["FK_WKATYPE"] as int? ?? 0))
        {
            return true; //Bij wijzigen, alleen doorgaan indien WKA of WKA type worden gewijzigd
        }

        if ((bool)data["WKA"] && (data["FK_WKATYPE"] as int? ?? 0) == 0)
        {
            reason = "Opslaan niet mogelijk. WKA-type is verplicht indien WKA van toepassing.";
            return false;
        }

        var wfCreateOrUpdateWkaFile = new Guid("83acb76a-38e1-4c48-a7ae-270c074e0ed9");

        var wfResult = _script.ExecuteWorkflowEvent("R_RELATION", (int)data["PK_R_RELATION"], wfCreateOrUpdateWkaFile, null);

        if (wfResult.HasError)
        {
            reason = wfResult.GetResult();
            return false;
        }

        return true;
    }

    private bool BlockIfIBANexistsInOtherRelation(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave && oldData["IBAN"].ToString() == data["IBAN"].ToString())
        {
            return true; //Alleen doorgaan indien IBAN wordt gewijzigd
        }

        //Check of de relatie wordt aangemaakt vanuit CreditSafe. Als dat het geval is niet het bericht tonen vanuit het 
        //SaveScript, anders crasht de client. Bericht wordt getoond vanuit script CreditSage_StartEnProcessForm
        if(saveType == SaveType.AfterInsertSave)
        {
            Guid wfImported = new Guid("1add85b0-ebaf-44c2-aa68-83b85bd0a0b8");

            if ((string)data["COCNUMBER"] != "")
            {
                var rsCreditSafeRequests = _script.GetRecordset("C_CREDITSAFEREQUESTS", "PK_C_CREDITSAFEREQUESTS",
                $"COCNUMBER = {data["COCNUMBER"]} AND FK_WORKFLOWSTATE = '{wfImported}'", "");

                if (rsCreditSafeRequests.RecordCount > 0)
                {
                    return true;
                }
            }  
        }

        if ((string)data["IBAN"] == "")
        {
            return true; //Stoppen als IBAN leeg wordt gemaakt
        }

        var rsRelation = _script.GetRecordset("R_RELATION", "NAME",
            $"IBAN = '{data["IBAN"]}' AND PK_R_RELATION <> {data["PK_R_RELATION"]}", "");

        if (rsRelation.RecordCount > 0)
        {
            var wfSendIbanMessage = new Guid("2748d62c-c32b-4140-9a2c-558c2408743f");
            var message = $"IBAN wordt al gebruikt bij relatie(s) { string.Join(", ", rsRelation.DataTable.AsEnumerable().Select(x => x.Field<string>("NAME")).ToList())}";
            var wfParameters = new Dictionary<string, object>();
            wfParameters.Add("Message", message);

            _script.ExecuteWorkflowEvent("R_RELATION", (int)data["PK_R_RELATION"], wfSendIbanMessage, wfParameters);
        }

        return true;
    }
    private bool BlockIfIBANgRekExistsInOtherRelation(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave && oldData["IBANGACCOUNT"].ToString() == data["IBANGACCOUNT"].ToString())
        {
            return true; //Alleen doorgaan indien IBAN g-rekening wordt gewijzigd
        }

        //Check of de relatie wordt aangemaakt vanuit CreditSafe. Als dat het geval is niet het bericht tonen vanuit het 
        //SaveScript, anders crasht de client. Bericht wordt getoond vanuit script CreditSage_StartEnProcessForm
        if (saveType == SaveType.AfterInsertSave)
        {
            Guid wfImported = new Guid("1add85b0-ebaf-44c2-aa68-83b85bd0a0b8");

            if ((string)data["COCNUMBER"] != "")
            {
                var rsCreditSafeRequests = _script.GetRecordset("C_CREDITSAFEREQUESTS", "PK_C_CREDITSAFEREQUESTS",
                $"COCNUMBER = {data["COCNUMBER"]} AND FK_WORKFLOWSTATE = '{wfImported}'", "");

                if (rsCreditSafeRequests.RecordCount > 0)
                {
                    return true;
                }
            }
        }

        if ((string)data["IBANGACCOUNT"] == "")
        {
            return true; //Stoppen als IBAN g-rekening leeg wordt gemaakt
        }

        var rsRelation = _script.GetRecordset("R_RELATION", "NAME",
            $"IBANGACCOUNT = '{data["IBANGACCOUNT"]}' AND PK_R_RELATION <> {data["PK_R_RELATION"]}", "");

        if (rsRelation.RecordCount > 0)
        {
            var wfSendIbanMessage = new Guid("2748d62c-c32b-4140-9a2c-558c2408743f");
            var message = $"IBAN G-rekening wordt al gebruikt bij relatie(s) { string.Join(", ", rsRelation.DataTable.AsEnumerable().Select(x => x.Field<string>("NAME")).ToList())}";
            var wfParameters = new Dictionary<string, object>();
            wfParameters.Add("Message", message);

            _script.ExecuteWorkflowEvent("R_RELATION", (int)data["PK_R_RELATION"], wfSendIbanMessage, wfParameters);
        }

        return true;
    }

    private bool BlockLengthRelationNameTelephone(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (data["NAME"].ToString().Length > 50)
        {
            reason = "Relatienaam mag niet langer zijn dan 50 tekens.";
            return false;
        }

        if (data["PHONE1"].ToString().Length > 25 || data["PHONE2"].ToString().Length > 25)
        {
            reason = "Telefoonnummer mag niet langer zijn dan 25 tekens.";
            return false;
        }

        return true;
    }

    private bool CreateLogsRelationFinancialInfo(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        var columnNamesToCheck = new string[] { "IBAN", "BIC", "IBANGACCOUNT", "BICGACCOUNT" };

        var logsRelationFinancialInfoToCreate = DetermineLogsRelationFinancialInf(columnNamesToCheck, data, oldData, saveType);

        if (!logsRelationFinancialInfoToCreate.Any())
        {
            return true;
        }

        var rsLogRelationFinancialInfo = _script.GetRecordset("C_LOGRELATIONFINANCIALINFO", "", "PK_C_LOGRELATIONFINANCIALINFO IS NULL", "");
        rsLogRelationFinancialInfo.UpdateWhenMoveRecord = false;
        rsLogRelationFinancialInfo.UseDataChanges = false;

        foreach (var logFinancialRelationInfo in logsRelationFinancialInfoToCreate)
        {
            rsLogRelationFinancialInfo.AddNew();
            rsLogRelationFinancialInfo.SetFieldValue("FK_RELATION", data["PK_R_RELATION"]);
            rsLogRelationFinancialInfo.SetFieldValue("COLUMNNAME", logFinancialRelationInfo.ColumnName);
            rsLogRelationFinancialInfo.SetFieldValue("OLDVALUE", logFinancialRelationInfo.OldValue);
            rsLogRelationFinancialInfo.SetFieldValue("NEWVALUE", logFinancialRelationInfo.NewValue);
        }

        rsLogRelationFinancialInfo.Update();

        return true;
    }

    private List<LogRelationFinancialInfo> DetermineLogsRelationFinancialInf(string[] columnNamesToCheck, RowData data, RowData oldData, SaveType saveType)
    {
        var result = new List<LogRelationFinancialInfo>();

        if (saveType == SaveType.AfterInsertSave)
        {
            foreach (var columnName in columnNamesToCheck)
            {
                if (!string.IsNullOrEmpty(data[columnName].ToString()))
                {
                    result.Add(new LogRelationFinancialInfo()
                    {
                        ColumnName = columnName,
                        OldValue = "",
                        NewValue = data[columnName].ToString(),
                    });
                }
            }
        }
        else
        {
            //savetype = SaveType.AfterUpdateSave
            foreach (var columnName in columnNamesToCheck)
            {
                if (!oldData[columnName].ToString().Equals(data[columnName].ToString()))
                {
                    result.Add(new LogRelationFinancialInfo()
                    {
                        ColumnName = columnName,
                        OldValue = oldData[columnName].ToString(),
                        NewValue = data[columnName].ToString(),
                    });
                }
            }
        }

        return result;
    }

    private bool SaveVisitingAddressAsDeliveryAddress(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave && oldData["FK_VISITINGADDRESS"].ToString() == data["FK_VISITINGADDRESS"].ToString())
        {
            return true; //Alleen doorgaan indien bezoekadres wordt gewijzigd
        }

        //Script moet alleen uitgevoerd worden als er geen Afleveradressen bekend zijn bij de relatie en als het bezoekadres ingevuld is
        //# AfleverAdressen berekenen
        var rsAfleverAdres = _script.GetRecordset("R_RELATIONDELIVERYADDRESS", "",
            $"FK_RELATION = {data["PK_R_RELATION"]}",
            "");

        //Controleren of er nog geen Afleveradressen aanwezig zijn en of het bezoekadres ingevuld is. 
        if (rsAfleverAdres.RecordCount == 0 && (data["FK_VISITINGADDRESS"] as int?).HasValue)
        {
            rsAfleverAdres.AddNew();
            rsAfleverAdres.UseDataChanges = true;
            rsAfleverAdres.Fields["FK_RELATION"].Value = (int)data["PK_R_RELATION"];
            rsAfleverAdres.Fields["NAME"].Value = data["NAME"].ToString();
            rsAfleverAdres.Fields["FK_ADDRESS"].Value = (int)data["FK_VISITINGADDRESS"];
            rsAfleverAdres.Update();
        }

        return true;
    }

    class LogRelationFinancialInfo
    {
        public string ColumnName { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
    }

    public class WkaFile
    {
        public string RelationName { get; set; }
        public string CocNumber { get; set; }
    }
}
