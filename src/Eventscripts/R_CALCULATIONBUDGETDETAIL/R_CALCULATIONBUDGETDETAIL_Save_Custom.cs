﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Communication.Script;

public class R_CALCULATIONBUDGETDETAIL_Save_Custom : ISaveScript
{
    private ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //30-04-2019
        //Copy user column 'Amount purchase orders new' from budget detail
        if(!CopyUserColumn(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }

    private bool CopyUserColumn(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true; //Alleen doorgaan bij invoegen
        }

        if (!(data["FK_PROJECTBUDGETDETAIL"] as int?).HasValue)
        {
            return true;
        }

        var rsBudgetDetail = _script.GetRecordset("R_PROJECTBUDGETDETAIL", "AMOUNTPURCHASEORDERSNEW",
            $"PK_R_PROJECTBUDGETDETAIL = {data["FK_PROJECTBUDGETDETAIL"]}", "");
        rsBudgetDetail.MoveFirst();

        data["AMOUNTPURCHASEORDERSNEW"] = rsBudgetDetail.Fields["AMOUNTPURCHASEORDERSNEW"].Value;

        return true;
    }
}
