﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;

public class R_PURCHASEINSTALLMENT_Save_Custom : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        // HVE
        // 18-5-2021
        // Set purchase contract from purchaseorder if it is the main purchase order
        if (!SetPurchaseContract(data, oldData, saveType, ref  reason))
        {
            return false;
        }

        //DB
        //23-11-2021
        //Zet vinkje 'Vooruitfacturatie' altijd aan. Dan wordt er OHW gecreëerd bij journaliseren inkoopfactuur en dat is wat Patrick/Joris willen
        data["INVOICEINADVANCE"] = true;

        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }

    private bool SetPurchaseContract(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true;
        }

        var purchaseOrderId = (int)data["FK_PURCHASEORDER"];
        var purchaseContractId = DeterminePurchaseContract(purchaseOrderId);
        if (purchaseContractId == 0)
        {
            return true;
        }

        data["FK_PURCHASECONTRACT"] = purchaseContractId;

        return true;
    }

    private int DeterminePurchaseContract(int purchaseOrderId)
    {
        var supplierId = _script
            .GetRecordset("R_PURCHASEORDER", "FK_SUPPLIER", $"PK_R_PURCHASEORDER = {purchaseOrderId}", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int>("FK_SUPPLIER"))
            .FirstOrDefault();

        var mainPurchaseId = DetermineTypeOfPurchase("Hoofdaanneemsom");
        if (mainPurchaseId == 0)
        {
            return 0;
        }

        var purchaseContractId = DeterminePurchaseContractId(purchaseOrderId, mainPurchaseId);
        var purchaseContractSupplier = GetSupplierInPurchaseContract(purchaseContractId);
        if (purchaseContractSupplier != supplierId)
        {
            return 0;
        }

        return purchaseContractId;
    }

    private int GetSupplierInPurchaseContract(int purchaseContractId)
    {
        return _script.GetRecordset("C_PURCHASECONTRACT", "FK_SUPPLIER", $"PK_C_PURCHASECONTRACT = {purchaseContractId}", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int>("FK_SUPPLIER"))
            .FirstOrDefault();
    }

    private int DeterminePurchaseContractId(int purchaseOrderId, int mainTypePurchaseId)
    {
        var columns = new List<string>() {"ITEM", "MISC", "OUTSOURCED"};
        var purchaseContractId = 0;
        
        foreach (var column in columns)
        {
            var purchaseDetails = _script.GetRecordset($"R_PURCHASEORDERDETAIL{column}", "",
                    $"FK_PURCHASEORDER = {purchaseOrderId} AND NOT FK_PURCHASECONTRACT IS NULL", "")
                .DataTable
                .AsEnumerable();

            if (!purchaseDetails.Any())
            {
                continue;
            }

            var purchaseDetailContractIds = purchaseDetails
                .Where(y => (y.Field<int?>("FK_PURCHASECONTRACT") ?? 0) > 0)
                .Select(x => x.Field<int>("FK_PURCHASECONTRACT"))
                .ToList()
                .Distinct();

            if (purchaseDetailContractIds.Count() > 1)
            {
                return 0;
            }

            if (!purchaseDetailContractIds.Any())
            {
                continue;
            }

            var purchaseDetailContractId = purchaseDetailContractIds.First();
            var purchaseDetailTypeOfpurchase = purchaseDetails
                .Where(y => y.Field<int>("FK_PURCHASECONTRACT") == purchaseDetailContractId)
                .Select(x => x.Field<int>("FK_TYPEPURCHASEORDER"))
                .FirstOrDefault();
            if (purchaseDetailTypeOfpurchase != mainTypePurchaseId)
            {
                return 0;
            }

            if (purchaseContractId != purchaseDetailContractId)
            {
                if (purchaseContractId == 0)
                {
                    purchaseContractId = purchaseDetailContractId;
                }
                else
                {
                    return 0;
                }
            }
        }

        return purchaseContractId;
    }

    private int DetermineTypeOfPurchase(string code)
    {
        return _script.GetRecordset("C_TYPEPURCHASEORDER", "", $"CODE = '{code}'", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int?>("PK_C_TYPEPURCHASEORDER") ?? 0)
            .FirstOrDefault();
    }
}
