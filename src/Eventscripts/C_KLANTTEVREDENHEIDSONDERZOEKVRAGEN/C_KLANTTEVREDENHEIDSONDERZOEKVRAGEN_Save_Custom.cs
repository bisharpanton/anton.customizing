﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Globalization;
using System.Linq;
using Ridder.Communication.Script;

public class C_KLANTTEVREDENHEIDSONDERZOEKVRAGEN_Save_Custom : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //12-04-2017
        //Controle op score 
		
        if( oldData != null && string.IsNullOrEmpty(data["ANSWERINTEXT"].ToString()) 
                            && ((int) data["SCORE"] < 1 || (int) data["SCORE"] > 10 ))
        {
            reason = "De score moet tussen 1 en 10 liggen.";
            return false;
        }

        //DB
        //27-10-2020
        //Vul automatisch score in indien 'Answer in text' een nummer is
        if (!FillScore(data, oldData, saveType, ref reason))
        {
            return false;
        }


        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }

    private bool FillScore(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave &&
            oldData["ANSWERINTEXT"].ToString().Equals(data["ANSWERINTEXT"].ToString()))
        {
            return true;
        }

        if (string.IsNullOrEmpty(data["ANSWERINTEXT"].ToString()))
        {
            return true;
        }

        if (!int.TryParse(data["ANSWERINTEXT"].ToString(), NumberStyles.Any, CultureInfo.CurrentCulture, out int iScore))
        {
            data["SCORE"] = 0;
        }
        else
        {
            data["SCORE"] = iScore < 1 || iScore > 10 ? 0 : iScore;
        }

        return true;
    }
}
