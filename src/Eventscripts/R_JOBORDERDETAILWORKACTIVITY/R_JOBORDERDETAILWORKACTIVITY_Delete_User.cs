﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Communication.Script;

public class R_JOBORDERDETAILWORKACTIVITY_Delete_User : IDeleteScript
{
    private ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeDelete(RowData data, ref string reason)
    {
        //DB
        //28-9-2023
        //Indien er gekoppelde bewerkingen zijn tbv Appreo, markeer deze bonregels bewerking dan als verwijderd
        if (!CreateDeletedDocumentsRecords(data, ref reason))
        {
            return false;
        }

        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterDelete(RowData data, ref string reason)
    {

        return true;
    }

    private bool CreateDeletedDocumentsRecords(RowData data, ref string reason)
    {
        var orderId = (int)data["FK_ORDER"];

        var orderDocuments = _script.GetRecordset("R_ORDERDOCUMENT", "PK_R_ORDERDOCUMENT, FK_DOCUMENT", $"FK_ORDER = {orderId}", "")
            .DataTable.AsEnumerable().ToList();

        if(!orderDocuments.Any())
        {
            return true;
        }

        var linkedWorkactivitiesPerOrder = _script.GetRecordset("U_WORKACTIVITIESPERORDERDOCUMENT", "FK_ORDERDOCUMENT, FK_WORKACTIVITY",
            $"FK_ORDERDOCUMENT IN ({string.Join(",", orderDocuments.Select(x => x.Field<int>("PK_R_ORDERDOCUMENT")))}) AND FK_WORKACTIVITY = {data["FK_WORKACTIVITY"]}", "")
            .DataTable.AsEnumerable().ToList();

        if (!linkedWorkactivitiesPerOrder.Any())
        {
            return true;
        }

        var rs = _script.GetRecordset("U_DELETEDDOCUMENTS", "", $"PK_U_DELETEDDOCUMENTS = NULL", "");

        rs.UpdateWhenMoveRecord = false;

        foreach (var linkedWorkactivityPerOrder in linkedWorkactivitiesPerOrder)
        {
            var orderDocumentId = linkedWorkactivityPerOrder.Field<int>("FK_ORDERDOCUMENT");
            var documentId = orderDocuments.First(x => x.Field<int>("PK_R_ORDERDOCUMENT") == orderDocumentId).Field<int>("FK_DOCUMENT");

            rs.AddNew();
            rs.SetFieldValue("DOCUMENTID", documentId);
            rs.SetFieldValue("TABLENAME", "R_JOBORDERDETAILWORKACTIVITY");
            rs.SetFieldValue("RECORDID", data["PK_R_JOBORDERDETAILWORKACTIVITY"]);
        }

        rs.MoveFirst();
        rs.Update();

        return true;
    }
}
