﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Communication.Script;

public class C_FORMSEARCHRELATIONONCREDITSAFEDETAILS_Save_Custom : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //27-5-2020
        //Zorg dat er altijd maar 1 record geselecteerd kan worden

        if(!UndoOtherSelectedDetails(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }

    private bool UndoOtherSelectedDetails(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterInsertSave)
        {
            return true;
        }

        if (saveType == SaveType.AfterUpdateSave && (bool)oldData["SELECT"] == (bool)data["SELECT"])
        {
            return true;
        }

        if (!(bool)data["SELECT"])
        {
            return true;
        }

        var currentSelectedFormDetailId = (int)data["PK_C_FORMSEARCHRELATIONONCREDITSAFEDETAILS"];
        var formId = (int)data["FK_FORMSEARCHRELATIONONCREDITSAFE"];

        var rsFormDetailsNowSelected = _script.GetRecordset("C_FORMSEARCHRELATIONONCREDITSAFEDETAILS", 
            "SELECT",
            $"FK_FORMSEARCHRELATIONONCREDITSAFE = {formId} AND PK_C_FORMSEARCHRELATIONONCREDITSAFEDETAILS <> {currentSelectedFormDetailId} AND SELECT = 1", "");

        if (rsFormDetailsNowSelected.RecordCount == 0)
        {
            return true;
        }

        rsFormDetailsNowSelected.MoveFirst();
        rsFormDetailsNowSelected.SetFieldValue("SELECT", false);
        rsFormDetailsNowSelected.Update();

        return true;
    }
}
