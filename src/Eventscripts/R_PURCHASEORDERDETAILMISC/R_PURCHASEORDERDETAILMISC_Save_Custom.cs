﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Communication.Script;

public class R_PURCHASEORDERDETAILMISC_Save_Custom : ISaveScript
{
    private ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //SP
        //16-3-2022
        //Blokkeer wijzigen van de inkoopprijs indien niet nieuw en totaal boven bestellimiet uitkomt
        if (!BlockIfPurchaseLimitExceded(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //SP
        //24-11-2022
        //Indien order gevuld is het ook verplicht om een bon in te vullen
        if (!JoborderObligatedWithOrder(data, oldData, saveType, ref reason))
        {
            return false;
        }

        //SP
        //18-1-2024
        //Indien er op een project wordt geboekt, check of het vinkje divers aan staat bij de gekoppelde budgetregel
        if (!CheckProjectBudgetDetailBudgetType(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }


    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {

        //DB
        //28-02-2018
        //Bij het inkopen van een bonregel of rechtstreeks op order, kopieer het werkadres van de order over naar het bestemmingsadres van de inkooporder
        if (!CopyWorkAdressToPurchaseOrders(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    private bool CheckProjectBudgetDetailBudgetType(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        var projectModuleActive = _script.GetRecordset("R_CRMSETTINGS", "PROJECTMODULEACTIVE",
            "", "").DataTable.AsEnumerable().First().Field<bool>("PROJECTMODULEACTIVE");

        if (!projectModuleActive)
        {
            return true;//Alleen van toepassing indien vinkje 'Projectenmodule actief' aanstaat
        }

        if (saveType == SaveType.AfterUpdateSave)
        {
            return true;//Alleen doorgaan bij eerste keer aanmaken
        }

        if (!(data["FK_ORDER"] as int?).HasValue || !(data["FK_JOBORDER"] as int?).HasValue)
        {
            return true;
        }

        Guid tableJoborder = new Guid("7de0ac3e-9dfa-47e0-b113-63d3cac55f90");
        Guid tablePhase = new Guid("7db2fca6-1cb4-4a2c-9f96-3b4376a5d37b");
        Guid tableBudget = new Guid("51e8daf8-b92c-4852-9518-433679420946");

        var rsWbsJoborder = _script.GetRecordset("R_WORKBREAKDOWNINFO", "PARENTRECORDID",
            $"RECORDID = {data["FK_JOBORDER"]} AND FK_TABLEINFO = '{tableJoborder}' AND FK_PARENTTABLEINFO = '{tablePhase}'", "");

        if(rsWbsJoborder.RecordCount == 0)
        {
            return true;//Geen WBS info gevonden voor deze bon
        }

        var projectPhaseId = rsWbsJoborder.DataTable.AsEnumerable().First().Field<int>("PARENTRECORDID");

        var rsWbsProjectbudgetDetail = _script.GetRecordset("R_WORKBREAKDOWNINFO", "RECORDID",
            $"PARENTRECORDID = {projectPhaseId} AND FK_TABLEINFO = '{tableBudget}' AND FK_PARENTTABLEINFO = '{tablePhase}'", "");

        if(rsWbsProjectbudgetDetail.RecordCount == 0)
        {
            return true;//Geen budgetregel gevonden voor deze projectfase
        }

        var projectbudgetDetailId = rsWbsProjectbudgetDetail.DataTable.AsEnumerable().First().Field<int>("RECORDID");

        var budgetType = _script.GetRecordset("R_PROJECTBUDGETDETAIL", "BUDGETTYPE",
            $"PK_R_PROJECTBUDGETDETAIL = {projectbudgetDetailId}", "").DataTable.AsEnumerable().First().Field<int>("BUDGETTYPE");

        if(budgetType == 1)
        {
            reason = "Deze bon is gekoppeld aan een budgetregel van het type 'Arbeid'. Hier mogen alleen uren op geboekt worden.";
            return false;
        }

        return true;
    }

    private bool JoborderObligatedWithOrder(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        var joborderMandatoryAtPurchase = _script.GetRecordset("R_CRMSETTINGS", "JOBORDERMANDATORYATPURCHASE", "", "")
            .DataTable.AsEnumerable().First().Field<bool>("JOBORDERMANDATORYATPURCHASE");

        //if (_script.GetUserInfo().DatabaseName == "Anton Rail & Infra" || _script.GetUserInfo().DatabaseName == "Hoogendijk Bouw")
        if (joborderMandatoryAtPurchase)
        {
            if (!(data["FK_ORDER"] == null || data["FK_ORDER"] == DBNull.Value))
            {
                // Als order gevuld dan ook bon invullen
                if (data["FK_JOBORDER"] == null || data["FK_JOBORDER"] == DBNull.Value)
                {
                    reason = "Bonnummer is verplicht bij gebruik van order";
                    return false;
                }
            }
        }

        return true;
    }

    private bool BlockIfPurchaseLimitExceded(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave && Math.Abs((double)oldData["NETPURCHASEPRICE"] - (double)data["NETPURCHASEPRICE"]) < 0.01)
        {
            return true;//inkoopprijs wordt niet gewijzigd
        }

        Guid wf_new = new Guid("0fc61ed2-0da1-4832-9644-635ef6e5bea6");

        var purchaseorder = _script.GetRecordset("R_PURCHASEORDER", "FK_WORKFLOWSTATE, FK_PURCHASER, FK_APPROVEDBY",
            $"PK_R_PURCHASEORDER = {data["FK_PURCHASEORDER"]}", "").DataTable.AsEnumerable().First();

        var wf_purchaseorder = purchaseorder.Field<Guid>("FK_WORKFLOWSTATE");

        if (wf_purchaseorder == wf_new)
        {
            return true;//Inkooporder is nieuw, prijzen mogen worden gewijzigd
        }

        if (purchaseorder.Field<int?>("FK_APPROVEDBY").HasValue)
        {
            return true; //Inkooporder is al gefiatteerd, prijzen mogen worden gewijzigd
        }

        var purchaserId = purchaseorder.Field<int?>("FK_PURCHASER");

        if (!purchaserId.HasValue)
        {
            return true; //geen inkoper geselecteerd
        }

        var purchaseLimit = DeterminePurchaseLimit(purchaserId.Value);

        var otherAmount = _script.GetRecordset("R_PURCHASEORDERALLDETAIL", "NETPURCHASEPRICE",
            $"FK_PURCHASEORDER = {data["FK_PURCHASEORDER"]} AND FK_PURCHASEORDERDETAILMISC <> {data["PK_R_PURCHASEORDERDETAILMISC"]}", "")
            .DataTable.AsEnumerable().Sum(x => x.Field<double>("NETPURCHASEPRICE"));

        var newAmount = (double)data["NETPURCHASEPRICE"];

        var totalAmount = (double)otherAmount + (double)newAmount;

        if ((double)totalAmount > (double)purchaseLimit)
        {
            reason = $"Het is niet toegestaan de inkoopprijs te wijzigen. Het totaalbedrag ligt boven uw bestellimiet. " +
                $"Zet de inkooporder terug naar nieuw om de prijzen te wijzigen en fiat aan te vragen.";
            return false;
        }

        return true;
    }

    private bool CopyWorkAdressToPurchaseOrders(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true;
        }

        if (!(bool)GetCrmSetting("COPYWORKADDRESSORDERTOPURCHASEORDER"))
        {
            return true;
        }

        if (((data["FK_JOBORDERDETAILMISC"] as int?).HasValue || (data["FK_ORDER"] as int?).HasValue))
        {
            var order = 0;

            if ((data["FK_JOBORDERDETAILMISC"] as int?).HasValue)
            {
                var rsJobOrderDetail = _script.GetRecordset("R_JOBORDERDETAILMISC", "FK_ORDER",
                    $"PK_R_JOBORDERDETAILMISC = {data["FK_JOBORDERDETAILMISC"]}", "");
                rsJobOrderDetail.MoveFirst();

                order = (int)rsJobOrderDetail.Fields["FK_ORDER"].Value;
            }
            else
            {
                order = (int)data["FK_ORDER"];
            }

            var rsOrder = _script.GetRecordset("R_ORDER", "FK_DESTINATIONADDRESS",
                $"PK_R_ORDER = {order}", "");
            rsOrder.MoveFirst();


            var rsPurchaseOrder = _script.GetRecordset("R_PURCHASEORDER", "FK_WORKADRESSORDER",
                $"PK_R_PURCHASEORDER = {data["FK_PURCHASEORDER"]}", "");
            rsPurchaseOrder.MoveFirst();

            if (!(rsPurchaseOrder.Fields["FK_WORKADRESSORDER"].Value as int?).HasValue)
            {
                rsPurchaseOrder.Fields["FK_WORKADRESSORDER"].Value = rsOrder.Fields["FK_DESTINATIONADDRESS"].Value;
                rsPurchaseOrder.Update();
            }
            else
            {
                //DB
                //07-08-2018 - Het is wel gewenst om meerdere orders te combineren. In dit geval wordt het laatste werkadres gepakt. Op de inkooporder onderdrukken we
                //dan het werkadres. 
                /*
				//Check of de order die gekoppeld wordt hetzelfde werkadres heeft als momenteel in de inkooporder staat
				if( (int)rsPurchaseOrder.Fields["FK_WORKADRESSORDER"].Value != (int)rsOrder.Fields["FK_DESTINATIONADDRESS"].Value )
				{
					reason = "Er worden behoeftes van verschillende werkadressen gecombineerd. Dit is niet mogelijk.";
					return false;
				}*/
            }
        }

        return true;
    }

    public object GetCrmSetting(string s)
    {
        var rsCRM = _script.GetRecordset("R_CRMSETTINGS", s, "", "");
        rsCRM.MoveFirst();

        return rsCRM.Fields[s].Value;
    }

    private double DeterminePurchaseLimit(int purchaserId)
    {
        return _script.GetRecordset("R_EMPLOYEE", "C_BESTELLIMIET",
            $"PK_R_EMPLOYEE = {purchaserId}", "").DataTable.AsEnumerable().Select(x => x.Field<double?>("C_BESTELLIMIET") ?? 0d).FirstOrDefault();
    }
}
