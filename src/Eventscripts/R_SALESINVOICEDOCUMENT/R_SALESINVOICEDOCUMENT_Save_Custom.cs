﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;

public class R_SALESINVOICEDOCUMENT_Save_Custom : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        // HVE
        // 27-7-2021
        // Images not allowed
        // SP 22-3-2022: Uitgebreid met message bestanden
        if (!CheckIfDocumentIsAllowed(data, oldData, saveType, ref reason))
        {
            return false;
        }
        return true;
    }


    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }
    private bool CheckIfDocumentIsAllowed(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true;
        }

        var docId = (int)data["FK_DOCUMENT"];

        var extension = _script.GetRecordset("R_DOCUMENT", "EXTENSION", $"PK_R_DOCUMENT = {docId}", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<string>("EXTENSION"))
            .FirstOrDefault();

        if(!extension.Equals(".pdf", StringComparison.OrdinalIgnoreCase))
        {
            reason = $"Bestand niet geaccepteerd. Alleen PDF bestanden kunnen als document toegevoegd worden bij de verkoopfactuur.";
            return false;
        }

        return true;
    }
}
