﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;
using Ridder.Communication.Script;

public class R_TOOL_Save_Custom : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //DB
        //19-2-2020
        //Opslaan datum wanneer afbeelding voor het laatst gewijzigd is
        if (!SaveDateImageChanged(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }
    private bool SaveDateImageChanged(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        
        if (saveType == SaveType.AfterInsertSave && data["IMAGE"] == DBNull.Value)
        {
            return true;
        }

        if (saveType == SaveType.AfterInsertSave && data["IMAGE"] != DBNull.Value)
        {
            data["DATEIMAGECHANGED"] = DateTime.Now;
            return true;
        }

        if (oldData["IMAGE"] == DBNull.Value && data["IMAGE"] == DBNull.Value)
        {
            return true; //Niets doen. Voor en na opslaan is er geen foto
        }

        if (oldData["IMAGE"] == DBNull.Value && data["IMAGE"] != DBNull.Value ||
            oldData["IMAGE"] != DBNull.Value && data["IMAGE"] == DBNull.Value)
        {
            //Foto wordt ingevuld of verwijderd
            data["DATEIMAGECHANGED"] = DateTime.Now;
            return true; 
        }

        if(!SameByteArray((byte[])oldData["IMAGE"], (byte[])data["IMAGE"]))
        {
            //Foto wordt gewijzigd
            data["DATEIMAGECHANGED"] = DateTime.Now;
            return true; 
        }

        return true;
    }

    public bool SameByteArray(byte[] a, byte[] b)
    {
        if (a == b)
        {
            return true;
        }

        if ((a != null) && (b != null))
        {
            if (a.Length != b.Length)
            {
                return false;
            }

            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] != b[i])
                {
                    return false;
                }
            }

            return true;
        }

        return false;
    }
}
