﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Communication.Script;

public class U_PURCHASEFRAMEWORKDETAIL_Save_User : ISaveScript
{
    private readonly ScriptHelper _script = new ScriptHelper();

    //When returning false fill reason to give meaningfull message to user
    public bool BeforeSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterSave(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        //SP
        //10-8-2023
        //Bij aanmaken raamcontract regel mogelijk alternatieven toevoegen
        if (!CheckForPurchaseFrameworkDetailAlternatives(data, oldData, saveType, ref reason))
        {
            return false;
        }

        return true;
    }

    private bool CheckForPurchaseFrameworkDetailAlternatives(RowData data, RowData oldData, SaveType saveType, ref string reason)
    {
        if (saveType == SaveType.AfterUpdateSave)
        {
            return true;//Alleen doorgaan bij aanmaken
        }

        var alternativeItemIds = _script.GetRecordset("R_ITEM", "PK_R_ITEM",
            $"FK_BASEITEM = {data["FK_ITEM"]}", "").DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_ITEM")).ToList();

        if(!alternativeItemIds.Any())
        {
            return true;//Geen alternatieven gevonden
        }

        var supplierId = _script.GetRecordset("U_PURCHASEFRAMEWORK", "FK_SUPPLIER",
                $"PK_U_PURCHASEFRAMEWORK = {data["FK_PURCHASEFRAMEWORK"]}", "").DataTable.AsEnumerable().First().Field<int>("FK_SUPPLIER");

        var rsAlternativeItems = _script.GetRecordset("U_PURCHASEFRAMEWORKDETAILALTERNATIVE", "", "PK_U_PURCHASEFRAMEWORKDETAILALTERNATIVE = -1", "");
        rsAlternativeItems.UseDataChanges = false;
        rsAlternativeItems.UpdateWhenMoveRecord = false;

        foreach(var alternativeItemId in alternativeItemIds)
        {
            var rsItemSupplier = _script.GetRecordset("R_ITEMSUPPLIER", "PURCHASEPRICE",
                $"FK_ITEM = {alternativeItemId} AND FK_RELATION = {supplierId}", "");

            if (rsItemSupplier.RecordCount == 0)
            {
                return true;
            }

            var orignalPrice = rsItemSupplier.DataTable.AsEnumerable().First().Field<double>("PURCHASEPRICE");

            rsAlternativeItems.AddNew();

            rsAlternativeItems.SetFieldValue("FK_PURCHASEFRAMEWORKDETAIL", data["PK_U_PURCHASEFRAMEWORKDETAIL"]);
            rsAlternativeItems.SetFieldValue("FK_ITEM", alternativeItemId);
            rsAlternativeItems.SetFieldValue("ORIGINALPURCHASEPRICE", orignalPrice);

        }

        var updateResult = rsAlternativeItems.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            reason = $"Aanmaken inkoop raamcontract detail alternatief misluk, oorzaak: {updateResult.First(x => x.HasError).GetResult()}";
            return false;
        }


        return true;
    }
}
