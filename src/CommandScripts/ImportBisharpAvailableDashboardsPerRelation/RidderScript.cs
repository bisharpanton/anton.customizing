﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class ImportBisharpAvailableDashboardsPerRelation_RidderScript : CommandScript
{
    public void Execute()
    {
        //SP
        //4-3-2024
        //Importeer bisharp beschikbare dashboards per relatie

        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("R_RELATION", StringComparison.OrdinalIgnoreCase))
        {
            MessageBox.Show("Dit script mag alleen van de Relatie tabel aangeroepen worden.");
            return;
        }

        var relationIds = FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue()).ToList();

        object[] selectie = null;
        selectie = OpenMultiSelectForm("U_BISHARPDASHBOARD", "PK_U_BISHARPDASHBOARD",
            "", "");

        if (selectie == null)
        {
            return;
        }

        ImportDashboardGroups(selectie, relationIds);

    }

    private void ImportDashboardGroups(object[] selectie, List<int> relationIds)
    {
        var rsAvailableDashboardsPerRelation = GetRecordset("U_BISHARPAVAILABLEDASHBOARDSPERRELATION", "",
            "PK_U_BISHARPAVAILABLEDASHBOARDSPERRELATION = -1", "");
        rsAvailableDashboardsPerRelation.UseDataChanges = true;
        rsAvailableDashboardsPerRelation.UpdateWhenMoveRecord = false;

        foreach (var relationId in relationIds)
        {
            foreach (var dashboardId in selectie)
            {
                rsAvailableDashboardsPerRelation.AddNew();
                rsAvailableDashboardsPerRelation.Fields["FK_BISHARPDASHBOARD"].Value = dashboardId;
                rsAvailableDashboardsPerRelation.Fields["FK_RELATION"].Value = relationId;
            }
        }

        var updateResult = rsAvailableDashboardsPerRelation.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Het importeren van Bisharp dashboards is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

    }
}
