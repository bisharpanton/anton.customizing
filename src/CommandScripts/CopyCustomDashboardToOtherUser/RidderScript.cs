﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class CopyCustomDashboardToOtherUser_RidderScript : CommandScript
{
	public void Execute()
	{
        //SP
        //14-6-2024
        //Kopieer scheduler task

        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("U_BISHARPPORTALUSERCUSTOMDASHBOARDS", StringComparison.OrdinalIgnoreCase))
        {
            MessageBox.Show("Dit script werkt alleen op de tabel 'Bisharp portal user custom dashboards'.", "Ridder iQ", MessageBoxButtons.OK,
               MessageBoxIcon.Error);
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        var ids = FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue()).ToList();

        object[] selectie = null;
        selectie = OpenMultiSelectForm("U_BISHARPPORTALUSER", "PK_U_BISHARPPORTALUSER",
            "", "");

        if (selectie == null)
        {
            return;
        }

        foreach (var id in ids)
        {
            ObjectToCreate objectToCreate = GetObjectToCreate(id);

            CopyCustomDashboard(selectie, objectToCreate);
        }

        
    }

    private void CopyCustomDashboard(object[] selectie, ObjectToCreate objectToCreate)
    {
        var rsNewBisharpCustomDashboard = GetRecordset("U_BISHARPPORTALUSERCUSTOMDASHBOARDS", "",
            "PK_U_BISHARPPORTALUSERCUSTOMDASHBOARDS = -1", "");
        rsNewBisharpCustomDashboard.UseDataChanges = true;
        rsNewBisharpCustomDashboard.UpdateWhenMoveRecord = false;

        foreach (var userId in selectie)
        {
            /*if ((int)userId == objectToCreate.PortalUserId)
            {
                MessageBox.Show($"Alleen toegestaan om naar andere users te kopiëren.");
                return;
            }*/

            rsNewBisharpCustomDashboard.AddNew();
            rsNewBisharpCustomDashboard.Fields["FK_BISHARPPORTALUSER"].Value = userId;
            rsNewBisharpCustomDashboard.Fields["POSITION"].Value = GetNextPosition(userId);
            rsNewBisharpCustomDashboard.Fields["TITLE"].Value = ValidateTitle(userId, objectToCreate.Title);
            rsNewBisharpCustomDashboard.Fields["ICONNAME"].Value = objectToCreate.IconName;
            rsNewBisharpCustomDashboard.Fields["GRIDCONTENT"].Value = objectToCreate.GridContent;
            rsNewBisharpCustomDashboard.Fields["MOBILEGRIDCONTENT"].Value = objectToCreate.MobileContent;
            rsNewBisharpCustomDashboard.Fields["PLAINTEXT_GRIDCONTENT"].Value = objectToCreate.GridContentPlain;
            rsNewBisharpCustomDashboard.Fields["PLAINTEXT_MOBILEGRIDCONTENT"].Value = objectToCreate.MobileContentPlain;
        }

        var updateResult = rsNewBisharpCustomDashboard.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Het kopiëren van custom dashboard(s) is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }
    }

    private object ValidateTitle(object userId, string title)
    {
        var rsTitle = GetRecordset("U_BISHARPPORTALUSERCUSTOMDASHBOARDS", "TITLE",
            $"FK_BISHARPPORTALUSER = {userId} AND TITLE = '{title}'", "");

        if(rsTitle.RecordCount > 0)
        {
            title = title + "_1";
        }

        return title;
    }

    private int GetNextPosition(object userId)
    {
        var rsPosition = GetRecordset("U_BISHARPPORTALUSERCUSTOMDASHBOARDS", "POSITION",
            $"FK_BISHARPPORTALUSER = {userId}", "");

        var position = 0;
        if (rsPosition.RecordCount > 0)
        {
            position = rsPosition.DataTable.AsEnumerable().Max(x => x.Field<int>("POSITION")) + 1;
        }

        return position;
    }

    private ObjectToCreate GetObjectToCreate(int id)
    {
        return GetRecordset("U_BISHARPPORTALUSERCUSTOMDASHBOARDS", "FK_BISHARPPORTALUSER, TITLE, ICONNAME, PLAINTEXT_GRIDCONTENT, PLAINTEXT_MOBILEGRIDCONTENT, GRIDCONTENT, MOBILEGRIDCONTENT",
            $"PK_U_BISHARPPORTALUSERCUSTOMDASHBOARDS = {id}", "").DataTable.AsEnumerable().Select(x => new ObjectToCreate()
            {
                PortalUserId = x.Field<int>("FK_BISHARPPORTALUSER"),
                Title = x.Field<string>("TITLE"),
                IconName = x.Field<string>("ICONNAME"),
                GridContent = x.Field<string>("GRIDCONTENT"),
                MobileContent = x.Field<string>("MOBILEGRIDCONTENT"),
                GridContentPlain = x.Field<string>("PLAINTEXT_GRIDCONTENT"),
                MobileContentPlain = x.Field<string>("PLAINTEXT_MOBILEGRIDCONTENT")

            }).First();
    }

    class ObjectToCreate
    {
        public int PortalUserId { get; set; }
        public string Title { get; set; }
        public string IconName { get; set; }
        public string GridContent { get; set; }
        public string MobileContent { get; set; }
        public string GridContentPlain { get; set; }
        public string MobileContentPlain { get; set; }
    }
}
