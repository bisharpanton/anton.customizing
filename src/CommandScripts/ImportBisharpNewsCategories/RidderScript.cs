﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class ImportBisharpNewsCategories_RidderScript : CommandScript
{
    public void Execute()
    {
        //SP
        //2-11-2022
        //Importeer bisharp dashboard groepen

        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("R_RELATION", StringComparison.OrdinalIgnoreCase))
        {
            MessageBox.Show("Dit script mag alleen van de Relatie-tabel aangeroepen worden.");
        }

        var relationIds = FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue()).ToList();

        object[] selectie = null;
        selectie = OpenMultiSelectForm("U_BISHARPPORTALNEWSCATEGORY", "PK_U_BISHARPPORTALNEWSCATEGORY",
            "", "");

        if (selectie == null)
        {
            return;
        }

        ImportDashboardGroups(selectie, relationIds);

    }
    private void ImportDashboardGroups(object[] selectie, List<int> relationIds)
    {
        var rsNewsCategoriesPerRelation = GetRecordset("U_BISHARPAVAILABLENEWSCATEGORIESPERRELATION", "",
            "PK_U_BISHARPAVAILABLENEWSCATEGORIESPERRELATION = -1", "");
        rsNewsCategoriesPerRelation.UseDataChanges = true;
        rsNewsCategoriesPerRelation.UpdateWhenMoveRecord = false;

        foreach (var relationId in relationIds)
        {
            foreach (var newsCategoryId in selectie)
            {
                rsNewsCategoriesPerRelation.AddNew();
                rsNewsCategoriesPerRelation.Fields["FK_BISHARPPORTALNEWSCATEGORY"].Value = newsCategoryId;
                rsNewsCategoriesPerRelation.Fields["FK_RELATION"].Value = relationId;
            }
        }

        var updateResult = rsNewsCategoriesPerRelation.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Het importeren van Bisharp nieuwsgroepen is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

    }
}
