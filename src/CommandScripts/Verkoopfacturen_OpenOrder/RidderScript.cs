﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using Ridder.Client.SDK.SDKParameters;

public class Verkoopfacturen_OpenOrder_RidderScript : CommandScript
{
    public void Execute()
    {
        //RK
        //1-12-2023
        //Open order vanuit verkoopfactuur

        if(FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("R_SALESINVOICE"))
        {
            MessageBox.Show("Dit script mag alleen vanaf de verkoopfacturen aangeroepen worden.");
            return;
        }

        if(!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        if(FormDataAwareFunctions.GetSelectedRecords().Count() > 1)
        {
            MessageBox.Show("Dit script mag voor één record tegelijk aangeroepen worden.");
            return;
        }

        var salesInvoiceId = (int)FormDataAwareFunctions.GetSelectedRecords().First().GetPrimaryKeyValue();

        //Opzoeken van de orders, via de verkoopfactuurregels, let op dat meerdere regels voor dezelfde order kunnen zijn
        var salesInvoiceLines = GetRecordset("R_SALESINVOICEALLDETAIL", "FK_ORDER", $"FK_SALESINVOICE = {salesInvoiceId}", "").DataTable.AsEnumerable().ToList();

        //Verwijder dubbele waardes
        var orderIds = salesInvoiceLines.Select(x => x.Field<int>("FK_ORDER")).Distinct().ToList();

        //Openen van de orders  (bij 1 of2 orders, openen in een edit, bij meer dan 2 een browse openen met die orders)
        if(salesInvoiceLines.Count <= 0)
        {
            MessageBox.Show("Deze verkoopfactuur heeft geen onderliggende orders.");
            return;
        }

        if (salesInvoiceLines.Count == 1)
        {
            OpenEdit(new EditParameters()
            {
                TableName = "R_ORDER",
                Id = orderIds.First(),
            }, false);
        } 
        else
        {
            OpenBrowse(new BrowseParameters()
            {
                TableName = "R_ORDER",
                Filter = $"PK_R_ORDER IN ({string.Join(",", orderIds)})",
                BaseLayoutName = "Default",
            }, false);
        }
    }
}
