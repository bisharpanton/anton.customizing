﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;

public class VerkoopKPI_RidderScript : CommandScript
{
    public void Execute()
    {
		//DB
		//11-11-2014
		//Bereken de KPI's van de verkopers
		
		//Ophalen werknemer
		var rsGebruiker = GetRecordset("R_USER", "FK_EMPLOYEE", "PK_R_USER = " + (int) CurrentUserId , "");
		rsGebruiker.MoveFirst();
		
		if(rsGebruiker.Fields["FK_EMPLOYEE"].Value != DBNull.Value)
        {
            DateTime jaarGeleden = DateTime.Now.Date.AddYears(-1).AddDays((-1 * DateTime.Now.Day) + 1);

			//Bereken KPI-waarden
			//Haal de offertes op die voldoen aan de volgende condities:
			//1) Niet gereviseerd en niet historisch
			//2) Ordercategorie is id 1 of 2
			//3) Offertes vanaf dezelfde maand van het vorige jaar
			//4) Verkoper is de gebruiker die inlogt
			ScriptRecordset rsOfferte = GetRecordset("R_OFFER", "PK_R_OFFER", 
				"FK_ORDERCATEGORY <= 2 AND FK_WORKFLOWSTATE <> 'bb352a2a-6168-4d76-9f2a-3ca32e319a8f' AND FK_WORKFLOWSTATE <> '0929ec3c-3e8f-4d59-accf-17b40cd4b8c9' AND FK_SALESPERSON = " + (int) rsGebruiker.Fields["FK_EMPLOYEE"].Value + " AND OFFERDATE  >= '" +
                $"{jaarGeleden:yyyy/M/d}" + "'", "");
			
			
			//Haal de orders op die en bereken bedrag excl meerwerk
			//1) Ordercategorie is id 1 of 2
			//2) Offertes vanaf dezelfde maand van het vorige jaar
			//3) Verkoper is de gebruiker die inlogt
			//Bij optellen van het orderbedrag wordt rekening gehouden met meerwerk
			ScriptRecordset rsOffertesGescoord = GetRecordset("R_ORDER", "PK_R_ORDER", 
				"FK_ORDERCATEGORY <= 2 AND FK_SALESPERSON = " + (int) rsGebruiker.Fields["FK_EMPLOYEE"].Value + " AND ORDERDATE  >= '" +
                $"{jaarGeleden:yyyy/M/d}" + "'" , "");
			
			//Open KPI-scherm
			var rsKPI = GetRecordset("U_VERKOOPKPI" , "", "FK_SALESPERSON = " + (int) rsGebruiker.Fields["FK_EMPLOYEE"].Value , "");
			
            //Als de ingelogde verkoper al KPI-instellingen heeft, open dan deze. Maak anders nieuwe KPI-instellingen aan. 
			if( rsKPI.RecordCount > 0)
			{
				rsKPI.MoveFirst();
				
				berekenKPI(rsOfferte, rsOffertesGescoord,  (int) rsKPI.Fields["PK_U_VERKOOPKPI"].Value );
				OpenEditForm("U_VERKOOPKPI", (int) rsKPI.Fields["PK_U_VERKOOPKPI"].Value , null, false);
			}
			else
			{
				rsKPI.AddNew();
				rsKPI.Fields["FK_SALESPERSON"].Value = (int) rsGebruiker.Fields["FK_EMPLOYEE"].Value;
				rsKPI.Update(null, null);
				
				berekenKPI(rsOfferte, rsOffertesGescoord,  (int) rsKPI.Fields["PK_U_VERKOOPKPI"].Value );
				
				OpenEditForm("U_VERKOOPKPI", (int) rsKPI.Fields["PK_U_VERKOOPKPI"].Value , null, false);
			}
		}
	}
	
	public void berekenKPI(ScriptRecordset of, ScriptRecordset or , int i)
	{
		var rsWegschrijvenKPI = GetRecordset("U_VERKOOPKPI" , "", "PK_U_VERKOOPKPI = " + i , "");
		rsWegschrijvenKPI.MoveFirst();
		
		rsWegschrijvenKPI.Fields["AANTALUITGEBRACHT"].Value = (int) of.RecordCount;
		rsWegschrijvenKPI.Fields["AANTALORDERGEWORDEN"].Value = (int) or.RecordCount;
				
		double totaalOfferteBedrag = 0.0;
		double totaalOrderBedrag = 0.0;
				
		//Optellen offertebedrag
		if( of.RecordCount > 0)
			of.MoveFirst();
		while(of.RecordCount > 0 && !of.EOF)
		{
			totaalOfferteBedrag += optellenOfferteBedrag( (int) of.Fields["PK_R_OFFER"].Value );
			of.MoveNext();
		}
				
		//Optellen orderbedrag
		if( or.RecordCount > 0)
			or.MoveFirst();
		while(or.RecordCount > 0 && !or.EOF)
		{
			totaalOrderBedrag += optellenOrderBedrag( (int) or.Fields["PK_R_ORDER"].Value );
			or.MoveNext();
		}
		//Wegschrijven totalen
		rsWegschrijvenKPI.Fields["TOTALEOFFERTEWAARDE"].Value = totaalOfferteBedrag;
		rsWegschrijvenKPI.Fields["TOTALEORDERWAARDE"].Value = totaalOrderBedrag;
		
		//Schrijf ook de datum van vorig jaar weg. 
		DateTime jaarGeledenWegschrijven = DateTime.Now.Date.AddYears(-1).AddDays((-1*DateTime.Now.Day) +1 )  ;
		rsWegschrijvenKPI.Fields["DATUMVORIGJAAR"].Value = jaarGeledenWegschrijven;
		
				
		//Update Recordset
		rsWegschrijvenKPI.Update(null, null);
	}
	
	
	
	public double optellenOfferteBedrag(int i)
	{
		double offerteWaarde = 0.0;
		//Artikel
		ScriptRecordset rsOfferteRegelArtikel = GetRecordset("R_OFFERDETAILITEM", "NETSALESAMOUNT", "FK_OFFER = " + i , "" );
		rsOfferteRegelArtikel.MoveFirst();
		
		while(rsOfferteRegelArtikel.RecordCount > 0 && !rsOfferteRegelArtikel.EOF)
		{
			offerteWaarde += (double) rsOfferteRegelArtikel.Fields["NETSALESAMOUNT"].Value;
			rsOfferteRegelArtikel.MoveNext();
		}
		
		//Divers
		ScriptRecordset rsOfferteRegelDivers = GetRecordset("R_OFFERDETAILMISC", "NETSALESAMOUNT", "FK_OFFER = " + i , "" );
		rsOfferteRegelDivers.MoveFirst();
		
		while(rsOfferteRegelDivers.RecordCount > 0 && !rsOfferteRegelDivers.EOF)
		{
			offerteWaarde += (double) rsOfferteRegelDivers.Fields["NETSALESAMOUNT"].Value;
			rsOfferteRegelDivers.MoveNext();
		}
		
		//Stuklijst
		ScriptRecordset rsOfferteRegelStuklijst = GetRecordset("R_OFFERDETAILASSEMBLY", "NETSALESAMOUNT", "FK_OFFER = " + i , "" );
		rsOfferteRegelStuklijst.MoveFirst();
		
		while(rsOfferteRegelStuklijst.RecordCount > 0 && !rsOfferteRegelStuklijst.EOF)
		{
			offerteWaarde += (double) rsOfferteRegelStuklijst.Fields["NETSALESAMOUNT"].Value;
			rsOfferteRegelStuklijst.MoveNext();
		}
		
		return offerteWaarde;
	}


    public double optellenOrderBedrag(int i)
    {
        double orderWaarde = 0.0;
        //Artikel
        ScriptRecordset rsOrderRegelArtikel = GetRecordset("R_SALESORDERDETAILITEM",
            "NETSALESAMOUNT, FK_OFFERDETAILITEM, ADDITIONALWORK", "FK_ORDER = " + i, "");
        rsOrderRegelArtikel.MoveFirst();

        while (rsOrderRegelArtikel.RecordCount > 0 && !rsOrderRegelArtikel.EOF)
        {
            if (!(bool)rsOrderRegelArtikel.Fields["ADDITIONALWORK"].Value)
                orderWaarde += (double)rsOrderRegelArtikel.Fields["NETSALESAMOUNT"].Value;

            rsOrderRegelArtikel.MoveNext();
        }

        //Divers
        ScriptRecordset rsOrderRegelDivers = GetRecordset("R_SALESORDERDETAILMISC",
            "NETSALESAMOUNT, FK_OFFERDETAILMISC, ADDITIONALWORK","FK_ORDER = " + i, "");
        rsOrderRegelDivers.MoveFirst();

        while (rsOrderRegelDivers.RecordCount > 0 && !rsOrderRegelDivers.EOF)
        {
            if (!(bool)rsOrderRegelDivers.Fields["ADDITIONALWORK"].Value)
                orderWaarde += (double)rsOrderRegelDivers.Fields["NETSALESAMOUNT"].Value;

            rsOrderRegelDivers.MoveNext();
        }

        //Stuklijst
        ScriptRecordset rsOrderRegelStuklijst = GetRecordset("R_SALESORDERDETAILASSEMBLY",
            "NETSALESAMOUNT, FK_OFFERDETAILASSEMBLY, ADDITIONALWORK", "FK_ORDER = " + i, "");
        rsOrderRegelStuklijst.MoveFirst();

        while (rsOrderRegelStuklijst.RecordCount > 0 && !rsOrderRegelStuklijst.EOF)
        {
            if (!(bool)rsOrderRegelStuklijst.Fields["ADDITIONALWORK"].Value)
                orderWaarde += (double)rsOrderRegelStuklijst.Fields["NETSALESAMOUNT"].Value;

            rsOrderRegelStuklijst.MoveNext();
        }

        return orderWaarde;
    }
}
