﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Ridder.Common.ADO;
using Ridder.Common.Framework.AttributeBased;
using Ridder.Common.Menu;
using Ridder.Common.Script;

public class RidderScript3 : CommandScript
{

    public List<CustomCommandInfo> GetAllCustomCommandDatas()
    {
        var converter = new BytesConverter();
        var result = new List<CustomCommandInfo>();
        var rsMenu = GetRecordset("S_MENU", "", "SCOPE <> 1", "");
        rsMenu.MoveFirst();

        while (!rsMenu.EOF)
        {
            var tempDic = converter.ConvertFromDBValue(rsMenu.Fields["COMMANDDATALIST"].Value) as Dictionary<Guid, CommandData>;

            if (tempDic != null)
            {
                foreach (var tempDicItem in tempDic.Where(tempDicItem => result.FirstOrDefault(x => x.Guid == tempDicItem.Key) == null))
                {
                    result.Add(new CustomCommandInfo(tempDicItem.Key, tempDicItem.Value));
                }
            }

            rsMenu.MoveNext();
        }
        return result;
    }

    public void UpdateStartupCommandsCustom(List<CustomCommandInfo> allCommandDatas, ScriptRecordset rsStartupCommands)
    {
        var tableInfo = GetRecordset("M_TABLEINFO", "PK_M_TABLEINFO, TABLENAME", string.Format("TABLENAME = '{0}'", "R_STARTUP"), "");

        if (tableInfo == null || tableInfo.RecordCount <= 0)
            throw new ArgumentNullException("Tabel 'R_STARTUP' niet gevonden!");

        tableInfo.MoveFirst();

        var columnInfo = GetRecordset("M_COLUMNINFO", "", string.Format("COLUMNNAME = '{0}' AND FK_TABLEINFO = '{1}'", "COMMANDINFO", tableInfo.Fields["PK_M_TABLEINFO"].Value), "");

        if (columnInfo == null || columnInfo.RecordCount <= 0)
            throw new ArgumentNullException("Kolom 'COMMANDINFO' in tabel 'R_STARTUP' niet gevonden. Maak dit veld (tekenreeks) aan a.u.b.!");

        rsStartupCommands.MoveFirst();
        rsStartupCommands.UpdateWhenMoveRecord = true;

        while (!rsStartupCommands.EOF)
        {
            var commandInfo = allCommandDatas.FirstOrDefault(x => x.Guid == (Guid)rsStartupCommands.Fields["COMMAND"].Value);
            if (commandInfo != null)
                rsStartupCommands.SetFieldValue("COMMANDINFO", commandInfo.Caption);
            rsStartupCommands.MoveNext();
        }
    }

    public class MultiSelectUserForm : Form
    {
        private DataGridView dataGridView1;
        private Button btnOk;
        private Button btnCancel;
        public List<CustomCommandInfo> Result { get; set; }
        public MultiSelectUserForm(CommandScript script, IList dataTable)
        {
            InitializeComponent();
            dataGridView1.AutoGenerateColumns = true;
            dataGridView1.DataSource = dataTable;
            Result = new List<CustomCommandInfo>();
        }
        private void InitializeComponent()
        {
            dataGridView1 = new DataGridView();
            btnOk = new Button();
            btnCancel = new Button();
            ((ISupportInitialize)(dataGridView1)).BeginInit();
            SuspendLayout();
            // 
            // dataGridView1
            // 
            dataGridView1.Anchor = ((AnchorStyles)((((AnchorStyles.Top | AnchorStyles.Bottom)
                | AnchorStyles.Left)
                | AnchorStyles.Right)));
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridView1.Location = new System.Drawing.Point(12, 12);
            dataGridView1.Name = "dataGridView1";
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.Size = new System.Drawing.Size(626, 416);
            dataGridView1.TabIndex = 0;
            // 
            // btnOk
            // 
            btnOk.Anchor = AnchorStyles.Bottom;
            btnOk.Location = new System.Drawing.Point(247, 434);
            btnOk.Name = "btnOk";
            btnOk.Size = new System.Drawing.Size(75, 23);
            btnOk.TabIndex = 1;
            btnOk.Text = "Ok";
            btnOk.UseVisualStyleBackColor = true;
            btnOk.Click += new EventHandler(btnOk_Click);
            // 
            // btnCancel
            // 
            btnCancel.Anchor = AnchorStyles.Bottom;
            btnCancel.Location = new System.Drawing.Point(328, 434);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(75, 23);
            btnCancel.TabIndex = 2;
            btnCancel.Text = "Cancel";
            btnCancel.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            ClientSize = new System.Drawing.Size(650, 469);
            Controls.Add(btnCancel);
            Controls.Add(btnOk);
            Controls.Add(dataGridView1);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "Form1";
            ((ISupportInitialize)(dataGridView1)).EndInit();
            ResumeLayout(false);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                Result.Add(row.DataBoundItem as CustomCommandInfo);

            DialogResult = DialogResult.OK;
            Close();
        }
    }

    public void Execute()
    {
        var customData = GetAllCustomCommandDatas();
        var form = new MultiSelectUserForm(this, customData);

        if (form.ShowDialog() != DialogResult.OK || form.Result == null || form.Result.Count <= 0)
            return;

        var converter = new BytesConverter();
        var dicAllItems = new Dictionary<Guid, CommandData>();
        var rsMenu = GetRecordset("S_MENU", "", "SCOPE <> 1", "");
        rsMenu.MoveFirst();

        while (!rsMenu.EOF)
        {
            var tempDic = converter.ConvertFromDBValue(rsMenu.Fields["COMMANDDATALIST"].Value) as Dictionary<Guid, CommandData>;
            if (tempDic != null)
            {
                foreach (var tempDicItem in tempDic.Where(tempDicItem => !dicAllItems.ContainsKey(tempDicItem.Key)))
                {
                    dicAllItems.Add(tempDicItem.Key, tempDicItem.Value);
                }
            }

            rsMenu.MoveNext();
        }

        //var basedOnUsers = MessageBox.Show("Wil je de acties koppelen aan gebruiker(s)?", "Vraag", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
        //var basedOnRoles = !basedOnUsers && MessageBox.Show("Wil je de acties koppelen aan rol(len)?", "Vraag", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;

        var basedOnRoles = MessageBox.Show("Wil je de acties koppelen aan rol(len)?", "Vraag", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;

        /*if (basedOnUsers)
		{
			var selectedUsers = OpenMultiSelectForm("R_USER", "PK_R_USER");
			if (selectedUsers == null || selectedUsers.Length <= 0)
				return;

			ProcessUsers(selectedUsers, converter, form.Result.Select(x => x.Guid), dicAllItems);
		}
		else */
        if (basedOnRoles)
        {
            var selectedRoles = OpenMultiSelectForm("R_ROLE", "PK_R_ROLE");
            if (selectedRoles == null || selectedRoles.Length <= 0)
                return;

            ProcessRoles(selectedRoles, converter, form.Result.Select(x => x.Guid), dicAllItems);

            var rsUserStartUpItem = GetRecordset("R_ROLEUSER", "", string.Format("FK_R_ROLE IN ({0})", string.Join(",", selectedRoles)), "");
            rsUserStartUpItem.MoveFirst();

            var selectedUsers = new List<object>();
            while (!rsUserStartUpItem.EOF)
            {
                if (!selectedUsers.Contains(rsUserStartUpItem.Fields["FK_R_USER"].Value))
                    selectedUsers.Add(rsUserStartUpItem.Fields["FK_R_USER"].Value);

                rsUserStartUpItem.MoveNext();
            }

            ProcessUsers(selectedUsers, converter, form.Result.Select(x => x.Guid), dicAllItems);
        }

    }

    private void ProcessUsers(IEnumerable<object> selectedUsers, BytesConverter converter, IEnumerable selectedCommands, Dictionary<Guid, CommandData> dicCommandDatas)
    {
        foreach (var selectedUser in selectedUsers.ToList())
        {
            var rsUserMenu = GetOrCreateMenu();
            rsUserMenu.MoveFirst();
            var userCommandList = converter.ConvertFromDBValue(rsUserMenu.Fields["COMMANDDATALIST"].Value) as Dictionary<Guid, CommandData>;

            if (userCommandList == null)
                continue;

            foreach (var selectedStartUpItem in selectedCommands)
            {
                if (!userCommandList.ContainsKey((Guid)selectedStartUpItem))
                    userCommandList.Add((Guid)selectedStartUpItem, dicCommandDatas[(Guid)selectedStartUpItem]);

                var rsUserStartUpItem = GetRecordset("R_STARTUP", "",
                    string.Format("FK_USER = '{0}' AND COMMAND = '{1}'", selectedUser, (Guid)selectedStartUpItem), "");
                if (rsUserStartUpItem.RecordCount > 0)
                    continue;

                rsUserStartUpItem.AddNew();
                rsUserStartUpItem.SetFieldValue("FK_USER", selectedUser);
                rsUserStartUpItem.SetFieldValue("COMMAND", (Guid)selectedStartUpItem);
                rsUserStartUpItem.Update();
            }

            rsUserMenu.Fields["COMMANDDATALIST"].Value = converter.ConvertToDBValue(userCommandList);
            rsUserMenu.Update();
        }
    }

    private void ProcessRoles(IEnumerable<object> selectedRoles, BytesConverter converter, IEnumerable selectedCommands, Dictionary<Guid, CommandData> dicCommandDatas)
    {
        foreach (var role in selectedRoles)
        {
            var rsMenu = GetOrCreateMenu();
            rsMenu.MoveFirst();

            var userCommandList = converter.ConvertFromDBValue(rsMenu.Fields["COMMANDDATALIST"].Value) as Dictionary<Guid, CommandData>;

            if (userCommandList == null)
                return;

            foreach (var selectedStartUpItem in selectedCommands)
            {
                var command = dicCommandDatas[(Guid)selectedStartUpItem];

                if (!userCommandList.ContainsKey((Guid)selectedStartUpItem))
                    userCommandList.Add((Guid)selectedStartUpItem, command);

                var rsStartupCommandRole = GetRecordset("C_STARTUPROLE", "", string.Format("FK_ROLE = '{0}' AND COMMAND = '{1}'", role, (Guid)selectedStartUpItem), "");
                rsStartupCommandRole.MoveFirst();

                if (rsStartupCommandRole.RecordCount > 0)
                    continue;

                rsStartupCommandRole.AddNew();
                rsStartupCommandRole.SetFieldValue("FK_ROLE", role);
                rsStartupCommandRole.SetFieldValue("COMMAND", (Guid)selectedStartUpItem);
                rsStartupCommandRole.SetFieldValue("COMMANDNAME", command.Caption);
                rsStartupCommandRole.Update();
            }
        }
    }

    public ScriptRecordset GetOrCreateMenu()
    {
        var rsMenu = GetRecordset("S_MENU", "", "SCOPE = '2'", "DATECREATED DESC");

        if (rsMenu.RecordCount <= 0)
        {
            var row = rsMenu.DataTable.NewRow();
            row["PK_S_MENU"] = Guid.NewGuid();
            row["SCOPE"] = 2;
            rsMenu.DataTable.Rows.Add(row);
            rsMenu.MoveFirst();
            rsMenu.Update();
        }
        else
        {
            return rsMenu;
        }

        return GetRecordset("S_MENU", "", "SCOPE = '2'", "DATECREATED DESC");
    }

    public class CustomCommandInfo
    {
        public Guid Guid { get; set; }
        public CommandData Data { get; set; }
        public string Caption
        {
            get { return Data == null ? string.Empty : Data.Caption; }
        }

        public CustomCommandInfo(Guid guid, CommandData data)
        {
            Guid = guid;
            Data = data;
        }
    }
}
