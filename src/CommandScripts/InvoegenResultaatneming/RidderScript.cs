﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using InvoegenResultaatneming.Models;
using Ridder.Recordset.Extensions;

public class InvoegenResultaatneming_RidderScript : CommandScript
{
    public void Execute()
    {
        // HVE
        // 10-8-2021
        // Invoegen resultaatneming vanuit import.
        if (FormDataAwareFunctions == null || FormDataAwareFunctions.TableName != "C_ORDERFINANCIALSTATE")
        {
            MessageBox.Show("Dit script mag alleen vanaf de Order OHW resultaatneming-tabel aangeroepen worden.");
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        var orderFinancialStateIds = FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue()).ToList();
        var rsOrderFinancial = GetRecordset("C_ORDERFINANCIALSTATE", "", $"PK_C_ORDERFINANCIALSTATE IN ({string.Join(",", orderFinancialStateIds)}) AND PROCESSED = 0", "");

        if(rsOrderFinancial.RecordCount == 0)
        {
            MessageBox.Show("Geen records gevonden die nog verwerkt moeten worden.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        var orderFinancialStates = rsOrderFinancial
            .As<OrderFinancialState>()
            .ToList();


        if(orderFinancialStates.Any(x => !x.DATE_TAKEPROFIT.HasValue))
        {
            MessageBox.Show("Bij het maken van een resultaatneming is de datum verplicht. Vul een 'Datum resultaatneming' in.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        var rsResultaatneming = GetRecordset("C_RESULTAATNEMING", "",
            $"PK_C_RESULTAATNEMING = -1", "");
        rsResultaatneming.UpdateWhenMoveRecord = false;
        rsResultaatneming.UseDataChanges = false;

        var ordersRendementAdjusted = new List<OrderRendementAdjustment>();
        var update = false;

        foreach (var orderFinancialState in orderFinancialStates)
        {
            if (orderFinancialState.PROCESSED)
            {
                continue;
            }

            /* DB - 14-8-2024 - Uitgezet
            if (orderFinancialState.RENDEMENT_PERC > 0d)
            {
                var orderRendementAdjustment = new OrderRendementAdjustment()
                {
                    OrderId = (int)orderFinancialState.FK_ORDER,
                    RendementPerc = (double)orderFinancialState.RENDEMENT_PERC
                };
                ordersRendementAdjusted.Add(orderRendementAdjustment);
            }*/

            rsResultaatneming.AddNew();
            rsResultaatneming.Fields["FK_ORDER"].Value = orderFinancialState.FK_ORDER;
            rsResultaatneming.Fields["AMOUNT"].Value = orderFinancialState.TAKE_PROFIT;
            rsResultaatneming.Fields["DATE"].Value = orderFinancialState.DATE_TAKEPROFIT;
            update = true;

            orderFinancialState.PROCESSED = true;
            orderFinancialState.DATE_PROCESSED = DateTime.Now;
        }

        if (update)
        {
            rsResultaatneming.MoveFirst();
            rsResultaatneming.Update();
        }

        if (ordersRendementAdjusted.Any())
        {
            UpdateOrderRendement(ordersRendementAdjusted);
        }


        var updateOrderFinancialState = false;

        rsOrderFinancial.UpdateWhenMoveRecord = false;
        rsOrderFinancial.MoveFirst();
        while (!rsOrderFinancial.EOF)
        {
            var orderFinancial = orderFinancialStates
                .First(y => y.PK_C_ORDERFINANCIALSTATE ==
                            (int)rsOrderFinancial.Fields["PK_C_ORDERFINANCIALSTATE"].Value);

            rsOrderFinancial.SetFieldValue("PROCESSED", orderFinancial.PROCESSED);
            rsOrderFinancial.SetFieldValue("DATE_PROCESSED", orderFinancial.DATE_PROCESSED);
            updateOrderFinancialState = true;
            rsOrderFinancial.MoveNext();
        }

        if (updateOrderFinancialState)
        {
            rsOrderFinancial.MoveFirst();
            rsOrderFinancial.Update();
        }

        var processed = orderFinancialStates.Count(y => y.TAKE_PROFIT > 0d);
        MessageBox.Show($"Resultaatneming verwerkt voor {processed} orders.", "Ridder iQ - Verwerken resultaatnemingen",
            MessageBoxButtons.OK, MessageBoxIcon.Information);

    }

    private void UpdateOrderRendement(List<OrderRendementAdjustment> ordersRendementAdjusted)
    {
        var orderIds = ordersRendementAdjusted
            .Select(x => x.OrderId)
            .ToList();

        var rsOrder = GetRecordset("R_ORDER", "", $"PK_R_ORDER IN ({string.Join(",", orderIds)})", "");
        rsOrder.UpdateWhenMoveRecord = false;
        rsOrder.MoveFirst();

        var update = false;

        while (!rsOrder.EOF)
        {
            var orderRendementAdjustment = ordersRendementAdjusted
                .First(y => y.OrderId == (int)rsOrder.Fields["PK_R_ORDER"].Value);

            var rendemnt_perc = rsOrder.Fields["RENDEMENT_PERC"].Value as double? ?? 0d;
            if (rendemnt_perc != orderRendementAdjustment.RendementPerc)
            {
                rsOrder.SetFieldValue("RENDEMENT_PERC", orderRendementAdjustment.RendementPerc);
                update = true;
            }

            rsOrder.MoveNext();
        }

        if (update)
        {
            rsOrder.MoveFirst();
            rsOrder.Update();
        }
    }
}
