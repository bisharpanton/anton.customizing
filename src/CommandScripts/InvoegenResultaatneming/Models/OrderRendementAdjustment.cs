﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoegenResultaatneming.Models
{
    public class OrderRendementAdjustment
    {
        public int OrderId { get; set; }
        public double RendementPerc { get; set; }
    }
}
