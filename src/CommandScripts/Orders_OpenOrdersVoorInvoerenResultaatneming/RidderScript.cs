﻿using ADODB;
using Ridder.Client.SDK.SDKParameters;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class Orders_OpenOrdersVoorInvoerenResultaatneming_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //14-8-2024
        //Open orders voor invoeren resultaatneming, indien een projectleider/projectmanager dit doet, open dan alleen zijn orders

        var currentUserIsProjectManager = DetermineCurrentUserIsProjectManager();

        var projectManagerFilter = string.Empty;

        if(currentUserIsProjectManager)
        {
            var employeeId = GetRecordset("R_USER", "FK_EMPLOYEE", $"PK_R_USER = {CurrentUserId}", "")
                .DataTable.AsEnumerable().First().Field<int?>("FK_EMPLOYEE") ?? 0;

            projectManagerFilter = $"FK_PLANNER = {employeeId}";
        }

        var browsePar = new BrowseParameters()
        {
            TableName = "R_ORDER",
            BaseLayoutName = "Invoerscherm t.b.v. resultaatneming",
            Filter = projectManagerFilter,
        };

        OpenBrowse(browsePar, false);
    }

    private bool DetermineCurrentUserIsProjectManager()
    {
        var projectManagerRoles = GetRecordset("R_ROLE", "PK_R_ROLE", $"NAME IN ('Projectleider','Projectmanager')", "")
            .DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_ROLE")).ToList();

        if(!projectManagerRoles.Any())
        {
            return false;
        }

        return GetRecordset("R_ROLEUSER", "PK_R_ROLEUSER", $"FK_R_USER = {CurrentUserId} AND FK_R_ROLE IN ({string.Join(",", projectManagerRoles)}) ", "").RecordCount > 0;
    }
}
