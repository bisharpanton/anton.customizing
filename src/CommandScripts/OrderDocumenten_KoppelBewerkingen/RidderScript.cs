﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class OrderDocumenten_KoppelBewerkingen_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //18-8-2023
        //Koppel bewerkingen

        if(FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("R_ORDERDOCUMENT"))
        {
            MessageBox.Show("Dit script mag alleen vanaf de orderdocumenten aangeroepen worden.");
            return;
        }

        if(!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        if(FormDataAwareFunctions.GetSelectedRecords().Count() > 1)
        {
            MessageBox.Show("Dit script mag voor één document tegelijk aangeroepen worden.");
            return;
        }

        var orderDocumentId = (int)FormDataAwareFunctions.GetSelectedRecords().First().GetPrimaryKeyValue();

        var alreadyLinkedWorkactivities = GetRecordset("U_WORKACTIVITIESPERORDERDOCUMENT", "FK_WORKACTIVITY",
            $"FK_ORDERDOCUMENT = {orderDocumentId}", "").DataTable.AsEnumerable().Select(x => x.Field<int>("FK_WORKACTIVITY")).ToList();

        var allDirectWorkactivities = GetRecordset("R_WORKACTIVITY", "PK_R_WORKACTIVITY", "WORKACTIVITYTYPE = 16", "")
            .DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_WORKACTIVITY")).ToList();

        var availableWorkactivities = allDirectWorkactivities.Except(alreadyLinkedWorkactivities).ToList();

        if(!availableWorkactivities.Any())
        {
            MessageBox.Show("Alle beschikbare bewerkingen zijn al gekoppeld.");
            return;
        }

        var resultWorkactivities = OpenMultiSelectForm("R_WORKACTIVITY", "PK_R_WORKACTIVITY", $"PK_R_WORKACTIVITY IN ({string.Join(",", availableWorkactivities)})", "ChooseWorkactivity");

        if(resultWorkactivities == null)
        {
            return;
        }

        CreateRecords(orderDocumentId, resultWorkactivities);
        DeleteDeletedDocumentsRecords(orderDocumentId, resultWorkactivities);
        TriggerDataChanged(orderDocumentId);

        FormDataAwareFunctions.Refres();
    }

    private void DeleteDeletedDocumentsRecords(int orderDocumentId, object[] resultWorkactivities)
    {
        var orderDocument = GetRecordset("R_ORDERDOCUMENT", "FK_ORDER, FK_DOCUMENT", $"PK_R_ORDERDOCUMENT = {orderDocumentId}", "")
            .DataTable.AsEnumerable().First();

        var joborderDetailWorkactivities = GetRecordset("R_JOBORDERDETAILWORKACTIVITY", "PK_R_JOBORDERDETAILWORKACTIVITY",
            $"FK_ORDER = {orderDocument.Field<int>("FK_ORDER")} AND FK_WORKACTIVITY IN ({string.Join(",", resultWorkactivities)})", "")
            .DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_JOBORDERDETAILWORKACTIVITY")).ToList();

        if(!joborderDetailWorkactivities.Any())
        {
            return;
        }

        var rs = GetRecordset("U_DELETEDDOCUMENTS", "", 
            $"DOCUMENTID = {orderDocument.Field<int>("FK_DOCUMENT")} AND TABLENAME = 'R_JOBORDERDETAILWORKACTIVITY' AND RECORDID IN ({string.Join(",", joborderDetailWorkactivities)})", "");

        if(rs.RecordCount == 0)
        {
            return;
        }

        rs.UpdateWhenMoveRecord = false;

        rs.MoveFirst();
        while (!rs.EOF)
        {
            rs.Delete();
            rs.MoveNext();
        }

        rs.MoveFirst();
        rs.Update();

    }

    private void TriggerDataChanged(int orderDocumentId)
    {
        var documentId = GetRecordset("R_ORDERDOCUMENT", "FK_DOCUMENT", $"PK_R_ORDERDOCUMENT = {orderDocumentId}", "")
            .DataTable.AsEnumerable().First().Field<int>("FK_DOCUMENT");

        var rsDocument = GetRecordset("R_DOCUMENT", "DUMMYREFRESHDATECHANGED", $"PK_R_DOCUMENT = {documentId}", "");
        rsDocument.MoveFirst();
        rsDocument.Fields["DUMMYREFRESHDATECHANGED"].Value = !(bool)rsDocument.GetField("DUMMYREFRESHDATECHANGED").Value;
        rsDocument.Update();
    }

    private void CreateRecords(int orderDocumentId, object[] resultWorkactivities)
    {
        var rs = GetRecordset("U_WORKACTIVITIESPERORDERDOCUMENT", "", "PK_U_WORKACTIVITIESPERORDERDOCUMENT IS NULL", "");
        
        foreach(var workactivityId in resultWorkactivities)
        {
            rs.AddNew();
            rs.SetFieldValue("FK_ORDERDOCUMENT", orderDocumentId);
            rs.SetFieldValue("FK_WORKACTIVITY", workactivityId);
        }

        rs.Update();
    }
}
