﻿using ADODB;
using Afas_VerzamelOverurenTbvExportNaarAfas;
using Afas_VerzamelOverurenTbvExportNaarAfas.Models;
using Ridder.Client.SDK.SDKParameters;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Recordset.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class Afas_VerzamelOverurenTbvExportNaarAfas_RidderScript : CommandScript
{
    public void Execute()
    {

        //DB
        //4-5-2021
        //Verzamel overuren van periode en maak hiervoor regels aan in de tabel

        var formDates = new FormDates();

        var dialogResult = formDates.ShowDialog();

        if (dialogResult != DialogResult.OK)
        {
            return;
        }

        var projectTimesToExport = DetermineProjectTimesToExport(formDates.StartDate, formDates.EndDate);

        if (!projectTimesToExport.Any())
        {
            MessageBox.Show("Geen uren gevonden om te exporteren naar AFAS.", "Ridder iQ",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        var createdToExportProjectTimes = CreateToExportProjectTimes(projectTimesToExport, formDates.StartDate, formDates.EndDate);

        if (!UpdateProjectTimes(projectTimesToExport, createdToExportProjectTimes))
        {
            return;
        }

        OpenCreatedRecords(createdToExportProjectTimes);
    }

    private void OpenCreatedRecords(List<CreatedToExportProjectTime> createdToExportProjectTimes)
    {
        var browsePar = new BrowseParameters()
        {
            TableName = "C_TOEXPORTPROJECTTIMES",
            Filter = $"PK_C_TOEXPORTPROJECTTIMES IN ({string.Join(",", createdToExportProjectTimes.Select(x => x.Pk))})",
        };

        OpenBrowse(browsePar, false);
    }

    private bool UpdateProjectTimes(List<ProjectTime> projectTimesToExport, List<CreatedToExportProjectTime> createdToExportProjectTimes)
    {
        var rsProjectTimes = GetRecordset("R_PROJECTTIME", "PK_R_PROJECTTIME, DATE, FK_EMPLOYEE, FK_OVERTIMECODE, FK_TOEXPORTPROJECTTIMES",
            $"PK_R_PROJECTTIME IN ({string.Join(",", projectTimesToExport.Select(x => x.PK_R_PROJECTTIME))})", "");
        rsProjectTimes.UpdateWhenMoveRecord = false;
        rsProjectTimes.UseDataChanges = false;


        rsProjectTimes.MoveFirst();
        while (!rsProjectTimes.EOF)
        {
            var year = ((DateTime)rsProjectTimes.GetField("DATE").Value).Year;
            var month = ((DateTime)rsProjectTimes.GetField("DATE").Value).Month;
            var employeeId = (int)rsProjectTimes.GetField("FK_EMPLOYEE").Value;
            var overTimeCodeId = rsProjectTimes.GetField("FK_OVERTIMECODE").Value as int? ?? 0;

            var createdToExportProjectTime = createdToExportProjectTimes
                .FirstOrDefault(x => x.Year == year && x.Month == month && x.EmployeeId == employeeId && (x.OverTimeId ?? 0) == overTimeCodeId);

            if (createdToExportProjectTime == null)
            {
                rsProjectTimes.MoveNext();
                continue;
            }

            rsProjectTimes.SetFieldValue("FK_TOEXPORTPROJECTTIMES", createdToExportProjectTime.Pk);

            rsProjectTimes.MoveNext();
        }

        rsProjectTimes.MoveFirst();
        var updateResult = rsProjectTimes.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Bijwerken urenboekingen is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);

            return false;
        }

        return true;
    }

    private List<CreatedToExportProjectTime> CreateToExportProjectTimes(List<ProjectTime> projectTimesToExport, DateTime startDate, DateTime endDate)
    {
        var rsProjectTimesToExport = GetRecordset("C_TOEXPORTPROJECTTIMES", "", "PK_C_TOEXPORTPROJECTTIMES IS NULL", "");
        rsProjectTimesToExport.UpdateWhenMoveRecord = false;
        rsProjectTimesToExport.UseDataChanges = true;

        //Fill Group by
        var idOverTimeNorm = GetRecordset("R_TIMECODE", "PK_R_TIMECODE", "CODE = 'NORM'", "")
            .DataTable.AsEnumerable().FirstOrDefault()?.Field<int>("PK_R_TIMECODE") ?? 0;

        foreach (var projectTime in projectTimesToExport)
        {
            projectTime.GROUPBY = (projectTime.FK_OVERTIMECODE ?? 0) == idOverTimeNorm
                ? $"{projectTime.YEAR}_{projectTime.MONTH:D2}_{projectTime.FK_EMPLOYEE}_{0}"
                : $"{projectTime.YEAR}_{projectTime.MONTH:D2}_{projectTime.FK_EMPLOYEE}_{projectTime.FK_OVERTIMECODE ?? 0}";
        }

        var projectTimesToExportGrouped = projectTimesToExport.GroupBy(x => x.GROUPBY).ToList();

        foreach (var group in projectTimesToExportGrouped)
        {
            var projectTimesToExportThisGroup = projectTimesToExport.Where(x => x.GROUPBY == group.Key).ToList();


            rsProjectTimesToExport.AddNew();
            rsProjectTimesToExport.SetFieldValue("YEAR", projectTimesToExportThisGroup.First().YEAR);
            rsProjectTimesToExport.SetFieldValue("MONTH", projectTimesToExportThisGroup.First().MONTH);
            rsProjectTimesToExport.SetFieldValue("DATEWAGECOMPONENT", DateTime.Now);
            rsProjectTimesToExport.SetFieldValue("FK_EMPLOYEE", projectTimesToExportThisGroup.First().FK_EMPLOYEE);

            if(!projectTimesToExportThisGroup.First().FK_OVERTIMECODE.HasValue || projectTimesToExportThisGroup.First().FK_OVERTIMECODE == idOverTimeNorm)
            {
                rsProjectTimesToExport.SetFieldValue("FK_OVERTIMECODE", DBNull.Value);
            }
            else
            {
                rsProjectTimesToExport.SetFieldValue("FK_OVERTIMECODE", projectTimesToExportThisGroup.First().FK_OVERTIMECODE);
            }

            
            rsProjectTimesToExport.SetFieldValue("HOURS", Math.Round(projectTimesToExportThisGroup.Sum(x => TimeSpan.FromTicks(x.TIMEEMPLOYEE).TotalHours), 2));
            rsProjectTimesToExport.SetFieldValue("STARTPERIOD", startDate);
            rsProjectTimesToExport.SetFieldValue("ENDPERIOD", endDate);
        }

        //Maak bij oproepkracht extra regel aan voor aantal dagen
        var projectTimesCallUpEmployeeIds = projectTimesToExport.Where(x => x.IsCallUpEmployee).Select(x => x.FK_EMPLOYEE).Distinct().ToList();

        foreach (var callUpEmployeeId in projectTimesCallUpEmployeeIds)
        {
            var projectTimesThisEmployee = projectTimesToExport.Where(x => x.FK_EMPLOYEE == callUpEmployeeId).ToList();

            rsProjectTimesToExport.AddNew();
            rsProjectTimesToExport.SetFieldValue("YEAR", projectTimesThisEmployee.First().YEAR);
            rsProjectTimesToExport.SetFieldValue("MONTH", projectTimesThisEmployee.First().MONTH);
            rsProjectTimesToExport.SetFieldValue("DATEWAGECOMPONENT", DateTime.Now);
            rsProjectTimesToExport.SetFieldValue("FK_EMPLOYEE", callUpEmployeeId);
            rsProjectTimesToExport.SetFieldValue("FK_OVERTIMECODE", DBNull.Value);

            rsProjectTimesToExport.SetFieldValue("DAYS", projectTimesThisEmployee.Select(x => x.DATE.Date).Distinct().Count());
            rsProjectTimesToExport.SetFieldValue("STARTPERIOD", startDate);
            rsProjectTimesToExport.SetFieldValue("ENDPERIOD", endDate);
        }

        var updateResult = rsProjectTimesToExport.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Aanmaken te exporteren overuren naar AFAS is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);

            return new List<CreatedToExportProjectTime>();
        }

        return rsProjectTimesToExport.DataTable.AsEnumerable().Select(x => new CreatedToExportProjectTime()
        {
            Pk = x.Field<int>("PK_C_TOEXPORTPROJECTTIMES"),
            Year = x.Field<int>("YEAR"),
            Month = x.Field<int>("MONTH"),
            EmployeeId = x.Field<int>("FK_EMPLOYEE"),
            OverTimeId = x.Field<int?>("FK_OVERTIMECODE") 
        }).ToList();
    }


    private List<ProjectTime> DetermineProjectTimesToExport(DateTime startDate, DateTime endDate)
    {
        var timeCodes = GetRecordset("R_TIMECODE", "", "AFASWAGECOMPONENTCODE <> ''", "").As<TimeCode>().ToList();

        var callUpEmployees = GetRecordset("R_EMPLOYEE", "PK_R_EMPLOYEE",
                $"EMPLOYEECATEGORY = '{(int)EmployeeCategory.Oproepkracht}'", "").DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_EMPLOYEE")).ToList();
        callUpEmployees.Add(0); //Avoid empty list

        if (!timeCodes.Any())
        {
            MessageBox.Show("Geen overuursoorten gevonden met een AFAS looncomponent code ingevuld.", "Ridder iQ",
                MessageBoxButtons.OK, MessageBoxIcon.Information);

            return new List<ProjectTime>();
        }

        var result =  GetRecordset("R_PROJECTTIME", "",
            $"DATE >= '{startDate.Date:yyyyMMdd}' AND DATE <= '{endDate.Date:yyyyMMdd}' AND FK_TOEXPORTPROJECTTIMES IS NULL AND CLOSED = 1 AND TIMEOFINLIEU = 0 " +
            $"AND (FK_OVERTIMECODE IN ({string.Join(",", timeCodes.Select(x => x.PK_R_TIMECODE))}) OR FK_EMPLOYEE IN ({string.Join(",", callUpEmployees)}))", "")
            .As<ProjectTime>().OrderBy(x => x.DATE).ToList();


        result.ForEach(x => x.IsCallUpEmployee = callUpEmployees.Contains(x.FK_EMPLOYEE));

        return result;
    }

    enum EmployeeCategory
    {
        Algemeen = 1,
        Indirect = 2,
        IndirectAIO = 3,
        ZZP = 4,
        Uitzendkracht = 5,
        Oproepkracht = 6
    }
}
