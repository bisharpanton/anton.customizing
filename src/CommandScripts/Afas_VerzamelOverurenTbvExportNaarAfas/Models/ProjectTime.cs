﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afas_VerzamelOverurenTbvExportNaarAfas.Models
{
    partial class ProjectTime
    {
        public int YEAR => DATE.Year;
        public int MONTH => DATE.Month;
        public string GROUPBY { get; set; }
        public bool IsCallUpEmployee { get; set; }
    }
}
