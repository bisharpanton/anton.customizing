//  <auto-generated />
namespace Afas_VerzamelOverurenTbvExportNaarAfas.Models
{
    using System;
    
    
    ///  <summary>
    ///  	 Table: C_TOEXPORTPROJECTTIMES
    ///  </summary>
    ///  <remarks>
    /// 	Available columns (for debugging, not necessarily available in this class):
    ///  	CREATOR, DATECHANGED, DATECREATED, DATEEXPORTED, DATEWAGECOMPONENT, EXPORTED, EXTERNALKEY, FK_EMPLOYEE, FK_EXPORTEDBY, FK_OVERTIMECODE
    ///  	FK_WORKFLOWSTATE, HOURS, MONTH, PK_C_TOEXPORTPROJECTTIMES, RECORDLINK, USERCHANGED, YEAR
    ///  
    ///  	Filter used: FK_TABLEINFO = '9232d851-a304-466a-96bd-c64ef629c6ae' AND (ISSECRET = 0)
    ///  	Included columns: 
    ///  	Excluded columns: 
    ///  </remarks>
    public partial class ToExportProjectTime
    {
        
        private string _CREATOR;
        
        private System.Nullable<System.DateTime> _DATECHANGED;
        
        private System.Nullable<System.DateTime> _DATECREATED;
        
        private System.Nullable<System.DateTime> _DATEEXPORTED;
        
        private System.Nullable<System.DateTime> _DATEWAGECOMPONENT;
        
        private bool _EXPORTED;
        
        private string _EXTERNALKEY;
        
        private System.Nullable<int> _FK_EMPLOYEE;
        
        private System.Nullable<int> _FK_EXPORTEDBY;
        
        private System.Nullable<int> _FK_OVERTIMECODE;
        
        private System.Nullable<System.Guid> _FK_WORKFLOWSTATE;
        
        private System.Nullable<double> _HOURS;
        
        private System.Nullable<int> _MONTH;
        
        private int _PK_C_TOEXPORTPROJECTTIMES;
        
        private string _RECORDLINK;
        
        private string _USERCHANGED;
        
        private System.Nullable<int> _YEAR;
        
        /// <summary>
        /// 	Aangemaakt door
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        public virtual string CREATOR
        {
            get
            {
                if (string.IsNullOrEmpty(this._CREATOR))
                {
                    return "";
                }
                return this._CREATOR;
            }
            set
            {
                this._CREATOR = value;
            }
        }
        
        /// <summary>
        /// 	Datum gewijzigd
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        public virtual System.Nullable<System.DateTime> DATECHANGED
        {
            get
            {
                return this._DATECHANGED;
            }
            set
            {
                this._DATECHANGED = value;
            }
        }
        
        /// <summary>
        /// 	Datum aangemaakt
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        public virtual System.Nullable<System.DateTime> DATECREATED
        {
            get
            {
                return this._DATECREATED;
            }
            set
            {
                this._DATECREATED = value;
            }
        }
        
        /// <summary>
        /// 	Datum geëxporteerd
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        public virtual System.Nullable<System.DateTime> DATEEXPORTED
        {
            get
            {
                return this._DATEEXPORTED;
            }
            set
            {
                this._DATEEXPORTED = value;
            }
        }
        
        /// <summary>
        /// 	Datum looncomponent
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        public virtual System.Nullable<System.DateTime> DATEWAGECOMPONENT
        {
            get
            {
                return this._DATEWAGECOMPONENT;
            }
            set
            {
                this._DATEWAGECOMPONENT = value;
            }
        }
        
        /// <summary>
        /// 	Geëxporteerd
        /// </summary>
        /// <remarks>
        /// 	DataType: Boolean
        /// 	Size: 1
        /// </remarks>
        public virtual bool EXPORTED
        {
            get
            {
                return this._EXPORTED;
            }
            set
            {
                this._EXPORTED = value;
            }
        }
        
        /// <summary>
        /// 	Externe sleutel
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 100
        /// </remarks>
        public virtual string EXTERNALKEY
        {
            get
            {
                if (string.IsNullOrEmpty(this._EXTERNALKEY))
                {
                    return "";
                }
                return this._EXTERNALKEY;
            }
            set
            {
                this._EXTERNALKEY = value;
            }
        }
        
        /// <summary>
        /// 	Werknemer
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: C_TOEXPORTPROJECTTIMES
        /// </remarks>
        public virtual System.Nullable<int> FK_EMPLOYEE
        {
            get
            {
                return this._FK_EMPLOYEE;
            }
            set
            {
                this._FK_EMPLOYEE = value;
            }
        }
        
        /// <summary>
        /// 	Geëxporteerd door
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: C_TOEXPORTPROJECTTIMES
        /// </remarks>
        public virtual System.Nullable<int> FK_EXPORTEDBY
        {
            get
            {
                return this._FK_EXPORTEDBY;
            }
            set
            {
                this._FK_EXPORTEDBY = value;
            }
        }
        
        /// <summary>
        /// 	Overuursoort
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: C_TOEXPORTPROJECTTIMES
        /// </remarks>
        public virtual System.Nullable<int> FK_OVERTIMECODE
        {
            get
            {
                return this._FK_OVERTIMECODE;
            }
            set
            {
                this._FK_OVERTIMECODE = value;
            }
        }
        
        /// <summary>
        /// 	Workflowstatus
        /// </summary>
        /// <remarks>
        /// 	DataType: Guid
        /// 	Size: 4
        /// 	Table: C_TOEXPORTPROJECTTIMES
        /// </remarks>
        public virtual System.Nullable<System.Guid> FK_WORKFLOWSTATE
        {
            get
            {
                return this._FK_WORKFLOWSTATE;
            }
            set
            {
                this._FK_WORKFLOWSTATE = value;
            }
        }
        
        /// <summary>
        /// 	Aantal uur
        /// </summary>
        /// <remarks>
        /// 	DataType: Float
        /// 	Size: 8
        /// </remarks>
        public virtual System.Nullable<double> HOURS
        {
            get
            {
                return this._HOURS;
            }
            set
            {
                this._HOURS = value;
            }
        }
        
        /// <summary>
        /// 	Maand
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        public virtual System.Nullable<int> MONTH
        {
            get
            {
                return this._MONTH;
            }
            set
            {
                this._MONTH = value;
            }
        }
        
        /// <summary>
        /// 	Te exporteren overuren naar AFAS id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        public virtual int PK_C_TOEXPORTPROJECTTIMES
        {
            get
            {
                return this._PK_C_TOEXPORTPROJECTTIMES;
            }
            set
            {
                this._PK_C_TOEXPORTPROJECTTIMES = value;
            }
        }
        
        /// <summary>
        /// 	Recordlink
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        public virtual string RECORDLINK
        {
            get
            {
                if (string.IsNullOrEmpty(this._RECORDLINK))
                {
                    return "";
                }
                return this._RECORDLINK;
            }
            set
            {
                this._RECORDLINK = value;
            }
        }
        
        /// <summary>
        /// 	Gewijzigd door
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        public virtual string USERCHANGED
        {
            get
            {
                if (string.IsNullOrEmpty(this._USERCHANGED))
                {
                    return "";
                }
                return this._USERCHANGED;
            }
            set
            {
                this._USERCHANGED = value;
            }
        }
        
        /// <summary>
        /// 	Jaar
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        public virtual System.Nullable<int> YEAR
        {
            get
            {
                return this._YEAR;
            }
            set
            {
                this._YEAR = value;
            }
        }
    }
}
