﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Afas_VerzamelOverurenTbvExportNaarAfas
{
    public partial class FormDates : Form
    {
        public FormDates()
        {
            InitializeComponent();

            var currentDatePreviousMonth = DateTime.Now.AddMonths(-1);
            dtpStart.Value = new DateTime(currentDatePreviousMonth.Year, currentDatePreviousMonth.Month, 1);
            dtpEnd.Value = new DateTime(currentDatePreviousMonth.Year, currentDatePreviousMonth.Month, DateTime.DaysInMonth(currentDatePreviousMonth.Year, currentDatePreviousMonth.Month));
        }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if(dtpStart.Value > dtpEnd.Value)
            {
                MessageBox.Show("Startdatum kan niet voor einddatum liggen.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }


            StartDate = dtpStart.Value;
            EndDate = dtpEnd.Value;

            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
