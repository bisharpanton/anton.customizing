﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Drawing;
using Ridder.Client.SDK.SDKParameters;

public class CreditSafe_StartEnProcessForm_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //29-4-2020
        //Open form om te zoeken naar CreditSafe relatie en verwerk daarna de gekozen relatie naar CreditSafe request tabel

        var formId = DeletePreviousFormThisUserAndCreateNew();

        if (formId == 0)
        {
            return;
        }

        OpenCreatedForm(formId);

        var formResult = GetFormResult(formId);

        if (!formResult.Process || formResult.CreatedRelationId == 0)
        {
            return;
        }

        CheckIbansCreatedRelation(formResult.CreatedRelationId);

        OpenCreatedRelation(formResult.CreatedRelationId);
    }

    private void CheckIbansCreatedRelation(int createdRelationId)
    {
        var relation = GetRecordset("R_RELATION", "IBAN, IBANGACCOUNT",
            $"PK_R_RELATION = {createdRelationId}", "").DataTable.AsEnumerable().First();

        CheckIbanCreatedRelation(createdRelationId, "IBAN", relation.Field<string>("IBAN"));
        CheckIbanCreatedRelation(createdRelationId, "IBANGACCOUNT", relation.Field<string>("IBANGACCOUNT"));
    }

    private void CheckIbanCreatedRelation(int createdRelationId, string columnname, string iban)
    {
        if (iban == "")
        {
            return; //Stoppen als IBAN leeg is
        }

        var rsRelation = GetRecordset("R_RELATION", "NAME",
           $"{columnname} = '{iban}' AND PK_R_RELATION <> {createdRelationId}", "");

        if (rsRelation.RecordCount > 0)
        {
            MessageBox.Show($"Let op! IBAN - {iban} wordt al gebruikt bij relatie(s) {string.Join(", ", rsRelation.DataTable.AsEnumerable().Select(x => x.Field<string>("NAME")).ToList())}", 
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }

    private void OpenCreatedRelation(int createdRelationId)
    {
        var editPar = new EditParameters() { TableName = "R_RELATION", Id = createdRelationId, };

        OpenEdit(editPar, false);
    }

    private FormResult GetFormResult(int formId)
    {
        var rsForm = GetRecordset("C_FORMSEARCHRELATIONONCREDITSAFE", "PROCESS, CREATEDRELATIONID",
            $"PK_C_FORMSEARCHRELATIONONCREDITSAFE = {formId}", "");
        rsForm.MoveFirst();
        return new FormResult()
        {
            Process = (bool)rsForm.Fields["PROCESS"].Value,
            CreatedRelationId = rsForm.Fields["CREATEDRELATIONID"].Value as int? ?? 0,
        };  
    }

    private void OpenCreatedForm(int formId)
    {
        var editPar = new EditParameters()
        {
            TableName = "C_FORMSEARCHRELATIONONCREDITSAFE",
            Id = formId,
            Context = "FormCreditSafe",
        };

        OpenEdit(editPar, true);
    }

    private int DeletePreviousFormThisUserAndCreateNew()
    {
        var rsFormCreditSafeSearch = GetRecordset("C_FORMSEARCHRELATIONONCREDITSAFE", "",
            $"FK_USER = {CurrentUserId}", "");
        rsFormCreditSafeSearch.UseDataChanges = false;

        if(rsFormCreditSafeSearch.RecordCount > 0)
        {
            rsFormCreditSafeSearch.MoveFirst();
            rsFormCreditSafeSearch.Delete();
        }

        rsFormCreditSafeSearch.AddNew();
        rsFormCreditSafeSearch.Fields["FK_USER"].Value = CurrentUserId;
        var updateResult = rsFormCreditSafeSearch.Update2();

        if (updateResult != null && updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Aanmaken nieuw CreditSafe zoek formulier is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return 0;
        }

        return (int)updateResult.First().PrimaryKey;
    }

    class FormResult
    {
        public bool Process { get; set; }
        public int CreatedRelationId { get; set; }
    }
}
