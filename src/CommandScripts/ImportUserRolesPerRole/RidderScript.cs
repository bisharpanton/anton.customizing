﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class ImportUsersPerRole_RidderScript : CommandScript
{
    public void Execute()
    {
        //SP
        //4-5-2023
        //Importeer gebruikers per rol

        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("R_ROLE", StringComparison.OrdinalIgnoreCase))
        {
            MessageBox.Show("Dit script mag alleen van de Rollen-tabel aangeroepen worden.");
            return;
        }

        var roleId = FormDataAwareFunctions.GetSelectedRecords().First().GetPrimaryKeyValue();

        object[] selectie = null;
        selectie = OpenMultiSelectForm("R_USER", "PK_R_USER",
            "INACTIVE = 0 AND USERNAME NOT LIKE 'SDK_%'", "");

        if (selectie == null)
        {
            return;
        }

        ImportUsersPerRole(selectie, (int)roleId);

    }
    private void ImportUsersPerRole(object[] selectie, int roleId)
    {
        var rsRoleUser = GetRecordset("R_ROLEUSER", "",
            "PK_R_ROLEUSER = -1", "");
        rsRoleUser.UseDataChanges = true;
        rsRoleUser.UpdateWhenMoveRecord = false;

        foreach (var userId in selectie)
        {
            rsRoleUser.AddNew();
            rsRoleUser.Fields["FK_R_USER"].Value = userId;
            rsRoleUser.Fields["FK_R_ROLE"].Value = roleId;
        }

        var updateResult = rsRoleUser.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Het importeren van gebruikers per rol is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

    }
}
