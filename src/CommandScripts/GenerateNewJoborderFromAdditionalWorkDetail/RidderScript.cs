﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;
using Ridder.Common.ProjectManagement;

public class GenerateNewJoborderFromAdditionalWorkDetail_RidderScript : CommandScript
{
    public void Execute()
    {
        //SP
        //21-7-2022
        //Genereer nieuwe bon, projectfase en budgetregel vanuit meerwerk details

        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("U_ADDITIONALWORKDETAILS", StringComparison.OrdinalIgnoreCase))
        {
            MessageBox.Show("Dit script mag alleen van de meerwerk details tabel aangeroepen worden.");
        }

        IRecord[] records = FormDataAwareFunctions.GetSelectedRecords();

        if (records.Length > 1)
        {
            MessageBox.Show("Dit script is voor één meerwerk detail tegelijk aan te roepen.", "Controle", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        int id = (int)records[0].GetPrimaryKeyValue();
        
        if(id == -1)
        {
            MessageBox.Show("Sla eerst de meerwerk regel op.", "Controle", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        var additionalWorkDetail = GetRecordset("U_ADDITIONALWORKDETAILS", "FK_OFFER, FK_JOBORDER, DESCRIPTION",
            $"PK_U_ADDITIONALWORKDETAILS = {id}", "")
            .DataTable.AsEnumerable().Select(x => new AddWorkDetail()
            {
                OfferId = x.Field<int?>("FK_OFFER"),
                JoborderId = x.Field<int?>("FK_JOBORDER"),
                Description = x.Field<string>("DESCRIPTION"),

            }).First();

        if(!(additionalWorkDetail.OfferId).HasValue)
        {
            MessageBox.Show("Aan deze meerwerkregel is geen offerte gekoppeld.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        if ((additionalWorkDetail.JoborderId).HasValue)
        {
            MessageBox.Show("Aan deze meerwerkregel is al een bon gekoppeld.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        var mainProject = GetMainProject((int)additionalWorkDetail.OfferId);

        if (mainProject == null)
        {
            return;
        }

        if(mainProject.ChangeNo == "" && GetUserInfo().DatabaseName == "Anton Rail & Infra")
        {
            MessageBox.Show("Wijzigingsnummer in de verkoopofferte ontbreekt.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        var createdJoborderId = CreateJoborder(mainProject, additionalWorkDetail);

        if(createdJoborderId == 0)
        {
            return;
        }

        var joborderNumber = GetRecordset("R_JOBORDER", "JOBORDERNUMBER",
            $"PK_R_JOBORDER = {createdJoborderId}", "").DataTable.AsEnumerable().First().Field<int>("JOBORDERNUMBER");

        var createdProjectPhaseId = CreateProjectPhase(mainProject, additionalWorkDetail, joborderNumber);

        if (createdProjectPhaseId == 0)
        {
            return;
        }

        var createdProjectBudgetDetailId = CreateProjectBudgetDetail(mainProject, additionalWorkDetail);

        if (createdProjectBudgetDetailId == 0)
        {
            return;
        }

        UpdateWbs(mainProject, additionalWorkDetail, createdProjectPhaseId, createdProjectBudgetDetailId, createdJoborderId);

        UpdateAddWorkDetail(id, createdJoborderId);
    }

    private void UpdateAddWorkDetail(int addWorkId, int joborderId)
    {
        var rsAddWorkDetail = GetRecordset("U_ADDITIONALWORKDETAILS", "FK_JOBORDER",
            $"PK_U_ADDITIONALWORKDETAILS = {addWorkId}", "");
        rsAddWorkDetail.UseDataChanges = true;
        rsAddWorkDetail.UpdateWhenMoveRecord = false;

        rsAddWorkDetail.MoveFirst();
        rsAddWorkDetail.Fields["FK_JOBORDER"].Value = joborderId;

        var updateResult = rsAddWorkDetail.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Het koppelen van de aangemaakte bon is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }
    }

    private void UpdateWbs(MainProject mainProject, AddWorkDetail addWorkDetail, int projectPhaseId, int projectBudgetDetailId, int joborderId)
    {
        var rsWBS = GetRecordset("R_WORKBREAKDOWNINFO", "", "PK_R_WORKBREAKDOWNINFO = -1", "");
        rsWBS.UseDataChanges = false;
        rsWBS.UpdateWhenMoveRecord = false;

        //Koppel fase onder order
        rsWBS.AddNew();
        rsWBS.Fields["FK_MAINPROJECT"].Value = mainProject.MainProjectId;
        rsWBS.Fields["FK_PARENTTABLEINFO"].Value = WBSTableInfo.Order;
        rsWBS.Fields["PARENTRECORDID"].Value = mainProject.OrderId;
        rsWBS.Fields["FK_TABLEINFO"].Value = WBSTableInfo.ProjectPhase;
        rsWBS.Fields["RECORDID"].Value = projectPhaseId;
        rsWBS.Fields["POSITION"].Value = 0;

        //Koppel bon onder fase
        rsWBS.AddNew();
        rsWBS.Fields["FK_MAINPROJECT"].Value = mainProject.MainProjectId;
        rsWBS.Fields["FK_PARENTTABLEINFO"].Value = WBSTableInfo.ProjectPhase;
        rsWBS.Fields["PARENTRECORDID"].Value = projectPhaseId;
        rsWBS.Fields["FK_TABLEINFO"].Value = WBSTableInfo.tableJoborder;
        rsWBS.Fields["RECORDID"].Value = joborderId;
        rsWBS.Fields["POSITION"].Value = 0;

        //Koppel budgetdetail onder fase
        rsWBS.AddNew();
        rsWBS.Fields["FK_MAINPROJECT"].Value = mainProject.MainProjectId;
        rsWBS.Fields["FK_PARENTTABLEINFO"].Value = WBSTableInfo.ProjectPhase;
        rsWBS.Fields["PARENTRECORDID"].Value = projectPhaseId;
        rsWBS.Fields["FK_TABLEINFO"].Value = WBSTableInfo.BudgetDetails;
        rsWBS.Fields["RECORDID"].Value = projectBudgetDetailId;
        rsWBS.Fields["POSITION"].Value = 0;

        var insertResult = rsWBS.Update2();
        if (insertResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Het toevoegen van nieuwe koppelingen in de WBS is mislukt, oorzaak: {insertResult.First(x => x.HasError).GetResult()}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }
    }

    private int CreateJoborder(MainProject mainProject, AddWorkDetail addWorkDetail)
    {
        var rsJoborder = GetRecordset("R_JOBORDER", "",
            "PK_R_JOBORDER = -1", "");
        rsJoborder.UseDataChanges = true;
        rsJoborder.UpdateWhenMoveRecord = false;

        rsJoborder.AddNew();
        rsJoborder.Fields["FK_ORDER"].Value = mainProject.OrderId;
        rsJoborder.Fields["DESCRIPTION"].Value =
            $"{mainProject.ChangeNo} - {addWorkDetail.Description}";

        var updateResult = rsJoborder.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Het toevoegen van nieuwe budgetregels is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return 0;
        }

        return (int)updateResult.First().PrimaryKey;
    }

    private int CreateProjectBudgetDetail(MainProject mainProject, AddWorkDetail addWorkDetail)
    {
        var rsProjectBudgetDetail = GetRecordset("R_PROJECTBUDGETDETAIL", "",
            "PK_R_PROJECTBUDGETDETAIL = -1", "");
        rsProjectBudgetDetail.UseDataChanges = false;
        rsProjectBudgetDetail.UpdateWhenMoveRecord = false;

        rsProjectBudgetDetail.AddNew();
        rsProjectBudgetDetail.Fields["FK_MAINPROJECT"].Value = mainProject.MainProjectId;
        rsProjectBudgetDetail.Fields["DESCRIPTION"].Value =
            $"{mainProject.ChangeNo} - {addWorkDetail.Description}";

        rsProjectBudgetDetail.Fields["BUDGETTYPE"].Value = (int)ProjectBudgetDetailType.Combined;
        rsProjectBudgetDetail.Fields["MATERIAL"].Value = true;
        rsProjectBudgetDetail.Fields["MISC"].Value = true;
        rsProjectBudgetDetail.Fields["OUTSOURCED"].Value = true;
        rsProjectBudgetDetail.Fields["QUANTITY"].Value = 1.0;
        rsProjectBudgetDetail.Fields["EXPECTEDQUANTITY"].Value = 1.0;
        rsProjectBudgetDetail.Fields["FK_UNIT"].Value = 2;//Stuks

        var updateResult = rsProjectBudgetDetail.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Het toevoegen van nieuwe budgetregels is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return 0;
        }

        return (int)updateResult.First().PrimaryKey;
    }

    private int CreateProjectPhase(MainProject mainProject, AddWorkDetail addWorkDetail, int joborderNumber)
    {
        var rsProjectPhase = GetRecordset("R_PROJECTPHASE", "",
            $"PK_R_PROJECTPHASE = -1", "");

        rsProjectPhase.UseDataChanges = true;
        rsProjectPhase.UpdateWhenMoveRecord = false;

        rsProjectPhase.AddNew();
        rsProjectPhase.Fields["FK_MAINPROJECT"].Value = mainProject.MainProjectId;
        rsProjectPhase.Fields["CODE"].Value = $"{joborderNumber}";
        rsProjectPhase.Fields["DESCRIPTION"].Value =
            $"{mainProject.ChangeNo} - {addWorkDetail.Description}";

        var updateResult = rsProjectPhase.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Het toevoegen van nieuwe fasen is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return 0;
        }

        return (int)updateResult.First().PrimaryKey;
    }

    private MainProject GetMainProject(int offerId)
    {
        var offer = GetRecordset("R_OFFER", "FK_MAINPROJECT, CHANGENUMBER",
            $"PK_R_OFFER = {offerId}", "").DataTable.AsEnumerable().First();

        var mainProjectId = offer.Field<int?>("FK_MAINPROJECT") ?? 0;

        if (mainProjectId == 0)
        {
            MessageBox.Show($"Order is nog niet aan een project gekoppeld. Maak het project aan via 'Genereer project' in de order workflow", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return null;
        }

        var mainProject = GetRecordset("R_MAINPROJECT", "STARTDATE, FK_ORDER",
            $"PK_R_MAINPROJECT = {mainProjectId}", "").DataTable.AsEnumerable().First();


        return new MainProject()
        {
            MainProjectId = mainProjectId,
            OrderId = mainProject.Field<int>("FK_ORDER"),
            ChangeNo = offer.Field<string>("CHANGENUMBER"),
            StartDate = mainProject.Field<DateTime>("STARTDATE"),
        };
    }

    class MainProject
    {
        public int MainProjectId { get; set; }
        public int OrderId { get; set; }
        public string ChangeNo { get; set; }
        public DateTime StartDate { get; set; }
    }


    class AddWorkDetail
    {
        public int? OfferId { get; set; }
        public int? JoborderId { get; set; }
        public string Description { get; set; }

    }

    private static class WBSTableInfo
    {
        public static readonly Guid ProjectPhase = new Guid("7db2fca6-1cb4-4a2c-9f96-3b4376a5d37b");
        public static readonly Guid Order = new Guid("abcc4e35-c70a-49e9-bd76-6c8a10c9fbb8");
        public static readonly Guid MainProject = new Guid("ba7d400f-a0d2-4f7e-adb1-bd53aae633c5");
        public static readonly Guid BudgetDetails = new Guid("51e8daf8-b92c-4852-9518-433679420946");
        public static readonly Guid tableJoborder = new Guid("7de0ac3e-9dfa-47e0-b113-63d3cac55f90");
    }
}
