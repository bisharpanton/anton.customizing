﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.IO;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System.Diagnostics;

public class RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //14-12-2023
        //Verzamel de inkoopfacturen, merge die tot één document en open deze

        if(FormDataAwareFunctions.TableName == null || !FormDataAwareFunctions.TableName.Equals("R_ORDERWIPALLDETAIL"))
        {
            MessageBox.Show("Dit script mag alleen vanaf de gecombineerde OHW regels aangeroepen worden.");
            return;
        }

        if(!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        if (FormDataAwareFunctions.FormParent.TableName == null || !FormDataAwareFunctions.FormParent.TableName.Equals("R_ORDER"))
        {
            MessageBox.Show("Dit script mag alleen vanaf de gecombineerde OHW regels vanuit een order aangeroepen worden.");
            return;
        }

        var orderId = (int)FormDataAwareFunctions.FormParent.CurrentRecord.GetPrimaryKeyValue();

        var orderWipAllDetailIds = FormDataAwareFunctions.GetSelectedRecords().Select(x => (Guid)x.GetPrimaryKeyValue()).ToList();

        var orderWipAllDetails = GetRecordset("R_ORDERWIPALLDETAIL",
            "FK_ORDERWIPDETAILITEM, FK_ORDERWIPDETAILITEMCOR, FK_ORDERWIPDETAILMISC, FK_ORDERWIPDETAILMISCCOR, FK_ORDERWIPDETAILOUTSOURCED, FK_ORDERWIPDETAILOUTSOURCEDCOR, FK_PURCHASEINVOICEDETAILINSTALLMENTITEM, FK_PURCHASEINVOICEDETAILINSTALLMENTITEMCOR, FK_PURCHASEINVOICEDETAILINSTALLMENTMISC, FK_PURCHASEINVOICEDETAILINSTALLMENTMISCCOR, FK_PURCHASEINVOICEDETAILINSTALLMENTOUTSOURCED, FK_PURCHASEINVOICEDETAILINSTALLMENTOUTSOURCEDCOR",
            $"PK_R_ORDERWIPALLDETAIL IN ('{string.Join("','", orderWipAllDetailIds)}')", "").DataTable.AsEnumerable().ToList();

        var alreadySessed = CheckForAlreadySessed(orderWipAllDetails);

        if(alreadySessed)
        {
            MessageBox.Show("Er zijn regels geselecteerd die al eerder op een een SES zijn gezet.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        var purchaseInvoiceIds = GetPurchaseInvoiceIds(orderWipAllDetails);

        if(!purchaseInvoiceIds.Any())
        {
            MessageBox.Show("Geen inkoopfacturen gevonden.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        var createdFile = CreateTotalPdf(purchaseInvoiceIds, orderId);

        if(string.IsNullOrEmpty(createdFile))
        {   
            return; 
        }

        SetBoolSesInWipDetails(orderWipAllDetails);

        Process.Start(Path.GetDirectoryName(createdFile));
    }

    private bool CheckForAlreadySessed(List<DataRow> orderWipAllDetails)
    {
        var sources = new string[12] { "ORDERWIPDETAILITEM", "ORDERWIPDETAILITEMCOR", "ORDERWIPDETAILMISC", "ORDERWIPDETAILMISCCOR",
            "ORDERWIPDETAILOUTSOURCED", "ORDERWIPDETAILOUTSOURCEDCOR", "PURCHASEINVOICEDETAILINSTALLMENTITEM", "PURCHASEINVOICEDETAILINSTALLMENTITEMCOR",
            "PURCHASEINVOICEDETAILINSTALLMENTMISC", "PURCHASEINVOICEDETAILINSTALLMENTMISCCOR", "PURCHASEINVOICEDETAILINSTALLMENTOUTSOURCED", "PURCHASEINVOICEDETAILINSTALLMENTOUTSOURCEDCOR"};

        foreach (var source in sources)
        {
            var detailIds = orderWipAllDetails.Where(x => x.Field<int?>($"FK_{source}").HasValue)
                .Select(x => x.Field<int>($"FK_{source}"))
                .ToList();

            if (!detailIds.Any())
            {
                continue;
            }

            var rsWipDetails = GetRecordset($"R_{source}", $"SES", $"PK_R_{source} IN ({string.Join(",", detailIds)}) AND SES = 1", "");

            if(rsWipDetails.RecordCount > 0)
            {
                return true;
            }
        }

        return false;
    }

    private void SetBoolSesInWipDetails(List<DataRow> orderWipAllDetails)
    {
        var sources = new string[12] { "ORDERWIPDETAILITEM", "ORDERWIPDETAILITEMCOR", "ORDERWIPDETAILMISC", "ORDERWIPDETAILMISCCOR",
            "ORDERWIPDETAILOUTSOURCED", "ORDERWIPDETAILOUTSOURCEDCOR", "PURCHASEINVOICEDETAILINSTALLMENTITEM", "PURCHASEINVOICEDETAILINSTALLMENTITEMCOR",
            "PURCHASEINVOICEDETAILINSTALLMENTMISC", "PURCHASEINVOICEDETAILINSTALLMENTMISCCOR", "PURCHASEINVOICEDETAILINSTALLMENTOUTSOURCED", "PURCHASEINVOICEDETAILINSTALLMENTOUTSOURCEDCOR"};

        foreach (var source in sources)
        {
            var detailIds = orderWipAllDetails.Where(x => x.Field<int?>($"FK_{source}").HasValue)
                .Select(x => x.Field<int>($"FK_{source}"))
                .ToList();

            if (!detailIds.Any())
            {
                continue;
            }

            var rsWipDetails = GetRecordset($"R_{source}", $"SES",
                $"PK_R_{source} IN ({string.Join(",", detailIds)})", "");
            rsWipDetails.UseDataChanges = false;
            rsWipDetails.UpdateWhenMoveRecord = false;

            rsWipDetails.MoveFirst();
            while(!rsWipDetails.EOF)
            {
                rsWipDetails.SetFieldValue("SES", true);
                rsWipDetails.MoveNext();
            }

            rsWipDetails.MoveFirst();
            rsWipDetails.Update();
        }
    }

    private string CreateTotalPdf(List<int> purchaseInvoiceIds, int orderId)
    {
        var purchaseInvoiceDocumentIds = GetRecordset("R_PURCHASEINVOICEDOCUMENT", "FK_DOCUMENT",
            $"FK_PURCHASEINVOICE IN ({string.Join(",", purchaseInvoiceIds)})", "").DataTable.AsEnumerable().Select(x => x.Field<int>("FK_DOCUMENT")).ToList();

        if(!purchaseInvoiceDocumentIds.Any())
        {
            MessageBox.Show("Geen inkoopfactuur documenten gevonden bij deze selectie van inkoopfacturen");
            return string.Empty;
        }

        var orderFolder = DetermineOrderFolder(orderId);
        var targetDirectory = Path.Combine(orderFolder, "Ingediende SES", $"SES {DateTime.Now:d-M-yyyy HH.mm.ss}");

        if(!Directory.Exists(targetDirectory))
        {
            Directory.CreateDirectory(targetDirectory);
        }

        var documents = GetRecordset("R_DOCUMENT", "DOCUMENTLOCATION, DOCUMENTDATA, STORAGESYSTEM", 
            $"PK_R_DOCUMENT IN ({string.Join(",", purchaseInvoiceDocumentIds)}) AND EXTENSION IN ('.pdf','.PDF') ", "")
            .DataTable.AsEnumerable().ToList();

        if (!documents.Any())
        {
            MessageBox.Show("Geen PDF documenten gevonden bij deze selectie van inkoopfacturen");
            return string.Empty;
        }

        var createdDatabaseDocuments = new List<string>();

        var filesToMerge = new List<string>();

        foreach(var document in documents)
        {
            if(document.Field<string>("STORAGESYSTEM").Equals("Verwijzing"))
            {
                filesToMerge.Add(document.Field<string>("DOCUMENTLOCATION"));
            }
            else
            {
                var targetFileName = Path.Combine(targetDirectory, $"{document.Field<int>("PK_R_DOCUMENT")}.pdf");
                try
                {
                    System.IO.File.WriteAllBytes(targetFileName, document.Field<byte[]>("DOCUMENTDATA"));
                }
                catch(Exception e)
                {
                    MessageBox.Show($"Genereren PDF file mislukt voor document met id {document.Field<int>("PK_R_DOCUMENT")}, oorzaak: {e}");
                    continue;
                }

                createdDatabaseDocuments.Add(targetFileName);
                filesToMerge.Add(targetFileName);
            }
        }

        
        var resultFilename = Path.Combine(targetDirectory, $"SES {DateTime.Now:d-M-yyyy}.pdf");

        try
        {
            PdfDocument outputDocument = new PdfDocument();

            // Iterate files
            foreach (string file in filesToMerge)
            {
                if(!System.IO.File.Exists(file))
                {
                    MessageBox.Show($"Bestand {file} niet gevonden");
                    continue;
                }

                // Open the document to import pages from it.
                PdfDocument inputDocument = PdfReader.Open(file, PdfDocumentOpenMode.Import);

                // Iterate pages
                int count = inputDocument.PageCount;
                for (int idx = 0; idx < count; idx++)
                {
                    // Get the page from the external document...
                    PdfPage page = inputDocument.Pages[idx];
                    // ...and add it to the output document.
                    outputDocument.AddPage(page);
                }
            }

            // Save the document...
            outputDocument.Save(resultFilename);
        }
        catch(Exception e)
        {
            MessageBox.Show($"Mergen PDF documenten is mislukt, oorzaak: {e}");
            return string.Empty;
        }


        foreach (var document in createdDatabaseDocuments)
        {
            System.IO.File.Delete(document);
        }


        return resultFilename;
    }

    private string DetermineOrderFolder(int orderId)
    {
        var directoryOrders = GetRecordset("R_CRMSETTINGS", "ORDERLOCATION", "", "").DataTable.AsEnumerable().First().Field<string>("ORDERLOCATION");
        var orderNumber = GetRecordset("R_ORDER", "ORDERNUMBER", $"PK_R_ORDER = {orderId}", "").DataTable.AsEnumerable().First().Field<int>("ORDERNUMBER");

        return Path.Combine(directoryOrders, orderNumber.ToString());
    }

    private List<int> GetPurchaseInvoiceIds(List<DataRow> orderWipAllDetails)
    {
        var result = new List<int>();

        var sources = new string[3] { "ITEM", "MISC", "OUTSOURCED" };

        result.AddRange(GetPurchaseInvoiceIdsFromWipDetails(orderWipAllDetails, sources));
        result.AddRange(GetPurchaseInvoiceIdsFromWipCorDetails(orderWipAllDetails, sources));

        result.AddRange(GetPurchaseInvoiceIdsFromPurchaseInvoiceDetailsInstallment(orderWipAllDetails, sources));
        
        return result.Distinct().OrderBy(x => x).ToList();
    }



    private List<int> GetPurchaseInvoiceIdsFromWipDetails(List<DataRow> orderWipAllDetails, string[] sources)
    {
        var result = new List<int>();

        foreach (var source in sources)
        {
            var wipDetailIds = orderWipAllDetails.Where(x => x.Field<int?>($"FK_ORDERWIPDETAIL{source}").HasValue)
                .Select(x => x.Field<int>($"FK_ORDERWIPDETAIL{source}"))
                .ToList();

            if(!wipDetailIds.Any())
            {
                continue;
            }

            var wipDetails = GetRecordset($"R_ORDERWIPDETAIL{source}", $"FK_GOODSRECEIPTDETAIL{source}, FK_PURCHASEINVOICEDETAIL{source}",
                $"PK_R_ORDERWIPDETAIL{source} IN ({string.Join(",", wipDetailIds)})", "").DataTable.AsEnumerable().ToList();

            if (!wipDetails.Any())
            {
                continue;
            }

            var goodsReceiptDetailIds = wipDetails.Select(x => x.Field<int?>($"FK_GOODSRECEIPTDETAIL{source}") ?? 0).ToList();
            var purchaseInvoiceDetailIds = wipDetails.Select(x => x.Field<int?>($"FK_PURCHASEINVOICEDETAIL{source}") ?? 0).ToList();

            var purchaseInvoiceDetails = GetRecordset($"R_PURCHASEINVOICEDETAIL{source}", "FK_PURCHASEINVOICE",
                $"FK_GOODSRECEIPTDETAIL{source} IN ({string.Join(",", goodsReceiptDetailIds)}) OR PK_R_PURCHASEINVOICEDETAIL{source} IN ({string.Join(",", purchaseInvoiceDetailIds)})", "")
                .DataTable.AsEnumerable().ToList();

            result.AddRange(purchaseInvoiceDetails.Select(x => x.Field<int>("FK_PURCHASEINVOICE")).ToList());
        }

        return result;
    }

    private List<int> GetPurchaseInvoiceIdsFromWipCorDetails(List<DataRow> orderWipAllDetails, string[] sources)
    {
        var result = new List<int>();

        foreach (var source in sources)
        {
            var wipDetailCorIds = orderWipAllDetails.Where(x => x.Field<int?>($"FK_ORDERWIPDETAIL{source}COR").HasValue)
                .Select(x => x.Field<int>($"FK_ORDERWIPDETAIL{source}COR"))
                .ToList();

            if (!wipDetailCorIds.Any())
            {
                continue;
            }

            var wipDetailsCor = GetRecordset($"R_ORDERWIPDETAIL{source}COR", $"FK_PURCHASEINVOICEDETAIL{source}",
                $"PK_R_ORDERWIPDETAIL{source}COR IN ({string.Join(",", wipDetailCorIds)})", "").DataTable.AsEnumerable().ToList();

            if (!wipDetailsCor.Any())
            {
                continue;
            }

            var purchaseInvoiceDetailIds = wipDetailsCor.Select(x => x.Field<int?>($"FK_PURCHASEINVOICEDETAIL{source}") ?? 0).ToList();

            var purchaseInvoiceDetails = GetRecordset($"R_PURCHASEINVOICEDETAIL{source}", "FK_PURCHASEINVOICE",
                $"PK_R_PURCHASEINVOICEDETAIL{source} IN ({string.Join(",", purchaseInvoiceDetailIds)})", "")
                .DataTable.AsEnumerable().ToList();

            result.AddRange(purchaseInvoiceDetails.Select(x => x.Field<int>("FK_PURCHASEINVOICE")).ToList());
        }

        return result;
    }

    private List<int> GetPurchaseInvoiceIdsFromPurchaseInvoiceDetailsInstallment(List<DataRow> orderWipAllDetails, string[] sources)
    {
        //Bij de correcties is ook de fk Factuurdeel ingevuld en werkt dit stukje code automatisch ook

        var result = new List<int>();

        foreach (var source in sources)
        {
            var purchaseInvoiceDetailInstallmentIds = orderWipAllDetails.Where(x => x.Field<int?>($"FK_PURCHASEINVOICEDETAILINSTALLMENT{source}").HasValue)
                .Select(x => x.Field<int>($"FK_PURCHASEINVOICEDETAILINSTALLMENT{source}"))
                .ToList();

            if (!purchaseInvoiceDetailInstallmentIds.Any())
            {
                continue;
            }

            var purchaseInvoiceDetailsInstallment = GetRecordset($"R_PURCHASEINVOICEDETAILINSTALLMENT{source}", $"FK_PURCHASEINVOICEDETAILINSTALLMENT",
                $"PK_R_PURCHASEINVOICEDETAILINSTALLMENT{source} IN ({string.Join(",", purchaseInvoiceDetailInstallmentIds)})", "").DataTable.AsEnumerable().ToList();

            if (!purchaseInvoiceDetailsInstallment.Any())
            {
                continue;
            }

            var purchaseInvoiceDetailIds = purchaseInvoiceDetailsInstallment.Select(x => x.Field<int>("FK_PURCHASEINVOICEDETAILINSTALLMENT")).ToList();

            var purchaseInvoiceDetails = GetRecordset($"R_PURCHASEINVOICEDETAILINSTALLMENT", "FK_PURCHASEINVOICE",
                $"PK_R_PURCHASEINVOICEDETAILINSTALLMENT IN ({string.Join(",", purchaseInvoiceDetailIds)})", "")
                .DataTable.AsEnumerable().ToList();

            result.AddRange(purchaseInvoiceDetails.Select(x => x.Field<int>("FK_PURCHASEINVOICE")).ToList());
        }

        return result;
    }
}
