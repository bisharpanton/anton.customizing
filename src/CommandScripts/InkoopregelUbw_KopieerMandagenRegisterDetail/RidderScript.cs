﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class InkoopregelUbw_KopieerMandagenRegisterDetail_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //15-3-2022
        //Kopieer regels

        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("U_FORMDETAILSUPPLIERMANDAYREGISTERDETAILS"))
        {
            MessageBox.Show("Dit script mag alleen vanaf Form Onderaannemer mandagenregister details aangeroepen worden.");
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        var selectedIds = FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue()).ToList();

        var selectedDetails = GetRecordset("U_FORMDETAILSUPPLIERMANDAYREGISTERDETAILS", "", $"PK_U_FORMDETAILSUPPLIERMANDAYREGISTERDETAILS IN ({string.Join(",", selectedIds)})", "")
            .DataTable.AsEnumerable().ToList();

        var relationId = GetRecordset("U_FORMSUPPLIERMANDAYREGISTERDETAILS", "FK_SUPPLIER",
            $"PK_U_FORMSUPPLIERMANDAYREGISTERDETAILS = {selectedDetails.First().Field<int>("FK_FORMSUPPLIERMANDAYREGISTERDETAILS")}", "")
            .DataTable.AsEnumerable().First().Field<int>("FK_SUPPLIER");

        var employeesResult = OpenMultiSelectForm("U_RELATIONWKAEMPLOYEES", "PK_U_RELATIONWKAEMPLOYEES", $"FK_RELATION = {relationId}", "ChooseContact");

        if(employeesResult == null)
        {
            return;
        }

        var wkaEmployees = GetRecordset("U_RELATIONWKAEMPLOYEES", "FIRSTNAME, NAMEPREFIX, LASTNAME, BSN, REGNO, COCNUMBER",
            $"PK_U_RELATIONWKAEMPLOYEES IN ({string.Join(",", employeesResult)})", "")
            .DataTable.AsEnumerable().ToList();

        var rsFormDetails = GetRecordset("U_FORMDETAILSUPPLIERMANDAYREGISTERDETAILS", "", $"PK_U_FORMDETAILSUPPLIERMANDAYREGISTERDETAILS IS NULL", "");
        rsFormDetails.UseDataChanges = false;
        rsFormDetails.UpdateWhenMoveRecord = false;

        foreach (var employeeId in employeesResult)
        {
            var wkaEmployee = wkaEmployees.First(x => x.Field<int>("PK_U_RELATIONWKAEMPLOYEES") == (int)employeeId);
            var totalName = string.IsNullOrEmpty(wkaEmployee.Field<string>("NAMEPREFIX"))
                ? $"{wkaEmployee.Field<string>("FIRSTNAME")} {wkaEmployee.Field<string>("LASTNAME")}"
                : $"{wkaEmployee.Field<string>("FIRSTNAME")} {wkaEmployee.Field<string>("NAMEPREFIX")} {wkaEmployee.Field<string>("LASTNAME")}";

            foreach (var detail in selectedDetails)
            {
                rsFormDetails.AddNew();

                rsFormDetails.SetFieldValue("FK_FORMSUPPLIERMANDAYREGISTERDETAILS", detail.Field<int>("FK_FORMSUPPLIERMANDAYREGISTERDETAILS"));
                rsFormDetails.SetFieldValue("EXTERNALEMPLOYEE", totalName);

                rsFormDetails.SetFieldValue("BSN", wkaEmployee.Field<string>("BSN"));
                rsFormDetails.SetFieldValue("REGNO", wkaEmployee.Field<string>("REGNO"));
                rsFormDetails.SetFieldValue("COCNUMBER", wkaEmployee.Field<string>("COCNUMBER"));

                rsFormDetails.SetFieldValue("YEAR", detail.Field<int>("YEAR"));
                rsFormDetails.SetFieldValue("WEEK", detail.Field<int>("WEEK"));

                rsFormDetails.SetFieldValue("MONDAY", detail.Field<long>("MONDAY"));
                rsFormDetails.SetFieldValue("TUESDAY", detail.Field<long>("TUESDAY"));
                rsFormDetails.SetFieldValue("WEDNESDAY", detail.Field<long>("WEDNESDAY"));
                rsFormDetails.SetFieldValue("THURSDAY", detail.Field<long>("THURSDAY"));
                rsFormDetails.SetFieldValue("FRIDAY", detail.Field<long>("FRIDAY"));
                rsFormDetails.SetFieldValue("SATURDAY", detail.Field<long>("SATURDAY"));
                rsFormDetails.SetFieldValue("SUNDAY", detail.Field<long>("SUNDAY"));
            }
        }

        var updateResult = rsFormDetails.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Aanmaken regels mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
