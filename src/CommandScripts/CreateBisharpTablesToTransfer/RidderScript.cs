﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class CreateBisharpTablesToTransfer_RidderScript : CommandScript
{
    public void Execute()
    {
        //SP
        //17-6-2022
        //Genereer bisharp tables to transfer voor een relatie

        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("U_BISHARPTABLESTOTRANSFER", StringComparison.OrdinalIgnoreCase))
        {
            MessageBox.Show("Dit script mag alleen van de Bisharp tabellen to transfer tabel aangeroepen worden.");
        }

        object[] relations = null;
        relations = OpenMultiSelectForm("R_RELATION", "PK_R_RELATION",
            "BISHARPCOMPANYNUMBER <> ''", "");

        if (relations == null)
        {
            return;
        }

        /*var resultRelation = OpenBrowseFormWithFilter("R_RELATION", "PK_R_RELATION",
            "BISHARPCOMPANYNUMBER <> ''", true);

        if (resultRelation == null)
        {
            return;
        }*/

        object[] bisharpTableGroups = null;
        bisharpTableGroups = OpenMultiSelectForm("U_BISHARPTABLEGROUPS", "PK_U_BISHARPTABLEGROUPS",
            "", "");

        if (bisharpTableGroups == null)
        {
            return;
        }

        CreateTables(bisharpTableGroups, relations);

    }

    public void CreateTables(object[] bisharpTableGroups, object[] relations)
    {

        var bisharpTablesToImport = GetRecordset("U_BISHARPTABLES", "TABLENAME",
            $"FK_BISHARPTABLEGROUP in ({string.Join(", ", bisharpTableGroups)})", "").DataTable.AsEnumerable().Select(x => x.Field<string>("TABLENAME")).Distinct().ToList();

        foreach(var relation in relations)
        {
            var currentTables = GetRecordset("U_BISHARPTABLESTOTRANSFER", "TABLENAME",
                $"FK_RELATION = {relation}", "").DataTable.AsEnumerable().Select(x => x.Field<string>("TABLENAME")).Distinct().ToList();

            var listToImport = bisharpTablesToImport.Except(currentTables).Distinct().ToList();

            if (!listToImport.Any())
            {
                MessageBox.Show("Alle te importeren tabellen bestaan al onder deze relatie.");
                return;
            }

            var rsNewBisharpTablesToTransfer = GetRecordset("U_BISHARPTABLESTOTRANSFER", "", "PK_U_BISHARPTABLESTOTRANSFER = -1", "");

            rsNewBisharpTablesToTransfer.UseDataChanges = true;
            rsNewBisharpTablesToTransfer.UpdateWhenMoveRecord = false;

            foreach (var tableName in listToImport)
            {
                rsNewBisharpTablesToTransfer.AddNew();
                rsNewBisharpTablesToTransfer.Fields["FK_RELATION"].Value = relation;
                rsNewBisharpTablesToTransfer.Fields["TABLENAME"].Value = tableName;
            }

            rsNewBisharpTablesToTransfer.Update();
        } 
    }
}
