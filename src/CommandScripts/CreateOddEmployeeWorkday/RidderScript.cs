﻿using ADODB;
using Ridder.Client.SDK.SDKParameters;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class CreateOddEmployeeWorkday_RidderScript : CommandScript
{
	public void Execute()
	{
        //SP
        //7-3-2024
        //Aanmaken van een afwijkende werktijd

        if (FormDataAwareFunctions == null ||
                (FormDataAwareFunctions.TableName != "U_PROJECTTIMEEMPLOYEEDAY" && FormDataAwareFunctions.TableName != "R_PROJECTTIME"))
        {
            MessageBox.Show("Dit script mag alleen vanaf de tabel 'Uren' of 'Uren werknemer per dag' aangeroepen worden.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            MessageBox.Show("Geen regel geselecteerd. Selecteer een urenboeking om het rooster te kunnen wijzigen.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        if (FormDataAwareFunctions.GetSelectedRecords().Count() > 1)
        {
            MessageBox.Show("Dit script mag alleen slechts voor één regel tegelijk aangeroepen worden.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        int projecttimeEmployeeWeekId = 0;
        int employeeId = 0;

        if (FormDataAwareFunctions.TableName.Equals("U_PROJECTTIMEEMPLOYEEDAY"))
        {
            if(FormDataAwareFunctions.FormParent == null || !FormDataAwareFunctions.FormParent.TableName.Equals("U_PROJECTTIMEEMPLOYEEWEEK"))
            {
                MessageBox.Show("Dit script mag alleen vanaf de Uren werknemer per dag binnen Uren werknemer per week aangeroepen worden.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            projecttimeEmployeeWeekId = (int)FormDataAwareFunctions.FormParent.CurrentRecord.GetPrimaryKeyValue();

            employeeId = (int)FormDataAwareFunctions.FormParent.CurrentRecord.GetCurrentRecordValue("FK_EMPLOYEE");
        }
        else
        {
            projecttimeEmployeeWeekId = (int)FormDataAwareFunctions.CurrentRecord.GetCurrentRecordValue("FK_PROJECTTIMEEMPLOYEEWEEK") as int? ?? 0;

            if(projecttimeEmployeeWeekId == 0)
            {
                MessageBox.Show("Geen uren per week gekoppeld aan deze urenboeking.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            employeeId = (int)FormDataAwareFunctions.CurrentRecord.GetCurrentRecordValue("FK_EMPLOYEE");
        }

        var rsProjecttimeEmployeeDays = GetRecordset("U_PROJECTTIMEEMPLOYEEDAY", "PK_U_PROJECTTIMEEMPLOYEEDAY, DATE, TOTALHOURSNEEDED",
            $"FK_PROJECTTIMEEMPLOYEEWEEK = {projecttimeEmployeeWeekId}", "");

        if (!(rsProjecttimeEmployeeDays.RecordCount > 0))
        {
            MessageBox.Show("Geen Uren werknemer per dagen gevonden bij deze Uren werknemer per week.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        var projectTimeEmployeeDays = rsProjecttimeEmployeeDays.DataTable.AsEnumerable().ToList();

        var createdFormId = CreatedForm(projectTimeEmployeeDays);

        var editParameter = new EditParameters()
        {
            TableName = "U_FORMIMPORTODDEMPLOYEEWORKDAYS",
            Id = createdFormId,
        };

        OpenEdit(editParameter, true);

        //Check of Edit correct is afgesloten
        var rsClosedForm = GetRecordset("U_FORMIMPORTODDEMPLOYEEWORKDAYS", "", $"PK_U_FORMIMPORTODDEMPLOYEEWORKDAYS = {createdFormId}", "");
        var rsClosedFormDetails = GetRecordset("U_FORMIMPORTODDEMPLOYEEWORKDAYSDETAILS", "", $"FK_FORMIMPORTODDEMPLOYEEWORKDAYS = {createdFormId}", "");

        if (!rsClosedForm.DataTable.AsEnumerable().First().Field<bool>("PROCESSED"))
        {

            //Delete Form Detailsd
            rsClosedFormDetails.UpdateWhenMoveRecord = false;
            rsClosedFormDetails.MoveFirst();
            while (!rsClosedFormDetails.EOF)
            {
                rsClosedFormDetails.Delete();
                rsClosedFormDetails.MoveNext();
            }
            rsClosedFormDetails.MoveFirst();
            rsClosedFormDetails.Update();

            //Delete Form
            rsClosedForm.MoveFirst();
            rsClosedForm.Delete();

            return; //Formulier niet correct afgesloten
        }

        Cursor.Current = Cursors.WaitCursor;
        try
        {
            ProcessForm(employeeId, projecttimeEmployeeWeekId, rsClosedFormDetails.DataTable.AsEnumerable().ToList(), projectTimeEmployeeDays);
        }
        finally
        {
            Cursor.Current = Cursors.Default;
        }

        //Delete Form Details
        rsClosedFormDetails.UpdateWhenMoveRecord = false;
        rsClosedFormDetails.MoveFirst();
        while (!rsClosedFormDetails.EOF)
        {
            rsClosedFormDetails.Delete();
            rsClosedFormDetails.MoveNext();
        }
        rsClosedFormDetails.MoveFirst();
        rsClosedFormDetails.Update();

        //Delete Form
        rsClosedForm.MoveFirst();
        rsClosedForm.Delete();

        FormDataAwareFunctions.Refres();

    }

    private int CreatedForm(List<DataRow> projectTimeEmployeeDays)
    {
        var rsForm = GetRecordset("U_FORMIMPORTODDEMPLOYEEWORKDAYS", "", "PK_U_FORMIMPORTODDEMPLOYEEWORKDAYS = -1", "");
        rsForm.AddNew();
        rsForm.Update();

        var createdFormId = (int)rsForm.GetField("PK_U_FORMIMPORTODDEMPLOYEEWORKDAYS").Value;

        var rsFormDetails = GetRecordset("U_FORMIMPORTODDEMPLOYEEWORKDAYSDETAILS", "", "PK_U_FORMIMPORTODDEMPLOYEEWORKDAYSDETAILS = -1", "");

        foreach(var projectTimeEmployeeDay in projectTimeEmployeeDays)
        {          
            rsFormDetails.AddNew();
            rsFormDetails.SetFieldValue("FK_FORMIMPORTODDEMPLOYEEWORKDAYS", createdFormId);
            rsFormDetails.SetFieldValue("DATE", projectTimeEmployeeDay.Field<DateTime>("DATE"));
            rsFormDetails.SetFieldValue("TOTALHOURSNEEDED", projectTimeEmployeeDay.Field<long>("TOTALHOURSNEEDED"));
        }

        rsFormDetails.Update();

        return createdFormId;
    }

    private void ProcessForm(int employeeId, int projecttimeEmployeeWeekId, List<DataRow> closedFormDetails, List<DataRow> projectTimeEmployeeDays)
    {
        if(!ValidateHoursNeeded(closedFormDetails, projectTimeEmployeeDays))
        {
            MessageBox.Show("Totaal ingevoerde uren is niet gelijk aan rooster totaal.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        List<NeededHoursPerEmployeePerCalendarDay> findNeededHoursDiscrepancies = FindNeededHoursDiscrepancies(closedFormDetails, projectTimeEmployeeDays);

        CreateOddEmployeeWorkdays(employeeId, findNeededHoursDiscrepancies);

        UpdateProjectTimeDays(projecttimeEmployeeWeekId, findNeededHoursDiscrepancies);
    }

    private bool ValidateHoursNeeded(List<DataRow> closedFormDetails, List<DataRow> projectTimeEmployeeDays)
    {
        return closedFormDetails.Sum(x => x.Field<long>("TOTALHOURSNEEDED")) == projectTimeEmployeeDays.Sum(x => x.Field<long>("TOTALHOURSNEEDED"));
    }


    private void CreateOddEmployeeWorkdays(int employeeId, List<NeededHoursPerEmployeePerCalendarDay> findNeededHoursDiscrepancies)
    {
        foreach (var findNeededHoursDiscrepancy in findNeededHoursDiscrepancies)
        {
            //Voer workflow [Intern] GetOrCreateWorkday uit bij de werknemer om de werkdag op te halen of te creëren

            var wfParameters = new Dictionary<string, object>();
            wfParameters.Add("Hours", TimeSpan.FromTicks(findNeededHoursDiscrepancy.NeededHours).TotalHours);

            var result = ExecuteWorkflowEvent("R_EMPLOYEE", employeeId, new Guid("5a9aa73d-4e27-4b82-96e8-8fd0122e6d7c"), wfParameters);

            var workday = result.Messages.Select(x => Regex.Match(x.Message, @"\d+").Value).FirstOrDefault();
            int workdayId = int.Parse(workday);

            var rsOddEmployeeWorkday = GetRecordset("R_ODDEMPLOYEEWORKDAY", "", 
                $"FK_EMPLOYEE = {employeeId} AND DATE = '{findNeededHoursDiscrepancy.Date:yyyyMMdd}'", "");
            rsOddEmployeeWorkday.UpdateWhenMoveRecord = false;
            rsOddEmployeeWorkday.UseDataChanges = true;

            if(rsOddEmployeeWorkday.RecordCount == 0)
            {
                rsOddEmployeeWorkday.AddNew();
                rsOddEmployeeWorkday.SetFieldValue("FK_EMPLOYEE", employeeId);
                rsOddEmployeeWorkday.SetFieldValue("FK_WORKDAY", workdayId);
                rsOddEmployeeWorkday.SetFieldValue("DATE", findNeededHoursDiscrepancy.Date);
            }
            else
            {
                rsOddEmployeeWorkday.MoveFirst();
                rsOddEmployeeWorkday.SetFieldValue("FK_WORKDAY", workdayId);
            }
            rsOddEmployeeWorkday.Update();
        }

    }

    private void UpdateProjectTimeDays(int projecttimeEmployeeWeekId, List<NeededHoursPerEmployeePerCalendarDay> findNeededHoursDiscrepancies)
    {
        foreach (var findNeededHoursDiscrepancy in findNeededHoursDiscrepancies)
        {
            var rsProjecttimeEmployeeDay = GetRecordset("U_PROJECTTIMEEMPLOYEEDAY", "",
                   $"FK_PROJECTTIMEEMPLOYEEWEEK = {projecttimeEmployeeWeekId} AND DATE = '{findNeededHoursDiscrepancy.Date:yyyyMMdd}'", "");

            rsProjecttimeEmployeeDay.MoveFirst();
            rsProjecttimeEmployeeDay.SetFieldValue("TOTALHOURSNEEDED", findNeededHoursDiscrepancy.NeededHours);
            rsProjecttimeEmployeeDay.Update();
        }            
    }

    private List<NeededHoursPerEmployeePerCalendarDay> FindNeededHoursDiscrepancies(List<DataRow> closedFormDetails, List<DataRow> projectTimeEmployeeDays)
    {
        var currentHoursDict = projectTimeEmployeeDays.ToDictionary(
            k => k.Field<DateTime>("DATE"),
            v => v.Field<long>("TOTALHOURSNEEDED"));

        var discrepancies = new List<NeededHoursPerEmployeePerCalendarDay>();

        foreach (var entry in closedFormDetails)
        {
            if (!currentHoursDict.TryGetValue((entry.Field<DateTime>("DATE")), out long currentHours) || entry.Field<long>("TOTALHOURSNEEDED") != currentHours)
            {
                discrepancies.Add(new NeededHoursPerEmployeePerCalendarDay
                {
                    Date = entry.Field<DateTime>("DATE"),
                    NeededHours = entry.Field<long>("TOTALHOURSNEEDED")
                });
            }
        }

        return discrepancies;
    }

    class NeededHoursPerEmployeePerCalendarDay
    {
        public DateTime Date { get; set; }
        public long NeededHours { get; set; }
    }
}
