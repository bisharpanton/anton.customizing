﻿using ADODB;
using Ridder.Client.SDK.SDKParameters;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Recordset.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using Uren_MaakInkooporderVoorZzp.Models;

public class Uren_MaakInkooporderVoorZzp_RidderScript : CommandScript
{
    public readonly string GrootboekCodeDoorbelastingPersoneelkosten = "4060";

    public void Execute()
    {
        //DB
        //15-3-2023
        //Maak inkooporder van geselecteerde uren voor zzp'er

        //SP - 23-8-2024 - Ook mogelijk vanuit de uren werknemer week (multiselect weken)

        if (FormDataAwareFunctions == null ||
            (!FormDataAwareFunctions.TableName.Equals("R_PROJECTTIME") &&
             !FormDataAwareFunctions.TableName.Equals("U_PROJECTTIMEEMPLOYEEWEEK")))
        {
            MessageBox.Show("Dit script mag alleen vanaf de uren aangeroepen worden.");
            return;
        }


        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        Cursor.Current = Cursors.WaitCursor;
        try
        {
            var selectedProjectTimeIds = new List<int>();

            if (FormDataAwareFunctions.TableName.Equals("R_PROJECTTIME"))
            {
                selectedProjectTimeIds = FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue()).ToList();
            }
            else
            {
                var projectTimeEmployeeWeekIds = FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue()).ToList();

                selectedProjectTimeIds = GetRecordset("R_PROJECTTIME", "PK_R_PROJECTTIME",
                    $"FK_PROJECTTIMEEMPLOYEEWEEK IN ({string.Join(", ", projectTimeEmployeeWeekIds)})", "").DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_PROJECTTIME")).ToList();

                if (!selectedProjectTimeIds.Any())
                {
                    return;
                }
            }

            var projectTimes = GetRecordset("R_PROJECTTIME", "DATE, FK_EMPLOYEE, FK_WORKACTIVITY, TIMEEMPLOYEE, FK_OVERTIMECODE, FK_WKAPURCHASEORDERDETAILOUTSOURCED, CLOSED",
                $"PK_R_PROJECTTIME IN ({string.Join(",", selectedProjectTimeIds)})", "")
                .As<ProjectTime>().ToList();

            if (projectTimes.Any(x => !x.CLOSED))
            {
                MessageBox.Show($"Urenboeking(en) zijn nog niet afgesloten. Maken inkooporder wordt geannuleerd."
                    , "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (projectTimes.Any(x => x.FK_WKAPURCHASEORDERDETAILOUTSOURCED.HasValue))
            {
                MessageBox.Show($"Urenboeking(en) zijn al eerder omgezet naar inkooporder. Meerdere keren omzetten is niet mogelijk. Maken inkooporder wordt geannuleerd."
                    , "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            var employees = GetRecordset("R_EMPLOYEE", "EMPLOYEECATEGORY, FK_RELATION, FK_PERSON",
                $"PK_R_EMPLOYEE IN ({string.Join(",", projectTimes.Select(x => x.FK_EMPLOYEE).Distinct())})", "").As<Employee>().ToList();

            var wrongEmployees = employees.Where(x => x.EMPLOYEECATEGORY != EmployeeCategory.ZZP_er && x.EMPLOYEECATEGORY != EmployeeCategory.Uitzendkracht).ToList();
            if (wrongEmployees.Any())
            {
                MessageBox.Show($"Maak inkooporder is alleen van toepassing voor ZZP'ers en uitzendkrachten. Werknemer(s) {string.Join(", ", wrongEmployees.Select(x => GetRecordTag("R_PERSON", x.FK_PERSON)))} zijn niet aangemerkt als ZZP'er of uitzendkracht in het werknemersbestand."
                    , "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            var wrongEmployees2 = employees.Where(x => !x.FK_RELATION.HasValue).ToList();
            if (wrongEmployees2.Any())
            {
                MessageBox.Show($"Geen relatie gevonden bij werknemer(s) {string.Join(", ", wrongEmployees2.Select(x => GetRecordTag("R_PERSON", x.FK_PERSON)))}. Inkooporder maken niet mogelijk.",
                    "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            var workactivitys = GetRecordset("R_WORKACTIVITY", "FK_WKAOUTSOURCEDACTIVITY, DESCRIPTION",
                $"PK_R_WORKACTIVITY IN ({string.Join(",", projectTimes.Select(x => x.FK_WORKACTIVITY).Distinct())})", "").As<Workactivity>().ToList();

            var wrongWorkactivities = workactivitys.Where(x => !x.FK_WKAOUTSOURCEDACTIVITY.HasValue).ToList();
            if (wrongWorkactivities.Any())
            {
                MessageBox.Show($"Bij bewerking(en) {string.Join(", ", wrongWorkactivities.Select(x => GetRecordTag("R_WORKACTIVITY", x.PK_R_WORKACTIVITY)))} is geen WKA uitbesteed werk gevonden. Inkooporder kan niet gemaakt worden."
                    , "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            var timeCodes = GetRecordset("R_TIMECODE", "CODE, PERCENTAGEEMPLOYEE",
                $"PK_R_TIMECODE IN ({string.Join(",", projectTimes.Select(x => x.FK_OVERTIMECODE ?? 0).Distinct())})", "").As<TimeCode>().ToList();


            projectTimes.ForEach(x => x.Workactivity = workactivitys.First(y => y.PK_R_WORKACTIVITY == x.FK_WORKACTIVITY));
            projectTimes.ForEach(x => x.TimeCode = timeCodes.FirstOrDefault(y => y.PK_R_TIMECODE == (x.FK_OVERTIMECODE ?? 0)));

            var grootboekDoorbelastingPersoneelskosten = GetCorrectGeneralLedgerAccount();

            if (grootboekDoorbelastingPersoneelskosten == 0)
            {
                MessageBox.Show($"Grootboekrekening 4060 - Doorbelasting personeelkosten niet gevonden. Maken inkooporder niet mogelijk.", "Ridder iQ",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            var createdPurchaseOrders = CreatePurchaseOrders(projectTimes, employees, workactivitys, grootboekDoorbelastingPersoneelskosten);

            if (createdPurchaseOrders == null)
            {
                return;
            }

            UpdateProjectTimes(selectedProjectTimeIds, projectTimes, createdPurchaseOrders.SelectMany(x => x.PurchaseOrderDetailsOutsourced).ToList());

            /*DB - 25-2-2025 - Ivm een bug in de nieuwste Ridder versie kunnen we niet meer mailen vanuit een client script
            SetPurchaseOrderReceivedAndMailPerfomanceOfDeclaration(createdPurchaseOrders);
            OpenPurchaseOrdersNotReceived(createdPurchaseOrders.Where(x => x.NotAutomaticReceived).ToList());*/
            MessageBox.Show("De inkooporder(s) worden nu geopend, maar zijn nog niet ontvangen gemeld. Verwerk de inkooporder en mail de Prestatieverklaring handmatig via de workflow.", "Ridder iQ",
                MessageBoxButtons.OK, MessageBoxIcon.Information );

            OpenPurchaseOrdersNotReceived(createdPurchaseOrders);
        }
        finally
        {
            Cursor.Current = Cursors.Default;
        }
    }

    private void OpenPurchaseOrdersNotReceived(List<PurchaseOrder> purchaseOrdersNotAutomaticReceived)
    {
        if (!purchaseOrdersNotAutomaticReceived.Any())
        {
            return;
        }

        if (purchaseOrdersNotAutomaticReceived.Count() <= 2)
        {
            foreach (var purchaseOrder in purchaseOrdersNotAutomaticReceived)
            {
                var editPar = new EditParameters()
                {
                    TableName = "R_PURCHASEORDER",
                    Id = purchaseOrder.PK_R_PURCHASEORDER,
                };

                OpenEdit(editPar, false);
            }
        }
        else
        {
            var browsePar = new BrowseParameters()
            {
                TableName = "R_PURCHASEORDER",
                Filter = $"PK_R_PURCHASEORDER IN ({string.Join(",", purchaseOrdersNotAutomaticReceived.Select(x => x.PK_R_PURCHASEORDER))})",
            };

            OpenBrowse(browsePar, false);
        }
    }

    private void SetPurchaseOrderReceivedAndMailPerfomanceOfDeclaration(List<PurchaseOrder> createdPurchaseOrders)
    {
        var relations = GetRecordset("R_RELATION", "NOTAUTOMATICRECEIVEPURCHASEORDER",
            $"PK_R_RELATION IN ({string.Join(",", createdPurchaseOrders.Select(x => x.FK_SUPPLIER))})", "")
            .DataTable.AsEnumerable().ToList();


        var wfSetPurchaseOrderReceivedAndMailPerformanceOfDeclaration = new Guid("739c1f59-5580-43c2-9b54-25ba3c7eab8f");

        foreach (var purchaseOrder in createdPurchaseOrders)
        {
            var relation = relations.First(x => x.Field<int>("PK_R_RELATION") == purchaseOrder.FK_SUPPLIER);

            if (relation.Field<bool>("NOTAUTOMATICRECEIVEPURCHASEORDER"))
            {
                purchaseOrder.NotAutomaticReceived = true;
                continue;
            }

            var wfResult = ExecuteWorkflowEvent("R_PURCHASEORDER", purchaseOrder.PK_R_PURCHASEORDER, wfSetPurchaseOrderReceivedAndMailPerformanceOfDeclaration, null);

            if (wfResult.HasError)
            {
                MessageBox.Show($"Uitvoeren workflow 'Mail prestatieverklaring en verwerk inkooporder' mislukt, oorzaak: {wfResult.GetResult()}", "Ridder iQ",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
    }

    private void UpdateProjectTimes(List<int> selectedProjectTimeIds, List<ProjectTime> projectTimes, List<PurchaseOrderDetailOutsourced> purchaseOrderDetailsOutsourced)
    {
        var rsProjectTimes = GetRecordset("R_PROJECTTIME", "DATE, FK_EMPLOYEE, FK_WORKACTIVITY, TIMEEMPLOYEE, FK_OVERTIMECODE, FK_WKAPURCHASEORDERDETAILOUTSOURCED",
            $"PK_R_PROJECTTIME IN ({string.Join(",", selectedProjectTimeIds)})", "");
        rsProjectTimes.UseDataChanges = false;
        rsProjectTimes.UpdateWhenMoveRecord = false;

        rsProjectTimes.MoveFirst();
        while (!rsProjectTimes.EOF)
        {
            var projectTime = projectTimes.First(x => x.PK_R_PROJECTTIME == (int)rsProjectTimes.GetField("PK_R_PROJECTTIME").Value);
            var createdPurchaseOrderDetailOutsourced = purchaseOrderDetailsOutsourced.First(x => x.EXTERNALKEY.Equals(projectTime.GroupByEmployeeOutsourcedctivityDate));

            rsProjectTimes.SetFieldValue("FK_WKAPURCHASEORDERDETAILOUTSOURCED", createdPurchaseOrderDetailOutsourced.PK_R_PURCHASEORDERDETAILOUTSOURCED);

            rsProjectTimes.MoveNext();
        }

        rsProjectTimes.MoveFirst();
        var updateResult = rsProjectTimes.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Bijwerken urenboekingen mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }
    }

    private int GetCorrectGeneralLedgerAccount()
    {
        return GetRecordset("R_GENERALLEDGERACCOUNT", "PK_R_GENERALLEDGERACCOUNT", $"GENERALLEDGERACCOUNTNUMBER = '{GrootboekCodeDoorbelastingPersoneelkosten}'", "")
            .DataTable.AsEnumerable().FirstOrDefault()?.Field<int>("PK_R_GENERALLEDGERACCOUNT") ?? 0;
    }

    private List<PurchaseOrder> CreatePurchaseOrders(List<ProjectTime> projectTimes, List<Employee> employees, List<Workactivity> workactivitys, int grootboekDoorbelastingPersoneelskosten)
    {
        var ownRelationId = GetRecordset("R_CRMSETTINGS", "FK_OWNCOMPANY", "", "").DataTable.AsEnumerable().First().Field<int>("FK_OWNCOMPANY");

        var outsourcedActivities = GetOutsourcedActivities(workactivitys.Select(x => x.FK_WKAOUTSOURCEDACTIVITY.Value).Distinct().ToList());

        var uniqueSuppliers = employees.Select(x => x.FK_RELATION.Value).Distinct().ToList();

        var addToExistingPurchaseOrderId = 0;
        if (uniqueSuppliers.Count == 1)
        {
            //Vraag of er naar een bestaande inkooporder omgezet moet worden
            var dg = MessageBox.Show("Uren omzetten naar een bestaande inkooporder?", "Ridder iQ", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

            if (dg == DialogResult.Cancel)
            {
                return null;
            }

            if (dg == DialogResult.Yes)
            {
                var browsePar = new BrowseParameters()
                {
                    TableName = "R_PURCHASEORDER",
                    Filter = $"FINANCIALDONE = 0 AND RECEIPTSTATE = 1 AND FK_SUPPLIER = {uniqueSuppliers.First()} AND WKA = 1",
                    ResultColumnName = "PK_R_PURCHASEORDER",
                };

                var resultOpenBrowse = OpenBrowse(browsePar, true);

                if (resultOpenBrowse == null)
                {
                    return null;
                }

                var rsExistingPurchaseOrderDetailsOutsourced = GetRecordset("R_PURCHASEORDERDETAILOUTSOURCED", "",
                    $"FK_PURCHASEORDER = {(int)resultOpenBrowse}", "");
                var existingPurchaseOrderDetailsOutsourced = rsExistingPurchaseOrderDetailsOutsourced.DataTable.AsEnumerable().ToList();

                if (existingPurchaseOrderDetailsOutsourced.Any())
                {
                    var dg2 = MessageBox.Show($"Op inkooporder {GetRecordTag("R_PURCHASEORDER", (int)resultOpenBrowse)} zijn bestaande inkoopregels aanwezig. Deze regels vervangen? Bij 'Nee' dan blijven de oorspronkelijke inkoopregels bestaan.",
                        "Ridder iQ", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                    if (dg2 == DialogResult.Cancel)
                    {
                        return null;
                    }

                    if (dg2 == DialogResult.Yes)
                    {
                        DeleteExistingPurchaseOrderDetailsOutsourced(rsExistingPurchaseOrderDetailsOutsourced);
                    }
                }

                addToExistingPurchaseOrderId = (int)resultOpenBrowse;
            }
        }

        var createdPurchaseOrders = new List<PurchaseOrder>();

        if (addToExistingPurchaseOrderId != 0)
        {
            createdPurchaseOrders = GetRecordset("R_PURCHASEORDER", "", $"PK_R_PURCHASEORDER = {addToExistingPurchaseOrderId}", "").As<PurchaseOrder>().ToList();
        }
        else
        {
            var rsPurchaseOrder = GetRecordset("R_PURCHASEORDER", "", "PK_R_PURCHASEORDER IS NULL", "");
            rsPurchaseOrder.UpdateWhenMoveRecord = false;

            foreach (var supplierId in uniqueSuppliers)
            {
                var employeeIdsThisSupplier = employees.Where(x => x.FK_RELATION == supplierId).Select(x => x.PK_R_EMPLOYEE).ToList();
                var projectTimesThisEmployees = projectTimes.Where(x => employeeIdsThisSupplier.Contains(x.FK_EMPLOYEE)).ToList();
                var datesThisProjectTimes = projectTimesThisEmployees.Select(x => x.DATE).Distinct().ToList();

                rsPurchaseOrder.UseDataChanges = true;

                rsPurchaseOrder.AddNew();
                rsPurchaseOrder.SetFieldValue("FK_SUPPLIER", supplierId);

                var description = datesThisProjectTimes.Max() != datesThisProjectTimes.Min()
                    ? $"Arbeid {datesThisProjectTimes.Min():d-M-yyyy} t/m {datesThisProjectTimes.Max():d-M-yyyy}"
                    : $"Arbeid {datesThisProjectTimes.Min():d-M-yyyy}";

                rsPurchaseOrder.SetFieldValue("REFERENCESUPPLIER", description);
                rsPurchaseOrder.SetFieldValue("FK_DELIVERYRELATION", ownRelationId);

                rsPurchaseOrder.SetFieldValue("WKA", true);
            }

            var updateResult = rsPurchaseOrder.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                MessageBox.Show($"Aanmaken inkooporder(s) mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }

            createdPurchaseOrders = rsPurchaseOrder.As<PurchaseOrder>().ToList();
        }

        var rsPurchaseOrderDetailOutsourced = GetRecordset("R_PURCHASEORDERDETAILOUTSOURCED", "", "PK_R_PURCHASEORDERDETAILOUTSOURCED IS NULL", "");
        rsPurchaseOrderDetailOutsourced.UseDataChanges = true;
        rsPurchaseOrderDetailOutsourced.UpdateWhenMoveRecord = false;

        foreach (var purchaseOrder in createdPurchaseOrders)
        {
            var employeeIdsThisSuppliers = employees.Where(x => x.FK_RELATION == purchaseOrder.FK_SUPPLIER).Select(x => x.PK_R_EMPLOYEE).ToList();
            var projectTimesThisEmployees = projectTimes.Where(x => employeeIdsThisSuppliers.Contains(x.FK_EMPLOYEE)).ToList();

            var projectTimesGrouped = projectTimesThisEmployees
                .GroupBy(x => x.GroupByEmployeeOutsourcedctivityDate)
                .OrderBy(x => x.Key)
                .ToList();

            foreach (var groupProjectTimes in projectTimesGrouped)
            {
                var workactivity = workactivitys.First(x => x.PK_R_WORKACTIVITY == groupProjectTimes.First().FK_WORKACTIVITY);
                var outsourcedActivity = outsourcedActivities.First(x => x.PK_R_OUTSOURCEDACTIVITY == workactivity.FK_WKAOUTSOURCEDACTIVITY);

                rsPurchaseOrderDetailOutsourced.AddNew();

                rsPurchaseOrderDetailOutsourced.SetFieldValue("FK_PURCHASEORDER", purchaseOrder.PK_R_PURCHASEORDER);
                rsPurchaseOrderDetailOutsourced.SetFieldValue("FK_OUTSOURCEDACTIVITY", workactivity.FK_WKAOUTSOURCEDACTIVITY);

                var description = groupProjectTimes.First().TimeCode?.GroupByPercentage > 0.0
                    ? $"{groupProjectTimes.First().DATE:dd-MM-yyyy} - {outsourcedActivity.DESCRIPTION} - {groupProjectTimes.First().TimeCode.CODE}"
                    : $"{groupProjectTimes.First().DATE:dd-MM-yyyy} - {outsourcedActivity.DESCRIPTION}";

                rsPurchaseOrderDetailOutsourced.SetFieldValue("DESCRIPTION", description);

                rsPurchaseOrderDetailOutsourced.SetFieldValue("QUANTITY", TimeSpan.FromTicks(groupProjectTimes.Sum(x => x.TIMEEMPLOYEE)).TotalHours * outsourcedActivity.Unit.FACTOR);

                rsPurchaseOrderDetailOutsourced.SetFieldValue("DIRECTTOORDER", false);
                rsPurchaseOrderDetailOutsourced.SetFieldValue("FK_GENERALLEDGERACCOUNT", grootboekDoorbelastingPersoneelskosten);

                rsPurchaseOrderDetailOutsourced.SetFieldValue("EXTERNALKEY", groupProjectTimes.Key);

                if (groupProjectTimes.First().TimeCode?.GroupByPercentage > 0.0)
                {
                    var currentGrossPurchasePrice = (double)rsPurchaseOrderDetailOutsourced.GetField("GROSSPURCHASEPRICE").Value;
                    rsPurchaseOrderDetailOutsourced.SetFieldValue("GROSSPURCHASEPRICE", currentGrossPurchasePrice * (1 + groupProjectTimes.First().TimeCode.GroupByPercentage));
                }

            }
        }

        var updateResult2 = rsPurchaseOrderDetailOutsourced.Update2();

        if (updateResult2.Any(x => x.HasError))
        {
            MessageBox.Show($"Aanmaken inkooporderregel(s) mislukt, oorzaak: {updateResult2.First(x => x.HasError).GetResult()}", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return null;
        }


        var purchaseOrderDetailsOutsourced = rsPurchaseOrderDetailOutsourced.As<PurchaseOrderDetailOutsourced>().ToList();

        createdPurchaseOrders.ForEach(x => x.PurchaseOrderDetailsOutsourced = purchaseOrderDetailsOutsourced.Where(y => y.FK_PURCHASEORDER == x.PK_R_PURCHASEORDER).ToList());

        return createdPurchaseOrders;
    }

    private void DeleteExistingPurchaseOrderDetailsOutsourced(ScriptRecordset rsExistingPurchaseOrderDetailsOutsourced)
    {
        rsExistingPurchaseOrderDetailsOutsourced.UpdateWhenMoveRecord = false;

        rsExistingPurchaseOrderDetailsOutsourced.MoveFirst();
        while (!rsExistingPurchaseOrderDetailsOutsourced.EOF)
        {
            rsExistingPurchaseOrderDetailsOutsourced.Delete();
            rsExistingPurchaseOrderDetailsOutsourced.MoveNext();
        }

        rsExistingPurchaseOrderDetailsOutsourced.MoveFirst();
        rsExistingPurchaseOrderDetailsOutsourced.Update();
    }

    private List<OutsourcedActivity> GetOutsourcedActivities(List<int> outsourcedActivityIds)
    {
        var outsourcedActivities = GetRecordset("R_OUTSOURCEDACTIVITY", "DESCRIPTION, FK_UNIT", $"PK_R_OUTSOURCEDACTIVITY IN ({string.Join(",", outsourcedActivityIds)})", "")
            .As<OutsourcedActivity>().ToList();

        var units = GetRecordset("R_UNIT", "FACTOR", $"PK_R_UNIT IN ({string.Join(",", outsourcedActivities.Select(x => x.FK_UNIT).Distinct())})", "").As<Unit>().ToList();

        outsourcedActivities.ForEach(x => x.Unit = units.First(y => y.PK_R_UNIT == x.FK_UNIT));

        return outsourcedActivities;
    }


}
