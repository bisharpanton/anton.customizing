//  <auto-generated />
namespace Uren_MaakInkooporderVoorZzp.Models
{
    using System;
    
    
    ///  <summary>
    ///  	 Table: R_TIMECODE
    ///  </summary>
    ///  <remarks>
    /// 	Available columns (for debugging, not necessarily available in this class):
    ///  	AFASWAGECOMPONENTCODE, CODE, CREATOR, DATECHANGED, DATECREATED, DESCRIPTION, EXTERNALKEY, FK_CAO, FK_R_TIMECODE, FK_TIMECODEDISCONTINUITY
    ///  	FK_WORKFLOWSTATE, PERCENTAGECALCULATION, PERCENTAGEEMPLOYEE, PERCENTAGEMACHINE, PERCENTAGETIMEOFINLIEUMARKUP, PK_R_TIMECODE, RECORDLINK, USERCHANGED
    ///  
    ///  	Filter used: FK_TABLEINFO = 'e77f7615-40bc-4053-b3b5-e683d60eb2d8' AND (ISSECRET = 0)
    ///  	Included columns: CODE, PERCENTAGEEMPLOYEE
    ///  	Excluded columns: 
    ///  </remarks>
    public partial class TimeCode
    {
        
        private string _CODE;
        
        private System.Nullable<double> _PERCENTAGEEMPLOYEE;
        
        private int _PK_R_TIMECODE;
        
        /// <summary>
        /// 	Code
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 10
        /// </remarks>
        public virtual string CODE
        {
            get
            {
                if (string.IsNullOrEmpty(this._CODE))
                {
                    return "";
                }
                return this._CODE;
            }
            set
            {
                this._CODE = value;
            }
        }
        
        /// <summary>
        /// 	Percentage employee
        /// </summary>
        /// <remarks>
        /// 	DataType: Float
        /// 	Size: 8
        /// </remarks>
        public virtual System.Nullable<double> PERCENTAGEEMPLOYEE
        {
            get
            {
                return this._PERCENTAGEEMPLOYEE;
            }
            set
            {
                this._PERCENTAGEEMPLOYEE = value;
            }
        }
        
        /// <summary>
        /// 	Time code id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        public virtual int PK_R_TIMECODE
        {
            get
            {
                return this._PK_R_TIMECODE;
            }
            set
            {
                this._PK_R_TIMECODE = value;
            }
        }
    }
}
