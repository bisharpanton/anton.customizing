﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uren_MaakInkooporderVoorZzp.Models
{
    partial class PurchaseOrder
    {
        public List<PurchaseOrderDetailOutsourced> PurchaseOrderDetailsOutsourced { get; set; }
        public bool NotAutomaticReceived { get; set; }
    }
}
