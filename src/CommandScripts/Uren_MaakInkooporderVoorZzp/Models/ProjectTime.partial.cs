﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uren_MaakInkooporderVoorZzp.Models
{
    partial class ProjectTime
    {
        public Workactivity Workactivity { get; set; }
        public TimeCode TimeCode { get; set; }

        public string GroupByEmployeeOutsourcedctivityDate => $"{FK_EMPLOYEE}_{Workactivity.FK_WKAOUTSOURCEDACTIVITY}_{DATE:yyyyMMdd}_{TimeCode?.GroupByPercentage ?? 0.0}";
    }
}
