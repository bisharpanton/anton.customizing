﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Drawing;

public class ProcessFormChangeMemo_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //19-03-2019
        //Sluit formulier

        if (FormDataAwareFunctions == null || FormDataAwareFunctions.TableName != "C_FORMCHANGESALESINVOICEMEMO")
        {
            MessageBox.Show("Dit script mag alleen vanaf het formulier wijzig memo aangeroepen worden.");
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        if (FormDataAwareFunctions.GetSelectedRecords().Length > 1)
        {
            MessageBox.Show("Dit script mag slechts voor één formulier tegelijk aangeroepen worden.");
            return;
        }

        FormDataAwareFunctions.Save();

        var rsForm = GetRecordset("C_FORMCHANGESALESINVOICEMEMO", "PROCESS",
            $"PK_C_FORMCHANGESALESINVOICEMEMO = {FormDataAwareFunctions.CurrentRecord.GetPrimaryKeyValue()}", "");
        rsForm.MoveFirst();
        rsForm.Fields["PROCESS"].Value = true;
        rsForm.Update();

        Form.ActiveForm.Close();
    }
}
