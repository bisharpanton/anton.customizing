﻿<#@ template debug="false" hostspecific="true" language="C#" #>
<#@ assembly name="System.Core" #>
<#@ assembly name="EnvDTE" #>
<#@ assembly name="VSLangProj" #>
<#@ import namespace="EnvDTE" #>
<#@ import namespace="System.IO" #>
<#@ import namespace="System.Linq" #>
<#@ import namespace="System.Collections.Generic" #>
<#@ import namespace="VSLangProj" #>
<#@ import namespace="Microsoft.VisualStudio.TextTemplating" #>
<#@ output extension=".txt" #>
<# 
    // Code bestanden van huidige project ophalen
    // true: Alleen bestanden samenvoegen vanaf het huidige .tt bestand (en onderliggende mappen/bestanden), niet bovenliggende bestanden
    //		 Op deze manier kan je bijv. een map 'A' en 'B' maken, met elk een Script.tt en eigen .cs bestanden, ze worden dan niet allemaal samengevoegd in één script, maar per map.
    // false: ALLE bestanden uit het project samen te voegen
    var codefiles = GetCodeFiles(fromCurrentFile: true);

    // Commentaar
    WriteHeader();
    
    // Eerst ridder script uitschrijven
    var ridderfile = Host.ResolvePath("RidderScript.cs");
    WriteFile(ridderfile);
    
    // dan overige bestanden
    foreach(var filename in codefiles)
    {
        WriteFile(filename);
    }
#>
<#+
    // De volgende code bestanden worden genegeerd:
    private readonly string[] ignoredCodeFiles =
        {
            "Program.cs", "AssemblyInfo.cs", "Resources.Designer.cs",
            "Settings.Designer.cs", "RidderScript.cs", "TestForm.cs", "TestForm.designer.cs"
        };

    // De volgende project references worden genegeerd:
    private readonly string[] ignoredReferences =
        {
            "System", "Microsoft", "mscorlib", "Ridder.Common", "Ridder.Client", "adodb", "Ridder.Communication"
        };

    private void WriteHeader()
    {
        WriteLine("/*");
        var desc = FindProjectItem("description.txt");
        if (desc != null)
        {
            var lines = File.ReadAllLines(Host.ResolvePath(desc.FileNames[0]));
            foreach (var line in lines)
            {
                WriteLine(" * " + line);
            }
        }
        else
        {
            WriteLine(" * <Add a description.txt to your project> ");            
        }
        WriteLine(" * ");
        
        // changelog, zoek alleen in root project items
        WriteLine(" * Changelog: ");
        WriteLine(" *   ");
        var item = FindProjectItem("changelog.txt");
        if (item != null)
        {
            var lines = File.ReadAllLines(Host.ResolvePath(item.FileNames[0]));
            foreach (var line in lines)
            {
                WriteLine(" *   " + line);
            }

        }
        else
        {
            WriteLine(" *   <add a changelog.txt to your project>");            
        }

        var references =
            GetProjectReferences()
                .Where(r => !ignoredReferences.Any(ign => r.Name.StartsWith(ign, StringComparison.OrdinalIgnoreCase)))
                .ToList();

        if (references.Any())
        {
            WriteLine(" * ");
            WriteLine(" * References:");
            WriteLine(" * ");
            foreach (var reference in references)
            {
                WriteLine(string.Format(" *   {0} ({1})", reference.Name, reference.Path));
            }
        }

        WriteLine(" * ");
        WriteLine(string.Format(" * Generated on: {0} by {1}", DateTime.Now.ToString("yyyy-MM-dd HH:mm"), Environment.UserName));
        WriteLine(" *   ");
        WriteLine(" */");
        Write(Environment.NewLine);
    }

    private IEnumerable<Reference> GetProjectReferences()
    {
        var visualStudio = (this.Host as IServiceProvider).GetCOMService(typeof(DTE)) as DTE;
        var project = visualStudio.Solution.FindProjectItem(this.Host.TemplateFile).ContainingProject;
        var vsProject = (VSProject)project.Object;
        
        return vsProject.References.Cast<Reference>();
    }

    private ProjectItem FindProjectItem(string fileName)
    {
        var visualStudio = (this.Host as IServiceProvider).GetCOMService(typeof(DTE)) as DTE;

        var projectItem = visualStudio.Solution.FindProjectItem(this.Host.TemplateFile);
        IEnumerable<ProjectItem> projectItems = null;
        projectItems = GetProjectItemsRecursively(projectItem.Collection);

        foreach (var item in projectItems)
        {
            if (item.Name.Equals(fileName, StringComparison.OrdinalIgnoreCase))
            {
                return item;
            }
        }

        return null;

    }

    // Haalt code items uit het huidige project.
    private IEnumerable<string> GetCodeFiles(bool fromCurrentFile = true)
    {
        var visualStudio = (this.Host as IServiceProvider).GetCOMService(typeof(DTE)) as DTE;

        var projectItem = visualStudio.Solution.FindProjectItem(this.Host.TemplateFile);
        IEnumerable<ProjectItem> projectItems = null;
        if (fromCurrentFile)
        {
            projectItems = GetProjectItemsRecursively(projectItem.Collection);
        }
        else
        {
            projectItems = GetProjectItemsRecursively(projectItem.ContainingProject.ProjectItems);
        }

        foreach(var item in projectItems)
        {
            if (item.FileCodeModel == null) continue;
            var filename = item.FileNames[0];

            if (!ignoredCodeFiles.Any(ign => filename.EndsWith(ign, StringComparison.OrdinalIgnoreCase)))
            {
                yield return filename;
            }
        }                
    }

    private void WriteFile(string filename)
    {
        // Leest bestand en schuift alles boven 'namespace' naar binnen.
        // dus:
        //   using bla;
        //   namespace xxx {
        //       // ...
        //   }
        //
        // wordt:
        //   namespace xxx {
        //       using bla;
        //       // ...
        //   }

        var lines = File.ReadAllLines(Host.ResolvePath(filename));
        WriteLine("/*");
        WriteLine(" * " + Path.GetFileName(filename));
        WriteLine(" */");

        int currentLine = 0;
        var namespaces = new List<string>();
        while (currentLine < lines.Length)
        {
            var line = lines[currentLine];
            if (!line.Trim().StartsWith("namespace", StringComparison.OrdinalIgnoreCase))
            {
                namespaces.Add(line);
                currentLine++;
                continue;
            }

            if (line.Contains("{"))
            {
                WriteLine(line);
                currentLine++;
            }
            else
            {
                WriteLine(line);
                currentLine++;
                while (!line.Contains("{") && currentLine < lines.Length)
                {
                    line = lines[currentLine];
                    WriteLine(line);
                    currentLine++;
               }
            }

            foreach (var ns in namespaces)
            {
                WriteLine("    " + ns);
            }
            
            namespaces.Clear();

            break;
        }

        foreach (var ns in namespaces)
        {
            WriteLine(ns);            
        }

        while (currentLine < lines.Length)
        {
            WriteLine(lines[currentLine]);
            currentLine++;
        }

        Write(Environment.NewLine);
    }

    private IEnumerable<ProjectItem> GetProjectItemsRecursively(ProjectItems items)
    {
        var ret = new List<ProjectItem>();
        if (items == null) return ret;
        foreach(ProjectItem item in items)
        {
            ret.Add(item);
            ret.AddRange(GetProjectItemsRecursively(item.ProjectItems));
        }
        return ret;
    }
#>
