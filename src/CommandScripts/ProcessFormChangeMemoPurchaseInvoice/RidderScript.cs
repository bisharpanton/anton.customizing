﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class ProcessFormChangeMemoPurchaseInvoice_RidderScript : CommandScript
{
	public void Execute()
	{
        //SP
        //14-05-2024
        //Sluit formulier

        if (FormDataAwareFunctions == null || FormDataAwareFunctions.TableName != "C_FORMCHANGESALESINVOICEMEMO")
        {
            MessageBox.Show("Dit script mag alleen vanaf het formulier wijzig memo aangeroepen worden.");
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        if (FormDataAwareFunctions.GetSelectedRecords().Length > 1)
        {
            MessageBox.Show("Dit script mag slechts voor één formulier tegelijk aangeroepen worden.");
            return;
        }

        FormDataAwareFunctions.Save();

        var rsForm = GetRecordset("C_FORMCHANGESALESINVOICEMEMO", "PROCESS",
            $"PK_C_FORMCHANGESALESINVOICEMEMO = {FormDataAwareFunctions.CurrentRecord.GetPrimaryKeyValue()}", "");
        rsForm.MoveFirst();
        rsForm.Fields["PROCESS"].Value = true;
        rsForm.Update();

        Form.ActiveForm.Close();


    }
}
