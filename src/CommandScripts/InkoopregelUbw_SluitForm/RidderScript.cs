﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class InkoopregelUbw_SluitForm_RidderScript : CommandScript
{
    public void Execute()
    {
        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("U_FORMDETAILSUPPLIERMANDAYREGISTERDETAILS"))
        {
            MessageBox.Show("Dit script mag alleen vanaf Form Onderaannemer mandagenregister details aangeroepen worden.");
            return;
        }

        FormDataAwareFunctions.Save();

        var formId = (int)FormDataAwareFunctions.FormParent.CurrentRecord.GetPrimaryKeyValue();

        var rsForm = GetRecordset("U_FORMSUPPLIERMANDAYREGISTERDETAILS", "", $"PK_U_FORMSUPPLIERMANDAYREGISTERDETAILS = {formId}", "");
        rsForm.MoveFirst();

        var formDetails = GetRecordset("U_FORMDETAILSUPPLIERMANDAYREGISTERDETAILS", "",
            $"FK_FORMSUPPLIERMANDAYREGISTERDETAILS = {formId}", "").DataTable.AsEnumerable().ToList();

        if(formDetails.Any(x => string.IsNullOrEmpty(x.Field<string>("EXTERNALEMPLOYEE"))))
        {
            MessageBox.Show("Externe werknemer is verplicht.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        if (formDetails.Any(x => x.Field<int>("YEAR") == 0 || x.Field<int>("WEEK") == 0))
        {
            MessageBox.Show("Jaar en/of week kunnen niet nul zijn. ", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        var updateQuantity = false;
        var checkTimeEquals = TimeWkaEqualsTimePurchaseOrderDetail(formDetails, (int)rsForm.GetField("FK_PURCHASEORDERDETAILOUTSOURCED").Value);
        if (!string.IsNullOrEmpty(checkTimeEquals))
        {
            var dgResult = MessageBox.Show(checkTimeEquals, "Ridder iQ", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

            if (dgResult == DialogResult.Cancel)
            {
                return;
            }

            updateQuantity = dgResult == DialogResult.Yes;
        }


        rsForm.SetFieldValue("UPDATEQUANTITY", updateQuantity);
        rsForm.SetFieldValue("PROCESS", true);
        rsForm.Update();


        if(updateQuantity && formDetails.GroupBy(x => x.Field<string>("EXTERNALEMPLOYEE")).Count() > 1)
        {
            MessageBox.Show("Let op, het aantal van de inkoopregel wordt aangepast, maar er zijn meerdere werknemers van toepassing. Controleer het uurtarief.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }


        Form.ActiveForm.Close();
    }

    private string TimeWkaEqualsTimePurchaseOrderDetail(List<DataRow> formDetails, int purchaseOrderDetailOutsourcedId)
    {
        var totalTimeFormDetails = formDetails.Sum(x => x.Field<long>("MONDAY") + x.Field<long>("TUESDAY") + x.Field<long>("WEDNESDAY") + x.Field<long>("THURSDAY")
                                                    + x.Field<long>("FRIDAY") + x.Field<long>("SATURDAY") + x.Field<long>("SUNDAY"));

        var purchaseOrderDetailOutsourced = GetRecordset("R_PURCHASEORDERDETAILOUTSOURCED", "FK_OUTSOURCEDACTIVITY, QUANTITY",
            $"PK_R_PURCHASEORDERDETAILOUTSOURCED = {purchaseOrderDetailOutsourcedId}", "").DataTable.AsEnumerable().First();

        var unitId = GetRecordset("R_OUTSOURCEDACTIVITY", "FK_UNIT",
            $"PK_R_OUTSOURCEDACTIVITY = {purchaseOrderDetailOutsourced.Field<int>("FK_OUTSOURCEDACTIVITY")}", "").DataTable.AsEnumerable().First().Field<int>("FK_UNIT");

        var unit = GetRecordset("R_UNIT", "FACTOR", $"PK_R_UNIT = {unitId}", "").DataTable.AsEnumerable().First();

        var quantityPurchased = purchaseOrderDetailOutsourced.Field<double>("QUANTITY") / unit.Field<double>("FACTOR");

        var tsQuantityPurchase = TimeSpan.FromHours(quantityPurchased);
        var tsTotalTimeFormDetails = TimeSpan.FromTicks(totalTimeFormDetails);

        return Math.Floor(tsQuantityPurchase.TotalHours) != Math.Floor(tsTotalTimeFormDetails.TotalHours) || tsQuantityPurchase.Minutes != tsTotalTimeFormDetails.Minutes
            ? $"Totale tijd mandagenregister ({Math.Floor(tsTotalTimeFormDetails.TotalHours):n0}u {tsTotalTimeFormDetails.Minutes}m) komt niet overeen met aantal uur uit inkoopregel ({Math.Floor(tsQuantityPurchase.TotalHours):n0}u {tsQuantityPurchase.Minutes}m). \n\nAantal van de inkoopregel aanpassen?"
            : string.Empty;
    }
}
