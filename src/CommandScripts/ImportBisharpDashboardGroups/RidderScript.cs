﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class ImportBisharpDashboardGroups_RidderScript : CommandScript
{
    public void Execute()
    {
        //SP
        //2-11-2022
        //Importeer bisharp dashboard groepen

        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("U_BISHARPPORTALUSER", StringComparison.OrdinalIgnoreCase))
        {
            MessageBox.Show("Dit script mag alleen van de Bisharp portal user tabel aangeroepen worden.");
            return;
        }

        var userIds = FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue()).ToList();

        object[] selectie = null;
        selectie = OpenMultiSelectForm("U_BISHARPDASHBOARDGROUP", "PK_U_BISHARPDASHBOARDGROUP",
            "", "");

        if (selectie == null)
        {
            return;
        }

        ImportDashboardGroups(selectie, userIds);

    }

    private void ImportDashboardGroups(object[] selectie, List<int> userIds)
    {
        var rsDashboardGroupsPerPortalUser = GetRecordset("U_BISHARPAVAILABLEDASHBOARDGROUPSPERPORTALUSER", "",
            "PK_U_BISHARPAVAILABLEDASHBOARDGROUPSPERPORTALUSER = -1", "");
        rsDashboardGroupsPerPortalUser.UseDataChanges = true;
        rsDashboardGroupsPerPortalUser.UpdateWhenMoveRecord = false;

        foreach(var userId in userIds)
        {
            foreach (var dashBoardGroupId in selectie)
            {
                rsDashboardGroupsPerPortalUser.AddNew();
                rsDashboardGroupsPerPortalUser.Fields["FK_BISHARPDASHBOARDGROUP"].Value = dashBoardGroupId;
                rsDashboardGroupsPerPortalUser.Fields["FK_BISHARPPORTALUSER"].Value = userId;
            }
        }

        var updateResult = rsDashboardGroupsPerPortalUser.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Het importeren van Bisharp dashboard groepen is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

    }
}
