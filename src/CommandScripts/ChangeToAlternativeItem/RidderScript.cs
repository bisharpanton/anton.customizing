﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class ChangeToAlternativeItem_RidderScript : CommandScript
{
	public void Execute()
	{
        //SP
        //10-8-2023
        //Wijzig de inkooporderregel artikel naar een alternatief artikel

        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("R_PURCHASEORDERDETAILITEM"))
        {
            MessageBox.Show("Dit script mag alleen vanaf de inkoopregel artikel aangeroepen worden.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        if (FormDataAwareFunctions.GetSelectedRecords().Count() > 1)
        {
            MessageBox.Show("Dit script mag alleen slechts voor één inkoopregel artikel tegelijk aangeroepen worden.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        var purchaseOrderDetailItemId = (int)FormDataAwareFunctions.GetSelectedRecords().First().GetPrimaryKeyValue();

        var rsPurchaseOrderDetailItem = GetRecordset("R_PURCHASEORDERDETAILITEM", "FK_ITEM, FK_PURCHASEFRAMEWORKDETAIL, GROSSPURCHASEPRICE",
            $"PK_R_PURCHASEORDERDETAILITEM = {purchaseOrderDetailItemId}", "");

        var purchaseFrameworkDetailId = rsPurchaseOrderDetailItem.DataTable.AsEnumerable().FirstOrDefault().Field<int?>("FK_PURCHASEFRAMEWORKDETAIL") ?? 0;

        if(purchaseFrameworkDetailId == 0)
        {
            MessageBox.Show("Deze inkoopregel is niet gekoppeld aan een raamcontract.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        List<int> ItemIds = new List<int>();

        var purchaseFrameworkDetail = GetRecordset("U_PURCHASEFRAMEWORKDETAIL", "FK_ITEM, GROSSCONTRACTPRICE",
            $"PK_U_PURCHASEFRAMEWORKDETAIL = {purchaseFrameworkDetailId}", "").DataTable.AsEnumerable().First();

        var originalItemId = purchaseFrameworkDetail.Field<int>("FK_ITEM");

        ItemIds.Add(originalItemId);

        var alternativeItemIds = GetRecordset("U_PURCHASEFRAMEWORKDETAILALTERNATIVE", "FK_ITEM",
            $"FK_PURCHASEFRAMEWORKDETAIL = {purchaseFrameworkDetailId}", "").DataTable.AsEnumerable().Select(x => x.Field<int>("FK_ITEM")).ToList();

        foreach(var alternativeItemId in alternativeItemIds)
        {
            ItemIds.Add(alternativeItemId);
        }

        ItemIds.Remove(rsPurchaseOrderDetailItem.DataTable.AsEnumerable().FirstOrDefault().Field<int>("FK_ITEM"));

        if(!ItemIds.Any())
        {
            MessageBox.Show("Geen alternatieven gevonden.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        var resultItemId = OpenBrowseFormWithFilter("R_ITEM", "PK_R_ITEM",
            $"PK_R_ITEM IN ({string.Join(", ", ItemIds)})", true);

        if (resultItemId == null)
        {
            return;
        }

        if((int)resultItemId == originalItemId)
        {
            UpdatePurchaseorderDetailItem(rsPurchaseOrderDetailItem, (int)resultItemId, purchaseFrameworkDetailId, purchaseFrameworkDetail.Field<double>("GROSSCONTRACTPRICE"));
        }
        else
        {
            UpdatePurchaseorderDetailItem(rsPurchaseOrderDetailItem, (int)resultItemId, purchaseFrameworkDetailId, 0.0);
        }
    }

    private void UpdatePurchaseorderDetailItem(ScriptRecordset rsPurchaseOrderDetailItem, int itemId, int purchaseFrameworkDetailId, double grossContractPrice)
    {
        double newPrice = 0.0;

        if(grossContractPrice == 0.0)
        {
            var rsPurchaseFrameworkDetailAlternative = GetRecordset("U_PURCHASEFRAMEWORKDETAILALTERNATIVE", "FK_ITEM, ORIGINALPURCHASEPRICE",
           $"FK_ITEM = {itemId} AND FK_PURCHASEFRAMEWORKDETAIL = {purchaseFrameworkDetailId}", "").DataTable.AsEnumerable().First();

            var originalPrice = rsPurchaseFrameworkDetailAlternative.Field<double>("ORIGINALPURCHASEPRICE");

            double discount = CalculateDiscount(purchaseFrameworkDetailId);

            newPrice = originalPrice - discount;
        }
        else
        {
            newPrice = grossContractPrice;
        }

        rsPurchaseOrderDetailItem.UseDataChanges = true;

        rsPurchaseOrderDetailItem.MoveFirst();
        rsPurchaseOrderDetailItem.SetFieldValue("FK_ITEM", itemId);
        rsPurchaseOrderDetailItem.SetFieldValue("GROSSPURCHASEPRICE", newPrice);

        var updateResult = rsPurchaseOrderDetailItem.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Wijzigen inkoopregel naar alternatief artikel mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }
    }

    private double CalculateDiscount(int purchaseFrameworkDetailId)
    {
        var rsPurchaseFrameworkDetail = GetRecordset("U_PURCHASEFRAMEWORKDETAIL", "ORIGINALPURCHASEPRICE, GROSSCONTRACTPRICE",
            $"PK_U_PURCHASEFRAMEWORKDETAIL = {purchaseFrameworkDetailId}", "").DataTable.AsEnumerable().First();

        var originalPrice = rsPurchaseFrameworkDetail.Field<double>("ORIGINALPURCHASEPRICE");
        var grossContractPrice = rsPurchaseFrameworkDetail.Field<double>("GROSSCONTRACTPRICE");

        return originalPrice - grossContractPrice;
    }
}
