﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Drawing;
using Ridder.Client.SDK.SDKParameters;

public class Order___Open_offerte_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //14-1-2019
        //Open de offerte vanuit de order

        if (FormDataAwareFunctions == null || FormDataAwareFunctions.TableName != "R_ORDER")
        {
            MessageBox.Show("Dit script mag alleen vanaf de order-tabel aangeroepen worden.");
            return;
        }

        if(!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        var orderIds = FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue()).ToList();

        var offerIds = GetOfferIds(orderIds);

        if (!offerIds.Any())
        {
            MessageBox.Show("Geen offerte(s) gevonden.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        OpenOffers(offerIds);
    }

    private void OpenOffers(List<int> offerIds)
    {
        if (offerIds.Count > 2)
        {
            var browsePar = new BrowseParameters()
            {
                TableName = "R_OFFER", Filter = $"PK_R_OFFER IN ({string.Join(",", offerIds)})",
            };

            OpenBrowse(browsePar, false);
        }
        else
        {
            foreach (var offerId in offerIds)
            {
                var editPar = new EditParameters()
                {
                    TableName = "R_OFFER",
                    Id = offerId,
                };

                OpenEdit(editPar, false);
            }
        }
    }

    private List<int> GetOfferIds(List<int> orderIds)
    {
        var result = new List<int>();
        var sources = new List<string>() {"ITEM", "MISC", "ASSEMBLY", "WORKACTIVITY"};

        foreach (var source in sources)
        {
            var offerDetailIds = GetRecordset(
                $"R_SALESORDERDETAIL{source}", $"FK_OFFERDETAIL{source}",
                $"FK_ORDER IN ({string.Join(",", orderIds)}) AND FK_OFFERDETAIL{source} IS NOT NULL",
                "").DataTable.AsEnumerable().Select(x => x.Field<int>($"FK_OFFERDETAIL{source}")).ToList();

            if(!offerDetailIds.Any())
            {
                continue;
            }

            var offerIds = GetRecordset($"R_OFFERDETAIL{source}", "FK_OFFER",
                $"PK_R_OFFERDETAIL{source} IN ({string.Join(",", offerDetailIds)})",
                "").DataTable.AsEnumerable().Select(x => x.Field<int>("FK_OFFER")).Distinct().ToList();

            result.AddRange(offerIds);
        }

        return result.Distinct().ToList();
    }
}
