﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using CreateVeiligheidsmelding.Models;
using Ridder.Client.SDK;
using Ridder.Client.SDK.SDKParameters;
using Ridder.Recordset.Extensions;

public class CreateVeiligheidsmelding_RidderScript : CommandScript
{
    public void Execute()
    {
        // HVE
        // 13-07-2021
        // Genereer veiligheidsmelding vanuit observatie    

        if (FormDataAwareFunctions.TableName == null || FormDataAwareFunctions.TableName != "C_VGINSPECTIEGEDRAGSPUNTEN")
        {
            MessageBox.Show("Dit script mag alleen vanaf de observatie-tabel aangeroepen worden.");
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        var observationId = (int)FormDataAwareFunctions.CurrentRecord.GetPrimaryKeyValue();
        
        var sdkDefaultValues = new SDKDefaultValues();
        sdkDefaultValues.AddColumn("FK_MELDER", DetermineCurrentEmployee());
        sdkDefaultValues.AddColumn("FK_BRONMELDINGEN", DetermineBronMelding());
        
        var editPar = new EditParameters()
        {
            TableName = "C_VEILIGHEIDSMELDINGEN",
            Id = 0,
            SdkDefaultValues = sdkDefaultValues,
        };

        OpenEdit(editPar, false);

    }

    private int DetermineCurrentEmployee()
    {
        return GetRecordset("R_USER", "FK_EMPLOYEE",
                $"PK_R_USER = {GetUserInfo().CurrentUserId}", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int?>("FK_EMPLOYEE") ?? 0)
            .FirstOrDefault();
    }

    private int DetermineBronMelding()
    {
        return GetRecordset("C_BRONMELDINGEN", "PK_C_BRONMELDINGEN",
                $"DESCRIPTION = 'Observatie'", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int?>("PK_C_BRONMELDINGEN") ?? 0)
            .FirstOrDefault();
    }
}
