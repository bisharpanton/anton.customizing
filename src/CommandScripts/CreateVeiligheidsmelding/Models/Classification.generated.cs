

namespace CreateVeiligheidsmelding.Models
{
    using System;
    using System.ComponentModel;
    
    
    public enum Classification : int
    {
        
        /// <summary>
        ///  
        /// </summary>
        [Description(" ")]
        N_ = 1,
        
        /// <summary>
        /// Best practice
        /// </summary>
        [Description("Best practice")]
        Best_practice = 2,
        
        /// <summary>
        /// Low potential
        /// </summary>
        [Description("Low potential")]
        Low_potential = 3,
        
        /// <summary>
        /// Medium potential
        /// </summary>
        [Description("Medium potential")]
        Medium_potential = 4,
        
        /// <summary>
        /// High potential
        /// </summary>
        [Description("High potential")]
        High_potential = 5,
    }
}
