﻿/*
 * 7/13/2021 2:18:13 PM
 * hveldhorst
 * Created CreateVeiligheidsmelding
 * 
 * Changelog: 
 *   
 * 
 * References:
 * 
 *   Ridder.iQ.Common.Recordset.Extensions (C:\Users\Douwe Beukers\.nuget\packages\ridder.clientdevelopment\1.8.251.16\lib\Ridder.iQ.Common.Recordset.Extensions.dll)
 *   Newtonsoft.Json (C:\Users\Douwe Beukers\.nuget\packages\newtonsoft.json\12.0.1\lib\net45\Newtonsoft.Json.dll)
 * 
 * Generated on: 2022-06-21 13:40 by Douwe Beukers
 *   
 */

/*
 * RidderScript.cs
 */
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using CreateVeiligheidsmelding.Models;
using Ridder.Client.SDK;
using Ridder.Client.SDK.SDKParameters;
using Ridder.Recordset.Extensions;

public class CreateVeiligheidsmelding_RidderScript : CommandScript
{
    public void Execute()
    {
        // HVE
        // 13-07-2021
        // Genereer veiligheidsmelding vanuit observatie    

        if (FormDataAwareFunctions.TableName == null || FormDataAwareFunctions.TableName != "C_VGINSPECTIEGEDRAGSPUNTEN")
        {
            MessageBox.Show("Dit script mag alleen vanaf de observatie-tabel aangeroepen worden.");
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        var observationId = (int)FormDataAwareFunctions.CurrentRecord.GetPrimaryKeyValue();
        
        var sdkDefaultValues = new SDKDefaultValues();
        sdkDefaultValues.AddColumn("FK_MELDER", DetermineCurrentEmployee());
        sdkDefaultValues.AddColumn("FK_BRONMELDINGEN", DetermineBronMelding());
        
        var editPar = new EditParameters()
        {
            TableName = "C_VEILIGHEIDSMELDINGEN",
            Id = 0,
            SdkDefaultValues = sdkDefaultValues,
        };

        OpenEdit(editPar, false);

    }

    private int DetermineCurrentEmployee()
    {
        return GetRecordset("R_USER", "FK_EMPLOYEE",
                $"PK_R_USER = {GetUserInfo().CurrentUserId}", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int?>("FK_EMPLOYEE") ?? 0)
            .FirstOrDefault();
    }

    private int DetermineBronMelding()
    {
        return GetRecordset("C_BRONMELDINGEN", "PK_C_BRONMELDINGEN",
                $"DESCRIPTION = 'Observatie'", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int?>("PK_C_BRONMELDINGEN") ?? 0)
            .FirstOrDefault();
    }
}

/*
 * Classification.generated.cs
 */
namespace CreateVeiligheidsmelding.Models
{
    
    
    using System;
    using System.ComponentModel;
    
    
    public enum Classification : int
    {
        
        /// <summary>
        ///  
        /// </summary>
        [Description(" ")]
        N_ = 1,
        
        /// <summary>
        /// Best practice
        /// </summary>
        [Description("Best practice")]
        Best_practice = 2,
        
        /// <summary>
        /// Low potential
        /// </summary>
        [Description("Low potential")]
        Low_potential = 3,
        
        /// <summary>
        /// Medium potential
        /// </summary>
        [Description("Medium potential")]
        Medium_potential = 4,
        
        /// <summary>
        /// High potential
        /// </summary>
        [Description("High potential")]
        High_potential = 5,
    }
}

/*
 * GedragspuntStatus.generated.cs
 */
namespace CreateVeiligheidsmelding.Models
{
    using System;
    using System.ComponentModel;
    
    
    public enum GedragspuntStatus : int
    {
        
        /// <summary>
        /// Openstaand
        /// </summary>
        [Description("Openstaand")]
        Openstaand = 1,
        
        /// <summary>
        /// Afgehandeld
        /// </summary>
        [Description("Afgehandeld")]
        Afgehandeld = 2,
    }
}

/*
 * Incidentmelding.generated.cs
 */
namespace CreateVeiligheidsmelding.Models
{
    using System;
    using System.ComponentModel;
    
    
    public enum Incidentmelding : int
    {
        
        /// <summary>
        /// -
        /// </summary>
        [Description("-")]
        N_ = 1,
        
        /// <summary>
        /// Ongeval met verzuim
        /// </summary>
        [Description("Ongeval met verzuim")]
        Ongeval_met_verzuim = 2,
        
        /// <summary>
        /// Ongeval met aangepast werk
        /// </summary>
        [Description("Ongeval met aangepast werk")]
        Ongeval_met_aangepast_werk = 3,
        
        /// <summary>
        /// Ongeval zonder verzuim (vanaf EHBO)
        /// </summary>
        [Description("Ongeval zonder verzuim (vanaf EHBO)")]
        Ongeval_zonder_verzuim__vanaf_EHBO_ = 4,
        
        /// <summary>
        /// Bijna-ongeval
        /// </summary>
        [Description("Bijna-ongeval")]
        Bijna_ongeval = 5,
        
        /// <summary>
        /// Onveilige situatie en handeling
        /// </summary>
        [Description("Onveilige situatie en handeling")]
        Onveilige_situatie_en_handeling = 6,
        
        /// <summary>
        /// Overige VGM-incidenten
        /// </summary>
        [Description("Overige VGM-incidenten")]
        Overige_VGM_incidenten = 7,
        
        /// <summary>
        /// Positieve melding
        /// </summary>
        [Description("Positieve melding")]
        Positieve_melding = 8,
    }
}

/*
 * IntercompanyRelation.generated.cs
 */
namespace CreateVeiligheidsmelding.Models
{
    using System;
    using System.ComponentModel;
    
    
    public enum IntercompanyRelation : int
    {
        
        /// <summary>
        /// N.v.t.
        /// </summary>
        [Description("N.v.t.")]
        N_v_t_ = 1,
        
        /// <summary>
        /// Anton Air Support
        /// </summary>
        [Description("Anton Air Support")]
        Anton_Air_Support = 2,
        
        /// <summary>
        /// Anton Bouw & Betontechniek
        /// </summary>
        [Description("Anton Bouw & Betontechniek")]
        Anton_Bouw___Betontechniek = 3,
        
        /// <summary>
        /// Anton Civiel en Infratechniek
        /// </summary>
        [Description("Anton Civiel en Infratechniek")]
        Anton_Civiel_en_Infratechniek = 4,
        
        /// <summary>
        /// Anton Constructiewerken
        /// </summary>
        [Description("Anton Constructiewerken")]
        Anton_Constructiewerken = 5,
        
        /// <summary>
        /// Anton Groep
        /// </summary>
        [Description("Anton Groep")]
        Anton_Groep = 6,
        
        /// <summary>
        /// Anton Industrial Services
        /// </summary>
        [Description("Anton Industrial Services")]
        Anton_Industrial_Services = 7,
        
        /// <summary>
        /// Anton Rail & Infra
        /// </summary>
        [Description("Anton Rail & Infra")]
        Anton_Rail___Infra = 8,
        
        /// <summary>
        /// Anton Staalbouw
        /// </summary>
        [Description("Anton Staalbouw")]
        Anton_Staalbouw = 9,
        
        /// <summary>
        /// BBD
        /// </summary>
        [Description("BBD")]
        BBD = 10,
        
        /// <summary>
        /// De Leeuw Protection Systems
        /// </summary>
        [Description("De Leeuw Protection Systems")]
        De_Leeuw_Protection_Systems = 11,
        
        /// <summary>
        /// Harder Constructie en Adviesbureau
        /// </summary>
        [Description("Harder Constructie en Adviesbureau")]
        Harder_Constructie_en_Adviesbureau = 12,
        
        /// <summary>
        /// Hoogendijk Bouw
        /// </summary>
        [Description("Hoogendijk Bouw")]
        Hoogendijk_Bouw = 13,
        
        /// <summary>
        /// Loos Betonreparaties
        /// </summary>
        [Description("Loos Betonreparaties")]
        Loos_Betonreparaties = 14,
        
        /// <summary>
        /// Loos Betonvloeren
        /// </summary>
        [Description("Loos Betonvloeren")]
        Loos_Betonvloeren = 15,
        
        /// <summary>
        /// Loos Betonvloeren Zuid
        /// </summary>
        [Description("Loos Betonvloeren Zuid")]
        Loos_Betonvloeren_Zuid = 16,
        
        /// <summary>
        /// Loos Wapeningscentrale
        /// </summary>
        [Description("Loos Wapeningscentrale")]
        Loos_Wapeningscentrale = 17,
    }
}

/*
 * Observatieronde.generated.cs
 */
namespace CreateVeiligheidsmelding.Models
{
    //  <auto-generated />
    using System;
    
    
    ///  <summary>
    ///  	 Table: C_VGINSPECTIEGEDRAGSPUNTEN
    ///  </summary>
    ///  <remarks>
    /// 	Available columns (for debugging, not necessarily available in this class):
    ///  	ACTIVITEIT, BEKNOPTEWEERGAVE, CREATOR, DATE, DATECHANGED, DATECREATED, EXTERNALKEY, FK_EMPLOYEE, FK_GEDRAGSONDERWERP, FK_ORDER
    ///  	FK_TODO, FK_VGINSPECTIE, FK_WORKFLOWSTATE, PK_C_VGINSPECTIEGEDRAGSPUNTEN, PLAINTEXT_ACTIVITEIT, PLAINTEXT_BEKNOPTEWEERGAVE, PLAINTEXT_RESULTAAT, RECORDLINK, RESULTAAT, STATE
    ///  	USERCHANGED
    ///  
    ///  	Filter used: FK_TABLEINFO = '6f08aee7-05f5-4c10-8b47-3ddbe5dd0ae0' AND (ISSECRET = 0)
    ///  	Included columns: 
    ///  	Excluded columns: 
    ///  </remarks>
    public partial class Observatieronde
    {
        
        private string _ACTIVITEIT;
        
        private string _BEKNOPTEWEERGAVE;
        
        private string _CREATOR;
        
        private System.Nullable<System.DateTime> _DATE;
        
        private System.Nullable<System.DateTime> _DATECHANGED;
        
        private System.Nullable<System.DateTime> _DATECREATED;
        
        private string _EXTERNALKEY;
        
        private System.Nullable<int> _FK_EMPLOYEE;
        
        private System.Nullable<int> _FK_GEDRAGSONDERWERP;
        
        private System.Nullable<int> _FK_ORDER;
        
        private System.Nullable<int> _FK_TODO;
        
        private System.Nullable<int> _FK_VGINSPECTIE;
        
        private System.Nullable<System.Guid> _FK_WORKFLOWSTATE;
        
        private int _PK_C_VGINSPECTIEGEDRAGSPUNTEN;
        
        private string _PLAINTEXT_ACTIVITEIT;
        
        private string _PLAINTEXT_BEKNOPTEWEERGAVE;
        
        private string _PLAINTEXT_RESULTAAT;
        
        private string _RECORDLINK;
        
        private string _RESULTAAT;
        
        private GedragspuntStatus _STATE;
        
        private string _USERCHANGED;
        
        /// <summary>
        /// 	Activiteit / Plaats
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        public virtual string ACTIVITEIT
        {
            get
            {
                if (string.IsNullOrEmpty(this._ACTIVITEIT))
                {
                    return "";
                }
                return this._ACTIVITEIT;
            }
            set
            {
                this._ACTIVITEIT = value;
            }
        }
        
        /// <summary>
        /// 	Beknopte weergave gesprek
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        public virtual string BEKNOPTEWEERGAVE
        {
            get
            {
                if (string.IsNullOrEmpty(this._BEKNOPTEWEERGAVE))
                {
                    return "";
                }
                return this._BEKNOPTEWEERGAVE;
            }
            set
            {
                this._BEKNOPTEWEERGAVE = value;
            }
        }
        
        /// <summary>
        /// 	Aangemaakt door
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        public virtual string CREATOR
        {
            get
            {
                if (string.IsNullOrEmpty(this._CREATOR))
                {
                    return "";
                }
                return this._CREATOR;
            }
            set
            {
                this._CREATOR = value;
            }
        }
        
        /// <summary>
        /// 	Datum
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        public virtual System.Nullable<System.DateTime> DATE
        {
            get
            {
                return this._DATE;
            }
            set
            {
                this._DATE = value;
            }
        }
        
        /// <summary>
        /// 	Datum gewijzigd
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        public virtual System.Nullable<System.DateTime> DATECHANGED
        {
            get
            {
                return this._DATECHANGED;
            }
            set
            {
                this._DATECHANGED = value;
            }
        }
        
        /// <summary>
        /// 	Datum aangemaakt
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        public virtual System.Nullable<System.DateTime> DATECREATED
        {
            get
            {
                return this._DATECREATED;
            }
            set
            {
                this._DATECREATED = value;
            }
        }
        
        /// <summary>
        /// 	Externe sleutel
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 100
        /// </remarks>
        public virtual string EXTERNALKEY
        {
            get
            {
                if (string.IsNullOrEmpty(this._EXTERNALKEY))
                {
                    return "";
                }
                return this._EXTERNALKEY;
            }
            set
            {
                this._EXTERNALKEY = value;
            }
        }
        
        /// <summary>
        /// 	Observator
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: C_VGINSPECTIEGEDRAGSPUNTEN
        /// </remarks>
        public virtual System.Nullable<int> FK_EMPLOYEE
        {
            get
            {
                return this._FK_EMPLOYEE;
            }
            set
            {
                this._FK_EMPLOYEE = value;
            }
        }
        
        /// <summary>
        /// 	Gedragsonderwerp
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: C_VGINSPECTIEGEDRAGSPUNTEN
        /// </remarks>
        public virtual System.Nullable<int> FK_GEDRAGSONDERWERP
        {
            get
            {
                return this._FK_GEDRAGSONDERWERP;
            }
            set
            {
                this._FK_GEDRAGSONDERWERP = value;
            }
        }
        
        /// <summary>
        /// 	Order
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: C_VGINSPECTIEGEDRAGSPUNTEN
        /// </remarks>
        public virtual System.Nullable<int> FK_ORDER
        {
            get
            {
                return this._FK_ORDER;
            }
            set
            {
                this._FK_ORDER = value;
            }
        }
        
        /// <summary>
        /// 	Taak
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: C_VGINSPECTIEGEDRAGSPUNTEN
        /// </remarks>
        public virtual System.Nullable<int> FK_TODO
        {
            get
            {
                return this._FK_TODO;
            }
            set
            {
                this._FK_TODO = value;
            }
        }
        
        /// <summary>
        /// 	V&G Inspectie
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: C_VGINSPECTIEGEDRAGSPUNTEN
        /// </remarks>
        public virtual System.Nullable<int> FK_VGINSPECTIE
        {
            get
            {
                return this._FK_VGINSPECTIE;
            }
            set
            {
                this._FK_VGINSPECTIE = value;
            }
        }
        
        /// <summary>
        /// 	Workflowstatus
        /// </summary>
        /// <remarks>
        /// 	DataType: Guid
        /// 	Size: 4
        /// 	Table: C_VGINSPECTIEGEDRAGSPUNTEN
        /// </remarks>
        public virtual System.Nullable<System.Guid> FK_WORKFLOWSTATE
        {
            get
            {
                return this._FK_WORKFLOWSTATE;
            }
            set
            {
                this._FK_WORKFLOWSTATE = value;
            }
        }
        
        /// <summary>
        /// 	V&G Inspectie - gedragspunten id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        public virtual int PK_C_VGINSPECTIEGEDRAGSPUNTEN
        {
            get
            {
                return this._PK_C_VGINSPECTIEGEDRAGSPUNTEN;
            }
            set
            {
                this._PK_C_VGINSPECTIEGEDRAGSPUNTEN = value;
            }
        }
        
        /// <summary>
        /// 	Activiteit / Plaats
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        public virtual string PLAINTEXT_ACTIVITEIT
        {
            get
            {
                if (string.IsNullOrEmpty(this._PLAINTEXT_ACTIVITEIT))
                {
                    return "";
                }
                return this._PLAINTEXT_ACTIVITEIT;
            }
            set
            {
                this._PLAINTEXT_ACTIVITEIT = value;
            }
        }
        
        /// <summary>
        /// 	Beknopte weergave gesprek
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        public virtual string PLAINTEXT_BEKNOPTEWEERGAVE
        {
            get
            {
                if (string.IsNullOrEmpty(this._PLAINTEXT_BEKNOPTEWEERGAVE))
                {
                    return "";
                }
                return this._PLAINTEXT_BEKNOPTEWEERGAVE;
            }
            set
            {
                this._PLAINTEXT_BEKNOPTEWEERGAVE = value;
            }
        }
        
        /// <summary>
        /// 	Resultaat / genomen actie
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        public virtual string PLAINTEXT_RESULTAAT
        {
            get
            {
                if (string.IsNullOrEmpty(this._PLAINTEXT_RESULTAAT))
                {
                    return "";
                }
                return this._PLAINTEXT_RESULTAAT;
            }
            set
            {
                this._PLAINTEXT_RESULTAAT = value;
            }
        }
        
        /// <summary>
        /// 	Record link
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        public virtual string RECORDLINK
        {
            get
            {
                if (string.IsNullOrEmpty(this._RECORDLINK))
                {
                    return "";
                }
                return this._RECORDLINK;
            }
            set
            {
                this._RECORDLINK = value;
            }
        }
        
        /// <summary>
        /// 	Resultaat / genomen actie
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        public virtual string RESULTAAT
        {
            get
            {
                if (string.IsNullOrEmpty(this._RESULTAAT))
                {
                    return "";
                }
                return this._RESULTAAT;
            }
            set
            {
                this._RESULTAAT = value;
            }
        }
        
        /// <summary>
        /// 	Status
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        public virtual GedragspuntStatus STATE
        {
            get
            {
                return this._STATE;
            }
            set
            {
                this._STATE = value;
            }
        }
        
        /// <summary>
        /// 	Gewijzigd door
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        public virtual string USERCHANGED
        {
            get
            {
                if (string.IsNullOrEmpty(this._USERCHANGED))
                {
                    return "";
                }
                return this._USERCHANGED;
            }
            set
            {
                this._USERCHANGED = value;
            }
        }
    }
}

/*
 * Veiligheidsbewustzijn.generated.cs
 */
namespace CreateVeiligheidsmelding.Models
{
    using System;
    using System.ComponentModel;
    
    
    public enum Veiligheidsbewustzijn : int
    {
        
        /// <summary>
        ///  
        /// </summary>
        [Description(" ")]
        N_ = 1,
        
        /// <summary>
        /// Onbewust onveilig
        /// </summary>
        [Description("Onbewust onveilig")]
        Onbewust_onveilig = 2,
        
        /// <summary>
        /// Bewust onveilig
        /// </summary>
        [Description("Bewust onveilig")]
        Bewust_onveilig = 3,
        
        /// <summary>
        /// Bewust veilig
        /// </summary>
        [Description("Bewust veilig")]
        Bewust_veilig = 4,
        
        /// <summary>
        /// Onbewust veilig
        /// </summary>
        [Description("Onbewust veilig")]
        Onbewust_veilig = 5,
    }
}

/*
 * Veiligheidsmelding.generated.cs
 */
namespace CreateVeiligheidsmelding.Models
{
    //  <auto-generated />
    using System;
    
    
    ///  <summary>
    ///  	 Table: C_VEILIGHEIDSMELDINGEN
    ///  </summary>
    ///  <remarks>
    /// 	Available columns (for debugging, not necessarily available in this class):
    ///  	CLASSIFICATION, COPIEDTOINTERCOMPANYRELATION, CREATOR, DATECHANGED, DATECREATED, DATUMMELDING, DATUMOPGELOST, DESCRIPTION, EXTERNALKEY, EXTERNALREPORTER
    ///  	FK_BRONMELDINGEN, FK_MELDER, FK_MELDINGSOORTEN, FK_PUNTENFUNCTIONERINGSGESPREKKEN, FK_VGINSPECTIE, FK_WORKFLOWSTATE, INCIDENTREPORTING, INTERCOMPANYRELATION, ISOUTSTANDING, MEMO
    ///  	PK_C_VEILIGHEIDSMELDINGEN, PLAINTEXT_MEMO, PLAINTEXT_RESPONSINTERCOMPANY, RECORDLINK, RESPONSINTERCOMPANY, RESPONSLINKEDBACK, SAFETYAWARENESS, USERCHANGED
    ///  
    ///  	Filter used: FK_TABLEINFO = 'a07400ef-fc98-4d75-82df-0deacd3fdced' AND (ISSECRET = 0)
    ///  	Included columns: 
    ///  	Excluded columns: 
    ///  </remarks>
    public partial class Veiligheidsmelding
    {
        
        private Classification _CLASSIFICATION;
        
        private bool _COPIEDTOINTERCOMPANYRELATION;
        
        private string _CREATOR;
        
        private System.Nullable<System.DateTime> _DATECHANGED;
        
        private System.Nullable<System.DateTime> _DATECREATED;
        
        private System.Nullable<System.DateTime> _DATUMMELDING;
        
        private System.Nullable<System.DateTime> _DATUMOPGELOST;
        
        private string _DESCRIPTION;
        
        private string _EXTERNALKEY;
        
        private string _EXTERNALREPORTER;
        
        private System.Nullable<int> _FK_BRONMELDINGEN;
        
        private System.Nullable<int> _FK_MELDER;
        
        private System.Nullable<int> _FK_MELDINGSOORTEN;
        
        private System.Nullable<int> _FK_PUNTENFUNCTIONERINGSGESPREKKEN;
        
        private System.Nullable<int> _FK_VGINSPECTIE;
        
        private System.Nullable<System.Guid> _FK_WORKFLOWSTATE;
        
        private Incidentmelding _INCIDENTREPORTING;
        
        private IntercompanyRelation _INTERCOMPANYRELATION;
        
        private bool _ISOUTSTANDING;
        
        private string _MEMO;
        
        private int _PK_C_VEILIGHEIDSMELDINGEN;
        
        private string _PLAINTEXT_MEMO;
        
        private string _PLAINTEXT_RESPONSINTERCOMPANY;
        
        private string _RECORDLINK;
        
        private string _RESPONSINTERCOMPANY;
        
        private bool _RESPONSLINKEDBACK;
        
        private Veiligheidsbewustzijn _SAFETYAWARENESS;
        
        private string _USERCHANGED;
        
        /// <summary>
        /// 	Classificatie
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        public virtual Classification CLASSIFICATION
        {
            get
            {
                return this._CLASSIFICATION;
            }
            set
            {
                this._CLASSIFICATION = value;
            }
        }
        
        /// <summary>
        /// 	Gekopieerd naar intercompany relatie
        /// </summary>
        /// <remarks>
        /// 	DataType: Boolean
        /// 	Size: 1
        /// </remarks>
        public virtual bool COPIEDTOINTERCOMPANYRELATION
        {
            get
            {
                return this._COPIEDTOINTERCOMPANYRELATION;
            }
            set
            {
                this._COPIEDTOINTERCOMPANYRELATION = value;
            }
        }
        
        /// <summary>
        /// 	Aangemaakt door
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        public virtual string CREATOR
        {
            get
            {
                if (string.IsNullOrEmpty(this._CREATOR))
                {
                    return "";
                }
                return this._CREATOR;
            }
            set
            {
                this._CREATOR = value;
            }
        }
        
        /// <summary>
        /// 	Datum gewijzigd
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        public virtual System.Nullable<System.DateTime> DATECHANGED
        {
            get
            {
                return this._DATECHANGED;
            }
            set
            {
                this._DATECHANGED = value;
            }
        }
        
        /// <summary>
        /// 	Datum aangemaakt
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        public virtual System.Nullable<System.DateTime> DATECREATED
        {
            get
            {
                return this._DATECREATED;
            }
            set
            {
                this._DATECREATED = value;
            }
        }
        
        /// <summary>
        /// 	Datum melding
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        public virtual System.Nullable<System.DateTime> DATUMMELDING
        {
            get
            {
                return this._DATUMMELDING;
            }
            set
            {
                this._DATUMMELDING = value;
            }
        }
        
        /// <summary>
        /// 	Datum opgelost
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        public virtual System.Nullable<System.DateTime> DATUMOPGELOST
        {
            get
            {
                return this._DATUMOPGELOST;
            }
            set
            {
                this._DATUMOPGELOST = value;
            }
        }
        
        /// <summary>
        /// 	Omschrijving
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 250
        /// </remarks>
        public virtual string DESCRIPTION
        {
            get
            {
                if (string.IsNullOrEmpty(this._DESCRIPTION))
                {
                    return "";
                }
                return this._DESCRIPTION;
            }
            set
            {
                this._DESCRIPTION = value;
            }
        }
        
        /// <summary>
        /// 	Externe sleutel
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 100
        /// </remarks>
        public virtual string EXTERNALKEY
        {
            get
            {
                if (string.IsNullOrEmpty(this._EXTERNALKEY))
                {
                    return "";
                }
                return this._EXTERNALKEY;
            }
            set
            {
                this._EXTERNALKEY = value;
            }
        }
        
        /// <summary>
        /// 	Externe melder
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 250
        /// </remarks>
        public virtual string EXTERNALREPORTER
        {
            get
            {
                if (string.IsNullOrEmpty(this._EXTERNALREPORTER))
                {
                    return "";
                }
                return this._EXTERNALREPORTER;
            }
            set
            {
                this._EXTERNALREPORTER = value;
            }
        }
        
        /// <summary>
        /// 	Bron melding
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: C_VEILIGHEIDSMELDINGEN
        /// </remarks>
        public virtual System.Nullable<int> FK_BRONMELDINGEN
        {
            get
            {
                return this._FK_BRONMELDINGEN;
            }
            set
            {
                this._FK_BRONMELDINGEN = value;
            }
        }
        
        /// <summary>
        /// 	Melder
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: C_VEILIGHEIDSMELDINGEN
        /// </remarks>
        public virtual System.Nullable<int> FK_MELDER
        {
            get
            {
                return this._FK_MELDER;
            }
            set
            {
                this._FK_MELDER = value;
            }
        }
        
        /// <summary>
        /// 	Soort melding
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: C_VEILIGHEIDSMELDINGEN
        /// </remarks>
        public virtual System.Nullable<int> FK_MELDINGSOORTEN
        {
            get
            {
                return this._FK_MELDINGSOORTEN;
            }
            set
            {
                this._FK_MELDINGSOORTEN = value;
            }
        }
        
        /// <summary>
        /// 	Punten uit functioneringsgesprekken
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: C_VEILIGHEIDSMELDINGEN
        /// </remarks>
        public virtual System.Nullable<int> FK_PUNTENFUNCTIONERINGSGESPREKKEN
        {
            get
            {
                return this._FK_PUNTENFUNCTIONERINGSGESPREKKEN;
            }
            set
            {
                this._FK_PUNTENFUNCTIONERINGSGESPREKKEN = value;
            }
        }
        
        /// <summary>
        /// 	V&G Inspectie
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: C_VEILIGHEIDSMELDINGEN
        /// </remarks>
        public virtual System.Nullable<int> FK_VGINSPECTIE
        {
            get
            {
                return this._FK_VGINSPECTIE;
            }
            set
            {
                this._FK_VGINSPECTIE = value;
            }
        }
        
        /// <summary>
        /// 	Workflowstatus
        /// </summary>
        /// <remarks>
        /// 	DataType: Guid
        /// 	Size: 4
        /// 	Table: C_VEILIGHEIDSMELDINGEN
        /// </remarks>
        public virtual System.Nullable<System.Guid> FK_WORKFLOWSTATE
        {
            get
            {
                return this._FK_WORKFLOWSTATE;
            }
            set
            {
                this._FK_WORKFLOWSTATE = value;
            }
        }
        
        /// <summary>
        /// 	Melding
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        public virtual Incidentmelding INCIDENTREPORTING
        {
            get
            {
                return this._INCIDENTREPORTING;
            }
            set
            {
                this._INCIDENTREPORTING = value;
            }
        }
        
        /// <summary>
        /// 	Intercompany relatie
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        public virtual IntercompanyRelation INTERCOMPANYRELATION
        {
            get
            {
                return this._INTERCOMPANYRELATION;
            }
            set
            {
                this._INTERCOMPANYRELATION = value;
            }
        }
        
        /// <summary>
        /// 	Is openstaand
        /// </summary>
        /// <remarks>
        /// 	DataType: Boolean
        /// 	Size: 1
        /// </remarks>
        public virtual bool ISOUTSTANDING
        {
            get
            {
                return this._ISOUTSTANDING;
            }
            set
            {
                this._ISOUTSTANDING = value;
            }
        }
        
        /// <summary>
        /// 	Memo
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        public virtual string MEMO
        {
            get
            {
                if (string.IsNullOrEmpty(this._MEMO))
                {
                    return "";
                }
                return this._MEMO;
            }
            set
            {
                this._MEMO = value;
            }
        }
        
        /// <summary>
        /// 	Veiligheidsmeldingen id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        public virtual int PK_C_VEILIGHEIDSMELDINGEN
        {
            get
            {
                return this._PK_C_VEILIGHEIDSMELDINGEN;
            }
            set
            {
                this._PK_C_VEILIGHEIDSMELDINGEN = value;
            }
        }
        
        /// <summary>
        /// 	Memo
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        public virtual string PLAINTEXT_MEMO
        {
            get
            {
                if (string.IsNullOrEmpty(this._PLAINTEXT_MEMO))
                {
                    return "";
                }
                return this._PLAINTEXT_MEMO;
            }
            set
            {
                this._PLAINTEXT_MEMO = value;
            }
        }
        
        /// <summary>
        /// 	Respons intercompany
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        public virtual string PLAINTEXT_RESPONSINTERCOMPANY
        {
            get
            {
                if (string.IsNullOrEmpty(this._PLAINTEXT_RESPONSINTERCOMPANY))
                {
                    return "";
                }
                return this._PLAINTEXT_RESPONSINTERCOMPANY;
            }
            set
            {
                this._PLAINTEXT_RESPONSINTERCOMPANY = value;
            }
        }
        
        /// <summary>
        /// 	Record link
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        public virtual string RECORDLINK
        {
            get
            {
                if (string.IsNullOrEmpty(this._RECORDLINK))
                {
                    return "";
                }
                return this._RECORDLINK;
            }
            set
            {
                this._RECORDLINK = value;
            }
        }
        
        /// <summary>
        /// 	Respons intercompany
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        public virtual string RESPONSINTERCOMPANY
        {
            get
            {
                if (string.IsNullOrEmpty(this._RESPONSINTERCOMPANY))
                {
                    return "";
                }
                return this._RESPONSINTERCOMPANY;
            }
            set
            {
                this._RESPONSINTERCOMPANY = value;
            }
        }
        
        /// <summary>
        /// 	Respons terug gekoppeld
        /// </summary>
        /// <remarks>
        /// 	DataType: Boolean
        /// 	Size: 1
        /// </remarks>
        public virtual bool RESPONSLINKEDBACK
        {
            get
            {
                return this._RESPONSLINKEDBACK;
            }
            set
            {
                this._RESPONSLINKEDBACK = value;
            }
        }
        
        /// <summary>
        /// 	Veiligheidsbewustzijn
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        public virtual Veiligheidsbewustzijn SAFETYAWARENESS
        {
            get
            {
                return this._SAFETYAWARENESS;
            }
            set
            {
                this._SAFETYAWARENESS = value;
            }
        }
        
        /// <summary>
        /// 	Gewijzigd door
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        public virtual string USERCHANGED
        {
            get
            {
                if (string.IsNullOrEmpty(this._USERCHANGED))
                {
                    return "";
                }
                return this._USERCHANGED;
            }
            set
            {
                this._USERCHANGED = value;
            }
        }
    }
}

