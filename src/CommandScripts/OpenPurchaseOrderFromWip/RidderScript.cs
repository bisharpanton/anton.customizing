﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Drawing;

public class OpenPurchaseOrderFromWip_RidderScript : CommandScript
{
    public void Execute()
    {
        // DB
        // 19-9-2019
        // Open inkooporder vanuit OHW

        if (FormDataAwareFunctions == null || FormDataAwareFunctions.TableName != "R_ORDERWIPALLDETAIL")
        {
            MessageBox.Show("Dit script mag alleen vanaf de gecombineerde OHW regels aangeroepen worden.");
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        var purchaseOrders = new List<int>();

        purchaseOrders.AddRange(GetPurchaseOrders("ITEM", FormDataAwareFunctions.GetSelectedRecords()));
        purchaseOrders.AddRange(GetPurchaseOrders("MISC", FormDataAwareFunctions.GetSelectedRecords()));
        purchaseOrders.AddRange(GetPurchaseOrders("OUTSOURCED", FormDataAwareFunctions.GetSelectedRecords()));

        if (!purchaseOrders.Any())
        {
            MessageBox.Show("Geen inkoopfacturen gevonden.", "Ridder iQ", MessageBoxButtons.OK,
                MessageBoxIcon.Information);
            return;
        }
        else if (purchaseOrders.Count == 1)
        {
            var editPar =
                new Ridder.Client.SDK.SDKParameters.EditParameters
                {
                    TableName = "R_PURCHASEORDER", Id = purchaseOrders.First()
                };

            OpenEdit(editPar, false);
        }
        else
        {
            var browsePar = new Ridder.Client.SDK.SDKParameters.BrowseParameters
            {
                TableName = "R_PURCHASEORDER",
                Filter = $"PK_R_PURCHASEORDER in ({string.Join(",", purchaseOrders)})",
                Context = "OpenPurchaseOrdersFromWip"
            };

            OpenBrowse(browsePar, false);
        }
    }

    private List<int> GetPurchaseOrders(string source, IRecord[] selectedRecords)
    {
        var wipDetails = selectedRecords
            .Where(x => (x.GetCurrentRecordValue($"FK_ORDERWIPDETAIL{source}") as int?).HasValue)
            .Select(x => (int)x.GetCurrentRecordValue($"FK_ORDERWIPDETAIL{source}")).ToList();

        if(!wipDetails.Any())
        {
            return new List<int>();
        }

        var receiptDetailIds = GetRecordset($"R_ORDERWIPDETAIL{source}", $"FK_GOODSRECEIPTDETAIL{source}",
                $"PK_R_ORDERWIPDETAIL{source} IN ({string.Join(",", wipDetails)}) AND FK_GOODSRECEIPTDETAIL{source} IS NOT NULL",
                "").DataTable.AsEnumerable()
            .Select(x => x.Field<int>($"FK_GOODSRECEIPTDETAIL{source}")).ToList();

        if (!receiptDetailIds.Any())
        {
            return new List<int>();
        }

        var purchaseOrderDetailIds = GetRecordset($"R_GOODSRECEIPTDETAIL{source}", $"FK_PURCHASEORDERDETAIL{source}",
                $"PK_R_GOODSRECEIPTDETAIL{source} IN ({string.Join(",", receiptDetailIds)}) AND FK_PURCHASEORDERDETAIL{source} IS NOT NULL",
                "").DataTable.AsEnumerable()
            .Select(x => x.Field<int>($"FK_PURCHASEORDERDETAIL{source}")).ToList();

        if (!purchaseOrderDetailIds.Any())
        {
            return new List<int>();
        }

        return GetRecordset($"R_PURCHASEORDERDETAIL{source}", "FK_PURCHASEORDER",
                $"PK_R_PURCHASEORDERDETAIL{source} IN ({string.Join(",", purchaseOrderDetailIds)})",
                "").DataTable.AsEnumerable()
            .Select(x => x.Field<int>("FK_PURCHASEORDER")).Distinct().ToList();
    }
}
