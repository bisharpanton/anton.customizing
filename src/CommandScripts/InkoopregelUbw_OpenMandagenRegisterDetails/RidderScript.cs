﻿using ADODB;
using Ridder.Client.SDK.SDKParameters;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.ItemManagement;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class InkoopregelUbw_OpenMandagenRegisterDetails_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //2-3-2022
        //Open venster om mandagenregister bij inkoopregel ubw in te geven

        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("R_PURCHASEORDERDETAILOUTSOURCED"))
        {
            MessageBox.Show("Dit script mag alleen vanaf de inkoopregel ubw aangeroepen worden.");
            return;
        }

        if(!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        if(FormDataAwareFunctions.GetSelectedRecords().Count() > 1)
        {
            MessageBox.Show("Dit script mag alleen slechts voor één inkoopregel tegelijk aangeroepen worden.");
            return;
        }

        var purchaseOrderDetailOutsourcedId = (int)FormDataAwareFunctions.GetSelectedRecords().First().GetPrimaryKeyValue();

        var purchaseOrderDetailOutsourced = GetRecordset("R_PURCHASEORDERDETAILOUTSOURCED", "FK_PURCHASEORDER, FK_OUTSOURCEDACTIVITY",
            $"PK_R_PURCHASEORDERDETAILOUTSOURCED = {purchaseOrderDetailOutsourcedId}", "").DataTable.AsEnumerable().First();

        var purchaseOrder = GetRecordset("R_PURCHASEORDER", "FK_SUPPLIER, WKA", $"PK_R_PURCHASEORDER = {purchaseOrderDetailOutsourced.Field<int>("FK_PURCHASEORDER")}", "")
            .DataTable.AsEnumerable().First();

        if (!purchaseOrder.Field<bool>("WKA"))
        {
            MessageBox.Show("Bij deze inkooporder is geen WKA van toepassing.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        var unit = GetUnit(purchaseOrderDetailOutsourced.Field<int>("FK_OUTSOURCEDACTIVITY"));

        if(unit.Field<UnitQuantity>("QUANTITY") != UnitQuantity.Time)
        {
            MessageBox.Show("Eenheid van uitbesteed werk is geen tijdseenheid. Controle of mandagenregister volledig is ingevuld niet mogelijk. Wijzig de eenheid van het uitbesteed werk of kies een andere uitbesteed werk code.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        var createdFormId = CreatedFormId(purchaseOrderDetailOutsourcedId, purchaseOrder.Field<int>("FK_SUPPLIER"));

        var editParameter = new EditParameters()
        {
            TableName = "U_FORMSUPPLIERMANDAYREGISTERDETAILS",
            Id = createdFormId,
        };

        OpenEdit(editParameter, true);

        //Check of Edit correct is afgesloten
        var rsClosedForm = GetRecordset("U_FORMSUPPLIERMANDAYREGISTERDETAILS", "", $"PK_U_FORMSUPPLIERMANDAYREGISTERDETAILS = {createdFormId}", "");
           
        

        if(!rsClosedForm.DataTable.AsEnumerable().First().Field<bool>("PROCESS"))
        {
            //Delete Form
            rsClosedForm.MoveFirst();
            rsClosedForm.Delete();

            return; //Formulier niet correct afgesloten
        }

        var formDetails = GetRecordset("U_FORMDETAILSUPPLIERMANDAYREGISTERDETAILS", "", $"FK_FORMSUPPLIERMANDAYREGISTERDETAILS = {createdFormId}", "")
            .DataTable.AsEnumerable().ToList();

        ProcessFormDetails(purchaseOrderDetailOutsourcedId, formDetails, rsClosedForm.DataTable.AsEnumerable().First().Field<bool>("UPDATEQUANTITY"));

        //Delete Form
        rsClosedForm.MoveFirst();
        rsClosedForm.Delete();

        FormDataAwareFunctions.Refres();
    }

    private void ProcessFormDetails(int purchaseOrderDetailOutsourcedId, List<DataRow> formDetails, bool updateQuantity)
    {
        var hoursPerDayInfo = ConvertFormDetailsToHoursPerDayInfo(formDetails);

        if(updateQuantity)
        {
            var totalTime = hoursPerDayInfo.Sum(x => x.TimeWorked);
            var totalHours = TimeSpan.FromTicks(totalTime).TotalHours;

            var rsPurchaseOrderDetailOutsourced = GetRecordset("R_PURCHASEORDERDETAILOUTSOURCED", "",
                $"PK_R_PURCHASEORDERDETAILOUTSOURCED = {purchaseOrderDetailOutsourcedId}", "");
            var purchaseOrderDetailOutsourced = rsPurchaseOrderDetailOutsourced.DataTable.AsEnumerable().First();

            var unitId = GetRecordset("R_OUTSOURCEDACTIVITY", "FK_UNIT",
                $"PK_R_OUTSOURCEDACTIVITY = {purchaseOrderDetailOutsourced.Field<int>("FK_OUTSOURCEDACTIVITY")}", "").DataTable.AsEnumerable().First().Field<int>("FK_UNIT");

            var unit = GetRecordset("R_UNIT", "FACTOR", $"PK_R_UNIT = {unitId}", "").DataTable.AsEnumerable().First();

            rsPurchaseOrderDetailOutsourced.UseDataChanges = true;
            rsPurchaseOrderDetailOutsourced.MoveFirst();
            rsPurchaseOrderDetailOutsourced.SetFieldValue("QUANTITY", totalHours * unit.Field<double>("FACTOR"));
            rsPurchaseOrderDetailOutsourced.Update();
        }


        var rowsToDelete = new List<int>();

        var rsMandayRegisterDetails = GetRecordset("U_SUPPLIERMANDAYREGISTERDETAILS", "", 
            $"FK_PURCHASEORDERDETAILOUTSOURCED = {purchaseOrderDetailOutsourcedId}", "");
        rsMandayRegisterDetails.UseDataChanges = false;
        rsMandayRegisterDetails.UpdateWhenMoveRecord = false;

        rsMandayRegisterDetails.MoveFirst();
        while(!rsMandayRegisterDetails.EOF)
        {
            var hourPerDay = hoursPerDayInfo.FirstOrDefault(x => x.Date.Date == ((DateTime)rsMandayRegisterDetails.GetField("DATE").Value).Date && x.ExternalEmployee.Equals((string)rsMandayRegisterDetails.GetField("EXTERNALEMPLOYEE").Value));

            if(hourPerDay == null)
            {
                //Regel is verwijderd in de popup
                rowsToDelete.Add((int)rsMandayRegisterDetails.GetField("PK_U_SUPPLIERMANDAYREGISTERDETAILS").Value);

                rsMandayRegisterDetails.MoveNext();
                continue;
            }

            rsMandayRegisterDetails.SetFieldValue("TIMEWORKED", hourPerDay.TimeWorked);
            hourPerDay.ProcessedToDb = true;

            rsMandayRegisterDetails.MoveNext();
        }

        foreach (var hourPerDay in hoursPerDayInfo.Where(x => !x.ProcessedToDb))
        {
            rsMandayRegisterDetails.AddNew();

            rsMandayRegisterDetails.SetFieldValue("FK_PURCHASEORDERDETAILOUTSOURCED", purchaseOrderDetailOutsourcedId);

            rsMandayRegisterDetails.SetFieldValue("DATE", hourPerDay.Date);
            rsMandayRegisterDetails.SetFieldValue("EXTERNALEMPLOYEE", hourPerDay.ExternalEmployee);
            rsMandayRegisterDetails.SetFieldValue("BSN", hourPerDay.Bsn);
            rsMandayRegisterDetails.SetFieldValue("REGNO", hourPerDay.RegNo);
            rsMandayRegisterDetails.SetFieldValue("COCNUMBER", hourPerDay.CocNumber);

            rsMandayRegisterDetails.SetFieldValue("TIMEWORKED", hourPerDay.TimeWorked);
        }

        rsMandayRegisterDetails.MoveFirst();
        rsMandayRegisterDetails.Update();

        if(rowsToDelete.Any())
        {
            var rsMandayRegisterDetailsToDelete = GetRecordset("U_SUPPLIERMANDAYREGISTERDETAILS", "",
                $"PK_U_SUPPLIERMANDAYREGISTERDETAILS IN ({string.Join(",", rowsToDelete)})", "");
            rsMandayRegisterDetailsToDelete.UpdateWhenMoveRecord = false;
            
            rsMandayRegisterDetailsToDelete.MoveFirst();
            while (!rsMandayRegisterDetailsToDelete.EOF)
            {
                rsMandayRegisterDetailsToDelete.Delete();
                rsMandayRegisterDetailsToDelete.MoveNext();
            }
            rsMandayRegisterDetailsToDelete.MoveFirst();
            rsMandayRegisterDetailsToDelete.Update();
        }
    }

    private List<HoursPerDay> ConvertFormDetailsToHoursPerDayInfo(List<DataRow> formDetails)
    {
        var result = new List<HoursPerDay>();

        var days = new string[7] { "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY" };

        foreach (var formDetail in formDetails)
        {
            var year = formDetail.Field<int>("YEAR");
            var weekNumber = formDetail.Field<int>("WEEK");
            var firstDateOfWeek = GetFirstDateOfWeek(year, weekNumber);

            for (int i = 0; i < days.Length; i++)
            {
                var time = formDetail.Field<long>(days[i]);

                if (time == 0)
                {
                    continue;
                }

                result.Add(new HoursPerDay()
                {
                    Date = firstDateOfWeek.AddDays(i),
                    ExternalEmployee = formDetail.Field<string>("EXTERNALEMPLOYEE"),
                    Bsn = formDetail.Field<string>("BSN"),
                    RegNo = formDetail.Field<string>("REGNO"),
                    CocNumber = formDetail.Field<string>("COCNUMBER"),
                    TimeWorked = time,
                    ProcessedToDb = false,
                });
            }
        }

        return result;
    }

    private int CreatedFormId(int purchaseOrderDetailOutsourcedId, int supplierId)
    {
        var exisitingMandayRegisterDetails = GetRecordset("U_SUPPLIERMANDAYREGISTERDETAILS", "", $"FK_PURCHASEORDERDETAILOUTSOURCED = {purchaseOrderDetailOutsourcedId}", "")
            .DataTable.AsEnumerable().ToList();

        var rsForm = GetRecordset("U_FORMSUPPLIERMANDAYREGISTERDETAILS", "", "PK_U_FORMSUPPLIERMANDAYREGISTERDETAILS = -1", "");
        rsForm.AddNew();
        rsForm.SetFieldValue("FK_PURCHASEORDERDETAILOUTSOURCED", purchaseOrderDetailOutsourcedId);
        rsForm.SetFieldValue("FK_SUPPLIER", supplierId);
        rsForm.Update();

        var createdFormId = (int)rsForm.GetField("PK_U_FORMSUPPLIERMANDAYREGISTERDETAILS").Value;

        if(exisitingMandayRegisterDetails.Any())
        {
            var rsFormDetails = GetRecordset("U_FORMDETAILSUPPLIERMANDAYREGISTERDETAILS", "", "PK_U_FORMDETAILSUPPLIERMANDAYREGISTERDETAILS = -1", "");

            var exisitingMandayRegisterDetailsGroupedByEmployeeByWeek = exisitingMandayRegisterDetails.GroupBy(x => $"{x.Field<string>("EXTERNALEMPLOYEE")}_{GetWeekOfYear(x.Field<DateTime>("DATE"))}");

            foreach (var group in exisitingMandayRegisterDetailsGroupedByEmployeeByWeek)
            {
                rsFormDetails.AddNew();

                rsFormDetails.SetFieldValue("FK_FORMSUPPLIERMANDAYREGISTERDETAILS", createdFormId);
                
                rsFormDetails.SetFieldValue("EXTERNALEMPLOYEE", group.First().Field<string>("EXTERNALEMPLOYEE"));
                rsFormDetails.SetFieldValue("BSN", group.First().Field<string>("BSN"));
                rsFormDetails.SetFieldValue("REGNO", group.First().Field<string>("REGNO"));
                rsFormDetails.SetFieldValue("COCNUMBER", group.First().Field<string>("COCNUMBER"));

                rsFormDetails.SetFieldValue("YEAR", group.First().Field<DateTime>("DATE").Year);
                rsFormDetails.SetFieldValue("WEEK", GetWeekOfYear(group.First().Field<DateTime>("DATE")));

                rsFormDetails.SetFieldValue("MONDAY", group.Where(x => x.Field<DateTime>("DATE").DayOfWeek == DayOfWeek.Monday).Sum(x => x.Field<long>("TIMEWORKED")));
                rsFormDetails.SetFieldValue("TUESDAY", group.Where(x => x.Field<DateTime>("DATE").DayOfWeek == DayOfWeek.Tuesday).Sum(x => x.Field<long>("TIMEWORKED")));
                rsFormDetails.SetFieldValue("WEDNESDAY", group.Where(x => x.Field<DateTime>("DATE").DayOfWeek == DayOfWeek.Wednesday).Sum(x => x.Field<long>("TIMEWORKED")));
                rsFormDetails.SetFieldValue("THURSDAY", group.Where(x => x.Field<DateTime>("DATE").DayOfWeek == DayOfWeek.Thursday).Sum(x => x.Field<long>("TIMEWORKED")));
                rsFormDetails.SetFieldValue("FRIDAY", group.Where(x => x.Field<DateTime>("DATE").DayOfWeek == DayOfWeek.Friday).Sum(x => x.Field<long>("TIMEWORKED")));
                rsFormDetails.SetFieldValue("SATURDAY", group.Where(x => x.Field<DateTime>("DATE").DayOfWeek == DayOfWeek.Saturday).Sum(x => x.Field<long>("TIMEWORKED")));
                rsFormDetails.SetFieldValue("SUNDAY", group.Where(x => x.Field<DateTime>("DATE").DayOfWeek == DayOfWeek.Sunday).Sum(x => x.Field<long>("TIMEWORKED")));
            }
            /*
            foreach (var mandayRegisterDetail in exisitingMandayRegisterDetails)
            {
                rsFormDetails.AddNew();

                rsFormDetails.SetFieldValue("FK_FORMSUPPLIERMANDAYREGISTERDETAILS", createdFormId);

                rsFormDetails.SetFieldValue("FK_EXTERNALEMPLOYEE", mandayRegisterDetail.Field<int>("FK_EXTERNALEMPLOYEE"));
                rsFormDetails.SetFieldValue("DATE", mandayRegisterDetail.Field<DateTime>("DATE"));
                rsFormDetails.SetFieldValue("TIMEWORKED", mandayRegisterDetail.Field<long>("TIMEWORKED"));

                rsFormDetails.SetFieldValue("SUPPLIERMANDAYREGISTERDETAILSID", mandayRegisterDetail.Field<int>("PK_U_SUPPLIERMANDAYREGISTERDETAILS"));
            }*/

            rsFormDetails.Update();
        }

        return createdFormId;
    }

    private DataRow GetUnit(int outsourcedActivityId)
    {
        var unitId = GetRecordset("R_OUTSOURCEDACTIVITY", "FK_UNIT", 
            $"PK_R_OUTSOURCEDACTIVITY = {outsourcedActivityId}", "").DataTable.AsEnumerable().First().Field<int>("FK_UNIT");

        return GetRecordset("R_UNIT", "QUANTITY", $"PK_R_UNIT = {unitId}", "").DataTable.AsEnumerable().First();
    }

    public static int GetWeekOfYear(DateTime date)
    {
        return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(date, CalendarWeekRule.FirstFullWeek, DayOfWeek.Monday);
    }

    class HoursPerDay
    {
        public string ExternalEmployee { get; set; }
        public string Bsn { get; set; }
        public string RegNo { get; set; }
        public string CocNumber { get; set; }
        public DateTime Date { get; set; }
        public long TimeWorked { get; set; }
        public bool ProcessedToDb { get; set; }
    }

    public static DateTime GetFirstDateOfWeek(int year, int weekOfYear)
    {
        DateTime jan1 = new DateTime(year, 1, 1);

        var firstMondayWeek1 = jan1;

        while(firstMondayWeek1.DayOfWeek != DayOfWeek.Monday)
        {
            firstMondayWeek1 = firstMondayWeek1.AddDays(1);
        }

        return firstMondayWeek1.AddDays(7 * (weekOfYear - 1));
    }
}
