﻿using ADODB;
using Ridder.Client.SDK.SDKParameters;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class CreatePurchaseDetailFromFramework_RidderScript : CommandScript
{
    public void Execute()
    {
        //SP
        //10-8-2023
        //Importeren van inkoopregel uit raamcontract

        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("R_PURCHASEORDERDETAILITEM"))
        {
            MessageBox.Show("Dit script mag alleen vanaf de inkooporderregels artikel aangeroepen worden.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        if (FormDataAwareFunctions.FormParent == null || !FormDataAwareFunctions.FormParent.TableName.Equals("R_PURCHASEORDER"))
        {
            MessageBox.Show("Dit script mag alleen vanaf de inkooporderregels artikel binnen een inkooporder aangeroepen worden.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        var purchaseOrderId = (int)FormDataAwareFunctions.FormParent.CurrentRecord.GetPrimaryKeyValue();

        var supplierId = (int)FormDataAwareFunctions.FormParent.CurrentRecord.GetCurrentRecordValue("FK_SUPPLIER");

        var checkPurchaseFrameworks = GetRecordset("U_PURCHASEFRAMEWORK", "PK_U_PURCHASEFRAMEWORK",
            $"FK_SUPPLIER = {supplierId}", "").RecordCount > 0;

        if (!checkPurchaseFrameworks)
        {
            MessageBox.Show("Geen raamcontracten gevonden bij deze leverancier.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        var createdFormId = CreatedForm();

        var editParameter = new EditParameters()
        {
            TableName = "U_FORMIMPORTPURCHASEDETAILFROMFRAMEWORK",
            Id = createdFormId,
        };

        OpenEdit(editParameter, true);

        //Check of Edit correct is afgesloten
        var rsClosedForm = GetRecordset("U_FORMIMPORTPURCHASEDETAILFROMFRAMEWORK", "", $"PK_U_FORMIMPORTPURCHASEDETAILFROMFRAMEWORK = {createdFormId}", "");

        if (!rsClosedForm.DataTable.AsEnumerable().First().Field<bool>("PROCESSED"))
        {
            //Delete Form
            rsClosedForm.MoveFirst();
            rsClosedForm.Delete();

            return; //Formulier niet correct afgesloten
        }

        Cursor.Current = Cursors.WaitCursor;
        try
        {
            ProcessForm(purchaseOrderId, rsClosedForm.DataTable.AsEnumerable().First());
        }
        finally
        {
            Cursor.Current = Cursors.Default;
        }

        //Delete Form
        rsClosedForm.MoveFirst();
        rsClosedForm.Delete();

        FormDataAwareFunctions.Refres();

    }

    private int CreatedForm()
    {
        var rsForm = GetRecordset("U_FORMIMPORTPURCHASEDETAILFROMFRAMEWORK", "", "PK_U_FORMIMPORTPURCHASEDETAILFROMFRAMEWORK = -1", "");
        rsForm.AddNew();
        rsForm.Update();

        var createdFormId = (int)rsForm.GetField("PK_U_FORMIMPORTPURCHASEDETAILFROMFRAMEWORK").Value;

        return createdFormId;
    }

    private void ProcessForm(int purchaseOrderId, DataRow form)
    {
        var purchaseFrameworkDetailId = form.Field<int?>("FK_PURCHASEFRAMEWORKDETAIL") ?? 0;

        var rsPurchaseFrameworkDetail = GetRecordset("U_PURCHASEFRAMEWORKDETAIL", "GROSSCONTRACTPRICE, FK_ITEM, QUANTITY",
            $"PK_U_PURCHASEFRAMEWORKDETAIL = {purchaseFrameworkDetailId}", "");
            
        var purchaseFrameworkDetail = rsPurchaseFrameworkDetail.DataTable.AsEnumerable().First();

        var itemId = purchaseFrameworkDetail.Field<int>("FK_ITEM");
        var grossContractPrice = purchaseFrameworkDetail.Field<double>("GROSSCONTRACTPRICE");
        var contractQuantity = purchaseFrameworkDetail.Field<double>("QUANTITY");

        var chosenQuantity = form.Field<double>("QUANTITY");

        var availableQuantity = ValidateQuantity(purchaseFrameworkDetailId, contractQuantity, chosenQuantity);

        if (availableQuantity < 0.0)
        {
            var chosenProcessAction = form.Field<ProcessAction>("PROCESSACTION");

            if(chosenProcessAction == ProcessAction.Afronden)
            {
                var quantityToCreate = availableQuantity + chosenQuantity;

                CreatePurchaseOrderDetailItem(purchaseOrderId, itemId, form, quantityToCreate, grossContractPrice);
            }

            if(chosenProcessAction == ProcessAction.ContractOphogen)
            {
                CreatePurchaseOrderDetailItem(purchaseOrderId, itemId, form, chosenQuantity, grossContractPrice);

                UpgradeContract(rsPurchaseFrameworkDetail, contractQuantity + (availableQuantity * -1));
            }
        }
        else
        {
            CreatePurchaseOrderDetailItem(purchaseOrderId, itemId, form, chosenQuantity, grossContractPrice);
        }
    }

    private void UpgradeContract(ScriptRecordset rsPurchaseFrameworkDetail, double upgradedQuantity)
    {
        rsPurchaseFrameworkDetail.MoveFirst();
        rsPurchaseFrameworkDetail.UseDataChanges = false;
        rsPurchaseFrameworkDetail.UpdateWhenMoveRecord = false;

        rsPurchaseFrameworkDetail.SetFieldValue("QUANTITY", upgradedQuantity);

        var updateResult = rsPurchaseFrameworkDetail.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Ophogen contract is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

    }

    private void CreatePurchaseOrderDetailItem(int purchaseOrderId, int itemId, DataRow form, double quantityToCreate, double grossContractPrice)
    {
        var rsPurchaseOrderDetailItem = GetRecordset("R_PURCHASEORDERDETAILITEM", "", $"PK_R_PURCHASEORDERDETAILITEM = -1 ", "");
        rsPurchaseOrderDetailItem.UseDataChanges = true;
        rsPurchaseOrderDetailItem.UpdateWhenMoveRecord = false;

        rsPurchaseOrderDetailItem.AddNew();

        rsPurchaseOrderDetailItem.SetFieldValue("FK_PURCHASEORDER", purchaseOrderId);
        rsPurchaseOrderDetailItem.SetFieldValue("FK_ITEM", itemId);
        rsPurchaseOrderDetailItem.SetFieldValue("FK_PURCHASEFRAMEWORKDETAIL", form.Field<int>("FK_PURCHASEFRAMEWORKDETAIL"));
        rsPurchaseOrderDetailItem.SetFieldValue("QUANTITY", quantityToCreate);
        rsPurchaseOrderDetailItem.SetFieldValue("GROSSPURCHASEPRICE", grossContractPrice);
        rsPurchaseOrderDetailItem.SetFieldValue("DIRECTTOORDER", false);

        var updateResult = rsPurchaseOrderDetailItem.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Aanmaken inkooporderregel vanuit raamcontract mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", 
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }
    }

    private double ValidateQuantity(int purchaseFrameworkDetailId, double contractQuantity, double chosenQuantity)
    {
        var usedQuantity = GetRecordset("R_PURCHASEORDERDETAILITEM", "QUANTITY",
            $"FK_PURCHASEFRAMEWORKDETAIL = {purchaseFrameworkDetailId}", "").DataTable.AsEnumerable().Sum(x => x.Field<double>("QUANTITY"));

        var availableQuantity = contractQuantity - usedQuantity - chosenQuantity;

        return availableQuantity;

    }

    private enum ProcessAction : int
    {
        GeenKeuzeGemaakt = 1,
        ContractOphogen = 2,
        Afronden = 3
    }
}
