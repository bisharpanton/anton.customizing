﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Drawing;
using Ridder.Client.SDK.SDKParameters;


public class Uren___RegistreerVerbruik_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //03-09-2019
        //Registreer verbruik vanuit urenboeking

        if (FormDataAwareFunctions == null || FormDataAwareFunctions.TableName != "R_PROJECTTIME")
        {
            MessageBox.Show("Dit script mag alleen vanaf de uren-tabel aangeroepen worden.");
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        if (FormDataAwareFunctions.GetSelectedRecords().Length > 1)
        {
            MessageBox.Show("Dit script mag slechts voor één urenboeking tegelijk aangeroepen worden.");
            return;
        }

        var projectTimeId = FormDataAwareFunctions.CurrentRecord.GetPrimaryKeyValue() as int? ?? 0;

        if (projectTimeId == 0)
        {
            MessageBox.Show("Uren id kan niet bepaald worden. Sla de urenboeking eerst op.", "Ridder iQ",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        var projectTime = GetRecordset("R_PROJECTTIME", "PK_R_PROJECTTIME, FK_ORDER, FK_WORKACTIVITY",
            $"PK_R_PROJECTTIME = {projectTimeId}", "").DataTable.AsEnumerable().First();

        if (!projectTime.Field<int?>("FK_ORDER").HasValue)
        {
            MessageBox.Show("Huidige urenboeking is niet gekoppeld aan een order. Verbruik registreren niet mogelijk.",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        ResetFavoritesPerWorkactivityThisUser(projectTimeId, projectTime.Field<int>("FK_WORKACTIVITY"));

        var editPar = new EditParameters()
        {
            TableName = "R_PROJECTTIME", Id = projectTimeId, Context = "RegistreerVerbruik",
        };

        OpenEdit(editPar, true);


    }

    private void ResetFavoritesPerWorkactivityThisUser(int projectTimeId, int workactivity)
    {
        var rsFavoritesThisUser = GetRecordset("C_TEMPUSEDITEMSPERUSER",
            "FK_USER, FK_PROJECTTIME, FK_ITEM", $"FK_USER = {CurrentUserId}", "");

        rsFavoritesThisUser.UpdateWhenMoveRecord = false;
        rsFavoritesThisUser.UseDataChanges = false;

        if (rsFavoritesThisUser.RecordCount > 0)
        {
            rsFavoritesThisUser.MoveFirst();
            while (!rsFavoritesThisUser.EOF)
            {
                rsFavoritesThisUser.Delete();
                rsFavoritesThisUser.MoveNext();
            }
            rsFavoritesThisUser.MoveFirst();
            rsFavoritesThisUser.Update();
        }

        var favoritesThisWorkactivity = GetRecordset("C_ITEMSPERWORKACTIVITY", "FK_ITEM",
                $"FK_WORKACTIVITY = {workactivity}", "").DataTable.AsEnumerable()
            .Select(x => x.Field<int>("FK_ITEM")).ToList();

        if (!favoritesThisWorkactivity.Any())
        {
            return;
        }

        foreach (var item in favoritesThisWorkactivity)
        {
            rsFavoritesThisUser.AddNew();
            rsFavoritesThisUser.Fields["FK_USER"].Value = CurrentUserId;
            rsFavoritesThisUser.Fields["FK_PROJECTTIME"].Value = projectTimeId;
            rsFavoritesThisUser.Fields["FK_ITEM"].Value = item;
        }

        rsFavoritesThisUser.Update();
    }
}
