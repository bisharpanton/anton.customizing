﻿using ADODB;
using Afas_ExporteerOverurenNaarAfas.AfasModels;
using Afas_ExporteerOverurenNaarAfas.Models;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Recordset.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class Afas_ExporteerOverurenNaarAfas_RidderScript : CommandScript
{
    /*Live omgeving AFAS*/
    const string AfasStartUrl = "https://90985.rest.afas.online";
    const string AfasToken = "AfasToken PHRva2VuPjx2ZXJzaW9uPjE8L3ZlcnNpb24+PGRhdGE+QUIxNUI5MzA3NDEyNEJBQjkyRjMyQzk2MDRDOERFNzZDODY0Qzk1NzRFOUI5NzFGRDI0MTVEODkzQzdDNUIxRjwvZGF0YT48L3Rva2VuPgo=";

    /*TEST Omgeving AFAS
    const string AfasStartUrl = "https://90985.resttest.afas.online";
    const string AfasToken = "AfasToken PHRva2VuPjx2ZXJzaW9uPjE8L3ZlcnNpb24+PGRhdGE+QjQxNjExMjExOEVBNDExOEFCNjlCQzUxQjg4OTZDODhEQjhEQzgyRDQxNTMwOUFFN0UxMzkyQjlDN0YzQUQ4NjwvZGF0YT48L3Rva2VuPgo=";
    */

    public void Execute()
    {
        //DB
        //22-12-2021
        //Exporteer verlofuur naar AFAS

        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("C_TOEXPORTPROJECTTIMES"))
        {
            MessageBox.Show("Dit script mag alleen vanaf de tabel 'Te exporteren overuren' aangeroepen worden.", "Ridder iQ",
                MessageBoxButtons.OK, MessageBoxIcon.Information);

            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        var selectedRecordIds = FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue()).ToList();

        var toExportProjectTimesToAfas = GetRecordset("C_TOEXPORTPROJECTTIMES", "",
            $"PK_C_TOEXPORTPROJECTTIMES IN ({string.Join(",", selectedRecordIds)}) AND EXPORTED = 0", "")
            .As<ToExportProjectTime>().ToList();

        if (!toExportProjectTimesToAfas.Any())
        {
            MessageBox.Show("Geen regels gevonden die nog geëxporteerd moeten worden.", "Ridder iQ",
                MessageBoxButtons.OK, MessageBoxIcon.Information);

            return;
        }


        ExportGroupedProjectTimesToAfas(toExportProjectTimesToAfas);

        UpdateRecordsToExportProjectTimes(toExportProjectTimesToAfas);

        FinishHoursFromSelectedEmployeePeriod(toExportProjectTimesToAfas);
    }

    private void FinishHoursFromSelectedEmployeePeriod(List<ToExportProjectTime> toExportProjectTimesToAfas)
    {
        if(!toExportProjectTimesToAfas.Any())
        {
            return;
        }

        var rsFinishedProjectTime = GetRecordset("R_FINISHEDPROJECTTIME", "", "PK_R_FINISHEDPROJECTTIME IS NULL", "");
        rsFinishedProjectTime.UpdateWhenMoveRecord = false;
        rsFinishedProjectTime.UseDataChanges = true;

        var employeeIds = toExportProjectTimesToAfas.Select(x => x.FK_EMPLOYEE);
        var minDate = toExportProjectTimesToAfas.Min(x => x.STARTPERIOD);
        var maxDate = toExportProjectTimesToAfas.Max(x => x.ENDPERIOD);

        foreach (var employeeId in employeeIds)
        {
            var checkForFinishedProjectTimes = GetRecordset("R_FINISHEDPROJECTTIME", "",
                $"FK_EMPLOYEE = '{employeeId}' AND STARTDATE >= '{minDate:yyyyMMdd}' AND ENDDATE <= '{maxDate:yyyyMMdd}'", "")
                .DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_FINISHEDPROJECTTIME"));

            if(checkForFinishedProjectTimes.Any())
            {
                continue;
            }

            rsFinishedProjectTime.AddNew();
            rsFinishedProjectTime.SetFieldValue("FK_EMPLOYEE", employeeId);
            rsFinishedProjectTime.SetFieldValue("STARTDATE", minDate);
            rsFinishedProjectTime.SetFieldValue("ENDDATE", maxDate);
        }
        

        if(rsFinishedProjectTime.RecordCount == 0)
        {
            return;
        }

        var updateResult = rsFinishedProjectTime.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Definitief afsluiten van uren uit geselecteerde periode mislukt: {updateResult.First(x => x.HasError).GetResult()}",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

    }

    private void UpdateRecordsToExportProjectTimes(List<ToExportProjectTime> toExportProjectTimesToAfas)
    {
        var toExportProjectTimeIdsToUpdate = toExportProjectTimesToAfas
            .Where(x => x.SuccesfulExportedToAfas).Select(x => x.PK_C_TOEXPORTPROJECTTIMES).ToList();

        if (!toExportProjectTimeIdsToUpdate.Any())
        {
            MessageBox.Show("Geen loonmutaties aangemaakt in AFAS.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);

            return;
        }

        var currentEmployeeId = GetRecordset("R_USER", "FK_EMPLOYEE", $"PK_R_USER = {CurrentUserId}", "")
            .DataTable.AsEnumerable().First().Field<int?>("FK_EMPLOYEE") ?? 0;

        var rsToExportProjectTimesToUpdate = GetRecordset("C_TOEXPORTPROJECTTIMES", "",
            $"PK_C_TOEXPORTPROJECTTIMES IN ({string.Join(",", toExportProjectTimeIdsToUpdate)})", "");
        rsToExportProjectTimesToUpdate.UpdateWhenMoveRecord = false;
        rsToExportProjectTimesToUpdate.UseDataChanges = true;

        rsToExportProjectTimesToUpdate.MoveFirst();
        while (!rsToExportProjectTimesToUpdate.EOF)
        {
            rsToExportProjectTimesToUpdate.SetFieldValue("EXPORTED", true);
            rsToExportProjectTimesToUpdate.SetFieldValue("DATEEXPORTED", DateTime.Now);

            if (currentEmployeeId != 0)
            {
                rsToExportProjectTimesToUpdate.SetFieldValue("FK_EXPORTEDBY", currentEmployeeId);
            }

            rsToExportProjectTimesToUpdate.MoveNext();
        }

        rsToExportProjectTimesToUpdate.MoveFirst();
        var updateResult = rsToExportProjectTimesToUpdate.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Bijwerken te exporteren overuren naar AFAS is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        MessageBox.Show($"{toExportProjectTimeIdsToUpdate.Count} loonmutaties aangemaakt in AFAS.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
    }

    private void ExportGroupedProjectTimesToAfas(List<ToExportProjectTime> toExportProjectTimesToAfas)
    {
        var employees = GetRecordset("R_EMPLOYEE", "CODE", $"PK_R_EMPLOYEE IN ({string.Join(",", toExportProjectTimesToAfas.Select(x => x.FK_EMPLOYEE))})",
            "").As<Employee>().ToList();

        var timeCodes = GetRecordset("R_TIMECODE", "AFASWAGECOMPONENTCODE",
            $"PK_R_TIMECODE IN ({string.Join(",", toExportProjectTimesToAfas.Select(x => x.FK_OVERTIMECODE ?? 0))})",
            "").As<TimeCode>().ToList();

        var employeeCodes = employees.Select(x => x.CODE).Distinct().ToList();
        var afasEmployees = GetEmployeesFromAfas(employeeCodes);

        foreach (var toExportProjectTimeToAfas in toExportProjectTimesToAfas)
        {
            var employee = employees.First(x => x.PK_R_EMPLOYEE == toExportProjectTimeToAfas.FK_EMPLOYEE);
            var timeCode = timeCodes.FirstOrDefault(x => x.PK_R_TIMECODE == toExportProjectTimeToAfas.FK_OVERTIMECODE);

            var afasEmployee = afasEmployees.FirstOrDefault(x => x.EmployeeId.Equals(employee.CODE));

            if(afasEmployee == null)
            {
                MessageBox.Show($"Geen AFAS werknemer gevonden bij {GetRecordTag("R_EMPLOYEE", employee.PK_R_EMPLOYEE)}", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                continue;
            }

            PostGroupedProjectTimeToAfas(toExportProjectTimeToAfas, employee, timeCode, afasEmployee);
        }
    }

    private void PostGroupedProjectTimeToAfas(ToExportProjectTime toExportProjectTimeToAfas, Employee employee, TimeCode timeCode,
        AfasEmployee afasEmployee)
    {
        var wageGroupComponent = string.Empty;
        if(timeCode != null)
        {
            wageGroupComponent = timeCode.AFASWAGECOMPONENTCODE;
        }
        else if(timeCode == null && toExportProjectTimeToAfas.HOURS.Value != 0.0)
        {
            wageGroupComponent = "20"; //Bij oproepkrachten is er geen overuursoort aanwezig en exporteren we naar looncomponent 20 - gewerkte uren
        }
        else if (timeCode == null && toExportProjectTimeToAfas.DAYS != 0.0)
        {
            wageGroupComponent = "10"; //Bij oproepkrachten is er geen overuursoort aanwezig en exporteren we naar looncomponent 10 - aantal dagen
        }


        var url = new Uri(
            $"{AfasStartUrl}/ProfitRestServices/connectors/HrCompMut");

        HttpClient client = new HttpClient();
        client.DefaultRequestHeaders.Add("Authorization", AfasToken);

        //De overuren van april worden in mei uitbetaald
        var yearWageComponent = toExportProjectTimeToAfas.MONTH.Value == 12 ? toExportProjectTimeToAfas.YEAR.Value + 1 : toExportProjectTimeToAfas.YEAR.Value;
        var monthWageComponent = toExportProjectTimeToAfas.MONTH.Value == 12 ? 1 : toExportProjectTimeToAfas.MONTH.Value + 1;

        var rootObjectHrCompMut = new RootObjectHrCompMut()
        {
            HrCompMut = new RootObjectHrCompMut.Hrcompmut()
            {
                Element = new RootObjectHrCompMut.Element()
                {
                    EmployerId = afasEmployee.EmployerId,
                    EmployeeId = employee.CODE,
                    PeriodTableId = afasEmployee.PeriodTable.Value,
                    Year = yearWageComponent,
                    Period = monthWageComponent,
                    Date = toExportProjectTimeToAfas.DATEWAGECOMPONENT.Value,
                    WageGroupComponent = wageGroupComponent, 
                    LineNumber = 1,
                    Fields = new RootObjectHrCompMut.Fields()
                    {
                        Hours = toExportProjectTimeToAfas.HOURS.Value == 0.0 //Bij oproepkrachten kan 
                            ? toExportProjectTimeToAfas.DAYS
                            : toExportProjectTimeToAfas.HOURS.Value,
                        Description = $"Overuren Ridder {toExportProjectTimeToAfas.YEAR.Value} maand {toExportProjectTimeToAfas.MONTH.Value}",
                    }
                }
            }
        };

        var response = client.PostAsJsonAsync(url, rootObjectHrCompMut).Result;

        if (!response.IsSuccessStatusCode)
        {
            toExportProjectTimeToAfas.SuccesfulExportedToAfas = false;

            try
            {
                var errorResult = response.Content.ReadAsAsync<ErrorResponse>().Result;

                MessageBox.Show($"Aanmaken loonmutatie in AFAS bij {GetRecordTag("R_EMPLOYEE", toExportProjectTimeToAfas.FK_EMPLOYEE)} is mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase} - {errorResult.ExternalMessage}",
                    "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show($"Aanmaken verlofboeking mislukt, deserializen object mislukt bij {GetRecordTag("R_EMPLOYEE", toExportProjectTimeToAfas.FK_EMPLOYEE)}",
                    "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return;
        }

        toExportProjectTimeToAfas.SuccesfulExportedToAfas = true;
    }

    public static List<AfasEmployee> GetEmployeesFromAfas(List<string> employeeCodes)
    {
        var result = new List<AfasEmployee>();
        var numberOfEmployeesPerCall = 100;
        var counter = 0;
        var allEmployeesCollected = false;

        while (!allEmployeesCollected)
        {
            var skip = counter * numberOfEmployeesPerCall;

            var filter = BuildUpFilter("EmployeeId", employeeCodes);

            if (string.IsNullOrEmpty(filter))
            {
                return new List<AfasEmployee>();
            }

            var url = new Uri(
                $"{AfasStartUrl}/ProfitRestServices/connectors/Ridder_Profit_Employees?{filter}&Skip={skip}&Take={numberOfEmployeesPerCall}&orderbyfieldids=EmployeeId");

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", AfasToken);

            var response = client.GetAsync(url).GetAwaiter().GetResult();

            if (!response.IsSuccessStatusCode)
            {
                MessageBox.Show(
                    $"Verbinden AFAS API mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase}", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return new List<AfasEmployee>();
            }

            RootObjectGet<AfasEmployee> rootObjectGetEmployees;

            try
            {
                rootObjectGetEmployees = response.Content.ReadAsAsync<RootObjectGet<AfasEmployee>>().Result;
            }
            catch (Exception e)
            {
                MessageBox.Show($"Converteren API response naar RootObject mislukt, oorzaak: {e}",
                        "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
                return new List<AfasEmployee>();
            }

            if (rootObjectGetEmployees == null)
            {
                return result;
            }

            result.AddRange(rootObjectGetEmployees.Rows);

            if (rootObjectGetEmployees.Rows.Count < numberOfEmployeesPerCall)
            {
                allEmployeesCollected = true;
            }

            counter++;
        }

        return result;
    }

    private static string BuildUpFilter(string filterField, List<string> codes)
    {
        if (!codes.Any())
        {
            return string.Empty;
        }

        var result = "filterfieldids=";

        for (int i = 0; i < codes.Count(); i++)
        {
            if (i == 0)
            {
                result += filterField;
            }
            else
            {
                result += $"%3B{filterField}";
            }
        }

        result += "&filtervalues=";

        for (int i = 0; i < codes.Count(); i++)
        {
            if (i == 0)
            {
                result += codes[i];
            }
            else
            {
                result += $"%3B{codes[i]}";
            }
        }

        result += "&operatortypes=";

        for (int i = 0; i < codes.Count(); i++)
        {
            if (i == 0)
            {
                result += "1";
            }
            else
            {
                result += $"%3B1";
            }
        }

        return result;
    }
}
