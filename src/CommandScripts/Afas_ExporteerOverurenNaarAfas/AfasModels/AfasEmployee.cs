﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Afas_ExporteerOverurenNaarAfas.AfasModels
{
    public class AfasEmployee
    {
        public string EmployeeId { get; set; }
        public string PersonId { get; set; }
        public string EmployerId { get; set; }
        public int? PeriodTable { get; set; }
    }
}
