﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Afas_ExporteerOverurenNaarAfas.AfasModels
{
    class RootObjectHrCompMut
    {
        public Hrcompmut HrCompMut { get; set; }

        public class Hrcompmut
        {
            public Element Element { get; set; }
        }

        public class Element
        {
            [JsonProperty("@PtId")]
            public int PeriodTableId { get; set; }
            [JsonProperty("@Year")]
            public int Year { get; set; }
            [JsonProperty("@PeId")]
            public int Period { get; set; }
            [JsonProperty("@Id")]
            public int LineNumber { get; set; }
            [JsonProperty("@DaTi")]
            public DateTime Date { get; set; }
            [JsonProperty("@EmId")]
            public string EmployeeId { get; set; }
            [JsonProperty("@ErId")]
            public string EmployerId { get; set; }
            [JsonProperty("@Sc02")]
            public string WageGroupComponent { get; set; }
            public Fields Fields { get; set; }
        }

        public class Fields
        {
            [JsonProperty("Ds")]
            public string Description { get; set; }
            [JsonProperty("VaD1")]
            public double Hours { get; set; }
        }
    }
}
