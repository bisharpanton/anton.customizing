﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Afas_ExporteerOverurenNaarAfas.AfasModels
{
    class ErrorResponse
    {
        [JsonProperty("errorNumber")]
        public int ErrorNumber { get; set; }
        [JsonProperty("externalMessage")]
        public string ExternalMessage { get; set; }
        [JsonProperty("profitLogReference")]
        public string ProfitLogReference { get; set; }
    }
}
