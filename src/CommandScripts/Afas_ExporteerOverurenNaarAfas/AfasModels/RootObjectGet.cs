﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Afas_ExporteerOverurenNaarAfas.AfasModels
{
    class RootObjectGet<T>
    {
        [JsonProperty("skip")]
        public int Skip { get; set; }
        [JsonProperty("take")]
        public int Take { get; set; }
        [JsonProperty("rows")]
        public List<T> Rows { get; set; }
    }
}
