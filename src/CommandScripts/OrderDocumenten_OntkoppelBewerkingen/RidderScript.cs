﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class OrderDocumenten_OntkoppelBewerkingen_RidderScript : CommandScript
{
    public void Execute()
    {
        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("R_ORDERDOCUMENT"))
        {
            MessageBox.Show("Dit script mag alleen vanaf de orderdocumenten aangeroepen worden.");
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        if (FormDataAwareFunctions.GetSelectedRecords().Count() > 1)
        {
            MessageBox.Show("Dit script mag voor één document tegelijk aangeroepen worden.");
            return;
        }

        var orderDocumentId = (int)FormDataAwareFunctions.GetSelectedRecords().First().GetPrimaryKeyValue();

        var alreadyLinkedWorkactivities = GetRecordset("U_WORKACTIVITIESPERORDERDOCUMENT", "FK_WORKACTIVITY",
            $"FK_ORDERDOCUMENT = {orderDocumentId}", "").DataTable.AsEnumerable().Select(x => x.Field<int>("FK_WORKACTIVITY")).ToList();

        if (!alreadyLinkedWorkactivities.Any())
        {
            MessageBox.Show("Geen bewerkingen gekoppeld.");
            return;
        }

        var resultWorkactivities = OpenMultiSelectForm("R_WORKACTIVITY", "PK_R_WORKACTIVITY", 
            $"PK_R_WORKACTIVITY IN ({string.Join(",", alreadyLinkedWorkactivities)})", "ChooseWorkactivity");

        if (resultWorkactivities == null)
        {
            return;
        }

        DeleteRecords(orderDocumentId, resultWorkactivities);
        CreateDeletedDocumentsRecords(orderDocumentId, resultWorkactivities);

        FormDataAwareFunctions.Refres();
    }

    private void CreateDeletedDocumentsRecords(int orderDocumentId, object[] resultWorkactivities)
    {
        var orderDocument = GetRecordset("R_ORDERDOCUMENT", "FK_ORDER, FK_DOCUMENT", $"PK_R_ORDERDOCUMENT = {orderDocumentId}", "")
            .DataTable.AsEnumerable().First();

        var joborderDetailWorkactivities = GetRecordset("R_JOBORDERDETAILWORKACTIVITY", "PK_R_JOBORDERDETAILWORKACTIVITY",
            $"FK_ORDER = {orderDocument.Field<int>("FK_ORDER")} AND FK_WORKACTIVITY IN ({string.Join(",", resultWorkactivities)})", "")
            .DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_JOBORDERDETAILWORKACTIVITY")).ToList();

        if(!joborderDetailWorkactivities.Any())
        {
            return;
        }

        var rs = GetRecordset("U_DELETEDDOCUMENTS", "", $"PK_U_DELETEDDOCUMENTS = NULL", "");

        rs.UpdateWhenMoveRecord = false;

        foreach (var joborderDetailWorkactivityId in joborderDetailWorkactivities)
        {
            rs.AddNew();
            rs.SetFieldValue("DOCUMENTID", orderDocument.Field<int>("FK_DOCUMENT"));
            rs.SetFieldValue("TABLENAME", "R_JOBORDERDETAILWORKACTIVITY");
            rs.SetFieldValue("RECORDID", joborderDetailWorkactivityId);
        }

        rs.MoveFirst();
        rs.Update();
    }

    private void DeleteRecords(int orderDocumentId, object[] resultWorkactivities)
    {
        var rs = GetRecordset("U_WORKACTIVITIESPERORDERDOCUMENT", "", 
            $"FK_ORDERDOCUMENT = {orderDocumentId} AND FK_WORKACTIVITY IN ({string.Join(",", resultWorkactivities)})", "");

        rs.UpdateWhenMoveRecord = false;

        rs.MoveFirst();
        while(!rs.EOF)
        {
            rs.Delete();
            rs.MoveNext();
        }

        rs.MoveFirst();
        rs.Update();
    }
}
