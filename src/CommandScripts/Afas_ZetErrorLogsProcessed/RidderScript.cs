﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class Afas_ZetErrorLogsProcessed_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //3-11-2022
        //Zet error logs processed in batch

        if(FormDataAwareFunctions == null || FormDataAwareFunctions.TableName != "C_LOG")
        {
            MessageBox.Show("Dit script mag alleen vanaf de log tabel aangeroepen worden.");
            return;
        }

        if(!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        var ids = FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue()).ToList();

        var rsLog = GetRecordset("C_LOG", "ERRORLOGDONE", $"PK_C_LOG IN ({string.Join(",", ids)}) AND ERRORLOGDONE = 0", "");
        
        if(rsLog.RecordCount == 0)
        {
            return;
        }

        rsLog.UseDataChanges = false;
        rsLog.UpdateWhenMoveRecord = false;

        rsLog.MoveFirst();
        while(!rsLog.EOF)
        {
            rsLog.SetFieldValue("ERRORLOGDONE", true);
            rsLog.MoveNext();
        }

        rsLog.MoveFirst();
        rsLog.Update();
    }
}
