﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;


public class OddEmployeeWorkday_CloseForm_RidderScript : CommandScript
{
	public void Execute()
	{
        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("U_FORMIMPORTODDEMPLOYEEWORKDAYS"))
        {
            MessageBox.Show("Dit script mag alleen vanaf Form import afwijkende werktijden aangeroepen worden.");
            return;
        }

        FormDataAwareFunctions.Save();

        var formId = (int)FormDataAwareFunctions.CurrentRecord.GetPrimaryKeyValue();

        var rsForm = GetRecordset("U_FORMIMPORTODDEMPLOYEEWORKDAYS", "", $"PK_U_FORMIMPORTODDEMPLOYEEWORKDAYS = {formId}", "");
        rsForm.MoveFirst();

        rsForm.SetFieldValue("PROCESSED", true);
        rsForm.Update();

        Form.ActiveForm.Close();

    }
}
