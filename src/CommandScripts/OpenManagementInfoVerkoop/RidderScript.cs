﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;

public class OpenManagementInfoVerkoop_RidderScript : CommandScript
{
    public void Execute()
    {
		double offertebedragBU = 0.0;
		double orderbedragBU = 0.0;
		double offertebedragLTB = 0.0;
		double orderbedragLTB = 0.0;
		double offertebedragINDUS = 0.0;
		double orderbedragINDUS = 0.0;
		double offertebedragGWW = 0.0;
		double orderbedragGWW = 0.0;
		
		//DB
		//17-11-2014
		//Bereken per sector het totale offertebedrag en het totale orderbedrag

        var jaarGeleden = System.DateTime.Now.Date.AddYears(-1).AddDays((-1 * System.DateTime.Now.Day) + 1);

		//Haal de offertes op die voldoen aan de volgende condities:
		//1) Niet gereviseerd en niet historisch
		//2) Ordercategorie is id 1 of 2
		//3) Offertes vanaf dezelfde maand van het vorige jaar
		ScriptRecordset rsOfferte = this.GetRecordset("R_OFFER", "PK_R_OFFER, FK_INDUSTRY", 
			"FK_ORDERCATEGORY <= 2 AND FK_WORKFLOWSTATE <> 'bb352a2a-6168-4d76-9f2a-3ca32e319a8f' AND FK_WORKFLOWSTATE <> '0929ec3c-3e8f-4d59-accf-17b40cd4b8c9' AND OFFERDATE  >= '" + String.Format("{0:yyyy/M/d}", jaarGeleden) + "'", "");
		rsOfferte.MoveFirst();

		var offertes = rsOfferte.DataTable.AsEnumerable();
		var offertesArray = rsOfferte.DataTable.AsEnumerable().Select( x => x.Field<int>("PK_R_OFFER") ).ToArray();

		if (!offertesArray.Any())
		{
			offertebedragBU = 0.0;
			offertebedragLTB = 0.0;
			offertebedragINDUS = 0.0;
			offertebedragGWW = 0.0;
		}
		else
		{
			//Optellen offertebedrag
			ScriptRecordset rsOfferteregels = this.GetRecordset("R_OFFERALLDETAIL", "FK_OFFER, NETSALESAMOUNT", string.Format("FK_OFFER in ({0})", string.Join(",", offertesArray)), "");
			var offerteregels = rsOfferteregels.DataTable.AsEnumerable();

			var offerteInfo =
				(from offerte in offertes
				 join offerteregel in offerteregels on offerte.Field<int>("PK_R_OFFER") equals offerteregel.Field<int>("FK_OFFER")
				 select new
				 {
					 offerteId = offerte.Field<int>("PK_R_OFFER"),
					 sector = offerte.Field<int>("FK_INDUSTRY"),
					 nettoVerkoopbedrag = offerteregel.Field<double>("NETSALESAMOUNT")
				 }).ToList();

			offertebedragBU = offerteInfo.Where(x => x.sector == 2).Select(x => x.nettoVerkoopbedrag).Sum();
			offertebedragLTB = offerteInfo.Where(x => x.sector == 3).Select(x => x.nettoVerkoopbedrag).Sum();
			offertebedragINDUS = offerteInfo.Where(x => x.sector == 4).Select(x => x.nettoVerkoopbedrag).Sum();
			offertebedragGWW = offerteInfo.Where(x => x.sector == 5).Select(x => x.nettoVerkoopbedrag).Sum();
		}

		//Tel verkoopbedrag op excl meerwerk
		//1) Ordercategorie is id 1 of 2
		//2) Orders vanaf dezelfde maand van het vorige jaar
		//Bij optellen van het orderbedrag wordt rekening gehouden dat het geen meerwerk is
		var rsOrder = GetRecordset("R_ORDER", "PK_R_ORDER, FK_INDUSTRY",
			"FK_ORDERCATEGORY <= 2 AND FK_INDUSTRY IS NOT NULL AND ORDERDATE  >= '" + $"{jaarGeleden:yyyy/M/d}" + "'", "");
        rsOrder.MoveFirst();

		var orders = rsOrder.DataTable.AsEnumerable();
		int[] ordersArray = orders.Select(x => x.Field<int>("PK_R_ORDER")).ToArray();

		if (!ordersArray.Any())
		{
			orderbedragBU = 0.0;
			orderbedragLTB = 0.0;
			orderbedragINDUS = 0.0;
			orderbedragGWW = 0.0;
		}
		else
		{
			//Optellen verkoopregelArtikel
			var rsVerkoopregelArtikel = GetRecordset("R_SALESORDERDETAILITEM", "FK_ORDER, NETSALESAMOUNT",
                $"FK_ORDER in ({string.Join(",", ordersArray)}) AND ADDITIONALWORK = 0", "");
			var verkoopregelsArtikel = rsVerkoopregelArtikel.DataTable.AsEnumerable();

			var verkoopregelArtikelInfo =
				(from order in orders
				 join verkoopregelArtikel in verkoopregelsArtikel on order.Field<int>("PK_R_ORDER") equals verkoopregelArtikel.Field<int>("FK_ORDER")
				 select new
				 {
					 orderId = order.Field<int>("PK_R_ORDER"),
					 sector = order.Field<int>("FK_INDUSTRY"),
					 nettoVerkoopbedrag = verkoopregelArtikel.Field<double>("NETSALESAMOUNT")
				 }).ToList();

			//Optellen verkoopregelDivers
			var rsVerkoopregelDivers = GetRecordset("R_SALESORDERDETAILMISC", "FK_ORDER, NETSALESAMOUNT",
                $"FK_ORDER in ({string.Join(",", ordersArray)}) AND ADDITIONALWORK = 0", "");
			var verkoopregelsDivers = rsVerkoopregelDivers.DataTable.AsEnumerable();

			var verkoopregelDiversInfo =
				(from order in orders
				 join verkoopregelDivers in verkoopregelsDivers on order.Field<int>("PK_R_ORDER") equals verkoopregelDivers.Field<int>("FK_ORDER")
				 select new
				 {
					 orderId = order.Field<int>("PK_R_ORDER"),
					 sector = order.Field<int>("FK_INDUSTRY"),
					 nettoVerkoopbedrag = verkoopregelDivers.Field<double>("NETSALESAMOUNT")
				 }).ToList();

			//Optellen verkoopregelStuklijst
			var rsVerkoopregelStuklijst = GetRecordset("R_SALESORDERDETAILASSEMBLY", "FK_ORDER, NETSALESAMOUNT",
                $"FK_ORDER in ({string.Join(",", ordersArray)}) AND ADDITIONALWORK = 0", "");
			var verkoopregelsStuklijst = rsVerkoopregelStuklijst.DataTable.AsEnumerable();

			var verkoopregelStuklijstInfo =
				(from order in orders
				 join verkoopregelStuklijst in verkoopregelsStuklijst on order.Field<int>("PK_R_ORDER") equals verkoopregelStuklijst.Field<int>("FK_ORDER")
				 select new
				 {
					 orderId = order.Field<int>("PK_R_ORDER"),
					 sector = order.Field<int>("FK_INDUSTRY"),
					 nettoVerkoopbedrag = verkoopregelStuklijst.Field<double>("NETSALESAMOUNT")
				 }).ToList();

			orderbedragBU = verkoopregelArtikelInfo.Where(x => x.sector == 2).Select(x => x.nettoVerkoopbedrag).Sum() + verkoopregelDiversInfo.Where(x => x.sector == 2).Select(x => x.nettoVerkoopbedrag).Sum() + verkoopregelStuklijstInfo.Where(x => x.sector == 2).Select(x => x.nettoVerkoopbedrag).Sum();
			orderbedragLTB = verkoopregelArtikelInfo.Where(x => x.sector == 3).Select(x => x.nettoVerkoopbedrag).Sum() + verkoopregelDiversInfo.Where(x => x.sector == 3).Select(x => x.nettoVerkoopbedrag).Sum() + verkoopregelStuklijstInfo.Where(x => x.sector == 3).Select(x => x.nettoVerkoopbedrag).Sum();
			orderbedragINDUS = verkoopregelArtikelInfo.Where(x => x.sector == 4).Select(x => x.nettoVerkoopbedrag).Sum() + verkoopregelDiversInfo.Where(x => x.sector == 4).Select(x => x.nettoVerkoopbedrag).Sum() + verkoopregelStuklijstInfo.Where(x => x.sector == 4).Select(x => x.nettoVerkoopbedrag).Sum();
			orderbedragGWW = verkoopregelArtikelInfo.Where(x => x.sector == 5).Select(x => x.nettoVerkoopbedrag).Sum() + verkoopregelDiversInfo.Where(x => x.sector == 5).Select(x => x.nettoVerkoopbedrag).Sum() + verkoopregelStuklijstInfo.Where(x => x.sector == 5).Select(x => x.nettoVerkoopbedrag).Sum();
		}

		//Wegschrijven resultaten
		ScriptRecordset rsManagementInfo = GetRecordset("U_PARAMETERSVERKOOPINFORMATIE", "", "", "");
		rsManagementInfo.MoveFirst();
		
		//BU
		rsManagementInfo.Fields["OFFERTEBEDRAGBU"].Value = offertebedragBU;
		rsManagementInfo.Fields["ORDERBEDRAGBU"].Value = orderbedragBU;
		if( offertebedragBU > 0.0)
			rsManagementInfo.Fields["SCORINGSKANSBU"].Value = orderbedragBU / offertebedragBU;
		
		//LTB
		rsManagementInfo.Fields["OFFERTEBEDRAGLTB"].Value = offertebedragLTB;
		rsManagementInfo.Fields["ORDERBEDRAGLTB"].Value = orderbedragLTB;
		if( offertebedragLTB > 0.0)
			rsManagementInfo.Fields["SCORINGSKANSLTB"].Value = orderbedragLTB / offertebedragLTB;
		
		//INDUS
		rsManagementInfo.Fields["OFFERTEBEDRAGINDUS"].Value = offertebedragINDUS;
		rsManagementInfo.Fields["ORDERBEDRAGINDUS"].Value = orderbedragINDUS;
		if( offertebedragINDUS > 0.0)
			rsManagementInfo.Fields["SCORINGSKANSINDUS"].Value = orderbedragINDUS / offertebedragINDUS;
		
		//GWW
		rsManagementInfo.Fields["OFFERTEBEDRAGGWW"].Value = offertebedragGWW;
		rsManagementInfo.Fields["ORDERBEDRAGGWW"].Value = orderbedragGWW;
		if( offertebedragGWW > 0.0)
			rsManagementInfo.Fields["SCORINGSKANSGWW"].Value = orderbedragGWW / offertebedragGWW;
		
		rsManagementInfo.Update(null, null);

		OpenEditForm("U_PARAMETERSVERKOOPINFORMATIE", (int) rsManagementInfo.Fields["PK_U_PARAMETERSVERKOOPINFORMATIE"].Value , null, false);
	}
}
