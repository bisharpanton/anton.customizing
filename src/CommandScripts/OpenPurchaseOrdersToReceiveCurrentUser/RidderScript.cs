﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using Ridder.Client.SDK.SDKParameters;

public class OpenPurchaseOrdersToReceiveCurrentUser_RidderScript : CommandScript
{
	public void Execute()
	{
        //SP
        //13-6-2024
        //Open de openstaande inkooporders van de huidige gebruiker

        var currentEmployeeId = GetRecordset("R_USER", "FK_EMPLOYEE",
               $"PK_R_USER = {CurrentUserInfo.CurrentUserId}", "").DataTable.AsEnumerable().First().Field<int?>("FK_EMPLOYEE") ?? 0;

        var purchaseOrdersToReceiveIds = GetRecordset("R_PURCHASEORDER", "PK_R_PURCHASEORDER",
            $"FK_PURCHASER = {currentEmployeeId} AND RECEIPTSTATE = 1", "").DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_PURCHASEORDER")).ToList();

        if(!purchaseOrdersToReceiveIds.Any())
        {
            return;
        }

        OpenPurchaseOrders(purchaseOrdersToReceiveIds);

    }

    private void OpenPurchaseOrders(List<int> purchaseOrdersToReceiveIds)
    {
        var browsePar = new BrowseParameters()
        {
            TableName = "R_PURCHASEORDER",
            Filter = $"PK_R_PURCHASEORDER IN ({string.Join(",", purchaseOrdersToReceiveIds)})",
        };

        OpenBrowse(browsePar, false);
    }
}
