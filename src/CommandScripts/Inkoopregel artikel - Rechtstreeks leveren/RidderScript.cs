﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Drawing;

public class BonregelArtikelRechtstreeksLeverenRidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //5-8-2020
        //Wijzig van geselecteerde inkoopregels de leverwijze naar 'Rechtstreeks leveren'
        
        if (FormDataAwareFunctions == null || FormDataAwareFunctions.TableName != "R_JOBORDERDETAILITEM")
        {
            MessageBox.Show("Dit script mag alleen vanaf de bonregel artikel-tabel aangeroepen worden.");
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        ChangeSelectedRecords();
    }

    private void ChangeSelectedRecords()
    {
        var selectedRecordIds = FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue())
            .ToList();

        var rsJoborderDetailItem = GetRecordset("R_JOBORDERDETAILITEM", "",
            $"PK_R_JOBORDERDETAILITEM IN ({string.Join(",", selectedRecordIds)})", "");
        rsJoborderDetailItem.UpdateWhenMoveRecord = false;
        rsJoborderDetailItem.UseDataChanges = true;

        var recordsChanged = false;
        rsJoborderDetailItem.MoveFirst();
        while (!rsJoborderDetailItem.EOF)
        {
            if ((DeliveryMethod)rsJoborderDetailItem.GetField("DELIVERYMETHOD").Value ==
                DeliveryMethod.DirectDelivery)
            {
                rsJoborderDetailItem.MoveNext();
                continue;
            }

            if (!recordsChanged)
            {
                recordsChanged = true;
            }

            rsJoborderDetailItem.SetFieldValue("DELIVERYMETHOD", DeliveryMethod.DirectDelivery);

            rsJoborderDetailItem.MoveNext();
        }

        if (!recordsChanged)
        {
            return;
        }

        rsJoborderDetailItem.MoveFirst();
        var updateResult = rsJoborderDetailItem.Update2();

        if (updateResult != null && updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Het wijzigen van de bonregels is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }
    }
}
