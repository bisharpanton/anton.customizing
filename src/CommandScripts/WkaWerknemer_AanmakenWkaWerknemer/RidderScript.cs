﻿using ADODB;
using Ridder.Client.SDK.SDKParameters;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class WkaWerknemer_AanmakenWkaWerknemer_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //4-4-2023
        //Aanmaken WKA werknemer door een call te sturen naar de centrale WKA API

        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("U_RELATIONWKAEMPLOYEES"))
        {
            MessageBox.Show("Dit script mag alleen vanaf de relatie WKA werknemers aangeroepen worden.");
            return;
        }

        if (FormDataAwareFunctions.FormParent == null || !FormDataAwareFunctions.FormParent.TableName.Equals("R_RELATION"))
        {
            MessageBox.Show("Dit script mag alleen vanaf de relatie WKA werknemers binnen een relatie aangeroepen worden.");
            return;
        }

        var relationId = (int)FormDataAwareFunctions.FormParent.CurrentRecord.GetPrimaryKeyValue();

        var relation = GetRecordset("R_RELATION", "NAME, COCNUMBER, WKA", $"PK_R_RELATION = {relationId}", "")
            .DataTable.AsEnumerable().First();

        if (!relation.Field<bool>("WKA"))
        {
            MessageBox.Show("Bij deze relatie is geen WKA van toepassing.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        if (string.IsNullOrEmpty(relation.Field<string>("COCNUMBER")))
        {
            MessageBox.Show("Bij deze relatie is geen KVK nummer bekend. Dit is wel nodig voor het centrale WKA dossier. ", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        var createdFormId = CreatedForm();

        var editParameter = new EditParameters()
        {
            TableName = "U_FORMCREATEWKAEMPLOYEE",
            Id = createdFormId,
        };

        OpenEdit(editParameter, true);

        //Check of Edit correct is afgesloten
        var rsClosedForm = GetRecordset("U_FORMCREATEWKAEMPLOYEE", "", $"PK_U_FORMCREATEWKAEMPLOYEE = {createdFormId}", "");

        if (!rsClosedForm.DataTable.AsEnumerable().First().Field<bool>("PROCESS"))
        {
            //Delete Form
            rsClosedForm.MoveFirst();
            rsClosedForm.Delete();

            return; //Formulier niet correct afgesloten
        }

        Cursor.Current = Cursors.WaitCursor;
        try
        {
            var formDetails = GetRecordset("U_FORMDETAILSCREATEWKAEMPLOYEE", "", $"FK_FORMCREATEWKAEMPLOYEE = {createdFormId}", "")
                .DataTable.AsEnumerable().ToList();

            ProcessForm(relationId, relation.Field<string>("COCNUMBER"), rsClosedForm.DataTable.AsEnumerable().First(), formDetails);
        }
        finally
        {
            Cursor.Current = Cursors.Default;
        }

        MessageBox.Show($"WKA werknemer aangemaakt in centraal WKA dossier.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);

        //Delete Form
        rsClosedForm.MoveFirst();
        rsClosedForm.Delete();

        FormDataAwareFunctions.Refres();
    }

    private void ProcessForm(int relationId, string cocNumber, DataRow form, List<DataRow> formDetails)
    {
        var wkaEmployeeInfo = ConvertFormToWkaEmployeeInfo(cocNumber, form, formDetails);

        var wfCreateWkaEmployeeAtApi = new Guid("788d7efa-d470-4ecd-a0e8-2f0e8199ee01");
        var wfParameters = new Dictionary<string, object>();

        wfParameters.Add("FirstName", wkaEmployeeInfo.FirstName);
        wfParameters.Add("NamePrefix", wkaEmployeeInfo.NamePrefix);
        wfParameters.Add("LastName", wkaEmployeeInfo.LastName);
        wfParameters.Add("BSN", wkaEmployeeInfo.BSN);
        wfParameters.Add("RegNo", wkaEmployeeInfo.RegNo);
        wfParameters.Add("CocNumber", wkaEmployeeInfo.CocNumber);
        wfParameters.Add("RequiredDocumentTypes", string.Join(";", wkaEmployeeInfo.RequiredDocumentTypes));

        var wfResult = ExecuteWorkflowEvent("R_RELATION", relationId, wfCreateWkaEmployeeAtApi, wfParameters);

        if(wfResult.HasError)
        {
            MessageBox.Show($"Uitvoeren workflow om WKA werknemer naar API te sturen is mislukt, oorzaak: {wfResult.GetResult()}", "Ridder iQ",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }
    }

    private WkaEmployee ConvertFormToWkaEmployeeInfo(string cocNumber, DataRow form, List<DataRow> formDetails)
    {
        var wkaDocumentTypesDescriptions = !formDetails.Any()
            ? new List<string>()
            : GetRecordset("U_WKADOCUMENTTYPE", "DESCRIPTION",
            $"PK_U_WKADOCUMENTTYPE IN ({string.Join(",", formDetails.Select(x => x.Field<int>("FK_WKADOCUMENTTYPE")))})", "")
            .DataTable.AsEnumerable().Select(x => x.Field<string>("DESCRIPTION")).ToList();

        return new WkaEmployee()
        {
            RelationCocNumber = cocNumber,
            FirstName = form.Field<string>("FIRSTNAME"),
            NamePrefix = form.Field<string>("NAMEPREFIX"),
            LastName = form.Field<string>("LASTNAME"),
            BSN = form.Field<string>("BSN"),
            RegNo = form.Field<string>("REGNO"),
            CocNumber = form.Field<string>("COCNUMBER"),
            RequiredDocumentTypes = wkaDocumentTypesDescriptions,
        };
    }

    private int CreatedForm()
    {
        var rsForm = GetRecordset("U_FORMCREATEWKAEMPLOYEE", "", "PK_U_FORMCREATEWKAEMPLOYEE = -1", "");
        rsForm.AddNew();
        rsForm.Update();

        var createdFormId = (int)rsForm.GetField("PK_U_FORMCREATEWKAEMPLOYEE").Value;

        return createdFormId;
    }

    class WkaEmployee
    {
        public string RelationCocNumber { get; set; }
        public string FirstName { get; set; }
        public string NamePrefix { get; set; }
        public string LastName { get; set; }
        public string BSN { get; set; }
        public string RegNo { get; set; }
        public string CocNumber { get; set; }
        public List<string> RequiredDocumentTypes { get; set; }
    }
}
