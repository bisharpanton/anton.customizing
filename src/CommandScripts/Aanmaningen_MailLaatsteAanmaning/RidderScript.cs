﻿using Aanmaningen_MailLaatsteAanmaning;
using ADODB;
using Ridder.Client.SDK.SDKParameters;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class Aanmaningen_MailLaatsteAanmaning_RidderScript : CommandScript
{
    // Kies de 'mail actie' die uitgevoerd moet worden (Direct verzenden, Opslaan in drafts of Weergeven)
    private MailAction _mailAction = MailAction.Display;

    // Mail body formaat: Unspecified / HTML / RichText / Plain
    private readonly OutlookMail.OutlookBodyFormat _mailBodyFormat = OutlookMail.OutlookBodyFormat.HTML;

    // Als gekozen is voor 'Send', kan hier een wachttijd worden aangegeven ('Bezorging uitstellen' in Outlook)
    // laat op 'null' om deze feature niet te gebruiken
    //
    // Voorbeeld:
    // private Func<DateTime> _deferredDeliveryTime = () => DateTime.Now.AddHours(1);
    private readonly Func<DateTime> _deferredDeliveryTime = null;

    // Guid van de tabel R_RELATION
    private readonly Guid _paymentReminderTableInfoId = new Guid("d92d8272-4179-4634-bf87-94f0b643cee0");

    public void Execute()
    {

        //DB
        //08-01-2019
        //Genereer aanmaning voor de verkoopfacturen uit het overzicht 'Openstaande posten'.

        if (FormDataAwareFunctions.TableName == null || FormDataAwareFunctions.TableName != "C_PAYMENTREMINDER")
        {
            MessageBox.Show("Dit script mag alleen vanaf de aanmaningen-tabel aangeroepen worden.");
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        var paymentReminderIds = FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue()).ToList();

        foreach (var paymentReminderId in paymentReminderIds)
        {
            GenerateMail(paymentReminderId);

            var rsPaymentReminder = GetRecordset("C_PAYMENTREMINDER", "DATELASTREMINDER", string.Format("PK_C_PAYMENTREMINDER = {0}", paymentReminderId), "");
            rsPaymentReminder.MoveFirst();
            rsPaymentReminder.SetFieldValue("DATELASTREMINDER", DateTime.Now);
            rsPaymentReminder.Update();
        }

        FormDataAwareFunctions.Refres();
    }

    private void GenerateMail(int paymentReminderId)
    {
        var result = new GenerateMailsResult();

        var rsSettings = GetRecordset("R_MAILREPORTSETTING", "",
            $"FK_TABLEINFO = '{_paymentReminderTableInfoId}'", "");

        if (rsSettings.RecordCount == 0)
        {
            throw new Exception(
                "No mailsetting found on table 'Payment reminders'.");
        }

        rsSettings.MoveFirst();

        var languageId = GetLanguageId(paymentReminderId);
        var systemLanguageId = GetRecordset("R_LANGUAGE", "SYSTEMLANGUAGE",
            $"PK_R_LANGUAGE = {languageId}", "")
            .DataTable
            .AsEnumerable()
            .First()
            .Field<int>("SYSTEMLANGUAGE");
        /*
        var rsMailSettingsDetail = GetRecordset("R_MAILREPORTSETTINGDETAIL", "",
                $"FK_MAILREPORTSETTING = {rsSettings.GetField("PK_R_MAILREPORTSETTING").Value} AND FK_LANGUAGE = {languageId}", "");

        if (rsMailSettingsDetail.RecordCount == 0)
        {
            throw new Exception(
                "No mailsetting specific language found on table 'Payment reminders'.");
        }

        rsMailSettingsDetail.MoveFirst();

        var calculatedColumns = new[] { "FILELOCATION", "TO", "FROM", "CC", "BCC" };
        var calculatedColumnsLanguage = new[] { "FILENAME", "SUBJECT", "SALUTATION" };
        var calculatedColumnData = GetCalculatedColumnOutput(rsSettings, calculatedColumns, "C_PAYMENTREMINDER", paymentReminderId);
        var calculatedColumnDataLanguage = GetCalculatedColumnOutput(rsMailSettingsDetail, calculatedColumnsLanguage, "C_PAYMENTREMINDER", paymentReminderId);*/

        var calculatedColumns = new[] { "FILELOCATION", "FILENAME", "TO", "SUBJECT", "FROM", "CC", "BCC", "SALUTATION" };
        var calculatedColumnData = GetCalculatedColumnOutput(rsSettings, calculatedColumns, "C_PAYMENTREMINDER", paymentReminderId);

        var path = Path.GetTempPath();

        var fileName = (string)calculatedColumnData["FILENAME"];

        if (string.IsNullOrEmpty(fileName))
        {
            throw new Exception(
                "Fout bij het ophalen van gecalculeerde kolommen van mailsjabloon (Bestandsnaam is leeg).");
        }

        fileName = fileName.Replace("\\", "_").Replace("/", "_")
            .Replace(":", "_").Replace("?", "_").Replace("*", "_")
            .Replace("\"", "_").Replace("<", "_").Replace(">", "_")
            .Replace("|", "_");

        var fullFileName = Path.Combine(path, fileName);


        Guid userReportId = (Guid)rsSettings.Fields["FK_USERREPORT"].Value;
        ExportReport(paymentReminderId, "C_PAYMENTREMINDER", DesignerScope.User, userReportId, Guid.Empty, fullFileName,
            ExportType.Pdf, false);

        using (var mail = new OutlookMail
        {
            To = (string)calculatedColumnData["TO"],
            Subject = (string)calculatedColumnData["SUBJECT"]
        })
        {
            var from = calculatedColumnData["FROM"] as string;
            if (!string.IsNullOrEmpty(from))
            {
                mail.SentOnBehalfOfName = from;
            }

            var cc = calculatedColumnData["CC"] as string;
            if (!string.IsNullOrEmpty(cc))
            {
                mail.CC = cc;
            }

            var bcc = calculatedColumnData["BCC"] as string;
            if (!string.IsNullOrEmpty(bcc))
            {
                mail.BCC = bcc;
            }

            mail.BodyFormat = _mailBodyFormat;

            var salutation = calculatedColumnData["SALUTATION"] as string;
            var signature = rsSettings.Fields["SIGNATURE"].Value as string;
            var body = GetTranslatableFieldValue("R_MAILREPORTSETTING", "BODY", systemLanguageId,
                rsSettings.Fields["PK_R_MAILREPORTSETTING"].Value);
            var signaturekind = (int)rsSettings.Fields["SIGNATUREKIND"].Value;

            var bodyWithSignature = mail.HTMLBody ?? "";
            var bodyTagStart = bodyWithSignature.IndexOf("<o:p>&nbsp;", StringComparison.OrdinalIgnoreCase);
            if (bodyTagStart >= 0)
            {
                var preBodyString = bodyWithSignature.Substring(0, bodyTagStart);
                var postBodyString = bodyWithSignature.Substring(bodyTagStart);
                var salutationAndBody = salutation + "<BR><BR>" + body;
                if (signaturekind == 2) // 2 = Eigen tekst
                    salutationAndBody += "<BR><BR>" + signature;

                var newBody = preBodyString + salutationAndBody + postBodyString;
                mail.HTMLBody = newBody;
            }
            else
            {
                mail.HTMLBody = salutation + "<BR><BR>" + bodyWithSignature;
            }

            // Add default report
            mail.AddAttachment(fullFileName);

            switch (_mailAction)
            {
                case MailAction.Send:
                    if (_deferredDeliveryTime != null)
                    {
                        mail.DeferredDeliveryTime = _deferredDeliveryTime();
                    }

                    mail.Send();
                    break;
                case MailAction.SaveInDrafts:
                    mail.SaveAsDraft();
                    break;
                case MailAction.Display:
                    mail.Display();
                    break;
            }
        }
    }

    private int GetLanguageId(int paymentReminderId)
    {
        var salesInvoiceId = GetRecordset("C_PAYMENTREMINDER", "FK_SALESINVOICE", $"PK_C_PAYMENTREMINDER = {paymentReminderId}", "")
            .DataTable
            .AsEnumerable()
            .First()
            .Field<int>("FK_SALESINVOICE");

        return GetRecordset("R_SALESINVOICE", "FK_LANGUAGE", $"PK_R_SALESINVOICE = {salesInvoiceId}", "")
            .DataTable
            .AsEnumerable()
            .First()
            .Field<int>("FK_LANGUAGE");
    }

    private IDictionary<string, object> GetCalculatedColumnOutput(ScriptRecordset recordset, string[] fields,
    string tableName, object recordId)
    {
        var result = new Dictionary<string, object>();
        foreach (var field in fields)
        {
            var fieldData = recordset.Fields[field].Value != DBNull.Value
                ? recordset.Fields[field].Value as byte[]
                : null;
            if (fieldData == null)
            {
                result[field] = null;
                continue;
            }

            result[field] = GetCalculatedColumnOutput(fieldData, tableName, recordId);
        }

        return result;
    }

    public enum MailAction
    {
        Send,
        SaveInDrafts,
        Display
    }

    internal class GenerateMailsResult
    {
        public GenerateMailsResult()
        {
            LogMessage = string.Empty;
        }

        public string LogMessage { get; set; }
    }
}
