namespace Bonnen_CombineerBonnen.Models
{
    using System;
    using System.ComponentModel;
    
    
    public enum MaterialState : int
    {
        
        /// <summary>
        /// Not applicable
        /// </summary>
        [Description("Not applicable")]
        Not_applicable = 1,
        
        /// <summary>
        /// False
        /// </summary>
        [Description("False")]
        False = 2,
        
        /// <summary>
        /// True
        /// </summary>
        [Description("True")]
        True = 3,
        
        /// <summary>
        /// True for this detail
        /// </summary>
        [Description("True for this detail")]
        True_for_this_detail = 4,
    }
}
