﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Bonnen_CombineerBonnen.Models;
using Ridder.Common.Financial;
using Ridder.Common.Script;
using Ridder.Recordset.Extensions;

public class Bonnen_CombineerBonnen_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //25-11-2020
        //Combineer geselecteerde bonnen

        if (FormDataAwareFunctions == null || FormDataAwareFunctions.TableName != "R_JOBORDER")
        {
            MessageBox.Show("Dit script mag alleen vanaf de bonnen-tabel aangeroepen worden.");
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        if (FormDataAwareFunctions.GetSelectedRecords().Length == 1)
        {
            MessageBox.Show("Selecteer meerdere bonnen om te combineren.",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        var selectedRecordIds = FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue())
            .ToList();

        var selectedJoborders = GetRecordset("R_JOBORDER", "",
            $"PK_R_JOBORDER IN ({string.Join(",", selectedRecordIds)})", "").As<Joborder>().ToList();

        if (selectedJoborders.Max(x => x.FK_ORDER) != selectedJoborders.Min(x => x.FK_ORDER))
        {
            MessageBox.Show("Combineren bonnen alleen mogelijk binnen dezelfde order.",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        var wfStateNew = new Guid("d82582b2-cf61-47d2-a0d0-d44b62329e53");
        if (selectedJoborders.Any(x => x.FK_WORKFLOWSTATE.Value != wfStateNew))
        {
            MessageBox.Show("Combineren bonnen alleen mogelijk indien de bon status 'Nieuw' heeft.",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        if (Math.Abs(selectedJoborders.Max(x => x.QUANTITYPLANNED ?? 0) -
                     selectedJoborders.Min(x => x.QUANTITYPLANNED ?? 0)) > 0.01)
        {
            MessageBox.Show(
                "Combineren van deze selectie bonnen niet mogelijk. Deze bonnen hebben een verschillend 'Aantal gepland'. Hierdoor gaat het totaal aantal van de bonregels mis.",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }
        
        var firstJoborder = selectedJoborders.OrderBy(x => x.JOBORDERNUMBER).First();
        var jobordersToCombine = selectedJoborders.Where(x => x != firstJoborder).ToList();

        if (JobordersAreReservedToPurchase(jobordersToCombine))
        {
            MessageBox.Show(
                "Combineren bonnen niet meer mogelijk. Er zijn al inkoopbehoeftes gegenereerd voor deze geselecteerde bonnen.",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        if (WipExists(jobordersToCombine))
        {
            MessageBox.Show(
                "Combineren bonnen niet meer mogelijk. Er is al OHW aanwezig bij de geselecteerde bonnen.",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        if (!selectedJoborders.Max(x => x.DESCRIPTION).Equals(selectedJoborders.Min(x => x.DESCRIPTION)))
        {
            var dialogResult = MessageBox.Show(
                "Bonnen hebben verschillende omschrijvingen. Wilt u doorgaan? \nDe omschrijving van de eerste bon zal aangehouden worden.",
                "Ridder iQ", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            if (dialogResult == DialogResult.No)
            {
                return;
            }
        }

        if (!MoveJoborderDetailsToFirstJoborder(firstJoborder, jobordersToCombine))
        {
            return;
        }

        DeleteCombinedJoborders(jobordersToCombine);
    }

    private void DeleteCombinedJoborders(List<Joborder> jobordersToCombine)
    {
        var rsJoborder = GetRecordset("R_JOBORDER", "",
            $"PK_R_JOBORDER IN ({string.Join(",", jobordersToCombine.Select(x => x.PK_R_JOBORDER))})", "");
        rsJoborder.UpdateWhenMoveRecord = false;
        rsJoborder.UseDataChanges = false;

        rsJoborder.MoveFirst();
        while (!rsJoborder.EOF)
        {
            rsJoborder.Delete();
            rsJoborder.MoveNext();
        }

        rsJoborder.MoveFirst();
        var updateResult = rsJoborder.Update2();

        if (updateResult != null && updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Het verwijderen van de bonnen is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }
    }

    private bool MoveJoborderDetailsToFirstJoborder(Joborder firstJoborder, List<Joborder> jobordersToCombine)
    {
        var sources = new string[] {"ITEM", "MISC", "WORKACTIVITY", "OUTSOURCED"};

        foreach (var source in sources)
        {
            var rsJoborderDetail = GetRecordset($"R_JOBORDERDETAIL{source}", "FK_JOBORDER",
                $"FK_JOBORDER IN ({string.Join(",", jobordersToCombine.Select(x => x.PK_R_JOBORDER))})", "");

            if (rsJoborderDetail.RecordCount == 0)
            {
                continue;
            }

            rsJoborderDetail.UseDataChanges = false;
            rsJoborderDetail.UpdateWhenMoveRecord = false;

            rsJoborderDetail.MoveFirst();
            while (!rsJoborderDetail.EOF)
            {
                rsJoborderDetail.SetFieldValue("FK_JOBORDER", firstJoborder.PK_R_JOBORDER);
                rsJoborderDetail.MoveNext();
            }

            rsJoborderDetail.MoveFirst();
            var updateResult = rsJoborderDetail.Update2();

            if (updateResult != null && updateResult.Any(x => x.HasError))
            {
                MessageBox.Show($"Het opslaan bij onderdeel {source} is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}",
                    "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        return true;
    }

    private bool WipExists(List<Joborder> selectedJoborders)
    {
        return GetRecordset("R_ORDERWIPALLDETAIL", "PK_R_ORDERWIPALLDETAIL",
                $"FK_JOBORDER IN ({string.Join(",", selectedJoborders.Select(x => x.PK_R_JOBORDER))})", "")
            .RecordCount > 0;
    }

    private bool JobordersAreReservedToPurchase(List<Joborder> selectedJoborders)
    {
        var allMaterialStateRecords = GetRecordset("R_MATERIALSTATE", "",
                $"FK_JOBORDER IN ({string.Join(",", selectedJoborders.Select(x => x.PK_R_JOBORDER))})", "")
            .As<MaterialstateRecord>().ToList();

        return allMaterialStateRecords.Any(x => x.RESERVED == MaterialState.True);
    }
}
