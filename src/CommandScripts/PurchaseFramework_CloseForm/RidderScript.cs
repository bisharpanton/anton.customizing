﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class PurchaseFramework_CloseForm_RidderScript : CommandScript
{
    public void Execute()
    {
        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("U_FORMIMPORTPURCHASEDETAILFROMFRAMEWORK"))
        {
            MessageBox.Show("Dit script mag alleen vanaf Form Inkoop raamcontracten aangeroepen worden.");
            return;
        }

        FormDataAwareFunctions.Save();

        var formId = (int)FormDataAwareFunctions.CurrentRecord.GetPrimaryKeyValue();

        var rsForm = GetRecordset("U_FORMIMPORTPURCHASEDETAILFROMFRAMEWORK", "", $"PK_U_FORMIMPORTPURCHASEDETAILFROMFRAMEWORK = {formId}", "");
        rsForm.MoveFirst();

        if ((double)rsForm.GetField("QUANTITY").Value <= 0.0 || !((int?)rsForm.GetField("FK_PURCHASEFRAMEWORKDETAIL").Value).HasValue)
        {
            MessageBox.Show("Vul de aantal en raamcontract regel in.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        var form = rsForm.DataTable.AsEnumerable().First();

        var purchaseFrameworkDetailId = form.Field<int>("FK_PURCHASEFRAMEWORKDETAIL");

        var contractQuantity = GetRecordset("U_PURCHASEFRAMEWORKDETAIL", "GROSSCONTRACTPRICE, FK_ITEM, QUANTITY",
            $"PK_U_PURCHASEFRAMEWORKDETAIL = {(int)rsForm.GetField("FK_PURCHASEFRAMEWORKDETAIL").Value}", "").DataTable.AsEnumerable().First().Field<double>("QUANTITY");

        var chosenQuantity = form.Field<double>("QUANTITY");

        var availableQuantity = ValidateQuantity(purchaseFrameworkDetailId, contractQuantity, chosenQuantity);

        if (availableQuantity < 0.0)
        {
            var chosenProcessAction = form.Field<ProcessAction>("PROCESSACTION");

            if (chosenProcessAction == ProcessAction.GeenKeuzeGemaakt)
            {
                MessageBox.Show("Ingegeven aantal overschreidt contract. Kies een verwerk actie.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (chosenProcessAction == ProcessAction.Afronden && (availableQuantity == (chosenQuantity * -1)))
            {
                MessageBox.Show("Afronden komt op aantal 0. Kan geen inkoopregel aanmaken met aantal 0.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        rsForm.SetFieldValue("PROCESSED", true);
        rsForm.Update();

        Form.ActiveForm.Close();

    }

    private double ValidateQuantity(int purchaseFrameworkDetailId, double contractQuantity, double chosenQuantity)
    {
        var usedQuantity = GetRecordset("R_PURCHASEORDERDETAILITEM", "QUANTITY",
            $"FK_PURCHASEFRAMEWORKDETAIL = {purchaseFrameworkDetailId}", "").DataTable.AsEnumerable().Sum(x => x.Field<double>("QUANTITY"));

        var availableQuantity = contractQuantity - usedQuantity - chosenQuantity;

        return availableQuantity;

    }

    private enum ProcessAction : int
    {
        GeenKeuzeGemaakt = 1,
        ContractOphogen = 2,
        Afronden = 3
    }
}
