﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Drawing;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;


public class GeneratePurchaseContractPDF_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //25-02-2016
        //Exporteer verkoopfactuur en gekoppelde documenten, merge deze zodat de mail deze kan oppakken en toevoegen als document

        ScriptRecordset rsCRM = this.GetRecordset("R_CRMSETTINGS", "LOCATIETIJDELIJKDIGITALEFACTURATIE", "", "");
        rsCRM.MoveFirst();

        string locatieDigitaleFacturatie = rsCRM.Fields["LOCATIETIJDELIJKDIGITALEFACTURATIE"].Value.ToString();

        if (this.FormDataAwareFunctions.TableName != "C_PURCHASECONTRACT")
        {
            MessageBox.Show("Dit script mag alleen vanaf de inkoopcontracten-tabel aangeroepen worden.");
            return;
        }

        if (this.FormDataAwareFunctions.GetSelectedRecords().Length == 0)
            return;

        if (this.FormDataAwareFunctions.GetSelectedRecords().Length > 1)
        {
            MessageBox.Show("Dit script mag voor één inkoopcontract tegelijk aangeroepen worden.");
            return;
        }

        Guid wfStatusAfgehandeld = new Guid("49978ba1-7725-459e-8a4f-934c050fa8c1");
        bool factuurIsAfgehandeld = (Guid)this.FormDataAwareFunctions.CurrentRecord.GetCurrentRecordValue("FK_WORKFLOWSTATE") == wfStatusAfgehandeld;

        locatieDigitaleFacturatie = string.Format(@"{0}\Tempmap_Inkoopcontracten", locatieDigitaleFacturatie);

        //Verwijder oude mappen van een week oud
        string[] directories = Directory.GetDirectories(locatieDigitaleFacturatie);

        foreach (string dir in directories)
        {
            DirectoryInfo di = new DirectoryInfo(dir);
            if (di.CreationTime < DateTime.Now.AddDays(-7))
            {
                di.Delete(true);
            }
        }

        int purchaseContractId = (int)this.FormDataAwareFunctions.CurrentRecord.GetPrimaryKeyValue();

        //0) Controleer of map al bestaat, verwijder dan de map. 
        string locatieDigitaleFacturatie_Factuur = string.Format(@"{0}\{1}", locatieDigitaleFacturatie, purchaseContractId);

        if (Directory.Exists(locatieDigitaleFacturatie_Factuur))
        {
            Directory.Delete(locatieDigitaleFacturatie_Factuur, true);
        }

        //0.5) Maak map aan
        Directory.CreateDirectory(locatieDigitaleFacturatie_Factuur);

        //1) Exporteer verkoopfactuur
        Guid reportGuid = new Guid("182e3a82-e6ec-4d0d-8b79-12fb8853a77a");
        Guid reportColumnID = Guid.Empty;

        string filename = string.Format(@"{0}\InkoopcontractLos {1}.pdf", locatieDigitaleFacturatie_Factuur, purchaseContractId);
        var par = new Ridder.Client.SDK.ExportReportParameter();
        par.Id = purchaseContractId;
        par.TableName = "C_PURCHASECONTRACT";
        par.DesignerScope = Ridder.Client.SDK.SDKParameters.DesignerScope.User;
        par.Report = reportGuid;
        par.ReportColumnId = reportColumnID;
        par.FileName = filename;
        par.ExportType = Ridder.Client.SDK.SDKParameters.ExportType.Pdf;
        par.ImagesAsJpegForPDF = false;
        this.ExportReportWithParameters(par, false);
        //Uit ivm update.
        //this.ExportReportWithParameters ( ( factuurID, "R_SALESINVOICE", Ridder.Client.SDK.SDKParameters.DesignerScope.User, reportGuid, reportColumnID , filename, Ridder.Client.SDK.SDKParameters.ExportType.Pdf , false);

        //2) Exporteer factuur documenten
        //SP 15-3-2022 haal eerst alle documenten met extensie PDF op
        var pdfDocumentIds = GetRecordset("R_DOCUMENT", "PK_R_DOCUMENT",
            $"EXTENSION LIKE '%.pdf%'", "").DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_DOCUMENT")).ToList();

        ScriptRecordset rsPurchaseContractDocumument = this.GetRecordset("C_PURCHASECONTRACTDOCUMENT", "FK_DOCUMENT, ATTACHMENTNUMBER",
            $"FK_PURCHASECONTRACT = {purchaseContractId} AND FK_DOCUMENT IN ({string.Join(",", pdfDocumentIds)})", "ATTACHMENTNUMBER ASC");//Todo: sortering op basis van bijlagenummer
        rsPurchaseContractDocumument.MoveFirst();

        int teller = 1;
        while (!rsPurchaseContractDocumument.EOF)
        {
            ScriptRecordset rsDocument = this.GetRecordset("R_DOCUMENT", "STORAGESYSTEM, DOCUMENTLOCATION, DOCUMENTDATA, DESCRIPTION", string.Format("PK_R_DOCUMENT = {0}", rsPurchaseContractDocumument.Fields["FK_DOCUMENT"].Value), "");
            rsDocument.MoveFirst();

            if (rsDocument.Fields["STORAGESYSTEM"].Value.ToString() == "Verwijzing")  //Document is als verwijzing opgeslagen
            {
                if (File.Exists(rsDocument.Fields["DOCUMENTLOCATION"].Value.ToString()))
                {
                    var reader = new PdfReader($@"{rsDocument.Fields["DOCUMENTLOCATION"].Value.ToString()}");
                    var fileStream = new FileStream($@"{string.Format(@"{0}\Document {1}.pdf", locatieDigitaleFacturatie_Factuur, teller)}", FileMode.Create, FileAccess.Write);

                    var document = new Document(reader.GetPageSizeWithRotation(1));
                    var writer = PdfWriter.GetInstance(document, fileStream);

                    document.Open();

                    for (var i = 1; i <= reader.NumberOfPages; i++)
                    {
                        document.NewPage();

                        var baseFont = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        var importedPage = writer.GetImportedPage(reader, i);

                        var contentByte = writer.DirectContent;
                        contentByte.BeginText();
                        contentByte.SetFontAndSize(baseFont, 14);

                        var attachmentHeader = $"Bijlage {rsPurchaseContractDocumument.Fields["ATTACHMENTNUMBER"].Value.ToString()} - {rsDocument.Fields["DESCRIPTION"].Value.ToString()} " +
                            $"- Pagina {i}";

                        contentByte.AddTemplate(importedPage, 0, 0);

                        contentByte.ShowTextAligned(PdfContentByte.ALIGN_LEFT, attachmentHeader, 35, 825, 0);

                        contentByte.EndText();

                    }

                    document.Close();
                    writer.Close();

                    //File.Copy(rsDocument.Fields["DOCUMENTLOCATION"].Value.ToString(), string.Format(@"{0}\Document {1}.pdf", locatieDigitaleFacturatie_Factuur, teller));
                }
            }
            else  //Document is in de database opgeslagen
            {
                byte[] data = rsDocument.Fields["DOCUMENTDATA"].Value as byte[];

                if (data != null)
                {
                    string filePath = string.Format(@"{0}\Document {1}.pdf", locatieDigitaleFacturatie_Factuur, teller);
                    try
                    {
                        File.WriteAllBytes(filePath, data);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message.ToString());
                        MessageBox.Show(rsDocument.Fields["DESCRIPTION"].Value.ToString());
                    }
                }
            }

            teller++;
            rsPurchaseContractDocumument.MoveNext();
        }

        //3) Merge Verkoopfactuur en documenten
        List<string> Files = new List<string>();
        Files.Add(string.Format(@"{0}\InkoopcontractLos {1}.pdf", locatieDigitaleFacturatie_Factuur, purchaseContractId));

        for (int i = 1; i < teller; i++)
        {
            Files.Add(string.Format(@"{0}\Document {1}.pdf", locatieDigitaleFacturatie_Factuur, i));
        }

        string output = string.Format(@"{0}\Inkoopcontract {1}.pdf", locatieDigitaleFacturatie_Factuur, purchaseContractId);
        //	MergePDFs( output , Files);
        Merge(Files, output);

    }

    public void Merge(List<string> InFiles, String OutFile)
    {
        using (FileStream stream = new FileStream(OutFile, FileMode.Create))
        {
            Document doc = new Document();
            PdfCopy pdf = new PdfCopy(doc, stream);

            doc.Open();

            PdfReader reader = null;
            PdfImportedPage page = null;

            foreach (var file in InFiles)
            {
                reader = new PdfReader(file);

                for (int i = 0; i < reader.NumberOfPages; i++)
                {
                    page = pdf.GetImportedPage(reader, i + 1);
                    pdf.AddPage(page);
                }

                pdf.FreeReader(reader);
                reader.Close();
            }

            doc.Close();
            pdf.Close();
        }

        System.Diagnostics.Process.Start(OutFile);
    }


    /*
	private void MergePDFs(string outPutFilePath, List<string> Files )
	{
		List<PdfReader> readerList = new List<PdfReader>();            
		
		foreach(string file in Files)
		{
			PdfReader pdfReader = new PdfReader( file );
			readerList.Add(pdfReader);
		}

		
		//Define a new output document and its size, type
		Document document = new Document(PageSize.A4, 0, 0, 0, 0);
		//Create blank output pdf file and get the stream to write on it.
		PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(outPutFilePath, FileMode.Create));
		document.Open();

		foreach (PdfReader reader in readerList)
		{
			for (int i = 1; i <= reader.NumberOfPages; i++)
			{
				PdfImportedPage page = writer.GetImportedPage(reader, i);
				document.Add(iTextSharp.text.Image.GetInstance(page));
			}			
		}
                               
		document.Close();
		foreach (PdfReader reader in readerList)
		{
			reader.Close();
		}

		
		System.Diagnostics.Process.Start(outPutFilePath);

	} */
}
