﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Drawing;
using Ridder.Client.SDK.SDKParameters;

public class StartFormToChangeSalesInvoiceMemo_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //19-3-2019
        //Start venster om interne memo van verkoopfactuur te wijzigen vanuit aanmaningen tabel

        if (FormDataAwareFunctions == null || 
            (FormDataAwareFunctions.TableName != "C_PAYMENTREMINDER" && FormDataAwareFunctions.TableName != "R_SALESINVOICE"))
        {
            MessageBox.Show("Dit script mag alleen vanaf de aanmaningen- en/of verkoopfactuur-tabel aangeroepen worden.");
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        if (FormDataAwareFunctions.GetSelectedRecords().Length > 1)
        {
            MessageBox.Show("Dit script mag slechts voor één aanmaning/verkoopfactuur tegelijk aangeroepen worden.");
            return;
        }


        var context = string.Empty;
        var salesInvoiceId = 0;
        
        if (FormDataAwareFunctions.TableName == "R_SALESINVOICE")
        {
            salesInvoiceId = (int)FormDataAwareFunctions.CurrentRecord.GetPrimaryKeyValue();
            context = "SALESINVOICE";
        }
        else
        {
            //TableName = Payment reminder
            var rsPaymentReminder = GetRecordset("C_PAYMENTREMINDER", "FK_SALESINVOICE",
                $"PK_C_PAYMENTREMINDER = {FormDataAwareFunctions.CurrentRecord.GetPrimaryKeyValue()}", "");
            rsPaymentReminder.MoveFirst();
            salesInvoiceId = (int)rsPaymentReminder.Fields["FK_SALESINVOICE"].Value;
            context = "PAYMENTREMINDER";

        }


        var rsSalesInvoice = GetRecordset("R_SALESINVOICE", "INTERNALMEMO",
            $"PK_R_SALESINVOICE = {salesInvoiceId}", "");
        rsSalesInvoice.MoveFirst();

        var input = StartInputForm((string)rsSalesInvoice.Fields["INTERNALMEMO"].Value, context);

        if (input.InputMemo == "_DONTPROCESS")
        {
            return;
        }

        rsSalesInvoice.Fields["INTERNALMEMO"].Value = input.InputMemo;

        if (FormDataAwareFunctions.TableName == "C_PAYMENTREMINDER")
        {
            var rsPaymentReminder = GetRecordset("C_PAYMENTREMINDER", "FOLLOWUPDATE",
                $"FK_SALESINVOICE = {salesInvoiceId}", "");
            rsPaymentReminder.MoveFirst();
            rsPaymentReminder.Fields["FOLLOWUPDATE"].Value = input.FollowupDate;
            rsPaymentReminder.Update();
        }

        rsSalesInvoice.Update();

        FormDataAwareFunctions.Refres();
    }

    private Input StartInputForm(string internalMemo, string context)
    {
        var rsFormChangeMemo = GetRecordset("C_FORMCHANGESALESINVOICEMEMO", "PK_C_FORMCHANGESALESINVOICEMEMO, PROCESS, MEMO, FOLLOWUPDATE",
            $"CREATOR = '{GetUserInfo().CurrentUserName}'", "");

        //Maak nieuw record op te openen
        rsFormChangeMemo.AddNew();
        rsFormChangeMemo.Fields["MEMO"].Value = internalMemo;
        rsFormChangeMemo.Fields["FOLLOWUPDATE"].Value = DetermineFolluwUpDate();

        rsFormChangeMemo.Update();

        int createdFormId = (int)rsFormChangeMemo.Fields["PK_C_FORMCHANGESALESINVOICEMEMO"].Value;

        var editPar = new EditParameters()
        {
            TableName = "C_FORMCHANGESALESINVOICEMEMO",
            Id = createdFormId,
            Context = context
        };

        OpenEdit(editPar, true);

        //Check of het formulier correct is afgesloten
        var rsFormAfterChange = GetRecordset("C_FORMCHANGESALESINVOICEMEMO",
            "PK_C_FORMCHANGESALESINVOICEMEMO, PROCESS, MEMO, FOLLOWUPDATE",
            $"PK_C_FORMCHANGESALESINVOICEMEMO = {createdFormId}", "");

        rsFormAfterChange.MoveFirst();

        var input = new Input();


        input.InputMemo = (!(bool)rsFormAfterChange.Fields["PROCESS"].Value) ? "_DONTPROCESS" : (string)rsFormAfterChange.Fields["MEMO"].Value;
        input.FollowupDate = (DateTime)rsFormAfterChange.Fields["FOLLOWUPDATE"].Value;
        //Ruim het record weer op
        rsFormAfterChange.Delete();

        return input;
    }

    private DateTime DetermineFolluwUpDate()
    {
        var addDays = GetRecordset("R_CRMSETTINGS", "FOLLOWUPDAYS", "", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int>("FOLLOWUPDAYS"))
            .First();

        return DateTime.Now.AddDays(addDays);
    }

    public class Input
    {
        public string InputMemo { get; set; }
        public DateTime FollowupDate { get; set; }
    }
}
