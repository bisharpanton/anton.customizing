﻿using ADODB;
using Ridder.Client.SDK.SDKParameters;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class MaakMeerwerkFactuurVanuitVerkoopregelDivers_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //27-10-2015
        //Maak meerwerkfactuur vanuit verkoopregel divers

        if (this.FormDataAwareFunctions.TableName != "R_SALESORDERDETAILMISC")
        {
            MessageBox.Show("Dit script mag alleen uitgevoerd worden van de tabel 'Verkoopregel divers'.");
            return;
        }

        IRecord[] records = this.FormDataAwareFunctions.GetSelectedRecords();

        if (records.Length > 1)
        {
            MessageBox.Show("Het genereren van een meerwerkfactuur kan maar voor één verkoopregel tegelijk.");
            return;
        }

        int verkoopregelID = (int)records[0].GetPrimaryKeyValue();
        int orderID = (int)records[0].GetCurrentRecordValue("FK_ORDER");

        ScriptRecordset rsOrder = this.GetRecordset("R_ORDER", "ORDERTYPE", string.Format("PK_R_ORDER = {0}", orderID), "");
        rsOrder.MoveFirst();
        int ordertype = (int)rsOrder.Fields["ORDERTYPE"].Value;

        //Indien ordertype is regie dan gewoon door. Indien ordertype is v en verkoopregel is meerwerk dan stoppen.
        if (ordertype != 3 && (bool)records[0].GetCurrentRecordValue("ADDITIONALWORK") != true)
        {
            MessageBox.Show("Dit script kan alleen uitgevoerd worden op meerwerk-verkoopregels.");
            return;
        }

        if ((bool)records[0].GetCurrentRecordValue("DELIVERYCOMPLETE"))
        {
            MessageBox.Show("Deze meerwerk-verkoopregel staat al op pakbon. De verkoopfactuur is al gemaakt of kan vanuit de pakbon gemaakt worden.");
            return;
        }

        var allSalesOrderDetail = GetRecordset("R_SALESORDERALLDETAIL", "PK_R_SALESORDERALLDETAIL, QUANTITY, REMAININGQUANTITYTODELIVER",
            $"FK_SALESORDERDETAILMISC = {verkoopregelID}", "").DataTable.AsEnumerable().First();

        int? projectOfferSalesInstallmentId = null;
        var quantity = allSalesOrderDetail.Field<double>("REMAININGQUANTITYTODELIVER");
        
        if(GetUserInfo().DatabaseName.Equals("Anton Rail & Infra"))
        {
            projectOfferSalesInstallmentId = AskForProjectOfferSalesInstallment(verkoopregelID);

            if(projectOfferSalesInstallmentId == null)
            {
                return;
            }

            quantity = GetRecordset("U_PROJECTOFFERSALESINSTALLMENT", "INSTALLMENTPERCENTAGE",
                $"PK_U_PROJECTOFFERSALESINSTALLMENT = {projectOfferSalesInstallmentId}", "")
                .DataTable.AsEnumerable().First().Field<double>("INSTALLMENTPERCENTAGE");
        }


        //Koppel verkoopregels op pakbon
        var createdShippingOrderId = CreateShippingOrderAndLinkSalesOrderDetail(orderID, allSalesOrderDetail, quantity);

        //Stap 3. Markeer pakbon als verzonden
        Guid wf = new Guid("1e5361f7-cc15-4640-a0dc-bfc64a5d69b3");
        var result = ExecuteWorkflowEvent("R_SHIPPINGORDER", createdShippingOrderId, wf);

        if(result.HasError)
        {
            MessageBox.Show($"Afdrukken pakbon is mislukt, oorzaak: {result.GetResult()}", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        //Stap 4. Genereer factuur
        var result2 = this.EventsAndActions.Sales.Actions.CreateSalesInvoiceFromShippingOrder(createdShippingOrderId);

        if (result2.HasError)
        {
            MessageBox.Show(string.Format("Het maken van de verkoopfactuur is mislukt: {0}", result2.GetResult()));
            return;
        }

        var createdSalesInvoiceId = (int)result2.PrimaryKey;

        if(projectOfferSalesInstallmentId.HasValue)
        {
            var rsProjectOfferSalesInstallment = GetRecordset("U_PROJECTOFFERSALESINSTALLMENT", "FK_SALESINVOICE",
                $"PK_U_PROJECTOFFERSALESINSTALLMENT = {projectOfferSalesInstallmentId}", "");
            rsProjectOfferSalesInstallment.MoveFirst();
            rsProjectOfferSalesInstallment.SetFieldValue("FK_SALESINVOICE", createdSalesInvoiceId);
            rsProjectOfferSalesInstallment.Update();
        }

        //Stap 5. Open de aangemaakte verkoopfactuur
        var editPar = new EditParameters()
        {
            TableName = "R_SALESINVOICE",
            Id = createdSalesInvoiceId,
        };
        OpenEdit(editPar, false);
    }

    private int? AskForProjectOfferSalesInstallment(int verkoopregelID)
    {
        var offerDetailMiscId = GetRecordset("R_SALESORDERDETAILMISC", "FK_OFFERDETAILMISC",
            $"PK_R_SALESORDERDETAILMISC = {verkoopregelID}", "").DataTable.AsEnumerable().First().Field<int?>("FK_OFFERDETAILMISC");

        if (!offerDetailMiscId.HasValue)
        {
            return null;
        }

        var offerId = GetRecordset("R_OFFERDETAILMISC", "FK_OFFER",
            $"PK_R_OFFERDETAILMISC = {offerDetailMiscId}", "").DataTable.AsEnumerable().First().Field<int>("FK_OFFER");

        var browsePar = new BrowseParameters()
        {
            TableName = "U_PROJECTOFFERSALESINSTALLMENT",
            Filter = $"FK_OFFER = {offerId} AND FK_SALESINVOICE IS NULL",
            ResultColumnName = "PK_U_PROJECTOFFERSALESINSTALLMENT",
            Context = "ChooseProjectOfferInstallment"
        };

        var result = OpenBrowse(browsePar, true);

        if(result == null)
        {
            return null;
        }

        return (int)result;
    }

    private int CreateShippingOrderAndLinkSalesOrderDetail(int orderID, DataRow allSalesOrderDetail, double quantity)
    {
        var row = new SDKCreateShippingOrderRow()
        {
            Id = allSalesOrderDetail.Field<Guid>("PK_R_SALESORDERALLDETAIL"),
            Quantity = quantity,
            RemainingQuantity = allSalesOrderDetail.Field<double>("REMAININGQUANTITYTODELIVER"),
            BackOrderQuantity = allSalesOrderDetail.Field<double>("REMAININGQUANTITYTODELIVER") - quantity,
            DeliveryComplete = quantity >= allSalesOrderDetail.Field<double>("REMAININGQUANTITYTODELIVER"),
        };

        var createShippingOrderParameter = new CreateShippingOrderParameter();
        createShippingOrderParameter.AddRow(row);

        var result = EventsAndActions.Sales.Events.CreateShippingOrderFromOrderFromSelectedDetails(orderID, createShippingOrderParameter);

        if(result.HasError)
        {
            MessageBox.Show($"Genereren pakbon is mislukt, oorzaak: {result.GetResult()}", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return 0;
        }

        return (int)result.PrimaryKey;
    }
}
