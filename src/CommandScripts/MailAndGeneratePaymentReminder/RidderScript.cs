﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Drawing;
using System.IO;
using MailAndGeneratePaymentReminder;
using Ridder.Client.SDK.SDKParameters;

public class MailAndGeneratePaymentReminder_RidderScript : CommandScript
{
    // Kies de 'mail actie' die uitgevoerd moet worden (Direct verzenden, Opslaan in drafts of Weergeven)
    private MailAction _mailAction = MailAction.Display;

    // Mail body formaat: Unspecified / HTML / RichText / Plain
    private readonly OutlookMail.OutlookBodyFormat _mailBodyFormat = OutlookMail.OutlookBodyFormat.HTML;

    // Als gekozen is voor 'Send', kan hier een wachttijd worden aangegeven ('Bezorging uitstellen' in Outlook)
    // laat op 'null' om deze feature niet te gebruiken
    //
    // Voorbeeld:
    // private Func<DateTime> _deferredDeliveryTime = () => DateTime.Now.AddHours(1);
    private readonly Func<DateTime> _deferredDeliveryTime = null;

    // Guid van de tabel R_RELATION
    private readonly Guid _relationTableInfoId = new Guid("b328fa3c-25e1-4b0a-9c0e-32631b19a160");

    public void Execute()
    {
        //DB
        //08-01-2019
        //Genereer aanmaning voor de verkoopfacturen uit het overzicht 'Openstaande posten'.

        if (FormDataAwareFunctions.TableName == null || FormDataAwareFunctions.TableName != "R_RELATION")
        {
            MessageBox.Show("Dit script mag alleen vanaf de relatie-tabel aangeroepen worden.");
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        var relationIds = FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue()).ToList();

        foreach (var relationId in relationIds)
        {
            GenerateMail(relationId);
        }

        CreatePaymentReminders(relationIds);

        FormDataAwareFunctions.Refres();
    }

    private void GenerateMail(int relationId)
    {
        var result = new GenerateMailsResult();

        var rsSettings = GetRecordset("R_MAILREPORTSETTING", "",
            $"FK_TABLEINFO = '{_relationTableInfoId}' AND DESCRIPTION = 'Mail overzicht openstaande posten'", "");

        if (rsSettings.RecordCount == 0)
        {
            throw new Exception(
                "No mailsetting found on table 'Relations' with description 'Mail overzicht openstaande posten'.");
        }

        rsSettings.MoveFirst();
        
        var languageId = GetLanguageId(relationId);
        var systemLanguageId = GetRecordset("R_LANGUAGE", "SYSTEMLANGUAGE",
            $"PK_R_LANGUAGE = {languageId}", "")
            .DataTable
            .AsEnumerable()
            .First()
            .Field<int>("SYSTEMLANGUAGE");
        /*
        var rsMailSettingsDetail = GetRecordset("R_MAILREPORTSETTINGDETAIL", "",
        $"FK_MAILREPORTSETTING = {rsSettings.GetField("PK_R_MAILREPORTSETTING").Value} AND FK_LANGUAGE = {languageId}", "");

        if (rsMailSettingsDetail.RecordCount == 0)
        {
            throw new Exception(
                "No mailsetting specific language found on table 'Payment reminders'.");
        }

        rsMailSettingsDetail.MoveFirst();

        var calculatedColumns = new[] { "FILELOCATION", "TO", "FROM", "CC", "BCC" };
        var calculatedColumnsLanguage = new[] { "FILENAME", "SUBJECT", "SALUTATION" };
        var calculatedColumnData = GetCalculatedColumnOutput(rsSettings, calculatedColumns, "R_RELATION", relationId);
        var calculatedColumnDataLanguage = GetCalculatedColumnOutput(rsMailSettingsDetail, calculatedColumnsLanguage, "R_RELATION", relationId);*/

        var calculatedColumns = new[] { "FILELOCATION", "FILENAME", "TO", "SUBJECT", "FROM", "CC", "BCC", "SALUTATION" };
        var calculatedColumnData = GetCalculatedColumnOutput(rsSettings, calculatedColumns, "R_RELATION", relationId);

        var path = Path.GetTempPath();

        var fileName = (string)calculatedColumnData["FILENAME"];

        if (string.IsNullOrEmpty(fileName))
        {
            throw new Exception(
                "Fout bij het ophalen van gecalculeerde kolommen van mailsjabloon (Bestandsnaam is leeg).");
        }

        fileName = fileName.Replace("\\", "_").Replace("/", "_")
            .Replace(":", "_").Replace("?", "_").Replace("*", "_")
            .Replace("\"", "_").Replace("<", "_").Replace(">", "_")
            .Replace("|", "_");

        var fullFileName = Path.Combine(path, fileName);


        Guid userReportId = (Guid)rsSettings.Fields["FK_USERREPORT"].Value;
        ExportReport(relationId, "R_RELATION", DesignerScope.User, userReportId, Guid.Empty, fullFileName,
            ExportType.Pdf, false);

        using (var mail = new OutlookMail
        {
            To = (string)calculatedColumnData["TO"], Subject = (string)calculatedColumnData["SUBJECT"]
        })
        {
            var from = calculatedColumnData["FROM"] as string;
            if (!string.IsNullOrEmpty(from))
            {
                mail.SentOnBehalfOfName = from;
            }

            var cc = calculatedColumnData["CC"] as string;
            if (!string.IsNullOrEmpty(cc))
            {
                mail.CC = cc;
            }

            var bcc = calculatedColumnData["BCC"] as string;
            if (!string.IsNullOrEmpty(bcc))
            {
                mail.BCC = bcc;
            }

            mail.BodyFormat = _mailBodyFormat;

            var salutation = calculatedColumnData["SALUTATION"] as string;
            var signature = rsSettings.Fields["SIGNATURE"].Value as string;
            var body = GetTranslatableFieldValue("R_MAILREPORTSETTING", "BODY", systemLanguageId,
                rsSettings.Fields["PK_R_MAILREPORTSETTING"].Value);
            var signaturekind = (int)rsSettings.Fields["SIGNATUREKIND"].Value;

            var bodyWithSignature = mail.HTMLBody ?? "";
            var bodyTagStart = bodyWithSignature.IndexOf("<o:p>&nbsp;", StringComparison.OrdinalIgnoreCase);
            if (bodyTagStart >= 0)
            {
                var preBodyString = bodyWithSignature.Substring(0, bodyTagStart);
                var postBodyString = bodyWithSignature.Substring(bodyTagStart);
                var salutationAndBody = salutation + "<BR><BR>" + body;
                if (signaturekind == 2) // 2 = Eigen tekst
                    salutationAndBody += "<BR><BR>" + signature;

                var newBody = preBodyString + salutationAndBody + postBodyString;
                mail.HTMLBody = newBody;
            }
            else
            {
                mail.HTMLBody = salutation + "<BR><BR>" + bodyWithSignature;
            }

            // Add default report
            mail.AddAttachment(fullFileName);

            switch (_mailAction)
            {
                case MailAction.Send:
                    if (_deferredDeliveryTime != null)
                    {
                        mail.DeferredDeliveryTime = _deferredDeliveryTime();
                    }

                    mail.Send();
                    break;
                case MailAction.SaveInDrafts:
                    mail.SaveAsDraft();
                    break;
                case MailAction.Display:
                    mail.Display();
                    break;
            }
        }
    }

    private int GetLanguageId(int relationId)
    {
        return GetRecordset("R_RELATION", "FK_LANGUAGE", $"PK_R_RELATION = {relationId}", "")
            .DataTable
            .AsEnumerable()
            .First()
            .Field<int>("FK_LANGUAGE");
    }

    private void CreatePaymentReminders(List<int> relationIds)
    {
        //1 - Niet aanmanen staat uit
        //2 - Vervaldatum voor vandaag
        //3 - Is betaald & Betaald staan uit
        //4 - Datum aanmaning is leeg
        //5 - Verkoopdagboek (filter memoriaal uit)

        var salesDaybooks = GetRecordset("R_DAYBOOK", "PK_R_DAYBOOK",
            "DAYBOOKTYPE = 2", "").DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_DAYBOOK")).ToList();
        salesDaybooks.Add(0); //Avoid empty list

        var rsSalesInvoicesRemindered = GetRecordset("R_SALESINVOICE",
            "PK_R_SALESINVOICE, DATEREMINDER",
            $"FK_INVOICERELATION IN ({string.Join(",", relationIds)}) AND NOTREMINDER = 0 AND PAYABLEDATE < '{DateTime.Now.Date:yyyyMMdd}' AND ISPAID = 0 AND U_BETAALD = 0 AND DATEREMINDER IS NULL AND FK_DAYBOOK IN ({string.Join(",", salesDaybooks)})",
            "");

        if (rsSalesInvoicesRemindered.RecordCount == 0)
        {
            return; //Zou vreemd zijn als er geen records gevonden worden
        }

        rsSalesInvoicesRemindered.MoveFirst();
        rsSalesInvoicesRemindered.UseDataChanges = false;
        rsSalesInvoicesRemindered.UpdateWhenMoveRecord = false;

        while(!rsSalesInvoicesRemindered.EOF)
        {
            rsSalesInvoicesRemindered.Fields["DATEREMINDER"].Value = DateTime.Now;
            rsSalesInvoicesRemindered.MoveNext();
        }

        rsSalesInvoicesRemindered.MoveFirst();
        var updateResult = rsSalesInvoicesRemindered.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show(
                $"Bijwerken verkoopfacturen is mislukt; oorzaak: {updateResult.First(x => x.HasError).GetResult()}",
                "Ridder iQ",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        var rsPaymentReminder = GetRecordset("C_PAYMENTREMINDER", "", "PK_C_PAYMENTREMINDER = -1", "");
        rsPaymentReminder.UseDataChanges = false;
        rsPaymentReminder.UpdateWhenMoveRecord = false;

        foreach (var salesInvoice in rsSalesInvoicesRemindered.DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_SALESINVOICE")))
        {
            rsPaymentReminder.AddNew();
            rsPaymentReminder.Fields["FK_SALESINVOICE"].Value = salesInvoice;
            rsPaymentReminder.Fields["DATEREMINDER"].Value = DateTime.Now;
            rsPaymentReminder.Fields["FK_CREATEDBY"].Value = GetCurrentEmployee(CurrentUserId);
        }

        var updateResult2 = rsPaymentReminder.Update2();

        if (updateResult2.Any(x => x.HasError))
        {
            MessageBox.Show(
                $"Het aanmaken van de aanmaningen is mislukt; oorzaak: {updateResult.First(x => x.HasError).GetResult()}",
                "Ridder iQ",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }

    private int? GetCurrentEmployee(int currentUserId)
    {
        var rsUser = GetRecordset("R_USER", "FK_EMPLOYEE",
            $"PK_R_USER = {currentUserId}", "");
        rsUser.MoveFirst();

        return rsUser.Fields["FK_EMPLOYEE"].Value as int?;
    }

    private IDictionary<string, object> GetCalculatedColumnOutput(ScriptRecordset recordset, string[] fields,
        string tableName, object recordId)
    {
        var result = new Dictionary<string, object>();
        foreach (var field in fields)
        {
            var fieldData = recordset.Fields[field].Value != DBNull.Value
                ? recordset.Fields[field].Value as byte[]
                : null;
            if (fieldData == null)
            {
                result[field] = null;
                continue;
            }

            result[field] = GetCalculatedColumnOutput(fieldData, tableName, recordId);
        }

        return result;
    }

    public enum MailAction
    {
        Send,
        SaveInDrafts,
        Display
    }

    internal class GenerateMailsResult
    {
        public GenerateMailsResult()
        {
            LogMessage = string.Empty;
        }

        public string LogMessage { get; set; }
    }
}
