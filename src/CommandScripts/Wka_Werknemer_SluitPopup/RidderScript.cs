﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class Wka_Werknemer_SluitPopup_RidderScript : CommandScript
{
    public void Execute()
    {
        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("U_FORMDETAILSCREATEWKAEMPLOYEE"))
        {
            MessageBox.Show("Dit script mag alleen vanaf Form Maak WKA werknemer aangeroepen worden.");
            return;
        }

        FormDataAwareFunctions.Save();

        var formId = (int)FormDataAwareFunctions.FormParent.CurrentRecord.GetPrimaryKeyValue();

        var rsForm = GetRecordset("U_FORMCREATEWKAEMPLOYEE", "", $"PK_U_FORMCREATEWKAEMPLOYEE = {formId}", "");
        rsForm.MoveFirst();

        if(string.IsNullOrEmpty(rsForm.GetField("FIRSTNAME").Value.ToString()) || string.IsNullOrEmpty(rsForm.GetField("LASTNAME").Value.ToString()) 
            || (string.IsNullOrEmpty(rsForm.GetField("REGNO").Value.ToString()) && string.IsNullOrEmpty(rsForm.GetField("BSN").Value.ToString()) && string.IsNullOrEmpty(rsForm.GetField("COCNUMBER").Value.ToString())))
        {
            MessageBox.Show("Vul de naam en het BSN, Registratienummer of KvK-nummer in.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        rsForm.SetFieldValue("PROCESS", true);
        rsForm.Update();

        Form.ActiveForm.Close();

    }
}
