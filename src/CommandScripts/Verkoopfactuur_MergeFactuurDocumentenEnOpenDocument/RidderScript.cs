﻿using ADODB;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;


public class Verkoopfactuur_MergeFactuurDocumentenEnOpenDocument_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //8-6-2023
        //Bij het mailen van de verkoopfactuur is het gewenst om de gekoppelde documenten te mergen tot één factuur. Daar is iTextSharp voor nodig.
        //In dit script mergen we de documenten en starten een mail

        if (FormDataAwareFunctions == null || FormDataAwareFunctions.TableName != "R_SALESINVOICE")
        {
            MessageBox.Show("Dit script mag alleen vanaf de verkoopfacturen-tabel aangeroepen worden.");
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }
            
        if (FormDataAwareFunctions.GetSelectedRecords().Length > 1)
        {
            MessageBox.Show("Dit script mag voor één factuur tegelijk aangeroepen worden.");
            return;
        }

        var salesInvoiceId = (int)FormDataAwareFunctions.GetSelectedRecords().First().GetPrimaryKeyValue();

        var salesInvoice = GetRecordset("R_SALESINVOICE", "FK_JOURNALENTRY, FK_REPORT, MANDAYREGISTERFROM, MANDAYREGISTERUNTILL, FK_WORKFLOWSTATE", 
            $"PK_R_SALESINVOICE = {salesInvoiceId}", "").DataTable.AsEnumerable().First();

        var journalEntryId = salesInvoice.Field<int?>("FK_JOURNALENTRY") ?? 0;

        var locatieDigitaleFacturatie = GetRecordset("R_CRMSETTINGS", "LOCATIETIJDELIJKDIGITALEFACTURATIE", "", "")
            .DataTable.AsEnumerable().First().Field<string>("LOCATIETIJDELIJKDIGITALEFACTURATIE");

        if (string.IsNullOrEmpty(locatieDigitaleFacturatie))
        {
            throw new Exception("Geen locatie Digitale facturatie gevonden in de CRM instellingen.");
        }

        if (journalEntryId == 0)
        {
            /*
            Guid wfGenereerFactuurNr = new Guid("b3fc040a-d468-4e08-9c7e-d33833848824");
            var result = ExecuteWorkflowEvent("R_SALESINVOICE", salesInvoiceId, wfGenereerFactuurNr, null);*/

            var result = EventsAndActions.Sales.Actions.GenerateReservedSalesJournalEntry(salesInvoiceId);

            if (result.HasError)
            {
                throw new Exception($"Het factuurnummer is niet te genereren om de volgende reden: {result.GetResult()}");
            }

            journalEntryId = GetRecordset("R_SALESINVOICE", "FK_JOURNALENTRY", 
                $"PK_R_SALESINVOICE = {salesInvoiceId}", "").DataTable.AsEnumerable().First().Field<int>("FK_JOURNALENTRY");
        }

        var journalEntryNumber = GetRecordset("R_JOURNALENTRY", "JOURNALENTRYNUMBER",
            $"PK_R_JOURNALENTRY = {journalEntryId}", "").DataTable.AsEnumerable().First().Field<int>("JOURNALENTRYNUMBER");

        //0) Controleer of map al bestaat, verwijder dan de map. 
        var directoryArchiveInvoiceDocuments = Path.Combine(locatieDigitaleFacturatie, salesInvoiceId.ToString());

        if (!Directory.Exists(directoryArchiveInvoiceDocuments))
        {
            Directory.CreateDirectory(directoryArchiveInvoiceDocuments);
        }

        Cursor.Current = Cursors.WaitCursor;
        try
        {
            var documentsToMerge = new List<string>();
            var databaseDocumentsCreated = new List<string>();

            var fileNameSalesInvoice = ExportSalesInvoice(salesInvoice, directoryArchiveInvoiceDocuments);
            documentsToMerge.Add(fileNameSalesInvoice);
            databaseDocumentsCreated.Add(fileNameSalesInvoice);

            var outputFileName = $"Factuur {journalEntryNumber}";

            if (MandayRegisterApplicable(salesInvoice))
            {
                var fileNameMandayRegister = ExportMandayRegister(salesInvoice, directoryArchiveInvoiceDocuments);
                documentsToMerge.Add(fileNameMandayRegister);
                databaseDocumentsCreated.Add(fileNameMandayRegister);
            }

            var documents = ArchiveLinkedDocuments(salesInvoiceId, directoryArchiveInvoiceDocuments, outputFileName, ref databaseDocumentsCreated);

            documentsToMerge.AddRange(documents);

            var totalFileNameOutputDocument = Path.Combine(directoryArchiveInvoiceDocuments, $"{outputFileName}.pdf");
            Merge(documentsToMerge, totalFileNameOutputDocument);

            DeleteDatabaseDocumentsCreated(databaseDocumentsCreated);

            System.Diagnostics.Process.Start(totalFileNameOutputDocument);
        }
        finally
        {
            Cursor.Current = Cursors.Default;
        }
    }

    private string ExportMandayRegister(DataRow salesInvoice, string directoryArchiveInvoiceDocuments)
    {
        var reportGuidMandayRegister = new Guid("2b791886-4fd7-447a-8b71-c8a443a5e735");
        Guid reportColumnID = Guid.Empty;

        var fileName = Path.Combine(directoryArchiveInvoiceDocuments, $"Mandagenregister {salesInvoice.Field<int>("PK_R_SALESINVOICE")}.pdf");

        var par = new Ridder.Client.SDK.ExportReportParameter();
        par.Id = salesInvoice.Field<int>("PK_R_SALESINVOICE");
        par.TableName = "R_SALESINVOICE";
        par.DesignerScope = Ridder.Client.SDK.SDKParameters.DesignerScope.User;
        par.Report = reportGuidMandayRegister;
        par.ReportColumnId = Guid.Empty;
        par.FileName = fileName;
        par.ExportType = Ridder.Client.SDK.SDKParameters.ExportType.Pdf;
        par.ImagesAsJpegForPDF = false;

        var result = ExportReportWithParameters(par, false);

        if (result.HasError)
        {
            throw new Exception($"Exporteren mandagenregister is mislukt, oorzaak: {result.GetResult()}");
        }

        return fileName;
    }

    private bool MandayRegisterApplicable(DataRow salesInvoice)
    {
        var wkaActive = GetRecordset("R_CRMSETTINGS", "WKAACTIVE", "", "")
            .DataTable.AsEnumerable().First().Field<bool>("WKAACTIVE");

        if (!wkaActive)
        {
            return false;
        }

        if (!salesInvoice.Field<DateTime?>("MANDAYREGISTERFROM").HasValue || !salesInvoice.Field<DateTime?>("MANDAYREGISTERUNTILL").HasValue)
        {
            return false;
        }

        return true;
    }

    private void DeleteDatabaseDocumentsCreated(List<string> databaseDocumentsCreated)
    {
        foreach (var document in databaseDocumentsCreated)
        {
            File.Delete(document);
        }
    }

    private List<string> ArchiveLinkedDocuments(int salesInvoiceId, string directoryArchiveInvoiceDocuments, string outputFileName, ref List<string> databaseDocumentsCreated)
    {
        var result = new List<string>();

        var documentIds = GetRecordset("R_SALESINVOICEDOCUMENT", "FK_DOCUMENT", string.Format("FK_SALESINVOICE = {0}", salesInvoiceId), "")
            .DataTable.AsEnumerable().Select(x => x.Field<int>("FK_DOCUMENT")).ToList();

        if (!documentIds.Any())
        {
            return new List<string>();
        }

        var documents = GetRecordset("R_DOCUMENT", "STORAGESYSTEM, DOCUMENTLOCATION, DOCUMENTDATA, DESCRIPTION",
            $"PK_R_DOCUMENT IN ({string.Join(",", documentIds)}) AND EXTENSION IN ('.pdf', '.PDF') AND DESCRIPTION <> '{outputFileName}'",
            "").DataTable.AsEnumerable().ToList();

        foreach (var document in documents)
        {
            if (document.Field<string>("STORAGESYSTEM").Equals("Verwijzing"))  //Document is als verwijzing opgeslagen
            {
                result.Add(document.Field<string>("DOCUMENTLOCATION"));
            }
            else  //Document is in de database opgeslagen
            {
                var documentData = document.Field<byte[]>("DOCUMENTDATA");

                if (documentData != null)
                {
                    var totalFileName = Path.Combine(directoryArchiveInvoiceDocuments, $"{document.Field<string>("DESCRIPTION")}.pdf");

                    try
                    {
                        File.WriteAllBytes(totalFileName, documentData);
                    }
                    catch (Exception e)
                    {
                        throw new Exception($"Wegschrijven documentdata om document te creëren is mislukt, oorzaak: {e}");
                    }

                    databaseDocumentsCreated.Add(totalFileName);
                    result.Add(totalFileName);
                }
            }
        }

        return result;
    }

    private string ExportSalesInvoice(DataRow salesInvoice, string directoryArchiveInvoiceDocuments)
    {
        var filterReportSubstitutes = salesInvoice.Field<int?>("FK_REPORT").HasValue
            ? string.Format("PK_R_REPORTSUBSTITUTE = {0}", salesInvoice.Field<int>("FK_REPORT"))
            : "FK_ORIGINALREPORT = 'e59c272e-7cc6-4f07-a7f6-fafab96014a3' AND DEFAULT = 1";

        var userReportGuid = GetRecordset("R_REPORTSUBSTITUTE", "FK_USERSUBSTITUTE", filterReportSubstitutes, "")
            .DataTable.AsEnumerable().First().Field<Guid>("FK_USERSUBSTITUTE");

        Guid reportColumnID = Guid.Empty;

        var fileName = Path.Combine(directoryArchiveInvoiceDocuments, $"FactuurLos {salesInvoice.Field<int>("PK_R_SALESINVOICE")}.pdf");

        var par = new Ridder.Client.SDK.ExportReportParameter();
        par.Id = salesInvoice.Field<int>("PK_R_SALESINVOICE");
        par.TableName = "R_SALESINVOICE";
        par.DesignerScope = Ridder.Client.SDK.SDKParameters.DesignerScope.User;
        par.Report = userReportGuid;
        par.ReportColumnId = Guid.Empty;
        par.FileName = fileName;
        par.ExportType = Ridder.Client.SDK.SDKParameters.ExportType.Pdf;
        par.ImagesAsJpegForPDF = false;

        var result = ExportReportWithParameters(par, false);

        if (result.HasError)
        {
            throw new Exception($"Exporteren verkoopfactuur is mislukt, oorzaak: {result.GetResult()}");
        }

        return fileName;
    }

    public void Merge(List<string> InFiles, String OutFile)
    {
        using (FileStream stream = new FileStream(OutFile, FileMode.Create))
        using (Document doc = new Document())
        using (PdfCopy pdf = new PdfCopy(doc, stream))
        {
            doc.Open();

            PdfReader reader = null;
            PdfImportedPage page = null;

            foreach (var file in InFiles)
            {
                reader = new PdfReader(file);

                for (int i = 0; i < reader.NumberOfPages; i++)
                {
                    page = pdf.GetImportedPage(reader, i + 1);
                    pdf.AddPage(page);
                }

                pdf.FreeReader(reader);
                reader.Close();
            }
        }
    }
}
