﻿using ADODB;
using Newtonsoft.Json;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class SendBisharpWelcomeMailByBrevo_RidderScript : CommandScript
{
    private const string BrevoApiKey = "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";

    public void Execute()
    {
        //SP
        //19-10-2022
        //Verstuur Bisharp welkomsmail via MailerSend API

        //RK
        //15-01-2024
        //Ombouwen gebruik Brevo API inplaats van MailerSend API
        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("U_BISHARPPORTALUSER", StringComparison.OrdinalIgnoreCase))
        {
            MessageBox.Show("Dit script mag alleen van de Bisharp portal user tabel aangeroepen worden.");
            return;
        }


        IRecord[] records = FormDataAwareFunctions.GetSelectedRecords();
        if (records.Length > 1)
        {
            MessageBox.Show("Dit script is voor één Bisharp portal user tegelijk aan te roepen.", "Controle", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        int id = (int)records[0].GetPrimaryKeyValue();

        var user = GetRecordset("U_BISHARPPORTALUSER", "FK_CONTACT, EMAIL, WELCOMEMAILSENT",
            $"PK_U_BISHARPPORTALUSER = {id}", "")
            .DataTable.AsEnumerable().Select(x => new User()
            {
                ContactId = x.Field<int?>("FK_CONTACT"),
                Email = x.Field<string>("EMAIL"),
                WelcomeMailSent = x.Field<bool>("WELCOMEMAILSENT")

            }).First();

        if (user.WelcomeMailSent == true)
        {
            MessageBox.Show("Welkomsmail is al verstuurd. Zet het vinkje 'Welkomsmail verstuurd' uit om opnieuw te kunnen sturen.",
                "Bisharp Welkomsmail", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        if (!user.ContactId.HasValue)
        {
            MessageBox.Show("Geen contactpersoon gevonden in Bisharp portaluser.",
                "Bisharp Welkomsmail", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        var personId = GetRecordset("R_CONTACT", "FK_PERSON",
            $"PK_R_CONTACT = {user.ContactId}", "").DataTable.AsEnumerable().First().Field<int>("FK_PERSON");

        var person = GetRecordset("R_PERSON", "FIRSTNAME, NAMEPREFIX, LASTNAME",
            $"PK_R_PERSON = {personId}", "")
            .DataTable.AsEnumerable().Select(x => new Person()
            {
                FirstName = x.Field<string>("FIRSTNAME"),
                NamePrefix = x.Field<string>("NAMEPREFIX"),
                LastName = x.Field<string>("LASTNAME"),

            }).First();

        var errorMessage = string.Empty;

        SendMailRequestModel requestModel = CreateRequestModel(user, person, ref errorMessage);

        if (requestModel == null)
        {
            MessageBox.Show($"Versturen welkomsmail geannuleerd. Opbouwen post object, mislukt. Oorzaak: {errorMessage}.");
        }

        SendMailRequest(requestModel, ref errorMessage);
        if (!string.IsNullOrEmpty(errorMessage))
        {
            MessageBox.Show($"Versturen welkomsmail is mislukt. Oorzaak: {errorMessage}.");
            return;
        }

        var rsUser = GetRecordset("U_BISHARPPORTALUSER", "FK_CONTACT, EMAIL, WELCOMEMAILSENT",
            $"PK_U_BISHARPPORTALUSER = {id}", "");
        rsUser.MoveFirst();
        rsUser.SetFieldValue("WELCOMEMAILSENT", true);
        rsUser.Update();

        MessageBox.Show("Welkomstmail is verstuurd!", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
    }

    private SendMailRequestModel CreateRequestModel(User user, Person person, ref string errorMessage)
    {
        return new SendMailRequestModel
        {
            Sender = new Sender() { },

            To = new Recipient[]
            {
                new Recipient()
                {
                    Email = user.Email,
                    Name = person.TotalName
                }
            },

            TemplateId = 11,
            TemplateParams = new Dictionary<string, string>()
            {
                { "firstname", person.FirstName },
                { "urlsetpassword", $"biportal.bisharp.nl/Register?email={user.Email}" }
            }
        };
    }
    private void SendMailRequest(SendMailRequestModel requestModel, ref string errorMessage)
    {
        HttpClient client = new HttpClient();

        client.DefaultRequestHeaders.Add("api-key", BrevoApiKey);

        HttpResponseMessage response;

        try
        {
            response = client.PostAsJsonAsync("https://api.brevo.com/v3/smtp/email", requestModel).Result;

            if (!response.IsSuccessStatusCode && response.StatusCode != System.Net.HttpStatusCode.Accepted && response.StatusCode != System.Net.HttpStatusCode.Created)
            {
                var message = response.Content.ReadAsStringAsync().Result;
                errorMessage = $"Verbinden Brevo API is mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase} - {message}";
            }
        }
        catch (Exception e)
        {
            errorMessage = $"Brevo API niet bereikbaar, error: {e}";
        }
    }
}

class User
{
    public int? ContactId { get; set; }
    public string Email { get; set; }
    public bool WelcomeMailSent { get; set; }
}

public class Person
{
    public string FirstName { get; set; }
    public string NamePrefix { get; set; }
    public string LastName { get; set; }
    public string TotalName => string.IsNullOrEmpty(NamePrefix)
        ? $"{FirstName} {LastName}"
        : $"{FirstName} {NamePrefix} {LastName}";
}

public class SendMailRequestModel
{
    [JsonProperty("sender")]
    public Sender Sender { get; set; }

    [JsonProperty("to")]
    public Recipient[] To { get; set; }

    [JsonProperty("cc")]
    public Recipient[] CC { get; set; }

    [JsonProperty("attachment")]
    public Attachment[] Attachments { get; set; }

    [JsonProperty("templateId")]
    public int TemplateId { get; set; }

    [JsonProperty("params")]
    public Dictionary<string, string> TemplateParams { get; set; }
}

public class Sender
{
    [JsonProperty("email")]
    public string Email { get; set; } = "no-reply@bisharp.nl";

    [JsonProperty("name")]
    public string Name { get; set; } = "Bisharp";
}

public class Recipient
{
    [JsonProperty("email")]
    public string Email { get; set; }

    [JsonProperty("name")]
    public string Name { get; set; }
}

public class Attachment
{
    [JsonProperty("content")]
    public string Content { get; set; }

    [JsonProperty("name")]
    public string Filename { get; set; }
}

