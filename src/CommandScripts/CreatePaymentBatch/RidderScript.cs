﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Drawing;
using Ridder.Client.SDK.SDKParameters;

public class CreatePaymentBatch_RidderScript : CommandScript
{
    public void Execute()
    {
        //SP
        //9-2-2023
        //Genereer nieuwe betalingbatch

        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("R_PURCHASEINVOICE", StringComparison.OrdinalIgnoreCase))
        {
            MessageBox.Show("Dit script werkt alleen op de tabel 'Inkoopfacturen'.", "Ridder iQ", MessageBoxButtons.OK,
               MessageBoxIcon.Error);
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        //Voeg alle geselecteerde records toe aan de betalingsbatch
        var purchaseInvoiceIds = FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue()).ToList();

        //Check of er inkoopfacturen in de selectie zitten die al aan een betalingsbatch zijn gekoppeld
        var purchaseInvoiceIdsAlreadyInBatch = GetRecordset("R_PURCHASEINVOICE", "",
               $"PK_R_PURCHASEINVOICE IN ({string.Join(", ", purchaseInvoiceIds)}) AND FK_PAYMENTBATCH IS NOT NULL", "")
            .DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_PURCHASEINVOICE")).ToList();

        if(purchaseInvoiceIdsAlreadyInBatch.Any())
        {
            throw new Exception($"Aanmaken nieuwe betalingsbatch is mislukt, oorzaak: één of meerdere inkoopfacturen zijn al aan een betalingsbatch gekoppeld. " +
                $"Controleer inkoopfactuur id(s) {string.Join(", ", purchaseInvoiceIdsAlreadyInBatch)}.");
        }

        //Maak een nieuwe betalingsbatch
        int paymentBatchId = CreateNewPaymentBatch();

        foreach (var purchaseInvoiceId in purchaseInvoiceIds)
        {
            var rsPurchaseInvoice = GetRecordset("R_PURCHASEINVOICE", "",
               $"PK_R_PURCHASEINVOICE = {purchaseInvoiceId}", "");
            rsPurchaseInvoice.MoveFirst();
            rsPurchaseInvoice.UseDataChanges = false;
            rsPurchaseInvoice.UpdateWhenMoveRecord = false;

            if ((rsPurchaseInvoice.Fields["FK_PAYMENTBATCH"].Value as int?).HasValue)
            {
                throw new Exception($"Aanmaken nieuwe betalingsbatch is mislukt, oorzaak: inkoopfactuur met id {rsPurchaseInvoice.Fields["PK_R_PURCHASEINVOICE"].Value} is al gebruikt " +
                    $"bij betalingsbatch met id {rsPurchaseInvoice.Fields["FK_PAYMENTBATCH"].Value}");
            }

            rsPurchaseInvoice.SetFieldValue("FK_PAYMENTBATCH", paymentBatchId);

            var updateResult = rsPurchaseInvoice.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                throw new Exception($"Aanmaken nieuwe betalingsbatch is mislukt, oorzaak {updateResult.First(x => x.HasError).GetResult()}");
            }
        }

        //Open de aangemaakte betalingsbatch
        var editParameters = new EditParameters()
        {
            TableName = "U_PAYMENTBATCH",
            Id = paymentBatchId,
        };

        OpenEdit(editParameters, false);

        //TODO: Dit is in de nieuwste update omgevallen. Als we een patch hebben geïnstalleerd dan weer aanzetten
        /* Dit is in de nieuwste update omgevallen. 
        //Verstuur de mail 
        var wfMail = new Guid("e29eb74b-c83c-4dea-9e11-ebc82cab1c71");

        var workflowResult = ExecuteWorkflowEvent("U_PAYMENTBATCH", paymentBatchId, wfMail, null);

        if (workflowResult.HasError)
        {
            throw new Exception($"Automatisch mailen bedrijfsleider(s) is mislukt; oorzaak: {workflowResult.GetResult()}");
        }*/
    }

    private int CreateNewPaymentBatch()
    {
        var rsPaymentBatch = GetRecordset("U_PAYMENTBATCH", "",
            $"PK_U_PAYMENTBATCH = -1", "");
        rsPaymentBatch.UseDataChanges = false;
        rsPaymentBatch.UpdateWhenMoveRecord = false;

        rsPaymentBatch.AddNew();
        rsPaymentBatch.SetFieldValue("BATCHDATECREATED", DateTime.Now);
        rsPaymentBatch.SetFieldValue("BATCHCREATEDBY", GetUserInfo().CurrentUserName);

        var updateResult = rsPaymentBatch.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            throw new Exception($"Aanmaken nieuwe betalingsbatch is mislukt, oorzaak {updateResult.First(x => x.HasError).GetResult()}");
        }

        return (int)updateResult.First().PrimaryKey;
    }
}
