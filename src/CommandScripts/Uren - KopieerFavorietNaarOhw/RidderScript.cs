﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Drawing;

public class Uren___KopieerFavorietNaarOhw_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //03-09-2019
        //Kopieer favoriet naar ohw artikel

        if (FormDataAwareFunctions == null || FormDataAwareFunctions.TableName != "C_TEMPUSEDITEMSPERUSER")
        {
            MessageBox.Show(
                "Dit script mag alleen vanaf de favorieten verbruik artikel per bewerking-tabel aangeroepen worden.");
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        var projectTimeId = (int)FormDataAwareFunctions.FormParent.CurrentRecord.GetPrimaryKeyValue();

        var selectedItems = FormDataAwareFunctions.GetSelectedRecords()
            .Select(x => (int)x.GetCurrentRecordValue("FK_ITEM"))
            .ToList();

        if (!CheckExpensesUsedOneTimePerDay(selectedItems, projectTimeId))
        {
            return;
        }

        var rsWipDetailItem = GetRecordset("R_ORDERWIPDETAILITEM", "", "PK_R_ORDERWIPDETAILITEM = -1", "");
        rsWipDetailItem.UseDataChanges = true;
        rsWipDetailItem.UpdateWhenMoveRecord = false;

        foreach (var selectedItem in selectedItems)
        {
            rsWipDetailItem.AddNew();

            rsWipDetailItem.Fields["FK_PROJECTTIME"].Value = projectTimeId;
            rsWipDetailItem.Fields["FK_ITEM"].Value = selectedItem;
            rsWipDetailItem.Fields["QUANTITY"].Value = 1.0;
        }

        rsWipDetailItem.Update();

        FormDataAwareFunctions.FormParent.Refres();
    }

    private bool CheckExpensesUsedOneTimePerDay(List<int> selectedItems, int projectTimeId)
    {
        var rsItemGroupExpenses = GetRecordset("R_ITEMGROUP", "PK_R_ITEMGROUP",
            "CODE = '8000'", "");

        if(rsItemGroupExpenses.RecordCount == 0)
        {
            return true;
        }

        var itemsExpenses = GetRecordset("R_ITEM", "PK_R_ITEM",
                $"PK_R_ITEM IN ({string.Join(",", selectedItems)}) AND FK_ITEMGROUP = {rsItemGroupExpenses.DataTable.AsEnumerable().First().Field<int>("PK_R_ITEMGROUP")}",
                "").DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_ITEM")).ToList();

        if (!itemsExpenses.Any())
        {
            return true;
        }

        var projectTime = GetRecordset("R_PROJECTTIME", "FK_EMPLOYEE, DATE",
            $"PK_R_PROJECTTIME = {projectTimeId}", "").DataTable.AsEnumerable().First();

        var date = projectTime.Field<DateTime>("DATE");
        var allProjectTimesThisEmployeeThisDate = GetRecordset("R_PROJECTTIME", "PK_R_PROJECTTIME",
            $"FK_EMPLOYEE = {projectTime.Field<int>("FK_EMPLOYEE")} AND DATE >= '{date:yyyyMMdd}' AND DATE < '{date.AddDays(1):yyyyMMdd}'",
            "").DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_PROJECTTIME")).ToList();

        if (!allProjectTimesThisEmployeeThisDate.Any())
        {
            return true;
        }

        var wipDetailsExpenses = GetRecordset("R_ORDERWIPDETAILITEM", "PK_R_ORDERWIPDETAILITEM, FK_ITEM",
            $"FK_PROJECTTIME IN ({string.Join(",", allProjectTimesThisEmployeeThisDate)}) AND FK_ITEM IN ({string.Join(",", itemsExpenses)})",
            "").DataTable.AsEnumerable();

        if (!wipDetailsExpenses.Any())
        {
            return true;
        }

        MessageBox.Show(
            $"Het is niet mogelijk om een onkostenvergoeding meerdere keren op één dag te boeken. Artikel {GetRecordTag("R_ITEM", wipDetailsExpenses.First().Field<int>("FK_ITEM"))} is al geboekt.",
            "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
        return false;
    }
}
