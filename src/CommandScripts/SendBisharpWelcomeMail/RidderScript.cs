﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.Net.Http;
using System.Net.Http.Headers;
using Bill.Porter.Helpers.MailerSend.Models;
using Newtonsoft.Json;


public class SendBisharpWelcomeMail_RidderScript : CommandScript
{
    private const string UrlMailerSend = "https://api.mailersend.com/v1";
    private const string MailerSendToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMGRkYWQ1ODdjNjZjMjY1ZTYwNDNhZDY5N2UwZWZkMjhhYmFlYzMwNmYzNmMyYWMyNWI5Nzk2YjY4MGM1MDUwZGY5NjExM2M2MGIxNGMxMWYiLCJpYXQiOjE2NTEyMTk0NjUuNTAzODEsIm5iZiI6MTY1MTIxOTQ2NS41MDM4MTIsImV4cCI6NDgwNjg5MzA2NS40OTk0NDUsInN1YiI6IjE4MzM3Iiwic2NvcGVzIjpbImVtYWlsX2Z1bGwiLCJkb21haW5zX2Z1bGwiLCJhY3Rpdml0eV9mdWxsIiwiYW5hbHl0aWNzX2Z1bGwiLCJ0b2tlbnNfZnVsbCIsIndlYmhvb2tzX2Z1bGwiLCJ0ZW1wbGF0ZXNfZnVsbCIsInN1cHByZXNzaW9uc19mdWxsIiwic21zX2Z1bGwiXX0.fNEz3tuAGJhlDMDAqM4gOAvWn2jnm0m2aoWFhRK_M4rvY79An04PbMUo-wq4VaGQgP23mHfQwzyzBgBUOwyjcEwKSzpT-REuPT3yxqtO-bPpk6fjcEw0VSmL8zgMj2Vxl7zkamgYKTGKV2WL-VZU4RIKfnynhOCQN4UB9-e17JtI3r11KPGEoFLaSz3NaUnwCmr7rIR8Qz4yvOr9x6jEonVBtaqk7bz0R7vRO3VkKmUbHCLkdvNeGbRsXSqxA7iP9RCQkXr2gWIzHIQytzcu7axoJx-o-kJSnbDn8c272-5_UmNqoVsjo6tr4yscF3g23YXtBShX6s417Be6u79kMeYWhUnHggFLU0wMdYaNzP_bEAeFMXRj4Iy6KW-ErqSr-IHosuRYkkfpL80xSS6xUzXPVZDO-XyABXfvDInwMRbP2jNiwkLj0CSHzdJPca2hqP83vQOPPFAAmEQtHK7a4LuGGjWiHhqbNpRaSSbDBx-SHZa3_BwtbM4CUn4OC4A-IJ4nZIHuk_xxQRdOUWtwAqOKXxf4bG0dtHJGxVy1aG_qRxhVpBDPtRNURfs0fym0aksG9RTDjLPvMdQFijeSB8ZzpNh3BN4os3m3awikIz941ioVo0N88id1GYajbfpB_P3AULpYuUwrJdDDh-SjeSPIrhqnBzoNf9U2d4pKfMo";
    private MailadressInfo _defaultFromMail = new MailadressInfo("Bisharp", "no-reply@bisharp.nl");
    private const string MailTemplate = "ynrw7gym3jng2k8e";
    private const string UrlBisharpPortal = "biportal.bisharp.nl";

    public void Execute()
    {
        //SP
        //19-10-2022
        //Verstuur Bisharp welkomsmail via MailerSend API

        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("U_BISHARPPORTALUSER", StringComparison.OrdinalIgnoreCase))
        {
            MessageBox.Show("Dit script mag alleen van de Bisharp portal user tabel aangeroepen worden.");
            return;
        }

        IRecord[] records = FormDataAwareFunctions.GetSelectedRecords();

        if (records.Length > 1)
        {
            MessageBox.Show("Dit script is voor één Bisharp portal user tegelijk aan te roepen.", "Controle", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        int id = (int)records[0].GetPrimaryKeyValue();

        var user = GetRecordset("U_BISHARPPORTALUSER", "FK_CONTACT, EMAIL, WELCOMEMAILSENT",
            $"PK_U_BISHARPPORTALUSER = {id}", "")
            .DataTable.AsEnumerable().Select(x => new User()
            {
                ContactId = x.Field<int?>("FK_CONTACT"),
                Email = x.Field<string>("EMAIL"),
                WelcomeMailSent = x.Field<bool>("WELCOMEMAILSENT")

            }).First();

        if(user.WelcomeMailSent == true)
        {
            MessageBox.Show("Welkomsmail is al verstuurd. Zet het vinkje 'Welkomsmail verstuurd' uit om opnieuw te kunnen sturen.", 
                "Bisharp Welkomsmail", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        if(!user.ContactId.HasValue)
        {
            MessageBox.Show("Geen contactpersoon gevonden in Bisharp portaluser.",
                "Bisharp Welkomsmail", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        var personId = GetRecordset("R_CONTACT", "FK_PERSON",
            $"PK_R_CONTACT = {user.ContactId}", "").DataTable.AsEnumerable().First().Field<int>("FK_PERSON");

        var person = GetRecordset("R_PERSON", "FIRSTNAME, NAMEPREFIX, LASTNAME",
            $"PK_R_PERSON = {personId}", "")
            .DataTable.AsEnumerable().Select(x => new Person()
            {
                FirstName = x.Field<string>("FIRSTNAME"),
                NamePrefix = x.Field<string>("NAMEPREFIX"),
                LastName = x.Field<string>("LASTNAME"),

            }).First();

        var errorMessage = string.Empty;

        var mailerSendPostEmail = CreatePostSendWelcomeMail(user, person, ref errorMessage);

        if (mailerSendPostEmail == null)
        {
            MessageBox.Show($"Versturen welkomsmail geannuleerd. Opbouwen post object, mislukt. Oorzaak: {errorMessage}.");
        }

        SendMailCore(mailerSendPostEmail, ref errorMessage);

        if (!string.IsNullOrEmpty(errorMessage))
        {
            MessageBox.Show($"Versturen welkomsmail is mislukt. Oorzaak: {errorMessage}.");
            return;
        }

        var rsUser = GetRecordset("U_BISHARPPORTALUSER", "FK_CONTACT, EMAIL, WELCOMEMAILSENT",
            $"PK_U_BISHARPPORTALUSER = {id}", "");
        rsUser.MoveFirst();
        rsUser.SetFieldValue("WELCOMEMAILSENT", true);
        rsUser.Update();


        MessageBox.Show("Welkomstmail is verstuurd!", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
    }



    private PostSendEmail CreatePostSendWelcomeMail(User user, Person person, ref string errorMessage)
    {
        var receiptInfo = new MailadressInfo(person.TotalName, user.Email);

        var variableInfo = new Variable[1]
         {
            new Variable()
            {
                Email = receiptInfo.Email,
                Substitutions = new Substitution[]
                {
                    new Substitution("firstname", person.FirstName),
                    new Substitution("urlRegisterPortalUser", $"{UrlBisharpPortal}/Register?email={receiptInfo.Email}")
                }
            }
         };

        return new PostSendEmail()
        {
            From = _defaultFromMail,
            To = new MailadressInfo[1] { receiptInfo },
            Subject = $"Welkom bij Bisharp!",
            TemplateId = MailTemplate,
            Variables = variableInfo,
        };
    }

    private void SendMailCore(PostSendEmail mailerSendPostEmail, ref string errorMessage)
    {
        HttpClientHandler clientHandler = new HttpClientHandler();
        clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

        HttpClient client = new HttpClient(clientHandler);
        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", MailerSendToken);

        var urlSendMail = $"{UrlMailerSend}/email";

        HttpResponseMessage response;

        try
        {
            response = client.PostAsJsonAsync(urlSendMail, mailerSendPostEmail).GetAwaiter().GetResult();
        }
        catch (Exception e)
        {
            errorMessage = $"MailerSend API niet bereikbaar, error: {e}";
            return;
        }

        if (!response.IsSuccessStatusCode || response.StatusCode != System.Net.HttpStatusCode.Accepted)
        {
            var message = response.Content.ReadAsStringAsync().Result;

            errorMessage = $"Verbinden MailerSend API is mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase} - {message} (URL: {urlSendMail})";
            return;
        }
    }
}

class User
{
    public int? ContactId { get; set; }
    public string Email { get; set; }
    public bool WelcomeMailSent { get; set; }
}

public class Person
{
    public string FirstName { get; set; }
    public string NamePrefix { get; set; }
    public string LastName { get; set; }
    public string TotalName => string.IsNullOrEmpty(NamePrefix)
        ? $"{FirstName} {LastName}"
        : $"{FirstName} {NamePrefix} {LastName}";
}


