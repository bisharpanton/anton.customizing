﻿using Newtonsoft.Json;

namespace Bill.Porter.Helpers.MailerSend.Models
{
    public class Substitution
    {
        public Substitution(string variable, string value)
        {
            Var = variable;
            Value = value;
        }

        [JsonProperty("var")]
        public string Var { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}

