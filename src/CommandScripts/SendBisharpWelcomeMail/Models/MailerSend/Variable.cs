﻿using Newtonsoft.Json;

namespace Bill.Porter.Helpers.MailerSend.Models
{
    public class Variable
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("substitutions")]
        public Substitution[] Substitutions { get; set; }
    }
}
