﻿using Newtonsoft.Json;

namespace Bill.Porter.Helpers.MailerSend.Models
{
    public class AttachmentInfo
    {
        [JsonProperty("content")]
        public string Content { get; set; }

        [JsonProperty("filename")]
        public string FileName { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
