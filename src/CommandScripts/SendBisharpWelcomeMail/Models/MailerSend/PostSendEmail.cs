﻿using Newtonsoft.Json;

namespace Bill.Porter.Helpers.MailerSend.Models
{
    public class PostSendEmail
    {
        [JsonProperty("from")]
        public MailadressInfo From { get; set; }
        [JsonProperty("to")]
        public MailadressInfo[] To { get; set; }
        [JsonProperty("subject")]
        public string Subject { get; set; }

        /*
        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("html")]
        public string Html { get; set; }

        [JsonProperty("attachments")]
        public AttachmentInfo[] Attachments { get; set; }*/

        [JsonProperty("template_id")]
        public string TemplateId { get; set; }

        [JsonProperty("variables")]
        public Variable[] Variables { get; set; }

    }
}
