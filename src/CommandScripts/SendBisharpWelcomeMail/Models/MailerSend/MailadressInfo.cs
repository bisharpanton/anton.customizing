﻿using Newtonsoft.Json;

namespace Bill.Porter.Helpers.MailerSend.Models
{
    public class MailadressInfo
    {
        public MailadressInfo(string name, string email)
        {
            Name = name;
            Email = email;
        }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}

