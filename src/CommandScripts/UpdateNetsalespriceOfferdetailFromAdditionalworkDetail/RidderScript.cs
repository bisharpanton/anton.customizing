﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class UpdateNetsalespriceOfferdetailFromAdditionalworkDetail_RidderScript : CommandScript
{
    public void Execute()
    {
        //SP
        //7-7-2022
        //Genereer bisharp tables to transfer voor een relatie

        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("U_ADDITIONALWORKDETAILS", StringComparison.OrdinalIgnoreCase))
        {
            MessageBox.Show("Dit script mag alleen van de meerwerk details tabel aangeroepen worden.");
        }

        IRecord[] records = FormDataAwareFunctions.GetSelectedRecords();

        if (records.Length > 1)
        {
            MessageBox.Show("Dit script is voor één meerwerk detail tegelijk aan te roepen.", "Controle", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        if (records.Length == 0)
        {
            MessageBox.Show("Selecteer een regel.", "Controle", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        int id = (int)records[0].GetPrimaryKeyValue();


        var offerdetailMiscId = GetRecordset("U_ADDITIONALWORKDETAILS", "FK_OFFERDETAILMISC",
            $"PK_U_ADDITIONALWORKDETAILS = {id}", "").DataTable.AsEnumerable().First().Field<int>("FK_OFFERDETAILMISC");

        var offerdetailMisc = GetRecordset("R_OFFERDETAILMISC", "PERCENTAGEBOUWPLAATS, PERCENTAGEPROJECTTEAM, PERCENTAGEOPSLAG",
            $"PK_R_OFFERDETAILMISC = {offerdetailMiscId}", "")
            .DataTable.AsEnumerable().Select(x => new OfferdetailMisc()
            {
                PercBouwplaats = x.Field<double>("PERCENTAGEBOUWPLAATS"),
                PercProject = x.Field<double>("PERCENTAGEPROJECTTEAM"),
                PercOplag = x.Field<double>("PERCENTAGEOPSLAG"),

            }).First();

        var subTotal = GetRecordset("U_ADDITIONALWORKDETAILS", "NETSALESPRICE",
            $"FK_OFFERDETAILMISC = {offerdetailMiscId}", "").DataTable.AsEnumerable().Sum(x => x.Field<double>("NETSALESPRICE"));

        var subTotalBouwplaats = subTotal + (subTotal * offerdetailMisc.PercBouwplaats);
        var subTotalProject = subTotalBouwplaats + (subTotalBouwplaats * offerdetailMisc.PercProject);
        var endTotal = subTotalProject + (subTotalProject * offerdetailMisc.PercOplag);

        var rsOfferdetailMisc = GetRecordset("R_OFFERDETAILMISC", "NETSALESAMOUNT",
            $"PK_R_OFFERDETAILMISC = {offerdetailMiscId}", "");
        rsOfferdetailMisc.MoveFirst();
        rsOfferdetailMisc.SetFieldValue("NETSALESAMOUNT", endTotal);

        var updateResult = rsOfferdetailMisc.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show("Error", $"Doorvoeren prijs naar offerteregel mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");
        }

    }

    class OfferdetailMisc
    {
        public double PercBouwplaats { get; set; }
        public double PercProject { get; set; }
        public double PercOplag { get; set; }

    }
}
