﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Drawing;

public class CreditSafe_SelectFormDetail_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //29-4-2020
        //Zet vinkje 'Select' aan bij geselecteerde form detail

        if (FormDataAwareFunctions == null || FormDataAwareFunctions.TableName != "C_FORMSEARCHRELATIONONCREDITSAFEDETAILS")
        {
            MessageBox.Show("Dit script mag alleen vanaf het formulier CreditSafe zoek relaties aangeroepen worden.");
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        if (FormDataAwareFunctions.GetSelectedRecords().Length > 1)
        {
            MessageBox.Show("Selecteer één zoekresultaat aub.", "Ridder iQ",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        var formId = (int)FormDataAwareFunctions.FormParent.CurrentRecord.GetPrimaryKeyValue();
        var currentSelectedFormDetailId = (int)FormDataAwareFunctions.GetSelectedRecords().First().GetPrimaryKeyValue();

        var rsFormDetail = GetRecordset("C_FORMSEARCHRELATIONONCREDITSAFEDETAILS", "SELECT",
            $"PK_C_FORMSEARCHRELATIONONCREDITSAFEDETAILS = {currentSelectedFormDetailId}", "");
        rsFormDetail.MoveFirst();
        rsFormDetail.Fields["SELECT"].Value = !(bool)rsFormDetail.Fields["SELECT"].Value;
        rsFormDetail.Update();

        FormDataAwareFunctions.Refres();
    }
}
