﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Drawing;
using Ridder.Client;
using Ridder.Common.BlockSystem;

public class RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //27-10-2016
        //Script om pop-up 'Invoeren termijnschema' te sluiten

        var ids = FormDataAwareFunctions.GetAllFilteredRecords().Select(x => x.GetPrimaryKeyValue()).ToList();
        this.FormDataAwareFunctions.Save();

        string username = this.GetUserInfo().CurrentUserName;

      //  ScriptRecordset rsInvoerTermijnschemaregels = this.GetRecordset("C_DUMMYINVOERTERMIJNSCHEMA", "",
      //  string.Format("FK_OFFER IS NULL AND FK_PURCHASEORDER IS NULL AND FK_PURCHASECONTRACT IS NULL AND CREATOR = '{0}'", username), "");

        var rsInvoerTermijnschemaregels = this.GetRecordset("C_DUMMYINVOERTERMIJNSCHEMA", "",
            $"PK_C_DUMMYINVOERTERMIJNSCHEMA IN ({string.Join(",", ids)})", "");
        rsInvoerTermijnschemaregels.MoveFirst();
        double totaalPercentage = rsInvoerTermijnschemaregels.DataTable.AsEnumerable().Sum(x => (double)x["PERCENTAGE"]);

        if (totaalPercentage < 0.999 || totaalPercentage > 1.001)
        {
            MessageBox.Show("Het totaal van de termijnen is niet 100%.", "Controle totaal termijnpercentage", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        //Bepaal omschrijving termijncode
        string omschrijving = "";
        while (!rsInvoerTermijnschemaregels.EOF)
        {
            double percentage = (double)rsInvoerTermijnschemaregels.Fields["PERCENTAGE"].Value;
            omschrijving += (omschrijving == "") ? percentage.ToString("0.00%") + "%" : " + " + percentage.ToString("0.00%") + "%";

            rsInvoerTermijnschemaregels.MoveNext();
        }

        //Aanmaken nieuwe termijncode
        ScriptRecordset rsTermijncode = this.GetRecordset("R_INVOICESCHEDULE", "", "PK_R_INVOICESCHEDULE = -1", "");
        rsTermijncode.AddNew();
        rsTermijncode.Fields["DESCRIPTION"].Value = (omschrijving.Length > 80) ? omschrijving.Substring(0, 80) : omschrijving;
        rsTermijncode.Update();

        //Verwerking termijnregels naar nieuwe termijncode
        ScriptRecordset rsTermijncoderegels = this.GetRecordset("R_INVOICESCHEDULEDETAIL", "", "PK_R_INVOICESCHEDULEDETAIL = -1", "");
        rsTermijncoderegels.UpdateWhenMoveRecord = false;

        rsInvoerTermijnschemaregels.MoveFirst();
        while (!rsInvoerTermijnschemaregels.EOF)
        {
            rsTermijncoderegels.AddNew();
            rsTermijncoderegels.Fields["FK_INVOICESCHEDULE"].Value = rsTermijncode.Fields["PK_R_INVOICESCHEDULE"].Value;
            rsTermijncoderegels.Fields["SEQUENCENUMBER"].Value = rsInvoerTermijnschemaregels.Fields["SEQUENCENUMBER"].Value;
            rsTermijncoderegels.Fields["DESCRIPTION"].Value = rsInvoerTermijnschemaregels.Fields["DESCRIPTION"].Value;
            rsTermijncoderegels.Fields["PERCENTAGE"].Value = rsInvoerTermijnschemaregels.Fields["PERCENTAGE"].Value;
            rsTermijncoderegels.Fields["FK_PAYMENTTERM"].Value = rsInvoerTermijnschemaregels.Fields["FK_PAYMENTTERM"].Value;

            rsInvoerTermijnschemaregels.MoveNext();
        }

        if (rsInvoerTermijnschemaregels.RecordCount > 0)
        {
            rsTermijncoderegels.MoveFirst();
            rsTermijncoderegels.Update();
        }

        //Gooi dummy invoertabel leeg
        rsInvoerTermijnschemaregels.MoveFirst();
        rsInvoerTermijnschemaregels.UpdateWhenMoveRecord = false;
        while (!rsInvoerTermijnschemaregels.EOF)
        {
            rsInvoerTermijnschemaregels.Delete();
            rsInvoerTermijnschemaregels.MoveNext();
        }

        if (rsInvoerTermijnschemaregels.RecordCount > 0)
        {
            rsInvoerTermijnschemaregels.MoveFirst();
            rsInvoerTermijnschemaregels.Update();
        }

        //Activeer gecreërde termijnschema
        var result = this.ExecuteAction("InvoiceScheduleActivatorAction", null, "R_INVOICESCHEDULE", (int)rsTermijncode.Fields["PK_R_INVOICESCHEDULE"].Value, null);

        Form.ActiveForm.Close();

    }
}
