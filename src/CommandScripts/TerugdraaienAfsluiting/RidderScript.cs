﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Drawing;

public class TerugdraaienAfsluiting_RidderScript : CommandScript
{
    public void Execute()
    {

        //DB
        //29-03-2018
        //9-7-2019 - Aangepast
        //Uren afsluiten ongedaan maken

        if (FormDataAwareFunctions.TableName != "R_PROJECTTIME")
        {
            MessageBox.Show("Dit script mag alleen vanaf de uren-tabel aangeroepen worden.");
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        List<int> listOfInts = FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue()).ToList();

        ScriptRecordset rsUrenAfgesloten = GetRecordset("R_PROJECTTIME", "CLOSED",
            $"PK_R_PROJECTTIME IN ({string.Join(",", listOfInts)}) AND CLOSED = 1", "");

        if (rsUrenAfgesloten.RecordCount == 0)
        {
            return;
        }

        //Zet vinkje 'Closed' uit bij geselecteerde uren
        rsUrenAfgesloten.MoveFirst();

        rsUrenAfgesloten.UpdateWhenMoveRecord = false;
        rsUrenAfgesloten.UseDataChanges = false;

        while (!rsUrenAfgesloten.EOF)
        {
            rsUrenAfgesloten.Fields["CLOSED"].Value = false;

            rsUrenAfgesloten.MoveNext();
        }

        rsUrenAfgesloten.MoveFirst();
        rsUrenAfgesloten.Update();

        FormDataAwareFunctions.Refres();



        //DB
        //28-10-2014
        //Titel: Verwijder boeking uit de tussentabel 'Afsluiten uren' om afsluiting urenboeking terug te draaien
        /*
		IRecord[] records = this.FormDataAwareFunctions.GetSelectedRecords();
		
		if(records.Length == 0)
			return;
		
		foreach(IRecord record in records)
		{
			//Haal id op van geselecteerde urenboeking
			ScriptRecordset rsUur = GetRecordset("R_PROJECTTIME", "PK_R_PROJECTTIME", "PK_R_PROJECTTIME = " + (int) record.GetPrimaryKeyValue()  ,"");
			rsUur.MoveFirst();
			
			//Controleer of urenboeking al is afgesloten
			ScriptRecordset rsAfsluitTabel  = GetRecordset("U_AFSLUITENUREN", "PK_U_AFSLUITENUREN, FK_URENBOEKING ", "FK_URENBOEKING = " + (int) rsUur.Fields["PK_R_PROJECTTIME"].Value,"");
			if( rsAfsluitTabel.RecordCount > 0)
			{
				rsAfsluitTabel.MoveFirst();
				rsAfsluitTabel.Delete();
			}
			else
			{
				System.Windows.Forms.MessageBox.Show("Urenboeking is nog niet afgesloten");	
			}
		}

		//Urentabel refreshen
		this.FormDataAwareFunctions.Refres();*/

    }
}
