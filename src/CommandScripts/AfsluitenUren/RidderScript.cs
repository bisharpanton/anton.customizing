﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Drawing;

public class AfsluitenUren_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //29-03-2018
        //9-7-2019 - Aangepast
        //Uren afsluiten

        if (FormDataAwareFunctions == null || FormDataAwareFunctions.TableName != "R_PROJECTTIME")
        {
            MessageBox.Show("Dit script mag alleen vanaf de uren-tabel aangeroepen worden.");
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        List<int> listOfInts = FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue()).ToList();

        var rsUrenNietAfgesloten = GetRecordset("R_PROJECTTIME", "CLOSED",
            $"PK_R_PROJECTTIME IN ({string.Join(",", listOfInts)}) AND CLOSED = 0", "");

        if (rsUrenNietAfgesloten.RecordCount == 0)
        {
            return;
        }

        //Zet vinkje 'Closed' aan bij geselecteerde uren
        rsUrenNietAfgesloten.MoveFirst();

        rsUrenNietAfgesloten.UpdateWhenMoveRecord = false;
        rsUrenNietAfgesloten.UseDataChanges = false;

        while (!rsUrenNietAfgesloten.EOF)
        {
            rsUrenNietAfgesloten.Fields["CLOSED"].Value = true;

            rsUrenNietAfgesloten.MoveNext();
        }

        rsUrenNietAfgesloten.MoveFirst();
        rsUrenNietAfgesloten.Update();

        FormDataAwareFunctions.Refres();


        /*
		IRecord[] records = this.FormDataAwareFunctions.GetSelectedRecords();
		
		if(records.Length == 0)
			return;
		
		ScriptRecordset rsAfsluitTabel  = GetRecordset("U_AFSLUITENUREN", "PK_U_AFSLUITENUREN, FK_URENBOEKING ", "FK_URENBOEKING = -1 " ,"");
		
		foreach(IRecord record in records)
		{
			//Haal id op van geselecteerde urenboeking
			ScriptRecordset rsUur = GetRecordset("R_PROJECTTIME", "PK_R_PROJECTTIME", "PK_R_PROJECTTIME = " + (int) record.GetPrimaryKeyValue()  ,"");
			rsUur.MoveFirst();
			
			//Controleer of urenboeking al is afgesloten
			ScriptRecordset rsControleAfsluitTabel  = GetRecordset("U_AFSLUITENUREN", "PK_U_AFSLUITENUREN, FK_URENBOEKING ", "FK_URENBOEKING = " + (int) rsUur.Fields["PK_R_PROJECTTIME"].Value,"");
			if( rsControleAfsluitTabel.RecordCount == 0)
			{
				rsAfsluitTabel.AddNew();
				rsAfsluitTabel.Fields["FK_URENBOEKING"].Value = (int) rsUur.Fields["PK_R_PROJECTTIME"].Value;
			}
			else
			{
				System.Windows.Forms.MessageBox.Show("Urenboeking is al afgesloten");	
			}
		}

		//Wijzigen wegschrijven en urentabel refreshen
		rsAfsluitTabel.Update();
		this.FormDataAwareFunctions.Refres();*/

    }
}
