﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using Ridder.Client.SDK.SDKParameters;

public class StartFormToChangePurchaseInvoiceMemo_RidderScript : CommandScript
{
    public void Execute()
    {
        //SP
        //02-05-2024
        //Start venster om interne memo van inkoopfactuur te wijzigen

        if (FormDataAwareFunctions == null || FormDataAwareFunctions.TableName != "R_PURCHASEINVOICE")
        {
            MessageBox.Show("Dit script mag alleen vanaf de inkoopfacturen-tabel aangeroepen worden.");
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        if (FormDataAwareFunctions.GetSelectedRecords().Length > 1)
        {
            MessageBox.Show("Dit script mag slechts voor één inkoopfactuur tegelijk aangeroepen worden.");
            return;
        }

        var purchaseInvoiceId = (int)FormDataAwareFunctions.CurrentRecord.GetPrimaryKeyValue();

        var rsPurchaseInvoice = GetRecordset("R_PURCHASEINVOICE", "MEMO",
            $"PK_R_PURCHASEINVOICE = {purchaseInvoiceId}", "");
        rsPurchaseInvoice.MoveFirst();

        var input = StartInputForm((string)rsPurchaseInvoice.Fields["MEMO"].Value);

        if (input.InputMemo == "_DONTPROCESS")
        {
            return;
        }

        rsPurchaseInvoice.Fields["MEMO"].Value = input.InputMemo;

        rsPurchaseInvoice.Update();

        FormDataAwareFunctions.Refres();
    }

    private Input StartInputForm(string internalMemo)
    {
        var rsFormChangeMemo = GetRecordset("C_FORMCHANGESALESINVOICEMEMO", "PK_C_FORMCHANGESALESINVOICEMEMO, PROCESS, MEMO, FOLLOWUPDATE",
            $"CREATOR = '{GetUserInfo().CurrentUserName}'", "");

        //Maak nieuw record op te openen
        rsFormChangeMemo.AddNew();
        rsFormChangeMemo.Fields["MEMO"].Value = internalMemo;
        //rsFormChangeMemo.Fields["FOLLOWUPDATE"].Value = DetermineFolluwUpDate();

        rsFormChangeMemo.Update();

        int createdFormId = (int)rsFormChangeMemo.Fields["PK_C_FORMCHANGESALESINVOICEMEMO"].Value;

        var editPar = new EditParameters()
        {
            TableName = "C_FORMCHANGESALESINVOICEMEMO",
            Id = createdFormId,
            Context = "R_PURCHASEINVOICE"
        };

        OpenEdit(editPar, true);

        //Check of het formulier correct is afgesloten
        var rsFormAfterChange = GetRecordset("C_FORMCHANGESALESINVOICEMEMO",
            "PK_C_FORMCHANGESALESINVOICEMEMO, PROCESS, MEMO, FOLLOWUPDATE",
            $"PK_C_FORMCHANGESALESINVOICEMEMO = {createdFormId}", "");

        rsFormAfterChange.MoveFirst();

        var input = new Input();


        input.InputMemo = (!(bool)rsFormAfterChange.Fields["PROCESS"].Value) ? "_DONTPROCESS" : (string)rsFormAfterChange.Fields["MEMO"].Value;
        //input.FollowupDate = (DateTime)rsFormAfterChange.Fields["FOLLOWUPDATE"].Value;
        //Ruim het record weer op
        rsFormAfterChange.Delete();

        return input;
    }

    public class Input
    {
        public string InputMemo { get; set; }
        public DateTime FollowupDate { get; set; }
    }
}
