﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class SetBoolBlockPaymentTrue_RidderScript : CommandScript
{
    public void Execute()
    {
        List<int> purchaseInvoiceIds = this.FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue()).ToList();

        if (purchaseInvoiceIds.Count == 0)
        {
            MessageBox.Show("Er zijn geen records geselecteerd.");
            return;
        }

        foreach (var purchaseInvoiceId in purchaseInvoiceIds)
        {
            var rsPurchaseInvoice = GetRecordset("R_PURCHASEINVOICE", "BLOCKPAYMENT",
                $"PK_R_PURCHASEINVOICE = {purchaseInvoiceId}", "");
            rsPurchaseInvoice.MoveFirst();

            rsPurchaseInvoice.SetFieldValue("BLOCKPAYMENT", true);

            var updateResult = rsPurchaseInvoice.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                MessageBox.Show("Error", $"Blokkeren betaling mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");
            }
        }

    }
}
