﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using Ridder.Client.SDK.SDKParameters;

public class CopyBisharpSchedulerTask_RidderScript : CommandScript
{
	public void Execute()
	{
        //SP
        //14-6-2024
        //Kopieer scheduler task

        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("U_BISHARPSCHEDULERTASKS", StringComparison.OrdinalIgnoreCase))
        {
            MessageBox.Show("Dit script werkt alleen op de tabel 'Bisharp scheduler tasks'.", "Ridder iQ", MessageBoxButtons.OK,
               MessageBoxIcon.Error);
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        int id = (int)FormDataAwareFunctions.CurrentRecord.GetPrimaryKeyValue();

        ObjectToCreate objectToCreate = GetObjectToCreate(id);

        var createdObjectId = CopyObject(objectToCreate);

        OpenCreatedSchedulerTask(createdObjectId);

    }

    private void OpenCreatedSchedulerTask(int createdObjectId)
    {
        var editParameters = new EditParameters()
        {
            TableName = "U_BISHARPSCHEDULERTASKS",
            Id = createdObjectId,
        };

        OpenEdit(editParameters, false);
    }

    private int CopyObject(ObjectToCreate objectToCreate)
    {
        var rsBisharpSchedulerTask = GetRecordset("U_BISHARPSCHEDULERTASKS", "", 
            "PK_U_BISHARPSCHEDULERTASKS = -1", "");
        rsBisharpSchedulerTask.UseDataChanges = true;
        rsBisharpSchedulerTask.UpdateWhenMoveRecord = false;
        rsBisharpSchedulerTask.AddNew();

        rsBisharpSchedulerTask.SetFieldValue("COMPANYNUMBER", objectToCreate.Company);
        rsBisharpSchedulerTask.SetFieldValue("TASKTYPE", objectToCreate.TaskType);

        var updateResult = rsBisharpSchedulerTask.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Het toevoegen van nieuwe fasen is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return 0;
        }

        return (int)updateResult.First().PrimaryKey;
    }

    private ObjectToCreate GetObjectToCreate(int id)
    {
        return GetRecordset("U_BISHARPSCHEDULERTASKS", "COMPANYNUMBER, TASKTYPE",
            $"PK_U_BISHARPSCHEDULERTASKS = {id}", "").DataTable.AsEnumerable().Select(x => new ObjectToCreate()
            {
                Company = x.Field<string>("COMPANYNUMBER"),
                TaskType = x.Field<string>("TASKTYPE")

            }).First();
    }

    class ObjectToCreate
    {
        public string Company { get; set; }
        public string TaskType { get; set; }
    }
}

