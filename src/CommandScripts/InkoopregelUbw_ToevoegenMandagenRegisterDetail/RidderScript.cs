﻿using ADODB;
using Ridder.Client.SDK.SDKParameters;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class InkoopregelUbw_ToevoegenMandagenRegisterDetail_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //15-3-2022
        //Kopieer regels

        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("U_FORMDETAILSUPPLIERMANDAYREGISTERDETAILS"))
        {
            MessageBox.Show("Dit script mag alleen vanaf Form Onderaannemer mandagenregister details aangeroepen worden.");
            return;
        }

        var formId = (int)FormDataAwareFunctions.FormParent.CurrentRecord.GetPrimaryKeyValue();

        var supplierId = GetRecordset("U_FORMSUPPLIERMANDAYREGISTERDETAILS", "", $"PK_U_FORMSUPPLIERMANDAYREGISTERDETAILS = {formId}", "")
            .DataTable.AsEnumerable().First().Field<int>("FK_SUPPLIER");

        var browsePar = new BrowseParameters()
        {
            TableName = "U_RELATIONWKAEMPLOYEES",
            Filter = $"FK_RELATION = {supplierId}",
            ResultColumnName = "PK_U_RELATIONWKAEMPLOYEES",
            Context = "ChooseContact",
        };

        var wkaEmployeeResult = OpenBrowse(browsePar, true); 

        if (wkaEmployeeResult == null)
        {
            return;
        }

        var wkaEmployee = GetRecordset("U_RELATIONWKAEMPLOYEES", "FIRSTNAME, NAMEPREFIX, LASTNAME, BSN, COCNUMBER, REGNO", 
            $"PK_U_RELATIONWKAEMPLOYEES = {wkaEmployeeResult}", "")
            .DataTable.AsEnumerable().First();

        var totalName = string.IsNullOrEmpty(wkaEmployee.Field<string>("NAMEPREFIX"))
            ? $"{wkaEmployee.Field<string>("FIRSTNAME")} {wkaEmployee.Field<string>("LASTNAME")}"
            : $"{wkaEmployee.Field<string>("FIRSTNAME")} {wkaEmployee.Field<string>("NAMEPREFIX")} {wkaEmployee.Field<string>("LASTNAME")}";

        var rsFormDetails = GetRecordset("U_FORMDETAILSUPPLIERMANDAYREGISTERDETAILS", "", $"PK_U_FORMDETAILSUPPLIERMANDAYREGISTERDETAILS IS NULL", "");
        rsFormDetails.UseDataChanges = false;
        rsFormDetails.UpdateWhenMoveRecord = false;

        rsFormDetails.AddNew();

        rsFormDetails.SetFieldValue("FK_FORMSUPPLIERMANDAYREGISTERDETAILS", formId);
        rsFormDetails.SetFieldValue("EXTERNALEMPLOYEE", totalName);
        rsFormDetails.SetFieldValue("YEAR", DateTime.Now.Year);

        rsFormDetails.SetFieldValue("BSN", wkaEmployee.Field<string>("BSN"));
        rsFormDetails.SetFieldValue("REGNO", wkaEmployee.Field<string>("REGNO"));
        rsFormDetails.SetFieldValue("COCNUMBER", wkaEmployee.Field<string>("COCNUMBER"));

        var updateResult = rsFormDetails.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Aanmaken regels mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

    }
}
