﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using Ridder.Client.SDK.SDKParameters;
using Ridder.Recordset.Extensions;
using Vergaderpunten_AanmakenTaak.Models;

public class Vergaderpunten_AanmakenTaak_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //1-12-2020
        //Aanmaken taak vanuit vergaderpunten

        if (FormDataAwareFunctions == null || FormDataAwareFunctions.TableName != "C_VERGADERPUNTEN")
        {
            MessageBox.Show("Dit script mag alleen vanaf de vergaderpunten-tabel aangeroepen worden.");
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        var selectedRecordIds =
            FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue()).ToList();

        var selectedMeetingPoints =
            GetRecordset("C_VERGADERPUNTEN", "", $"PK_C_VERGADERPUNTEN IN ({string.Join(",", selectedRecordIds)})", "")
                .As<MeetingPoint>().ToList();

        var createdTodos = CreateTodos(selectedMeetingPoints);

        if (!createdTodos.Any())
        {
            return;
        }

        var message = createdTodos.Count == 1
            ? $"1 taak aangemaakt, wil je deze openen?"
            : $"{createdTodos.Count} taken aangemaakt, wil je deze openen?";
        var dialogResult = MessageBox.Show(message, "Ridder iQ", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

        if (dialogResult == DialogResult.Yes)
        {
            OpenCreatedTodos(createdTodos);
        }
    }

    private void OpenCreatedTodos(List<int> createdTodos)
    {
        if (createdTodos.Count == 1)
        {
            var editPar = new EditParameters()
            {
                TableName = "R_TODO",
                Id = createdTodos.First(),
            };

            OpenEdit(editPar, false);
        }
        else
        {
            var browsePar = new BrowseParameters()
            {
                TableName = "R_TODO",
                Filter = $"PK_R_TODO IN ({string.Join(",", createdTodos)})",
            };

            OpenBrowse(browsePar, false);
        }
    }

    private List<int> CreateTodos(List<MeetingPoint> selectedMeetingPoints)
    {
        var crmSetting = GetCrmSettings();

        if (crmSetting == null)
        {
            return new List<int>();
        }

        if ((crmSetting.FK_TODOTYPEMEETINGPOINT ?? 0) == 0)
        {
            MessageBox.Show("Taaktype vergaderpunt taak niet ingevuld in de CRM instellingen.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return new List<int>();
        }

        if (selectedMeetingPoints.Any(x => !x.FK_OFFER.HasValue && !x.FK_ORDER.HasValue))
        {
            MessageBox.Show(
                "Een taak kan alleen gegenereerd worden indien een vergaderpunt aan een offerte of order gekoppeld is.",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return new List<int>();
        }

        if (selectedMeetingPoints.Any(x => string.IsNullOrEmpty(x.DESCRIPTION)))
        {
            MessageBox.Show(
                "Bij het aanmaken van een taak is de omschrijving verplicht. Vul een omschrijving bij de vergaderpunten in.",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return new List<int>();
        }

        
        var offers = GetOfferInfo(selectedMeetingPoints);
        var orders = GetOrderInfo(selectedMeetingPoints);
        var existingTodos = GetTodoInfo(selectedMeetingPoints);

        if (offers.Any(x => !x.FK_SALESPERSON.HasValue))
        {
            MessageBox.Show($"Bij offerte {offers.First(x => !x.FK_SALESPERSON.HasValue).OFFERNUMBER} is geen verkoper gevonden.",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return new List<int>();
        }

        if (orders.Any(x => !x.FK_TEKENAAR.HasValue))
        {
            MessageBox.Show($"Bij order {orders.First(x => !x.FK_TEKENAAR.HasValue).ORDERNUMBER} is geen werkvoorbereider gevonden.",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return new List<int>();
        }

        if (existingTodos.Any())
        {
            var meetingPoint = selectedMeetingPoints.First(x => x.PK_C_VERGADERPUNTEN == existingTodos.First().FK_MEETINGPOINT);

            MessageBox.Show($"Voor vergaderpunt '{meetingPoint.DESCRIPTION}' is al eerder een taak gegenereerd.",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return new List<int>();
        }

        var rsTodo = GetRecordset("R_TODO", "", "PK_R_TODO = NULL", "");
        rsTodo.UpdateWhenMoveRecord = false;
        rsTodo.UseDataChanges = true;

        foreach (var selectedMeetingPoint in selectedMeetingPoints)
        {
            rsTodo.AddNew();
            rsTodo.SetFieldValue("FK_TODOTYPE", crmSetting.FK_TODOTYPEMEETINGPOINT);

            if (selectedMeetingPoint.DESCRIPTION.Length > 80)
            {
                rsTodo.SetFieldValue("DESCRIPTION", selectedMeetingPoint.DESCRIPTION.Substring(0, 80));
                rsTodo.SetFieldValue("MEMO", selectedMeetingPoint.DESCRIPTION);
            }
            else
            {
                rsTodo.SetFieldValue("DESCRIPTION", selectedMeetingPoint.DESCRIPTION);
            }

            if (selectedMeetingPoint.FK_OFFER.HasValue)
            {
                var offer = offers.First(x => x.PK_R_OFFER == selectedMeetingPoint.FK_OFFER.Value);

                rsTodo.SetFieldValue("FK_OFFER", offer.PK_R_OFFER);
                rsTodo.SetFieldValue("FK_ASSIGNEDTO", offer.FK_SALESPERSON);

                rsTodo.SetFieldValue("FK_RELATION", offer.FK_RELATION);

                if(offer.FK_CONTACT.HasValue)
                {
                    rsTodo.SetFieldValue("FK_CONTACT", offer.FK_CONTACT);
                }                
            }
            else
            {
                var order = orders.First(x => x.PK_R_ORDER == selectedMeetingPoint.FK_ORDER.Value);

                rsTodo.SetFieldValue("FK_ORDER", order.PK_R_ORDER);
                rsTodo.SetFieldValue("FK_ASSIGNEDTO", order.FK_TEKENAAR);

                rsTodo.SetFieldValue("FK_RELATION", order.FK_RELATION);

                if (order.FK_CONTACT.HasValue)
                {
                    rsTodo.SetFieldValue("FK_CONTACT", order.FK_CONTACT);
                }
            }

            rsTodo.SetFieldValue("DUEDATE", selectedMeetingPoint.DATUM.Value.AddDays(crmSetting.LEADTIMEMEETINGPOINTTODO ?? 0));

            rsTodo.SetFieldValue("FK_MEETINGPOINT", selectedMeetingPoint.PK_C_VERGADERPUNTEN);
        }

        var updateResult = rsTodo.Update2();

        if (updateResult != null && updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Aanmaken taken mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return new List<int>();
        }

        return updateResult.Select(x => (int)x.PrimaryKey).ToList();
    }

    private List<Todo> GetTodoInfo(List<MeetingPoint> selectedMeetingPoints)
    {
        return GetRecordset("R_TODO", "FK_MEETINGPOINT",
                $"FK_MEETINGPOINT IN ({string.Join(",", selectedMeetingPoints.Select(x => x.PK_C_VERGADERPUNTEN))})",
                "").As<Todo>().ToList();
    }

    private List<Order> GetOrderInfo(List<MeetingPoint> selectedMeetingPoints)
    {
        var orderIds = selectedMeetingPoints.Where(x => x.FK_ORDER.HasValue).Select(x => x.FK_ORDER ?? 0).ToList();

        if (!orderIds.Any())
        {
            return new List<Order>();
        }

        return GetRecordset("R_ORDER", "", $"PK_R_ORDER IN ({string.Join(",", orderIds)})", "")
            .As<Order>().ToList();
    }

    private List<Offer> GetOfferInfo(List<MeetingPoint> selectedMeetingPoints)
    {
        var offerIds = selectedMeetingPoints.Where(x => x.FK_OFFER.HasValue).Select(x => x.FK_OFFER ?? 0).ToList();

        if (!offerIds.Any())
        {
            return new List<Offer>();
        }

        return GetRecordset("R_OFFER", "", $"PK_R_OFFER IN ({string.Join(",", offerIds)})", "")
            .As<Offer>().ToList();
    }

    private CrmSetting GetCrmSettings()
    {
        var customizingSettings = GetRecordset("R_CRMSETTINGS", "", "", "").As<CrmSetting>().ToList();

        if (!customizingSettings.Any())
        {
            MessageBox.Show("Geen CRM instellingen gevonden.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return null;
        }

        return customizingSettings.First();
    }
}
