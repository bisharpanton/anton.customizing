//  <auto-generated />
namespace Vergaderpunten_AanmakenTaak.Models
{
    using System;
    
    
    ///  <summary>
    ///  	 Table: R_OFFER
    ///  </summary>
    ///  <remarks>
    /// 	Available columns (for debugging, not necessarily available in this class):
    ///  	AFRONDENCALCULATIE, BILLINGSCHEDULE, BOOKINCURRENCY, CALCULATIEOMSCHRIJVING, CALCULATIETYPE, CORROSIEKLASSE, CREATOR, CUSTOMERDRAWINGNUMBER, DATEAPPROVAL, DATECHANGED
    ///  	DATECREATED, DATEREQUESTAPPROVAL, DELIVERYDATE, DELIVERYPERIOD, DESCRIPTION, DRAWINGNUMBER, DUMMYORIGINTODO, ESTIMATEDLEADTIME, EXCHANGERATE, EXCHANGERATEDATE
    ///  	EXECUTIEKLASSE, EXPECTEDORDERDATE, EXTERNALKEY, FK_ALTERNATIVEREPORTLAYOUT, FK_APPROVER, FK_BASISCALCULATIE, FK_CONTACT, FK_COSTINGPARAMETERS, FK_COSTUNIT, FK_CURRENCY
    ///  	FK_DEALER, FK_DELIVERYRELATION, FK_DESTINATIONADDRESS, FK_DIVERGENTTRADINGNAME, FK_INDUSTRY, FK_INVOICESCHEDULE, FK_LANGUAGE, FK_LOSTTO, FK_MAINPROJECT, FK_ORDERCATEGORY
    ///  	FK_ORDERMETHOD, FK_PAYMENTMETHOD, FK_PAYMENTTERM, FK_PLANNER, FK_PROJECT, FK_R_OFFER, FK_REJECTIONCODE, FK_RELATION, FK_RENTALCONTRACT, FK_REPORT
    ///  	FK_REVENUECODE, FK_SALESPERSON, FK_SENDERADDRESS, FK_SHIPPINGAGENT, FK_SHIPPINGTYPE, FK_UITGANGSNORM, FK_VATCOMPANYGROUP, FK_WORKFLOWSTATE, FOOTERMEMO, HEADERMEMO
    ///  	ISORDER, MAXIMALPRODUCTIONFINISHDATE, MEMOALGEMENEVOORWAARDEN, MEMOINTERN, MINIMALPRODUCTIONSTARTDATE, NIETINVOORSTELMEEGENOMEN, OFFERDATE, OFFERNUMBER, ORDERTYPE, PK_R_OFFER
    ///  	PLAINTEXT_FOOTERMEMO, PLAINTEXT_HEADERMEMO, PLAINTEXT_MEMOALGEMENEVOORWAARDEN, PLAINTEXT_MEMOINTERN, PLAINTEXT_NIETINVOORSTELMEEGENOMEN, PLAN, PLANDIRECTION, PLANNEDPRODUCTIONENDDATE, PLANNEDPRODUCTIONSTARTDATE, PLANNINGPRIORITY
    ///  	PROBABILITYOFSALE, RECORDLINK, REFERENCE, REJECTIONDATE, RENTALENDDATE, RENTALPERIOD, RENTALSTARTDATE, REQUESTFORQUOTATIONNUMBER, RESETCALCULATIESCHERM, REVISION
    ///  	SENDDATE, SHIPPINGPLACE, U_INLEVERDATUM, UNINTERRUPTEDBATCH, USERCHANGED, VALIDPERIOD, VERWACHTEORDERDATUMJAAR, VERWACHTEORDERDATUMKWARTAAL, VGPLAN, VOORBEWERKINGSGRAAD
    ///  
    ///  	Filter used: FK_TABLEINFO = '801dce67-0043-48b9-8a0b-edf5b73c37ca' AND (ISSECRET = 0)
    ///  	Included columns: OFFERNUMBER, FK_RELATION, FK_CONTACT, FK_SALESPERSON
    ///  	Excluded columns: 
    ///  </remarks>
    public partial class Offer
    {
        
        private System.Nullable<int> _FK_CONTACT;
        
        private int _FK_RELATION;
        
        private System.Nullable<int> _FK_SALESPERSON;
        
        private int _OFFERNUMBER;
        
        private int _PK_R_OFFER;
        
        /// <summary>
        /// 	Contact
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: R_OFFER
        /// </remarks>
        public virtual System.Nullable<int> FK_CONTACT
        {
            get
            {
                return this._FK_CONTACT;
            }
            set
            {
                this._FK_CONTACT = value;
            }
        }
        
        /// <summary>
        /// 	Relation
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: R_OFFER
        /// </remarks>
        public virtual int FK_RELATION
        {
            get
            {
                return this._FK_RELATION;
            }
            set
            {
                this._FK_RELATION = value;
            }
        }
        
        /// <summary>
        /// 	Sales person
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: R_OFFER
        /// </remarks>
        public virtual System.Nullable<int> FK_SALESPERSON
        {
            get
            {
                return this._FK_SALESPERSON;
            }
            set
            {
                this._FK_SALESPERSON = value;
            }
        }
        
        /// <summary>
        /// 	Offer number
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        public virtual int OFFERNUMBER
        {
            get
            {
                return this._OFFERNUMBER;
            }
            set
            {
                this._OFFERNUMBER = value;
            }
        }
        
        /// <summary>
        /// 	Business offers id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        public virtual int PK_R_OFFER
        {
            get
            {
                return this._PK_R_OFFER;
            }
            set
            {
                this._PK_R_OFFER = value;
            }
        }
    }
}
