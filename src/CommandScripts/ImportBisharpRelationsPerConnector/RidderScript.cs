﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class ImportBisharpRelationsPerConnector_RidderScript : CommandScript
{
	public void Execute()
	{
        //SP
        //25-7-2023
        //Importeer bisharp relaties per portal user

        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("U_BISHARPCONNECTORS", StringComparison.OrdinalIgnoreCase))
        {
            MessageBox.Show("Dit script mag alleen van de Bisharp koppelingen tabel aangeroepen worden.");
            return;
        }

        var connectorIds = FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue()).ToList();

        object[] selectie = null;
        selectie = OpenMultiSelectForm("R_RELATION", "PK_R_RELATION",
            "BISHARPCOMPANYNUMBER IS NOT NULL", "");

        if (selectie == null)
        {
            return;
        }

        ImportDashboardGroups(selectie, connectorIds);

    }

    private void ImportDashboardGroups(object[] selectie, List<int> connectorIds)
    {
        var rsBisharpRelationsPerConnector = GetRecordset("U_BISHARPRELATIONCONNECTOR", "",
            "PK_U_BISHARPRELATIONCONNECTOR = -1", "");
        rsBisharpRelationsPerConnector.UseDataChanges = true;
        rsBisharpRelationsPerConnector.UpdateWhenMoveRecord = false;

        foreach (var connectorId in connectorIds)
        {
            foreach (var relationId in selectie)
            {
                rsBisharpRelationsPerConnector.AddNew();
                rsBisharpRelationsPerConnector.Fields["FK_RELATION"].Value = relationId;
                rsBisharpRelationsPerConnector.Fields["FK_BISHARPCONNECTOR"].Value = connectorId;
            }
        }

        var updateResult = rsBisharpRelationsPerConnector.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Het importeren van Bisharp relaties per connector is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

    }
}
