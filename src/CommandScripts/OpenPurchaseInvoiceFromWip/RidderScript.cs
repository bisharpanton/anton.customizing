﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Drawing;

public class OpenPurchaseInvoiceFromWIP_RidderScript : CommandScript
{
    public void Execute()
    {
        // HVE
        // 29-11-2018
        // Inkoopfactuur vanuit OHW openen

        // Tabel: ALLE REGELS OHW  = R_ORDERWIPALLDETAIL
        if (FormDataAwareFunctions.TableName != "R_ORDERWIPALLDETAIL")
        {
            MessageBox.Show("Dit script mag alleen vanaf de gecombineerde OHW-regels worden aangeroepen.", "Ridder iQ",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        // als niks geselecteerd:
        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        List<int> results = new List<int>();

        foreach (var record in FormDataAwareFunctions.GetSelectedRecords())
        {

            // Brontabelnaam R_ORDERWIPDETAILITEM (artikel)
            if (record.GetCurrentRecordValue("SOURCE_TABLENAME").ToString() == "R_ORDERWIPDETAILITEM")
            {
                var OrderWipDetailItemId = (int)record.GetCurrentRecordValue("FK_ORDERWIPDETAILITEM");

                var rsOrderWipDetailItem = GetRecordset("R_ORDERWIPDETAILITEM", "FK_GOODSRECEIPTDETAILITEM, FK_PURCHASEINVOICEDETAILITEM",
                    $"PK_R_ORDERWIPDETAILITEM == {OrderWipDetailItemId}", "");

                rsOrderWipDetailItem.MoveFirst();

                // ITEM Inkoopfactuur via goederen-ontvangst 
                var goodsReceiptDetailItemId = rsOrderWipDetailItem.Fields["FK_GOODSRECEIPTDETAILITEM"].Value as int? ?? 0;
                if (goodsReceiptDetailItemId != 0)
                {
                    // Bepaal FK_PURCHAEINVOICE welke gelijk is aan Goederenontvangst.
                    var rsPurchaseInvoiceDetailItem = GetRecordset("R_PURCHASEINVOICEDETAILITEM", "FK_PURCHASEINVOICE",
                        $"FK_GOODSRECEIPTDETAILITEM = {goodsReceiptDetailItemId}", "");

                    if (rsPurchaseInvoiceDetailItem.RecordCount != 0)
                    {
                        rsPurchaseInvoiceDetailItem.MoveFirst();
                        int purchaseInvoiceId = (int)rsPurchaseInvoiceDetailItem.Fields["FK_PURCHASEINVOICE"].Value;

                        if (!results.Contains(purchaseInvoiceId))
                        {
                            results.Add(purchaseInvoiceId);
                        }
                    }
                }
                // ITEM  Inkoopfactuur via direct inkoopfactuur
                else if ((rsOrderWipDetailItem.Fields["FK_PURCHASEINVOICEDETAILITEM"].Value as int?).HasValue)
                {
                    int purchaseInvoiceDetailItemId = (int)rsOrderWipDetailItem.Fields["FK_PURCHASEINVOICEDETAILITEM"].Value;

                    var rsPurchaseInvoiceDetailItem = GetRecordset("R_PURCHASEINVOICEDETAILITEM", "FK_PURCHASEINVOICE",
                       $"PK_R_PURCHASEINVOICEDETAILITEM = {purchaseInvoiceDetailItemId}", "");
                    rsPurchaseInvoiceDetailItem.MoveFirst();
                    int purchaseInvoiceId = (int)rsPurchaseInvoiceDetailItem.Fields["FK_PURCHASEINVOICE"].Value;

                    if (!results.Contains(purchaseInvoiceId))
                    {
                        results.Add(purchaseInvoiceId);
                    }
                }

            }
            // Brontabelnaam R_ORDERWIPDETAILITEMCOR (Correctie artikel)
            else if (record.GetCurrentRecordValue("SOURCE_TABLENAME").ToString() == "R_ORDERWIPDETAILITEMCOR")
            {
                var OrderWipDetailItemCorId = (int)record.GetCurrentRecordValue("FK_ORDERWIPDETAILITEMCOR");
                var rsOrderWipDetailItemCor = GetRecordset("R_ORDERWIPDETAILITEMCOR", "FK_PURCHASEINVOICEDETAILITEM",
                    $"PK_R_ORDERWIPDETAILITEMCOR == {OrderWipDetailItemCorId}", "");

                rsOrderWipDetailItemCor.MoveFirst();

                var purchaseInvoiceDetailItemId = rsOrderWipDetailItemCor.Fields["FK_PURCHASEINVOICEDETAILITEM"].Value as int? ?? 0;
                // ITEMCOR  Inkoopfactuur via direct inkoopfactuur
                if (purchaseInvoiceDetailItemId != 0)
                {
                    var rsPurchaseInvoiceDetailItemCor = GetRecordset("R_PURCHASEINVOICEDETAILITEM", "FK_PURCHASEINVOICE",
                       $"PK_R_PURCHASEINVOICEDETAILITEM = {purchaseInvoiceDetailItemId}", "");

                    rsPurchaseInvoiceDetailItemCor.MoveFirst();
                    int purchaseInvoiceId = (int)rsPurchaseInvoiceDetailItemCor.Fields["FK_PURCHASEINVOICE"].Value;

                    if (!results.Contains(purchaseInvoiceId))
                    {
                        results.Add(purchaseInvoiceId);
                    }
                }

            }
            // Brontabelnaam R_ORDERWIPDETAILMISC (diverse)
            else if (record.GetCurrentRecordValue("SOURCE_TABLENAME").ToString() == "R_ORDERWIPDETAILMISC")
            {
                var OrderWipDetailMiscId = (int)record.GetCurrentRecordValue("FK_ORDERWIPDETAILMISC");
                var rsOrderWipDetailMisc = GetRecordset("R_ORDERWIPDETAILMISC", "FK_GOODSRECEIPTDETAILMISC, FK_PURCHASEINVOICEDETAILMISC",
                    $"PK_R_ORDERWIPDETAILMISC == {OrderWipDetailMiscId}", "");
                rsOrderWipDetailMisc.MoveFirst();

                // MISC Inkoopfactuur via goederen-ontvangst 
                var goodsReceiptDetailMiscId = rsOrderWipDetailMisc.Fields["FK_GOODSRECEIPTDETAILMISC"].Value as int? ?? 0;
                if (goodsReceiptDetailMiscId != 0)
                {
                    // Bepaal FK_PURCHAEINVOICE welke gelijk is aan Goederenontvangst.
                    var rsPurchaseInvoiceDetailMisc = GetRecordset("R_PURCHASEINVOICEDETAILMISC", "FK_PURCHASEINVOICE",
                        $"FK_GOODSRECEIPTDETAILMISC = {goodsReceiptDetailMiscId}", "");

                    if (rsPurchaseInvoiceDetailMisc.RecordCount != 0)
                    {
                        rsPurchaseInvoiceDetailMisc.MoveFirst();
                        int purchaseInvoiceId = (int)rsPurchaseInvoiceDetailMisc.Fields["FK_PURCHASEINVOICE"].Value;

                        if (!results.Contains(purchaseInvoiceId))
                        {
                            results.Add(purchaseInvoiceId);
                        }
                    }
                }
                // MISC  Inkoopfactuur via direct inkoopfactuur
                else if ((rsOrderWipDetailMisc.Fields["FK_PURCHASEINVOICEDETAILMISC"].Value as int?).HasValue)
                {
                    int purchaseInvoiceDetailMiscId = (int)rsOrderWipDetailMisc.Fields["FK_PURCHASEINVOICEDETAILMISC"].Value;

                    var rsPurchaseInvoiceDetailMisc = GetRecordset("R_PURCHASEINVOICEDETAILMISC", "FK_PURCHASEINVOICE",
                       $"PK_R_PURCHASEINVOICEDETAILMISC = {purchaseInvoiceDetailMiscId}", "");

                    rsPurchaseInvoiceDetailMisc.MoveFirst();
                    int purchaseInvoiceId = (int)rsPurchaseInvoiceDetailMisc.Fields["FK_PURCHASEINVOICE"].Value;

                    if (!results.Contains(purchaseInvoiceId))
                    {
                        results.Add(purchaseInvoiceId);
                    }
                }

            }
            // Brontabelnaam R_ORDERWIPDETAILMiscCOR (Correctie diverse)
            else if (record.GetCurrentRecordValue("SOURCE_TABLENAME").ToString() == "R_ORDERWIPDETAILMISCCOR")
            {
                var OrderWipDetailMiscCorId = (int)record.GetCurrentRecordValue("FK_ORDERWIPDETAILMISCCOR");

                var rsOrderWipDetailMiscCor = GetRecordset("R_ORDERWIPDETAILMISCCOR", "FK_PURCHASEINVOICEDETAILMISC",
                    $"PK_R_ORDERWIPDETAILMISCCOR == {OrderWipDetailMiscCorId}", "");

                rsOrderWipDetailMiscCor.MoveFirst();
                var purchaseInvoiceDetailMisCId = rsOrderWipDetailMiscCor.Fields["FK_PURCHASEINVOICEDETAILMISC"].Value as int? ?? 0;
                // MISCCOR  Inkoopfactuur via direct inkoopfactuur
                if (purchaseInvoiceDetailMisCId != 0)
                {
                    var rsPurchaseInvoiceDetailMiscCor = GetRecordset("R_PURCHASEINVOICEDETAILMISC", "FK_PURCHASEINVOICE",
                       $"PK_R_PURCHASEINVOICEDETAILMISC = {purchaseInvoiceDetailMisCId}", "");

                    rsPurchaseInvoiceDetailMiscCor.MoveFirst();
                    int purchaseInvoiceId = (int)rsPurchaseInvoiceDetailMiscCor.Fields["FK_PURCHASEINVOICE"].Value;

                    if (!results.Contains(purchaseInvoiceId))
                    {
                        results.Add(purchaseInvoiceId);
                    }
                }
            }
            // Brontabelnaam R_ORDERWIPDETAILOUTSOURCED (UBW)
            else if (record.GetCurrentRecordValue("SOURCE_TABLENAME").ToString() == "R_ORDERWIPDETAILOUTSOURCED")
            {
                var OrderWipDetailOutsourcedId = (int)record.GetCurrentRecordValue("FK_ORDERWIPDETAILOUTSOURCED");

                var rsOrderWipDetailOutsourced = GetRecordset("R_ORDERWIPDETAILOUTSOURCED", "FK_GOODSRECEIPTDETAILOUTSOURCED, FK_PURCHASEINVOICEDETAILOUTSOURCED",
                    $"PK_R_ORDERWIPDETAILOUTSOURCED == {OrderWipDetailOutsourcedId}", "");

                rsOrderWipDetailOutsourced.MoveFirst();

                // Outsourced Inkoopfactuur via goederen-ontvangst 
                var goodsReceiptDetailOutsourcedId = rsOrderWipDetailOutsourced.Fields["FK_GOODSRECEIPTDETAILOUTSOURCED"].Value as int? ?? 0;
                if (goodsReceiptDetailOutsourcedId != 0)
                {
                    // Bepaal FK_PURCHAEINVOICE welke gelijk is aan Goederenontvangst.
                    var rsPurchaseInvoiceDetailOutsourced = GetRecordset("R_PURCHASEINVOICEDETAILOUTSOURCED", "FK_PURCHASEINVOICE",
                        $"FK_GOODSRECEIPTDETAILOUTSOURCED = {goodsReceiptDetailOutsourcedId}", "");

                    if (rsPurchaseInvoiceDetailOutsourced.RecordCount != 0)
                    {
                        rsPurchaseInvoiceDetailOutsourced.MoveFirst();
                        int purchaseInvoiceId = (int)rsPurchaseInvoiceDetailOutsourced.Fields["FK_PURCHASEINVOICE"].Value;

                        if (!results.Contains(purchaseInvoiceId))
                        {
                            results.Add(purchaseInvoiceId);
                        }
                    }
                }
                // Outsorced  Inkoopfactuur via direct inkoopfactuur
                else if ((rsOrderWipDetailOutsourced.Fields["FK_PURCHASEINVOICEDETAILOUTSOURCED"].Value as int?).HasValue)
                {
                    int purchaseInvoiceDetailOutsourcedId = (int)rsOrderWipDetailOutsourced.Fields["FK_PURCHASEINVOICEDETAILOUTSOURCED"].Value;

                    var rsPurchaseInvoiceDetailOutsourced = GetRecordset("R_PURCHASEINVOICEDETAILOUTSOURCED", "FK_PURCHASEINVOICE",
                       $"PK_R_PURCHASEINVOICEDETAILOUTSOURCED = {purchaseInvoiceDetailOutsourcedId}", "");

                    rsPurchaseInvoiceDetailOutsourced.MoveFirst();
                    int purchaseInvoiceId = (int)rsPurchaseInvoiceDetailOutsourced.Fields["FK_PURCHASEINVOICE"].Value;

                    if (!results.Contains(purchaseInvoiceId))
                    {
                        results.Add(purchaseInvoiceId);
                    }
                }
            }
            // Brontabelnaam R_ORDERWIPDETAILOUTSOURCEDCOR (Correctie UBW)
            else if (record.GetCurrentRecordValue("SOURCE_TABLENAME").ToString() == "R_ORDERWIPDETAILOUTSOURCEDCOR")
            {
                var OrderWipDetailOutsourcedCorId = (int)record.GetCurrentRecordValue("FK_ORDERWIPDETAILOUTSOURCEDCOR");

                var rsOrderWipDetailOutsourcedCor = GetRecordset("R_ORDERWIPDETAILOUTSOURCEDCOR", "FK_PURCHASEINVOICEDETAILOUTSOURCED",
                    $"PK_R_ORDERWIPDETAILOUTSOURCEDCOR == {OrderWipDetailOutsourcedCorId}", "");

                rsOrderWipDetailOutsourcedCor.MoveFirst();

                var purchaseInvoiceDetailOutsourcedCorId = rsOrderWipDetailOutsourcedCor.Fields["FK_PURCHASEINVOICEDETAILOUTSOURCED"].Value as int? ?? 0;
                // MISCCOR  Inkoopfactuur via direct inkoopfactuur
                if (purchaseInvoiceDetailOutsourcedCorId != 0)
                {
                    var rsPurchaseInvoiceDetailOutsourcedCor = GetRecordset("R_PURCHASEINVOICEDETAILOUTSOURCED", "FK_PURCHASEINVOICE",
                       $"PK_R_PURCHASEINVOICEDETAILOUTSOURCED = {purchaseInvoiceDetailOutsourcedCorId}", "");

                    rsPurchaseInvoiceDetailOutsourcedCor.MoveFirst();
                    int purchaseInvoiceId = (int)rsPurchaseInvoiceDetailOutsourcedCor.Fields["FK_PURCHASEINVOICE"].Value;

                    if (!results.Contains(purchaseInvoiceId))
                    {
                        results.Add(purchaseInvoiceId);
                    }
                }
            }
        }
        // 0 = foutmelding
        // 1 = edit
        // +1 = browse 
        if (!results.Any())
        {
            MessageBox.Show("Geen inkoopfacturen gevonden.", "Ridder iQ", MessageBoxButtons.OK);
            return;
        }
        else if (results.Count == 1)
        {
            var editPar = new Ridder.Client.SDK.SDKParameters.EditParameters();
            editPar.TableName = "R_PURCHASEINVOICE";
            editPar.Id = results.First();

            OpenEdit(editPar, false);
        }
        else
        {
            var browsePar = new Ridder.Client.SDK.SDKParameters.BrowseParameters();
            browsePar.TableName = "R_PURCHASEINVOICE";
            browsePar.Filter = $"PK_R_PURCHASEINVOICE in ({string.Join(",", results)})";
            // tbv layout z. filter
            browsePar.Context = "OpenPurchaseInvoices";

            OpenBrowse(browsePar, false);
        }
    }
}
