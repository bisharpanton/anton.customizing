﻿using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Ridder.Common;
using Ridder.Common.ADO;
using Ridder.Common.Script;
using System.Windows.Forms;

public class OpenRelationDocumentLocation_RidderScript : CommandScript
{
	public void Execute()
	{
        //SP
        //24-8-2023
        //Open maplocatie relatiedocument

        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("R_RELATION"))
        {
            MessageBox.Show("Dit script mag alleen vanaf de tabel Relaties aangeroepen worden.");
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        IRecord[] records = FormDataAwareFunctions.GetSelectedRecords();

        var defaultLocation = GetRecordset("R_CRMSETTINGS", "DEFAULTDOCUMENTLOCATION", "", "").DataTable.AsEnumerable().First().Field<string>("DEFAULTDOCUMENTLOCATION");

        foreach (IRecord record in records)
        {
            var relationCode = GetRecordset("R_RELATION", "CODE",
                    $"PK_R_RELATION = {record.GetPrimaryKeyValue()}", "").DataTable.AsEnumerable().First().Field<string>("CODE");

            string subPath = $@"{defaultLocation}\Relatiedocumenten\{relationCode}";

            if (string.IsNullOrEmpty(subPath))
            {
                return;
            }

            if (!Directory.Exists(subPath))
            {
                Directory.CreateDirectory(subPath);
            }

            Process prc = new Process();
            prc.StartInfo.FileName = subPath;
            prc.Start();
        }

    }
}
