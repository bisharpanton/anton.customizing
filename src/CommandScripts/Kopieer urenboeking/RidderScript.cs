﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Drawing;

public class Kopieer_urenboeking_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //9-7-2019
        //Kopieer urenboeking

        if (FormDataAwareFunctions == null || FormDataAwareFunctions.TableName != "R_PROJECTTIME")
        {
            MessageBox.Show("Dit script mag alleen vanaf de uren-tabel aangeroepen worden.");
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        /*DB - 20191712 - Wel toestaan voor meerdere records
        if (FormDataAwareFunctions.GetSelectedRecords().Length > 1)
        {
            MessageBox.Show("Dit script mag slechts voor één urenboeking tegelijk aangeroepen worden.");
            return;
        }*/

        if (FormDataAwareFunctions.GetSelectedRecords().Any(x => (bool)x.GetCurrentRecordValue("CLOSED")))
        {
            MessageBox.Show("Een van de geselecteerde urenboekingen is al afgesloten. Kopiëren niet toegestaan.", "Ridder iQ",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        var employees = OpenMultiSelectForm("R_EMPLOYEE", "PK_R_EMPLOYEE",
            $"NIETDECLARABEL = 0 AND (EMPLOYMENTTERMINATIONDATE IS NULL OR EMPLOYMENTTERMINATIONDATE > '{DateTime.Now:yyyyMMdd}')",
            "KiesWerknemers");

        if (employees == null)
        {
            return;
        }

        var rsProjectTime = GetRecordset("R_PROJECTTIME", "", "PK_R_PROJECTTIME = -1", "");
        rsProjectTime.UseDataChanges = true;
        rsProjectTime.UpdateWhenMoveRecord = false;

        Cursor.Current = Cursors.WaitCursor;
        try
        {
            foreach (var record in FormDataAwareFunctions.GetSelectedRecords())
            {
                foreach (var employee in employees)
                {
                    rsProjectTime.AddNew();

                    rsProjectTime.Fields["FK_EMPLOYEE"].Value = employee;
                    rsProjectTime.Fields["DATE"].Value = record.GetCurrentRecordValue("DATE");
                    rsProjectTime.Fields["FK_ORDER"].Value =
                        record.GetCurrentRecordValue("FK_ORDER");
                    rsProjectTime.Fields["FK_JOBORDER"].Value =
                        record.GetCurrentRecordValue("FK_JOBORDER");
                    rsProjectTime.Fields["FK_JOBORDERDETAILWORKACTIVITY"].Value =
                        record.GetCurrentRecordValue("FK_JOBORDERDETAILWORKACTIVITY");

                    rsProjectTime.Fields["FK_WORKACTIVITY"].Value =
                        record.GetCurrentRecordValue("FK_WORKACTIVITY");

                    rsProjectTime.Fields["TIMEEMPLOYEE"].Value =
                        record.GetCurrentRecordValue("TIMEEMPLOYEE");
                    rsProjectTime.Fields["TIMEDEVICE"].Value =
                        record.GetCurrentRecordValue("TIMEDEVICE");

                    rsProjectTime.Fields["OPERATIONORSETUP"].Value =
                        record.GetCurrentRecordValue("OPERATIONORSETUP");

                    /*DB - 20220202 - Overuren worden bepaald volgens rooster
                    rsProjectTime.Fields["FK_OVERTIMECODE"].Value = record.GetCurrentRecordValue("FK_OVERTIMECODE");

                    rsProjectTime.Fields["TIMEOFINLIEU"].Value = record.GetCurrentRecordValue("TIMEOFINLIEU");*/

                    rsProjectTime.Fields["VERKORTEMEMO"].Value =
                        record.GetCurrentRecordValue("VERKORTEMEMO");

                    rsProjectTime.Fields["COPIEDPROJECTTIME"].Value =
                        record.GetPrimaryKeyValue();
                }

                var updateResult = rsProjectTime.Update2();

                if (updateResult != null && updateResult.Any(x => x.HasError))
                {
                    MessageBox.Show(
                        $"Het aanmaken van de urenboekingen is mislukt; oorzaak: {updateResult.First(x => x.HasError).GetResult()}",
                        "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
        }
        finally
        {
            Cursor.Current = Cursors.Default;
        }




        FormDataAwareFunctions.Refres();
    }
}
