﻿using ADODB;
using Ridder.Client.SDK.SDKParameters;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.CRM;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class Inkoop_OpenInkoopfacturen_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //30-1-2023
        //Open de gekoppelde PDF inkoopfacturen 

        if(FormDataAwareFunctions == null || FormDataAwareFunctions.TableName != "R_PURCHASEORDER")
        {
            MessageBox.Show("Dit script mag alleen vanaf de inkooporder aangeroepen worden.");
            return;
        }

        if(FormDataAwareFunctions.GetSelectedRecords().Count() > 1)
        {
            MessageBox.Show("Dit script mag voor één inkooporder tegelijk aangeroepen worden.");
            return;
        }

        if(!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        var purchaseOrderId = (int)FormDataAwareFunctions.GetSelectedRecords().First().GetPrimaryKeyValue();

        var purchaseInvoiceIds = new List<int>();

        GetAllPurchaseInvoiceIdsFromGoodsReceipts(purchaseInvoiceIds, purchaseOrderId);
        GetPurchaseInvoiceIdsFromInstallments(purchaseInvoiceIds, purchaseOrderId);

        if(!purchaseInvoiceIds.Any())
        {
            MessageBox.Show("Geen inkoopfacturen gevonden bij deze inkooporder.");
            return;
        }

        var purchaseInvoiceDocumentIds = GetRecordset("R_PURCHASEINVOICEDOCUMENT", "FK_DOCUMENT",
            $"FK_PURCHASEINVOICE IN ({string.Join(",", purchaseInvoiceIds)})", "")
            .DataTable.AsEnumerable().Select(x => x.Field<int>("FK_DOCUMENT")).ToList();

        if (!purchaseInvoiceDocumentIds.Any())
        {
            MessageBox.Show("Geen documenten gevonden bij de inkoopfacturen.");
            return;
        }

        var documentLocations = GetRecordset("R_DOCUMENT", "DOCUMENTLOCATION",
            $"PK_R_DOCUMENT IN ({string.Join(",", purchaseInvoiceDocumentIds)}) AND STORAGESYSTEM = 'Verwijzing' AND DOCUMENTLOCATION <> '' AND EXTENSION IN ('.pdf', '.PDF')", "")
            .DataTable.AsEnumerable().Select(x => (string)x["DOCUMENTLOCATION"]).ToList();

        foreach (var documentLocation in documentLocations)
        {
            Process.Start(documentLocation);
        }
    }

    private void GetPurchaseInvoiceIdsFromInstallments(List<int> purchaseInvoiceIds, int purchaseOrderId)
    {
        var purchaseInvoiceIdsInstallments = GetRecordset($"R_PURCHASEINSTALLMENT", "FK_PURCHASEINVOICE",
            $"FK_PURCHASEORDER = {purchaseOrderId}", "")
            .DataTable.AsEnumerable().Select(x => (int)x[$"FK_PURCHASEINVOICE"]).ToList();

        purchaseInvoiceIds.AddRange(purchaseInvoiceIdsInstallments);
    }

    private void GetAllPurchaseInvoiceIdsFromGoodsReceipts(List<int> purchaseInvoiceIds, int purchaseOrderId)
    {
        var sources = new List<string>() { "ITEM", "MISC", "OUTSOURCED" };

        foreach (var source in sources)
        {
            var purchaseOrderDetailIds = GetRecordset($"R_PURCHASEORDERDETAIL{source}", $"PK_R_PURCHASEORDERDETAIL{source}",
                $"FK_PURCHASEORDER = {purchaseOrderId}", "").DataTable.AsEnumerable().Select(x => (int)x[$"PK_R_PURCHASEORDERDETAIL{source}"]).ToList();

            if(!purchaseOrderDetailIds.Any())
            {
                continue;
            }

            var goodsReceiptDetailIds = GetRecordset($"R_GOODSRECEIPTDETAIL{source}", $"PK_R_GOODSRECEIPTDETAIL{source}",
                $"FK_PURCHASEORDERDETAIL{source} IN ({string.Join(",", purchaseOrderDetailIds)})", "")
                .DataTable.AsEnumerable().Select(x => (int)x[$"PK_R_GOODSRECEIPTDETAIL{source}"]).ToList();

            if(!goodsReceiptDetailIds.Any())
            {
                continue;
            }

            var purchaseInvoiceIdsThisGoodsReceiptDetails = GetRecordset($"R_PURCHASEINVOICEDETAIL{source}", "FK_PURCHASEINVOICE",
                $"FK_GOODSRECEIPTDETAIL{source} IN ({string.Join(",", goodsReceiptDetailIds)})", "")
                .DataTable.AsEnumerable().Select(x => (int)x[$"FK_PURCHASEINVOICE"]).ToList();

            purchaseInvoiceIds.AddRange(purchaseInvoiceIdsThisGoodsReceiptDetails);
        }
    }
}
