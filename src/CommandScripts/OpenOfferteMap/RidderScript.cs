﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Drawing;

public class RidderScript : CommandScript
{
    string offeryear;
    string offernumber;

    public void Execute()
    {
        IRecord[] records = this.FormDataAwareFunctions.GetSelectedRecords();

        if (this.FormDataAwareFunctions.TableName == "R_OFFER")
        {
            // Als niets geselecteerd stoppen
            if (records.Length == 0)
                return;
            foreach (IRecord record in records)
            {
                var rsPurchaseOffer = GetRecordset("R_PURCHASEOFFER", "PURCHASEOFFERNUMBER, DATECREATED",
                $"PK_R_PURCHASEOFFER = {record.GetPrimaryKeyValue()}", "");
                rsPurchaseOffer.MoveFirst();

                offernumber = rsPurchaseOffer.Fields["PURCHASEOFFERNUMBER"].Value.ToString();
                offeryear = DateTime.Parse(rsPurchaseOffer.Fields["DATECREATED"].Value.ToString()).Year.ToString();

                string subPath = @"\\asp-nlams-smb-1\Company\Aanbiedingen\RidderIQ\purchase-offers\" + offeryear + "\\" + offernumber;

                bool IsExists = System.IO.Directory.Exists(subPath);
                if (!IsExists)
                {
                    System.IO.Directory.CreateDirectory(subPath);
                    //{
                    //System.IO.Directory.CreateDirectory(subPath + "\\Bijlagen");
                    //}
                }

                System.Diagnostics.Process prc = new System.Diagnostics.Process();
                prc.StartInfo.FileName = subPath;
                prc.Start();
            }
        }
        else if (this.FormDataAwareFunctions.TableName == "R_PURCHASEOFFERRELATION")
        {
            // Als niets geselecteerd stoppen
            if (records.Length == 0)
                return;
            foreach (IRecord record in records)
            {
                var rsPurchaseofferRelation = GetRecordset("R_PURCHASEOFFERRELATION", "FK_PURCHASEOFFER",
                    $"PK_R_PURCHASEOFFERRELATION = {record.GetPrimaryKeyValue()}", "");
                rsPurchaseofferRelation.MoveFirst();

                var purchaseOfferId = (int)rsPurchaseofferRelation.Fields["FK_PURCHASEOFFER"].Value;

                var rsPurchaseOffer = GetRecordset("R_PURCHASEOFFER", "PURCHASEOFFERNUMBER, DATECREATED",
                $"PK_R_PURCHASEOFFER = {purchaseOfferId}", "");
                rsPurchaseOffer.MoveFirst();

                offernumber = rsPurchaseOffer.Fields["PURCHASEOFFERNUMBER"].Value.ToString();
                offeryear = DateTime.Parse(rsPurchaseOffer.Fields["DATECREATED"].Value.ToString()).Year.ToString();

                string subPath = @"\\asp-nlams-smb-1\Company\Aanbiedingen\RidderIQ\purchase-offers\" + offeryear + "\\" + offernumber;

                bool IsExists = System.IO.Directory.Exists(subPath);
                if (!IsExists)
                {
                    System.IO.Directory.CreateDirectory(subPath);
                    //{
                    //System.IO.Directory.CreateDirectory(subPath + "\\Bijlagen");
                    //}
                }

                System.Diagnostics.Process prc = new System.Diagnostics.Process();
                prc.StartInfo.FileName = subPath;
                prc.Start();
            }
        }



    }
}
