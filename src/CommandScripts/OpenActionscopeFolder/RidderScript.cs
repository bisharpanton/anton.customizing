﻿using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Ridder.Common;
using Ridder.Common.ADO;
using Ridder.Common.Script;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System;

public class OpenActionscopeFolder_RidderScript : CommandScript
{
    public void Execute()
    {
        IRecord[] records = FormDataAwareFunctions.GetSelectedRecords();

        // Als niets geselecteerd stoppen
        if (records.Length == 0)
            return;

        string dirDigiDossier = @"\\cloud.avananton.nl\dfs\data\Bisharp\Ridder iQ\Actiekader documenten";

        foreach (IRecord record in records)
        {
            string dossiernaam = "";

            if (FormDataAwareFunctions.TableName == "R_TODO")
            {
                var rsTaken = GetRecordset("R_TODO", "FK_ACTIONSCOPE",
                    $"PK_R_TODO = {record.GetPrimaryKeyValue()}", "");
                rsTaken.MoveFirst();

                var rsActiekader = GetRecordset("R_ACTIONSCOPE", "PK_R_ACTIONSCOPE, CODE",
                    $"PK_R_ACTIONSCOPE = {rsTaken.Fields["FK_ACTIONSCOPE"].Value}", "");
                rsActiekader.MoveFirst();

                dossiernaam = rsActiekader.Fields["CODE"].Value.ToString();
            }
            else if (FormDataAwareFunctions.TableName == "R_INBOUNDMESSAGE")
            {
                var rsBerichten = GetRecordset("R_INBOUNDMESSAGE", "FK_ACTIONSCOPE",
                    $"PK_R_INBOUNDMESSAGE = {record.GetPrimaryKeyValue()}", "");
                rsBerichten.MoveFirst();

                var rsActiekader = GetRecordset("R_ACTIONSCOPE", "CODE",
                    $"PK_R_ACTIONSCOPE = {rsBerichten.Fields["FK_ACTIONSCOPE"].Value}", "");
                rsActiekader.MoveFirst();

                dossiernaam = rsActiekader.Fields["CODE"].Value.ToString();
            }
            else
            {
                var rsActiekader = GetRecordset("R_ACTIONSCOPE", "CODE",
                    $"PK_R_ACTIONSCOPE = {record.GetPrimaryKeyValue()}", "");
                rsActiekader.MoveFirst();

                dossiernaam = rsActiekader.Fields["CODE"].Value.ToString();
            }

            if (dossiernaam == "")
            {
                MessageBox.Show("Geen DigiDossier gevonden", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return;
            }

            string subPath = $@"{dirDigiDossier}\\{dossiernaam}";

            bool IsExists = System.IO.Directory.Exists(subPath);
            if (!IsExists)
            {
                System.IO.Directory.CreateDirectory(subPath);
            }

            System.Diagnostics.Process prc = new System.Diagnostics.Process();
            prc.StartInfo.FileName = subPath;
            prc.Start();
        }
    }
}
