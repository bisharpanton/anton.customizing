﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class WkaWerknemer_VoegDocumentTypeToe_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //24-4-2023
        //Voeg documenttype toe

        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("U_RELATIONWKAEMPLOYEES"))
        {
            MessageBox.Show("Dit script mag alleen vanaf de relatie WKA werknemers aangeroepen worden.");
            return;
        }

        if (FormDataAwareFunctions.FormParent == null || !FormDataAwareFunctions.FormParent.TableName.Equals("R_RELATION"))
        {
            MessageBox.Show("Dit script mag alleen vanaf de relatie WKA werknemers binnen een relatie aangeroepen worden.");
            return;
        }

        if(!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        if (FormDataAwareFunctions.GetSelectedRecords().Count() > 1)
        {
            MessageBox.Show("Dit script mag per 1 WKA werknemer aangeroepen worden.");
            return;
        }

        var relationId = (int)FormDataAwareFunctions.FormParent.CurrentRecord.GetPrimaryKeyValue();

        var relation = GetRecordset("R_RELATION", "NAME, COCNUMBER, WKA", $"PK_R_RELATION = {relationId}", "")
            .DataTable.AsEnumerable().First();

        if (!relation.Field<bool>("WKA"))
        {
            MessageBox.Show("Bij deze relatie is geen WKA van toepassing.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        if (string.IsNullOrEmpty(relation.Field<string>("COCNUMBER")))
        {
            MessageBox.Show("Bij deze relatie is geen KVK nummer bekend. Dit is wel nodig voor het centrale WKA dossier. ", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        var currentRelationEmployee = GetRecordset("U_RELATIONWKAEMPLOYEES", "",
            $"PK_U_RELATIONWKAEMPLOYEES = {FormDataAwareFunctions.GetSelectedRecords().First().GetPrimaryKeyValue()}", "")
            .DataTable.AsEnumerable().First();

        var existingDocuments = currentRelationEmployee.Field<string>("REQUIREDDOCUMENTS").Split(',').Select(x => x.Trim()).ToList();

        var result = OpenMultiSelectForm("U_WKADOCUMENTTYPE", "DESCRIPTION", $"NOT DESCRIPTION IN ('{string.Join("','", existingDocuments)}')");

        if(result == null)
        {
            return;
        }


        var newTotalDocuments = existingDocuments.Concat(result.Select(x => x.ToString()).ToList()).ToList();

        var wkaEmployeeInfo = new WkaEmployee()
        {
            RelationCocNumber = relation.Field<string>("COCNUMBER"),
            FirstName = currentRelationEmployee.Field<string>("FIRSTNAME"),
            NamePrefix = currentRelationEmployee.Field<string>("NAMEPREFIX"),
            LastName = currentRelationEmployee.Field<string>("LASTNAME"),
            BSN = currentRelationEmployee.Field<string>("BSN"),
            RegNo = currentRelationEmployee.Field<string>("REGNO"),
            CocNumber = currentRelationEmployee.Field<string>("COCNUMBER"),
            RequiredDocumentTypes = newTotalDocuments,
        };

        var wfCreateWkaEmployeeAtApi = new Guid("788d7efa-d470-4ecd-a0e8-2f0e8199ee01");
        var wfParameters = new Dictionary<string, object>();

        wfParameters.Add("FirstName", wkaEmployeeInfo.FirstName);
        wfParameters.Add("NamePrefix", wkaEmployeeInfo.NamePrefix);
        wfParameters.Add("LastName", wkaEmployeeInfo.LastName);
        wfParameters.Add("BSN", wkaEmployeeInfo.BSN);
        wfParameters.Add("RegNo", wkaEmployeeInfo.RegNo);
        wfParameters.Add("CocNumber", wkaEmployeeInfo.CocNumber);
        wfParameters.Add("RequiredDocumentTypes", string.Join(";", wkaEmployeeInfo.RequiredDocumentTypes));

        var wfResult = ExecuteWorkflowEvent("R_RELATION", relationId, wfCreateWkaEmployeeAtApi, wfParameters);

        if (wfResult.HasError)
        {
            MessageBox.Show($"Uitvoeren workflow om WKA werknemer naar API te sturen is mislukt, oorzaak: {wfResult.GetResult()}", "Ridder iQ",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        FormDataAwareFunctions.Refres();
    }

    class WkaEmployee
    {
        public string RelationCocNumber { get; set; }
        public string FirstName { get; set; }
        public string NamePrefix { get; set; }
        public string LastName { get; set; }
        public string BSN { get; set; }
        public string RegNo { get; set; }
        public string CocNumber { get; set; }
        public List<string> RequiredDocumentTypes { get; set; }
    }
}
