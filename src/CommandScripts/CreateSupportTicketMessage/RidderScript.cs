﻿using ADODB;
using Ridder.Client.SDK.SDKParameters;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class CreateSupportTicketMessage_RidderScript : CommandScript
{
	public void Execute()
	{
        //SP
        //22-8-2024
        //Genereer bericht in ticketsysteem

        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("R_INBOUNDMESSAGE"))
        {
            MessageBox.Show("Dit script mag alleen vanaf de tabel 'Berichten' aangeroepen worden.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        if (FormDataAwareFunctions.FormParent == null || !FormDataAwareFunctions.FormParent.TableName.Equals("R_ACTIONSCOPE"))
        {
            MessageBox.Show("Dit script mag alleen vanaf de tabel 'Berichten' binnen een Actiekader aangeroepen worden.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        var actionScopeId = (int)FormDataAwareFunctions.FormParent.CurrentRecord.GetPrimaryKeyValue();
        var relationId = (int)FormDataAwareFunctions.FormParent.CurrentRecord.GetCurrentRecordValue("FK_RELATION");
        var externalPortalUserId = (int)FormDataAwareFunctions.FormParent.CurrentRecord.GetCurrentRecordValue("FK_BISHARPPORTALUSER");
        var code = (string)FormDataAwareFunctions.FormParent.CurrentRecord.GetCurrentRecordValue("CODE");

        var contactId = GetRecordset("U_BISHARPPORTALUSER", "FK_CONTACT",
               $"PK_U_BISHARPPORTALUSER = {externalPortalUserId}", "").DataTable.AsEnumerable().First().Field<int?>("FK_CONTACT") ?? 0;

        var currentEmployeeId = GetRecordset("R_USER", "FK_EMPLOYEE",
               $"PK_R_USER = {CurrentUserInfo.CurrentUserId}", "").DataTable.AsEnumerable().First().Field<int?>("FK_EMPLOYEE") ?? 0;

        if (currentEmployeeId == 0)
        {
            MessageBox.Show("Geen werknemer gevonden voor huidige gebruiker.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        var employeeEmail = GetRecordset("R_EMPLOYEE", "EMAIL",
            $"PK_R_EMPLOYEE = {currentEmployeeId}", "").DataTable.AsEnumerable().First().Field<string>("EMAIL");

        if (employeeEmail == "")
        {
            MessageBox.Show("Geen email gevonden voor huidige gebruiker.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        var internalContactId = GetRecordset("R_CONTACT", "PK_R_CONTACT",
               $"EMAIL = '{employeeEmail}' AND FK_RELATION = 9861", "").DataTable.AsEnumerable().First().Field<int?>("PK_R_CONTACT") ?? 0;

        if (internalContactId == 0)
        {
            MessageBox.Show("Geen contactpersoon gevonden voor huidige gebruiker.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        var portalUserId = GetRecordset("U_BISHARPPORTALUSER", "PK_U_BISHARPPORTALUSER",
               $"EMAIL = '{employeeEmail}' AND FK_CONTACT = {internalContactId}", "").DataTable.AsEnumerable().First().Field<int?>("PK_U_BISHARPPORTALUSER") ?? 0;

        if (portalUserId == 0)
        {
            MessageBox.Show("Geen portal user gevonden voor huidige gebruiker.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        var createdMessageId = CreateMessage(actionScopeId, relationId, code, currentEmployeeId, contactId, portalUserId);

        //Bisharp Test: bf90f316-d3fe-463b-a827-d7243bcb8d90
        //Bisharp: 026fb036-6fa6-494a-b6cc-5e53b04cd541

        Guid wfInBehandeling = new Guid("bf90f316-d3fe-463b-a827-d7243bcb8d90");

        ExecuteWorkflowEvent("R_ACTIONSCOPE", actionScopeId, wfInBehandeling, null);

        var editParameter = new EditParameters()
        {
            TableName = "R_INBOUNDMESSAGE",
            Id = createdMessageId,
        };

        OpenEdit(editParameter, false);

    }

    private int CreateMessage(int actionScopeId, int relationId, string code, int currentEmployeeId, int contactId, int portalUserId)
    {
        var rsMessage = GetRecordset("R_INBOUNDMESSAGE", "", "PK_R_INBOUNDMESSAGE = -1", "");
        rsMessage.UseDataChanges = true;
        rsMessage.UpdateWhenMoveRecord = false;

        rsMessage.AddNew();

        rsMessage.SetFieldValue("FK_ACTIONSCOPE", actionScopeId);
        rsMessage.SetFieldValue("FK_BISHARPPORTALUSER", portalUserId);
        rsMessage.SetFieldValue("FK_RELATION", relationId);

        rsMessage.SetFieldValue("FK_CONTACT", contactId);
        rsMessage.SetFieldValue("FK_EMPLOYEE", currentEmployeeId);
        rsMessage.SetFieldValue("FK_RECEIVER", currentEmployeeId);
        rsMessage.SetFieldValue("FK_INBOUNDMESSAGETYPE", 4);//PORTALOUT
        rsMessage.SetFieldValue("DESCRIPTION", $"Bericht op ticket - {code}");

        rsMessage.Update();

        return (int)rsMessage.GetField("PK_R_INBOUNDMESSAGE").Value;
    }
}
