﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using System.IO;
using Ridder.Common.Script;

public class Order___Ordermap_openen_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB

        //RK
        //11-01-2024
        //Ordermap openen vanuit project
        if (FormDataAwareFunctions == null || 
                (FormDataAwareFunctions.TableName != "R_ORDER" && FormDataAwareFunctions.TableName != "R_JOBORDER" && FormDataAwareFunctions.TableName != "R_MAINPROJECT"))
        {
            MessageBox.Show("Dit script mag alleen vanaf de order, bon of projecten tabel aangeroepen worden.");
            return;
        }

        if (!FormDataAwareFunctions.GetSelectedRecords().Any())
        {
            return;
        }

        var orderNumbers = GetOrderNumbers(FormDataAwareFunctions);
        if(orderNumbers.Count == 0)
        {
            MessageBox.Show("Geen orders gevonden, probeer het opnieuw");
            return;
        }

        foreach (var orderNumber in orderNumbers)
        {
            string subPath = Path.Combine(haalLocatieOp("ORDERLOCATION"), orderNumber.ToString());
            
            if (!Directory.Exists(subPath))
            {
                MessageBox.Show("Ordermap bestaat niet. Advies is om deze handmatig aan te maken.");
                return;
            }

            /*System.Diagnostics.Process prc = new System.Diagnostics.Process();
            prc.StartInfo.FileName = subPath;
            prc.Start();*/
            System.Diagnostics.Process.Start("explorer.exe", subPath);
        }
    }

    private List<int> GetOrderNumbers(IFormDataAwareFunctions formDataAwareFunctions)
    {
        if (formDataAwareFunctions.TableName.Equals("R_ORDER"))
        {
            var orderIds = formDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue())
                .Distinct().ToList();

            return GetRecordset("R_ORDER", "ORDERNUMBER",
                    $"PK_R_ORDER IN ({string.Join(",", orderIds)})", "")
                .DataTable.AsEnumerable().Select(x => x.Field<int>("ORDERNUMBER")).ToList();
        }
        else if(formDataAwareFunctions.TableName.Equals("R_JOBORDER"))
        {
            //TableName = Bonnen
            var jobOrderIds = formDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue())
                .ToList();

            var orderIds = GetRecordset("R_JOBORDER", "FK_ORDER",
                    $"PK_R_JOBORDER IN ({string.Join(",", jobOrderIds)})", "")
                .DataTable.AsEnumerable().Select(x => x.Field<int>("FK_ORDER")).Distinct().ToList();

            return GetRecordset("R_ORDER", "ORDERNUMBER",
                    $"PK_R_ORDER IN ({string.Join(",", orderIds)})", "")
                .DataTable.AsEnumerable().Select(x => x.Field<int>("ORDERNUMBER")).ToList();
        } else if (formDataAwareFunctions.TableName.Equals("R_MAINPROJECT"))
        {
            var mainProjectIds = formDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue()).ToList();

            var orderIds = GetRecordset("R_MAINPROJECT", "FK_ORDER", $"PK_R_MAINPROJECT IN ({string.Join(",", mainProjectIds)})", "")
                .DataTable.AsEnumerable().Select(x => x.Field<int>("FK_ORDER")).Distinct().ToList();

            return GetRecordset("R_ORDER", "ORDERNUMBER", $"PK_R_ORDER IN ({string.Join(",", orderIds)})", "").DataTable.AsEnumerable().Select(x => x.Field<int>("ORDERNUMBER")).ToList();
        } else
        {
            return new List<int>();
        }
    }

    public string haalLocatieOp(string s)
    {
        var rsCRMSettings = GetRecordset("R_CRMSETTINGS", s, "", "");
        rsCRMSettings.MoveFirst();

        return rsCRMSettings.Fields[s].Value.ToString();
    }
}
