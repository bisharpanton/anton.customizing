﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using Ridder.Client.SDK.SDKParameters;

public class CreateCreditSalesInvoice_RidderScript : CommandScript
{
    public bool invoiceDeleted = false;

    public void Execute()
    {
        //SP
        //23-5-2024
        //Maak creditfactuur voor geselecteerde factuurregels

        if (FormDataAwareFunctions == null || FormDataAwareFunctions.TableName != "R_SALESINVOICE")
        {
            MessageBox.Show("Dit script is alleen vanaf de tabel 'Verkoopfacturen' aan te roepen.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        IRecord[] records = FormDataAwareFunctions.GetSelectedRecords();

        if (records.Length > 1)
        {
            MessageBox.Show("Dit script is voor één factuur tegelijk aan te roepen.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        int invoiceId = (int)records[0].GetPrimaryKeyValue();

        var invoiceDetails = GetRecordset("R_SALESINVOICEALLDETAIL",
            "FK_SALESINVOICEDETAILASSEMBLY, FK_SALESINVOICEDETAILINSTALLMENT, FK_SALESINVOICEDETAILITEM, FK_SALESINVOICEDETAILJOBORDER, FK_SALESINVOICEDETAILMISC, " +
            "FK_SALESINVOICEDETAILOUTSOURCED, FK_SALESINVOICEDETAILRENTALTYPE, FK_SALESINVOICEDETAILSERVICECONTRACT, FK_SALESINVOICEDETAILTEXT, FK_SALESINVOICEDETAILWORKACTIVITY",
            $"FK_SALESINVOICE = {invoiceId}", "LINENUMBER ASC").DataTable.AsEnumerable();

        if (!invoiceDetails.Any())
        {
            MessageBox.Show("Geen verkoopfactuurregels aanwezig. Crediteren niet mogelijk", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        var newInvoiceId = CopyInvoiceHeader(invoiceId);

        foreach (var invoiceDetail in invoiceDetails)
        {
            if (invoiceDetail.Field<int?>("FK_SALESINVOICEDETAILASSEMBLY").HasValue)
            {
                CreateObjectToCopy(newInvoiceId, "credit", invoiceDetail.Field<int>("FK_SALESINVOICEDETAILASSEMBLY"), "R_SALESINVOICEDETAILASSEMBLY", "PK_R_SALESINVOICEDETAILASSEMBLY", "FK_ASSEMBLY");
            }

            if (invoiceDetail.Field<int?>("FK_SALESINVOICEDETAILINSTALLMENT").HasValue)
            {
                CreateObjectToCopy(newInvoiceId, "credit", invoiceDetail.Field<int>("FK_SALESINVOICEDETAILINSTALLMENT"), "R_SALESINVOICEDETAILINSTALLMENT", "PK_R_SALESINVOICEDETAILINSTALLMENT", "FK_SALESINSTALLMENT");
            }

            if (invoiceDetail.Field<int?>("FK_SALESINVOICEDETAILITEM").HasValue)
            {
                CreateObjectToCopy(newInvoiceId, "credit", invoiceDetail.Field<int>("FK_SALESINVOICEDETAILITEM"), "R_SALESINVOICEDETAILITEM", "PK_R_SALESINVOICEDETAILITEM", "FK_ITEM");
            }

            if (invoiceDetail.Field<int?>("FK_SALESINVOICEDETAILJOBORDER").HasValue)
            {
                CreateObjectToCopy(newInvoiceId, "credit", invoiceDetail.Field<int>("FK_SALESINVOICEDETAILJOBORDER"), "R_SALESINVOICEDETAILJOBORDER", "PK_R_SALESINVOICEDETAILJOBORDER", "FK_JOBORDER");
            }

            if (invoiceDetail.Field<int?>("FK_SALESINVOICEDETAILMISC").HasValue)
            {
                CreateObjectToCopy(newInvoiceId, "credit", invoiceDetail.Field<int>("FK_SALESINVOICEDETAILMISC"), "R_SALESINVOICEDETAILMISC", "PK_R_SALESINVOICEDETAILMISC", "FK_MISCELLANEOUS");
            }

            if (invoiceDetail.Field<int?>("FK_SALESINVOICEDETAILOUTSOURCED").HasValue)
            {
                CreateObjectToCopy(newInvoiceId, "credit", invoiceDetail.Field<int>("FK_SALESINVOICEDETAILOUTSOURCED"), "R_SALESINVOICEDETAILOUTSOURCED", "PK_R_SALESINVOICEDETAILOUTSOURCED", "FK_OUTSOURCEDACTIVITY");
            }

            if (invoiceDetail.Field<int?>("FK_SALESINVOICEDETAILRENTALTYPE").HasValue)
            {
                CreateObjectToCopy(newInvoiceId, "credit", invoiceDetail.Field<int>("FK_SALESINVOICEDETAILRENTALTYPE"), "R_SALESINVOICEDETAILRENTALTYPE", "PK_R_SALESINVOICEDETAILRENTALTYPE", "FK_RENTALTYPE");
            }

            if (invoiceDetail.Field<int?>("FK_SALESINVOICEDETAILSERVICECONTRACT").HasValue)
            {
                CreateObjectToCopy(newInvoiceId, "credit", invoiceDetail.Field<int>("FK_SALESINVOICEDETAILSERVICECONTRACT"), "R_SALESINVOICEDETAILSERVICECONTRACT", "PK_R_SALESINVOICEDETAILSERVICECONTRACT", "FK_SERVICECONTRACT");
            }

            if (invoiceDetail.Field<int?>("FK_SALESINVOICEDETAILTEXT").HasValue)
            {
                CreateObjectToCopy(newInvoiceId, "credit", invoiceDetail.Field<int>("FK_SALESINVOICEDETAILTEXT"), "R_SALESINVOICEDETAILTEXT", "PK_R_SALESINVOICEDETAILTEXT", "");
            }

            if (invoiceDetail.Field<int?>("FK_SALESINVOICEDETAILWORKACTIVITY").HasValue)
            {
                CreateObjectToCopy(newInvoiceId, "credit", invoiceDetail.Field<int>("FK_SALESINVOICEDETAILWORKACTIVITY"), "R_SALESINVOICEDETAILWORKACTIVITY", "PK_R_SALESINVOICEDETAILWORKACTIVITY", "FK_WORKACTIVITY");
            }
        }

        if(!invoiceDeleted)
        {
            var editPar = new EditParameters()
            {
                TableName = "R_SALESINVOICE",
                Id = newInvoiceId
            };

            OpenEdit(editPar, false);
        }
    }

    private int CopyInvoiceHeader(int invoiceId)
    {
        var originalInvoice = GetRecordset("R_SALESINVOICE", "",
            $"PK_R_SALESINVOICE = {invoiceId}", "").DataTable.AsEnumerable().First();

        var rsSalesInvoice = GetRecordset("R_SALESINVOICE", "", "PK_R_SALESINVOICE = -1", "");
        rsSalesInvoice.UseDataChanges = true;
        rsSalesInvoice.AddNew();
        
        rsSalesInvoice.Fields["FK_ORDERRELATION"].Value = originalInvoice.Field<int>("FK_ORDERRELATION");
        rsSalesInvoice.Fields["FK_INVOICERELATION"].Value = originalInvoice.Field<int>("FK_INVOICERELATION");
        rsSalesInvoice.Fields["FK_PAYMENTTERM"].Value = originalInvoice.Field<int>("FK_PAYMENTTERM");
        //rsSalesInvoice.Fields["FK_INCOTERMS"].Value = originalInvoice.Field<int>("FK_INCOTERMS");

        rsSalesInvoice.Fields["ENTRYDATE"].Value = originalInvoice.Field<DateTime>("ENTRYDATE");
        rsSalesInvoice.Fields["ANTONBOOKDATE"].Value = originalInvoice.Field<DateTime>("ANTONBOOKDATE");
        rsSalesInvoice.Fields["EXCHANGERATEDATE"].Value = originalInvoice.Field<DateTime>("EXCHANGERATEDATE");
        rsSalesInvoice.Fields["EXPECTEDPAYDATE"].Value = originalInvoice.Field<DateTime>("EXPECTEDPAYDATE");
        rsSalesInvoice.Fields["INVOICEDATE"].Value = originalInvoice.Field<DateTime>("INVOICEDATE");
        rsSalesInvoice.Fields["PAYABLEDATE"].Value = originalInvoice.Field<DateTime>("PAYABLEDATE");
        rsSalesInvoice.Fields["PAYABLEDATEPAYMENTDISCOUNT"].Value = originalInvoice.Field<DateTime>("PAYABLEDATEPAYMENTDISCOUNT");

        rsSalesInvoice.Fields["MEMO"].Value = originalInvoice.Field<string>("MEMO");
        rsSalesInvoice.Fields["INTERNALMEMO"].Value = originalInvoice.Field<string>("INTERNALMEMO");
        rsSalesInvoice.Fields["HEADERMEMO"].Value = originalInvoice.Field<string>("HEADERMEMO");
        rsSalesInvoice.Fields["FOOTERMEMO"].Value = originalInvoice.Field<string>("FOOTERMEMO");

        rsSalesInvoice.Fields["PLAINTEXT_MEMO"].Value = originalInvoice.Field<string>("PLAINTEXT_MEMO");
        rsSalesInvoice.Fields["PLAINTEXT_INTERNALMEMO"].Value = originalInvoice.Field<string>("PLAINTEXT_INTERNALMEMO");
        rsSalesInvoice.Fields["PLAINTEXT_HEADERMEMO"].Value = originalInvoice.Field<string>("PLAINTEXT_HEADERMEMO");
        rsSalesInvoice.Fields["PLAINTEXT_FOOTERMEMO"].Value = originalInvoice.Field<string>("PLAINTEXT_FOOTERMEMO");

        var description = (originalInvoice.Field<int>("FK_LANGUAGE") == 1) ? "Creditfactuur" : "Credit invoice";

        if (!originalInvoice.Field<int?>("FK_JOURNALENTRY").HasValue)
        {
            throw new Exception("Verkoopfactuur nog niet gejournaliseerd. Crediteren niet mogelijk.");
        }

        var invoiceNumber = GetRecordset("R_JOURNALENTRY", "JOURNALENTRYNUMBER",
            $"PK_R_JOURNALENTRY = {originalInvoice.Field<int>("FK_JOURNALENTRY")}", "").DataTable.AsEnumerable().First().Field<int>("JOURNALENTRYNUMBER");

        rsSalesInvoice.Fields["DESCRIPTION"].Value = $"{description} {invoiceNumber}";

        var updateResult = rsSalesInvoice.Update2();

        return (int)updateResult[0].PrimaryKey;
    }

    private void CreateObjectToCopy(int newInvoiceId, string balanceSide, int invoiceDetailId, string tablename, string pkname, string fksource)
    {
        double factor = (balanceSide == "credit") ? -1.0 : 1.0;
        long longFactor = (balanceSide == "credit") ? -1 : 1;

        if (tablename == "R_SALESINVOICEDETAILWORKACTIVITY")
        {
            var originalInvoiceDetail = GetRecordset($"{tablename}", $"",
            $"{pkname} = {invoiceDetailId}", "")
            .DataTable.AsEnumerable().Select(x => new OriginalInvoiceDetail()
            {
                FkInvoice = x.Field<int>("FK_SALESINVOICE"),
                FkSource = x.Field<int?>($"{fksource}") ?? 0,
                FkOrder = x.Field<int?>("FK_ORDER") ?? 0,
                FkGlAccount = x.Field<int?>("FK_GLACCOUNT") ?? 0,
                FkCostCenter = x.Field<int?>("FK_COSTCENTER") ?? 0,
                FkCostUnit = x.Field<int?>("FK_COSTUNIT") ?? 0,
                FkVatCode = x.Field<int?>("FK_VATCODE") ?? 0,
                FkDetailGroup = x.Field<int?>("FK_DETAILGROUP") ?? 0,
                FkIncoTerm = x.Field<Guid?>("FK_INCOTERM") ?? Guid.Empty,
                FkRevenueCode = x.Field<int?>("FK_REVENUECODE") ?? 0,
                Description = x.Field<string>("DESCRIPTION"),
                Memo = x.Field<string>("MEMO"),
                TimeSpan = x.Field<long>("TIMESPAN"),
                NetSalesAmount = x.Field<double>("NETSALESAMOUNT") * factor,
                GrossSalesPrice = x.Field<double>("GROSSSALESPRICE") * factor,
                CostPrice = x.Field<double>("COSTPRICE") * factor

            }).First();

            CopyInvoiceDetail(newInvoiceId, originalInvoiceDetail, tablename, pkname, fksource, balanceSide);
        }
        else if (tablename == "R_SALESINVOICEDETAILTEXT")
        {
            var originalofferdetail = GetRecordset($"{tablename}", $"FK_SALESINVOICE, DESCRIPTION, MEMO, FK_DETAILGROUP",
            $"{pkname} = {invoiceDetailId}", "")
            .DataTable.AsEnumerable().Select(x => new OriginalInvoiceDetail()
            {
                FkInvoice = x.Field<int>("FK_SALESINVOICE"),
                FkDetailGroup = x.Field<int?>("FK_DETAILGROUP") ?? 0,
                Description = x.Field<string>("DESCRIPTION"),
                Memo = x.Field<string>("MEMO")

            }).First();

            CopyInvoiceDetail(newInvoiceId, originalofferdetail, tablename, pkname, fksource, balanceSide);
        }
        else if (tablename == "R_SALESINVOICEDETAILINSTALLMENT")
        {
            var originalInvoiceDetail = GetRecordset($"{tablename}", $"",
            $"{pkname} = {invoiceDetailId}", "")
            .DataTable.AsEnumerable().Select(x => new OriginalInvoiceDetail()
            {
                FkInvoice = x.Field<int>("FK_SALESINVOICE"),
                FkSource = GetDefaultMisc(),
                FkOrder = x.Field<int?>("FK_ORDER") ?? 0,
                FkGlAccount = x.Field<int?>("FK_GENERALLEDGERACCOUNT") ?? 0,
                FkCostCenter = x.Field<int?>("FK_COSTCENTER") ?? 0,
                FkCostUnit = x.Field<int?>("FK_COSTUNIT") ?? 0,
                FkVatCode = x.Field<int?>("FK_VATCODE") ?? 0,
                FkDetailGroup = x.Field<int?>("FK_DETAILGROUP") ?? 0,
                FkRevenueCode = x.Field<int?>("FK_REVENUECODE") ?? 0,
                Description = x.Field<string>("DESCRIPTION"),
                Memo = x.Field<string>("MEMO"),
                NetSalesAmount = x.Field<double>("NETSALESAMOUNT") * factor,
                Quantity = 1.0

            }).First();

            var orderClosed = CheckOrderState((int)originalInvoiceDetail.FkOrder) == 0;
            if(orderClosed )//Order is afgesloten, we moeten tegenboeken met een divers
            {
                CopyInvoiceDetail(newInvoiceId, originalInvoiceDetail, "R_SALESINVOICEDETAILMISC", "PK_R_SALESINVOICEDETAILMISC", "FK_MISCELLANEOUS", balanceSide);
            }
            else//Order is nog niet afgesloten, termijnen moeten handmatig worden tegengeboekt
            {
                MessageBox.Show(
                        $"Crediteren van factuurregel termijn is mislukt, oorzaak: order {GetRecordTag("R_ORDER", originalInvoiceDetail.FkOrder)} is nog niet afgesloten. " +
                        $"Termijnen dienen handmatig te worden tegengeboekt.",
                        "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);

                invoiceDeleted = DeleteCreatedSalesInvoice(newInvoiceId);

                return;
            }            
        }
        else
        {
            var originalofferdetail = GetRecordset($"{tablename}", $"",
            $"{pkname} = {invoiceDetailId}", "")
            .DataTable.AsEnumerable().Select(x => new OriginalInvoiceDetail()
            {
                FkInvoice = x.Field<int>("FK_SALESINVOICE"),
                FkSource = x.Field<int?>($"{fksource}") ?? 0,
                FkOrder = x.Field<int?>("FK_ORDER") ?? 0,
                FkGlAccount = x.Field<int?>("FK_GLACCOUNT") ?? 0,
                FkCostCenter = x.Field<int?>("FK_COSTCENTER") ?? 0,
                FkCostUnit = x.Field<int?>("FK_COSTUNIT") ?? 0,
                FkVatCode = x.Field<int?>("FK_VATCODE") ?? 0,
                FkDetailGroup = x.Field<int?>("FK_DETAILGROUP") ?? 0,
                FkIncoTerm = x.Field<Guid?>("FK_INCOTERM") ?? Guid.Empty,
                FkRevenueCode = x.Field<int?>("FK_REVENUECODE") ?? 0,
                Description = x.Field<string>("DESCRIPTION"),
                Memo = x.Field<string>("MEMO"),
                Quantity = x.Field<double>("QUANTITY"),
                NetSalesAmount = x.Field<double>("NETSALESAMOUNT") * factor,
                GrossSalesPrice = x.Field<double>("GROSSSALESPRICE") * factor,
                CostPrice = x.Field<double>("COSTPRICE") * factor

            }).First();

            CopyInvoiceDetail(newInvoiceId, originalofferdetail, tablename, pkname, fksource, balanceSide);
        }
    }

    private int? GetDefaultMisc()
    {
        return GetRecordset("R_MISC", "PK_R_MISC",
            $"CODE = '-'", "").DataTable.AsEnumerable().First().Field<int>("PK_R_MISC");
    }

    private bool DeleteCreatedSalesInvoice(int newInvoiceId)
    {
        var rsSalesInvoice = GetRecordset("R_SALESINVOICE", "",
            $"PK_R_SALESINVOICE = {newInvoiceId}", "");
        rsSalesInvoice.UpdateWhenMoveRecord = false;
        rsSalesInvoice.UseDataChanges = false;

        rsSalesInvoice.MoveFirst();
        while (!rsSalesInvoice.EOF)
        {
            rsSalesInvoice.Delete();
            rsSalesInvoice.MoveNext();
        }

        rsSalesInvoice.MoveFirst();

        var updateResult = rsSalesInvoice.Update2();

        if (updateResult != null && updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Het verwijderen de aangemaakte verkoopfactuur is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return false;
        }

        return true;
    }

    private int CheckOrderState(int orderId)
    {
        if(orderId == 0)
        {
            return 0;
        }
        else
        {
            var orderClosed = GetRecordset("R_ORDER", "DATUMAFGESLOTEN",
            $"PK_R_ORDER = {orderId}", "").DataTable.AsEnumerable().First().Field<DateTime?>("DATUMAFGESLOTEN").HasValue;

            if (orderClosed)
            {
                return 0;
            }
            else
            {
                return orderId;
            }
        }
    }

    private void CopyInvoiceDetail(int newInvoiceId, OriginalInvoiceDetail originalInvoiceDetail, string tablename, string pkname, string fksource, string balanceSide)
    {
        var rsInvoiceDetail = GetRecordset($"{tablename}", "", $"{pkname} = -1", "");
        rsInvoiceDetail.UseDataChanges = true;
        rsInvoiceDetail.UpdateWhenMoveRecord = false;

        rsInvoiceDetail.AddNew();
        rsInvoiceDetail.Fields["FK_SALESINVOICE"].Value = newInvoiceId;
        rsInvoiceDetail.Fields["FK_DETAILGROUP"].Value = originalInvoiceDetail.FkDetailGroup;

        if (tablename == "R_SALESINVOICEDETAILWORKACTIVITY")
        {
            rsInvoiceDetail.Fields["TIMESPAN"].Value = originalInvoiceDetail.TimeSpan;
        }
        else if (tablename != "R_SALESINVOICEDETAILTEXT" && tablename != "R_SALESINVOICEDETAILINSTALLMENT")
        {
            rsInvoiceDetail.Fields["QUANTITY"].Value = originalInvoiceDetail.Quantity;
        }

        if (tablename != "R_SALESINVOICEDETAILTEXT")
        {
            rsInvoiceDetail.Fields[$"{fksource}"].Value = originalInvoiceDetail.FkSource;
            if(tablename  != "R_SALESINVOICEDETAILINSTALLMENT")
            {
                rsInvoiceDetail.Fields["FK_GLACCOUNT"].Value = originalInvoiceDetail.FkGlAccount;
                rsInvoiceDetail.Fields["GROSSSALESPRICE"].Value = originalInvoiceDetail.GrossSalesPrice;
                rsInvoiceDetail.Fields["COSTPRICE"].Value = originalInvoiceDetail.CostPrice;

                if (originalInvoiceDetail.FkIncoTerm.HasValue)
                {
                    rsInvoiceDetail.Fields["FK_INCOTERM"].Value = originalInvoiceDetail.FkIncoTerm;
                }
                else
                {
                    rsInvoiceDetail.Fields["FK_INCOTERM"].Value = DBNull.Value;
                }
            }

            rsInvoiceDetail.Fields["FK_COSTCENTER"].Value = originalInvoiceDetail.FkCostCenter;
            rsInvoiceDetail.Fields["FK_COSTUNIT"].Value = originalInvoiceDetail.FkCostUnit;
            rsInvoiceDetail.Fields["FK_REVENUECODE"].Value = originalInvoiceDetail.FkRevenueCode;
            rsInvoiceDetail.Fields["FK_VATCODE"].Value = originalInvoiceDetail.FkVatCode;
            rsInvoiceDetail.Fields["NETSALESAMOUNT"].Value = originalInvoiceDetail.NetSalesAmount;

            if(tablename != "R_SALESINVOICEDETAILSERVICECONTRACT" && tablename != "R_SALESINVOICEDETAILRENTALTYPE")
            {
                var orderId = CheckOrderState((int)originalInvoiceDetail.FkOrder);
                if(orderId != 0)
                {
                    rsInvoiceDetail.Fields["FK_ORDER"].Value = orderId;
                }
            }
        }

        string desc = (balanceSide == "credit") ? "Credit - " + originalInvoiceDetail.Description : originalInvoiceDetail.Description;

        rsInvoiceDetail.Fields["DESCRIPTION"].Value = desc;
        rsInvoiceDetail.Fields["MEMO"].Value = originalInvoiceDetail.Memo;

        var updateResult = rsInvoiceDetail.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show(
                $"Kopiëren van factuurregel is mislukt, oorzaak {updateResult.First(x => x.HasError).GetResult()}",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }
    }
}

class OriginalInvoiceDetail
{
    public int FkInvoice { get; set; }
    public int? FkSource { get; set; }
    public int? FkOrder { get; set; }
    public int? FkGlAccount { get; set; }
    public int? FkCostCenter { get; set; }
    public int? FkCostUnit { get; set; }
    public int? FkDetailGroup { get; set; }
    public int? FkVatCode { get; set; }
    public Guid? FkIncoTerm { get; set; }
    public int? FkRevenueCode { get; set; }
    public string Description { get; set; }
    public string Memo { get; set; }
    public double Quantity { get; set; }
    public long TimeSpan { get; set; }
    public double NetSalesAmount { get; set; }
    public double GrossSalesPrice { get; set; }
    public double CostPrice { get; set; }
}
