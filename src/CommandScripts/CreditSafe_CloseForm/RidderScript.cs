﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Drawing;
using Ridder.Client.SDK.SDKParameters;

public class CreditSafe_CloseForm_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //29-04-2020
        //Sluit formulier

        if (FormDataAwareFunctions == null || FormDataAwareFunctions.TableName != "C_FORMSEARCHRELATIONONCREDITSAFEDETAILS")
        {
            MessageBox.Show("Dit script mag alleen vanaf het formulier CreditSafe zoek relaties aangeroepen worden.");
            return;
        }

        if (FormDataAwareFunctions.FormParent == null || FormDataAwareFunctions.FormParent.TableName != "C_FORMSEARCHRELATIONONCREDITSAFE")
        {
            MessageBox.Show("Dit script mag alleen vanaf het formulier CreditSafe zoek relaties aangeroepen worden.");
            return;
        }

        FormDataAwareFunctions.Save();

        var formId = (int)FormDataAwareFunctions.FormParent.CurrentRecord.GetPrimaryKeyValue();

        if (!CheckSelectedFormDetails(formId))
        {
            return;
        }

        try
        {
            var selectedSearchResult = GetSelectedSearchResult(formId);

            var chosenRelationType = AskForRelationTypeSectorAndSource();

            if (chosenRelationType == null)
            {
                return;
            }

            var createdCreditSafeRequest = CreateCreditSafeRequest(selectedSearchResult, chosenRelationType);

            if (createdCreditSafeRequest == 0)
            {
                return;
            }

            if (!CreateRelationWithCreditSafeInfo(createdCreditSafeRequest))
            {
                return;
            }

            var createdRelationId = GetCreatedRelation(createdCreditSafeRequest);

            if (createdRelationId == 0)
            {
                return;
            }

            UpdateForm(formId, createdRelationId);

            Form.ActiveForm.Close();
        }
        finally
        {
            Cursor.Current = Cursors.Default;
        }
    }

    private RelationTypeSectorAndSource AskForRelationTypeSectorAndSource()
    {
        var browsePar = new BrowseParameters()
        {
            TableName = "R_RELATIONTYPE", ResultColumnName = "PK_R_RELATIONTYPE", Context = "ChoseRelationType",
        };

        var browseResultRelationType = OpenBrowse(browsePar, true);

        if (browseResultRelationType == null)
        {
            return null;
        }

        var codeRelationType = GetRecordset("R_RELATIONTYPE", "CODE",
            $"PK_R_RELATIONTYPE = {browseResultRelationType}", "").DataTable.AsEnumerable().First().Field<string>("CODE");

        int sector = 0;
        int source = 0; 

        if (codeRelationType.Contains("OPDRG"))
        {
            var sectorResult = AskForSector();

            if (sectorResult == null)
            {
                return null;
            }

            sector = (int)sectorResult;

            if (SourceMandatory(GetUserInfo().CompanyName))
            {
                var sourceResult = AskForSource();

                if (sourceResult == null)
                {
                    return null;
                }

                source = (int)sourceResult;

            }
        }

        return new RelationTypeSectorAndSource()
        {
            RelationType = (int)browseResultRelationType,
            Sector = sector,
            Source = source,
        };
    }

    private bool SourceMandatory(string companyName)
    {
        return companyName.Equals("De Leeuw Protection Systems");
    }

    private object AskForSector()
    {
        var browsePar = new BrowseParameters()
        {
            TableName = "R_INDUSTRY", ResultColumnName = "PK_R_INDUSTRY", Context = "ChoseSector",
        };

        return OpenBrowse(browsePar, true);
    }

    private object AskForSource()
    {
        var browsePar = new BrowseParameters()
        {
            TableName = "R_RELATIONSOURCE", ResultColumnName = "PK_R_RELATIONSOURCE", Context = "ChoseSource",
        };

        return OpenBrowse(browsePar, true);
    }
    
    private void UpdateForm(int formId, int createdRelationId)
    {
        var rsForm = GetRecordset("C_FORMSEARCHRELATIONONCREDITSAFE", "PROCESS, CREATEDRELATIONID",
            $"PK_C_FORMSEARCHRELATIONONCREDITSAFE = {formId}", "");
        rsForm.MoveFirst();
        rsForm.Fields["PROCESS"].Value = true;
        rsForm.Fields["CREATEDRELATIONID"].Value = createdRelationId;
        rsForm.Update();
    }

    private int GetCreatedRelation(int createdCreditSafeRequestId)
    {
        var creditSafeRequest = GetRecordset("C_CREDITSAFEREQUESTS", "FK_RELATION, IMPORTMESSAGE, FK_WORKFLOWSTATE",
                $"PK_C_CREDITSAFEREQUESTS = {createdCreditSafeRequestId}", "")
            .DataTable.AsEnumerable().First();

        var wfStateCancelled = new Guid("c30fde4d-db6a-4e8f-af2f-9b18b3c88dc0");

        if ((Guid)creditSafeRequest.Field<Guid>("FK_WORKFLOWSTATE") == wfStateCancelled)
        {
            MessageBox.Show(creditSafeRequest.Field<string>("IMPORTMESSAGE"), "Ridder iQ",
                MessageBoxButtons.OK, MessageBoxIcon.Error);

            return 0;
        }

        var createdRelation = creditSafeRequest.Field<int?>("FK_RELATION") ?? 0;

        if (createdRelation == 0)
        {
            MessageBox.Show("Geen relatie gevonden in 'CreditSafe request'", "Ridder iQ",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
            return 0;
        }

        return createdRelation;
    }

    private bool CreateRelationWithCreditSafeInfo(int createdCreditSafeRequest)
    {
        var wfImportRelationFromCreditSafe = new Guid("2b0d3015-d70b-4fb4-a143-2b456e060397");

        var wfResult = ExecuteWorkflowEvent("C_CREDITSAFEREQUESTS", createdCreditSafeRequest,
            wfImportRelationFromCreditSafe, null);

        if (wfResult.HasError)
        {
            MessageBox.Show(
                $"Uitvoeren workflow 'Importeer relatie vanuit CreditSafe' mislukt, oorzaak: {wfResult.GetResult()}",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);

            return false;
        }

        if(wfResult.Messages.Any())
        {
            foreach (var message in wfResult.Messages)
            {
                MessageBox.Show(message.Message, "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        return true;
    }

    private int CreateCreditSafeRequest(SearchResult searchResult, RelationTypeSectorAndSource relationTypeSectorAndSource)
    {
        var rsCreditSafeRequests = GetRecordset("C_CREDITSAFEREQUESTS", "",
            "PK_C_CREDITSAFEREQUESTS = -1", "");
        rsCreditSafeRequests.UseDataChanges = false;

        rsCreditSafeRequests.AddNew();
        rsCreditSafeRequests.Fields["CREDITSAFEID"].Value = searchResult.CreditSafeId;
        rsCreditSafeRequests.Fields["COCNUMBER"].Value = searchResult.CocNumber;

        rsCreditSafeRequests.Fields["RELATIONTYPEID"].Value = relationTypeSectorAndSource.RelationType;
        rsCreditSafeRequests.Fields["SECTORID"].Value = relationTypeSectorAndSource.Sector;
        rsCreditSafeRequests.Fields["SOURCEID"].Value = relationTypeSectorAndSource.Source;

        var updateResult = rsCreditSafeRequests.Update2();

        if (updateResult != null && updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Aanmaken nieuw CreditSafe request is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return 0;
        }

        return (int)updateResult.First().PrimaryKey;
    }

    private SearchResult GetSelectedSearchResult(int formId)
    {
        //We vangen in het sluiten van het scherm af of er wel 1 regel is geselecteerd
        return GetRecordset("C_FORMSEARCHRELATIONONCREDITSAFEDETAILS",
                "CREDITSAFEID, COCNUMBER",
                $"FK_FORMSEARCHRELATIONONCREDITSAFE = {formId} AND SELECT = 1", "")
            .DataTable.AsEnumerable().Select(x => new SearchResult()
            {
                CreditSafeId = x.Field<string>("CREDITSAFEID"), CocNumber = x.Field<string>("COCNUMBER"),
            }).First();
    }

    private bool CheckSelectedFormDetails(int formId)
    {
        var numberOfSelectedFormDetails = GetRecordset("C_FORMSEARCHRELATIONONCREDITSAFEDETAILS",
            "PK_C_FORMSEARCHRELATIONONCREDITSAFEDETAILS",
            $"FK_FORMSEARCHRELATIONONCREDITSAFE = {formId} AND SELECT = 1", "").RecordCount;

        if (numberOfSelectedFormDetails == 0)
        {
            MessageBox.Show("Selecteer een relatie uit de zoekresultaten.", 
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return false;
        }

        if (numberOfSelectedFormDetails > 1)
        {
            MessageBox.Show("Selecteer één zoekresulaat.", 
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return false;
        }

        return true;
    }

    class SearchResult
    {
        public string CreditSafeId { get; set; }
        public string CocNumber { get; set; }
    }

    class RelationTypeSectorAndSource
    {
        public int RelationType { get; set; }
        public int Sector { get; set; }
        public int Source { get; set; }
    }
}
