﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CreditSafe_SearchRelations.CreditSafeModels
{
    class Address
    {
        [JsonProperty("simpleValue")]
        public string SimpleValue { get; set; }
        [JsonProperty("street")]
        public string Street { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("postCode")]
        public string PostCode { get; set; }
        [JsonProperty("houseNo")]
        public string HouseNo { get; set; }
        [JsonProperty("telephone")]
        public string Telephone { get; set; }
    }
}
