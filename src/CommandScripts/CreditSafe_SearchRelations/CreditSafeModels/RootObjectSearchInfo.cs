﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CreditSafe_SearchRelations.CreditSafeModels
{
    class RootObjectSearchInfo
    {
        [JsonProperty("totalSize")]
        public int TotalSize { get; set; }
        [JsonProperty("companies")]
        public List<Company> Companies { get; set; }
    }
}
