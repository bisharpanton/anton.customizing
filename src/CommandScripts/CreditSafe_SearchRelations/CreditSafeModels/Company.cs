﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CreditSafe_SearchRelations.CreditSafeModels
{
    class Company
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("regNo")]
        public string RegNo { get; set; }
        [JsonProperty("vatNo")]
        public List<string> VatNumbers { get; set; }
        [JsonProperty("safeNo")]
        public string SafeNo { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("tradingNames")]
        public List<string> TradingNames { get; set; }
        [JsonProperty("address")]
        public Address Address { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("officeType")]
        public string OfficeType { get; set; }
        [JsonProperty("dateOfLatestAccounts")]
        public DateTime DateOfLatestAccounts { get; set; }
        [JsonProperty("dateOfLatestChange")]
        public DateTime DateOfLatestChange { get; set; }
        [JsonProperty("activityCode")]
        public string ActivityCode { get; set; }
        [JsonProperty("statusDescription")]
        public string StatusDescription { get; set; }
    }
}
