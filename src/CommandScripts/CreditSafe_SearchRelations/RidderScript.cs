﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Drawing;
using System.Net.Http;
using System.Net.Http.Headers;
using CreditSafe_SearchRelations.CreditSafeModels;
using CreditSafe_SearchRelations.Models;
using CreditSafe_SearchRelations.TokenModels;
using Ridder.Client.SDK.SDKParameters;

public class CreditSafe_SearchRelations_RidderScript : CommandScript
{
    public void Execute()
    {
        //DB
        //29-4-2020
        //Ga met de ingegeven naam en kvk-nummer op zoek naar relaties in CreditSafe

        if (FormDataAwareFunctions == null || 
            (FormDataAwareFunctions.TableName != "C_FORMSEARCHRELATIONONCREDITSAFEDETAILS" &&
             FormDataAwareFunctions.TableName != "C_FORMSEARCHRELATIONONCREDITSAFE"))
        {
            MessageBox.Show("Dit script mag alleen vanaf het formulier CreditSafe zoek relaties aangeroepen worden.");
            return;
        }

        if (FormDataAwareFunctions.TableName == "C_FORMSEARCHRELATIONONCREDITSAFEDETAILS" && 
            (FormDataAwareFunctions.FormParent == null || FormDataAwareFunctions.FormParent.TableName != "C_FORMSEARCHRELATIONONCREDITSAFE"))
        {
            MessageBox.Show("Dit script mag alleen vanaf het formulier CreditSafe zoek relaties aangeroepen worden.");
            return;
        }

        FormDataAwareFunctions.Save();

        var formId = FormDataAwareFunctions.TableName == "C_FORMSEARCHRELATIONONCREDITSAFEDETAILS"
            ? (int)FormDataAwareFunctions.FormParent.CurrentRecord.GetPrimaryKeyValue()
            : (int)FormDataAwareFunctions.CurrentRecord.GetPrimaryKeyValue();

        var searchInput = GetSearchInput(formId);

        if (string.IsNullOrEmpty(searchInput.Name) && string.IsNullOrEmpty(searchInput.CocNumber))
        {
            MessageBox.Show("Vul een naam of een KVK-nummer in om te zoeken.", "Ridder iQ",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
        }

        if (!DeleteOldSearchResults(formId))
        {
            return;
        }

        var searchResults = SearchOnCreditSafe(searchInput);

        if (searchResults == null || !searchResults.Companies.Any())
        {
            FormDataAwareFunctions.Refres();
            return;
        }

        CreateFormDetails(formId, searchResults);
    }

    private bool DeleteOldSearchResults(int formId)
    {
        var rsFormDetail = GetRecordset("C_FORMSEARCHRELATIONONCREDITSAFEDETAILS", "",
            $"FK_FORMSEARCHRELATIONONCREDITSAFE = {formId}", "");
        rsFormDetail.UpdateWhenMoveRecord = false;
        rsFormDetail.UseDataChanges = false;

        if (rsFormDetail.RecordCount == 0)
        {
            return true;
        }

        rsFormDetail.MoveFirst();
        while (!rsFormDetail.EOF)
        {
            rsFormDetail.Delete();
            rsFormDetail.MoveNext();
        }

        rsFormDetail.MoveFirst();
        
        var updateResult = rsFormDetail.Update2();

        if (updateResult != null && updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Het verwijderen van oude zoekresultaten is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return false;
        }

        return true;
    }

    private void CreateFormDetails(int formId, RootObjectSearchInfo searchResults)
    {
        var rsFormDetail = GetRecordset("C_FORMSEARCHRELATIONONCREDITSAFEDETAILS", "",
            $"PK_C_FORMSEARCHRELATIONONCREDITSAFEDETAILS = -1", "");
        rsFormDetail.UpdateWhenMoveRecord = false;
        rsFormDetail.UseDataChanges = false;

        foreach (var company in searchResults.Companies)
        {
            rsFormDetail.AddNew();

            rsFormDetail.SetFieldValue("FK_FORMSEARCHRELATIONONCREDITSAFE", formId);
            rsFormDetail.SetFieldValue("CREDITSAFEID", company.Id);
            rsFormDetail.SetFieldValue("SELECT", false);
            rsFormDetail.SetFieldValue("NAME", company.Name);

            var tradingNames = string.Join(", ", company.TradingNames).Length > 2000
                ?  string.Join(", ", company.TradingNames).Substring(0, 2000)
                :  string.Join(", ", company.TradingNames);

            rsFormDetail.Fields["TRADINGNAMES"].Value = company.TradingNames == null
                ? ""
                : tradingNames;
            rsFormDetail.Fields["ADDRESS"].Value = company.Address == null
                ? ""
                : $"{company.Address.Street} {company.Address.HouseNo} {company.Address.City}";

            rsFormDetail.Fields["COCNUMBER"].Value = company.RegNo.Length > 8
                ? company.RegNo.Substring(0, 8)
                : company.RegNo;

            rsFormDetail.SetFieldValue("STATE", company.Status);
            rsFormDetail.SetFieldValue("OFFICETYPE", company.OfficeType);
        }

        var updateResult = rsFormDetail.Update2();

        if (updateResult != null && updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Het aanmaken van nieuwe zoekresultaten is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        FormDataAwareFunctions.Refres();
    }

    private RootObjectSearchInfo SearchOnCreditSafe(SearchInput searchInput)
    {
        var token = new Token(this).GetOrCreateNewToken();

        if(token == null)
        {
            return null;
        }

        var allCompanies = new List<Company>(); 
        
        //We kunnen per 50 relaties ophalen bij een zoekstatement
        var totalCompaniesPerPage = 50;
        var pageNumber = 0;
        var totalCompaniesFound = int.MaxValue;
        var maxSearchResults = 50;

        while (totalCompaniesFound > (pageNumber * totalCompaniesPerPage))
        {
            pageNumber++;

            var searchFilterNameAndCocNumber = DetermineSearchFilterNameAndCocNumber(searchInput);
            
            //Vraag relatie info op mbv naam of KVK
            var urlPath = new Uri(
                $"https://connect.creditsafe.com/v1/companies?countries=NL{searchFilterNameAndCocNumber}&page={pageNumber}&pageSize={totalCompaniesPerPage}");

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token.Token);

            var response = client.GetAsync(urlPath).GetAwaiter().GetResult();

            if(!response.IsSuccessStatusCode)
            {
                MessageBox.Show(
                    $"Opvragen relatie info bij CreditSafe mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase}.",
                    "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return null;
            }

            response.EnsureSuccessStatusCode();
            var rootObjectCompanyInfo = response.Content.ReadAsAsync<RootObjectSearchInfo>().Result;

            if (pageNumber == 1)
            {
                if (rootObjectCompanyInfo.TotalSize > maxSearchResults)
                {
                    /*MessageBox.Show(
                        $"Er zijn {rootObjectCompanyInfo.TotalSize:n0} resultaten gevonden. De eerste {maxSearchResults} worden opgehaald.",
                        "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);*/

                    totalCompaniesFound = maxSearchResults;
                }
                else
                {
                    totalCompaniesFound = rootObjectCompanyInfo.TotalSize;
                }
            }

            allCompanies.AddRange(rootObjectCompanyInfo.Companies);
        }

        return new RootObjectSearchInfo()
        {
            TotalSize = totalCompaniesFound,
            Companies = allCompanies,
        };
    }

    private string DetermineSearchFilterNameAndCocNumber(SearchInput searchInput)
    {
        if (!string.IsNullOrEmpty(searchInput.Name) && !string.IsNullOrEmpty(searchInput.CocNumber))
        {
            return $"&name={searchInput.Name}&regNo={searchInput.CocNumber}";
        }
        else if (string.IsNullOrEmpty(searchInput.Name) && !string.IsNullOrEmpty(searchInput.CocNumber))
        {
            return $"&regNo={searchInput.CocNumber}";
        }
        else if (!string.IsNullOrEmpty(searchInput.Name) && string.IsNullOrEmpty(searchInput.CocNumber))
        {
            return $"&name={searchInput.Name}";
        }
        else
        {
            return "";
        }
    }

    private SearchInput GetSearchInput(int formId)
    {
        return GetRecordset("C_FORMSEARCHRELATIONONCREDITSAFE", "NAME, COCNUMBER",
                $"PK_C_FORMSEARCHRELATIONONCREDITSAFE = {formId}", "")
            .DataTable.AsEnumerable().Select(x => new SearchInput()
            {
                Name = x.Field<string>("NAME"), CocNumber = x.Field<string>("COCNUMBER"),
            }).First();
    }
}
