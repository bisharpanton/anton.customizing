﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditSafe_SearchRelations.TokenModels
{
    class CreditSafeTokenInfo
    {
        public CreditSafeCredentials CreditSafeCredentials { get; set; }
        public string Token { get; set; }
        public DateTime TokenGenerated { get; set; }
    }
}
