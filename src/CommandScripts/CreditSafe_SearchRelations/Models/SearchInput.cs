﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditSafe_SearchRelations.Models
{
    class SearchInput
    {
        public string Name { get; set; }
        public string CocNumber { get; set; }
    }
}
