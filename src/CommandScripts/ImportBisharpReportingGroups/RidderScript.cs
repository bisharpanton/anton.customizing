﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class ImportBisharpReportingGroups_RidderScript : CommandScript
{
    public void Execute()
    {
        //SP
        //2-11-2022
        //Importeer bisharp reporting groepen

        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("U_BISHARPPORTALUSER", StringComparison.OrdinalIgnoreCase))
        {
            MessageBox.Show("Dit script mag alleen van de Bisharp portal user tabel aangeroepen worden.");
            return;
        }

        var userIds = FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue()).ToList();

        object[] selectie = null;
        selectie = OpenMultiSelectForm("U_BISHARPREPORTINGGROUP", "PK_U_BISHARPREPORTINGGROUP",
            "", "");

        if (selectie == null)
        {
            return;
        }

        ImportDashboardGroups(selectie, userIds);

    }

    private void ImportDashboardGroups(object[] selectie, List<int> userIds)
    {
        var rsReportingGroupPerPortalUser = GetRecordset("U_BISHARPAVAILABLEREPORTGROUPSPERPORTALUSER", "",
            "PK_U_BISHARPAVAILABLEREPORTGROUPSPERPORTALUSER = -1", "");
        rsReportingGroupPerPortalUser.UseDataChanges = true;
        rsReportingGroupPerPortalUser.UpdateWhenMoveRecord = false;

        foreach (var userId in userIds)
        {
            foreach (var dashBoardGroupId in selectie)
            {
                rsReportingGroupPerPortalUser.AddNew();
                rsReportingGroupPerPortalUser.Fields["FK_BISHARPREPORTINGGROUP"].Value = dashBoardGroupId;
                rsReportingGroupPerPortalUser.Fields["FK_BISHARPPORTALUSER"].Value = userId;
            }
        }

        var updateResult = rsReportingGroupPerPortalUser.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Het importeren van Bisharp reporting groepen is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

    }
}
