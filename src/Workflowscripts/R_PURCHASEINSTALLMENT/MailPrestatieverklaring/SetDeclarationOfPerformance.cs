﻿namespace PurchaseInstallment
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Drawing;
    using System.Drawing.Printing;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Xml;
    using ADODB;
    using Ridder.Common.ADO;
    using Ridder.Common.Choices;
    using Ridder.Common.Search;
    using Ridder.Common.WorkflowModel.Activities;

    public class SetDeclarationOfPerformance : WorkflowScriptInfo
    {
        public void Execute()
        {
            // HVE
            // 6-4-2021
            // Set Declaration of performance true
            var purchaseInstallmentId = (int)RecordId;

            var purchaseInstallment = GetRecordset("R_PURCHASEINSTALLMENT", "DECLARATIONOFPERFORMANCE",
                $"PK_R_PURCHASEINSTALLMENT = {purchaseInstallmentId}", "");
            purchaseInstallment.UseDataChanges = true;

            purchaseInstallment.MoveFirst();
            purchaseInstallment.SetFieldValue("DECLARATIONOFPERFORMANCE", true);
            purchaseInstallment.Update();

        }
    }

}
