﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.ItemManagement;
using Ridder.Common.ProjectAdministration;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class GetOrCreateWorkdayForSpecificHours : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //14-3-2024
        //Op twee plaatsen is deze functie van toepassing, bij het importeren van de roosters vanuit AFAS uit de scheduler
        //en bij het switchen van werkdagen voor parttimers waarbij we dan een werkdag uitzondering aanmaken. 
        //Omdat het op twee plaatsen van toepassing is hebben we dit gecentraliseerd naar 1 workflow

        var hours = Convert.ToDouble(Parameters["Hours"].Value);

        if (!CheckForWholeOrHalveHours(hours))
        {
            Log.Error("Error", $"Alleen hele of halve uren toegestaan.", "");
            return;
        }

        var employee = GetRecordset("R_EMPLOYEE", "FK_CAO, DEFAULTTOIL",
            $"PK_R_EMPLOYEE = {id}", "").DataTable.AsEnumerable().First();

        var workdays = GetAllWorkdays();

        var caoDescription = GetRecordset("C_CAO", "DESCRIPTION", $"PK_C_CAO = {employee.Field<int>("FK_CAO")}", "")
            .DataTable.AsEnumerable().First().Field<string>("DESCRIPTION");

        if (hours == 0.0)
        {
            var noWorkday = GetNoWorkdayPerCao(employee, caoDescription);

            if(noWorkday == null)
            {
                return;
            }

            Log.Info("Info", $"{noWorkday.Id}", "");
            return;
        }


        var workdaysThisHours = workdays.Where(x => Math.Abs(x.DefaultHoursInHours - hours) < 0.01).ToList();

        if (!workdaysThisHours.Any())
        {
            var createdWorkdayId = CreateNewWorkday(employee, hours, caoDescription);
            Log.Info("Info", $"{createdWorkdayId}", "");
            return;
        }

        var workdayThisHoursWithCorrectOvertime = employee.Field<DefaultTOIL>("DEFAULTTOIL") != DefaultTOIL.Never
            ? workdaysThisHours.FirstOrDefault(x => x.WorkdayOverTimes.First().OverTime.Code.Contains("TVT") && x.WorkdayOverTimes.First().OverTime.CaoId == employee.Field<int>("FK_CAO"))
            : (caoDescription.Equals("Basis CAO") 
                ? workdaysThisHours.FirstOrDefault(x => x.WorkdayOverTimes.First().OverTime.Code.Equals("NORM"))
                : workdaysThisHours.FirstOrDefault(x => !x.WorkdayOverTimes.First().OverTime.Code.Equals("NORM") && !x.WorkdayOverTimes.First().OverTime.Code.Contains("TVT") && x.WorkdayOverTimes.First().OverTime.CaoId == employee.Field<int>("FK_CAO")));

        if(workdayThisHoursWithCorrectOvertime == null)
        {
            var createdWorkdayId = CreateNewWorkday(employee, hours, caoDescription);
            Log.Info("Info", $"{createdWorkdayId}", "");
            return;
        }

        Log.Info("Info", $"{workdayThisHoursWithCorrectOvertime.Id}", "");
    }

    private Workday GetNoWorkdayPerCao(DataRow employee, string caoDescription)
    {
        var workdays = GetRecordset("R_WORKDAY", "CODE, DEFAULTHOURS", "", "")
            .DataTable.AsEnumerable().Select(x => new Workday()
            {
                Id = x.Field<int>("PK_R_WORKDAY"),
                Code = x.Field<string>("CODE"),
                DefaultHours = x.Field<long>("DEFAULTHOURS"),
            }).ToList();

        var codeNoWorkday = DetermineCodeWorkday(employee, caoDescription);

        var workdayNoWorkday = workdays.FirstOrDefault(x => x.Code.Equals(codeNoWorkday));

        if (workdayNoWorkday == null)
        {
            Log.Error("Error", $"Geen werkdag '{codeNoWorkday}' gevonden.", "");
            return null;
        }

        return workdayNoWorkday;
    }

    private string DetermineCodeWorkday(DataRow employee, string caoDescription)
    {
        if (caoDescription.Equals("Metaal") && employee.Field<DefaultTOIL>("DEFAULTTOIL") == DefaultTOIL.Never)
        {
            return "GW";
        }
        else if (caoDescription.Equals("Metaal") && employee.Field<DefaultTOIL>("DEFAULTTOIL") != DefaultTOIL.Never)
        {
            return "GW TVT";
        }
        else if (caoDescription.Equals("Bouw") && employee.Field<DefaultTOIL>("DEFAULTTOIL") == DefaultTOIL.Never)
        {
            return "GW B";
        }
        else if (caoDescription.Equals("Bouw") && employee.Field<DefaultTOIL>("DEFAULTTOIL") != DefaultTOIL.Never)
        {
            return "GW B TVT";
        }
        else
        {
            //Bij Basis CAO komt dit eigenlijk niet voor. Pak daarom maar gewoon een weekend dag
            return "WKD NORM";
        }
    }

    private bool CheckForWholeOrHalveHours(double hours)
    {
        var deviation = Math.Abs(Math.Round(hours) - hours);

        if(deviation < 0.001)
        {
            return true; //Whole number
        }

        if(deviation > 0.499 && deviation < 0.501)
        {
            return true; //Half number
        }

        return false;
    }

    private int CreateNewWorkday(DataRow employee, double hours, string caoDescription)
    {
        var sHours = Math.Abs(Math.Round(hours) - hours) < 0.001 ? $"{hours:n0}" : $"{hours:n1}";
        var sTvtAdditionShort = employee.Field<DefaultTOIL>("DEFAULTTOIL") != DefaultTOIL.Never ? " T" : "";
        var sTvtAddition = employee.Field<DefaultTOIL>("DEFAULTTOIL") != DefaultTOIL.Never ? " TvT" : "";

        var rsWorkday = GetRecordset("R_WORKDAY", "", "PK_R_WORKDAY = -1", "");
        rsWorkday.UseDataChanges = true;
        rsWorkday.AddNew();
        rsWorkday.SetFieldValue("CODE", $"{caoDescription.Substring(0,3)} {sHours}{sTvtAdditionShort}");
        rsWorkday.SetFieldValue("DESCRIPTION", $"Werkdag {caoDescription} {sHours}{sTvtAddition}");
        rsWorkday.SetFieldValue("DEFAULTHOURS", TimeSpan.FromHours(hours).Ticks);

        var updateResult = rsWorkday.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            Log.Error("Error", $"Aanmaken nieuwe werkdag is mislukt, oorzaak {updateResult.First(x => x.HasError).GetResult()}", "");
            return 0;
        }

        var createdWorkday = (int)updateResult.First().PrimaryKey;

        var overTimesToCreate = DetermineOvertimesToCreate(employee, hours, caoDescription);

        if (overTimesToCreate == null)
        {
            return 0;
        }

        var rsWorkdayOvertimes = GetRecordset("R_WORKDAYOVERTIME", "", "PK_R_WORKDAYOVERTIME = -1", "");

        foreach (var overtimeToCreate in overTimesToCreate)
        {
            rsWorkdayOvertimes.AddNew();
            rsWorkdayOvertimes.SetFieldValue("FK_WORKDAY", createdWorkday);
            rsWorkdayOvertimes.SetFieldValue("FK_TIMECODE", overtimeToCreate.Item1);
            rsWorkdayOvertimes.SetFieldValue("TIMESPAN", overtimeToCreate.Item2);
        }

        var updateResult2 = rsWorkdayOvertimes.Update2();

        if (updateResult2.Any(x => x.HasError))
        {
            Log.Error("Error", $"Aanmaken nieuwe werkdag is mislukt, oorzaak {updateResult2.First(x => x.HasError).GetResult()}", "");
            return 0;
        }

        return createdWorkday;
    }

    private List<Tuple<int, long>> DetermineOvertimesToCreate(DataRow employee, double hours, string caoDescription)
    {
        var overtimes = GetRecordset("R_TIMECODE", "CODE, FK_CAO", "", "")
            .DataTable.AsEnumerable().Select(x => new OverTime()
            {
                Id = x.Field<int>("PK_R_TIMECODE"),
                Code = x.Field<string>("CODE"),
                CaoId = x.Field<int>("FK_CAO")
            }).ToList();

        var overTimeThisDay = TimeSpan.FromHours(24.0 - hours);

        if (employee.Field<DefaultTOIL>("DEFAULTTOIL") != DefaultTOIL.Never)
        {
            //Tvt opbouwe
            var overTimeTvt = overtimes.FirstOrDefault(x => x.Code.Contains("TVT") && x.CaoId == employee.Field<int>("FK_CAO"));

            if(overTimeTvt != null)
            {
                return new List<Tuple<int, long>>() { Tuple.Create(overTimeTvt.Id, overTimeThisDay.Ticks) };
            }

            var createdOverTimeTvt = CreatedOverTimeTvt(employee, caoDescription);

            if(createdOverTimeTvt == 0)
            {
                return null;
            }

            return new List<Tuple<int, long>>() { Tuple.Create(createdOverTimeTvt, overTimeThisDay.Ticks) };
        }
        else
        {
            if(caoDescription.Equals("Basis CAO"))
            {
                var overTimeNorm = overtimes.FirstOrDefault(x => x.Code.Equals("NORM"));

                if(overTimeNorm == null)
                {
                    Log.Error("Error", $"Overuursoort 'NORM' niet gevonden. Zoek contact met HR.", "");
                    return null;
                }

                return new List<Tuple<int, long>>() { Tuple.Create(overTimeNorm.Id, overTimeThisDay.Ticks) };
            }
            else if(caoDescription.Equals("Bouw"))
            {
                var overTimeOvwWeek = overtimes.FirstOrDefault(x => x.Code.Equals("OVW WEEK"));

                if (overTimeOvwWeek == null)
                {
                    Log.Error("Error", $"Overuursoort 'OVW WEEK' niet gevonden. Zoek contact met HR.", "");
                    return null;
                }

                return new List<Tuple<int, long>>() { Tuple.Create(overTimeOvwWeek.Id, overTimeThisDay.Ticks) };
            }
            else if (caoDescription.Equals("Metaal"))
            {
                var overTimeOvw78 = overtimes.FirstOrDefault(x => x.Code.Equals("OVW 0.78"));
                var overTimeOvw89 = overtimes.FirstOrDefault(x => x.Code.Equals("OVW 0.89"));

                if (overTimeOvw78 == null || overTimeOvw89 == null)
                {
                    Log.Error("Error", $"Overuursoort 'OVW 0.78' en/of overuursoort 'OVW 0.89' niet gevonden. Zoek contact met HR.", "");
                    return null;
                }

                return new List<Tuple<int, long>>() 
                { 
                    Tuple.Create(overTimeOvw78.Id, TimeSpan.FromHours(2).Ticks ),
                    Tuple.Create(overTimeOvw89.Id, overTimeThisDay.Ticks - TimeSpan.FromHours(2).Ticks),
                };
            }
            else
            {
                Log.Error("Error", $"CAO '{caoDescription}' geen bekende CAO binnen de Anton Groep. Zoek contact met HR.", "");
                return null;
            }
        }
    }

    private int CreatedOverTimeTvt(DataRow employee, string caoDescription)
    {
        var code = $"TVT {caoDescription}".Length > 10 ? $"TVT {caoDescription}".Substring(0, 10) : $"TVT {caoDescription}";

        var rsTimeCode = GetRecordset("R_TIMECODE", "", "PK_R_TIMECODE = -1", "");
        rsTimeCode.AddNew();
        rsTimeCode.SetFieldValue("CODE", code);
        rsTimeCode.SetFieldValue("DESCRIPTION", $"TVT {caoDescription}");
        rsTimeCode.SetFieldValue("FK_CAO", employee.Field<int>("FK_CAO"));

        var updateResult = rsTimeCode.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            Log.Error("Error", $"Aanmaken nieuwe TVT overuursoort is mislukt, oorzaak {updateResult.First(x => x.HasError).GetResult()}", "");
            return 0;
        }

        return (int)updateResult.First().PrimaryKey;
    }

    private List<Workday> GetAllWorkdays()
    {
        var workdays = GetRecordset("R_WORKDAY", "CODE, DEFAULTHOURS", "EXCLUDEFORDETERMININGWORKDAY = 0", "")
            .DataTable.AsEnumerable().Select(x => new Workday()
            {
                Id = x.Field<int>("PK_R_WORKDAY"),
                Code = x.Field<string>("CODE"),
                DefaultHours = x.Field<long>("DEFAULTHOURS"),
            }).ToList();

        var workdayOverTimes = GetRecordset("R_WORKDAYOVERTIME", "FK_TIMECODE, FK_WORKDAY", "", "")
            .DataTable.AsEnumerable().Select(x => new WorkdayOverTime()
            {
                Id = x.Field<int>("PK_R_WORKDAYOVERTIME"),
                TimeCodeId = x.Field<int>("FK_TIMECODE"),
                WorkdayId = x.Field<int>("FK_WORKDAY"),
            }).ToList();

        var overtimes = GetRecordset("R_TIMECODE", "CODE, FK_CAO", "", "")
            .DataTable.AsEnumerable().Select(x => new OverTime()
            {
                Id = x.Field<int>("PK_R_TIMECODE"),
                Code = x.Field<string>("CODE"),
                CaoId = x.Field<int>("FK_CAO")
            }).ToList();

        workdayOverTimes.ForEach(x => x.OverTime = overtimes.First(y => y.Id == x.TimeCodeId));
        workdays.ForEach(x => x.WorkdayOverTimes = workdayOverTimes.Where(y => y.WorkdayId == x.Id).ToList());

        return workdays;
    }

    private class Workday
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public long DefaultHours { get; set; }
        public double DefaultHoursInHours => TimeSpan.FromTicks(DefaultHours).TotalHours;
        public List<WorkdayOverTime> WorkdayOverTimes { get; set; }
    }

    private class WorkdayOverTime
    {
        public int Id { get; set; }
        public int TimeCodeId { get; set; }
        public int WorkdayId { get; set; }
        public OverTime OverTime { get; set; }
    }

    private class OverTime
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int CaoId { get; set; }
    }
}
