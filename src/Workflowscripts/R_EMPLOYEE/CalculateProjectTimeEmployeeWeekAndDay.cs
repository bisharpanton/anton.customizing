﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;
using System.Globalization;

public class CalculateProjectTimeEmployeeWeekAndDay : WorkflowScriptInfo
{
    public void Execute()
    {
        var employeeId = (int)RecordId;

        //SP
        //31-8-2023
        //Calculeer de roosteruren en gewerkte uren per week

        var employeeStartDate = validateInLabour(employeeId);

        if(!employeeStartDate.HasValue)
        {
            return;//Werknemer uit dienst of datum uit dienst later dan vandaag
        }

        var rsTimeTableOffset = GetRecordset("R_TIMETABLEOFFSET", "PK_R_TIMETABLEOFFSET, FK_TIMETABLE, FK_TIMETABLEWORKDAY, STARTDATE",
                    $"FK_EMPLOYEE = {employeeId} AND STARTDATE <= '{DateTime.Now:yyyyMMdd}'", "STARTDATE DESC");

        if (rsTimeTableOffset.RecordCount == 0)
        {
            return;//Geen rooster startinstellingen gevonden bij werknemer
        }

        var startDate = rsTimeTableOffset.DataTable.AsEnumerable().First().Field<DateTime>("STARTDATE");
        var startDayId = rsTimeTableOffset.DataTable.AsEnumerable().First().Field<int>("FK_TIMETABLEWORKDAY");
        var startDay = GetRecordset("R_TIMETABLEWORKDAY", "SEQUENCENR",
            $"PK_R_TIMETABLEWORKDAY = {startDayId}", "").DataTable.AsEnumerable().First().Field<int>("SEQUENCENR");

        var timeTableId = rsTimeTableOffset.DataTable.AsEnumerable().First().Field<int>("FK_TIMETABLE");

        var timeTableWorkdayIds = GetRecordset("R_TIMETABLEWORKDAY", "PK_R_TIMETABLEWORKDAY",
            $"FK_TIMETABLE = {timeTableId}", "SEQUENCENR ASC").DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_TIMETABLEWORKDAY")).ToList();

        List<NeededHoursPerEmployeePerTimeTableWorkDay> neededHoursPerEmployeePerTimeTableWorkDay = new List<NeededHoursPerEmployeePerTimeTableWorkDay>();

        if (timeTableWorkdayIds.Any())
        {
            var timeTableWorkdays = GetRecordset("R_TIMETABLEWORKDAY", "SEQUENCENR, FK_WORKDAY",
                 $"PK_R_TIMETABLEWORKDAY IN ({string.Join(",", timeTableWorkdayIds)})", "SEQUENCENR ASC").DataTable.AsEnumerable().ToList();

            var workdayIds = timeTableWorkdays.Select(x => x.Field<int>("FK_WORKDAY")).ToList();
            var workdays = GetRecordset("R_WORKDAY", "DEFAULTHOURS",
                $"PK_R_WORKDAY IN ({string.Join(",", workdayIds)})", "").DataTable.AsEnumerable().ToList();

            foreach (var timeTableWorkdayId in timeTableWorkdayIds)
            {
                neededHoursPerEmployeePerTimeTableWorkDay.Add(GetTimeTableHoursPerDay(employeeId, timeTableWorkdayId, timeTableWorkdays, workdays));
            }

            List<NeededHoursPerEmployeePerCalendarDay> neededHoursPerEmployeePerDay = GenerateNeededHoursPerDay(employeeId, neededHoursPerEmployeePerTimeTableWorkDay, startDate, startDay);

            List<NeededHoursPerEmployeePerCalendarDay> currentNeededHoursPerEmployeePerDay = GetCurrentNeededHoursPerEmployeePerDay(employeeId);

            List<NeededHoursPerEmployeePerCalendarDay> findNeededHoursDiscrepancies = FindNeededHoursDiscrepancies(neededHoursPerEmployeePerDay, currentNeededHoursPerEmployeePerDay);

            if (findNeededHoursDiscrepancies.Any())
            {
                CreateOrUpdateProjectTimeWeeksAndDays(findNeededHoursDiscrepancies);
            }

            CheckOpenHours(employeeId);
        }
    }

    private DateTime? validateInLabour(int employeeId)
    {
        var today = DateTime.Now;
        return GetRecordset("R_EMPLOYEE", "EMPLOYMENTSTARTDATE",
             $"PK_R_EMPLOYEE = {employeeId} AND EMPLOYMENTSTARTDATE <= '{today:yyyyMMdd}' AND" +
             $"(EMPLOYMENTTERMINATIONDATE IS NULL OR EMPLOYMENTTERMINATIONDATE > '{today:yyyyMMdd}')", "").DataTable.AsEnumerable().FirstOrDefault()?.Field<DateTime?>("EMPLOYMENTSTARTDATE");
    }

    private void CheckOpenHours(int employeeId)
    {
        var openEmployeeHours = GetRecordset("R_PROJECTTIME", "PK_R_PROJECTTIME, YEARWEEKNR, DATE, FK_PROJECTTIMEEMPLOYEEWEEK, FK_PROJECTTIMEEMPLOYEEDAY",
            $"FK_EMPLOYEE = {employeeId} AND YEARWEEKNR >= 202401", "").DataTable.AsEnumerable();

        /*var openEmployeeHours = GetRecordset("R_PROJECTTIME", "PK_R_PROJECTTIME, YEARWEEKNR, DATE, FK_PROJECTTIMEEMPLOYEEWEEK, FK_PROJECTTIMEEMPLOYEEDAY",
            $"FK_EMPLOYEE = {employeeId} AND CLOSED = 0", "").DataTable.AsEnumerable();*/ //Deze gebruiken na eerste keer aanmaken

        var hoursWithoutProjecttimeWeekOrDay = openEmployeeHours.Where(x => !x.Field<int?>("FK_PROJECTTIMEEMPLOYEEWEEK").HasValue ||
            !x.Field<int?>("FK_PROJECTTIMEEMPLOYEEDAY").HasValue).ToList();

        if(!hoursWithoutProjecttimeWeekOrDay.Any())
        {
            return;
        }

        foreach (var hourWithoutProjecttimeWeekOrDay in hoursWithoutProjecttimeWeekOrDay)
        {
            var projectTimeEmployeeWeekId = GetRecordset("U_PROJECTTIMEEMPLOYEEWEEK", "PK_U_PROJECTTIMEEMPLOYEEWEEK",
                $"FK_EMPLOYEE = {employeeId} AND YEARWEEKNR = {hourWithoutProjecttimeWeekOrDay.Field<int>("YEARWEEKNR")}", "").DataTable.AsEnumerable().FirstOrDefault()?.Field<int>("PK_U_PROJECTTIMEEMPLOYEEWEEK") ?? 0;

            if (projectTimeEmployeeWeekId == 0)
            {
                return;
            }

            var date = hourWithoutProjecttimeWeekOrDay.Field<DateTime>("DATE");

            var projectTimeEmployeeDayId = GetRecordset("U_PROJECTTIMEEMPLOYEEDAY", "PK_U_PROJECTTIMEEMPLOYEEDAY",
                $"FK_PROJECTTIMEEMPLOYEEWEEK = {projectTimeEmployeeWeekId} AND DATE = '{date:yyyyMMdd}'", "").DataTable.AsEnumerable().FirstOrDefault()?.Field<int>("PK_U_PROJECTTIMEEMPLOYEEDAY") ?? 0;        

            UpdateProjectTime(hourWithoutProjecttimeWeekOrDay.Field<int>("PK_R_PROJECTTIME"), projectTimeEmployeeWeekId, projectTimeEmployeeDayId);

        }
    }

    private void UpdateProjectTime(int projectTimeId, int projectTimeEmployeeWeekId, int projectTimeEmployeeDayId)
    {
        var rsProjectTime = GetRecordset("R_PROJECTTIME", "PK_R_PROJECTTIME, FK_PROJECTTIMEEMPLOYEEWEEK, FK_PROJECTTIMEEMPLOYEEDAY",
            $"PK_R_PROJECTTIME = {projectTimeId}", "");
        rsProjectTime.UseDataChanges = true;
        rsProjectTime.UpdateWhenMoveRecord = false;

        rsProjectTime.MoveFirst();
        rsProjectTime.SetFieldValue("FK_PROJECTTIMEEMPLOYEEWEEK", projectTimeEmployeeWeekId);
        rsProjectTime.SetFieldValue("FK_PROJECTTIMEEMPLOYEEDAY", projectTimeEmployeeDayId);

        var updateResult = rsProjectTime.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            throw new Exception($"Het bijwerken van urenboekingen zonder jaarweek is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");
        }
    }

    private void CreateOrUpdateProjectTimeWeeksAndDays(List<NeededHoursPerEmployeePerCalendarDay> findNeededHoursDiscrepancies)
    {
        var rsNewProjecttimeEmployeeDay = GetRecordset("U_PROJECTTIMEEMPLOYEEDAY", "", "PK_U_PROJECTTIMEEMPLOYEEDAY = -1", "");
        rsNewProjecttimeEmployeeDay.UseDataChanges = false;
        rsNewProjecttimeEmployeeDay.UpdateWhenMoveRecord = false;

        foreach (var findNeededHoursDiscrepancy in findNeededHoursDiscrepancies)
        {
            var date = findNeededHoursDiscrepancy.Date;

            if(date.Year >= 2024)
            {
                var day = (int)CultureInfo.CurrentCulture.DateTimeFormat.Calendar.GetDayOfWeek(date);
                day = day == 0 ? 7 : day;

                var weekStart = date.AddDays(-((day - 1) % 7));

                var jan4 = new DateTime(date.Year, 1, 4);
                var day4Jan = (int)CultureInfo.CurrentCulture.DateTimeFormat.Calendar.GetDayOfWeek(jan4);
                day4Jan = day4Jan == 0 ? 7 : day4Jan;

                var isoYearWeekStart = jan4.AddDays(1 - day4Jan);

                string yearWeek;
                if (weekStart < isoYearWeekStart)
                {
                    yearWeek = (date.Year - 1).ToString() + "52";
                    if (day4Jan == 4)
                    {
                        yearWeek = (date.Year - 1).ToString() + "53";
                    }
                }
                else
                {
                    var weekNum = (weekStart - isoYearWeekStart).Days / 7 + 1;

                    if (weekNum >= 52 && date.Month == 12 && date.Day - day > 28)
                    {
                        yearWeek = (date.Year + 1).ToString() + "01";
                    }
                    else
                    {
                        yearWeek = String.Format("{0:0000}{1:00}", date.Year, weekNum);
                    }
                }

                var rsProjecttimeEmployeeWeek = GetRecordset("U_PROJECTTIMEEMPLOYEEWEEK", "PK_U_PROJECTTIMEEMPLOYEEWEEK",
                    $"FK_EMPLOYEE = {findNeededHoursDiscrepancy.EmployeeId} AND YEARWEEKNR = '{yearWeek}'", "");

                int projectTimeWeekId = 0;
                if (rsProjecttimeEmployeeWeek.RecordCount == 0)
                {
                    projectTimeWeekId = CreateNewProjecttimeEmployeeWeek((int)findNeededHoursDiscrepancy.EmployeeId, yearWeek);
                }
                else
                {
                    projectTimeWeekId = rsProjecttimeEmployeeWeek.DataTable.AsEnumerable().First().Field<int>("PK_U_PROJECTTIMEEMPLOYEEWEEK");
                }

                var rsProjecttimeEmployeeDay = GetRecordset("U_PROJECTTIMEEMPLOYEEDAY", "",
                    $"FK_PROJECTTIMEEMPLOYEEWEEK = {projectTimeWeekId} AND DATE = '{findNeededHoursDiscrepancy.Date:yyyyMMdd}'", "");

                if (rsProjecttimeEmployeeDay.RecordCount == 0)
                {
                    rsNewProjecttimeEmployeeDay.AddNew();
                    rsNewProjecttimeEmployeeDay.SetFieldValue("FK_PROJECTTIMEEMPLOYEEWEEK", projectTimeWeekId);
                    rsNewProjecttimeEmployeeDay.SetFieldValue("DATE", findNeededHoursDiscrepancy.Date);
                    rsNewProjecttimeEmployeeDay.SetFieldValue("TOTALHOURSNEEDED", findNeededHoursDiscrepancy.NeededHours);
                }
                else
                {
                    rsProjecttimeEmployeeDay.MoveFirst();
                    rsProjecttimeEmployeeDay.SetFieldValue("TOTALHOURSNEEDED", findNeededHoursDiscrepancy.NeededHours);
                    rsProjecttimeEmployeeDay.Update();
                }
            }

            
        }

        if (rsNewProjecttimeEmployeeDay.RecordCount > 0)
        {
            var updateResult2 = rsNewProjecttimeEmployeeDay.Update2();
        }

    }

    private List<NeededHoursPerEmployeePerCalendarDay> FindNeededHoursDiscrepancies(
    List<NeededHoursPerEmployeePerCalendarDay> NeededHoursPerEmployeePerDay,
    List<NeededHoursPerEmployeePerCalendarDay> CurrentNeededHoursPerEmployeePerDay)
    {
        var currentHoursDict = CurrentNeededHoursPerEmployeePerDay.ToDictionary(
            k => (k.EmployeeId, k.Date),
            v => v.NeededHours);

        var discrepancies = new List<NeededHoursPerEmployeePerCalendarDay>();

        foreach (var entry in NeededHoursPerEmployeePerDay)
        {
            if (!currentHoursDict.TryGetValue((entry.EmployeeId, entry.Date), out long currentHours) || entry.NeededHours != currentHours)
            {
                discrepancies.Add(new NeededHoursPerEmployeePerCalendarDay
                {
                    EmployeeId = entry.EmployeeId,
                    Date = entry.Date,
                    NeededHours = entry.NeededHours
                });
            }
        }

        return discrepancies;
    }

    private List<NeededHoursPerEmployeePerCalendarDay> GetCurrentNeededHoursPerEmployeePerDay(int employeeId)
    {
        var projecttimeEmployeeWeekIds = GetRecordset("U_PROJECTTIMEEMPLOYEEWEEK", "PK_U_PROJECTTIMEEMPLOYEEWEEK",
            $"FK_EMPLOYEE = {employeeId}", "").DataTable.AsEnumerable().Select(x => x.Field<int>("PK_U_PROJECTTIMEEMPLOYEEWEEK")).ToList();

        if (projecttimeEmployeeWeekIds.Any())
        {
            return GetRecordset("U_PROJECTTIMEEMPLOYEEDAY", "DATE, TOTALHOURSNEEDED",
            $"FK_PROJECTTIMEEMPLOYEEWEEK IN ({string.Join(",", projecttimeEmployeeWeekIds)}) AND DATE >= '{DateTime.Now:yyyyMMdd}'", "").DataTable.AsEnumerable().Select(x => new NeededHoursPerEmployeePerCalendarDay()
            {
                EmployeeId = employeeId,
                Date = x.Field<DateTime>("DATE"),
                NeededHours = x.Field<long>("TOTALHOURSNEEDED"),

            }).ToList();
        }
        else
        {
            return new List<NeededHoursPerEmployeePerCalendarDay>();
        }


    }

    private List<NeededHoursPerEmployeePerCalendarDay> GenerateNeededHoursPerDay(int employeeId, List<NeededHoursPerEmployeePerTimeTableWorkDay> neededHoursPerEmployeePerTimeTableWorkDay, DateTime startDate, int startDay)
    {
        int sequenceLength = neededHoursPerEmployeePerTimeTableWorkDay.Select(n => n.SequenceNumber).Distinct().Count();
        int offset = (startDay - 1) % sequenceLength;

        var yearlySchedule = new List<NeededHoursPerEmployeePerCalendarDay>();

        DateTime endDate = GetFirstSundayAfterOneYearFromNow();
        int daysToCalculate = (endDate - startDate).Days;

        for (int day = 0; day < daysToCalculate; day++)
        {
            // Increment start date by the number of looping days
            DateTime currentDate = startDate.AddDays(day);
            int cycleDay = (day + offset) % sequenceLength;
            int sequenceNumberForDay = cycleDay + 1;

            long neededHours = neededHoursPerEmployeePerTimeTableWorkDay
                                .FirstOrDefault(x => x.SequenceNumber == sequenceNumberForDay)?.NeededHours ?? 0;

            yearlySchedule.Add(new NeededHoursPerEmployeePerCalendarDay
            {
                EmployeeId = employeeId,
                Date = currentDate,
                NeededHours = neededHours
            });
        }

        return yearlySchedule;
    }

    private NeededHoursPerEmployeePerTimeTableWorkDay GetTimeTableHoursPerDay(int employeeId, int timeTableWorkdayId, List<DataRow> timeTableWorkdays, List<DataRow> workdays)
    {
        var timeTableWorkday = timeTableWorkdays.First(x => x.Field<int>("PK_R_TIMETABLEWORKDAY") == timeTableWorkdayId);

        var workday = workdays.First(x => x.Field<int>("PK_R_WORKDAY") == timeTableWorkday.Field<int>("FK_WORKDAY"));

        return new NeededHoursPerEmployeePerTimeTableWorkDay()
        {
            EmployeeId = employeeId,
            SequenceNumber = timeTableWorkday.Field<int>("SEQUENCENR"),
            NeededHours = workday.Field<long>("DEFAULTHOURS"),
        };
    }

    private int CreateNewProjecttimeEmployeeWeek(int employeeId, string weekToCreate)
    {
        var rsNewProjecttimeEmployeeWeek = GetRecordset("U_PROJECTTIMEEMPLOYEEWEEK", "", "PK_U_PROJECTTIMEEMPLOYEEWEEK = -1", "");
        rsNewProjecttimeEmployeeWeek.UseDataChanges = false;
        rsNewProjecttimeEmployeeWeek.UpdateWhenMoveRecord = false;

        rsNewProjecttimeEmployeeWeek.AddNew();
        rsNewProjecttimeEmployeeWeek.SetFieldValue("FK_EMPLOYEE", employeeId);
        rsNewProjecttimeEmployeeWeek.SetFieldValue("YEARWEEKNR", $"{weekToCreate}");

        var updateResult = rsNewProjecttimeEmployeeWeek.Update2();

        return (int)updateResult.First().PrimaryKey;
    }

    private DateTime GetFirstSundayAfterOneYearFromNow()
    {
        // Get the date of one year from now
        DateTime oneYearFromNow = DateTime.Now.AddYears(1);

        // Find the first Sunday after one year from now
        while (oneYearFromNow.DayOfWeek != DayOfWeek.Sunday)
        {
            oneYearFromNow = oneYearFromNow.AddDays(1);
        }

        return oneYearFromNow;
    }


    class NeededHoursPerEmployeePerTimeTableWorkDay
    {
        public int EmployeeId { get; set; }
        public int SequenceNumber { get; set; }
        public long NeededHours { get; set; }
    }

    class NeededHoursPerEmployeePerCalendarDay
    {
        public int EmployeeId { get; set; }
        public DateTime Date { get; set; }
        public long NeededHours { get; set; }
    }
}
