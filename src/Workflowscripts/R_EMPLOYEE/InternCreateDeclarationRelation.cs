﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.ItemManagement;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class InternCreateDeclarationRelation : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //24-8-2023
        //We willen het maken van een declaratie relatie vanuit de Appreo API uitvoeren. Daarom maak een workflow voor de intelligentie
        //De daadwerkelijke workflow is een client script die deze workflow aanroept en daarna de relatie opent.

        var rsEmployee = GetRecordset("R_EMPLOYEE", "FK_PERSON, FK_RELATION",
            $"PK_R_EMPLOYEE = {id}", "");
        var employee = rsEmployee.DataTable.AsEnumerable().First();

        if (employee.Field<int?>("FK_RELATION").HasValue)
        {
            return;
        }

        var createdRelation = CreateRelation(employee.Field<int>("FK_PERSON"));

        if (createdRelation == 0)
        {
            return;
        }

        //Update employee
        rsEmployee.MoveFirst();
        rsEmployee.Fields["FK_RELATION"].Value = createdRelation;
        rsEmployee.Update();

        CreateCreditor(createdRelation);
    }


    private void CreateCreditor(int createdRelation)
    {
        var daybookTypePurchase = DayBookType.Purchase;

        var rsPurchaseDaybook = GetRecordset("R_DAYBOOK", "PK_R_DAYBOOK",
            $"DAYBOOKTYPE = {(int)daybookTypePurchase}", "");

        if (rsPurchaseDaybook.RecordCount == 0)
        {
            Log.Error("Error", "Crediteur maken niet mogelijk, omdat er geen inkoop dagboeken aanwezig zijn.", "");
            return;
        }

        rsPurchaseDaybook.MoveFirst();

        var result =
            EventsAndActions.CRM.Actions.CreateCreditor(createdRelation,
                (int)rsPurchaseDaybook.Fields["PK_R_DAYBOOK"].Value);

        if (result.HasError)
        {
            Log.Error("Error", $"Crediteur maken mislukt, oorzaak: {result.GetResult()}.", "");
        }
    }


    private int CreateRelation(int personId)
    {
        var rsRelation = GetRecordset("R_RELATION", "",
            $"PK_R_RELATION = -1", "");
        rsRelation.UseDataChanges = true;
        rsRelation.UpdateWhenMoveRecord = false;

        rsRelation.AddNew();
        rsRelation.Fields["NAME"].Value = GetRecordTag("R_PERSON", personId);

        rsRelation.Fields["FK_RELATIONTYPE"].Value = GetRelationType("LEV");
        rsRelation.Fields["FK_BEDRIJFSTAK1"].Value = GetOrCreateLeveranciersDiscipline("Werknemer");

        var addressIdObdam = GetOrCreateAddresObdam();
        rsRelation.Fields["FK_VISITINGADDRESS"].Value = addressIdObdam;
        rsRelation.Fields["FK_POSTALADDRESS"].Value = addressIdObdam;

        var paymentTermEightDays = GetPayMentTermIdByCode("08");
        if (paymentTermEightDays != 0)
        {
            rsRelation.SetFieldValue("FK_PURCHASEPAYMENTTERM", paymentTermEightDays);
        }

        var vatCompanyGroupVerlegd = GetVatCompanyGroupVerlegd();
        if (vatCompanyGroupVerlegd != 0)
        {
            rsRelation.SetFieldValue("FK_VATCOMPANYGROUP", vatCompanyGroupVerlegd);
        }


        var updateResult = rsRelation.Update2();

        if (updateResult != null && updateResult.Any(x => x.HasError))
        {
            Log.Error("Error", $"Het aanmaken van een nieuwe relatie is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "");
            return 0;
        }

        return (int)updateResult.First().PrimaryKey;
    }

    private int GetOrCreateAddresObdam()
    {
        var rsAddress = GetRecordset("R_ADDRESS", "",
            $"CITY IN ('OBDAM', 'Obdam') AND STREET = '' AND ZIPCODE = ''", "");

        if (rsAddress.RecordCount > 0)
        {
            rsAddress.MoveFirst();
            return (int)rsAddress.GetField("PK_R_ADDRESS").Value;
        }

        rsAddress.UseDataChanges = false;
        rsAddress.UpdateWhenMoveRecord = false;

        rsAddress.AddNew();
        rsAddress.Fields["CITY"].Value = "OBDAM";
        rsAddress.Fields["FK_COUNTRY"].Value = GetCountryNl();
        var updateResult = rsAddress.Update2();

        if (updateResult != null && updateResult.Any(x => x.HasError))
        {
            Log.Error("Error", $"Het aanmaken van nieuw Obdam adres is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "");
            return 0;
        }

        return (int)updateResult.First().PrimaryKey;
    }

    private int GetCountryNl()
    {
        return GetRecordset("R_COUNTRY", "PK_R_COUNTRY",
            "CODE = 'NL'", "").DataTable.AsEnumerable().FirstOrDefault()?.Field<int>("PK_R_COUNTRY") ?? 0;
    }

    private int GetVatCompanyGroupVerlegd()
    {
        var allVatCompanyGroups = GetRecordset("R_VATCOMPANYGROUP", "PK_R_VATCOMPANYGROUP, CODE",
            "", "").DataTable.AsEnumerable().ToList();

        //Bij de Anton bedrijven is BTW bedrijfsgroep 'NLVERL' de NL verlegde btw code
        if (allVatCompanyGroups.Any(x => x.Field<string>("CODE").Equals("NLVERL")))
        {
            return allVatCompanyGroups.First(x => x.Field<string>("CODE").Equals("NLVERL"))
                .Field<int>("PK_R_VATCOMPANYGROUP");
        }

        //Bij de Loos bedrijven is BTW bedrijfsgroep '3' de NL verlegde btw code
        if (allVatCompanyGroups.Any(x => x.Field<string>("CODE").Equals("3")))
        {
            return allVatCompanyGroups.First(x => x.Field<string>("CODE").Equals("3"))
                .Field<int>("PK_R_VATCOMPANYGROUP");
        }

        return 0;
    }

    private int GetPayMentTermIdByCode(string code)
    {
        var rsPaymentTerm = GetRecordset("R_PAYMENTTERM", "PK_R_PAYMENTTERM",
            $"CODE = '{code}'", "");

        if (rsPaymentTerm.RecordCount == 0)
        {
            return 0;
        }

        rsPaymentTerm.MoveFirst();
        return (int)rsPaymentTerm.Fields["PK_R_PAYMENTTERM"].Value;
    }

    private int GetOrCreateLeveranciersDiscipline(string description)
    {
        var rsLeveraniersDiscipline = GetRecordset("U_BEDRIJFSTAKKEN", "PK_U_BEDRIJFSTAKKEN, DESCRIPTION",
            $"DESCRIPTION = '{description}'", "");

        if (rsLeveraniersDiscipline.RecordCount > 0)
        {
            rsLeveraniersDiscipline.MoveFirst();
            return (int)rsLeveraniersDiscipline.Fields["PK_U_BEDRIJFSTAKKEN"].Value;
        }
        else
        {
            rsLeveraniersDiscipline.AddNew();
            rsLeveraniersDiscipline.Fields["DESCRIPTION"].Value = description;
            rsLeveraniersDiscipline.Update();

            return (int)rsLeveraniersDiscipline.Fields["PK_U_BEDRIJFSTAKKEN"].Value;
        }
    }

    private object GetRelationType(string code)
    {
        var rsRelationType = GetRecordset("R_RELATIONTYPE", "PK_R_RELATIONTYPE",
            $"CODE = '{code}'", "");

        if (rsRelationType.RecordCount == 0)
        {
            return DBNull.Value;
        }

        rsRelationType.MoveFirst();
        return (int)rsRelationType.Fields["PK_R_RELATIONTYPE"].Value;
    }
}
