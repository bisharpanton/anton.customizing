﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;

public class ExportContactToWkaFile : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //SP
        //16-3-2023
        //Exporteer contactpersoon het WKA-dossier

        var rsContact = GetRecordset("R_CONTACT", "FK_RELATION, FK_PERSON",
            $"PK_R_CONTACT = {id}", "").DataTable.AsEnumerable().First();

        var relationId = rsContact.Field<int>("FK_RELATION");
        var personId = rsContact.Field<int>("FK_PERSON");

        var rsRelation = GetRecordset("R_RELATION", "COCNUMBER", $"PK_R_RELATION = {relationId} AND WKA = 1", "");

        if(rsRelation.RecordCount == 0)
        {
            return;//WKA niet van toepassing
        }

        var CocNumber = rsRelation.DataTable.AsEnumerable().First().Field<string>("COCNUMBER");

        var person = GetRecordset("R_PERSON", "FIRSTNAME, NAMEPREFIX, LASTNAME",
            $"PK_R_PERSON = {personId}", "")
            .DataTable.AsEnumerable().Select(x => new Person()
            {
                CocNumber = CocNumber,
                FirstName = x.Field<string>("FIRSTNAME"),
                NamePrefix = x.Field<string>("NAMEPREFIX"),
                LastName = x.Field<string>("LASTNAME")

            }).First();



        CreateContactInWkaFile(person);


    }

    private void CreateContactInWkaFile(Person person)
    {
        using (var httpClient = new HttpClient())
        {
            var url = $"https://localhost:44388/api/WkaContacts";
            //httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accesToken);

            HttpResponseMessage response;
            try
            {
                response = httpClient.PostAsJsonAsync(url, person).Result;
            }
            catch (Exception e)
            {
                Log.Error("Error", $"Aanmaken contactpersoon in WKA-dossier is mislukt, oorzaak: {e.Message}", "");
                return;
            }

            if (!response.IsSuccessStatusCode)
            {
                var resultString = response.Content.ReadAsStringAsync().Result;
                Log.Error("Error", $"Aanmaken WKA-dossier is mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase} {resultString}", "");
                return;
            }
        }
    }

    public class Person
    {
        public string CocNumber { get; set; }
        public string FirstName { get; set; }
        public string NamePrefix { get; set; }
        public string LastName { get; set; }
    }
}
