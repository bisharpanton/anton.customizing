﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;
using System.Globalization;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Runtime.InteropServices.WindowsRuntime;
using Workflowscripts.C_CREDITSAFEMONITORINGEVENTS.ProcessMonitoringEvent.ProcessMonitoringEvent.Models;

public class ProcessMonitoringEvent_RidderScript : WorkflowScriptInfo   
{
    public Boolean Execute()
    {
        //DB
        //9-7-2020
        //Verwerk monitoring event naar de relatie

        var id = (int)RecordId;

        var monitoringEvent = GetRecordset("C_CREDITSAFEMONITORINGEVENTS", "",
            $"PK_C_CREDITSAFEMONITORINGEVENTS = {id}", "").As<MonitoringEvent>().First();
        
        if(!monitoringEvent.FK_RELATION.HasValue)
        {
            LogImportMessage(id, "Huidige monitoring event is niet aan een relatie gekoppeld.");

            return false;
        }

        return ProcessToRelation(monitoringEvent);
    }

    private bool ProcessToRelation(MonitoringEvent monitoringEvent)
    {
        var columnNameToChange = DetermineColumnNameToChange(monitoringEvent.MONITORINGRULECODE);

        if (string.IsNullOrEmpty(columnNameToChange))
        {
            LogImportMessage(monitoringEvent.PK_C_CREDITSAFEMONITORINGEVENTS, 
                $"Geen kolom gevonden om te wijzigen voor monitoring code {monitoringEvent.MONITORINGRULECODE}");

            return false;
        }

        var newValue = DetermineNewValue(monitoringEvent);

        if (newValue == null)
        {
            return false;
        }

        var rsRelation = GetRecordset("R_RELATION", columnNameToChange,
            $"PK_R_RELATION = {monitoringEvent.FK_RELATION.Value}", "");
        rsRelation.MoveFirst();

        var importMessage = string.Empty;

        //1504, bedrijf is corrupt is een uitzondering op de anderen.
        if (monitoringEvent.MONITORINGRULECODE.Equals("1504"))
        {
            rsRelation.SetFieldValue("SALESCREDITLIMITSCORE", "geen score - faillissement");
            rsRelation.SetFieldValue("SALESCREDITLIMIT", 0.0);
            rsRelation.SetFieldValue("BLOCKED", true);

            var currentName = rsRelation.GetField("NAME").Value.ToString();
            rsRelation.SetFieldValue("NAME", $"{currentName} (FAILLIET)");

            importMessage = "Relatie is Failliet. Relatie op geblokkeerd gezet en kredietlimiet op nul gezet.";
        }
        else
        {
            var oldValue = rsRelation.GetField(columnNameToChange).Value;
            rsRelation.SetFieldValue(columnNameToChange, newValue);

            importMessage = $"{oldValue} aangepast naar {newValue}";
        }
        
        
        var updateResult = rsRelation.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            LogImportMessage(monitoringEvent.PK_C_CREDITSAFEMONITORINGEVENTS, 
                $"Relatie bijwerken is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");

            return false;
        }

        LogImportMessage(monitoringEvent.PK_C_CREDITSAFEMONITORINGEVENTS, importMessage);

        return true;
    }

    private object DetermineNewValue(MonitoringEvent monitoringEvent)
    {
        if (monitoringEvent.MONITORINGRULECODE.Equals("101") || monitoringEvent.MONITORINGRULECODE.Equals("1503"))
        {
            if (!int.TryParse(monitoringEvent.NEWVALUE, NumberStyles.Any, CultureInfo.CurrentCulture,
                out int iNewValue))
            {
                LogImportMessage(monitoringEvent.PK_C_CREDITSAFEMONITORINGEVENTS,
                    $"Omzetten nieuwe kredietlimiet score '{monitoringEvent.NEWVALUE}' omzetten naar een getal is mislukt.");
                return null;
            }

            var creditSafeLetterScore = DetermineCreditSafeLetterAtThisNewScore(iNewValue);

            return string.IsNullOrEmpty(creditSafeLetterScore)
                ? monitoringEvent.NEWVALUE
                : $"{monitoringEvent.NEWVALUE} - {creditSafeLetterScore}";
        }
        else if (monitoringEvent.MONITORINGRULECODE.Equals("102"))
        {
            //Nieuw verkoop kredietlimiet
            if (!double.TryParse(monitoringEvent.NEWVALUE, NumberStyles.Any, CultureInfo.CurrentCulture,
                out double dNewValue))
            {
                LogImportMessage(monitoringEvent.PK_C_CREDITSAFEMONITORINGEVENTS, 
                    $"Omzetten nieuwe kredietlimiet '{monitoringEvent.NEWVALUE}' omzetten naar een getal is mislukt.");
                return null;
            }

            return dNewValue;
        }
        else if (monitoringEvent.MONITORINGRULECODE.Equals("1508"))
        {
            //Aantal werknemers
            if (!int.TryParse(monitoringEvent.NEWVALUE, NumberStyles.Any, CultureInfo.CurrentCulture,
                out int iNewValue))
            {
                LogImportMessage(monitoringEvent.PK_C_CREDITSAFEMONITORINGEVENTS, 
                    $"Omzetten nieuw aantal werknemers '{monitoringEvent.NEWVALUE}' omzetten naar een getal is mislukt.");
                return null;
            }

            return iNewValue;
        }
        else if (monitoringEvent.MONITORINGRULECODE.Equals("106"))
        {
            //Telefoon nummer
            return monitoringEvent.NEWVALUE;
        }
        else if (monitoringEvent.MONITORINGRULECODE.Equals("1504"))
        {
            //Bankruptcy Data, bedrijf is failliet
            return 0.0;
        }

        return null;
    }

    private string DetermineCreditSafeLetterAtThisNewScore(int newScore)
    {
        if (newScore <= 36)
        {
            return "D";
        }
        else if(newScore <= 58)
        {
            return "C";
        }
        else if(newScore <= 73)
        {
            return "B";
        }
        else if(newScore <= 100)
        {
            return "A";
        }

        return string.Empty;
    }

    private string DetermineColumnNameToChange(string ruleCode)
    {
        switch (ruleCode)
        {
            case "101":
                return "SALESCREDITLIMITSCORE";
            case "1503":
                return "SALESCREDITLIMITSCORE";
            case "102":
                return "SALESCREDITLIMIT";
            case "1508":
                return "NUMBEROFEMPLOYEES";
            case "106":
                return "PHONE1";
            case "1504":
                return "NAME, SALESCREDITLIMIT, SALESCREDITLIMITSCORE, BLOCKED";
            default:
                return string.Empty;
        }
    }

    private void LogImportMessage(int monitoringEventId, string importMessage)
    {
        var rsMonitoringEvent = GetRecordset("C_CREDITSAFEMONITORINGEVENTS", "",
            $"PK_C_CREDITSAFEMONITORINGEVENTS = {monitoringEventId}", "");
        rsMonitoringEvent.MoveFirst();
        rsMonitoringEvent.SetFieldValue("LOG", importMessage);
        rsMonitoringEvent.Update();
    }
}
