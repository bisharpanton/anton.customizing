﻿/*
 * <Add a description.txt to your project> 
 * 
 * Changelog: 
 *   
 *   <add a changelog.txt to your project>
 * 
 * References:
 * 
 *   Newtonsoft.Json (C:\Projects\Anton\Anton.Customizing\packages\Newtonsoft.Json.12.0.1\lib\net45\Newtonsoft.Json.dll)
 *   Ridder.iQ.Common.Recordset.Extensions (C:\Projects\Anton\Anton.Customizing\packages\Ridder.ClientDevelopment.1.8.251.16\lib\Ridder.iQ.Common.Recordset.Extensions.dll)
 * 
 * Generated on: 2023-06-01 16:31 by Douwe Beukers
 *   
 */

/*
 * RidderScript.cs
 */
using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;
using System.Globalization;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Runtime.InteropServices.WindowsRuntime;
using Workflowscripts.C_CREDITSAFEMONITORINGEVENTS.ProcessMonitoringEvent.ProcessMonitoringEvent.Models;

public class ProcessMonitoringEvent_RidderScript : WorkflowScriptInfo   
{
    public Boolean Execute()
    {
        //DB
        //9-7-2020
        //Verwerk monitoring event naar de relatie

        var id = (int)RecordId;

        var monitoringEvent = GetRecordset("C_CREDITSAFEMONITORINGEVENTS", "",
            $"PK_C_CREDITSAFEMONITORINGEVENTS = {id}", "").As<MonitoringEvent>().First();
        
        if(!monitoringEvent.FK_RELATION.HasValue)
        {
            LogImportMessage(id, "Huidige monitoring event is niet aan een relatie gekoppeld.");

            return false;
        }

        return ProcessToRelation(monitoringEvent);
    }

    private bool ProcessToRelation(MonitoringEvent monitoringEvent)
    {
        var columnNameToChange = DetermineColumnNameToChange(monitoringEvent.MONITORINGRULECODE);

        if (string.IsNullOrEmpty(columnNameToChange))
        {
            LogImportMessage(monitoringEvent.PK_C_CREDITSAFEMONITORINGEVENTS, 
                $"Geen kolom gevonden om te wijzigen voor monitoring code {monitoringEvent.MONITORINGRULECODE}");

            return false;
        }

        var newValue = DetermineNewValue(monitoringEvent);

        if (newValue == null)
        {
            return false;
        }

        var rsRelation = GetRecordset("R_RELATION", columnNameToChange,
            $"PK_R_RELATION = {monitoringEvent.FK_RELATION.Value}", "");
        rsRelation.MoveFirst();

        var importMessage = string.Empty;

        //1504, bedrijf is corrupt is een uitzondering op de anderen.
        if (monitoringEvent.MONITORINGRULECODE.Equals("1504"))
        {
            rsRelation.SetFieldValue("SALESCREDITLIMITSCORE", "geen score - faillissement");
            rsRelation.SetFieldValue("SALESCREDITLIMIT", 0.0);
            rsRelation.SetFieldValue("BLOCKED", true);

            var currentName = rsRelation.GetField("NAME").Value.ToString();
            rsRelation.SetFieldValue("NAME", $"{currentName} (FAILLIET)");

            importMessage = "Relatie is Failliet. Relatie op geblokkeerd gezet en kredietlimiet op nul gezet.";
        }
        else
        {
            var oldValue = rsRelation.GetField(columnNameToChange).Value;
            rsRelation.SetFieldValue(columnNameToChange, newValue);

            importMessage = $"{oldValue} aangepast naar {newValue}";
        }
        
        
        var updateResult = rsRelation.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            LogImportMessage(monitoringEvent.PK_C_CREDITSAFEMONITORINGEVENTS, 
                $"Relatie bijwerken is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");

            return false;
        }

        LogImportMessage(monitoringEvent.PK_C_CREDITSAFEMONITORINGEVENTS, importMessage);

        return true;
    }

    private object DetermineNewValue(MonitoringEvent monitoringEvent)
    {
        if (monitoringEvent.MONITORINGRULECODE.Equals("101") || monitoringEvent.MONITORINGRULECODE.Equals("1503"))
        {
            if (!int.TryParse(monitoringEvent.NEWVALUE, NumberStyles.Any, CultureInfo.CurrentCulture,
                out int iNewValue))
            {
                LogImportMessage(monitoringEvent.PK_C_CREDITSAFEMONITORINGEVENTS,
                    $"Omzetten nieuwe kredietlimiet score '{monitoringEvent.NEWVALUE}' omzetten naar een getal is mislukt.");
                return null;
            }

            var creditSafeLetterScore = DetermineCreditSafeLetterAtThisNewScore(iNewValue);

            return string.IsNullOrEmpty(creditSafeLetterScore)
                ? monitoringEvent.NEWVALUE
                : $"{monitoringEvent.NEWVALUE} - {creditSafeLetterScore}";
        }
        else if (monitoringEvent.MONITORINGRULECODE.Equals("102"))
        {
            //Nieuw verkoop kredietlimiet
            if (!double.TryParse(monitoringEvent.NEWVALUE, NumberStyles.Any, CultureInfo.CurrentCulture,
                out double dNewValue))
            {
                LogImportMessage(monitoringEvent.PK_C_CREDITSAFEMONITORINGEVENTS, 
                    $"Omzetten nieuwe kredietlimiet '{monitoringEvent.NEWVALUE}' omzetten naar een getal is mislukt.");
                return null;
            }

            return dNewValue;
        }
        else if (monitoringEvent.MONITORINGRULECODE.Equals("1508"))
        {
            //Aantal werknemers
            if (!int.TryParse(monitoringEvent.NEWVALUE, NumberStyles.Any, CultureInfo.CurrentCulture,
                out int iNewValue))
            {
                LogImportMessage(monitoringEvent.PK_C_CREDITSAFEMONITORINGEVENTS, 
                    $"Omzetten nieuw aantal werknemers '{monitoringEvent.NEWVALUE}' omzetten naar een getal is mislukt.");
                return null;
            }

            return iNewValue;
        }
        else if (monitoringEvent.MONITORINGRULECODE.Equals("106"))
        {
            //Telefoon nummer
            return monitoringEvent.NEWVALUE;
        }
        else if (monitoringEvent.MONITORINGRULECODE.Equals("1504"))
        {
            //Bankruptcy Data, bedrijf is failliet
            return 0.0;
        }

        return null;
    }

    private string DetermineCreditSafeLetterAtThisNewScore(int newScore)
    {
        if (newScore <= 36)
        {
            return "D";
        }
        else if(newScore <= 58)
        {
            return "C";
        }
        else if(newScore <= 73)
        {
            return "B";
        }
        else if(newScore <= 100)
        {
            return "A";
        }

        return string.Empty;
    }

    private string DetermineColumnNameToChange(string ruleCode)
    {
        switch (ruleCode)
        {
            case "101":
                return "SALESCREDITLIMITSCORE";
            case "1503":
                return "SALESCREDITLIMITSCORE";
            case "102":
                return "SALESCREDITLIMIT";
            case "1508":
                return "NUMBEROFEMPLOYEES";
            case "106":
                return "PHONE1";
            case "1504":
                return "NAME, SALESCREDITLIMIT, SALESCREDITLIMITSCORE, BLOCKED";
            default:
                return string.Empty;
        }
    }

    private void LogImportMessage(int monitoringEventId, string importMessage)
    {
        var rsMonitoringEvent = GetRecordset("C_CREDITSAFEMONITORINGEVENTS", "",
            $"PK_C_CREDITSAFEMONITORINGEVENTS = {monitoringEventId}", "");
        rsMonitoringEvent.MoveFirst();
        rsMonitoringEvent.SetFieldValue("LOG", importMessage);
        rsMonitoringEvent.Update();
    }
}

/*
 * MonitoringEvent.generated.cs
 */
namespace Workflowscripts.C_CREDITSAFEMONITORINGEVENTS.ProcessMonitoringEvent.ProcessMonitoringEvent.Models
{
    
    
    //  <auto-generated />
    using System;
    
    
    ///  <summary>
    ///  	 Table: C_CREDITSAFEMONITORINGEVENTS
    ///  </summary>
    ///  <remarks>
    /// 	Available columns (for debugging, not necessarily available in this class):
    ///  	CREATOR, DATECHANGED, DATECREATED, EVENTDATE, EVENTID, EXTERNALKEY, FK_RELATION, FK_WORKFLOWSTATE, LOG, MONITORINGRULECODE
    ///  	MONITORINGRULEDESCRIPTION, NEWVALUE, OLDVALUE, PK_C_CREDITSAFEMONITORINGEVENTS, PLAINTEXT_LOG, RECORDLINK, USERCHANGED
    ///  
    ///  	Filter used: FK_TABLEINFO = '987d3892-ef96-446c-9cb6-0791113906b1' AND (ISSECRET = 0)
    ///  	Included columns: 
    ///  	Excluded columns: 
    ///  </remarks>
    public partial class MonitoringEvent
    {
        
        private string _CREATOR;
        
        private System.Nullable<System.DateTime> _DATECHANGED;
        
        private System.Nullable<System.DateTime> _DATECREATED;
        
        private System.Nullable<System.DateTime> _EVENTDATE;
        
        private System.Nullable<int> _EVENTID;
        
        private string _EXTERNALKEY;
        
        private System.Nullable<int> _FK_RELATION;
        
        private System.Nullable<System.Guid> _FK_WORKFLOWSTATE;
        
        private string _LOG;
        
        private string _MONITORINGRULECODE;
        
        private string _MONITORINGRULEDESCRIPTION;
        
        private string _NEWVALUE;
        
        private string _OLDVALUE;
        
        private int _PK_C_CREDITSAFEMONITORINGEVENTS;
        
        private string _PLAINTEXT_LOG;
        
        private string _RECORDLINK;
        
        private string _USERCHANGED;
        
        /// <summary>
        /// 	Aangemaakt door
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        public virtual string CREATOR
        {
            get
            {
                if (string.IsNullOrEmpty(this._CREATOR))
                {
                    return "";
                }
                return this._CREATOR;
            }
            set
            {
                this._CREATOR = value;
            }
        }
        
        /// <summary>
        /// 	Datum gewijzigd
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        public virtual System.Nullable<System.DateTime> DATECHANGED
        {
            get
            {
                return this._DATECHANGED;
            }
            set
            {
                this._DATECHANGED = value;
            }
        }
        
        /// <summary>
        /// 	Datum aangemaakt
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        public virtual System.Nullable<System.DateTime> DATECREATED
        {
            get
            {
                return this._DATECREATED;
            }
            set
            {
                this._DATECREATED = value;
            }
        }
        
        /// <summary>
        /// 	Event datum
        /// </summary>
        /// <remarks>
        /// 	DataType: DateTime
        /// 	Size: 8
        /// </remarks>
        public virtual System.Nullable<System.DateTime> EVENTDATE
        {
            get
            {
                return this._EVENTDATE;
            }
            set
            {
                this._EVENTDATE = value;
            }
        }
        
        /// <summary>
        /// 	Event id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        public virtual System.Nullable<int> EVENTID
        {
            get
            {
                return this._EVENTID;
            }
            set
            {
                this._EVENTID = value;
            }
        }
        
        /// <summary>
        /// 	Externe sleutel
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 100
        /// </remarks>
        public virtual string EXTERNALKEY
        {
            get
            {
                if (string.IsNullOrEmpty(this._EXTERNALKEY))
                {
                    return "";
                }
                return this._EXTERNALKEY;
            }
            set
            {
                this._EXTERNALKEY = value;
            }
        }
        
        /// <summary>
        /// 	Relatie
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: C_CREDITSAFEMONITORINGEVENTS
        /// </remarks>
        public virtual System.Nullable<int> FK_RELATION
        {
            get
            {
                return this._FK_RELATION;
            }
            set
            {
                this._FK_RELATION = value;
            }
        }
        
        /// <summary>
        /// 	Workflowstatus
        /// </summary>
        /// <remarks>
        /// 	DataType: Guid
        /// 	Size: 4
        /// 	Table: C_CREDITSAFEMONITORINGEVENTS
        /// </remarks>
        public virtual System.Nullable<System.Guid> FK_WORKFLOWSTATE
        {
            get
            {
                return this._FK_WORKFLOWSTATE;
            }
            set
            {
                this._FK_WORKFLOWSTATE = value;
            }
        }
        
        /// <summary>
        /// 	Log
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        public virtual string LOG
        {
            get
            {
                if (string.IsNullOrEmpty(this._LOG))
                {
                    return "";
                }
                return this._LOG;
            }
            set
            {
                this._LOG = value;
            }
        }
        
        /// <summary>
        /// 	Monitoring regel code
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        public virtual string MONITORINGRULECODE
        {
            get
            {
                if (string.IsNullOrEmpty(this._MONITORINGRULECODE))
                {
                    return "";
                }
                return this._MONITORINGRULECODE;
            }
            set
            {
                this._MONITORINGRULECODE = value;
            }
        }
        
        /// <summary>
        /// 	Monitoring regel
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 250
        /// </remarks>
        public virtual string MONITORINGRULEDESCRIPTION
        {
            get
            {
                if (string.IsNullOrEmpty(this._MONITORINGRULEDESCRIPTION))
                {
                    return "";
                }
                return this._MONITORINGRULEDESCRIPTION;
            }
            set
            {
                this._MONITORINGRULEDESCRIPTION = value;
            }
        }
        
        /// <summary>
        /// 	Nieuwe waarde
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 150
        /// </remarks>
        public virtual string NEWVALUE
        {
            get
            {
                if (string.IsNullOrEmpty(this._NEWVALUE))
                {
                    return "";
                }
                return this._NEWVALUE;
            }
            set
            {
                this._NEWVALUE = value;
            }
        }
        
        /// <summary>
        /// 	Oude waarde
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 150
        /// </remarks>
        public virtual string OLDVALUE
        {
            get
            {
                if (string.IsNullOrEmpty(this._OLDVALUE))
                {
                    return "";
                }
                return this._OLDVALUE;
            }
            set
            {
                this._OLDVALUE = value;
            }
        }
        
        /// <summary>
        /// 	Credit Safe monitoring events id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        public virtual int PK_C_CREDITSAFEMONITORINGEVENTS
        {
            get
            {
                return this._PK_C_CREDITSAFEMONITORINGEVENTS;
            }
            set
            {
                this._PK_C_CREDITSAFEMONITORINGEVENTS = value;
            }
        }
        
        /// <summary>
        /// 	Log
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        public virtual string PLAINTEXT_LOG
        {
            get
            {
                if (string.IsNullOrEmpty(this._PLAINTEXT_LOG))
                {
                    return "";
                }
                return this._PLAINTEXT_LOG;
            }
            set
            {
                this._PLAINTEXT_LOG = value;
            }
        }
        
        /// <summary>
        /// 	Recordlink
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 0
        /// </remarks>
        public virtual string RECORDLINK
        {
            get
            {
                if (string.IsNullOrEmpty(this._RECORDLINK))
                {
                    return "";
                }
                return this._RECORDLINK;
            }
            set
            {
                this._RECORDLINK = value;
            }
        }
        
        /// <summary>
        /// 	Gewijzigd door
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        public virtual string USERCHANGED
        {
            get
            {
                if (string.IsNullOrEmpty(this._USERCHANGED))
                {
                    return "";
                }
                return this._USERCHANGED;
            }
            set
            {
                this._USERCHANGED = value;
            }
        }
    }
}

/*
 * Relation.generated.cs
 */
namespace Workflowscripts.C_CREDITSAFEMONITORINGEVENTS.ProcessMonitoringEvent.ProcessMonitoringEvent.Models
{
    //  <auto-generated />
    using System;
    
    
    ///  <summary>
    ///  	 Table: R_RELATION
    ///  </summary>
    ///  <remarks>
    /// 	Available columns (for debugging, not necessarily available in this class):
    ///  	BANKACCOUNTNUMBER, BANKCITY, BANKNAME, BIC, BICGACCOUNT, BLOCKED, CCPURCHASEORDER, CHECKDATEVATNUMBER, COCCITY, COCDATE
    ///  	COCNUMBER, CODE, CREATOR, CREDITSAFEID, CUSTOMERSATISFACTION, DATECHANGED, DATECREATED, DATECUSTOMER, DATEPROSPECT, DEFAULTSALESMARKUP
    ///  	EANCODE, EMAIL, EXTERNALKEY, FACTUURMAIL, FAX, FK_AREA, FK_BEDRIJFSTAK1, FK_BEDRIJFSTAK2, FK_BEDRIJFSTAK3, FK_COSTINGPARAMETERS
    ///  	FK_COSTUNIT, FK_CURRENCY, FK_DEALER, FK_DEFAULTORDER, FK_DEFAULTPURCHASELEDGERACCOUNT, FK_INDUSTRY, FK_INDUSTRYCUSTOM, FK_LANGUAGE, FK_LEGALFORM, FK_PARENTCOMPANY
    ///  	FK_POSTALADDRESS, FK_PRIVATEPERSON, FK_PURCHASEPAYMENTMETHOD, FK_PURCHASEPAYMENTTERM, FK_PURCHASESHIPPINGTYPE, FK_R_RELATION, FK_RELATIONFINANCIALSTATE, FK_RELATIONTYPE, FK_REVENUECODE, FK_SALESCREDITLIMITCURRENCY
    ///  	FK_SALESINVOICERELATION, FK_SALESPAYMENTMETHOD, FK_SALESPAYMENTTERM, FK_SALESPERSON, FK_SALESPRICERELATION, FK_SALESSHIPPINGTYPE, FK_SBIMAINACTIVITY, FK_SHIPPINGAGENT, FK_SOURCE, FK_VATCOMPANYGROUP
    ///  	FK_VISITINGADDRESS, FK_WORKFLOWSTATE, GIRO, IBAN, IBANGACCOUNT, INTEGRATIONID, INTERCOMPANYRELATION, INTERNALMEMO, LASTDATEIMPORTCREDITSAFE, MAILING
    ///  	MAINCONTACT, MAINCONTACTCREDITOR, MAINCONTACTDEBTOR, NAME, NUMBEROFEMPLOYEES, PERCENTAGEGACCOUNT, PHONE1, PHONE2, PK_R_RELATION, PLAINTEXT_INTERNALMEMO
    ///  	PLAINTEXT_WARNING, PRIVATEPERSON, PURCHASECREDITLIMIT, PURCHASEDISCOUNT, PURCHASEORDERMAIL, PURCHASESHIPPINGPLACE, PURCHASESMALLORDERHANDLING, PURCHASESMALLORDERLIMIT, PURCHASESMALLORDERSURCHARGEAMOUNT, RECORDLINK
    ///  	SALESCREDITLIMIT, SALESCREDITLIMITSCORE, SALESDISCOUNT, SALESORDERSURCHARGEAMOUNT, SALESSHIPPINGPLACE, SALESSMALLORDERHANDLING, SALESSMALLORDERLIMIT, SALESSTAGE, SEARCHCODE, SHOWWARNING
    ///  	SPEEDDIAL, U_VERKOOPSTADIUM, USERCHANGED, VATNUMBER, VCA, VENDOR, WARNING, WEBSITE, WKA
    ///  
    ///  	Filter used: FK_TABLEINFO = 'b328fa3c-25e1-4b0a-9c0e-32631b19a160' AND (ISSECRET = 0)
    ///  	Included columns: SALESCREDITLIMIT, SALESCREDITLIMITSCORE
    ///  	Excluded columns: 
    ///  </remarks>
    public partial class Relation
    {
        
        private int _PK_R_RELATION;
        
        private System.Nullable<double> _SALESCREDITLIMIT;
        
        private string _SALESCREDITLIMITSCORE;
        
        /// <summary>
        /// 	Business relations id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        public virtual int PK_R_RELATION
        {
            get
            {
                return this._PK_R_RELATION;
            }
            set
            {
                this._PK_R_RELATION = value;
            }
        }
        
        /// <summary>
        /// 	Kredietlimiet verk
        /// </summary>
        /// <remarks>
        /// 	DataType: Money
        /// 	Size: 8
        /// </remarks>
        public virtual System.Nullable<double> SALESCREDITLIMIT
        {
            get
            {
                return this._SALESCREDITLIMIT;
            }
            set
            {
                this._SALESCREDITLIMIT = value;
            }
        }
        
        /// <summary>
        /// 	Kredietlimiet verk score
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 50
        /// </remarks>
        public virtual string SALESCREDITLIMITSCORE
        {
            get
            {
                if (string.IsNullOrEmpty(this._SALESCREDITLIMITSCORE))
                {
                    return "";
                }
                return this._SALESCREDITLIMITSCORE;
            }
            set
            {
                this._SALESCREDITLIMITSCORE = value;
            }
        }
    }
}

