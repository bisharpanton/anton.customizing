﻿# How to nest generated files (*.generated.cs) under Models.tt

To nest all generated files (*.generated.cs) under 'Models.tt', add the following snippet in your .csproj file:

    <ItemGroup>
      <Compile Update="ProcessMonitoringEvent\Models\*.generated.cs">
        <AutoGen>True</AutoGen>
        <DependentUpon>Models.tt</DependentUpon>
      </Compile>
    </ItemGroup>

(sadly, this is not yet possible using the Visual Studio templating system)
