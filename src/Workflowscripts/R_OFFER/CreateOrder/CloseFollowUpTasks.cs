﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class CloseFollowUpTasks : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //10-12-2019
        //Sluit de opvolg-taak indien aanwezig

        var wfStateClosed = new Guid("43ea659f-14bb-473a-869b-819c63ef43ac");
        var followUpTodos = GetRecordset("R_TODO", "PK_R_TODO",
                $"FK_OFFER = {id} AND ISOFFERFOLLOWUPTODO = 1 AND FK_WORKFLOWSTATE <> '{wfStateClosed}'", "")
            .DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_TODO")).ToList();

        if (!followUpTodos.Any())
        {
            return; //Geen opvolgtaken deze offerte
        }

        var wfToDone = new Guid("d94de796-4b24-44ff-9c66-230d1511d144");
        foreach (var followUpTodo in followUpTodos)
        {
            var result = ExecuteWorkflowEvent("R_TODO", followUpTodo, wfToDone, null);

            if (result.HasError)
            {
                Log.Info("Info", $"Afsluiten opvolg-taak is mislukt; oorzaak: {result.GetResult()}", "");
            }
        }

    }
}
