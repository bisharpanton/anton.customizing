﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class UpdateOrder : WorkflowScriptInfo
{
    public void Execute()
    {
        // HVE
        // 14-12-2021
        // Update order with offer info

        var offerId = (int)RecordId;

        var orderId = EventsAndActions.Sales.Actions.GetOrderIdsFromSalesOffer(offerId).OrderByDescending(x => x).FirstOrDefault() as int? ?? 0;

        if (orderId == 0)
        {
            return;
        }

        UpdatePurchaseOffersBasedOnSalesoffer(offerId, orderId);

        //DB
        //30-11-2023
        //Neem totaal begrote kostprijs over en vul deze in bij de projectbeheersing functinonaliteit
        UpdateTotalExpectedCosts(orderId);
    }

    private void UpdateTotalExpectedCosts(int orderId)
    {
        var allSalesOrderDetails = GetRecordset("R_SALESORDERALLDETAIL", "COSTPRICE, NETSALESAMOUNT",
            $"FK_ORDER = {orderId} AND SOURCE_TABLENAME <> 'R_SALESORDERDETAILTEXT'", "").DataTable.AsEnumerable().ToList();

        var totalCostPrice = allSalesOrderDetails.Sum(x => x.Field<double?>("COSTPRICE") ?? 0);
        var totalNetSalesAmount = allSalesOrderDetails.Sum(x => x.Field<double?>("NETSALESAMOUNT") ?? 0);

        var rsOrder = GetRecordset("R_ORDER", "", $"PK_R_ORDER = {orderId}", "");
        rsOrder.UseDataChanges = false; //We kunnen niet van de bestaande datachanges gebruik maken. TOTALNETAMOUNT is nog niet bekend

        rsOrder.MoveFirst();
        rsOrder.SetFieldValue("REMAININGCOST", totalCostPrice);
        rsOrder.SetFieldValue("EXPECTEDTOTALCOSTS", totalCostPrice);

        double margin = totalNetSalesAmount - totalCostPrice;
        double marginPerc = totalNetSalesAmount == 0.0 ? 0.0 : margin / totalNetSalesAmount;

        rsOrder.SetFieldValue("EXPECTEDMARGINAMOUNT", margin);
        rsOrder.SetFieldValue("EXPECTEDMARGINPERC", marginPerc);

        rsOrder.Update();
    }

    private double DetermineTotalWip(int orderId)
    {
        //TODO: sdfdS
        return 0.0;
    }

    private double DeterminePurchaseCosts(int orderId)
    {
        //TODO: sdf
        return 0.0;
    }

    private void UpdatePurchaseOffersBasedOnSalesoffer(int offerId, int orderId)
    {
        var rsPurchaseOffers = GetRecordset("R_PURCHASEOFFER", "", $"FK_OFFER = {offerId} AND FK_ORDER IS NULL", "");

        if (rsPurchaseOffers.RecordCount == 0)
        {
            return;
        }

        var purchaseOfferId = (int)rsPurchaseOffers.Fields["PK_R_PURCHASEOFFER"].Value;
        if (OrderSetInDetails(purchaseOfferId))
        {
            return;
        }

        rsPurchaseOffers.UpdateWhenMoveRecord = false;
        rsPurchaseOffers.MoveFirst();

        while (!rsPurchaseOffers.EOF)
        {
            var wfState = (Guid)rsPurchaseOffers.Fields["FK_WORKFLOWSTATE"].Value;
            if (wfState != PurchaseOfferWfstates.Cancelled)
            {
                rsPurchaseOffers.SetFieldValue("FK_ORDER", orderId);
            }

            rsPurchaseOffers.MoveNext();
        }

        rsPurchaseOffers.MoveFirst();
        rsPurchaseOffers.Update();
    }

    private bool OrderSetInDetails(int purchaseOfferId)
    {
        return GetRecordset("R_PURCHASEOFFERALLDETAIL", "", 
                $"FK_PURCHASEOFFER = {purchaseOfferId} AND NOT FK_ORDER IS NULL", "")
            .DataTable
            .AsEnumerable()
            .Any();
    }

    public static class PurchaseOfferWfstates
    {
        public static Guid Cancelled = new Guid("a8ad2779-d5f5-4486-a0ce-925f83ec41a5");
        public static Guid Done = new Guid("091a6927-656f-4388-9941-9fd74d07e5b0");
        public static Guid New = new Guid("eb12ba9a-2a51-4174-867c-fd3230044a6a");
    }
}
