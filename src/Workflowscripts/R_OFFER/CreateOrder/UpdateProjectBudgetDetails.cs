﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class UpdateProjectBudgetDetails : WorkflowScriptInfo
{
    Guid tablePhase = new Guid("7db2fca6-1cb4-4a2c-9f96-3b4376a5d37b");
    Guid tableJoborder = new Guid("7de0ac3e-9dfa-47e0-b113-63d3cac55f90");
    Guid tableBudget = new Guid("51e8daf8-b92c-4852-9518-433679420946");

    public void Execute()
    {
        var projectModuleActive = GetRecordset("R_CRMSETTINGS", "PROJECTMODULEACTIVE",
            "", "").DataTable.AsEnumerable().First().Field<bool>("PROJECTMODULEACTIVE");

        if(!projectModuleActive)
        {
            return;//Alleen van toepassing indien vinkje 'Projectenmodule actief' aanstaat
        }

        var id = (int)RecordId;

        var mainprojectId = GetRecordset("R_OFFER", "FK_MAINPROJECT",
            $"PK_R_OFFER = {id}", "").DataTable.AsEnumerable().FirstOrDefault().Field<int?>("FK_MAINPROJECT") as int? ?? 0;

        if(mainprojectId == 0)
        {
            return;//Geen project gekoppeld
        }

        var offerdetailMiscIds = GetRecordset("R_OFFERDETAILMISC", "PK_R_OFFERDETAILMISC",
            $"FK_OFFER = {id}", "").DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_OFFERDETAILMISC")).ToList();

        if(!offerdetailMiscIds.Any())
        {
            return;//Geen offerteregels divers
        }

        foreach(var offerdetailMiscId in offerdetailMiscIds)
        {
            //Doorvoeren van de meerwerk details naar de gekoppelde bonnen
            var additionalWorks = GetRecordset("U_ADDITIONALWORKDETAILS", "PK_U_ADDITIONALWORKDETAILS, FK_JOBORDER, DESCRIPTION, COSTPRICE, NOCOSTPRICENEEDED",
               $"FK_OFFERDETAILMISC = {offerdetailMiscId}", "SEQUENCENUMBER ASC")
           .DataTable.AsEnumerable().Select(x => new AddWork()
           {
               SourceId = x.Field<int>("PK_U_ADDITIONALWORKDETAILS"),
               JoborderId = x.Field<int?>("FK_JOBORDER") ?? 0,
               CostPrice = x.Field<double>("COSTPRICE"),
               Description = x.Field<string>("DESCRIPTION"),
               NoCostpriceNeeded = x.Field<bool>("NOCOSTPRICENEEDED"),
               Source = "AddWorks"

           }).ToList();

            if (!additionalWorks.Any())
            {
                return;
            }

            foreach (AddWork additionalWork in additionalWorks)
            {
                if (additionalWork.JoborderId == 0 && additionalWork.NoCostpriceNeeded == false)
                {
                    throw new Exception("Er zijn meerwerk details zonder gekoppelde bon. Alle regels behalve de regels met het vinkje 'Geen kostprijs benodigd' hebben een bon nodig.");
                }

                if (additionalWork.CostPrice == 0.0 && additionalWork.NoCostpriceNeeded == false)
                {
                    throw new Exception("Er zijn meerwerk details met kostprijs 0. Alle regels behalve de regels met het vinkje 'Geen kostprijs benodigd' hebben een kostprijs nodig.");
                }

                if (additionalWork.NoCostpriceNeeded == false)
                {
                    UpdateBudgetDetails(id, mainprojectId, (int)additionalWork.JoborderId, additionalWork.CostPrice, additionalWork.Description,
                    additionalWork.Source, additionalWork.SourceId);
                }

            }

            //Doervoeren van de opslagen naar de gekoppelde bonnen
            var additionalWorkMarkups = GetRecordset("U_ADDITIONALWORKMARKUP", "PK_U_ADDITIONALWORKMARKUP, FK_JOBORDER, DESCRIPTION, COSTPRICE, NOCOSTPRICENEEDED",
               $"FK_OFFERDETAILMISC = {offerdetailMiscId}", "SEQUENCENUMBER ASC")
           .DataTable.AsEnumerable().Select(x => new Markup()
           {
               SourceId = x.Field<int>("PK_U_ADDITIONALWORKMARKUP"),
               JoborderId = x.Field<int?>("FK_JOBORDER") ?? 0,
               CostPrice = x.Field<double>("COSTPRICE"),
               Description = "Opslag "+x.Field<string>("DESCRIPTION"),
               NoCostpriceNeeded = x.Field<bool>("NOCOSTPRICENEEDED"),
               Source = "Markups"

           }).ToList();

            if (!additionalWorkMarkups.Any())
            {
                return;
            }

            foreach (Markup additionalWorkMarkup in additionalWorkMarkups)
            {
                if (additionalWorkMarkup.JoborderId == 0 && additionalWorkMarkup.NoCostpriceNeeded == false)
                {
                    throw new Exception("Er zijn meerwerk opslagen regels zonder gekoppelde bon. Alle regels behalve de regels met het vinkje 'Geen kostprijs benodigd' hebben een bon nodig.");
                }

                if (additionalWorkMarkup.CostPrice == 0.0 && additionalWorkMarkup.NoCostpriceNeeded == false)
                {
                    throw new Exception("Er zijn meerwerk opslagen regels met kostprijs 0. Alle regels behalve de regels met het vinkje 'Geen kostprijs benodigd' hebben een kostprijs nodig.");
                }

                if(additionalWorkMarkup.NoCostpriceNeeded == false)
                {
                    UpdateBudgetDetails(id, mainprojectId, (int)additionalWorkMarkup.JoborderId, additionalWorkMarkup.CostPrice, additionalWorkMarkup.Description,
                    additionalWorkMarkup.Source, additionalWorkMarkup.SourceId);
                }
            }

        }

    }

    private void UpdateBudgetDetails(int offerId, int mainprojectId, int joborderId, double amount, string description, string source, int sourceId)
    {
        var projectPhase = GetRecordset("R_WORKBREAKDOWNINFO", "PARENTRECORDID",
            $"RECORDID = {joborderId} AND FK_TABLEINFO = '{tableJoborder}' AND FK_PARENTTABLEINFO = '{tablePhase}' AND FK_MAINPROJECT = {mainprojectId}", "")
            .DataTable.AsEnumerable().FirstOrDefault();

        var joborder = GetRecordset("R_JOBORDER", "JOBORDERNUMBER, DESCRIPTION",
            $"PK_R_JOBORDER = {joborderId}", "").DataTable.AsEnumerable().First();

        var joborderNumber = joborder.Field<int>("JOBORDERNUMBER");
        var joborderDesc = joborder.Field<string>("DESCRIPTION");

        if (projectPhase == null)
        {
            throw new Exception($"Bon {joborderNumber} - {joborderDesc} heeft geen projectfase. Controleer de WBS.");//Alleen doorgaan als er bij deze bon een projectfase hoort
        }

        var projectPhaseId = projectPhase.Field<int?>("PARENTRECORDID") ?? 0;

        if (projectPhaseId == 0)
        {
            throw new Exception($"Niet alle bonnen hebben een projectfase.");//Alleen doorgaan als er bij deze bon een projectfase hoort
        }

        var budgetDetailId = GetRecordset("R_WORKBREAKDOWNINFO", "RECORDID",
            $"PARENTRECORDID = {projectPhaseId} AND FK_TABLEINFO = '{tableBudget}' AND FK_PARENTTABLEINFO = '{tablePhase}' AND FK_MAINPROJECT = {mainprojectId}", "")
            .DataTable.AsEnumerable().FirstOrDefault().Field<int?>("RECORDID") as int? ?? 0;

        if (budgetDetailId == 0)
        {
            throw new Exception($"Niet alle projectfasen hebben een budgetregel.");//Alleen doorgaan als er bij deze projectfase een budgetdetail hoort
        }

        /*var rsBudgetDetail = GetRecordset("R_PROJECTBUDGETDETAIL", "REMAININGCOST, EXPECTEDCOSTPRICE",
            $"PK_R_PROJECTBUDGETDETAIL = {budgetDetailId}", "");
        rsBudgetDetail.MoveFirst();

        var currentRemainingCost = (double)rsBudgetDetail.Fields["REMAININGCOST"].Value;
        var newRemainingCost = currentRemainingCost + amount;

        var currentExpectedCost = (double)rsBudgetDetail.Fields["EXPECTEDCOSTPRICE"].Value;
        var newExpectedCost = currentExpectedCost + amount;

        rsBudgetDetail.UseDataChanges = false;
        rsBudgetDetail.UpdateWhenMoveRecord = false;
        rsBudgetDetail.SetFieldValue("REMAININGCOST", newRemainingCost);
        rsBudgetDetail.SetFieldValue("EXPECTEDCOSTPRICE", newExpectedCost);

        var updateResult = rsBudgetDetail.Update2();
        if (updateResult.Any(x => x.HasError))
        {
            throw new Exception($"Het bijwerken van de te verwachten kostprijs is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");
        }*/

        var rsAdditionalWorkLogs = GetRecordset("U_ADDITIONALWORKLOGS", "", "PK_U_ADDITIONALWORKLOGS = -1", "");
        rsAdditionalWorkLogs.MoveFirst();
        rsAdditionalWorkLogs.UseDataChanges = false;
        rsAdditionalWorkLogs.UpdateWhenMoveRecord = false;

        rsAdditionalWorkLogs.AddNew();
        rsAdditionalWorkLogs.Fields["FK_PROJECTBUDGETDETAIL"].Value = budgetDetailId;
        rsAdditionalWorkLogs.Fields["FK_OFFER"].Value = offerId;
        rsAdditionalWorkLogs.Fields["DATE"].Value = DateTime.Now;
        rsAdditionalWorkLogs.Fields["AMOUNT"].Value = amount;
        rsAdditionalWorkLogs.Fields["TYPE"].Value = 2;//Extern
        rsAdditionalWorkLogs.Fields["DESCRIPTION"].Value = description;

        if(source == "AddWorks")
        {
            rsAdditionalWorkLogs.Fields["FK_ADDITIONALWORKDETAIL"].Value = sourceId;
        }
        if(source == "Markups")
        {
            rsAdditionalWorkLogs.Fields["FK_ADDITIONALWORKMARKUP"].Value = sourceId;
        }

        var updateResult2 = rsAdditionalWorkLogs.Update2();

        if (updateResult2.Any(x => x.HasError))
        {
            throw new Exception($"Het aanmaken van een meerwerk mutatie is mislukt, oorzaak: {updateResult2.First(x => x.HasError).GetResult()}");
        }
    }

    class AddWork
    {
        public int SourceId { get; set; }
        public int? JoborderId { get; set; }
        public double CostPrice { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public bool NoCostpriceNeeded { get; set; }
    }

    class Markup
    {
        public int SourceId { get; set; }
        public int? JoborderId { get; set; }
        public double CostPrice { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public bool NoCostpriceNeeded { get; set; }
    }
}
