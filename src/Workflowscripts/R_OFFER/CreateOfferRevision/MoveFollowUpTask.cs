﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class MoveFollowUpTask : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //07-08-2018	
        //Verplaats offerte opvolgtaak naar revisie offerte

        var rsCurrentOffer = GetRecordset("R_OFFER", "FK_RELATION, OFFERNUMBER",
            $"PK_R_OFFER = {RecordId}", "");
        rsCurrentOffer.MoveFirst();

        var rsFollowUpTodos = GetRecordset("R_TODO", "PK_R_TODO, FK_OFFER",
            $"FK_OFFER = {RecordId} AND ISOFFERFOLLOWUPTODO = 1", "");

        if (rsFollowUpTodos.RecordCount == 0)
        {
            return; //Geen opvolgtaak gevonden
        }

        var rsNewOffer = this.GetRecordset("R_OFFER", "PK_R_OFFER",
            string.Format("FK_RELATION = {0} AND OFFERNUMBER = {1}", rsCurrentOffer.Fields["FK_RELATION"].Value, rsCurrentOffer.Fields["OFFERNUMBER"].Value ), "REVISION DESC");
        rsNewOffer.MoveFirst();

        rsFollowUpTodos.MoveFirst();
        rsFollowUpTodos.Fields["FK_OFFER"].Value = rsNewOffer.Fields["PK_R_OFFER"].Value;
        rsFollowUpTodos.Update();
    }
}
