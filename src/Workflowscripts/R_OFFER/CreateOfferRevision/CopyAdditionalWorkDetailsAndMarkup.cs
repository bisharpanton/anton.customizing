﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class CopyAdditionalWorkDetailsAndMarkup : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //SP
        //12-1-2023
        //Kopieer de meerwerk details en opslagen naar de nieuwe revisie

        var projectModuleActive = GetRecordset("R_CRMSETTINGS", "PROJECTMODULEACTIVE",
            "", "").DataTable.AsEnumerable().First().Field<bool>("PROJECTMODULEACTIVE");

        if (!projectModuleActive)
        {
            return;//Alleen van toepassing indien vinkje 'Projectenmodule actief' aanstaat
        }

        var mainprojectId = GetRecordset("R_OFFER", "FK_MAINPROJECT",
            $"PK_R_OFFER = {id}", "").DataTable.AsEnumerable().FirstOrDefault().Field<int?>("FK_MAINPROJECT") as int? ?? 0;

        if (mainprojectId == 0)
        {
            return;//Alleen doorgaan indien project aanwezig
        }

        var rsOffer = GetRecordset("R_OFFER", "OFFERNUMBER, FK_RELATION", string.Format("PK_R_OFFER = {0}", id), "");
        rsOffer.MoveFirst();
        var offerNumber = rsOffer.Fields["OFFERNUMBER"].Value;

        //Dit stuk gebruiken in de workflow maak offerterevisie
        var rsOfferRevision = GetRecordset("R_OFFER", "PK_R_OFFER", string.Format("OFFERNUMBER = {0}", offerNumber), "REVISION DESC");
        rsOfferRevision.MoveFirst();
        var newOfferId = (int)rsOfferRevision.Fields["PK_R_OFFER"].Value;
        //

        //Dit stuk gebruiken in de workflow kopieer offerte
        /*var relation = rsOffer.Fields["FK_RELATION"].Value;

        var rsNewOffer = GetRecordset("R_OFFER", "PK_R_OFFER",
            string.Format("FK_RELATION = {0} AND CREATOR = '{1}'", relation, CurrentUser.UserName), "DATECREATED DESC");
        rsNewOffer.MoveFirst();
        var newOfferId = (int)rsNewOffer.Fields["PK_R_OFFER"].Value;*/
        //

        var offerDetails = GetRecordset("R_OFFERDETAILMISC", "", string.Format("FK_OFFER = {0}", id), "")
            .DataTable.AsEnumerable();

        var newOfferDetails = GetRecordset("R_OFFERDETAILMISC", "", string.Format("FK_OFFER = {0}", newOfferId), "")
            .DataTable.AsEnumerable();

        foreach (var offerDetail in offerDetails)
        {
            var newOfferDetailId = newOfferDetails.Where(x => x.Field<string>("DESCRIPTION") == offerDetail.Field<string>("DESCRIPTION")).First().Field<int>("PK_R_OFFERDETAILMISC");

            //Kopieer de meerwerk details
            var additionalWorkDetails = GetRecordset("U_ADDITIONALWORKDETAILS", "",
              $"FK_OFFERDETAILMISC = {offerDetail.Field<int>("PK_R_OFFERDETAILMISC")}", "")
                  .DataTable.AsEnumerable().Select(x => new AddWorkDetail()
                  {
                      SequenceNumber = x.Field<int>("SEQUENCENUMBER"),
                      JoborderId = x.Field<int?>("FK_JOBORDER") ?? 0,
                      UnitId = x.Field<int?>("FK_UNIT") ?? 0,
                      Type = x.Field<int>("TYPE"),
                      AppendixNumber = x.Field<int?>("APPENDIXNUMBER"),
                      Description = x.Field<string>("DESCRIPTION"),
                      Header = x.Field<string>("HEADER"),
                      Memo = x.Field<string>("MEMO"),
                      CostPrice = x.Field<double>("COSTPRICE"),
                      NetSalesPrice = x.Field<double>("NETSALESPRICE"),
                      GrossSalesPrice = x.Field<double>("GROSSSALESPRICE"),
                      Quantity = x.Field<double>("QUANTITY"),
                      NoCostpriceNeeded = x.Field<bool>("NOCOSTPRICENEEDED")

                  }).ToList();

            var rsAdditionalWorkDetail = GetRecordset("U_ADDITIONALWORKDETAILS", "", "PK_U_ADDITIONALWORKDETAILS = -1", "");
            rsAdditionalWorkDetail.MoveFirst();
            rsAdditionalWorkDetail.UseDataChanges = false;
            rsAdditionalWorkDetail.UpdateWhenMoveRecord = false;

            foreach (AddWorkDetail additionalWorkDetail in additionalWorkDetails)
            {
                rsAdditionalWorkDetail.AddNew();
                rsAdditionalWorkDetail.Fields["FK_OFFERDETAILMISC"].Value = newOfferDetailId;
                rsAdditionalWorkDetail.Fields["FK_OFFER"].Value = newOfferId;
                rsAdditionalWorkDetail.Fields["SEQUENCENUMBER"].Value = additionalWorkDetail.SequenceNumber;
                if(additionalWorkDetail.JoborderId != 0)
                {
                    rsAdditionalWorkDetail.Fields["FK_JOBORDER"].Value = additionalWorkDetail.JoborderId;
                }
                if (additionalWorkDetail.UnitId != 0)
                {
                    rsAdditionalWorkDetail.Fields["FK_UNIT"].Value = additionalWorkDetail.UnitId;
                }
                rsAdditionalWorkDetail.Fields["TYPE"].Value = additionalWorkDetail.Type;
                rsAdditionalWorkDetail.Fields["APPENDIXNUMBER"].Value = additionalWorkDetail.AppendixNumber;
                rsAdditionalWorkDetail.Fields["DESCRIPTION"].Value = additionalWorkDetail.Description;
                rsAdditionalWorkDetail.Fields["HEADER"].Value = additionalWorkDetail.Header;
                rsAdditionalWorkDetail.Fields["MEMO"].Value = additionalWorkDetail.Memo;
                rsAdditionalWorkDetail.Fields["COSTPRICE"].Value = additionalWorkDetail.CostPrice;
                rsAdditionalWorkDetail.Fields["NETSALESPRICE"].Value = additionalWorkDetail.NetSalesPrice;
                rsAdditionalWorkDetail.Fields["GROSSSALESPRICE"].Value = additionalWorkDetail.GrossSalesPrice;
                rsAdditionalWorkDetail.Fields["QUANTITY"].Value = additionalWorkDetail.Quantity;
                rsAdditionalWorkDetail.Fields["NOCOSTPRICENEEDED"].Value = additionalWorkDetail.NoCostpriceNeeded;
            }

            var updateResult = rsAdditionalWorkDetail.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                throw new Exception($"Het kopiëren van de meerwerk details is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");
            }

            //Verwijder de meerwerk opslagen die worden ingelezen bij het SaveScript
            DeleteDefaultAddWorkMarkups(newOfferDetailId);

            //Kopieer de meerwerk opslagen
            var additionalWorkMarkups = GetRecordset("U_ADDITIONALWORKMARKUP", "",
              $"FK_OFFERDETAILMISC = {offerDetail.Field<int>("PK_R_OFFERDETAILMISC")}", "")
                  .DataTable.AsEnumerable().Select(x => new AddWorkMarkup()
                  {
                      JoborderId = x.Field<int?>("FK_JOBORDER") ?? 0,
                      SequenceNumber = x.Field<int>("SEQUENCENUMBER"),
                      Description = x.Field<string>("DESCRIPTION"),
                      Percentage = x.Field<double>("PERCENTAGE"),
                      Projectteam = x.Field<bool>("PROJECTTEAM"),
                      NoCostpriceNeeded = x.Field<bool>("NOCOSTPRICENEEDED")

                  }).ToList();

            var rsAdditionalWorkMarkup = GetRecordset("U_ADDITIONALWORKMARKUP", "", "PK_U_ADDITIONALWORKMARKUP = -1", "");
            rsAdditionalWorkMarkup.MoveFirst();
            rsAdditionalWorkMarkup.UseDataChanges = false;
            rsAdditionalWorkMarkup.UpdateWhenMoveRecord = false;

            foreach (AddWorkMarkup additionalWorkMarkup in additionalWorkMarkups)
            {
                rsAdditionalWorkMarkup.AddNew();
                rsAdditionalWorkMarkup.Fields["FK_OFFERDETAILMISC"].Value = newOfferDetailId;
                rsAdditionalWorkMarkup.Fields["FK_OFFER"].Value = newOfferId;
                if (additionalWorkMarkup.JoborderId != 0)
                {
                    rsAdditionalWorkDetail.Fields["FK_JOBORDER"].Value = additionalWorkMarkup.JoborderId;
                }
                rsAdditionalWorkMarkup.Fields["SEQUENCENUMBER"].Value = additionalWorkMarkup.SequenceNumber;
                rsAdditionalWorkMarkup.Fields["DESCRIPTION"].Value = additionalWorkMarkup.Description;
                rsAdditionalWorkMarkup.Fields["PERCENTAGE"].Value = additionalWorkMarkup.Percentage;
                rsAdditionalWorkMarkup.Fields["PROJECTTEAM"].Value = additionalWorkMarkup.Projectteam;
                rsAdditionalWorkMarkup.Fields["NOCOSTPRICENEEDED"].Value = additionalWorkMarkup.NoCostpriceNeeded;
            }

            var updateResult2 = rsAdditionalWorkMarkup.Update2();

            if (updateResult2.Any(x => x.HasError))
            {
                throw new Exception($"Het kopiëren van de meerwerk opslagen is mislukt, oorzaak: {updateResult2.First(x => x.HasError).GetResult()}");
            }
        }

    }

    private void DeleteDefaultAddWorkMarkups(int newOfferDetailId)
    {
        var rsDefaultAddWorkMarkups = GetRecordset("U_ADDITIONALWORKMARKUP", "",
            $"FK_OFFERDETAILMISC = {newOfferDetailId}", "");

        if (rsDefaultAddWorkMarkups.RecordCount == 0)
        {
            return;
        }

        rsDefaultAddWorkMarkups.UseDataChanges = false;
        rsDefaultAddWorkMarkups.UpdateWhenMoveRecord = false;

        rsDefaultAddWorkMarkups.MoveFirst();
        while (!rsDefaultAddWorkMarkups.EOF)
        {
            rsDefaultAddWorkMarkups.Delete();
            rsDefaultAddWorkMarkups.MoveNext();
        }

        rsDefaultAddWorkMarkups.MoveFirst();

        var deleteResult = rsDefaultAddWorkMarkups.Update2();

        if (deleteResult.Any(x => x.HasError))
        {
            throw new Exception($"Verwijderen tekstblokken offerte mislukt, oorzaak: {deleteResult.First(x => x.HasError).GetResult()}");
        }
    }

    class AddWorkDetail
    {
        public int SequenceNumber { get; set; }
        public int? JoborderId { get; set; }
        public int? UnitId { get; set; }
        public int Type { get; set; }
        public int? AppendixNumber { get; set; }
        public string Description { get; set; }
        public string Header { get; set; }
        public string Memo { get; set; }
        public double CostPrice { get; set; }
        public double NetSalesPrice { get; set; }
        public double GrossSalesPrice { get; set; }
        public double Quantity { get; set; }
        public bool NoCostpriceNeeded { get; set; }
    }

    class AddWorkMarkup
    {
        public int? JoborderId { get; set; }
        public int SequenceNumber { get; set; }
        public string Description { get; set; }
        public double Percentage { get; set; }
        public double CostPrice { get; set; }
        public bool Projectteam { get; set; }
        public bool NoCostpriceNeeded { get; set; }
    }
}
