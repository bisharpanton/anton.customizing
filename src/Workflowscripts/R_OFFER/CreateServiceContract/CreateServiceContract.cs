﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;
using Ridder.Common.Service;

public class CreateServiceContract : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //21-9-2023
        //Maak Service contract

        var offer = GetRecordset("R_OFFER", "FK_RELATION, FK_CONTACT, DESCRIPTION", $"PK_R_OFFER = {id}", "")
            .DataTable.AsEnumerable().First();

        var rsServiceContract = GetRecordset("R_SERVICECONTRACT", "", "PK_R_SERVICECONTRACT = -1", "");
        rsServiceContract.UseDataChanges = true;

        rsServiceContract.AddNew();
        rsServiceContract.SetFieldValue("FK_RELATION", offer.Field<int>("FK_RELATION"));

        if(offer.Field<int?>("FK_CONTACT").HasValue)
        {
            rsServiceContract.SetFieldValue("FK_CONTACT", offer.Field<int>("FK_CONTACT"));
        }

        rsServiceContract.SetFieldValue("FK_SERVICECONTRACTTYPE", Convert.ToInt32(Parameters["ServiceContractType"].Value));
        rsServiceContract.SetFieldValue("DESCRIPTION", offer.Field<string>("DESCRIPTION"));

        rsServiceContract.SetFieldValue("FK_OFFER", id);

        var updateResult = rsServiceContract.Update2();

        if(updateResult.Any(x => x.HasError))
        {
            Log.Error("Error", $"Aanmaken service contract is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "");
        }

        CreateServiceContractDetails(id, (int)updateResult.First().PrimaryKey);

    }

    private void CreateServiceContractDetails(int offerId, int serviceContractId)
    {
        var offerDetailsWithServiceObject = GetRecordset("R_OFFERDETAILMISC", "FK_SERVICEOBJECT, DESCRIPTION, NETSALESAMOUNT",
            $"FK_OFFER = {offerId} AND FK_SERVICEOBJECT IS NOT NULL", "")
            .DataTable.AsEnumerable().ToList();

        if(!offerDetailsWithServiceObject.Any())
        {
            return;
        }

        var rsServiceContractDetail = GetRecordset("R_SERVICECONTRACTDETAIL", "", "PK_R_SERVICECONTRACTDETAIL = -1", "");
        rsServiceContractDetail.UseDataChanges = true;

        foreach (var offerDetail in offerDetailsWithServiceObject)
        {
            rsServiceContractDetail.AddNew();
            rsServiceContractDetail.SetFieldValue("FK_SERVICECONTRACT", serviceContractId);
            
            rsServiceContractDetail.SetFieldValue("FK_SERVICEOBJECT", offerDetail.Field<int>("FK_SERVICEOBJECT"));
            rsServiceContractDetail.SetFieldValue("DESCRIPTION", offerDetail.Field<string>("DESCRIPTION"));
            rsServiceContractDetail.SetFieldValue("AMOUNT", offerDetail.Field<double>("NETSALESAMOUNT"));
        }

        var updateResult = rsServiceContractDetail.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            Log.Error("Error", $"Aanmaken service contractregels is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "");
        }

    }
}
