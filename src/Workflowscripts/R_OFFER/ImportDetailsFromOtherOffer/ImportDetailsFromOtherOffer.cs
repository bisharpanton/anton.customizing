﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class ImportDetailsFromOtherOffer : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;
        var offerId = Convert.ToInt32(Parameters["Offerte"].Value);

        //SP
        //12-1-2023
        //Kopieer de meerwerk details en opslagen naar de nieuwe revisie

        var offerDetailIds = GetRecordset("R_OFFERDETAILMISC", "", 
            $"FK_OFFER = {offerId}", "").DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_OFFERDETAILMISC")).ToList();

        if(!offerDetailIds.Any())
        {
            return;//Geen regels aanwezig
        }

        foreach (var offerDetailId in offerDetailIds)
        {
            //Kopieer de meerwerk details
            var offerDetailMisc = GetRecordset("R_OFFERDETAILMISC", "",
              $"PK_R_OFFERDETAILMISC = {offerDetailId}", "")
                  .DataTable.AsEnumerable().Select(x => new OfferDetailMisc()
                  {
                      SequenceNumber = x.Field<int>("LINENUMBER"),
                      MiscId = x.Field<int>("FK_MISC"),
                      UnitId = x.Field<int?>("FK_UNIT"),
                      Description = x.Field<string>("DESCRIPTION"),
                      Memo = x.Field<string>("MEMO"),
                      CostPrice = x.Field<double>("COSTPRICE"),
                      NetSalesPrice = x.Field<double>("NETSALESAMOUNT"),
                      GrossSalesPrice = x.Field<double>("GROSSSALESPRICE"),
                      Quantity = x.Field<double>("QUANTITY")

                  }).First();

            var rsOfferDetailMisc = GetRecordset("R_OFFERDETAILMISC", "", "PK_R_OFFERDETAILMISC = -1", "");
            rsOfferDetailMisc.MoveFirst();
            rsOfferDetailMisc.UseDataChanges = true;
            rsOfferDetailMisc.UpdateWhenMoveRecord = false;

            rsOfferDetailMisc.AddNew();
            rsOfferDetailMisc.Fields["FK_OFFER"].Value = id;
            rsOfferDetailMisc.Fields["LINENUMBER"].Value = offerDetailMisc.SequenceNumber;
            rsOfferDetailMisc.Fields["FK_MISC"].Value = offerDetailMisc.MiscId;
            rsOfferDetailMisc.Fields["FK_UNIT"].Value = offerDetailMisc.UnitId;
            rsOfferDetailMisc.Fields["DESCRIPTION"].Value = offerDetailMisc.Description;
            rsOfferDetailMisc.Fields["MEMO"].Value = offerDetailMisc.Memo;
            rsOfferDetailMisc.Fields["COSTPRICE"].Value = offerDetailMisc.CostPrice;
            rsOfferDetailMisc.Fields["NETSALESAMOUNT"].Value = offerDetailMisc.NetSalesPrice;
            rsOfferDetailMisc.Fields["GROSSSALESPRICE"].Value = offerDetailMisc.GrossSalesPrice;
            rsOfferDetailMisc.Fields["QUANTITY"].Value = offerDetailMisc.Quantity;

            var updateResult = rsOfferDetailMisc.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                throw new Exception($"Het kopiëren van de meerwerk details is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");
            }

            var newOfferDetailId = rsOfferDetailMisc.Fields["PK_R_OFFERDETAILMISC"].Value;

            //Kopieer de meerwerk details
            var additionalWorkDetails = GetRecordset("U_ADDITIONALWORKDETAILS", "",
              $"FK_OFFERDETAILMISC = {offerDetailId}", "")
                  .DataTable.AsEnumerable().Select(x => new AddWorkDetail()
                  {
                      SequenceNumber = x.Field<int>("SEQUENCENUMBER"),
                      UnitId = x.Field<int?>("FK_UNIT"),
                      Type = x.Field<int>("TYPE"),
                      AppendixNumber = x.Field<int?>("APPENDIXNUMBER"),
                      Description = x.Field<string>("DESCRIPTION"),
                      Header = x.Field<string>("HEADER"),
                      Memo = x.Field<string>("MEMO"),
                      CostPrice = x.Field<double>("COSTPRICE"),
                      NetSalesPrice = x.Field<double>("NETSALESPRICE"),
                      GrossSalesPrice = x.Field<double>("GROSSSALESPRICE"),
                      Quantity = x.Field<double>("QUANTITY"),
                      NoCostpriceNeeded = x.Field<bool>("NOCOSTPRICENEEDED")

                  }).ToList();

            if(additionalWorkDetails.Any())
            {
                var rsAdditionalWorkDetail = GetRecordset("U_ADDITIONALWORKDETAILS", "", "PK_U_ADDITIONALWORKDETAILS = -1", "");
                rsAdditionalWorkDetail.MoveFirst();
                rsAdditionalWorkDetail.UseDataChanges = false;
                rsAdditionalWorkDetail.UpdateWhenMoveRecord = false;

                foreach (AddWorkDetail additionalWorkDetail in additionalWorkDetails)
                {
                    rsAdditionalWorkDetail.AddNew();
                    rsAdditionalWorkDetail.Fields["FK_OFFERDETAILMISC"].Value = newOfferDetailId;
                    rsAdditionalWorkDetail.Fields["FK_OFFER"].Value = id;
                    rsAdditionalWorkDetail.Fields["SEQUENCENUMBER"].Value = additionalWorkDetail.SequenceNumber;
                    rsAdditionalWorkDetail.Fields["FK_UNIT"].Value = additionalWorkDetail.UnitId;
                    rsAdditionalWorkDetail.Fields["TYPE"].Value = additionalWorkDetail.Type;
                    rsAdditionalWorkDetail.Fields["APPENDIXNUMBER"].Value = additionalWorkDetail.AppendixNumber;
                    rsAdditionalWorkDetail.Fields["DESCRIPTION"].Value = additionalWorkDetail.Description;
                    rsAdditionalWorkDetail.Fields["HEADER"].Value = additionalWorkDetail.Header;
                    rsAdditionalWorkDetail.Fields["MEMO"].Value = additionalWorkDetail.Memo;
                    rsAdditionalWorkDetail.Fields["COSTPRICE"].Value = additionalWorkDetail.CostPrice;
                    rsAdditionalWorkDetail.Fields["NETSALESPRICE"].Value = additionalWorkDetail.NetSalesPrice;
                    rsAdditionalWorkDetail.Fields["GROSSSALESPRICE"].Value = additionalWorkDetail.GrossSalesPrice;
                    rsAdditionalWorkDetail.Fields["QUANTITY"].Value = additionalWorkDetail.Quantity;
                    rsAdditionalWorkDetail.Fields["NOCOSTPRICENEEDED"].Value = additionalWorkDetail.NoCostpriceNeeded;
                }

                var updateResult2 = rsAdditionalWorkDetail.Update2();

                if (updateResult2.Any(x => x.HasError))
                {
                    throw new Exception($"Het kopiëren van de meerwerk details is mislukt, oorzaak: {updateResult2.First(x => x.HasError).GetResult()}");
                }
            }
            
            //Verwijder de meerwerk opslagen die worden ingelezen bij het SaveScript
            DeleteDefaultAddWorkMarkups((int)newOfferDetailId);

            //Kopieer de meerwerk opslagen
            var additionalWorkMarkups = GetRecordset("U_ADDITIONALWORKMARKUP", "",
              $"FK_OFFERDETAILMISC = {offerDetailId}", "")
                  .DataTable.AsEnumerable().Select(x => new AddWorkMarkup()
                  {
                      SequenceNumber = x.Field<int>("SEQUENCENUMBER"),
                      Description = x.Field<string>("DESCRIPTION"),
                      Percentage = x.Field<double>("PERCENTAGE"),
                      Projectteam = x.Field<bool>("PROJECTTEAM"),
                      NoCostpriceNeeded = x.Field<bool>("NOCOSTPRICENEEDED")

                  }).ToList();

            if (additionalWorkMarkups.Any())
            {
                var rsAdditionalWorkMarkup = GetRecordset("U_ADDITIONALWORKMARKUP", "", "PK_U_ADDITIONALWORKMARKUP = -1", "");
                rsAdditionalWorkMarkup.MoveFirst();
                rsAdditionalWorkMarkup.UseDataChanges = false;
                rsAdditionalWorkMarkup.UpdateWhenMoveRecord = false;

                foreach (AddWorkMarkup additionalWorkMarkup in additionalWorkMarkups)
                {
                    rsAdditionalWorkMarkup.AddNew();
                    rsAdditionalWorkMarkup.Fields["FK_OFFERDETAILMISC"].Value = newOfferDetailId;
                    rsAdditionalWorkMarkup.Fields["FK_OFFER"].Value = id;
                    rsAdditionalWorkMarkup.Fields["SEQUENCENUMBER"].Value = additionalWorkMarkup.SequenceNumber;
                    rsAdditionalWorkMarkup.Fields["DESCRIPTION"].Value = additionalWorkMarkup.Description;
                    rsAdditionalWorkMarkup.Fields["PERCENTAGE"].Value = additionalWorkMarkup.Percentage;
                    rsAdditionalWorkMarkup.Fields["PROJECTTEAM"].Value = additionalWorkMarkup.Projectteam;
                    rsAdditionalWorkMarkup.Fields["NOCOSTPRICENEEDED"].Value = additionalWorkMarkup.NoCostpriceNeeded;
                }

                var updateResult3 = rsAdditionalWorkMarkup.Update2();

                if (updateResult3.Any(x => x.HasError))
                {
                    throw new Exception($"Het kopiëren van de meerwerk opslagen is mislukt, oorzaak: {updateResult3.First(x => x.HasError).GetResult()}");
                }
            }
        }
    }

    private void DeleteDefaultAddWorkMarkups(int newOfferDetailId)
    {
        var rsDefaultAddWorkMarkups = GetRecordset("U_ADDITIONALWORKMARKUP", "",
            $"FK_OFFERDETAILMISC = {newOfferDetailId}", "");

        if (rsDefaultAddWorkMarkups.RecordCount == 0)
        {
            return;
        }

        rsDefaultAddWorkMarkups.UseDataChanges = false;
        rsDefaultAddWorkMarkups.UpdateWhenMoveRecord = false;

        rsDefaultAddWorkMarkups.MoveFirst();
        while (!rsDefaultAddWorkMarkups.EOF)
        {
            rsDefaultAddWorkMarkups.Delete();
            rsDefaultAddWorkMarkups.MoveNext();
        }

        rsDefaultAddWorkMarkups.MoveFirst();

        var deleteResult = rsDefaultAddWorkMarkups.Update2();

        if (deleteResult.Any(x => x.HasError))
        {
            throw new Exception($"Verwijderen standaard meerwerk opslagen mislukt, oorzaak: {deleteResult.First(x => x.HasError).GetResult()}");
        }
    }

    class AddWorkDetail
    {
        public int SequenceNumber { get; set; }
        public int? UnitId { get; set; }
        public int Type { get; set; }
        public int? AppendixNumber { get; set; }
        public string Description { get; set; }
        public string Header { get; set; }
        public string Memo { get; set; }
        public double CostPrice { get; set; }
        public double NetSalesPrice { get; set; }
        public double GrossSalesPrice { get; set; }
        public double Quantity { get; set; }
        public bool NoCostpriceNeeded { get; set; }
    }

    class AddWorkMarkup
    {
        public int SequenceNumber { get; set; }
        public string Description { get; set; }
        public double Percentage { get; set; }
        public double CostPrice { get; set; }
        public bool Projectteam { get; set; }
        public bool NoCostpriceNeeded { get; set; }
    }

    class OfferDetailMisc
    {
        public int SequenceNumber { get; set; }
        public int? MiscId { get; set; }
        public int? UnitId { get; set; }
        public string Description { get; set; }
        public string Memo { get; set; }
        public double CostPrice { get; set; }
        public double NetSalesPrice { get; set; }
        public double GrossSalesPrice { get; set; }
        public double Quantity { get; set; }
    }
}

