﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class ResetExpectedOrderDate : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //14-05-2019
        //Herbereken verwachte orderdatum

        var relation = GetRecordset("R_OFFER", "FK_RELATION", $"PK_R_OFFER = {id}", "")
            .DataTable.AsEnumerable().First().Field<int>("FK_RELATION");

        var rsNewOffer = GetRecordset("R_OFFER", "DATECREATED, VERWACHTEORDERDATUMJAAR, VERWACHTEORDERDATUMKWARTAAL",
            $"FK_RELATION = {relation}", "DATECREATED DESC");
        rsNewOffer.MoveFirst();

        var datecreated = (DateTime)rsNewOffer.Fields["DATECREATED"].Value;

        if(!(datecreated.AddMinutes(1) > DateTime.Now))
        {
            return; //Als de gevonden offerte ouder is dan 1 minuut dan klopt er iets niet
        }

        var leadtime = GetLeadTimeFromCrmSettings();
        var expectedOrderDate = DateTime.Now.AddMonths(Convert.ToInt32(Math.Round(leadtime, 0)));

        rsNewOffer.Fields["VERWACHTEORDERDATUMKWARTAAL"].Value = DetermineQuarter(expectedOrderDate);
        rsNewOffer.Fields["VERWACHTEORDERDATUMJAAR"].Value = expectedOrderDate;
        rsNewOffer.Update();
    }

    private int DetermineQuarter(DateTime expectedOrderDate)
    {
        if (expectedOrderDate.Month >= 1 && expectedOrderDate.Month <= 3)
        {
            return 2; //Choice 2 = kwartaal 1
        }
        else if (expectedOrderDate.Month >= 4 && expectedOrderDate.Month <= 6)
        {
            return 3;
        }
        else if (expectedOrderDate.Month >= 7 && expectedOrderDate.Month <= 9)
        {
            return 4;
        }
        else if (expectedOrderDate.Month >= 10 && expectedOrderDate.Month <= 12)
        {
            return 5;
        }

        return 1;
    }

    private double GetLeadTimeFromCrmSettings()
    {
        return GetRecordset("R_CRMSETTINGS", "LEADTIMEOFFEREXPECTEDORDERDATE", "", "")
            .DataTable.AsEnumerable().First().Field<double>("LEADTIMEOFFEREXPECTEDORDERDATE");
    }
}
