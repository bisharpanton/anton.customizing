﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;
using Ridder.Common.ItemManagement;

public class CheckInvoiceSchedule4 : WorkflowScriptInfo
{
    public Boolean Execute()
    {
        var id = (int)RecordId;

        var rsOfferte = GetRecordset("R_OFFER", "FK_INVOICESCHEDULE, ORDERTYPE", $"PK_R_OFFER = {id}", "");
        rsOfferte.MoveFirst();

        // HVE
        // 22-11-2018
        if ((OrderType)rsOfferte.Fields["ORDERTYPE"].Value != OrderType.FixedPrice)
        {
            return true;
        }

        return (rsOfferte.Fields["FK_INVOICESCHEDULE"].Value as int?).HasValue;
    }
}
