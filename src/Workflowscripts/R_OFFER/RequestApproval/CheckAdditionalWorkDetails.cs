﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class CheckAdditionalWorkDetails : WorkflowScriptInfo
{
    public void Execute()
    {
        //SP
        //6-2-2025
        //Bij het uitbrengen van een offerte wordt er meerwerk doorgeschoten. Hier worden allerlei controles gedaan.
        //De controles zijn ook gewenst bij het aanvragen van de fiat, dus het controle stukje van het script is overgenomen
        //en geplaatst bij de workflow voor het aanvragen van de fiat.

        var projectModuleActive = GetRecordset("R_CRMSETTINGS", "PROJECTMODULEACTIVE",
            "", "").DataTable.AsEnumerable().First().Field<bool>("PROJECTMODULEACTIVE");

        if (!projectModuleActive)
        {
            return;//Alleen van toepassing indien vinkje 'Projectenmodule actief' aanstaat
        }

        var id = (int)RecordId;

        var mainprojectId = GetRecordset("R_OFFER", "FK_MAINPROJECT",
            $"PK_R_OFFER = {id}", "").DataTable.AsEnumerable().FirstOrDefault().Field<int?>("FK_MAINPROJECT") as int? ?? 0;

        if (mainprojectId == 0)
        {
            return;//Geen project gekoppeld
        }

        var offerdetailMiscIds = GetRecordset("R_OFFERDETAILMISC", "PK_R_OFFERDETAILMISC",
            $"FK_OFFER = {id}", "").DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_OFFERDETAILMISC")).ToList();

        if (!offerdetailMiscIds.Any())
        {
            return;//Geen offerteregels divers
        }

        foreach (var offerdetailMiscId in offerdetailMiscIds)
        {
            //Doorvoeren van de meerwerk details naar de gekoppelde bonnen
            var additionalWorks = GetRecordset("U_ADDITIONALWORKDETAILS", "PK_U_ADDITIONALWORKDETAILS, FK_JOBORDER, DESCRIPTION, COSTPRICE, NOCOSTPRICENEEDED",
               $"FK_OFFERDETAILMISC = {offerdetailMiscId}", "SEQUENCENUMBER ASC")
           .DataTable.AsEnumerable().Select(x => new AddWork()
           {
               SourceId = x.Field<int>("PK_U_ADDITIONALWORKDETAILS"),
               JoborderId = x.Field<int?>("FK_JOBORDER") ?? 0,
               CostPrice = x.Field<double>("COSTPRICE"),
               Description = x.Field<string>("DESCRIPTION"),
               NoCostpriceNeeded = x.Field<bool>("NOCOSTPRICENEEDED"),
               Source = "AddWorks"

           }).ToList();

            if (!additionalWorks.Any())
            {
                return;
            }

            foreach (AddWork additionalWork in additionalWorks)
            {
                if (additionalWork.JoborderId == 0 && additionalWork.NoCostpriceNeeded == false)
                {
                    throw new Exception("Er zijn meerwerk details zonder gekoppelde bon. Alle regels behalve de regels met het vinkje 'Geen kostprijs benodigd' hebben een bon nodig.");
                }

                if (additionalWork.CostPrice == 0.0 && additionalWork.NoCostpriceNeeded == false)
                {
                    throw new Exception("Er zijn meerwerk details met kostprijs 0. Alle regels behalve de regels met het vinkje 'Geen kostprijs benodigd' hebben een kostprijs nodig.");
                }

            }

            //Doervoeren van de opslagen naar de gekoppelde bonnen
            var additionalWorkMarkups = GetRecordset("U_ADDITIONALWORKMARKUP", "PK_U_ADDITIONALWORKMARKUP, FK_JOBORDER, DESCRIPTION, COSTPRICE, NOCOSTPRICENEEDED",
               $"FK_OFFERDETAILMISC = {offerdetailMiscId}", "SEQUENCENUMBER ASC")
           .DataTable.AsEnumerable().Select(x => new Markup()
           {
               SourceId = x.Field<int>("PK_U_ADDITIONALWORKMARKUP"),
               JoborderId = x.Field<int?>("FK_JOBORDER") ?? 0,
               CostPrice = x.Field<double>("COSTPRICE"),
               Description = "Opslag " + x.Field<string>("DESCRIPTION"),
               NoCostpriceNeeded = x.Field<bool>("NOCOSTPRICENEEDED"),
               Source = "Markups"

           }).ToList();

            if (!additionalWorkMarkups.Any())
            {
                return;
            }

            foreach (Markup additionalWorkMarkup in additionalWorkMarkups)
            {
                if (additionalWorkMarkup.JoborderId == 0 && additionalWorkMarkup.NoCostpriceNeeded == false)
                {
                    throw new Exception("Er zijn meerwerk opslagen regels zonder gekoppelde bon. Alle regels behalve de regels met het vinkje 'Geen kostprijs benodigd' hebben een bon nodig.");
                }

                if (additionalWorkMarkup.CostPrice == 0.0 && additionalWorkMarkup.NoCostpriceNeeded == false)
                {
                    throw new Exception("Er zijn meerwerk opslagen regels met kostprijs 0. Alle regels behalve de regels met het vinkje 'Geen kostprijs benodigd' hebben een kostprijs nodig.");
                }

            }

        }

    }

    class AddWork
    {
        public int SourceId { get; set; }
        public int? JoborderId { get; set; }
        public double CostPrice { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public bool NoCostpriceNeeded { get; set; }
    }

    class Markup
    {
        public int SourceId { get; set; }
        public int? JoborderId { get; set; }
        public double CostPrice { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public bool NoCostpriceNeeded { get; set; }
    }
}
