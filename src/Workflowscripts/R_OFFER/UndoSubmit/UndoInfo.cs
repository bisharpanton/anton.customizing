﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class UndoInfo : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //16-04-2019
        //Sla gegevens op

        var rsOffer = GetRecordset("R_OFFER", "DATEREQUESTAPPROVAL, DATEAPPROVAL, FK_APPROVER",
            $"PK_R_OFFER = {id}", "");
        rsOffer.MoveFirst();
        rsOffer.Fields["DATEREQUESTAPPROVAL"].Value = DBNull.Value;
        rsOffer.Fields["DATEAPPROVAL"].Value = DBNull.Value;
        rsOffer.Fields["FK_APPROVER"].Value = DBNull.Value;
        rsOffer.Update();

    }
}
