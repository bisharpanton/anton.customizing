﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class CreateTodo2 : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //07-08-2018
        //Creëer taak bij uitbrengen offerte

        var rsCRM = GetRecordset("R_CRMSETTINGS", "CREATETODOATOFFER, FOLLOWUPTIME, FK_TODOTYPEFOLLOWUPOFFER", "", "");
        rsCRM.MoveFirst();

        if (!(bool)rsCRM.Fields["CREATETODOATOFFER"].Value)
        {
            return; //Er hoeven geen taken gecreëerd te worden
        }

        if (!(rsCRM.Fields["FK_TODOTYPEFOLLOWUPOFFER"].Value as int?).HasValue)
        {
            return; //Geen taaktype gevonden
        }

        var offerId = (int)RecordId;
        var rsOffer = GetRecordset("R_OFFER",
            "PK_R_OFFER, OFFERNUMBER, REVISION, FK_RELATION, FK_CONTACT, FK_SALESPERSON",
            $"PK_R_OFFER = {offerId}", "");
        rsOffer.MoveFirst();

        var rsTodo = GetRecordset("R_TODO", "",
            $"FK_OFFER = {offerId} AND ISOFFERFOLLOWUPTODO = 1", "");

        if (rsTodo.RecordCount > 0)
        {
            return; //Opvolgtaak bestaat al bij deze offerte
        }

        rsTodo.UseDataChanges = true;
        rsTodo.AddNew();
        rsTodo.Fields["FK_OFFER"].Value = offerId;
        rsTodo.Fields["FK_TODOTYPE"].Value = rsCRM.Fields["FK_TODOTYPEFOLLOWUPOFFER"].Value;

        var tekstRevisie = ((int)rsOffer.Fields["REVISION"].Value == 0) ? "" : $"Rev. {rsOffer.Fields["REVISION"].Value}";
        rsTodo.Fields["DESCRIPTION"].Value = $"Opvolging offerte {rsOffer.Fields["OFFERNUMBER"].Value} {tekstRevisie}";

        rsTodo.Fields["FK_RELATION"].Value = rsOffer.Fields["FK_RELATION"].Value;
        rsTodo.Fields["FK_CONTACT"].Value = rsOffer.Fields["FK_CONTACT"].Value;
        rsTodo.Fields["FK_ASSIGNEDTO"].Value = rsOffer.Fields["FK_SALESPERSON"].Value;
        rsTodo.Fields["ISOFFERFOLLOWUPTODO"].Value = 1;


        var numberOfDays = new TimeSpan((long)rsCRM.Fields["FOLLOWUPTIME"].Value).TotalDays;
        var numberOfWeeks = Math.Floor(numberOfDays / 5);
        var numberOfDaysRemain = numberOfDays % 5;

        var weekendCorrectionNumberOfWeeks = numberOfWeeks * 2;
        var weekendCorrectionCauseStartdate =
            DetermineWeekendCorrectionCauseStartdate(DateTime.Now, numberOfDaysRemain);

        rsTodo.Fields["DUEDATE"].Value =
            DateTime.Now.AddDays(numberOfDays + weekendCorrectionNumberOfWeeks + weekendCorrectionCauseStartdate);
        rsTodo.Update();

    }

    private double DetermineWeekendCorrectionCauseStartdate(DateTime startdate, double numberOfDaysRemain)
    {
        //The remaining days are always less than 5
        var day = startdate.DayOfWeek;

        if (day == DayOfWeek.Saturday)
        {
            return 2;
        }
        else if (day == DayOfWeek.Sunday)
        {
            return 1;
        }
        else if (day == DayOfWeek.Monday || day == DayOfWeek.Tuesday)
        {
            return 0;
        }
        else if (day == DayOfWeek.Wednesday && numberOfDaysRemain >= 4)
        {
            return 2;
        }
        else if (day == DayOfWeek.Thursday && numberOfDaysRemain >= 3)
        {
            return 2;
        }
        else if (day == DayOfWeek.Friday && numberOfDaysRemain >= 2)
        {
            return 2;
        }

        return 0;
    }
}
