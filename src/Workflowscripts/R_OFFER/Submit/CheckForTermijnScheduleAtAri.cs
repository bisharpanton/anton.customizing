﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;
using Ridder.Common.ItemManagement;

public class CheckForTermijnScheduleAtAri : WorkflowScriptInfo
{
    public Boolean Execute()
    {
        var id = (int)RecordId;

        //DB
        //24-2-2023

        if (!GetUserInfo().CompanyName.Equals("Anton Rail & Infra"))
        {
            return true; 
        }

        var offer = GetRecordset("R_OFFER", "FK_MAINPROJECT", $"PK_R_OFFER = {id}", "")
            .DataTable.AsEnumerable().First();

        if (!offer.Field<int?>("FK_MAINPROJECT").HasValue)
        {
            return true;
        }

        var numberOfProjectOfferSalesInstallments = GetRecordset("U_PROJECTOFFERSALESINSTALLMENT", "PK_U_PROJECTOFFERSALESINSTALLMENT",
            $"FK_OFFER = {id}", "").RecordCount;

        return numberOfProjectOfferSalesInstallments > 0;
    }
}
