﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;
using System.IO;
using Ridder.Client.SDK.SDKParameters;
using Ridder.Common.Script;

public class ArchiveOfferDocument2 : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //1-4-2020
        //Bij het uitbrengen van een taak wordt er nabel taak aangemaakt. Die taak komt in de Appreo App. 
        //In die appreo app willen we het offerte document kunnen oproepen.

        var followUpTodoId = GetFollowUpTodoId(id);
        if (followUpTodoId == 0)
        {
            return; //Geen opvolg taak aanwezig
        }

        var archiveLocation = ArchiveOfferReport(id);

        if (archiveLocation == "")
        {
            return;
        }

        CreateDocumentAtFollowUpTodo(id, followUpTodoId, archiveLocation);

    }

    private void CreateDocumentAtFollowUpTodo(int offerId, int followUpTodoId, string archiveLocation)
    {
        var rsTodoDocument = GetRecordset("R_TODODOCUMENT", "",
            $"FK_TODO = {followUpTodoId}", "");

        if (rsTodoDocument.RecordCount > 0)
        {
            var documentIds = rsTodoDocument.DataTable.AsEnumerable().Select(x => x.Field<int>("FK_DOCUMENT")).ToList();

            if (CheckIfOfferDocumentAlreadyLinkedAndThenRefresh(documentIds, archiveLocation))
            {
                return;
            }
        }

        var result = EventsAndActions.CRM.Actions.AddDocument(archiveLocation, "0", true,
            "R_TODO", followUpTodoId, Path.GetFileNameWithoutExtension(archiveLocation));

        if(result.HasError)
        {
            Log.Error(GetRecordTag("R_OFFER", offerId),
                $"Toevoegen offerterapport bij opvolgtaak mislukt, oorzaak: {result.GetResult()}", "");
            return;
        }
    }

    private bool CheckIfOfferDocumentAlreadyLinkedAndThenRefresh(List<int> documentIds, string archiveLocation)
    {
        var rsDocument = GetRecordset("R_DOCUMENT", "PK_R_DOCUMENT, DUMMYREFRESHDATECHANGED",
            $"PK_R_DOCUMENT IN ({string.Join(",", documentIds)}) AND DOCUMENTLOCATION = '{archiveLocation}'", "");

        if (rsDocument.RecordCount == 0)
        {
            return false;
        }

        rsDocument.MoveFirst();
        rsDocument.Fields["DUMMYREFRESHDATECHANGED"].Value = !(bool)rsDocument.Fields["DUMMYREFRESHDATECHANGED"].Value;
        rsDocument.Update();

        return true;
    }

    private string ArchiveOfferReport(int offerId)
    {
        //Get file path from mail settings
        var tableName = "R_OFFER";
        var tableInfoOffer = new Guid("801dce67-0043-48b9-8a0b-edf5b73c37ca");

        var rsMailReportSettings = GetRecordset("R_MAILREPORTSETTING", "PK_R_MAILREPORTSETTING, FILELOCATION, FILENAME", 
            $"FK_TABLEINFO = '{tableInfoOffer}' AND [DEFAULT] = 1", null);

        if (rsMailReportSettings.RecordCount == 0)
        {
            return "";
        }

        rsMailReportSettings.MoveFirst();

        var fileLocation = rsMailReportSettings.GetField("FILELOCATION").Value as byte[];
        var fileName = rsMailReportSettings.GetField("FILENAME").Value as byte[];

        if (fileLocation == null || fileName == null)
        {
            Log.Info(GetRecordTag("R_OFFER", offerId),
                "Offerterapport kan niet gearchiveerd worden. Check de mailinstellingen.", "");
            return "";
        }

        var fileLocationCalc = GetCalculatedColumnOutput(fileLocation, tableName, offerId) as string;
        var fileNameCalc = GetCalculatedColumnOutput(fileName, tableName, offerId) as string;
        var filePathCalc = Path.Combine(new string[] {fileLocationCalc, fileNameCalc});

        if (!Directory.Exists(fileLocationCalc))
        {
            Directory.CreateDirectory(fileLocationCalc);
        }

        var systemReportOffer = new Guid("4f62734f-5159-4410-b2f7-bf6fdea61e73");
        var columnReport = new Guid("155619d1-54fc-4d4a-b961-6d3664cd3f9c");

        var result = ExportReport(offerId, "R_OFFER", DesignerScope.System, systemReportOffer, columnReport,
            filePathCalc, ExportType.Pdf, false);

        if (result.HasError)
        {
            Log.Info(GetRecordTag("R_OFFER", offerId),
                $"Archiveren offerterapport mislukt; oorzaak:{result.GetResult()}", "");
            return "";
        }

        return filePathCalc;
    }

    private int GetFollowUpTodoId(int offerId)
    {
        var rsFollowUpTodo = GetRecordset("R_TODO", "PK_R_TODO",
            $"FK_OFFER = {offerId} AND ISOFFERFOLLOWUPTODO = 1", "");
        
        if(rsFollowUpTodo.RecordCount == 0)
        {
            return 0;
        }

        return rsFollowUpTodo.DataTable.AsEnumerable().First().Field<int>("PK_R_TODO");
    }
}
