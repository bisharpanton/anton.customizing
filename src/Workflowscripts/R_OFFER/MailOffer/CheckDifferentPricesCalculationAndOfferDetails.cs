﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class CheckDifferentPricesCalculationAndOfferDetails : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //26-02-2019
        //Check of er offerteregels zijn die afwijken van hun oorspronkelijke calculatieregels

        var offerDetails = GetRecordset("R_OFFERDETAILASSEMBLY", "FK_ASSEMBLY, COSTPRICE, NETSALESAMOUNT, DESCRIPTION",
            $"FK_OFFER = {id}", "").DataTable.AsEnumerable().Select(x => new DetailInfo()
            {
                AssemblyId = x.Field<int>("FK_ASSEMBLY"),
                TotalCostprice = x.Field<double>("COSTPRICE"),
                TotalSalesprice = x.Field<double>("NETSALESAMOUNT"),
                Description = x.Field<string>("DESCRIPTION"),
            });

        if (!offerDetails.Any())
        {
            return;
        }

        var calculationDetails = GetCalculationDetails(id);

        if (calculationDetails == null)
        {
            return;
        }

        foreach (var offerDetail in offerDetails)
        {
            var calculationDetail = calculationDetails.FirstOrDefault(x => x.AssemblyId == offerDetail.AssemblyId);

            if (calculationDetail == null)
            {
                continue;
            }

            if (Math.Abs(calculationDetail.TotalCostprice - offerDetail.TotalCostprice) > 0.1 ||
                Math.Abs(calculationDetail.TotalSalesprice - offerDetail.TotalSalesprice) > 0.1)
            {
                throw new Exception(
                    $"Bij stuklijst {GetAssemblyCode(offerDetail.AssemblyId)} wijkt de prijs van de calculatieregel af van de prijs van de offerteregel.");
            }

            if (!string.Equals(calculationDetail.Description, offerDetail.Description))
            {
                throw new Exception(
                    $"Bij stuklijst {GetAssemblyCode(offerDetail.AssemblyId)} wijkt de omschrijving van de calculatieregel af van de omschrijving van de offerteregel.");
            }
        }
    }

    private string GetAssemblyCode(int assemblyId)
    {
        var rsAssembly = GetRecordset("R_ASSEMBLY", "CODE",
            $"PK_R_ASSEMBLY = {assemblyId}", "");
        rsAssembly.MoveFirst();

        return (string)rsAssembly.Fields["CODE"].Value;
    }

    private List<DetailInfo> GetCalculationDetails(int offerId)
    {
        var calculations = GetRecordset("C_CALCULATION", "FK_ASSEMBLY, FK_CALCULATIONTOTAL, DESCRIPTION",
            $"FK_OFFER = {offerId} AND FK_ASSEMBLY IS NOT NULL AND FK_CALCULATIONTOTAL IS NOT NULL", "").DataTable.AsEnumerable();

        if (!calculations.Any())
        {
            return null;
        }

        var calculationTotals = GetRecordset("C_CALCULATIONTOTALS",
            "PK_C_CALCULATIONTOTALS, TOTALCOSTPRICE, TOTALSALESPRICE",
            $"PK_C_CALCULATIONTOTALS IN ({string.Join(",", calculations.Select(x => x.Field<int>("FK_CALCULATIONTOTAL")))})",
            "").DataTable.AsEnumerable();

        return (from calculation in calculations
                join calculationTotal in calculationTotals on calculation.Field<int>("FK_CALCULATIONTOTAL") equals
                    calculationTotal.Field<int>("PK_C_CALCULATIONTOTALS")
                select new DetailInfo()
                {
                    AssemblyId = calculation.Field<int>("FK_ASSEMBLY"),
                    TotalCostprice = calculationTotal.Field<double>("TOTALCOSTPRICE"),
                    TotalSalesprice = calculationTotal.Field<double>("TOTALSALESPRICE"),
                    Description = calculation.Field<string>("DESCRIPTION"),
                }).ToList();

    }

    class DetailInfo
    {
        public int AssemblyId { get; set; }
        public double TotalCostprice { get; set; }
        public double TotalSalesprice { get; set; }

        public string Description { get; set; }

    }
}
