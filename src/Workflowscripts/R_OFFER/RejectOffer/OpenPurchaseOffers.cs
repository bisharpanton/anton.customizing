﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Formatting;

public class RidderScript : WorkflowScriptInfo
{
    public Boolean Execute()
    {
        var offerId = this.RecordId;

        return GetRecordset("R_PURCHASEOFFER", "", $"FK_OFFER = {offerId}", "")
            .DataTable
            .AsEnumerable()
            .Any();
    }
}
