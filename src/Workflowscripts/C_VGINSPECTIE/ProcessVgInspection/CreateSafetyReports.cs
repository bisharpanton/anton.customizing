﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class CreateSafetyReports : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
		//18-04-2017
		//Genereer veiligheidsmeldingen
		
		//1. Haal alle keuze kolommen op
		Guid pkChoiceType = new Guid("48481a61-5c39-49af-891d-83a1622b76d4");
		
		var rsTableInfo = GetRecordset("M_TABLEINFO", "PK_M_TABLEINFO", "TABLENAME = 'C_VGINSPECTIE'" , "");
		rsTableInfo.MoveFirst();
		var pkTableInfo = (Guid) rsTableInfo.Fields["PK_M_TABLEINFO"].Value;
		
		var rsColumnInfo = GetRecordset("M_COLUMNINFO", "COLUMNNAME, DESCRIPTION",
            $"FK_TABLEINFO = '{pkTableInfo}' AND FK_CHOICETYPE = '{pkChoiceType}'", "");
		rsColumnInfo.MoveFirst();
		
		//2. Haal huidige VG-Inspectie op
		var rsVGInspectie = GetRecordset("C_VGINSPECTIE", "", $"PK_C_VGINSPECTIE = {RecordId}", "");
		rsVGInspectie.MoveFirst();

        var safetyReportsToCreate = new List<SafetyReportsToCreate>();

		//3. Loop alle kolommen langs van het huidige record en controleer op onvoldoende-score
		while(!rsColumnInfo.EOF)
		{
			string columnnameCheck = rsColumnInfo.Fields["COLUMNNAME"].Value.ToString();
			string columnnameMemo = columnnameCheck.Replace('1', '2');
			
			if( (int) rsVGInspectie.Fields[columnnameCheck].Value == 2 ) //Onvoldoende
            {
                safetyReportsToCreate.Add(new SafetyReportsToCreate()
                {
                    Reporter = rsVGInspectie.Fields["FK_INSPECTER"].Value as int? ?? 0,
                    Description = rsColumnInfo.Fields["DESCRIPTION"].Value.ToString(),
                    Memo = rsVGInspectie.Fields[$"PLAINTEXT_{columnnameMemo}"].Value.ToString(),
                });
			}
			
			rsColumnInfo.MoveNext();
		}

        CreateSafetyReportsForThisList(id, safetyReportsToCreate);


    }

    private void CreateSafetyReportsForThisList(int id, List<SafetyReportsToCreate> safetyReportsToCreate)
    {
        if(!safetyReportsToCreate.Any())
        {
            return;
        }

        var sourceSafetyReportId = GetOrCreateSource("Werkplekinspectie");

        var rsVeiligheidsMeldingen = GetRecordset("C_VEILIGHEIDSMELDINGEN", "", "PK_C_VEILIGHEIDSMELDINGEN = -1", "");
        rsVeiligheidsMeldingen.UseDataChanges = true;
        rsVeiligheidsMeldingen.UpdateWhenMoveRecord = false;
        
        foreach (var safetyReport in safetyReportsToCreate)
        {
            rsVeiligheidsMeldingen.AddNew();
            rsVeiligheidsMeldingen.Fields["FK_VGINSPECTIE"].Value = id;
            rsVeiligheidsMeldingen.Fields["FK_BRONMELDINGEN"].Value = sourceSafetyReportId;
            rsVeiligheidsMeldingen.Fields["FK_MELDER"].Value = safetyReport.Reporter == 0 ? (object) DBNull.Value : safetyReport.Reporter;

            rsVeiligheidsMeldingen.Fields["DESCRIPTION"].Value = safetyReport.Memo.Length > 250
                ? safetyReport.Memo.Substring(0, 250)
                : safetyReport.Memo;

            rsVeiligheidsMeldingen.SetFieldValue("MEMO", safetyReport.Memo);
        }

        rsVeiligheidsMeldingen.Update();
    }

    private int GetOrCreateSource(string description)
    {
        var rsSourceSafetyReport = GetRecordset("C_BRONMELDINGEN", "PK_C_BRONMELDINGEN, DESCRIPTION",
            $"DESCRIPTION = '{description}'", "");

        if (rsSourceSafetyReport.RecordCount > 0)
        {
            rsSourceSafetyReport.MoveFirst();
            return (int)rsSourceSafetyReport.GetField("PK_C_BRONMELDINGEN").Value;
        }

        rsSourceSafetyReport.AddNew();
        rsSourceSafetyReport.SetFieldValue("DESCRIPTION", description);
        rsSourceSafetyReport.Update();

        return (int)rsSourceSafetyReport.GetField("PK_C_BRONMELDINGEN").Value;
    }

    class SafetyReportsToCreate
    {
        public int Reporter { get; set; }
        public string Description { get; set; }
        public string Memo { get; set; }
    }
}
