﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using Workflowscripts.C_CREDITSAFEREQUESTS.ImportRelationFromCreditSafe.CreditSafeModels;
using Workflowscripts.C_CREDITSAFEREQUESTS.ImportRelationFromCreditSafe.Models;
using Workflowscripts.C_CREDITSAFEREQUESTS.ImportRelationFromCreditSafe.TokenModels;

public class ImportRelationFromCreditSafe_RidderScript : WorkflowScriptInfo
{
    public Boolean Execute()
    {
        var id = (int)RecordId;

        //DB
        //30-4-2020
        //Importeer relatie vanuit CreditSafe mbv het CreditSafe id

        var currentCreditSafeRequest = GetRecordset("C_CREDITSAFEREQUESTS", "",
            $"PK_C_CREDITSAFEREQUESTS = {id}", "").As<CreditSafeRequest>().First();

        if (string.IsNullOrEmpty(currentCreditSafeRequest.CREDITSAFEID) && string.IsNullOrEmpty(currentCreditSafeRequest.COCNUMBER))
        {
            LogImportMessage(id,
                "Geen CreditSafe id en geen KVK-nummer ingevuld. Importeren relatie niet mogelijk."); 

            return false;
        }

        var checkRelationAlreadyExists = CheckRelationExists("R_RELATION", currentCreditSafeRequest.CREDITSAFEID, 
            currentCreditSafeRequest.COCNUMBER, currentCreditSafeRequest.ISUPDATECREDITSAFEINFO);

        if (checkRelationAlreadyExists.RelationId != 0)
        {
            LogImportMessage(id, checkRelationAlreadyExists.Message);

            return false;
        }

        //Check of relatie bestaat in andere werkmaatschappij
        var checkRelationAlreadyExistsOtherCompany = CheckRelationExists("C_ALLRELATIONS", currentCreditSafeRequest.CREDITSAFEID,
            currentCreditSafeRequest.COCNUMBER, currentCreditSafeRequest.ISUPDATECREDITSAFEINFO);

        var createdRelationId = 0;
        var copiedFromOtherCompany = false;

        if(checkRelationAlreadyExistsOtherCompany.RelationId != 0)
        {
            var relationOtherCompany = GetRecordset("C_ALLRELATIONS", "",
                    $"UNIQUERELATIONINDEX = '{checkRelationAlreadyExistsOtherCompany.UniqueRelationIndex}'", "")
                .As<AllRelation>().First();

            createdRelationId = ImportRelationFromOtherCompany(relationOtherCompany, currentCreditSafeRequest);

            if (createdRelationId == 0)
            {
                return false;
            }

            CreateDebtorAndOrCreditor(createdRelationId, currentCreditSafeRequest);

            LogImportMessage(id,
                $"Gekozen relatie bestaat al in bedrijf {relationOtherCompany.COMPANY}. De relatie is vanuit {relationOtherCompany.COMPANY} geïmporteerd.");
            copiedFromOtherCompany = true;
        }
        else
        {
            RootObjectCompanyReport rootObjectCompanyReport = GetCompanyReport(currentCreditSafeRequest.CREDITSAFEID, id);

            if (rootObjectCompanyReport == null)
            {
                LogImportMessage(id,
                    $"Root object van company report is null. De import is gestopt.");

                return false;
            }

            createdRelationId =
                CreateOrUpdateRelation(rootObjectCompanyReport.Report, currentCreditSafeRequest);

            if (createdRelationId == 0)
            {
                return false;
            }

            CreateDebtorAndOrCreditor(createdRelationId, currentCreditSafeRequest);
        }

        if (createdRelationId == 0)
        {
            return false;
        }

        SaveRelationInCreditSafeRequest(currentCreditSafeRequest.PK_C_CREDITSAFEREQUESTS, createdRelationId, 
            copiedFromOtherCompany, currentCreditSafeRequest.ISUPDATECREDITSAFEINFO);

        return true;
    }

    private void CreateDebtorAndOrCreditor(int createdRelationId, CreditSafeRequest currentCreditSafeRequest)
    {
        if (currentCreditSafeRequest.ISUPDATECREDITSAFEINFO)
        {
            return;
        }

        var chosenRelationType = GetRecordset("R_RELATIONTYPE", "PK_R_RELATIONTYPE, CODE",
                $"PK_R_RELATIONTYPE = {currentCreditSafeRequest.RELATIONTYPEID.Value}", "").As<RelationType>()
            .FirstOrDefault();

        if(chosenRelationType == null)
        {
            return;
        }

        if(chosenRelationType.CODE.Equals("OPDRG"))
        {
            CreateDebtor(createdRelationId, currentCreditSafeRequest.PK_C_CREDITSAFEREQUESTS);
        }
        else if (chosenRelationType.CODE.Equals("LEV"))
        {
            CreateCreditor(createdRelationId, currentCreditSafeRequest.PK_C_CREDITSAFEREQUESTS);
        }
        else if(chosenRelationType.CODE.Equals("OPDRG/LEV"))
        {
            CreateDebtor(createdRelationId, currentCreditSafeRequest.PK_C_CREDITSAFEREQUESTS);
            CreateCreditor(createdRelationId, currentCreditSafeRequest.PK_C_CREDITSAFEREQUESTS);
        }
    }

    private void CreateCreditor(int createdRelationId, int currentCreditSafeRequestId)
    {
        var rsPurchaseDayBook = GetRecordset("R_DAYBOOK", "PK_R_DAYBOOK",
            $"DAYBOOKTYPE = {(int)DaybookType.Purchase}", "");

        if (rsPurchaseDayBook.RecordCount == 0)
        {
            return;
        }

        var purchaseDayBookId = rsPurchaseDayBook.DataTable.AsEnumerable().First()
            .Field<int>("PK_R_DAYBOOK");

        var result = EventsAndActions.CRM.Actions.CreateCreditor(createdRelationId, purchaseDayBookId);

        if(result.HasError)
        {
            LogImportMessage(currentCreditSafeRequestId,
                $"Genereren crediteur is mislukt, oorzaak: {result.GetResult()}");
        }
    }

    private void CreateDebtor(int createdRelationId, int currentCreditSafeRequestId)
    {
        var rsSalesDayBook = GetRecordset("R_DAYBOOK", "PK_R_DAYBOOK",
            $"DAYBOOKTYPE = {(int)DaybookType.Sales}", "");

        if (rsSalesDayBook.RecordCount == 0)
        {
            return;
        }

        var salesDayBookId = rsSalesDayBook.DataTable.AsEnumerable().First()
            .Field<int>("PK_R_DAYBOOK");

        var result =EventsAndActions.CRM.Actions.CreateDebtor(createdRelationId, salesDayBookId);

        if(result.HasError)
        {
            LogImportMessage(currentCreditSafeRequestId,
                $"Genereren debiteur is mislukt, oorzaak: {result.GetResult()}");
        }
    }


    private void LogImportMessage(int creditSafeRequestId, string importMessage)
    {
        var rsCreditSafeRequests = GetRecordset("C_CREDITSAFEREQUESTS", "IMPORTMESSAGE",
            $"PK_C_CREDITSAFEREQUESTS = {creditSafeRequestId}", "");
        rsCreditSafeRequests.MoveFirst();
        rsCreditSafeRequests.SetFieldValue("IMPORTMESSAGE", importMessage);
        rsCreditSafeRequests.Update();
    }

    private void SaveRelationInCreditSafeRequest(int creditSafeRequestId, int createdRelationId,
        bool copiedFromOtherCompany, bool isUpdateCreditSafeInfo)
    {
        if (isUpdateCreditSafeInfo)
        {
            return;
        }

        var rsCreditSafeRequests = GetRecordset("C_CREDITSAFEREQUESTS", 
            "FK_RELATION, COPIEDFROMOTHERCOMPANY",
            $"PK_C_CREDITSAFEREQUESTS = {creditSafeRequestId}", "");
        rsCreditSafeRequests.MoveFirst();
        rsCreditSafeRequests.SetFieldValue("FK_RELATION", createdRelationId);
        rsCreditSafeRequests.SetFieldValue("COPIEDFROMOTHERCOMPANY", copiedFromOtherCompany);
        rsCreditSafeRequests.Update();
    }

    private int CreateOrUpdateRelation(Report report, CreditSafeRequest currentCreditSafeRequest)
    {
        //Soortgelijke code staat bij 'ImportRelationFromOtherCompany'

        var filter = currentCreditSafeRequest.ISUPDATECREDITSAFEINFO
            ? $"PK_R_RELATION = {currentCreditSafeRequest.FK_RELATION.Value}"
            : $"PK_R_RELATION = -1";

        var rsRelation = GetRecordset("R_RELATION", "", filter, "");
        rsRelation.UseDataChanges = true;
        rsRelation.UpdateWhenMoveRecord = false;

        if (!currentCreditSafeRequest.ISUPDATECREDITSAFEINFO)
        {
            rsRelation.AddNew();

            if(report.CompanySummary.BusinessName.Length > 50)
            {
                rsRelation.Fields["NAME"].Value = report.CompanySummary.BusinessName.Substring(0, 50);

                Log.Info("Info", "De naam op CreditSafe is langer dan 50 karakters. De naam is daarom ingekort. \n\nControleer de nieuwe naam.", "");
            }
            else
            {
                rsRelation.Fields["NAME"].Value = report.CompanySummary.BusinessName;
            }

            rsRelation.Fields["COCNUMBER"].Value = report.CompanySummary.CompanyRegistrationNumber;
            rsRelation.Fields["CREDITSAFEID"].Value = currentCreditSafeRequest.CREDITSAFEID;

            if ((currentCreditSafeRequest.RELATIONTYPEID ?? 0) != 0)
            {
                rsRelation.Fields["FK_RELATIONTYPE"].Value = currentCreditSafeRequest.RELATIONTYPEID.Value;
            }

            if ((currentCreditSafeRequest.SECTORID?? 0) != 0)
            {
                rsRelation.Fields["FK_INDUSTRYCUSTOM"].Value = currentCreditSafeRequest.SECTORID.Value;
            }

            if ((currentCreditSafeRequest.SOURCEID ?? 0) != 0)
            {
                rsRelation.Fields["FK_SOURCE"].Value = currentCreditSafeRequest.SOURCEID.Value;
            }

            if (!string.IsNullOrEmpty(report.AdditionalInformation.Misc.RsinNumber))
            {
                rsRelation.Fields["VATNUMBER"].Value = $"NL{report.AdditionalInformation.Misc.RsinNumber}B01";
            }
        }
        else
        {
            rsRelation.MoveFirst();
        }
        

        if (report.ContactInformation?.MainAddress != null)
        {
            if (!currentCreditSafeRequest.ISUPDATECREDITSAFEINFO || 
                NewAddressOnCreditSafe(rsRelation.GetField("FK_VISITINGADDRESS").Value as int? ?? 0, report.ContactInformation.MainAddress))
            {
                var createdAddresId = CreateAddresWithCreditSafeInformation(report.ContactInformation.MainAddress,
                    report.CompanySummary.Country);

                rsRelation.Fields["FK_VISITINGADDRESS"].Value = createdAddresId;

                if (report.CompanySummary.Country != "NL")
                {
                    //Bij Anton wordt de BTW-bedrijfsgroep leeg gemaakt indien het bezoekadres buiten NL is. Zet hem dummy op 'EU'.
                    var vatCompanyGroupEu = GetVatCompanyGroup("EU", currentCreditSafeRequest.PK_C_CREDITSAFEREQUESTS);

                    if (vatCompanyGroupEu == 0)
                    {
                        return 0;
                    }

                    rsRelation.Fields["FK_VATCOMPANYGROUP"].Value = vatCompanyGroupEu;
                }

                rsRelation.Fields["FK_POSTALADDRESS"].Value = createdAddresId;
            }
        }



        rsRelation.Fields["PHONE1"].Value = report.ContactInformation?.MainAddress?.Telephone?.Length > 25
            ? report.ContactInformation.MainAddress.Telephone.Substring(0, 25)
            : report.ContactInformation?.MainAddress?.Telephone ?? string.Empty;

        if(report.ContactInformation.Websites != null && report.ContactInformation.Websites.Any())
        {
            rsRelation.Fields["WEBSITE"].Value = report.ContactInformation.Websites.First();
        }

        if (report.CompanySummary.CreditRating.CreditLimit.Value == "Geen krediet limiet")
        {
            rsRelation.Fields["SALESCREDITLIMIT"].Value = 0.0;
        }
        else
        {
            rsRelation.Fields["SALESCREDITLIMIT"].Value = report.CompanySummary.CreditRating.CreditLimit.Value;
        }

        rsRelation.Fields["SALESCREDITLIMITSCORE"].Value =
            $"{report.CompanySummary.CreditRating.ProviderValue.Value} - {report.CompanySummary.CreditRating.CommonValue}";

        rsRelation.Fields["FK_LEGALFORM"].Value = GetOrCreate("R_LEGALFORM", "DESCRIPTION", 
            report.CompanyIdentification.BasicInformation.LegalForm.Description);

        if (report.OtherInformation?.EmployeesInformation != null && report.OtherInformation.EmployeesInformation.Any())
        {
            rsRelation.Fields["NUMBEROFEMPLOYEES"].Value = report.OtherInformation.EmployeesInformation
                .OrderByDescending(x => x.Year).First().NumberOfEmployees;
        }
        
        rsRelation.Fields["FK_RELATIONFINANCIALSTATE"].Value =
            GetOrCreate("R_RELATIONFINANCIALSTATE", "DESCRIPTION", report.CompanySummary.CompanyStatus.Description);

        if (report.CompanySummary?.MainActivity != null)
        {
            var companyMainActivity = report.CompanySummary.MainActivity;
            var sbiMainActivity = GetOrCreateSbiMainActivity(companyMainActivity.Code, companyMainActivity.Description);

            rsRelation.Fields["FK_SBIMAINACTIVITY"].Value = sbiMainActivity == 0
                ? (object) DBNull.Value
                : sbiMainActivity;
        }

        var parentRelationId = GetOrCreateParentCompanyFromUltimateParentObject(report, currentCreditSafeRequest);
        rsRelation.Fields["FK_PARENTCOMPANY"].Value = parentRelationId == 0
            ? (object)DBNull.Value
            : parentRelationId;
        
        var updateResult = rsRelation.Update2();

        if (updateResult != null && updateResult.Any(x => x.HasError))
        {
            LogImportMessage(currentCreditSafeRequest.PK_C_CREDITSAFEREQUESTS,
                $"Het aanmaken van de nieuwe relatie is mislukt, oorzaak: {updateResult.First(x => x.HasError).Messages.First().Message}");

            return 0;
        }

        return (int)updateResult.First().PrimaryKey;
    }

    private int ImportRelationFromOtherCompany(AllRelation relationOtherCompany, CreditSafeRequest currentCreditSafeRequest)
    {
        //Soortgelijke code staat bij 'CreateRelation'
        var rsRelation = GetRecordset("R_RELATION", "",
            $"PK_R_RELATION = -1", "");
        rsRelation.UseDataChanges = true;
        rsRelation.UpdateWhenMoveRecord = false;

        rsRelation.AddNew();

        var mappingSucceeds = MapRelationObjectFieldsToRecordset(rsRelation, relationOtherCompany, currentCreditSafeRequest);

        if (!mappingSucceeds)
        {
            return 0;
        }

        var parentRelationId = GetOrCreateParentCompanyFromOtherCompany(relationOtherCompany, currentCreditSafeRequest);

        rsRelation.Fields["FK_PARENTCOMPANY"].Value = parentRelationId == 0
            ? (object)DBNull.Value
            : parentRelationId;

        var updateResult = rsRelation.Update2();

        if (updateResult != null && updateResult.Any(x => x.HasError))
        {
            LogImportMessage(currentCreditSafeRequest.PK_C_CREDITSAFEREQUESTS,
                $"Het aanmaken van een nieuwe relatie bij het kopiëren vanuit een ander bedrijf is mislukt, oorzaak: {updateResult.First(x => x.HasError).Messages.First().Message}");

            return 0;
        }

        return (int)updateResult.First().PrimaryKey;
    }

    private bool MapRelationObjectFieldsToRecordset(ScriptRecordset rsRelation, AllRelation relationOtherCompany, CreditSafeRequest creditSafeRequest)
    {
        if(relationOtherCompany.NAME.Length > 50)
        {
            rsRelation.Fields["NAME"].Value = relationOtherCompany.NAME.Substring(0, 50);

            Log.Info("Info", "De naam op CreditSafe is langer dan 50 karakters. De naam is daarom ingekort. Controleer de nieuwe naam.", "");
        }
        else
        {
            rsRelation.Fields["NAME"].Value = relationOtherCompany.NAME;
        }
        
        rsRelation.Fields["COCNUMBER"].Value = relationOtherCompany.COCNUMBER;
        
        rsRelation.Fields["CREDITSAFEID"].Value = relationOtherCompany.CREDITSAFEID;

        if ((creditSafeRequest.RELATIONTYPEID ?? 0) != 0)
        {
            rsRelation.Fields["FK_RELATIONTYPE"].Value = creditSafeRequest.RELATIONTYPEID.Value;
        }

        if ((creditSafeRequest.SECTORID ?? 0) != 0)
        {
            rsRelation.Fields["FK_INDUSTRYCUSTOM"].Value = creditSafeRequest.SECTORID.Value;
        }

        if ((creditSafeRequest.SOURCEID ?? 0) != 0)
        {
            rsRelation.Fields["FK_SOURCE"].Value = creditSafeRequest.SOURCEID.Value;
        }

        rsRelation.Fields["IBAN"].Value = relationOtherCompany.IBAN;
        rsRelation.Fields["BIC"].Value = relationOtherCompany.BIC;
        rsRelation.Fields["VATNUMBER"].Value = relationOtherCompany.VATNUMBER;

        var createdAddress = CreateNewAddress(relationOtherCompany.STREET, relationOtherCompany.HOUSENUMBER2,
            relationOtherCompany.ADDITIONHOUSENUMBER, relationOtherCompany.ZIPCODE, relationOtherCompany.CITY,
            relationOtherCompany.COUNTRYCODE);

        rsRelation.Fields["FK_VISITINGADDRESS"].Value = createdAddress == 0
            ? (object) DBNull.Value
            : createdAddress;

        if (relationOtherCompany.COUNTRYCODE != "NL")
        {
            //Bij Anton wordt de BTW-bedrijfsgroep leeg gemaakt indien het bezoekadres buiten NL is. Zet hem dummy op 'EU'.
            var vatCompanyGroupEu = GetVatCompanyGroup("EU", creditSafeRequest.PK_C_CREDITSAFEREQUESTS);

            if (vatCompanyGroupEu == 0)
            {
                return false;
            }
            rsRelation.Fields["FK_VATCOMPANYGROUP"].Value = vatCompanyGroupEu;
        }

        if (!string.IsNullOrEmpty(relationOtherCompany.POSTADDRESS_COUNTRYCODE))
        {
            var createdAddress2 = CreateNewAddress(relationOtherCompany.POSTADDRESS_STREET,
                relationOtherCompany.POSTADDRESS_HOUSENUMBER2,
                relationOtherCompany.POSTADDRESS_ADDITIONHOUSENUMBER, relationOtherCompany.POSTADDRESS_ZIPCODE,
                relationOtherCompany.POSTADDRESS_CITY, relationOtherCompany.POSTADDRESS_COUNTRYCODE);

            rsRelation.Fields["FK_POSTALADDRESS"].Value = createdAddress2 == 0
                ? (object)DBNull.Value
                : createdAddress2;
        }

        rsRelation.Fields["PHONE1"].Value = relationOtherCompany.PHONE1.Length > 25
            ? relationOtherCompany.PHONE1.Substring(0, 25)
            : relationOtherCompany.PHONE1;

        rsRelation.Fields["EMAIL"].Value = relationOtherCompany.EMAIL;
        rsRelation.Fields["FACTUURMAIL"].Value = relationOtherCompany.FACTUURMAIL;
        rsRelation.Fields["PURCHASEORDERMAIL"].Value = relationOtherCompany.PURCHASEORDERMAIL;
        rsRelation.Fields["CCPURCHASEORDER"].Value = relationOtherCompany.CCPURCHASEORDER;
        rsRelation.Fields["WEBSITE"].Value = relationOtherCompany.WEBSITE;

        rsRelation.Fields["SALESCREDITLIMIT"].Value = relationOtherCompany.SALESCREDITLIMIT;
        rsRelation.Fields["SALESCREDITLIMITSCORE"].Value = relationOtherCompany.SALESCREDITLIMITSCORE;

        if (!string.IsNullOrEmpty(relationOtherCompany.LEGALFORMDESCRIPTION))
        {
            rsRelation.Fields["FK_LEGALFORM"].Value = GetOrCreate("R_LEGALFORM", "DESCRIPTION",
                relationOtherCompany.LEGALFORMDESCRIPTION);
        }

        rsRelation.Fields["NUMBEROFEMPLOYEES"].Value = relationOtherCompany.NUMBEROFEMPLOYEES;

        if (!string.IsNullOrEmpty(relationOtherCompany.RELATIONFINANCIALSTATE))
        {
            rsRelation.Fields["FK_RELATIONFINANCIALSTATE"].Value = GetOrCreate("R_RELATIONFINANCIALSTATE", "DESCRIPTION",
                relationOtherCompany.RELATIONFINANCIALSTATE);
        }

        var sbiMainActivity =
            GetOrCreateSbiMainActivity(relationOtherCompany.SBICODE, relationOtherCompany.SBIDESCRIPTION);
        rsRelation.Fields["FK_SBIMAINACTIVITY"].Value = sbiMainActivity == 0
            ? (object)DBNull.Value
            : sbiMainActivity;

        return true;
    }

    private bool NewAddressOnCreditSafe(int visitingAddressId, Address contactInformationMainAddress)
    {
        if (visitingAddressId == 0)
        {
            return true;
        }

        var addresss = GetRecordset("R_ADDRESS", "STREET, HOUSENUMBER2", $"PK_R_ADDRESS = {visitingAddressId}", "")
            .DataTable.AsEnumerable().First();

        return !addresss.Field<string>("STREET").Equals(contactInformationMainAddress.Street) ||
               !addresss.Field<string>("HOUSENUMBER2").Equals(contactInformationMainAddress.HouseNo);
    }

    private int CreateNewAddress(string street, string houseNumber2, string additionHouseNumber, string zipCode, string city, string countryCode)
    {
        if (string.IsNullOrEmpty(countryCode))
        {
            return 0;
        }

        var rsAddress = GetRecordset("R_ADDRESS", "",
            "PK_R_ADDRESS = -1", "");
        rsAddress.AddNew();
        rsAddress.Fields["STREET"].Value = street;
        rsAddress.Fields["HOUSENUMBER2"].Value = houseNumber2;
        rsAddress.Fields["ADDITIONHOUSENUMBER"].Value = additionHouseNumber;
        rsAddress.Fields["ZIPCODE"].Value = zipCode;
        rsAddress.Fields["CITY"].Value = city;
        rsAddress.Fields["FK_COUNTRY"].Value = GetOrCreateCountry(countryCode);
        rsAddress.Update();

        return (int)rsAddress.Fields["PK_R_ADDRESS"].Value;
    }

    private int GetVatCompanyGroup(string code, int creditSafeRequestId)
    {
        var rsVatCompanyGroup = GetRecordset("R_VATCOMPANYGROUP", "", 
            $"CODE = '{code}'", "");

        if (rsVatCompanyGroup.RecordCount > 0)
        {
            rsVatCompanyGroup.MoveFirst();
            return (int)rsVatCompanyGroup.Fields["PK_R_VATCOMPANYGROUP"].Value;
        }

        LogImportMessage(creditSafeRequestId,
            "Aanmaken relatie mislukt. Relatie ligt buiten NL, maar er is geen BTW-bedrijfsgroep 'EU' gevonden.");

        return 0;
    }

    private object CreateAddresWithCreditSafeInformation(Address address, string countryCode)
    {
        var rsAddress = GetRecordset("R_ADDRESS", "",
            "PK_R_ADDRESS = -1", "");
        rsAddress.AddNew();
        rsAddress.Fields["STREET"].Value = address.Street;
        rsAddress.Fields["HOUSENUMBER2"].Value = address.HouseNo;
        rsAddress.Fields["ZIPCODE"].Value = address.PostCode;
        rsAddress.Fields["CITY"].Value = address.City;
        rsAddress.Fields["FK_COUNTRY"].Value = GetOrCreateCountry(countryCode);
        rsAddress.Update();

        return (int)rsAddress.Fields["PK_R_ADDRESS"].Value;
    }

    private int GetOrCreateCountry(string countryCode)
    {
        var rsCountry = GetRecordset("R_COUNTRY", "", 
            $"CODE = '{countryCode}'", "");

        if (rsCountry.RecordCount > 0)
        {
            rsCountry.MoveFirst();
            return (int)rsCountry.Fields["PK_R_COUNTRY"].Value;
        }
        else
        {
            rsCountry.AddNew();
            rsCountry.Fields["CODE"].Value = countryCode;
            rsCountry.Fields["NAME"].Value = countryCode;
            rsCountry.Update();

            return (int)rsCountry.Fields["PK_R_COUNTRY"].Value;
        }
    }

    private int GetOrCreateParentCompanyFromOtherCompany(AllRelation relationOtherCompany, CreditSafeRequest currentCreditSafeRequest)
    {
        if ((relationOtherCompany.PARENTCOMPANYID ?? 0) == 0)
        {
            return 0;
        }

        var parentRelationOtherCompany = GetRecordset("C_ALLRELATIONS", "",
                $"UNIQUERELATIONINDEX = '{relationOtherCompany.ABBREVIATIONCOMPANY}_{relationOtherCompany.PARENTCOMPANYID.Value}'", "")
            .As<AllRelation>().FirstOrDefault();

        if (parentRelationOtherCompany == null)
        {
            return 0; //Parent company niet gevonden in 'Alle relaties'
        }


        var parentCompanyThisAdministration = CheckRelationExists("R_RELATION", parentRelationOtherCompany.CREDITSAFEID, 
            parentRelationOtherCompany.COCNUMBER, false); 

        if(parentCompanyThisAdministration.RelationId != 0)
        {
            return parentCompanyThisAdministration.RelationId;
        }

        //Relatie bestaat nog niet. Dan aanmaken
        
        var rsRelation = GetRecordset("R_RELATION", "",
            $"PK_R_RELATION = -1", "");
        rsRelation.UseDataChanges = true;
        rsRelation.UpdateWhenMoveRecord = false;

        rsRelation.AddNew();

        var mappingSucceeds = MapRelationObjectFieldsToRecordset(rsRelation, parentRelationOtherCompany, currentCreditSafeRequest);

        if (!mappingSucceeds)
        {
            return 0;
        }

        var updateResult = rsRelation.Update2();

        if (updateResult != null && updateResult.Any(x => x.HasError))
        {
            LogImportMessage(currentCreditSafeRequest.PK_C_CREDITSAFEREQUESTS,
                $"Het aanmaken van een nieuwe moederbedrijf vanuit een ander bedrijf is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");

            return 0;
        }

        return (int)updateResult.First().PrimaryKey;
    }

    private ExistingRelationResult CheckRelationExists(string tableName, string creditSafeId, string cocNumber,
        bool isUpdateCreditSafeInfo)
    {
        if (isUpdateCreditSafeInfo)
        {
            //Check of relatie al bestaat is overbodig want we willen juist de bestaande relatie updaten
            return new ExistingRelationResult(){RelationId = 0, Message = "", UniqueRelationIndex = ""}; 
        }

        var filter = "";
        if(string.IsNullOrEmpty(creditSafeId) && string.IsNullOrEmpty(cocNumber))
        {
            //Geen creditsafe id en geen KvK-nummer. Opzoeken in huidige administratie niet mogelijk
            return new ExistingRelationResult(){RelationId = 0, Message = "", UniqueRelationIndex = ""}; 
        }
        else if(string.IsNullOrEmpty(creditSafeId) && !string.IsNullOrEmpty(cocNumber))
        {
            filter = $"COCNUMBER = '{cocNumber}'";
        }
        else if (!string.IsNullOrEmpty(creditSafeId) && string.IsNullOrEmpty(cocNumber))
        {
            filter = $"CREDITSAFEID = '{creditSafeId}'";
        }
        else
        {
            //Beide gevuld
            filter = $"CREDITSAFEID = '{creditSafeId}' OR COCNUMBER = '{cocNumber}'";
        }

        //Zoek met behulp van CreditSafeId of KVK-nummer of de relatie al aanwezig is in de huidige administratie
        var columns = tableName == "R_RELATION"
            ? "PK_R_RELATION, NAME"
            : "NAME, RELATIONID, UNIQUERELATIONINDEX";

        var rsRelationExists = GetRecordset(tableName, columns,
             filter, "");

        if (rsRelationExists.RecordCount > 0)
        {
            rsRelationExists.MoveFirst();
            return new ExistingRelationResult()
            {
                RelationId = tableName == "R_RELATION"
                    ? (int)rsRelationExists.GetField("PK_R_RELATION").Value
                    : (int)rsRelationExists.GetField("RELATIONID").Value,
                Message = !string.IsNullOrEmpty(cocNumber)
                    ? $"KVK-nummer {cocNumber} komt al voor bij relatie {rsRelationExists.Fields["NAME"].Value}."
                    : $"CreditSafe id {creditSafeId} komt al voor bij relatie {rsRelationExists.Fields["NAME"].Value}.",
                UniqueRelationIndex = tableName == "R_RELATION"
                    ? rsRelationExists.Fields["PK_R_RELATION"].Value.ToString()
                    : rsRelationExists.Fields["UNIQUERELATIONINDEX"].Value.ToString(),
            };
        }

        return new ExistingRelationResult(){RelationId = 0, Message = "", UniqueRelationIndex = ""}; 
    }

    private int GetOrCreateParentCompanyFromUltimateParentObject(Report report, CreditSafeRequest currentCreditSafeRequest)
    {
        if (report.GroupStructure?.UltimateParent == null)
        {
            return 0;
        }

        var ultimateParent = report.GroupStructure.UltimateParent;

        var rsRelationExists = GetRecordset("R_RELATION", "PK_R_RELATION",
            $"CREDITSAFEID = '{ultimateParent.Id}'", "");

        if (rsRelationExists.RecordCount > 0)
        {
            rsRelationExists.MoveFirst();
            return (int)rsRelationExists.GetField("PK_R_RELATION").Value;
        }

        //Relatie bestaat nog niet. Dan aanmaken
        var rsRelation = GetRecordset("R_RELATION", "",
            "PK_R_RELATION = -1", "");
        rsRelation.UseDataChanges = true;
        rsRelation.UpdateWhenMoveRecord = false;

        rsRelation.AddNew();
        rsRelation.Fields["NAME"].Value = ultimateParent.Name.Length > 50
            ? ultimateParent.Name.Substring(0, 50)
            : ultimateParent.Name;

        rsRelation.Fields["CREDITSAFEID"].Value = ultimateParent.Id;

        if(ultimateParent.RegNo != null && ultimateParent.RegNo.Length >= 8)
        {
            rsRelation.Fields["COCNUMBER"].Value = ultimateParent.RegNo.Substring(0, 8);
        }

        if ((currentCreditSafeRequest.RELATIONTYPEID ?? 0) != 0)
        {
            rsRelation.Fields["FK_RELATIONTYPE"].Value = currentCreditSafeRequest.RELATIONTYPEID.Value;
        }

        if ((currentCreditSafeRequest.SECTORID?? 0) != 0)
        {
            rsRelation.Fields["FK_INDUSTRYCUSTOM"].Value = currentCreditSafeRequest.SECTORID.Value;
        }

        if (ultimateParent.Address != null)
        {
            rsRelation.Fields["FK_VISITINGADDRESS"].Value = CreateAddresWithCreditSafeInformation(ultimateParent.Address, ultimateParent.Country);

            if (ultimateParent.Country != "NL")
            {
                //Bij Anton wordt de BTW-bedrijfsgroep leeg gemaakt indien het bezoekadres buiten NL is. Zet hem dummy op 'EU'.
                var vatCompanyGroupEu = GetVatCompanyGroup("EU", currentCreditSafeRequest.PK_C_CREDITSAFEREQUESTS);

                if (vatCompanyGroupEu == 0)
                {
                    return 0;
                }
                rsRelation.Fields["FK_VATCOMPANYGROUP"].Value = vatCompanyGroupEu;
            }
        }
    
        var updateResult = rsRelation.Update2();

        if (updateResult != null && updateResult.Any(x => x.HasError))
        {
            LogImportMessage(currentCreditSafeRequest.PK_C_CREDITSAFEREQUESTS,
                $"Aanmaken moeder bedrijf als nieuwe relatie mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");

            return 0;
        }

        return (int)updateResult.First().PrimaryKey;
    }

    private int GetOrCreate(string tableName, string columnName, string companyDescription)
    {
        var rs = GetRecordset(tableName, "", "", "");

        if (rs.DataTable.AsEnumerable().Any(x => x.Field<string>(columnName).Equals(companyDescription, StringComparison.OrdinalIgnoreCase)))
        {
            return rs.DataTable.AsEnumerable()
                .First(x => x.Field<string>(columnName).Equals(companyDescription, StringComparison.OrdinalIgnoreCase))
                .Field<int>($"PK_{tableName}");
        }
        else
        {
            var newCode = $"{rs.RecordCount + 1:00}"; 
            
            rs.AddNew();
            rs.Fields["CODE"].Value = newCode;
            rs.Fields["DESCRIPTION"].Value = companyDescription;
            rs.Update();

            return (int)rs.Fields[$"PK_{tableName}"].Value;
        }
    }

    private int GetOrCreateSbiMainActivity(string sbiCode, string sbiDescription)
    {
        if (string.IsNullOrEmpty(sbiCode))
        {
            return 0;
        }

        var rsSbiActivities = GetRecordset("C_SBIACTIVITIES", "",
            $"CODE = '{sbiCode}'", "");

        if (rsSbiActivities.RecordCount > 0)
        {
            rsSbiActivities.MoveFirst();
        }
        else
        {
            rsSbiActivities.AddNew();
            rsSbiActivities.Fields["CODE"].Value = sbiCode;
            rsSbiActivities.Fields["DESCRIPTION"].Value = sbiDescription;
            rsSbiActivities.Update();
        }

        return (int)rsSbiActivities.Fields["PK_C_SBIACTIVITIES"].Value;
    }


    private RootObjectCompanyReport GetCompanyReport(string creditSafeId, int creditSafeRequestId)
    {
        var token = new Token(this).GetOrCreateNewToken();

        if(token == null)
        {
            return null;
        }

        //Vraag company report op mbv CreditSafe id 
        var urlPath = 
            new Uri($"https://connect.creditsafe.com/v1/companies/{creditSafeId}?language=NL");

        HttpClient client = new HttpClient();
        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token.Token);

        var response = client.GetAsync(urlPath).GetAwaiter().GetResult();

        if(!response.IsSuccessStatusCode)
        {
            LogImportMessage(creditSafeRequestId, 
                $"Opvragen krediet-rapport bij CreditSafe mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase}.");

            return null;
            //return;
        }

        response.EnsureSuccessStatusCode();

        return response.Content.ReadAsAsync<RootObjectCompanyReport>().Result;
        //var responseEvents = await client.GetStringAsync(urlPath);
        //return;
    }

    class ExistingRelationResult
    {
        public int RelationId { get; set; }
        public string Message { get; set; }
        public string UniqueRelationIndex { get; set; }
    }
}
