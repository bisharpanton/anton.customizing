﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Workflowscripts.C_CREDITSAFEREQUESTS.ImportRelationFromCreditSafe.TokenModels
{
    class TokenResponse
    {
        [JsonProperty("token")]
        public string Token { get; set; }
    }
}
