﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using Ridder.Common.WorkflowModel.Activities;

namespace Workflowscripts.C_CREDITSAFEREQUESTS.ImportRelationFromCreditSafe.TokenModels
{
    class Token
    {
        private const string _connectionString = @"Data Source=SRV-APP03\RIDDERIQ;Initial Catalog=Anton Groep;User Id=sa;Password=Welkom@ridderiq";
        //private const string _connectionString = @"Data Source=LT115\RIDDERIQ;Initial Catalog=Anton Groep;User Id=sa;Password=Welkom@ridderiq";
        public WorkflowScriptInfo _workflowScript { get; set; }
        public string TokenResult { get; set; }

        public Token(WorkflowScriptInfo workflowScript)
        {
            _workflowScript = workflowScript;
        }

        public TokenResponse GetOrCreateNewToken()
        {
            var creditSafeTokenInfo = DetermineCreditSafeTokenInfoFromAntonGroup();

            //Token is 1 uur geldig dus deze is nog bruikbaar
            if (creditSafeTokenInfo.TokenGenerated > DateTime.Now.AddHours(-1))
            {
                return new TokenResponse() {Token = creditSafeTokenInfo.Token};
            }

            //Genereer nieuwe token door verbinding te maken met CreditSafe
            var urlPath = new Uri(@"https://connect.creditsafe.com/v1/authenticate");

            HttpClient client = new HttpClient();
            HttpResponseMessage result = client.PostAsJsonAsync(urlPath, creditSafeTokenInfo.CreditSafeCredentials)
                .GetAwaiter().GetResult();

            if (!result.IsSuccessStatusCode)
            {
                _workflowScript.Log.Error("GetOrCreateNewToken CreditSafe",
                    $"Verbinding maken met CreditSafe is mislukt; oorzaak: {(int)result.StatusCode}:{result.ReasonPhrase}","");
                return null;
            }

            var newToken = result.Content.ReadAsAsync<TokenResponse>().GetAwaiter().GetResult();
            SaveNewTokenInAntonGroup(newToken.Token);

            return newToken;
        }

        private void SaveNewTokenInAntonGroup(string newToken)
        {
            if (_workflowScript.GetUserInfo().CompanyName == "Anton Groep")
            {
                var rsCRM = _workflowScript.GetRecordset("R_CRMSETTINGS",
                    "CREDITSAFEUSERNAME, CREDITSAFEPASSWORD, CREDITSAFETOKEN, CREDITSAFETOKENGENERATED", "", "");
                rsCRM.MoveFirst();
                rsCRM.Fields["CREDITSAFETOKEN"].Value = newToken;
                rsCRM.Fields["CREDITSAFETOKENGENERATED"].Value = DateTime.Now;
                rsCRM.Update();
            }
            else
            {
                using (var conn = new System.Data.SqlClient.SqlConnection(_connectionString))
                {
                    conn.Open();

                    string sqlQuery = @"
				            UPDATE X_R_CRMSETTINGS 
                            SET CREDITSAFETOKEN = @newToken, CREDITSAFETOKENGENERATED = @newDate
				            FROM [Anton Groep].dbo.X_R_CRMSETTINGS;";

                    var sqlCmd = new SqlCommand(sqlQuery, conn);
                    sqlCmd.Parameters.AddWithValue("@newToken", newToken);
                    sqlCmd.Parameters.AddWithValue("@newDate", DateTime.Now);

                    sqlCmd.ExecuteNonQuery();

                    conn.Close();
                }
            }
        }

        private CreditSafeTokenInfo DetermineCreditSafeTokenInfoFromAntonGroup()
        {
            CreditSafeTokenInfo result = new CreditSafeTokenInfo();

            if (_workflowScript.GetUserInfo().CompanyName == "Anton Groep")
            {
                var crmInfo = _workflowScript.GetRecordset("R_CRMSETTINGS",
                        "CREDITSAFEUSERNAME, CREDITSAFEPASSWORD, CREDITSAFETOKEN, CREDITSAFETOKENGENERATED", "", "")
                    .DataTable.AsEnumerable().First();
                result.CreditSafeCredentials = new CreditSafeCredentials()
                {
                    UserName = crmInfo.Field<string>("CREDITSAFEUSERNAME"),
                    Password = crmInfo.Field<string>("CREDITSAFEPASSWORD"),
                };
                result.Token = crmInfo.Field<string>("CREDITSAFETOKEN");
                result.TokenGenerated = !crmInfo.Field<DateTime?>("CREDITSAFETOKENGENERATED").HasValue
                    ? DateTime.MinValue
                    : crmInfo.Field<DateTime>("CREDITSAFETOKENGENERATED");
            }
            else
            {
                using (var conn = new System.Data.SqlClient.SqlConnection(_connectionString))
                {
                    conn.Open();

                    string sqlSelectTemplate = @"
				            SELECT CRM.CREDITSAFEUSERNAME, CRM.CREDITSAFEPASSWORD, CRM.CREDITSAFETOKEN, CRM.CREDITSAFETOKENGENERATED
				            FROM [Anton Groep].dbo.X_R_CRMSETTINGS AS CRM;";

                    System.Data.SqlClient.SqlCommand sqlCmd =
                        new System.Data.SqlClient.SqlCommand(sqlSelectTemplate, conn);

                    System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(sqlCmd);
                    DataTable dtSelectTemplate = new DataTable("Template");
                    da.Fill(dtSelectTemplate);

                    result.CreditSafeCredentials = new CreditSafeCredentials()
                    {
                        UserName = dtSelectTemplate.Rows[0]["CREDITSAFEUSERNAME"].ToString(),
                        Password = dtSelectTemplate.Rows[0]["CREDITSAFEPASSWORD"].ToString(),
                    };
                    result.Token = dtSelectTemplate.Rows[0]["CREDITSAFETOKEN"].ToString();
                    result.TokenGenerated = (DateTime)dtSelectTemplate.Rows[0]["CREDITSAFETOKENGENERATED"];

                    da = null;
                    sqlCmd = null;

                    conn.Close();
                }
            }

            return result;
        }
    }
}
