﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Workflowscripts.C_CREDITSAFEREQUESTS.ImportRelationFromCreditSafe.CreditSafeModels
{
    class AdditionalInformation
    {
        [JsonProperty("limitHistory")]
        public List<LimitHistory> LimitHistories { get; set; }
        [JsonProperty("misc")]
        public MiscAdditionalInformation Misc { get; set; }
    }
}
