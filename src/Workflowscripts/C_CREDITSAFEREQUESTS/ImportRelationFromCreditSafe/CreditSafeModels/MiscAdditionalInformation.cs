﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Workflowscripts.C_CREDITSAFEREQUESTS.ImportRelationFromCreditSafe.CreditSafeModels
{
    class MiscAdditionalInformation
    {
        [JsonProperty("rsinnumber")]
        public string RsinNumber { get; set; }
    }
}
