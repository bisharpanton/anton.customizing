﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Workflowscripts.C_CREDITSAFEREQUESTS.ImportRelationFromCreditSafe.CreditSafeModels
{
    class CompanyIdentification
    {
        [JsonProperty("basicInformation")]
        public BasicInformation BasicInformation { get; set; }
    }
}
