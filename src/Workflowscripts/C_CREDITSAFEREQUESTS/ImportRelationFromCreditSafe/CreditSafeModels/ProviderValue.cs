﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Workflowscripts.C_CREDITSAFEREQUESTS.ImportRelationFromCreditSafe.CreditSafeModels
{
    class ProviderValue
    {
        [JsonProperty("maxValue")]
        public string MaxValue { get; set; }
        [JsonProperty("minValue")]
        public string MinValue { get; set; }
        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
