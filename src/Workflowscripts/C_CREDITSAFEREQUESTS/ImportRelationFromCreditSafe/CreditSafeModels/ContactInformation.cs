﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Workflowscripts.C_CREDITSAFEREQUESTS.ImportRelationFromCreditSafe.CreditSafeModels
{
    class ContactInformation
    {
        [JsonProperty("mainAddress")]
        public Address MainAddress { get; set; }
        [JsonProperty("otherAddresses")]
        public List<Address> OtherAddresses { get; set; }
        [JsonProperty("websites")]
        public List<string> Websites { get; set; }
    }
}
