﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Workflowscripts.C_CREDITSAFEREQUESTS.ImportRelationFromCreditSafe.CreditSafeModels
{
    class Report
    {
        [JsonProperty("companyId")]
        public string CompanyId { get; set; }
        [JsonProperty("language")]
        public string Language { get; set; }
        [JsonProperty("companySummary")]
        public CompanySummary CompanySummary { get; set; }
        [JsonProperty("otherInformation")]
        public OtherInformation OtherInformation { get; set; }
        [JsonProperty("groupStructure")]
        public GroupStructure GroupStructure { get; set; }
        [JsonProperty("additionalInformation")]
        public AdditionalInformation AdditionalInformation { get; set; }
        [JsonProperty("contactInformation")]
        public ContactInformation ContactInformation { get; set; }
        [JsonProperty("companyIdentification")]
        public CompanyIdentification CompanyIdentification { get; set; }
    }
}
