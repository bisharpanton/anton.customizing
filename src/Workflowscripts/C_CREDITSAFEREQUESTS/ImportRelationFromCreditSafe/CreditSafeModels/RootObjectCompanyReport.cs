﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Workflowscripts.C_CREDITSAFEREQUESTS.ImportRelationFromCreditSafe.CreditSafeModels
{
    class RootObjectCompanyReport
    {
        [JsonProperty("orderId")]
        public string OrderId { get; set; }
        [JsonProperty("companyId")]
        public string CompanyId { get; set; }
        [JsonProperty("dateOfOrder")]
        public DateTime DateOfOrder { get; set; }
        [JsonProperty("language")]
        public string Language { get; set; }
        [JsonProperty("userId")]
        public string UserId { get; set; }
        [JsonProperty("chargeRef")]
        public object ChargeRef { get; set; }
        [JsonProperty("report")]
        public Report Report { get; set; }
    }
}
