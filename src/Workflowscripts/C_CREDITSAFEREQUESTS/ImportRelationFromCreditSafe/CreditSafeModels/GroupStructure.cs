﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Workflowscripts.C_CREDITSAFEREQUESTS.ImportRelationFromCreditSafe.CreditSafeModels
{
    class GroupStructure
    {
        [JsonProperty("ultimateParent")]
        public Company UltimateParent { get; set; }
        [JsonProperty("subsidiaryCompanies")]
        public List<Company> SubsidiaryCompanies { get; set; }
        [JsonProperty("affiliatedCompanies")]
        public List<Company> AffiliatedCompanies { get; set; }
    }
}
