﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Workflowscripts.C_CREDITSAFEREQUESTS.ImportRelationFromCreditSafe.CreditSafeModels
{
    class LegalForm
    {
        [JsonProperty("providerCode")]
        public string ProviderCode { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
    }
}
