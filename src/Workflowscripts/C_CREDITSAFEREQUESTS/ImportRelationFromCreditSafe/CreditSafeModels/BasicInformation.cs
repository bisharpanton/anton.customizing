﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Workflowscripts.C_CREDITSAFEREQUESTS.ImportRelationFromCreditSafe.CreditSafeModels
{
    class BasicInformation
    {
        [JsonProperty("businessName")]
        public string BusinessName { get; set; }
        [JsonProperty("registeredCompanyName")]
        public string RegisteredCompanyName { get; set; }
        [JsonProperty("companyRegistrationNumber")]
        public string CompanyRegistrationNumber { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("companyRegistrationDate")]
        public DateTime CompanyRegistrationDate { get; set; }
        [JsonProperty("operationsStartDate")]
        public DateTime OperationsStartDate { get; set; }
        [JsonProperty("legalForm")]
        public LegalForm LegalForm { get; set; }
        [JsonProperty("companyStatus")]
        public CompanyStatus CompanyStatus { get; set; }
        [JsonProperty("contactAddress")]
        public Address ContactAddress { get; set; }
    }
}
