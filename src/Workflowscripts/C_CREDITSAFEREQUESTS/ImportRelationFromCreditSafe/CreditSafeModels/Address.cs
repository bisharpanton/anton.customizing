﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Workflowscripts.C_CREDITSAFEREQUESTS.ImportRelationFromCreditSafe.CreditSafeModels
{
    class Address
    {
        [JsonProperty("simpleValue")]
        public string SimpleValue { get; set; }
        [JsonProperty("street")]
        public string Street { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("postalCode")]
        public string PostCode { get; set; }
        [JsonProperty("houseNumber")]
        public string HouseNo { get; set; }
        [JsonProperty("province")]
        public string Province { get; set; }
        [JsonProperty("telephone")]
        public string Telephone { get; set; }
    }
}
