﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Workflowscripts.C_CREDITSAFEREQUESTS.ImportRelationFromCreditSafe.CreditSafeModels
{
    class CompanySummary
    {
        [JsonProperty("businessName")]
        public string BusinessName { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("companyNumber")]
        public string CompanyNumber { get; set; }
        [JsonProperty("companyRegistrationNumber")]
        public string CompanyRegistrationNumber { get; set; }
        [JsonProperty("mainActivity")]
        public MainActivity MainActivity { get; set; }
        [JsonProperty("companyStatus")]
        public CompanyStatus CompanyStatus { get; set; }
        [JsonProperty("creditRating")]
        public CreditRating CreditRating { get; set; }
    }
}
