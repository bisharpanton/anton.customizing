﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Workflowscripts.C_CREDITSAFEREQUESTS.ImportRelationFromCreditSafe.CreditSafeModels
{
    class LimitHistory
    {
        [JsonProperty("date")]
        public DateTime Date { get; set; }
        [JsonProperty("companyValue")]
        public string CompanyValue { get; set; }
    }
}
