﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Workflowscripts.C_CREDITSAFEREQUESTS.ImportRelationFromCreditSafe.CreditSafeModels
{
    class CreditRating
    {
        [JsonProperty("commonValue")]
        public string CommonValue { get; set; }
        [JsonProperty("commonDescription")]
        public string CommonDescription { get; set; }
        [JsonProperty("creditLimit")]
        public CreditLimit CreditLimit { get; set; }
        [JsonProperty("providerValue")]
        public ProviderValue ProviderValue { get; set; }
        [JsonProperty("providerDescription")]
        public string ProviderDescription { get; set; }
    }
}
