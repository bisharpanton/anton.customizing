﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Workflowscripts.C_CREDITSAFEREQUESTS.ImportRelationFromCreditSafe.CreditSafeModels
{
    class EmployeesInformation
    {
        [JsonProperty("year")]
        public int Year { get; set; }
        [JsonProperty("numberOfEmployees")]
        public string NumberOfEmployees { get; set; }
    }
}
