namespace Workflowscripts.C_CREDITSAFEREQUESTS.ImportRelationFromCreditSafe.Models
{
    using System;
    using System.ComponentModel;
    
    
    public enum DaybookType : int
    {
        
        /// <summary>
        /// Purchase
        /// </summary>
        [Description("Purchase")]
        Purchase = 1,
        
        /// <summary>
        /// Sales
        /// </summary>
        [Description("Sales")]
        Sales = 2,
        
        /// <summary>
        /// General journals
        /// </summary>
        [Description("General journals")]
        General_journals = 3,
        
        /// <summary>
        /// Cash
        /// </summary>
        [Description("Cash")]
        Cash = 4,
        
        /// <summary>
        /// Bank
        /// </summary>
        [Description("Bank")]
        Bank = 5,
        
        /// <summary>
        /// Giro
        /// </summary>
        [Description("Giro")]
        Giro = 6,
    }
}
