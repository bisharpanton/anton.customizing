﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;

public class CheckWkaFiles : WorkflowScriptInfo
{
	public void Execute()
	{
        //SP
        //22-6-2023
        //Check of WKA dossier compleet is

        var wkaEmployeeIds = GetRecordset("U_WKAEMPLOYEE", "PK_U_WKAEMPLOYEE",
            $"", "").DataTable.AsEnumerable().Select(x => x.Field<int>("PK_U_WKAEMPLOYEE")).ToList();

        foreach(var wkaEmployeeId in wkaEmployeeIds)
		{
            var wkaFileComplete = CheckIfWkaEmployeeIsComplete(wkaEmployeeId);

            UpdateBoolAtWkaEmployee("U_WKAEMPLOYEE", wkaEmployeeId, wkaFileComplete);
        }

        var wkaFileIds = GetRecordset("U_WKAFILE", "PK_U_WKAFILE",
            $"", "").DataTable.AsEnumerable().Select(x => x.Field<int>("PK_U_WKAFILE")).ToList();

        foreach (var wkaFileId in wkaFileIds)
        {
            var wkaTypeId = GetRecordset("U_WKAFILE", "FK_WKATYPE",
                $"PK_U_WKAFILE = {wkaFileId}", "").DataTable.AsEnumerable().First().Field<int>("FK_WKATYPE");

            var wkaFileComplete = CheckIfWkaFileIsComplete(wkaTypeId, wkaFileId);

            UpdateBoolAtWkaEmployee("U_WKAFILE", wkaFileId, wkaFileComplete);
        }

    }

	private void UpdateBoolAtWkaEmployee(string tablename, int id, bool wkaFileComplete)
    {
        var rsWkaEmployee = GetRecordset($"{tablename}", "WKAFILECOMPLETE", $"PK_{tablename} = {id}", "");
        rsWkaEmployee.MoveFirst();
        rsWkaEmployee.SetFieldValue("WKAFILECOMPLETE", wkaFileComplete);
        rsWkaEmployee.Update();
    }

    private bool CheckIfWkaFileIsComplete(int wkaTypeId, int wkaFileId)
    {
        var requiredDocumentTypeIds = GetRecordset("U_WKAREQUIREDDOCUMENTTYPESPERWKATYPE", "FK_WKADOCUMENTTYPE",
            $"FK_WKATYPE = {wkaTypeId}", "").DataTable.AsEnumerable().Select(x => x.Field<int>("FK_WKADOCUMENTTYPE")).ToList();

        var wkaFileDocuments = GetRecordset("U_WKAFILEDOCUMENT", "FK_WKADOCUMENTTYPE, EXPIRATIONDATE",
            $"FK_WKAFILE = {wkaFileId} AND FK_WKADOCUMENTTYPE IS NOT NULL AND EXPIRATIONDATE IS NOT NULL", "").DataTable.AsEnumerable().ToList();

        foreach (var documentId in requiredDocumentTypeIds)
        {
            var employeeDocumentsThisDocumentId = wkaFileDocuments.Where(x => x.Field<int>("FK_WKADOCUMENTTYPE") == documentId).ToList();

            if (!employeeDocumentsThisDocumentId.Any())
            {
                return false;
            }

            if (!employeeDocumentsThisDocumentId.Any(x => x.Field<DateTime?>("EXPIRATIONDATE").HasValue && x.Field<DateTime>("EXPIRATIONDATE").Date >= DateTime.Now.Date))
            {
                return false;
            }
        }

        return true;
    }

    private bool CheckIfWkaEmployeeIsComplete(int wkaEmployeeId)
    {
        var requiredDocumentTypeIds = GetRecordset("U_WKAREQUIREDDOCUMENTTYPESPERWKAEMPLOYEE", "FK_WKADOCUMENTTYPE",
            $"FK_WKAEMPLOYEE = {wkaEmployeeId}", "").DataTable.AsEnumerable().Select(x => x.Field<int>("FK_WKADOCUMENTTYPE")).ToList();

        var wkaEmployeeDocuments = GetRecordset("U_WKAEMPLOYEEDOCUMENT", "FK_WKADOCUMENTTYPE, EXPIRATIONDATE",
            $"FK_WKAEMPLOYEE = {wkaEmployeeId} AND FK_WKADOCUMENTTYPE IS NOT NULL AND EXPIRATIONDATE IS NOT NULL", "").DataTable.AsEnumerable().ToList();

        foreach (var documentId in requiredDocumentTypeIds)
        {
            var employeeDocumentsThisDocumentId = wkaEmployeeDocuments.Where(x => x.Field<int>("FK_WKADOCUMENTTYPE") == documentId).ToList();

            if (!employeeDocumentsThisDocumentId.Any())
            {
                return false;
            }

            if (!employeeDocumentsThisDocumentId.Any(x => x.Field<DateTime?>("EXPIRATIONDATE").HasValue && x.Field<DateTime>("EXPIRATIONDATE").Date >= DateTime.Now.Date))
            {
                return false;
            }
        }

        return true;
    }
}
