﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class AddExtraJournalEntryDetails : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        var vatCodeId = GetRecordset("R_VAT", "PK_R_VAT",
            $"CODE = '8KE'", "").DataTable.AsEnumerable().First().Field<int>("PK_R_VAT");

        double vatAmount = CalculateVatAmount(id, vatCodeId);

        if (vatAmount == 0.0)
        {
            return;//Geen BTW te verleggen
        }

        var journalEntryId = GetRecordset("R_PURCHASEINVOICE", "FK_JOURNALENTRY",
            $"PK_R_PURCHASEINVOICE = {id}", "").DataTable.AsEnumerable().First().Field<int?>("FK_JOURNALENTRY") ?? 0;

        if (journalEntryId == 0)
        {
            return;
        }

        CreateJournalEntryDetails(vatAmount, journalEntryId, vatCodeId);
    }

    private void CreateJournalEntryDetails(double vatAmount, int journalEntryId, int vatCodeId)
    {
        var journalEntryNumber = GetRecordset("R_JOURNALENTRY", "JOURNALENTRYNUMBER",
            $"PK_R_JOURNALENTRY = {journalEntryId}", "").DataTable.AsEnumerable().First().Field<int>("JOURNALENTRYNUMBER");

        var generalledLedgerAccountIds = GetRecordset("R_GENERALLEDGERACCOUNT", "PK_R_GENERALLEDGERACCOUNT",
            $"GENERALLEDGERACCOUNTNUMBER IN ('1530', '1531')", "").DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_GENERALLEDGERACCOUNT")).ToList();

        if(!generalledLedgerAccountIds.Any())
        {
            return;
        }

        var rsJournalEntryDetail = GetRecordset("R_JOURNALENTRYDETAIL", "", "PK_R_JOURNALENTRYDETAIL = -1", "");
        rsJournalEntryDetail.UseDataChanges = true;
        rsJournalEntryDetail.UpdateWhenMoveRecord = false;

        foreach (var generalledLedgerAccountId in generalledLedgerAccountIds)
        {
            var generalledLedgerAccount = GetRecordset("R_GENERALLEDGERACCOUNT", "BALANCESIDE, DESCRIPTION",
                $"PK_R_GENERALLEDGERACCOUNT = {generalledLedgerAccountId}", "").DataTable.AsEnumerable().First();

            var balanceSide = generalledLedgerAccount.Field<int>("BALANCESIDE");
            var description = generalledLedgerAccount.Field<string>("DESCRIPTION");

            rsJournalEntryDetail.AddNew();

            rsJournalEntryDetail.SetFieldValue("FK_JOURNALENTRY", journalEntryId);
            rsJournalEntryDetail.SetFieldValue("FK_GENERALLEDGERACCOUNT", generalledLedgerAccountId);
            rsJournalEntryDetail.SetFieldValue("FK_VAT", vatCodeId);
            rsJournalEntryDetail.SetFieldValue("BALANCESIDE", 1);
            rsJournalEntryDetail.SetFieldValue("COSTUNITQUANTITY", 1.0);
            rsJournalEntryDetail.SetFieldValue("DESCRIPTION", journalEntryNumber + " - " + description);

            if (balanceSide == 1)//Debet
            {
                var roundedVatAmount = Math.Round(vatAmount * 0.21, 2);

                rsJournalEntryDetail.SetFieldValue("AMOUNT", roundedVatAmount);
            }
            else
            {
                var roundedVatAmount = Math.Round(vatAmount * -1 * 0.21, 2);

                rsJournalEntryDetail.SetFieldValue("AMOUNT", roundedVatAmount);
            }


        }

        var updateResult = rsJournalEntryDetail.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            throw new Exception($"Aanmaken journaalpostregels t.b.v. BTW verlegd mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");
        }
    }

    private double CalculateVatAmount(int id, int vatCodeId)
    {
        return GetRecordset("R_PURCHASEINVOICEALLDETAIL", "NETPURCHASEPRICE",
            $"FK_PURCHASEINVOICE = {id} AND FK_VATCODE = {vatCodeId}", "").DataTable.AsEnumerable().Sum(x => x.Field<double>("NETPURCHASEPRICE"));
    }
}
