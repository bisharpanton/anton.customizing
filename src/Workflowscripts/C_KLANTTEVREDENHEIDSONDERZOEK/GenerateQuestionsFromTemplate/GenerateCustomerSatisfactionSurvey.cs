﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class GenerateCustomerSatisfactionSurvey : WorkflowScriptInfo
{
    //private static string _sqlDataSource = @"LT115\RIDDERIQ"; //Lokaal testen
    private static string _sqlDataSource = @"SRV-APP03\RIDDERIQ"; //Live bij Anton

    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //04-12-2017
        //Haal template uit bedrijf Anton Groep en kopieer van daaruit de vragen

        string connectionString = $@"Data Source={_sqlDataSource};Initial Catalog=Anton Groep;User Id=sa;Password=Welkom@ridderiq";

        using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connectionString))
        {
            conn.Open();

            string sqlSelectTemplate = @"
				SELECT CATEGORY, QUESTION
				FROM [Anton Groep].dbo.C_TEMPLATEKLANTTEVREDENHEIDSONDERZOEK;";

            System.Data.SqlClient.SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand(sqlSelectTemplate, conn);
            //sqlCmd.Parameters.Add(new SqlParameter("@QuoteNr", offerNumber));

            System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(sqlCmd);
            DataTable dtSelectTemplate = new DataTable("Template");
            da.Fill(dtSelectTemplate);

            da = null;
            sqlCmd = null;

            conn.Close();

            ScriptRecordset rsKlanttevredenheidsonderzoekVragen = this.GetRecordset("C_KLANTTEVREDENHEIDSONDERZOEKVRAGEN", "", "PK_C_KLANTTEVREDENHEIDSONDERZOEKVRAGEN = -1", "");

            foreach (DataRow row in dtSelectTemplate.Rows)
            {
                rsKlanttevredenheidsonderzoekVragen.AddNew();
                rsKlanttevredenheidsonderzoekVragen.Fields["FK_KLANTTEVREDENHEIDSONDERZOEK"].Value = id;
                rsKlanttevredenheidsonderzoekVragen.Fields["CATEGORY"].Value = (int)row["CATEGORY"];
                rsKlanttevredenheidsonderzoekVragen.Fields["QUESTION"].Value = row["QUESTION"].ToString();
            }

            if (dtSelectTemplate.Rows.Count > 0)
            {
                rsKlanttevredenheidsonderzoekVragen.MoveFirst();
                rsKlanttevredenheidsonderzoekVragen.Update();
            }
        }

    }
}
