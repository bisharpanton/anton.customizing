﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;
using System.Globalization;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Newtonsoft.Json;

public class SendSurveyRequestToIntranet : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //28-10-2020
        //Stuur een uitnodiging voor het KTO via intranet naar de klant.
        //We doen dit door een POST call te doen richting het intranet en het Intranet Survey id op te slaan

        var surveyRequestInformation = GetSurveyRequestInformation(id);

        if (surveyRequestInformation == null)
        {
            return;
        }


        var result = SendPostRequestToIntranet(surveyRequestInformation);
        var intranetSurveyId = result.Result;

        if (string.IsNullOrEmpty(intranetSurveyId))
        {
            return;
        }

        UpdateKtoInRidderIq(id, intranetSurveyId);
    }

    private void UpdateKtoInRidderIq(int surveyId, string intranetSurveyId)
    {
        var rsSurvey = GetRecordset("C_KLANTTEVREDENHEIDSONDERZOEK",
            "INTRANETSURVEYID, SENDDATE",
            $"PK_C_KLANTTEVREDENHEIDSONDERZOEK = {surveyId}", "");
        rsSurvey.MoveFirst();

        rsSurvey.SetFieldValue("INTRANETSURVEYID", intranetSurveyId);
        rsSurvey.SetFieldValue("SENDDATE", DateTime.Now);

        rsSurvey.Update();
    }

    private async Task<string> SendPostRequestToIntranet(SurveyRequestInformation surveyRequestInformation)
    {
        var username = "8PFE-DRCE-FVAA-2GUO";
        var password = "x";

        var urlPath = new Uri(@"https://e-klanttevredenheidsonderzoek.nl/endpoint/create-invitation.php");

        HttpClient client = new HttpClient();

        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
            "Basic", Convert.ToBase64String(
                System.Text.ASCIIEncoding.ASCII.GetBytes(
                    $"{username}:{password}")));

        HttpResponseMessage result = client.PostAsJsonAsync(urlPath, surveyRequestInformation)
            .GetAwaiter().GetResult();

        if (!result.IsSuccessStatusCode)
        {
            Log.Error("Verbinden met Intranet",
                $"Verbinding maken met Intranet is mislukt; oorzaak: {(int)result.StatusCode}:{result.ReasonPhrase}", "");
            return string.Empty;
        }

        string response = await result.Content.ReadAsStringAsync();
        var rootObject = JsonConvert.DeserializeObject<Rootobject>(response);

        return rootObject.id;
    }

    private SurveyRequestInformation GetSurveyRequestInformation(int surveyId)
    {
        var survey = GetRecordset("C_KLANTTEVREDENHEIDSONDERZOEK",
            "FK_RELATION, FK_CONTACT, FK_EMPLOYEE, FK_ORDER",
            $"PK_C_KLANTTEVREDENHEIDSONDERZOEK = {surveyId}", "")
            .DataTable.AsEnumerable().First();

        var resultCheckMandatoryFields = CheckMandatoryFieldsSurvey(survey);

        if (resultCheckMandatoryFields.HasError)
        {
            Log.Error(GetRecordTag("C_KLANTTEVREDENHEIDSONDERZOEK", surveyId),
                resultCheckMandatoryFields.Message, "");
            return null;
        }

        var relationInfo = GetRelationInfo(survey.Field<int>("FK_RELATION"));

        var employee = GetRecordset("R_EMPLOYEE",
                "FK_PERSON, EMAIL",
                $"PK_R_EMPLOYEE = {survey.Field<int>("FK_EMPLOYEE")}", "")
            .DataTable.AsEnumerable().First();

        var employeePersonInfo = GetPersonInfo(employee.Field<int>("FK_PERSON"));

        var contact = GetRecordset("R_CONTACT",
                "FK_PERSON, EMAIL",
                $"PK_R_CONTACT = {survey.Field<int>("FK_CONTACT")}", "")
            .DataTable.AsEnumerable().First();

        var contactPersonInfo = GetPersonInfo(contact.Field<int>("FK_PERSON"));

        var surveyTemplate = GetSurveyTemplateFromCrm();

        if (string.IsNullOrEmpty(surveyTemplate))
        {
            Log.Error(GetRecordTag("C_KLANTTEVREDENHEIDSONDERZOEK", surveyId),
                $"In de CRM instellingen is het veld 'KTO template' niet ingevuld.", "");
            return null;
        }

        var orderReference = GetOrderReference(survey.Field<int>("FK_ORDER"));

        if (string.IsNullOrEmpty(orderReference))
        {
            Log.Error(GetRecordTag("C_KLANTTEVREDENHEIDSONDERZOEK", surveyId),
                $"Geen omschrijving gevonden bij de order.", "");
            return null;
        }

        if (string.IsNullOrEmpty(contact.Field<string>("EMAIL")))
        {
            Log.Error(GetRecordTag("C_KLANTTEVREDENHEIDSONDERZOEK", surveyId),
                $"Geen e-mail gevonden bij deze contactpersoon.", "");
            return null;
        }

        return new SurveyRequestInformation()
        {
            Company = relationInfo.Name,
            Salutation = contactPersonInfo.Salutation,
            Fullname = contactPersonInfo.FullName,
            Emailaddress = contact.Field<string>("EMAIL"),
            Subsidiary = surveyTemplate,
            Reference = orderReference,
            Language = relationInfo.LanguageAbbreviation,
            Invitationtxt = "",
            Employee = employeePersonInfo.FullName,
            EmployeeEmailaddress = employee.Field<string>("EMAIL"),
            AntonCompany = GetUserInfo().CompanyName,
        };
    }

    private RelationInfo GetRelationInfo(int relationId)
    {
        var relation = GetRecordset("R_RELATION", "NAME, FK_LANGUAGE",
            $"PK_R_RELATION = {relationId}", "").DataTable.AsEnumerable().First();

        var languageCode = GetRecordset("R_LANGUAGE", "CODE",
                $"PK_R_LANGUAGE = {relation.Field<int>("FK_LANGUAGE")}", "")
            .DataTable.AsEnumerable().First().Field<string>("CODE");

        return new RelationInfo()
        {
            Name = relation.Field<string>("NAME"),
            LanguageAbbreviation = languageCode.Equals("NL") ? "nl" : "en",
        };
    }

    private string GetOrderReference(int orderId)
    {
        var order = GetRecordset("R_ORDER", "DESCRIPTION, REFERENCE, FK_LANGUAGE",
                $"PK_R_ORDER = {orderId}", "").DataTable.AsEnumerable().First();

        if (string.IsNullOrEmpty(order.Field<string>("DESCRIPTION")))
        {
            return string.Empty; //Omschrijving moet gevuld zijn.
        }

        var additionReference = "";

        if (!string.IsNullOrEmpty(order.Field<string>("REFERENCE")))
        {
            additionReference = order.Field<int>("FK_LANGUAGE") == 1 //NL
                ? $", uw referentie: {order.Field<string>("REFERENCE")}"
                : $", your reference: {order.Field<string>("REFERENCE")}";
        }
        
        return order.Field<string>("DESCRIPTION") + additionReference;
    }

    private string GetSurveyTemplateFromCrm()
    {
        return GetRecordset("R_CRMSETTINGS", "SURVEYTEMPLATE", "", "")
            .DataTable.AsEnumerable().First().Field<string>("SURVEYTEMPLATE");
    }

    private PersonInfo GetPersonInfo(int personId)
    {
        var person = GetRecordset("R_PERSON",
                "FIRSTNAME, NAMEPREFIX, LASTNAME, GENDER",
                $"PK_R_PERSON = {personId}", "")
            .DataTable.AsEnumerable().First();

        return new PersonInfo()
        {
            Salutation = person.Field<int>("GENDER") == 2 ? "v" : "m",
            FullName = string.IsNullOrEmpty(person.Field<string>("NAMEPREFIX"))
                ? $"{person.Field<string>("FIRSTNAME")} {person.Field<string>("LASTNAME")}"
                : $"{person.Field<string>("FIRSTNAME")} {person.Field<string>("NAMEPREFIX")} {person.Field<string>("LASTNAME")}",
        };
    }

    private Result CheckMandatoryFieldsSurvey(DataRow survey)
    {
        if (!survey.Field<int?>("FK_RELATION").HasValue)
        {
            return new Result() { HasError = true, Message = $"Relatie is verplicht bij verzenden KTO via intranet." };
        }

        if (!survey.Field<int?>("FK_CONTACT").HasValue)
        {
            return new Result() { HasError = true, Message = $"Contactpersoon is verplicht bij verzenden KTO via intranet." };
        }

        if (!survey.Field<int?>("FK_EMPLOYEE").HasValue)
        {
            return new Result() { HasError = true, Message = $"Werknemer is verplicht bij verzenden KTO via intranet." };
        }

        if (!survey.Field<int?>("FK_ORDER").HasValue)
        {
            return new Result() { HasError = true, Message = $"Order is verplicht bij verzenden KTO via intranet. Anders is er geen project referentie bekend." };
        }

        return new Result() { HasError = false, Message = "" };
    }

    class RelationInfo
    {
        public string Name { get; set; }
        public string LanguageAbbreviation { get; set; }
    }

    class PersonInfo
    {
        public string Salutation { get; set; }
        public string FullName { get; set; }
    }

    class Result
    {
        public bool HasError { get; set; }
        public string Message { get; set; }
    }

    class SurveyRequestInformation
    {
        [JsonProperty("company")]
        public string Company { get; set; }
        [JsonProperty("salutation")]
        public string Salutation { get; set; }
        [JsonProperty("fullname")]
        public string Fullname { get; set; }
        [JsonProperty("emailaddress")]
        public string Emailaddress { get; set; }
        [JsonProperty("subsidiary")]
        public string Subsidiary { get; set; }
        [JsonProperty("reference")]
        public string Reference { get; set; }
        [JsonProperty("language")]
        public string Language { get; set; }
        [JsonProperty("invitationtxt")]
        public string Invitationtxt { get; set; }
        [JsonProperty("employee")]
        public string Employee { get; set; }
        [JsonProperty("employee_emailaddress")]
        public string EmployeeEmailaddress { get; set; }
        [JsonProperty("anton_company")]
        public string AntonCompany { get; set; }
    }

    public class Rootobject
    {
        public string id { get; set; }
        public string item_key { get; set; }
        public string name { get; set; }
        public string ip { get; set; }
        public Meta meta { get; set; }
        public string form_id { get; set; }
        public string post_id { get; set; }
        public string user_id { get; set; }
        public string parent_item_id { get; set; }
        public string is_draft { get; set; }
        public string updated_by { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
    }

    public class Meta
    {
        public string company { get; set; }
        public string salutation { get; set; }
        public string salutationvalue { get; set; }
        public string fullname { get; set; }
        public string emailaddress { get; set; }
        public string subsidiary { get; set; }
        public string subsidiaryvalue { get; set; }
        public string reference { get; set; }
        public string language { get; set; }
        public string invitationtxt { get; set; }
        public string invitationtxtvalue { get; set; }
        public string employee { get; set; }
        public string employeeemailaddress { get; set; }
        public string surveyhash { get; set; }
        public string source { get; set; }
        public string employeeid { get; set; }
        public string employeeidvalue { get; set; }
    }
}
