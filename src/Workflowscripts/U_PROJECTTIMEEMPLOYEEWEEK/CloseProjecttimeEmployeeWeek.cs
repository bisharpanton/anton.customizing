﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class CloseProjecttimeEmployeeWeek : WorkflowScriptInfo
{
    public void Execute()
    {
        var closeHours = Convert.ToBoolean(Parameters["CloseHours"].Value);

        if(!closeHours)
        {
            return;
        }

        var id = (int)RecordId;

        var rsProjectTime = GetRecordset("R_PROJECTTIME", "PK_R_PROJECTTIME, CLOSED",
            $"FK_PROJECTTIMEEMPLOYEEWEEK = {id}", "");
        
        if(rsProjectTime.RecordCount > 0)
        {
            CloseOpenProjecttimes(rsProjectTime);
        }

        UpdateCloseInfo(id);
    }

    private void UpdateCloseInfo(int id)
    {
        var rsProjecttimeEmployeeWeek = GetRecordset("U_PROJECTTIMEEMPLOYEEWEEK", "DATECLOSED, CLOSEDBY",
            $"PK_U_PROJECTTIMEEMPLOYEEWEEK = {id}", "");
        rsProjecttimeEmployeeWeek.MoveFirst();

        rsProjecttimeEmployeeWeek.SetFieldValue("DATECLOSED", DateTime.Now);
        rsProjecttimeEmployeeWeek.SetFieldValue("CLOSEDBY", GetUserInfo().CurrentUserName);

        rsProjecttimeEmployeeWeek.Update();

    }

    private void CloseOpenProjecttimes(ScriptRecordset rsProjectTime)
    {
        //Zet vinkje 'Closed' aan bij geselecteerde uren
        rsProjectTime.MoveFirst();

        rsProjectTime.UpdateWhenMoveRecord = false;
        rsProjectTime.UseDataChanges = false;

        while (!rsProjectTime.EOF)
        {
            rsProjectTime.Fields["CLOSED"].Value = true;

            rsProjectTime.MoveNext();
        }

        rsProjectTime.MoveFirst();
        rsProjectTime.Update();
    }
}
