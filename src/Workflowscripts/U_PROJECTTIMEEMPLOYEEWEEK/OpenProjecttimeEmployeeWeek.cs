﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class OpenProjecttimeEmployeeWeek : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        var rsProjectTime = GetRecordset("R_PROJECTTIME", "PK_R_PROJECTTIME, CLOSED",
            $"FK_PROJECTTIMEEMPLOYEEWEEK = {id}", "");

        if (rsProjectTime.RecordCount > 0)
        {
            OpenClosedProjecttimes(rsProjectTime);
        }

        UpdateCloseInfo(id);
    }

    private void UpdateCloseInfo(int id)
    {
        var rsProjecttimeEmployeeWeek = GetRecordset("U_PROJECTTIMEEMPLOYEEWEEK", "DATECLOSED, CLOSEDBY",
            $"PK_U_PROJECTTIMEEMPLOYEEWEEK = {id}", "");
        rsProjecttimeEmployeeWeek.MoveFirst();

        rsProjecttimeEmployeeWeek.SetFieldValue("DATECLOSED", DBNull.Value);
        rsProjecttimeEmployeeWeek.SetFieldValue("CLOSEDBY", "");

        rsProjecttimeEmployeeWeek.Update();

    }

    private void OpenClosedProjecttimes(ScriptRecordset rsProjectTime)
    {
        //Zet vinkje 'Closed' uit bij geselecteerde uren
        rsProjectTime.MoveFirst();

        rsProjectTime.UpdateWhenMoveRecord = false;
        rsProjectTime.UseDataChanges = false;

        while (!rsProjectTime.EOF)
        {
            rsProjectTime.Fields["CLOSED"].Value = false;

            rsProjectTime.MoveNext();
        }

        rsProjectTime.MoveFirst();
        rsProjectTime.Update();
    }
}
