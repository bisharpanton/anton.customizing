﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Formatting;

public class RidderScript : WorkflowScriptInfo
{
    public void Execute()
    {
        //DB
        //21-12-2017
        //Genereer orderbasis met subaantal en zet deze om

        double aantal = Convert.ToDouble(Parameters["Aantal"].Value);

        if (Math.Round(aantal, 1) == 0.0)
        {
            throw new Exception("Een aantal van 0.0 is niet toegestaan.");
        }

        var rsVerkoopregelStuklijst = GetRecordset("R_SALESORDERDETAILASSEMBLY", "",
            $"PK_R_SALESORDERDETAILASSEMBLY = {RecordId}", "");
        rsVerkoopregelStuklijst.MoveFirst();

        var rsOrderBase = GetRecordset("R_ORDERBASE", "QUANTITY",
            $"FK_SALESORDERDETAILASSEMBLYCUSTOM = {RecordId}", "");
        double quantityOutDeliverd = rsOrderBase.DataTable.AsEnumerable().Sum(x => x.Field<double>("QUANTITY"));

        if ((double)rsVerkoopregelStuklijst.Fields["QUANTITY"].Value - (aantal + quantityOutDeliverd) < 0.0)
        {
            //Het totaal uit te leveren aantal is groter dan het aantal van de verkoopregel, werk de verkoopregel bij
            rsVerkoopregelStuklijst.UseDataChanges = true;
            rsVerkoopregelStuklijst.Fields["QUANTITY"].Value = aantal + quantityOutDeliverd;
            rsVerkoopregelStuklijst.Fields["OUTDELIVERYCOMPLETE"].Value = true;
            rsVerkoopregelStuklijst.Update();
        }
        else if (Convert.ToBoolean(Parameters["OutdeliveryComplete"].Value))
        {
            rsVerkoopregelStuklijst.Fields["OUTDELIVERYCOMPLETE"].Value = true;
            rsVerkoopregelStuklijst.Update();
        }

        //Genereer basisregels voor alle verkoopregels, verwijder daarna degene die overbodig zijn gegenereerd
        var resultGenerateOrderbase = EventsAndActions.Production.Actions.GenerateOrderBase((int)rsVerkoopregelStuklijst.Fields["FK_ORDER"].Value);

        if (resultGenerateOrderbase.HasError)
        {
            throw new Exception($"Het genereren van de basisregels is mislukt; {resultGenerateOrderbase.GetResult()}");
        }

        //Haal de overbodige orderbasis op verwijderd deze
        var rsOrderBaseOverbodig = GetRecordset("R_ORDERBASE", "PK_R_ORDERBASE",
            $"FK_SALESORDERDETAILASSEMBLY <> {RecordId} AND CONVERTED = 0", "");

        if (rsOrderBaseOverbodig.RecordCount > 0)
        {
            rsOrderBaseOverbodig.UpdateWhenMoveRecord = false;
            rsOrderBaseOverbodig.MoveFirst();

            while (!rsOrderBaseOverbodig.EOF)
            {
                rsOrderBaseOverbodig.Delete();
                rsOrderBaseOverbodig.MoveNext();
            }

            rsOrderBaseOverbodig.MoveFirst();
            rsOrderBaseOverbodig.Update();
        }

        //Controleer of er een nieuwe basisregel is gegenerered voor de huidige verkoopregel stuklijst
        var rsOrderbasis = GetRecordset("R_ORDERBASE", "",
            $"FK_SALESORDERDETAILASSEMBLY = {RecordId} AND CONVERTED = 0", "");

        if (rsOrderbasis.RecordCount > 0)
        {
            rsOrderbasis.MoveFirst();
            rsOrderbasis.Fields["QUANTITY"].Value = aantal;
            rsOrderbasis.Update();
        }
        else
        {
            rsOrderbasis.UseDataChanges = true;

            rsOrderbasis.AddNew();
            rsOrderbasis.Fields["FK_ORDERNUMBER"].Value = rsVerkoopregelStuklijst.Fields["FK_ORDER"].Value;
            rsOrderbasis.Fields["FK_ASSEMBLY"].Value = rsVerkoopregelStuklijst.Fields["FK_ASSEMBLY"].Value;
            rsOrderbasis.Fields["QUANTITY"].Value = aantal;
            rsOrderbasis.Fields["FK_SALESORDERDETAILASSEMBLYCUSTOM"].Value = RecordId;
            rsOrderbasis.Update();
        }

        //Convert ordersbasis
        Guid wfEventConvertOrderBase = new Guid("7a788724-80b2-4541-8a5f-7ec285dcd08f");

        var result = ExecuteWorkflowEvent("R_ORDERBASE", (int)rsOrderbasis.Fields["PK_R_ORDERBASE"].Value, wfEventConvertOrderBase, null);

        if (result.HasError)
        {
            throw new Exception($"Het genereren van de bonnen is mislukt; {result.GetResult()}");
        }
    }
}
