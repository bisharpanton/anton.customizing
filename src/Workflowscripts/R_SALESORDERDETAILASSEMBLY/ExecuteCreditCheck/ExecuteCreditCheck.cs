﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class ExecuteCreditCheck : WorkflowScriptInfo
{
    public Boolean Execute()
    {
        var id = (int)RecordId;

        //DB
        //23-11-2017
        //Controleer of de gekoppelde verkoopregel voldoet aan de kredietcheck,
        //indien ja probeer kredietcheck automatisch te doen

        var rsSalesOrderDetailAssembly = GetRecordset("R_SALESORDERDETAILASSEMBLY", 
            "FK_ORDER, CREDITCHECK, NETSALESAMOUNT, MEMOCREDITCHECK, OVERRULECREDITCHECK, FK_CREDITCHECKOVERRULEDBY",
            $"PK_R_SALESORDERDETAILASSEMBLY = {id}", "");
        rsSalesOrderDetailAssembly.MoveFirst();

        var rsCRM = GetRecordset("R_CRMSETTINGS", "CREDITCHECKAPPLICABLE", "", "");
        rsCRM.MoveFirst();

        if (!(bool)rsCRM.Fields["CREDITCHECKAPPLICABLE"].Value)
        {
            UpdateSalesOrderDetailAssemblyCreditCheckMemo(rsSalesOrderDetailAssembly, "Geen kredietcheck ingesteld in CRM instellingen.");

            return true;
        }

        if ((bool)rsSalesOrderDetailAssembly.Fields["OVERRULECREDITCHECK"].Value)
        {
            rsSalesOrderDetailAssembly.SetFieldValue("CREDITCHECK", CreditCheck.Akkoord);
            rsSalesOrderDetailAssembly.Update();

            UpdateSalesOrderDetailAssemblyCreditCheckMemo(rsSalesOrderDetailAssembly, 
                $"Kredietcheck overruled door {GetRecordTag("R_EMPLOYEE", rsSalesOrderDetailAssembly.GetField("FK_CREDITCHECKOVERRULEDBY").Value)}.");

            return true;
        }

        if ((CreditCheck)rsSalesOrderDetailAssembly.GetField("CREDITCHECK").Value == CreditCheck.Akkoord)
        {
            UpdateSalesOrderDetailAssemblyCreditCheckMemo(rsSalesOrderDetailAssembly, "Kreditcheck verkoopregel staat al op akkoord.");

            return true;
        }

        var relationId = GetRelationFromOrder((int)rsSalesOrderDetailAssembly.GetField("FK_ORDER").Value);

        if (InterCompanyRelation(relationId))
        {
            rsSalesOrderDetailAssembly.SetFieldValue("CREDITCHECK", CreditCheck.Akkoord);
            rsSalesOrderDetailAssembly.Update();

            UpdateSalesOrderDetailAssemblyCreditCheckMemo(rsSalesOrderDetailAssembly, "Kreditcheck automatisch akkoord bij intercompany relaties.");

            return true;
        }


        var netSalesAmountSalesOrderDetailAssembly = (double)rsSalesOrderDetailAssembly.GetField("NETSALESAMOUNT").Value;

        var onderhandenOrders = GetOnderhandenOrders(relationId);

        var totalOrderAmountOnderhanden = CalculateTotalOrderAmount(onderhandenOrders);
        var outstandingAmountCreditCheck = GetOutstandingAmountCreditCheck(relationId);
        var invoicedAmountOrderOnderhanden = CalculateInvoicedAmount(onderhandenOrders);
        var totalOutstandingOrderAmount =
            totalOrderAmountOnderhanden - outstandingAmountCreditCheck - invoicedAmountOrderOnderhanden;

        var outstandingInvoicedAmount = GetTotalOutstandingInvoicedAmount(relationId);

        var totalOutstanding = netSalesAmountSalesOrderDetailAssembly + totalOutstandingOrderAmount +
                               outstandingInvoicedAmount;

        var creditLimitRelation = GetCreditLimitRelation(relationId);

        var message = $"Verkoopregel netto verkoopbedrag: €{netSalesAmountSalesOrderDetailAssembly:n2}";
        message += $"\nOpenstaand orderbedrag: €{totalOutstandingOrderAmount:n2}";
        message += $"\nOpenstaand bedrag verkoopfacturen: €{outstandingInvoicedAmount:n2}";
        message +=
            $"\nTotaal openstaand: €{netSalesAmountSalesOrderDetailAssembly + totalOutstandingOrderAmount + outstandingInvoicedAmount:n2}";

        message +=
            $"\n\nKredietlimiet relatie: €{creditLimitRelation:n2}";

        message +=
            $"\n\nVerschil: €{creditLimitRelation - (netSalesAmountSalesOrderDetailAssembly + totalOutstandingOrderAmount + outstandingInvoicedAmount):n2}";

        UpdateSalesOrderDetailAssemblyCreditCheckMemo(rsSalesOrderDetailAssembly, message);

        if (totalOutstanding < creditLimitRelation)
        {
            rsSalesOrderDetailAssembly.SetFieldValue("CREDITCHECK", CreditCheck.Akkoord);
            rsSalesOrderDetailAssembly.Update();

            return true;
        }
        else
        {
            rsSalesOrderDetailAssembly.SetFieldValue("CREDITCHECK", CreditCheck.Geblokkeerd);
            rsSalesOrderDetailAssembly.Update();

            return false;
        }
    }

    private bool InterCompanyRelation(int relationId)
    {
        return GetRecordset("R_RELATION", "INTERCOMPANYRELATION",
            $"PK_R_RELATION = {relationId}", "").DataTable.AsEnumerable().First().Field<bool>("INTERCOMPANYRELATION");
    }

    private double GetOutstandingAmountCreditCheck(int relationId)
    {
        return GetRecordset("C_OUTSTANDINGCREDITCHECK", "NETSALESAMOUNT",
                $"FK_RELATION = {relationId}", "")
            .DataTable.AsEnumerable().Sum(x => (double)x["NETSALESAMOUNT"]);
    }

    private double GetCreditLimitRelation(int relationId)
    {
        var rsRelation = GetRecordset("R_RELATION",
            "FK_PARENTCOMPANY, SALESCREDITLIMIT, SALESCREDITLIMITSCORE",
            $"PK_R_RELATION = {relationId}", "");
        rsRelation.MoveFirst();

        if ((rsRelation.GetField("FK_PARENTCOMPANY").Value as int?).HasValue &&
            rsRelation.GetField("SALESCREDITLIMITSCORE").Value.ToString().Contains("403"))
        {
            //Bij 403 verklaring, haal dan het kredietlimiet van het moederbedrijf op
            return GetRecordset("R_RELATION",
                    "SALESCREDITLIMIT",
                    $"PK_R_RELATION = {rsRelation.GetField("FK_PARENTCOMPANY").Value}", "")
                .DataTable.AsEnumerable().First().Field<double>("SALESCREDITLIMIT");
        }

        return (double)rsRelation.GetField("SALESCREDITLIMIT").Value;
    }

    private double GetTotalOutstandingInvoicedAmount(int relationId)
    {
        return GetRecordset("R_SALESINVOICE", "OUTSTANDINGAMOUNT",
                $"FK_INVOICERELATION = {relationId}", "")
            .DataTable.AsEnumerable().Sum(x => (double)x["OUTSTANDINGAMOUNT"]);
    }

    private double CalculateInvoicedAmount(List<int> onderhandenOrders)
    {
        if (!onderhandenOrders.Any())
        {
            return 0.0;
        }

        var salesInvoiceAllDetails = GetRecordset("R_SALESINVOICEALLDETAIL",
            "FK_SALESINVOICE, NETSALESAMOUNT",
            $"FK_ORDER IN ({string.Join(",", onderhandenOrders)})", "").DataTable.AsEnumerable().ToList();

        if (!salesInvoiceAllDetails.Any())
        {
            return 0.0;
        }

        var wfStateDone = new Guid("49978ba1-7725-459e-8a4f-934c050fa8c1");
        var salesInvoiceIdsWithStateDone = GetRecordset("R_SALESINVOICE", "PK_R_SALESINVOICE",
                $"FK_WORKFLOWSTATE = '{wfStateDone}' AND PK_R_SALESINVOICE IN ({string.Join(",", salesInvoiceAllDetails.Select(x => x.Field<int>("FK_SALESINVOICE")).ToList())})", 
                "").DataTable.AsEnumerable().Select(x => (int)x["PK_R_SALESINVOICE"]).ToList();

        if (!salesInvoiceIdsWithStateDone.Any())
        {
            return 0.0;
        }

        return salesInvoiceAllDetails
            .Where(x => salesInvoiceIdsWithStateDone.Contains(x.Field<int>("FK_SALESINVOICE")))
            .Sum(x => (double)x["NETSALESAMOUNT"]);
    }

    private double CalculateTotalOrderAmount(List<int> onderhandenOrders)
    {
        if (!onderhandenOrders.Any())
        {
            return 0.0;
        }

        var totalOrderAmount = GetRecordset("R_ORDER",
                "TOTALNETAMOUNT",
                $"PK_R_ORDER IN ({string.Join(",", onderhandenOrders)})", "")
            .DataTable.AsEnumerable().Sum(x => (double)x["TOTALNETAMOUNT"]);

        return totalOrderAmount;
    }

    private List<int> GetOnderhandenOrders(int relationId)
    {
        var wfStateNew = new Guid("6205d9d2-72d7-49df-8b4b-6e5c1c3d4927");
        var wfStateOnderhanden = new Guid("e83694f3-b6a8-4c2f-8c65-20107edf22bc");

        return GetRecordset("R_ORDER", "PK_R_ORDER",
            $"FK_RELATION = {relationId} AND (FK_WORKFLOWSTATE = '{wfStateNew}' OR FK_WORKFLOWSTATE = '{wfStateOnderhanden}')",
            "").DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_ORDER")).ToList();
    }

    private int GetRelationFromOrder(int orderId)
    {
        return GetRecordset("R_ORDER", "FK_RELATION", 
            $"PK_R_ORDER = {orderId}", "")
            .DataTable.AsEnumerable().First().Field<int>("FK_RELATION");
    }

    private void UpdateSalesOrderDetailAssemblyCreditCheckMemo(ScriptRecordset rsSalesOrderDetailAssembly, 
        string message)
    {
        rsSalesOrderDetailAssembly.MoveFirst();

        if (rsSalesOrderDetailAssembly.GetField("MEMOCREDITCHECK").Value.ToString() == "")
        {
            rsSalesOrderDetailAssembly.SetFieldValue("MEMOCREDITCHECK", $"[{DateTime.Now:dd-MM-yyyy hh:mm:ss}][{CurrentUser.UserName}] \n{message}");
        }
        else
        {
            rsSalesOrderDetailAssembly.SetFieldValue("MEMOCREDITCHECK", $"[{DateTime.Now:dd-MM-yyyy hh:mm:ss}][{CurrentUser.UserName}] \n{message}\n\n{rsSalesOrderDetailAssembly.GetField("MEMOCREDITCHECK").Value}");
        }

        rsSalesOrderDetailAssembly.Update();
    }

    private enum CreditCheck : int
    {
        _ = 1,
        Akkoord = 2,
        Geblokkeerd = 3
    }
}
