﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class ValidateAndLogInfo : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //02-03-2016
        //Zet datum
        //26-3-2020 - Verplicht invullen veld 'Ongeval- en incidentmelding'

        var choiceIncidentReportNothing = 1;
        var choiceIncidentReportOngevalMetVerzuim = 2;
        var choiceIncidentReportOngevalZonderVerzuim = 4;

        var rsSafeteReport = GetRecordset("C_VEILIGHEIDSMELDINGEN", "DATUMOPGELOST, INCIDENTREPORTING, ISOUTSTANDING, CAUSE, MEASURES, LONGTERMMEASURES",
            $"PK_C_VEILIGHEIDSMELDINGEN = {id}", "");
        rsSafeteReport.MoveFirst();
        
        if ((int)rsSafeteReport.Fields["INCIDENTREPORTING"].Value == choiceIncidentReportNothing)
        {
            Log.Error(GetRecordTag("C_VEILIGHEIDSMELDINGEN", id),
                "Melding is verplicht bij het afsluiten van een veiligheidsmelding. ", "");
            return;
        }

        if((int)rsSafeteReport.Fields["INCIDENTREPORTING"].Value == choiceIncidentReportOngevalMetVerzuim
            || (int)rsSafeteReport.Fields["INCIDENTREPORTING"].Value == choiceIncidentReportOngevalZonderVerzuim)
        {
            //Check of de memo velden gevuld zijn
            if(string.IsNullOrEmpty(rsSafeteReport.GetField("CAUSE").Value.ToString()) || string.IsNullOrEmpty(rsSafeteReport.GetField("MEASURES").Value.ToString())
                || string.IsNullOrEmpty(rsSafeteReport.GetField("LONGTERMMEASURES").Value.ToString()))
            {
                Log.Error(GetRecordTag("C_VEILIGHEIDSMELDINGEN", id),
                    "Bij types 'Ongeval met verzuim' en 'Ongeval zonder verzuim' zijn de memo velden 'Oorzaak', 'Maatregelen' en 'Lange termijn maatregelen' verplicht.", "");
                return;
            }
        }


        rsSafeteReport.Fields["ISOUTSTANDING"].Value = false;
        rsSafeteReport.Fields["DATUMOPGELOST"].Value = DateTime.Now;
        rsSafeteReport.Update();
    }
}
