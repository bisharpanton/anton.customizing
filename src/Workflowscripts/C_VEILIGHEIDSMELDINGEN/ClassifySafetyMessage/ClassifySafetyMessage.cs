﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class ClassifySafetyMessage : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //22-01-2019
        //Sla ingegeven parameters op in huidige veiligheidsmelding

        int classification = Convert.ToInt32(Parameters["Classification"].Value);
        int safetyAwareness = Convert.ToInt32(Parameters["SafetyAwareness"].Value);

        if (classification == 1 || safetyAwareness == 1)
        {
            Log.Error("Error", "Ongeldige invoer", "");
            return;
        }

        var rsSafetyMessage = GetRecordset("C_VEILIGHEIDSMELDINGEN", "CLASSIFICATION, SAFETYAWARENESS",
            $"PK_C_VEILIGHEIDSMELDINGEN = {id}", "");
        rsSafetyMessage.MoveFirst();
        
        rsSafetyMessage.Fields["CLASSIFICATION"].Value = classification ;
        rsSafetyMessage.Fields["SAFETYAWARENESS"].Value = safetyAwareness;
        rsSafetyMessage.Update();

    }
}
