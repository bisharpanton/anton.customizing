﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class SafetyReportLogInfo : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        var rsSafeteReport = GetRecordset("C_VEILIGHEIDSMELDINGEN", "DATUMOPGELOST, INCIDENTREPORTING, ISOUTSTANDING",
            $"PK_C_VEILIGHEIDSMELDINGEN = {id}", "");
        rsSafeteReport.MoveFirst();

        rsSafeteReport.Fields["ISOUTSTANDING"].Value = false;
        rsSafeteReport.Update();

    }
}
