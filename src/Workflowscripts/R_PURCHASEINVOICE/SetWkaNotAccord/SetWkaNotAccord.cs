﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class SetWkaNotAccord : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //5-2-2019
        //Zet WKA-status niet akkoord

        var rsPurchaseInvoice = GetRecordset("R_PURCHASEINVOICE", "WKASTATE, WKADATECHANGED, FK_WKACHANGEDBY",
            $"PK_R_PURCHASEINVOICE = {id}", "");
        rsPurchaseInvoice.MoveFirst();

        rsPurchaseInvoice.Fields["WKASTATE"].Value = WkaState.NietAkkoord;
        rsPurchaseInvoice.Fields["WKADATECHANGED"].Value = DateTime.Now;
        rsPurchaseInvoice.Fields["FK_WKACHANGEDBY"].Value = CurrentUser.EmployeeID;
        rsPurchaseInvoice.Update();

    }

    private enum WkaState : int
    {
        Nvt = 1,
        NogTeControleren = 2,
        Akkoord = 3,
        NietAkkoord = 4
    }
}
