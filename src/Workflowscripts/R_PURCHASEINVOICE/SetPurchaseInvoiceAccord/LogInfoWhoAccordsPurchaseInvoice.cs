﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class LogInfoWhoAccordsPurchaseInvoice : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //09-03-2016
        //Log gegevens wie de workflow uitvoert
		
        ScriptRecordset rs = this.GetRecordset("R_PURCHASEINVOICE", "FK_CREATEDBY, FK_INVOICEAUDITOR, DATEACCORD, AKKOORDVANUITIC", string.Format("PK_R_PURCHASEINVOICE = {0}", this.RecordId), "");
        rs.MoveFirst();
		
        rs.Fields["DATEACCORD"].Value = System.DateTime.Now;

        if (CurrentUser.UserName == "SCANSYS" || (bool)rs.Fields["AKKOORDVANUITIC"].Value)
        {
            rs.Fields["FK_INVOICEAUDITOR"].Value = rs.Fields["FK_CREATEDBY"].Value;
        }
        else if (CurrentUser.UserName == "SDK_ADMIN")
        {
            //Niets doen, gewoon huidige factuurcontroleur laten staan
        }
        else
        {
            rs.Fields["FK_INVOICEAUDITOR"].Value = CurrentUser.EmployeeID;
        }

        rs.Update();

    }
}
