﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class CloseDeclarationTodo : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //10-12-2019
        //Sluit de declaratie-taak indien aanwezig

        //Niets doen indien de workflow 'Zet status naar Akkoord' vanuit het afhandelen van de declaratie taak wordt aangeroepen.
        //Anders lopen we in een loop
        if (Convert.ToBoolean(Parameters["ExecutedFromSetDeclarationTodoDone"].Value))
        {
            return;
        }

        var wfStateClosed = new Guid("43ea659f-14bb-473a-869b-819c63ef43ac");
        var rsDeclarationTodo = GetRecordset("R_TODO", "PK_R_TODO",
                $"FK_PURCHASEINVOICE = {id} AND ISDECLARATIONTODO = 1 AND FK_WORKFLOWSTATE <> '{wfStateClosed}'", "");

        if (rsDeclarationTodo.RecordCount == 0)
        {
            return;
        }

        var declarationTodo= rsDeclarationTodo.DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_TODO")).FirstOrDefault();
        
        var wfToDone = new Guid("d94de796-4b24-44ff-9c66-230d1511d144");

        var result = ExecuteWorkflowEvent("R_TODO", declarationTodo, wfToDone, null);

        if (result.HasError)
        {
            Log.Info("Info", $"Afsluiten declaratie taak is mislukt; oorzaak: {result.GetResult()}", "");
        }
    }
}
