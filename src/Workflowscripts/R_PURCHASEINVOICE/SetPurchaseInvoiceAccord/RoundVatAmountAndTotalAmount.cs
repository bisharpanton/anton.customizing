﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class RoundVatAmountAndTotalAmount : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //15-08-2019
        //Vanuit IC komen de het btw-bedrag en het totaalbedrag met meer dan 2 cijfers achter de komma binnen.
        //Hierdoor gaat de export naar Snelstart onderuit. Bij deze actie ronden we deze twee bedragen af.

        //Dit script staat ook bij de workflow 'Controleer factuur'

        var decimalsHomeCurrency = GetDecimalsHomeCurrency();

        var sources = new string[] {"ITEM", "MISC", "OUTSOURCED"};

        foreach (var source in sources)
        {
            var rsPurchaseInvoiceDetail = GetRecordset($"R_PURCHASEINVOICEDETAIL{source}",
                "NETPURCHASEPRICE, VATAMOUNT, TOTALAMOUNT",
                $"FK_PURCHASEINVOICE = {id}", "");

            if (rsPurchaseInvoiceDetail.RecordCount == 0)
            {
                continue;
            }

            rsPurchaseInvoiceDetail.UpdateWhenMoveRecord = false;
            rsPurchaseInvoiceDetail.UseDataChanges = true;

            rsPurchaseInvoiceDetail.MoveFirst();
            while (!rsPurchaseInvoiceDetail.EOF)
            {
                if (Math.Abs((double)rsPurchaseInvoiceDetail.Fields["VATAMOUNT"].Value -
                             Math.Round((double)rsPurchaseInvoiceDetail.Fields["VATAMOUNT"].Value, decimalsHomeCurrency)) >= 0.00000001)
                {
                    rsPurchaseInvoiceDetail.Fields["VATAMOUNT"].Value =
                        Math.Round((double)rsPurchaseInvoiceDetail.Fields["VATAMOUNT"].Value, decimalsHomeCurrency);
                }

                rsPurchaseInvoiceDetail.MoveNext();
            }


            rsPurchaseInvoiceDetail.MoveFirst();
            rsPurchaseInvoiceDetail.Update();
        }
    }

    private int GetDecimalsHomeCurrency()
    {
        var homeCurrency = GetRecordset("R_FINANCIALSETTINGS", "FK_DEFAULTCURRENCY", "", "")
            .DataTable.AsEnumerable().First().Field<Guid>("FK_DEFAULTCURRENCY");

        return GetRecordset("R_CURRENCY", "DECIMALS",
            $"PK_R_CURRENCY = '{homeCurrency}'", "").DataTable.AsEnumerable().First().Field<int>("DECIMALS");
    }
}
