﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.CRM;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class CheckLedgerAccountForOrderOrDestinationOrder : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        if(IgnoreBlockade(id))
        {
            return;
        }

        var generalLedgerAccounts = GetRecordset("R_GENERALLEDGERACCOUNT", "GENERALLEDGERACCOUNTNUMBER",
            $"", "").DataTable.AsEnumerable().Select(x => new GeneralLedgerAccount()
            {
                Id = x.Field<int>("PK_R_GENERALLEDGERACCOUNT"),
                Number = x.Field<string>("GENERALLEDGERACCOUNTNUMBER")
            }).ToList();


        var allReservations = GetRecordset($"R_ALLRESERVATION", $"FK_ORDER, FK_PURCHASEINVOICEDETAILITEM, FK_PURCHASEINVOICEDETAILMISC, FK_PURCHASEINVOICEDETAILOUTSOURCED",
                $"FK_PURCHASEINVOICE = {id}", "").DataTable.AsEnumerable().ToList();


        var sources = new string[3] { "ITEM", "MISC", "OUTSOURCED" };

        foreach (var source in sources)
        {
            var columnNameFkItem = source.Equals("ITEM") ? ", FK_ITEM" : "";
            var purchaseInvoiceDetails = GetRecordset($"R_PURCHASEINVOICEDETAIL{source}", $"FK_GENERALLEDGERACCOUNT, DIRECTTOORDER, DESCRIPTION {columnNameFkItem}",
                $"FK_PURCHASEINVOICE = {id}", "").DataTable.AsEnumerable().ToList();

            if(!purchaseInvoiceDetails.Any())
            {
                continue;
            }


            foreach (var purchaseInvoiceDetail in purchaseInvoiceDetails)
            {
                var allReservationsThisPurchaseInvoiceDetail = allReservations
                    .Where(x => x.Field<int?>($"FK_PURCHASEINVOICEDETAIL{source}") == purchaseInvoiceDetail.Field<int>($"PK_R_PURCHASEINVOICEDETAIL{source}")).ToList();

                var purchaseInvoiceDetailIsDestinatedToOrder = allReservationsThisPurchaseInvoiceDetail.Any(x => x.Field<int?>("FK_ORDER").HasValue) || purchaseInvoiceDetail.Field<bool>("DIRECTTOORDER");
                var isInventoryItem = DetermineIsInventoryItem(source, purchaseInvoiceDetail);

                if (purchaseInvoiceDetailIsDestinatedToOrder && !isInventoryItem && !purchaseInvoiceDetail.Field<int?>("FK_GENERALLEDGERACCOUNT").HasValue)
                {
                    Log.Error("Error", $"Inkoopfactuurregel '{purchaseInvoiceDetail.Field<string>("DESCRIPTION")}' is bestemd voor order en moet daarom op een 7xxx grootboekrekening worden gekoppeld.", "");
                    return;
                }  
                
                if(purchaseInvoiceDetail.Field<int?>("FK_GENERALLEDGERACCOUNT").HasValue)
                {
                    var generalLedgerAccount = generalLedgerAccounts.First(x => x.Id == purchaseInvoiceDetail.Field<int>("FK_GENERALLEDGERACCOUNT"));

                    if (purchaseInvoiceDetailIsDestinatedToOrder && !isInventoryItem && !generalLedgerAccount.Number.StartsWith("7"))
                    {
                        Log.Error("Error", $"Inkoopfactuurregel '{purchaseInvoiceDetail.Field<string>("DESCRIPTION")}' is bestemd voor order en moet daarom op een 7xxx grootboekrekening worden gekoppeld.", "");
                        return;
                    }

                    if (!purchaseInvoiceDetailIsDestinatedToOrder && generalLedgerAccount.Number.StartsWith("7"))
                    {
                        Log.Error("Error", $"Foutieve grootboekrekening bij inkoopfactuurregel '{purchaseInvoiceDetail.Field<string>("DESCRIPTION")}'. Een 7xxx grootboekrekening mag alleen worden gebruikt als de inkoop bestemd is voor order.", "");
                        return;
                    }
                }
            }
        }
    }

    private bool DetermineIsInventoryItem(string source, DataRow purchaseInvoiceDetail)
    {
        if(!source.Equals("ITEM"))
        {
            return false;
        }

        return GetRecordset("R_ITEM", "INVENTORYITEM", $"PK_R_ITEM = {purchaseInvoiceDetail.Field<int>("FK_ITEM")}", "")
            .DataTable.AsEnumerable().First().Field<bool>("INVENTORYITEM");
    }

    private bool IgnoreBlockade(int id)
    {
        return GetRecordset("R_PURCHASEINVOICE", "IGNOREBLOCKADELEDGER",
            $"PK_R_PURCHASEINVOICE = {id}", "").DataTable.AsEnumerable().First().Field<bool>("IGNOREBLOCKADELEDGER");
    }


    public class GeneralLedgerAccount
    {
        public int Id { get; set; }
        public string Number { get; set; }
    }
}
