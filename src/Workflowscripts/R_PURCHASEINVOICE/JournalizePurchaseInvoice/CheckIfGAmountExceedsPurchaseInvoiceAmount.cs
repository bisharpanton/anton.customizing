﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class CheckIfGAmountExceedsPurchaseInvoiceAmount : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        var rsPurchaseInvoice = GetRecordset("R_PURCHASEINVOICE", "AMOUNTGACCOUNT, INVOICEAMOUNTEXVAT",
            $"PK_R_PURCHASEINVOICE = {id}", "");
        rsPurchaseInvoice.MoveFirst();

        if ((double)rsPurchaseInvoice.Fields["AMOUNTGACCOUNT"].Value == 0.0)
        {
            return;
        }

        if (Math.Abs((double)rsPurchaseInvoice.Fields["AMOUNTGACCOUNT"].Value) >
            Math.Abs((double)rsPurchaseInvoice.Fields["INVOICEAMOUNTEXVAT"].Value))
        {
            Log.Error("Error", $"Bedrag G-rekening mag niet groter zijn dan Factuurbedrag excl. BTW", "");
            return;
        }
    }
}
