﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class CloseDeclarationTodo2 : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //10-12-2019
        //Sluit de declaratie-taak indien aanwezig

        //Niets doen indien deze workflow vanuit het afhandelen van de declaratie taak wordt aangeroepen.
        //Anders lopen we in een loop
        if (Convert.ToBoolean(Parameters["ExecutedFromSetDeclarationTodoDone"].Value))
        {
            return;
        }

        var wfStateOpen = new Guid("82100d7d-d342-4fe4-922c-be7002fe8844");
        var rsDeclarationTodo = GetRecordset("R_TODO", "PK_R_TODO",
            $"FK_PURCHASEINVOICE = {id} AND ISDECLARATIONTODO = 1 AND FK_WORKFLOWSTATE = '{wfStateOpen}'", "");

        if (rsDeclarationTodo.RecordCount == 0)
        {
            return;
        }

        var declarationTodo= rsDeclarationTodo.DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_TODO")).FirstOrDefault();
        
        var wfToReject = new Guid("8df4a1fb-9829-402f-8fbe-f3342da4b9f9");

        var result = ExecuteWorkflowEvent("R_TODO", declarationTodo, wfToReject, null);

        if (result.HasError)
        {
            Log.Info("Info", $"Afkeuren declaratie taak is mislukt; oorzaak: {result.GetResult()}", "");
        }
    }
}
