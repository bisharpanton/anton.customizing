﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class ApprovePaymentBatch : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        var rsPaymentBatch = GetRecordset("U_PAYMENTBATCH", "APPROVED, APPROVEDBY, DATEAPPROVED",
                $"PK_U_PAYMENTBATCH = {id}", "");
        rsPaymentBatch.MoveFirst();

        rsPaymentBatch.SetFieldValue("APPROVED", true);
        rsPaymentBatch.SetFieldValue("APPROVEDBY", GetUserInfo().CurrentUserName);
        rsPaymentBatch.SetFieldValue("DATEAPPROVED", DateTime.Now);

        var updateResult = rsPaymentBatch.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            Log.Error("Error", $"Akkoord geven betalingsbatch mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "");
        } 

    }
}
