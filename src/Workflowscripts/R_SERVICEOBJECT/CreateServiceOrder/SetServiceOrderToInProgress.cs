﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class SetServiceOrderToInProgress : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //25-5-2021
        //Een service order is altijd direct onderhanden daarom direct order handen handen zetten (in opdracht van LPS gemaakt)

        var createdJoborder = GetRecordset("R_JOBORDER", "FK_ORDER, DATECREATED",
            $"FK_SERVICEOBJECT = {id}", "DATECREATED DESC").DataTable.AsEnumerable().First();

        if((DateTime)createdJoborder["DATECREATED"] < DateTime.Now.AddMinutes(-1))
        {
            return; //Er gaat iets mis, de laatst aangemaakte bon bij dit object is niet in de afgelopen minuut aangemaakt
        }

        var orderId = (int)createdJoborder["FK_ORDER"];

        if (CheckWorkflowstateIsNew(orderId))
        {
            var wfSetStateToInprogress = new Guid("811f0af2-261e-47f6-9e53-1f1213770aee");
            ExecuteWorkflowEvent("R_ORDER", orderId, wfSetStateToInprogress, null);
        }

        var rsOrder = GetRecordset("R_ORDER", "SERVICEORDER",
            $"PK_R_ORDER = {orderId}", "");
        rsOrder.MoveFirst();
        rsOrder.SetFieldValue("SERVICEORDER", true);
        rsOrder.Update();
    }

    private bool CheckWorkflowstateIsNew(int orderId)
    {
        var wfStateNew = new Guid("6205d9d2-72d7-49df-8b4b-6e5c1c3d4927");
        var wfSateOrder = GetWfStateOrder(orderId);

        return wfStateNew == wfSateOrder ? true : false;

    }

    private Guid GetWfStateOrder(int orderId)
    {
        return GetRecordset("R_ORDER", "FK_WORKFLOWSTATE", $"PK_R_ORDER = {orderId}", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<Guid>("FK_WORKFLOWSTATE"))
            .First();
    }

    private int GetOrderId(int joborderId)
    {
        return GetRecordset("R_JOBORDER", "FK_ORDER", $"PK_R_JOBORDER = {joborderId}", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int>("FK_ORDER"))
            .First();
    }
}
