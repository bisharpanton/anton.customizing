﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class CheckForDocuments : WorkflowScriptInfo
{
    public void Execute()
    {
        // HVE
        // 23-04-2021
        // Check if there are documents
        var purchaseContractId = (int)RecordId;

        var linkedDocuments = GetRecordset("C_PURCHASECONTRACTDOCUMENT", "PK_C_PURCHASECONTRACTDOCUMENT",
                $"FK_PURCHASECONTRACT = {purchaseContractId}", "")
            .DataTable
            .AsEnumerable();

        if (!linkedDocuments.Any())
        {
            Log.Error(GetRecordTag("C_PURCHASECONTRACTDOCUMENT", purchaseContractId),
                "Er zijn geen documenten aanwezig, voeg het getekende document toe aan het inkoopcontract.",
                "Inkoopcontract getekend retour");
        }
    }
}
