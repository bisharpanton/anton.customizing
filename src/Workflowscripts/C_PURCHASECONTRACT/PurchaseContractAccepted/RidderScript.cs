﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using ADODB;
using Ridder.Client.SDK.SDKParameters;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;
using Ridder.Recordset.Extensions;
using Workflowscripts.C_PURCHASECONTRACT.PurchaseContractAccepted.Models;

public class Ridderscript : WorkflowScriptInfo
{
    public void Execute()
    {
        // HVE
        // 1-6-2021
        // Create purchaseorder for purchase contract
        var purchaseContractId = (int)RecordId;
        var recordTag = GetRecordTag("C_PURCHASECONTRACT", purchaseContractId);
        var purchaseContract = GetRecordset("C_PURCHASECONTRACT", "",
                $"PK_C_PURCHASECONTRACT = {purchaseContractId}", "")
            .As<PurchaseContract>()
            .First();
        var purchaseSettings = GetRecordset("R_PURCHASESETTINGS", "",
                $"PK_R_PURCHASESETTINGS = 1", "")
            .As<PurchaseSettings>()
            .First();

        if (purchaseSettings.FK_DEFAULTTYPEPURCHASE == null
        || purchaseSettings.FK_DEFAULTMISC == null)
        {
           Log.Error(recordTag , $"Stel het Standaard type opdracht en Divers tbv inkoopcontract in bij de Instellingen Inkoop." + 
                "Inkooporder niet aan te maken", "Inkoopcontract akkoord");
            return;
        }

        var purchaseOrder = CreatePurchase(purchaseContract, purchaseSettings);
        if (purchaseOrder.PK_R_PURCHASEORDER == 0)
        {
            return;
        }

        //TODO: Update state purchase order
        var wf_SetPurchaseOrderOrdered = new Guid("324de220-9561-40ce-8d50-9ff207dee26a");
        var resultPurchaseOrdered = ExecuteWorkflowEvent("R_PURCHASEORDER", purchaseOrder.PK_R_PURCHASEORDER,
            wf_SetPurchaseOrderOrdered, null);
        if (resultPurchaseOrdered != null && resultPurchaseOrdered.HasError)
        {
            Log.Error(recordTag,
                $"Inkooporder ontvangen melden mislukt, oorzaak: {resultPurchaseOrdered.GetResult()}",
                "Mail Prestatieverklaring");
            return;
        }

        var wf_SetStatePurchaseContractApproved = new Guid("4f2799cd-2bcb-4f63-a18b-1dffd8fe23e4");
        ExecuteWorkflowEvent("C_PURCHASECONTRACT", purchaseContractId, wf_SetStatePurchaseContractApproved, null);
    }

    private PurchaseOrder CreatePurchase(PurchaseContract purchaseContract, PurchaseSettings purchaseSettings)
    {
        var purchaseOrder = CreateNewPurchaseOrder(purchaseContract);
        if (purchaseOrder.PK_R_PURCHASEORDER == 0)
        {
            return purchaseOrder;
        }
        CreatePurchaseDetail(purchaseOrder, purchaseContract, purchaseSettings);

        return purchaseOrder;
    }

    private void CreatePurchaseDetail(PurchaseOrder purchaseOrder, PurchaseContract purchaseContract, PurchaseSettings purchaseSettings)
    {
        var purchaseOrderDetail = GetRecordset("R_PURCHASEORDERDETAILMISC", "",
            $"PK_R_PURCHASEORDERDETAILMISC IS NULL", "");
        purchaseOrderDetail.UseDataChanges = true;
        purchaseOrderDetail.AddNew();
        purchaseOrderDetail.SetFieldValue("FK_PURCHASEORDER", purchaseOrder.PK_R_PURCHASEORDER);
        purchaseOrderDetail.SetFieldValue("FK_PURCHASECONTRACT", purchaseContract.PK_C_PURCHASECONTRACT);
        purchaseOrderDetail.SetFieldValue("FK_TYPEPURCHASEORDER", purchaseSettings.FK_DEFAULTTYPEPURCHASE);
        purchaseOrderDetail.SetFieldValue("FK_MISC", purchaseSettings.FK_DEFAULTMISC);
        purchaseOrderDetail.SetFieldValue("QUANTITY", 1);
        purchaseOrderDetail.SetFieldValue("DESCRIPTION", $"{purchaseContract.DESCRIPTION} - {purchaseContract.CONTRACTNUMBER}");
        purchaseOrderDetail.SetFieldValue("DIRECTTOORDER", true);
        purchaseOrderDetail.SetFieldValue("FK_ORDER", purchaseContract.FK_ORDER);
        purchaseOrderDetail.SetFieldValue("GROSSPURCHASEPRICE", purchaseContract.AMOUNT);
        purchaseOrderDetail.SetFieldValue("NETPURCHASEPRICE", purchaseContract.AMOUNT);
        purchaseOrderDetail.Update();

    }

    private PurchaseOrder CreateNewPurchaseOrder(PurchaseContract purchaseContract)
    {
        var rsPurchaseOrder = GetRecordset("R_PURCHASEORDER", "",
            $"PK_R_PURCHASEORDER IS NULL", "");
        rsPurchaseOrder.UseDataChanges = true;

        rsPurchaseOrder.AddNew();
        rsPurchaseOrder.SetFieldValue("FK_DELIVERYRELATION", GetOwnCompany());
        rsPurchaseOrder.SetFieldValue("FK_SUPPLIER", (int)purchaseContract.FK_SUPPLIER);
        rsPurchaseOrder.SetFieldValue("FK_CONTACT", purchaseContract.FK_CONTACT);
        rsPurchaseOrder.SetFieldValue("FK_PURCHASER", (int)purchaseContract.FK_EMPLOYEE);
        rsPurchaseOrder.SetFieldValue("REFERENCESUPPLIER",
            $"{purchaseContract.DESCRIPTION} - {purchaseContract.CONTRACTNUMBER}");
        if ((purchaseContract.FK_INVOICESCHEDULE as int? ?? 0) > 0)
        {
            rsPurchaseOrder.SetFieldValue("FK_INVOICESCHEDULE", purchaseContract.FK_INVOICESCHEDULE);
        }

        var result = rsPurchaseOrder.Update2();
        if (result != null && result.Any(x => x.HasError))
        {
           Log.Error(purchaseContract.CONTRACTNUMBER,
               $"Het opslaan is mislukt, oorzaak: {string.Join("\r\n", result.Where(y => y.HasError).Select(x => x.GetResult()))}",
                "Ridder iQ");
            return new PurchaseOrder();
        }

        var purchaseOrderId = (int)result.Select(x => x.PrimaryKey).First();
        return GetRecordset("R_PURCHASEORDER", "", $"PK_R_PURCHASEORDER = {purchaseOrderId}", "")
            .As<PurchaseOrder>()
            .First();
    }

    private int GetOwnCompany()
    {
       return GetRecordset("R_CRMSETTINGS", "FK_OWNCOMPANY", "", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int>("FK_OWNCOMPANY"))
            .First();

    }
}
