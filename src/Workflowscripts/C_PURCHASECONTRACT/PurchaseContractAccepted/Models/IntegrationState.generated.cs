namespace Workflowscripts.C_PURCHASECONTRACT.PurchaseContractAccepted.Models
{
    using System;
    using System.ComponentModel;
    
    
    public enum IntegrationState : int
    {
        
        /// <summary>
        /// New
        /// </summary>
        [Description("New")]
        New = 1,
        
        /// <summary>
        /// Message sent
        /// </summary>
        [Description("Message sent")]
        Message_sent = 2,
        
        /// <summary>
        /// Response processed
        /// </summary>
        [Description("Response processed")]
        Response_processed = 3,
        
        /// <summary>
        /// Not processed
        /// </summary>
        [Description("Not processed")]
        Not_processed = 4,
    }
}
