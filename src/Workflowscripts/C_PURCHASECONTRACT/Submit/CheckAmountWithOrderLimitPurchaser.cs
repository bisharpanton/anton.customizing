﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class CheckAmountWithOrderLimitPurchaser : WorkflowScriptInfo
{
    public void Execute()
    {
        // HVE
        // 1-6-2021
        // Check amount with buylimit purchaser
        var purchasecontractId = (int)RecordId;
        var recordTag = GetRecordTag("C_PURCHASECONTRACT", purchasecontractId);
        var purchasecontract = GetRecordset("C_PURCHASECONTRACT", "", $"PK_C_PURCHASECONTRACT = {purchasecontractId}", "")
            .DataTable
            .AsEnumerable()
            .First();

        var wf_Fiat = new Guid("482484af-515a-41d6-9b46-0e52d4aec0ac");
        if (purchasecontract.Field<Guid>("FK_WORKFLOWSTATE") == wf_Fiat)
        {
            return;
        }

        var purchaserId = purchasecontract.Field<int?>("FK_EMPLOYEE") ?? 0;
        if (purchaserId == 0)
        {
            Log.Error(recordTag,"Er is geen inkoper geselecteerd. Selecteer eerst een inkoper.", "Inkoopcontract uitbrengen");
            return;
        }

        var purchaseLimit = DeterminePurchaseLimit(purchaserId);
        var amount = purchasecontract.Field<double>("AMOUNT");

        if (amount > purchaseLimit)
        {
            Log.Error(recordTag, "Het bedrag in het inkoopcontract valt buiten het bestellimiet van de inkoper. Inkoopcontract dient eerst gefiatteerd te worden.",
                "Inkoopcontract uitbrengen");
            return;
        }
    }

    private double DeterminePurchaseLimit(int purchaserId)
    {
        return GetRecordset("R_EMPLOYEE", "C_BESTELLIMIET", $"PK_R_EMPLOYEE = {purchaserId}", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<double?>("C_BESTELLIMIET") ?? 0d)
            .FirstOrDefault();
    }
}
