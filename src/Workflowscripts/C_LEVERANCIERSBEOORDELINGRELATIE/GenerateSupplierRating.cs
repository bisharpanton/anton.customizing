﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class GenerateSupplierRating : WorkflowScriptInfo
{
    //private static string _sqlDataSource = @"LT115\RIDDERIQ"; //Lokaal testen
    private static readonly string _sqlDataSource = @"SRV-APP03\RIDDERIQ"; //Live bij Anton

    readonly string _connectionString = $@"Data Source={_sqlDataSource};Initial Catalog=Anton Groep;User Id=sa;Password=Welkom@ridderiq";

    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //24-03-2016
        //Haal template uit bedrijf Anton Groep en kopieer van daaruit de vragen

        var rows = new List<TemplateRow>();

        try
        {
            rows = GetRowsFromAntonGroep();
        }
        catch (Exception e)
        {
            Log.Info("Info", $"Uitlezen template Anton Groep mislukt; oorzaak {e}", "");
            throw;
        }

        var language = DetermineSystemLanguageAtRelation(id);

        var rsLeveranciersbeoordeling =
            GetRecordset("C_LEVERANCIERSBEOORDELING", "", "PK_C_LEVERANCIERSBEOORDELING = -1", "");

        foreach (var row in rows.GroupBy(x => x.PkTemplateRow))
        {
            var rowsThisGroup = rows.Where(y => y.PkTemplateRow == row.Key);

            rsLeveranciersbeoordeling.AddNew();

            rsLeveranciersbeoordeling.Fields["FK_LEVERANCIERSBEOORDELINGRELATIE"].Value = id;
            rsLeveranciersbeoordeling.Fields["CATEGORY"].Value = rowsThisGroup.First().Category;
            rsLeveranciersbeoordeling.Fields["QUESTION"].Value = DetermineLanguage(rowsThisGroup, language);
            rsLeveranciersbeoordeling.Fields["WEIGHTINGFACTOR"].Value = rowsThisGroup.First().WeightFactor;
            rsLeveranciersbeoordeling.Fields["NEGATIVESCORE"].Value = rowsThisGroup.First().NegativeFactor;
        }
        
        if (rsLeveranciersbeoordeling.RecordCount > 0)
        {
            rsLeveranciersbeoordeling.MoveFirst();
            rsLeveranciersbeoordeling.Update();
        }
    }

    private string DetermineLanguage(IEnumerable<TemplateRow> rowsThisGroup, int languageRelation)
    {
        if(rowsThisGroup.Any(x => x.FkLanguage == languageRelation))
        {
            return rowsThisGroup.First(x => x.FkLanguage == languageRelation).Question;
        }
        else if (rowsThisGroup.Any(x => x.FkLanguage == 2)) //Engels
        {
            return rowsThisGroup.First(x => x.FkLanguage == 2).Question;
        }
        else if (rowsThisGroup.Any(x => x.FkLanguage == 1)) //NL
        {
            return rowsThisGroup.First(x => x.FkLanguage == 1).Question;
        }

        return "";
    }

    private int DetermineSystemLanguageAtRelation(int leveranciersBeoordelingId)
    {
        var relation = GetRecordset("C_LEVERANCIERSBEOORDELINGRELATIE", "FK_RELATION",
                $"PK_C_LEVERANCIERSBEOORDELINGRELATIE = {leveranciersBeoordelingId}", "").DataTable.AsEnumerable()
            .First()
            .Field<int>("FK_RELATION");

        var languageId =  GetRecordset("R_RELATION", "FK_LANGUAGE", $"PK_R_RELATION = {relation}", "")
            .DataTable.AsEnumerable().First().Field<int>("FK_LANGUAGE");

        return GetRecordset("R_LANGUAGE", "SYSTEMLANGUAGE", $"PK_R_LANGUAGE = {languageId}", "")
            .DataTable.AsEnumerable().First().Field<int>("SYSTEMLANGUAGE");
    }

    private List<TemplateRow> GetRowsFromAntonGroep()
    {
        var result = new List<TemplateRow>();

        using (var conn = new System.Data.SqlClient.SqlConnection(_connectionString))
        {
            conn.Open();

            string sqlSelectTemplate = @"
				SELECT TLB.PK_C_TEMPLATELEVERANCIERSBEOORDELING, TLB.CATEGORY, L.VALUE, L.FK_LANGUAGE, TLB.WEIGHTINGFACTOR, TLB.NEGATIVESCORE
				FROM [Anton Groep].dbo.C_TEMPLATELEVERANCIERSBEOORDELING AS TLB
                INNER JOIN [Anton Groep].dbo.L_C_TEMPLATELEVERANCIERSBEOORDELING_QUESTION AS L 
				ON TLB.PK_C_TEMPLATELEVERANCIERSBEOORDELING = L.FK_QUESTION
				ORDER BY TLB.PK_C_TEMPLATELEVERANCIERSBEOORDELING;";

            System.Data.SqlClient.SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand(sqlSelectTemplate, conn);
            //sqlCmd.Parameters.Add(new SqlParameter("@QuoteNr", offerNumber));

            System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(sqlCmd);
            DataTable dtSelectTemplate = new DataTable("Template");
            da.Fill(dtSelectTemplate);

            foreach (DataRow row in dtSelectTemplate.Rows)
            {
                result.Add(new TemplateRow()
                {
                    PkTemplateRow = (int)row["PK_C_TEMPLATELEVERANCIERSBEOORDELING"],
                    Category = (int)row["CATEGORY"],
                    Question = row["VALUE"].ToString(),
                    FkLanguage = (int)row["FK_LANGUAGE"],
                    WeightFactor = (int)row["WEIGHTINGFACTOR"],
                    NegativeFactor = (bool)row["NEGATIVESCORE"],
                });
            }

            da = null;
            sqlCmd = null;

            conn.Close();
        }

        return result;
    }

    class TemplateRow
    {
        public int PkTemplateRow { get; set; }
        public int Category { get; set; }
        public string Question { get; set; }
        public int FkLanguage { get; set; }
        public int WeightFactor { get; set; }
        public bool NegativeFactor { get; set; }
    }
}
