﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;

public class CreateNewSalesTodoFromTemplate : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //13-1-2025
        //Maak nieuwe sales taak aan

        var todo = GetRecordset("R_TODO", "", $"PK_R_TODO = {id}", "")
            .DataTable.AsEnumerable().First();

        if(!(todo.Field<int?>("FK_TEMPLATESALESTODO").HasValue))
        {
            return; //Geen sales taak van toepassing
        }

        var currentTemplateSalesTodo = GetRecordset("U_TEMPLATESALESTODOS", "SEQUENCENUMBER, WAITINGTIME",
            $"PK_U_TEMPLATESALESTODOS = {todo.Field<int>("FK_TEMPLATESALESTODO")}", "")
            .DataTable.AsEnumerable().First();
        
        var currentSequence = currentTemplateSalesTodo.Field<int>("SEQUENCENUMBER");
        var waitingTime = currentTemplateSalesTodo.Field<int>("WAITINGTIME");

        var nextStepSalesTodoTemplate = GetRecordset("U_TEMPLATESALESTODOS", "", $"SEQUENCENUMBER > {currentSequence}", "SEQUENCENUMBER ASC")
            .DataTable.AsEnumerable().AsEnumerable().FirstOrDefault();

        if (nextStepSalesTodoTemplate == null)
        {
            //Log.Error("Error", "Geen volgende sales taak gevonden in het Sales taken template.", "");
            return;
        }

        var rsTodo = GetRecordset("R_TODO", "", "PK_R_TODO = NULL", "");
        rsTodo.AddNew();
        rsTodo.SetFieldValue("FK_RELATION", todo.Field<int>("FK_RELATION"));

        if(todo.Field<int?>("FK_CONTACT").HasValue)
        {
            rsTodo.SetFieldValue("FK_CONTACT", todo.Field<int>("FK_CONTACT"));
        }
        
        rsTodo.SetFieldValue("FK_TODOTYPE", nextStepSalesTodoTemplate.Field<int>("FK_TODOTYPE"));
        rsTodo.SetFieldValue("DESCRIPTION", nextStepSalesTodoTemplate.Field<string>("DESCRIPTION"));
        rsTodo.SetFieldValue("FK_TEMPLATESALESTODO", nextStepSalesTodoTemplate.Field<int>("PK_U_TEMPLATESALESTODOS"));
        rsTodo.SetFieldValue("DUEDATE", DetermineDueDate(waitingTime));
        rsTodo.SetFieldValue("FK_ASSIGNEDTO", todo.Field<int>("FK_ASSIGNEDTO"));
        rsTodo.SetFieldValue("FK_ACTIONSCOPE", todo.Field<int>("FK_ACTIONSCOPE"));

        rsTodo.Update();
    }

    private DateTime DetermineDueDate(int waitingTime)
    {
        var newDate = DateTime.Now.AddDays(waitingTime);    

        if(newDate.DayOfWeek == DayOfWeek.Saturday)
        {
            return newDate.AddDays(2);
        }
        else if(newDate.DayOfWeek == DayOfWeek.Sunday)
        {
            return newDate.AddDays(1);
        }
        else
        {
            return newDate;
        }
    }
}
