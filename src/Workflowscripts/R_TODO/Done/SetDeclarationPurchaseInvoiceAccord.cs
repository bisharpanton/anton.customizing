﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class SetDeclarationPurchaseInvoiceAccord : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //13-5-2020
        //Indien er een declaratie taak goed gekeurd wordt, ga naar de inkoopfactuur en zet daar de wf-status door naar 'Akkoord'

        var todo = GetRecordset("R_TODO", "ISDECLARATIONTODO, FK_PURCHASEINVOICE",
            $"PK_R_TODO = {id}", "").DataTable.AsEnumerable().First();

        if (!todo.Field<bool>("ISDECLARATIONTODO"))
        {
            return;
        }

        var purchaseInvoiceId = todo.Field<int?>("FK_PURCHASEINVOICE") ?? 0;

        if (purchaseInvoiceId == 0)
        {
            return; //Geen factuur gekoppeld
        }

        var purchaseInvoiceWorkflowState = GetRecordset("R_PURCHASEINVOICE", "FK_WORKFLOWSTATE",
                $"PK_R_PURCHASEINVOICE = {purchaseInvoiceId}", "")
            .DataTable.AsEnumerable().First().Field<Guid>("FK_WORKFLOWSTATE");

        var wfStateNew = new Guid("466c4d1e-21ce-47bb-a2c6-8b96c6ec0d23");
        var wfStateBlocked = new Guid("23dd10a0-492d-4c1b-aae6-a661160d3ffe");

        if (purchaseInvoiceWorkflowState != wfStateNew && purchaseInvoiceWorkflowState != wfStateBlocked)
        {
            return;
        }

        Guid wfSetToAccord = new Guid("0075174d-1a1a-4525-b8f4-06c267d803d2");

        var parameters = new Dictionary<string, object>();
        parameters.Add("ExecutedFromSetDeclarationTodoDone", true);

        var result = ExecuteWorkflowEvent("R_PURCHASEINVOICE",
            purchaseInvoiceId, wfSetToAccord, parameters);

        if (result.HasError)
        {
            Log.Error(GetRecordTag("R_TODO", id), 
                $"Accorderen declaratie inkoopfactuur is mislukt; oorzaak: {result.GetResult()}", "");
            return;
        }
    }
}
