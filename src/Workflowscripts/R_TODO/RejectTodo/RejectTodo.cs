﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class RejectTodo : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //13-5-2020
        //Indien er een declaratie taak goed gekeurd wordt, ga naar de inkoopfactuur en zet daar de wf-status door naar 'Geblokkerd'
        //en log gegevens in de taak

        var rsTodo = GetRecordset("R_TODO", "ISDECLARATIONTODO, FK_PURCHASEINVOICE, FK_REJECTEDBY, DATEREJECTED",
            $"PK_R_TODO = {id}", ""); 
        rsTodo.MoveFirst();
        
        if (!(bool)rsTodo.GetField("ISDECLARATIONTODO").Value)
        {
            return;
        }

        rsTodo.SetFieldValue("FK_REJECTEDBY", CurrentUser.EmployeeID);
        rsTodo.SetFieldValue("DATEREJECTED", DateTime.Now);
        rsTodo.Update();

        var purchaseInvoiceId = rsTodo.GetField("FK_PURCHASEINVOICE").Value as int? ?? 0;

        if (purchaseInvoiceId == 0)
        {
            return; //Geen factuur gekoppeld
        }

        var purchaseInvoiceWorkflowState = GetRecordset("R_PURCHASEINVOICE", "FK_WORKFLOWSTATE",
                $"PK_R_PURCHASEINVOICE = {purchaseInvoiceId}", "")
            .DataTable.AsEnumerable().First().Field<Guid>("FK_WORKFLOWSTATE");

        var wfStateNew = new Guid("466c4d1e-21ce-47bb-a2c6-8b96c6ec0d23");

        if (purchaseInvoiceWorkflowState != wfStateNew)
        {
            return;
        }

        Guid wfSetToBlocked = new Guid("47f70ef4-5c62-4461-93c7-606a4acaf9e5");

        var parameters = new Dictionary<string, object>();
        parameters.Add("ExecutedFromSetDeclarationTodoDone", true);

        var result = ExecuteWorkflowEvent("R_PURCHASEINVOICE",
            purchaseInvoiceId, wfSetToBlocked, parameters);

        if (result.HasError)
        {
            Log.Error(GetRecordTag("R_TODO", id), 
                $"Blokkeren declaratie inkoopfactuur is mislukt; oorzaak: {result.GetResult()}", "");
            return;
        }
    }
}
