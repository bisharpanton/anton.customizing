﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;

public class StartSalesTodoTemplate : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //13-1-2025
        //Start Sales todo template

        var relation = GetRecordset("R_RELATION", "NAME, MAINCONTACTDEBTOR", $"PK_R_RELATION = {id}", "")
            .DataTable.AsEnumerable().First();

        var firstStepSalesTodoTemplate = GetRecordset("U_TEMPLATESALESTODOS", "", "", "SEQUENCENUMBER ASC")
            .DataTable.AsEnumerable().AsEnumerable().FirstOrDefault();

        if(firstStepSalesTodoTemplate == null)
        {
            Log.Error("Error", "Geen records gevonden in Sales taken template.", "");
            return;
        }

        var rsTodo = GetRecordset("R_TODO", "", "PK_R_TODO = NULL", "");
        rsTodo.AddNew();
        rsTodo.SetFieldValue("FK_RELATION", id);

        if(relation.Field<int?>("MAINCONTACTDEBTOR").HasValue)
        {
            rsTodo.SetFieldValue("FK_CONTACT", relation.Field<int>("MAINCONTACTDEBTOR"));
        }

        rsTodo.SetFieldValue("FK_TODOTYPE", firstStepSalesTodoTemplate.Field<int>("FK_TODOTYPE"));
        rsTodo.SetFieldValue("DESCRIPTION", firstStepSalesTodoTemplate.Field<string>("DESCRIPTION"));
        rsTodo.SetFieldValue("FK_TEMPLATESALESTODO", firstStepSalesTodoTemplate.Field<int>("PK_U_TEMPLATESALESTODOS"));
        rsTodo.SetFieldValue("DUEDATE", DateTime.Now);
        rsTodo.SetFieldValue("FK_ASSIGNEDTO", CurrentUser.EmployeeID);
        rsTodo.SetFieldValue("INTERNALMEMO", firstStepSalesTodoTemplate.Field<string>("MEMO"));


        rsTodo.Update();
    }

}
