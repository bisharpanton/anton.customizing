﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;

public class CreateOrUpdateWkaFile : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //SP
        //16-3-2023
        //Maak of update het WKA-dossier

        var relation = GetRecordset("R_RELATION", "NAME, COCNUMBER, FK_WKATYPE", $"PK_R_RELATION = {id}", "")
            .DataTable.AsEnumerable().First();

        CreateOrUpdateWkaFiles(relation);
    }

    private void CreateOrUpdateWkaFiles(DataRow relation)
    {
        var wkaType = GetRecordset("U_WKATYPE", "DESCRIPTION",
            $"PK_U_WKATYPE = {relation.Field<int>("FK_WKATYPE")}", "").DataTable.AsEnumerable().First().Field<string>("DESCRIPTION");

        var newWkaFile = new WkaRelation()
        {
            RelationName = relation.Field<string>("NAME"),
            CocNumber = relation.Field<string>("COCNUMBER"),
            WkaType = wkaType
        };

        var wkaApiUrl = GetRecordset("R_CRMSETTINGS", "WKAAPIURL", "", "")
            .DataTable.AsEnumerable().First().Field<string>("WKAAPIURL");

        using (var httpClient = new HttpClient())
        {
            var url = $"{wkaApiUrl}/api/WkaRelations";
            //httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accesToken);

            HttpResponseMessage response;
            try
            {
                response = httpClient.PostAsJsonAsync(url, newWkaFile).Result;
            }
            catch (Exception e)
            {
                Log.Error("Error", $"Aanmaken WKA-dossier is mislukt, oorzaak: {e.Message} op url: {url}", "");
                return;
            }

            if (!response.IsSuccessStatusCode)
            {
                var resultString = response.Content.ReadAsStringAsync().Result;
                Log.Error("Error", $"Aanmaken WKA-dossier is mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase} {resultString} (url: {url})", "");
                return;
            }
        }
    }

    public class WkaRelation
    {
        public string RelationName { get; set; }
        public string CocNumber { get; set; }
        public string WkaType { get; set; }
    }
}
