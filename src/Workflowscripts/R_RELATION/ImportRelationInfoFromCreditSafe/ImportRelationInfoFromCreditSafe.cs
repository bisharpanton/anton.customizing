﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Ridder.Common.Reports;

public class ImportRelationInfoFromCreditSafe : WorkflowScriptInfo
{
    string _ConnectionString = @"Data Source=SRV-APP03\RIDDERIQ;Initial Catalog=Anton Groep;User Id=sa;Password=Welkom@ridderiq";
    //string _ConnectionString = @"Data Source=LT115\RIDDERIQ;Initial Catalog=Anton Groep;User Id=sa;Password=Welkom@ridderiq";

    public void Execute()
    {

        //DB - Deze code staat uit. We zoeken nu relaties op creditsafe via een ander script
        /*
        var id = (int)RecordId;

        //DB
        //12-11-2019
        //Op basis van het KVK-nummer gaan we op zoek in CreditSafe naar relatie informatie
        //en die importeren we

        var kvkNumber = GetRecordset("R_RELATION", "COCNUMBER",
            $"PK_R_RELATION = {id}", "").DataTable.AsEnumerable().First().Field<string>("COCNUMBER");
        
        if(kvkNumber == "")
        {
            Log.Info("Info", "KVK-nummer is leeg. Import kan niet uitgevoerd worden.", "");
            return;
        }

        var token = GetOrCreateNewToken();

        RootObjectCompanyInfo rootObjectCompanyInfo = GetRootObjectFromCreditSafe(token, kvkNumber);

        if (rootObjectCompanyInfo == null)
        {
            return;
        }

        if (rootObjectCompanyInfo.TotalSize == 0)
        {
            Log.Info("Info", $"Geen bedrijf gevonden op CreditSafe met KVK-nummer {kvkNumber}.", "");
            return;
        }

        Company foundCompany = new Company();

        if (rootObjectCompanyInfo.TotalSize > 1)
        {
            if (rootObjectCompanyInfo.Companies.Count(x => x.StatusDescription == "ACTIVE") > 1)
            {
                Log.Info("Info",
                    $"{rootObjectCompanyInfo.Companies.Count(x => x.StatusDescription == "ACTIVE")} bedrijven gevonden op CreditSafe met KVK-nummer {kvkNumber} en status 'Actief'.",
                    "");

                return;
            }
            else if (rootObjectCompanyInfo.Companies.Count(x => x.StatusDescription == "ACTIVE") == 0)
            {
                Log.Info("Info",
                    $"{rootObjectCompanyInfo.TotalSize} bedrijven gevonden op CreditSafe met KVK-nummer {kvkNumber}, maar allemaal 'Inactief'. De import is gestopt.",
                    "");

                return;
            }

            foundCompany = rootObjectCompanyInfo.Companies.First(x => x.StatusDescription == "ACTIVE");
        }
        else if (rootObjectCompanyInfo.TotalSize == 1)
        {
            foundCompany = rootObjectCompanyInfo.Companies.First();
        }

        RootObjectCompanyReport rootObjectCompanyReport = GetCompanyReport(token, foundCompany);

        if (rootObjectCompanyReport == null)
        {
            return;
        }

        SaveCreditSafeInfoInRelation(id, foundCompany, rootObjectCompanyReport.Report);*/
    }
    /*
    private void SaveCreditSafeInfoInRelation(int relationId, Company company, Report report)
    {
        var rsRelatie = GetRecordset("R_RELATION", "",
            $"PK_R_RELATION = {relationId}", "");
        rsRelatie.MoveFirst();
        rsRelatie.Fields["NAME"].Value = company.Name;
       
        //Adres
        if (!(rsRelatie.Fields["FK_VISITINGADDRESS"].Value as int?).HasValue)
        {
            var rsAddress = GetRecordset("R_ADDRESS", "",
                "PK_R_ADDRESS = -1", "");
            rsAddress.AddNew();
            rsAddress.Fields["STREET"].Value = company.Address.Street;
            rsAddress.Fields["HOUSENUMBER2"].Value = company.Address.HouseNo;
            rsAddress.Fields["ZIPCODE"].Value = company.Address.PostCode;
            rsAddress.Fields["CITY"].Value = company.Address.City;
            rsAddress.Fields["FK_COUNTRY"].Value = GetCountryNL();
            rsAddress.Update();

            rsRelatie.Fields["FK_VISITINGADDRESS"].Value = rsAddress.Fields["PK_R_ADDRESS"].Value;
        }
        else
        {
            var rsAddress = GetRecordset("R_ADDRESS", "",
                $"PK_R_ADDRESS = {rsRelatie.Fields["FK_VISITINGADDRESS"].Value}", "");
            rsAddress.MoveFirst();
            rsAddress.Fields["STREET"].Value = company.Address.Street;
            rsAddress.Fields["HOUSENUMBER2"].Value = company.Address.HouseNo;
            rsAddress.Fields["ZIPCODE"].Value = company.Address.PostCode;
            rsAddress.Fields["CITY"].Value = company.Address.City;
            rsAddress.Fields["FK_COUNTRY"].Value = GetCountryNL();
            rsAddress.Update();
        }

        rsRelatie.Fields["PHONE1"].Value = report.ContactInformation.MainAddress.Telephone;

        if(report.ContactInformation.Websites != null && report.ContactInformation.Websites.Any())
        {
            rsRelatie.Fields["WEBSITE"].Value = report.ContactInformation.Websites.First();
        }

        if (report.CompanySummary.CreditRating.CreditLimit.Value == "Geen krediet limiet")
        {
            //Log.Info("Info", "Geen krediet limiet gevonden in CreditSafe.", "");
            rsRelatie.Fields["SALESCREDITLIMIT"].Value = 0.0;
        }
        else
        {
            rsRelatie.Fields["SALESCREDITLIMIT"].Value = report.CompanySummary.CreditRating.CreditLimit.Value;
        }

        rsRelatie.Fields["SALESCREDITLIMITSCORE"].Value =
            $"{report.CompanySummary.CreditRating.ProviderValue.Value} - {report.CompanySummary.CreditRating.CommonValue}";

        rsRelatie.Fields["FK_LEGALFORM"].Value = GetOrCreate("R_LEGALFORM", "DESCRIPTION", report.CompanyIdentification.BasicInformation.LegalForm.Description);

        if (report.OtherInformation?.EmployeesInformation != null && report.OtherInformation.EmployeesInformation.Any())
        {
            rsRelatie.Fields["NUMBEROFEMPLOYEES"].Value = report.OtherInformation.EmployeesInformation
                .OrderByDescending(x => x.Year).First().NumberOfEmployees;
        }
        
        rsRelatie.Fields["FK_RELATIONFINANCIALSTATE"].Value =
            GetOrCreate("R_RELATIONFINANCIALSTATE", "DESCRIPTION", report.CompanySummary.CompanyStatus.Description);

        rsRelatie.Fields["FK_SBIMAINACTIVITY"].Value = GetOrCreateSbiMainActivity(report.CompanySummary.MainActivity);

        rsRelatie.Fields["LASTDATEIMPORTCREDITSAFE"].Value = DateTime.Now;
        rsRelatie.Update();
    }

    private int GetOrCreate(string tableName, string columnName, string companyDescription)
    {
        var rs = GetRecordset(tableName, "", "", "");

        if (rs.DataTable.AsEnumerable().Any(x => x.Field<string>(columnName).Equals(companyDescription, StringComparison.OrdinalIgnoreCase)))
        {
            return rs.DataTable.AsEnumerable()
                .First(x => x.Field<string>(columnName).Equals(companyDescription, StringComparison.OrdinalIgnoreCase))
                .Field<int>($"PK_{tableName}");
        }
        else
        {
            var newCode = $"{rs.RecordCount + 1:00}"; 
            
            rs.AddNew();
            rs.Fields["CODE"].Value = newCode;
            rs.Fields["DESCRIPTION"].Value = companyDescription;
            rs.Update();

            return (int)rs.Fields[$"PK_{tableName}"].Value;
        }
    }

    private int GetOrCreateSbiMainActivity(MainActivity companySummaryMainActivity)
    {
        var rsSbiActivities = GetRecordset("C_SBIACTIVITIES", "",
            $"CODE = '{companySummaryMainActivity.Code}'", "");

        if (rsSbiActivities.RecordCount > 0)
        {
            rsSbiActivities.MoveFirst();
        }
        else
        {
            rsSbiActivities.AddNew();
            rsSbiActivities.Fields["CODE"].Value = companySummaryMainActivity.Code;
            rsSbiActivities.Fields["DESCRIPTION"].Value = companySummaryMainActivity.Description;
            rsSbiActivities.Update();
        }

        return (int)rsSbiActivities.Fields["PK_C_SBIACTIVITIES"].Value;
    }


    private RootObjectCompanyReport GetCompanyReport(TokenResponse token, Company company)
    {
        //Vraag company report op mbv CreditSafe company id 
        var urlPath = 
            new Uri($"https://connect.creditsafe.com/v1/companies/{company.Id}?language=NL");

        HttpClient client = new HttpClient();
        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token.Token);

        var response = client.GetAsync(urlPath).GetAwaiter().GetResult();

        if(!response.IsSuccessStatusCode)
        {
            Log.Info("Info", $"Opvragen krediet-rapport bij CreditSafe mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase}.", "");
            return null;
        }

        response.EnsureSuccessStatusCode();
        return response.Content.ReadAsAsync<RootObjectCompanyReport>().Result;
    }

    private RootObjectCompanyInfo GetRootObjectFromCreditSafe(TokenResponse token, string kvkNumber)
    {
        var allCompanies = new List<Company>(); 
        
        //We kunnen per 50 relaties ophalen bij een zoekstatement
        var totalCompaniesPerPage = 50;
        var pageNumber = 0;
        var totalCompaniesFound = Int32.MaxValue;

        while (totalCompaniesFound > (pageNumber * totalCompaniesPerPage))
        {
            pageNumber++;

            //Vraag relatie info op mbv KVK van de huidige relatie
            var urlPath = new Uri(
                    $"https://connect.creditsafe.com/v1/companies?regNo={kvkNumber}&countries=NL&page={pageNumber}&pageSize={totalCompaniesPerPage}");

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token.Token);

            var response = client.GetAsync(urlPath).GetAwaiter().GetResult();

            if(!response.IsSuccessStatusCode)
            {
                Log.Info("Info", $"Opvragen relatie info bij CreditSafe mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase}.", "");
                return null;
            }

            response.EnsureSuccessStatusCode();
            var rootObjectCompanyInfo = response.Content.ReadAsAsync<RootObjectCompanyInfo>().Result;

            totalCompaniesFound = rootObjectCompanyInfo.TotalSize;
            allCompanies.AddRange(rootObjectCompanyInfo.Companies);
        }

        return new RootObjectCompanyInfo()
        {
            TotalSize = totalCompaniesFound,
            Companies = allCompanies,
        };
    }

    private TokenResponse GetOrCreateNewToken()
    {
        var creditSafeTokenInfo = DetermineCreditSafeTokenInfoFromAntonGroup();

        //Token is 1 uur geldig dus deze is nog bruikbaar
        if(creditSafeTokenInfo.TokenGenerated > DateTime.Now.AddHours(-1))
        {
            return new TokenResponse() {Token = creditSafeTokenInfo.Token};
        }

        //Genereer nieuwe token door verbinding te maken met CreditSafe
        var urlPath = new Uri(@"https://connect.creditsafe.com/v1/authenticate");

        HttpClient client = new HttpClient();
        HttpResponseMessage result = client.PostAsJsonAsync(urlPath, creditSafeTokenInfo.CreditSafeCredentials)
            .GetAwaiter().GetResult();

        if (!result.IsSuccessStatusCode)
        {
            Log.Info("Error", $"Verbinding maken met CreditSafe is mislukt; oorzaak: {(int)result.StatusCode}:{result.ReasonPhrase}", "");
        }

        var newToken = result.Content.ReadAsAsync<TokenResponse>().GetAwaiter().GetResult();
        SaveNewTokenInAntonGroup(newToken.Token);

        return newToken;
    }

    private void SaveNewTokenInAntonGroup(string newToken)
    {
        if(GetUserInfo().CompanyName == "Anton Groep")
        {
            var rsCRM = GetRecordset("R_CRMSETTINGS",
                    "CREDITSAFEUSERNAME, CREDITSAFEPASSWORD, CREDITSAFETOKEN, CREDITSAFETOKENGENERATED", "", "");
            rsCRM.MoveFirst();
            rsCRM.Fields["CREDITSAFETOKEN"].Value = newToken;
            rsCRM.Fields["CREDITSAFETOKENGENERATED"].Value = DateTime.Now;
            rsCRM.Update();
        }
        else
        {
            using (var conn = new System.Data.SqlClient.SqlConnection(_ConnectionString))
            {
                conn.Open();

                string sqlQuery = @"
				UPDATE X_R_CRMSETTINGS 
                SET CREDITSAFETOKEN = @newToken, CREDITSAFETOKENGENERATED = @newDate
				FROM [Anton Groep].dbo.X_R_CRMSETTINGS;";

                var sqlCmd = new SqlCommand(sqlQuery, conn);
                sqlCmd.Parameters.AddWithValue("@newToken", newToken);
                sqlCmd.Parameters.AddWithValue("@newDate", DateTime.Now);

                sqlCmd.ExecuteNonQuery();

                conn.Close();
            }
        }
    }

    private CreditSafeTokenInfo DetermineCreditSafeTokenInfoFromAntonGroup()
    {
        CreditSafeTokenInfo result = new CreditSafeTokenInfo();

        if(GetUserInfo().CompanyName == "Anton Groep")
        {
            var crmInfo = GetRecordset("R_CRMSETTINGS",
                    "CREDITSAFEUSERNAME, CREDITSAFEPASSWORD, CREDITSAFETOKEN, CREDITSAFETOKENGENERATED", "", "")
                .DataTable.AsEnumerable().First();
            result.CreditSafeCredentials = new CreditSafeCredentials()
            {
                UserName = crmInfo.Field<string>("CREDITSAFEUSERNAME"), 
                Password = crmInfo.Field<string>("CREDITSAFEPASSWORD"), 
            };
            result.Token = crmInfo.Field<string>("CREDITSAFETOKEN");
            result.TokenGenerated = !crmInfo.Field<DateTime?>("CREDITSAFETOKENGENERATED").HasValue
                ? DateTime.MinValue
                : crmInfo.Field<DateTime>("CREDITSAFETOKENGENERATED");
        }
        else
        {
            using (var conn = new System.Data.SqlClient.SqlConnection(_ConnectionString))
            {
                conn.Open();

                string sqlSelectTemplate = @"
				SELECT CRM.CREDITSAFEUSERNAME, CRM.CREDITSAFEPASSWORD, CRM.CREDITSAFETOKEN, CRM.CREDITSAFETOKENGENERATED
				FROM [Anton Groep].dbo.X_R_CRMSETTINGS AS CRM;";

                System.Data.SqlClient.SqlCommand sqlCmd = new System.Data.SqlClient.SqlCommand(sqlSelectTemplate, conn);
                
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(sqlCmd);
                DataTable dtSelectTemplate = new DataTable("Template");
                da.Fill(dtSelectTemplate);

                result.CreditSafeCredentials = new CreditSafeCredentials()
                {
                    UserName = dtSelectTemplate.Rows[0]["CREDITSAFEUSERNAME"].ToString(),
                    Password = dtSelectTemplate.Rows[0]["CREDITSAFEPASSWORD"].ToString(),
                };
                result.Token = dtSelectTemplate.Rows[0]["CREDITSAFETOKEN"].ToString();
                result.TokenGenerated = (DateTime)dtSelectTemplate.Rows[0]["CREDITSAFETOKENGENERATED"];

                da = null;
                sqlCmd = null;

                conn.Close();
            }
        }

        return result;
    }

    private int GetCountryNL()
    {
        return GetRecordset("R_COUNTRY", "PK_R_COUNTRY", "CODE = 'NL'", "")
            .DataTable.AsEnumerable().First().Field<int>("PK_R_COUNTRY");
    }

    class CreditSafeTokenInfo
    {
        public CreditSafeCredentials CreditSafeCredentials { get; set; }
        public string Token { get; set; }
        public DateTime TokenGenerated { get; set; }
    }

    class CreditSafeCredentials
    {
        [JsonProperty("username")]
        public string UserName { get; set; }
        [JsonProperty("password")]
        public string Password { get; set; }
    }

    public class TokenResponse
    {
        [JsonProperty("token")]
        public string Token { get; set; }
    }

    public class Address
    {
        [JsonProperty("simpleValue")]
        public string SimpleValue { get; set; }
        [JsonProperty("street")]
        public string Street { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("postCode")]
        public string PostCode { get; set; }
        [JsonProperty("houseNo")]
        public string HouseNo { get; set; }
        [JsonProperty("telephone")]
        public string Telephone { get; set; }

    }

    public class Company
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("regNo")]
        public string RegNo { get; set; }
        [JsonProperty("vatNo")]
        public List<string> VatNumbers { get; set; }
        [JsonProperty("safeNo")]
        public string SafeNo { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("tradingNames")]
        public List<string> TradingNames { get; set; }
        [JsonProperty("address")]
        public Address Address { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("officeType")]
        public string OfficeType { get; set; }
        [JsonProperty("dateOfLatestAccounts")]
        public DateTime DateOfLatestAccounts { get; set; }
        [JsonProperty("dateOfLatestChange")]
        public DateTime DateOfLatestChange { get; set; }
        [JsonProperty("activityCode")]
        public string ActivityCode { get; set; }
        [JsonProperty("statusDescription")]
        public string StatusDescription { get; set; }
    }

    public class RootObjectCompanyInfo
    {
        [JsonProperty("totalSize")]
        public int TotalSize { get; set; }
        [JsonProperty("companies")]
        public List<Company> Companies { get; set; }
    }

    public class MainActivity
    {
        [JsonProperty("code")]
        public string Code { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("classification")]
        public string Classification { get; set; }
    }

    public class CompanyStatus
    {
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
    }

    public class CreditLimit
    {
        [JsonProperty("value")]
        public string Value { get; set; }
    }

    public class ProviderValue
    {
        [JsonProperty("maxValue")]
        public string MaxValue { get; set; }
        [JsonProperty("minValue")]
        public string MinValue { get; set; }
        [JsonProperty("value")]
        public string Value { get; set; }
    }

    public class CreditRating
    {
        [JsonProperty("commonValue")]
        public string CommonValue { get; set; }
        [JsonProperty("commonDescription")]
        public string CommonDescription { get; set; }
        [JsonProperty("creditLimit")]
        public CreditLimit CreditLimit { get; set; }
        [JsonProperty("providerValue")]
        public ProviderValue ProviderValue { get; set; }
        [JsonProperty("providerDescription")]
        public string ProviderDescription { get; set; }
    }

    public class CompanySummary
    {
        [JsonProperty("businessName")]
        public string BusinessName { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("companyNumber")]
        public string CompanyNumber { get; set; }
        [JsonProperty("companyRegistrationNumber")]
        public string CompanyRegistrationNumber { get; set; }
        [JsonProperty("mainActivity")]
        public MainActivity MainActivity { get; set; }
        [JsonProperty("companyStatus")]
        public CompanyStatus CompanyStatus { get; set; }
        [JsonProperty("creditRating")]
        public CreditRating CreditRating { get; set; }
    }

    public class ContactInformation
    {
        [JsonProperty("mainAddress")]
        public Address MainAddress { get; set; }
        [JsonProperty("otherAddresses")]
        public List<Address> OtherAddresses { get; set; }
        [JsonProperty("websites")]
        public List<string> Websites { get; set; }
    }

    public class EmployeesInformation
    {
        [JsonProperty("year")]
        public int Year { get; set; }
        [JsonProperty("numberOfEmployees")]
        public string NumberOfEmployees { get; set; }
    }

    public class OtherInformation
    {
        [JsonProperty("employeesInformation")]
        public List<EmployeesInformation> EmployeesInformation { get; set; }
    }

    public class LimitHistory
    {
        [JsonProperty("date")]
        public DateTime Date { get; set; }
        [JsonProperty("companyValue")]
        public int CompanyValue { get; set; }
    }

    public class GroupStructure
    {
        [JsonProperty("ultimateParent")]
        public Company UltimateParent { get; set; }
        [JsonProperty("subsidiaryCompanies")]
        public List<Company> SubsidiaryCompanies { get; set; }
        [JsonProperty("affiliatedCompanies")]
        public List<Company> AffiliatedCompanies { get; set; }
    }
    public class AdditionalInformation
    {
        [JsonProperty("limitHistory")]
        public List<LimitHistory> LimitHistories { get; set; }
    }

    public class LegalForm
    {
        [JsonProperty("providerCode")]
        public string ProviderCode { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
    }

    public class BasicInformation
    {
        [JsonProperty("businessName")]
        public string BusinessName { get; set; }
        [JsonProperty("registeredCompanyName")]
        public string RegisteredCompanyName { get; set; }
        [JsonProperty("companyRegistrationNumber")]
        public string CompanyRegistrationNumber { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("companyRegistrationDate")]
        public DateTime CompanyRegistrationDate { get; set; }
        [JsonProperty("operationsStartDate")]
        public DateTime OperationsStartDate { get; set; }
        [JsonProperty("legalForm")]
        public LegalForm LegalForm { get; set; }
        [JsonProperty("companyStatus")]
        public CompanyStatus CompanyStatus { get; set; }
        [JsonProperty("contactAddress")]
        public Address ContactAddress { get; set; }
    }

    public class CompanyIdentification
    {
        [JsonProperty("basicInformation")]
        public BasicInformation BasicInformation { get; set; }
    }

    public class Report
    {
        [JsonProperty("companyId")]
        public string CompanyId { get; set; }
        [JsonProperty("language")]
        public string Language { get; set; }
        [JsonProperty("companySummary")]
        public CompanySummary CompanySummary { get; set; }
        [JsonProperty("otherInformation")]
        public OtherInformation OtherInformation { get; set; }
        [JsonProperty("groupStructure")]
        public GroupStructure GroupStructure { get; set; }
        [JsonProperty("additionalInformation")]
        public AdditionalInformation AdditionalInformation { get; set; }
        [JsonProperty("contactInformation")]
        public ContactInformation ContactInformation { get; set; }
        [JsonProperty("companyIdentification")]
        public CompanyIdentification CompanyIdentification { get; set; }
    }


    public class RootObjectCompanyReport
    {
        [JsonProperty("orderId")]
        public string OrderId { get; set; }
        [JsonProperty("companyId")]
        public string CompanyId { get; set; }
        [JsonProperty("dateOfOrder")]
        public DateTime DateOfOrder { get; set; }
        [JsonProperty("language")]
        public string Language { get; set; }
        [JsonProperty("userId")]
        public string UserId { get; set; }
        [JsonProperty("chargeRef")]
        public object ChargeRef { get; set; }
        [JsonProperty("report")]
        public Report Report { get; set; }
    }
    */
}
