﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class ChangeCreditSafeId : WorkflowScriptInfo
{
    public void Execute()
    {
        var relationId = (int)RecordId;

        var newCreditSafeId = Convert.ToString(Parameters["CreditSafeId"].Value);

        var rsRelation = GetRecordset("R_RELATION", "CREDITSAFEID",
            $"PK_R_RELATION = {relationId}", "");
        rsRelation.MoveFirst();

        rsRelation.SetFieldValue("CREDITSAFEID", newCreditSafeId);

        rsRelation.Update();

        var updateResult2 = rsRelation.Update2();

        if (updateResult2.Any(x => x.HasError))
        {
            Log.Error("Error", $"Wijzigen van CreditSafe id mislukt, oorzaak {updateResult2.First(x => x.HasError).GetResult()}",
                    "");
            return;
        }


    }
}
