﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;

public class CreateWkaEmployee : WorkflowScriptInfo
{
    public void Execute()
    {
        var relationId = (int)RecordId;

        //SP
        //16-3-2023
        //Exporteer contactpersoon het WKA-dossier

        var firstName = Convert.ToString(Parameters["FirstName"].Value);
        var namePrefix = Convert.ToString(Parameters["NamePrefix"].Value);
        var lastName = Convert.ToString(Parameters["LastName"].Value);
        var bsn = Convert.ToString(Parameters["BSN"].Value);
        var regNo = Convert.ToString(Parameters["RegNo"].Value);
        var cocNumber = Convert.ToString(Parameters["CocNumber"].Value);
        var requiredDocumentTypes = Convert.ToString(Parameters["RequiredDocumentTypes"].Value);

        if (string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName) || (string.IsNullOrEmpty(bsn) && string.IsNullOrEmpty(regNo) && string.IsNullOrEmpty(cocNumber)))
        {
            Log.Error("Error", "Geen naam en BSN / Registratienummer / KvK-nummer gevonden.", "");
            return;
        }

        var relationCocNumber = GetRecordset("R_RELATION", "COCNUMBER", $"PK_R_RELATION = {relationId} AND WKA = 1", "")
            .DataTable.AsEnumerable().First().Field<string>("COCNUMBER");

        if(string.IsNullOrEmpty(relationCocNumber))
        {
            Log.Error("Error", "Geen KVK nummer gevonden bij deze relatie.", "");
            return;
        }

        var wkaDocumentTypesDescriptions = string.IsNullOrEmpty(requiredDocumentTypes)
            ? new List<string>()
            : requiredDocumentTypes.Split(';').ToList();

        var wkaEmployee = new WkaEmployee()
        {
            FirstName = firstName,
            NamePrefix = namePrefix,
            LastName = lastName,
            Bsn = bsn,
            RegNo = regNo,
            CocNumber = cocNumber,
            RequiredDocumentTypes = wkaDocumentTypesDescriptions,
        };

        SendWkaEmployeeToWkaApi(relationCocNumber, wkaEmployee);
    }

    private void SendWkaEmployeeToWkaApi(string relationCocNumber, WkaEmployee wkaEmployee)
    {
        var wkaApiUrl = GetRecordset("R_CRMSETTINGS", "WKAAPIURL", "", "")
            .DataTable.AsEnumerable().First().Field<string>("WKAAPIURL");

        using (var httpClient = new HttpClient())
        {
            var url = $"{wkaApiUrl}/api/WkaEmployees?cocNumber={relationCocNumber}";

            HttpResponseMessage response;
            try
            {
                response = httpClient.PostAsJsonAsync(url, wkaEmployee).Result;
            }
            catch (Exception e)
            {
                Log.Error("Error", $"Aanmaken contactpersoon in WKA-dossier is mislukt, oorzaak: {e.Message}", "");
                return;
            }

            if (!response.IsSuccessStatusCode)
            {
                var resultString = response.Content.ReadAsStringAsync().Result;
                Log.Error("Error", $"Aanmaken WKA-dossier is mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase} {resultString}", "");
                return;
            }
        }
    }

    class WkaEmployee
    {
        public string FirstName { get; set; }
        public string NamePrefix { get; set; }
        public string LastName { get; set; }
        public string Bsn { get; set; }
        public string RegNo { get; set; }
        public string CocNumber { get; set; }
        public List<string> RequiredDocumentTypes { get; set; }
    }
}
