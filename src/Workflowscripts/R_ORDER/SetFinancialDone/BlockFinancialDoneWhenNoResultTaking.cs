﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class BlockFinancialDoneWhenNoResultTaking : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //20-6-2024
        //Blokkeer financieel gereed melden indien geen resultaatnemingen

        var numberOfResultTaking = GetRecordset("C_RESULTAATNEMING", "PK_C_RESULTAATNEMING", $"FK_ORDER = {id}", "").RecordCount;

        if(numberOfResultTaking == 0)
        {
            Log.Error("Error", "Financieel gereed melden mislukt, er zijn geen resultaatnemingen bij deze order.", "");
            return;
        }
    }
}
