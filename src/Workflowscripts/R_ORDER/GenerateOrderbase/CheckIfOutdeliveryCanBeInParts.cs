﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class CheckIfOutdeliveryCanBeInParts : WorkflowScriptInfo
{
    public Boolean Execute()
    {
        var id = (int)RecordId;

        //DB
        //19-03-2019
        //Controleer of genereren van basisregels in delen kan zijn

        var rsCRM = GetRecordset("R_CRMSETTINGS", "OUTDELIVERYSALESORDERDETAILASSEMBLYINPARTS", "", "");
        rsCRM.MoveFirst();

        return (bool)rsCRM.Fields["OUTDELIVERYSALESORDERDETAILASSEMBLYINPARTS"].Value;
    }
}
