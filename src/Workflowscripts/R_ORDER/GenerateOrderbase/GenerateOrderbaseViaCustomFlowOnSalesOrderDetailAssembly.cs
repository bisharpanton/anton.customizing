﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class GenerateOrderbaseViaCustomFlowOnSalesOrderDetailAssembly : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //21-12-2017
        //Roep custom-workflow genereer orderbasis aan voor de onderliggende verkoopregels

        var rsVerkoopregelStuklijst = GetRecordset("R_SALESORDERDETAILASSEMBLY", "PK_R_SALESORDERDETAILASSEMBLY, QUANTITY",
            $"FK_ORDER = {RecordId}", "");
        rsVerkoopregelStuklijst.MoveFirst();

        if (rsVerkoopregelStuklijst.RecordCount == 0)
        {
            return;
        }
        
        var rsOrderBase = GetRecordset("R_ORDERBASE", "FK_SALESORDERDETAILASSEMBLYCUSTOM, QUANTITY",
            $"FK_ORDERNUMBER = {RecordId}", "");
        var orderbase = rsOrderBase.DataTable.AsEnumerable();

        while (!rsVerkoopregelStuklijst.EOF)
        {
            var salesOrderDetailAssemblyId = (int)rsVerkoopregelStuklijst.Fields["PK_R_SALESORDERDETAILASSEMBLY"].Value;
            var orderbaseInfo = orderbase.Where(x =>
                x.Field<int?>("FK_SALESORDERDETAILASSEMBLYCUSTOM").HasValue &&
                x.Field<int>("FK_SALESORDERDETAILASSEMBLYCUSTOM") == salesOrderDetailAssemblyId);

            var aantalOmgezet = (!orderbaseInfo.Any()) ? 0.0 : orderbaseInfo.Sum(x => x.Field<double>("QUANTITY"));
            var restant = (double)rsVerkoopregelStuklijst.Fields["QUANTITY"].Value - aantalOmgezet;

            if (Math.Abs(restant) < 0.1)
            {
                rsVerkoopregelStuklijst.MoveNext();
                continue;
            }

            var wfCustomGenerateOrderBase = new Guid("93c157b5-8ee1-49ce-af7a-930c0738c8df");

            var myParams = new Dictionary<string, object>
            {
                {"Aantal", restant},
                {"OutdeliveryComplete", true}
            };
            var result = ExecuteWorkflowEvent("R_SALESORDERDETAILASSEMBLY", salesOrderDetailAssemblyId, wfCustomGenerateOrderBase, myParams);

            if (result.HasError)
            {
                throw new Exception($"Het genereren van de basisregels is mislukt; {result.GetResult()}");
            }

            rsVerkoopregelStuklijst.MoveNext();
        }

    }
}
