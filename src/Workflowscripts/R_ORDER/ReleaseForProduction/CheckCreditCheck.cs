﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class CheckCreditCheck : WorkflowScriptInfo
{
    public Boolean Execute()
    {
        var id = (int)RecordId;

        //DB
        //23-11-2017
        //Controleer of de kredietcheck van alle verkoopregels stuklijst akkoord zijn. Indien niet geef dan de bonnen los vrij

        var rsCRM = GetRecordset("R_CRMSETTINGS", "CREDITCHECKAPPLICABLE", "", "");
        rsCRM.MoveFirst();

        if (!(bool)rsCRM.Fields["CREDITCHECKAPPLICABLE"].Value)
        {
            return true;
        }

        var rsVerkoopregelStuklijst = GetRecordset("R_SALESORDERDETAILASSEMBLY", "PK_R_SALESORDERDETAILASSEMBLY",
            $"FK_ORDER = {RecordId} AND CREDITCHECK <> 2", "");

        return rsVerkoopregelStuklijst.RecordCount == 0;
    }
}
