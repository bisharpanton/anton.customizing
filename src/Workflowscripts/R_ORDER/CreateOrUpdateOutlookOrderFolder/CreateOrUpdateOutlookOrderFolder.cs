﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;

public class CreateOrUpdateOutlookOrderFolder : WorkflowScriptInfo
{
    private const string FolderNameAfgeslotenOrders = "Afgesloten orders";

    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //1-2-2023
        //Bij het aanmaken van een order of wijzigen van de order omschrijving, maak of werk de Outlook folder bij

        var crmSettings = GetRecordset("R_CRMSETTINGS", "CREATEOUTLOOKORDERFOLDERS, OUTLOOKORDERMAILBOX, OUTLOOKORDERMAILBOXPARENTFOLDERID", "", "")
            .DataTable.AsEnumerable().First();

        if (!crmSettings.Field<bool>("CREATEOUTLOOKORDERFOLDERS") || string.IsNullOrEmpty(crmSettings.Field<string>("OUTLOOKORDERMAILBOX")))
        {
            return;
        }

        var username = crmSettings.Field<string>("OUTLOOKORDERMAILBOX");
        var outlookInboxFolderId = crmSettings.Field<string>("OUTLOOKORDERMAILBOXPARENTFOLDERID");

        var accesToken = GetAccessTokenAtMicrosoftGraphApi();

        if (string.IsNullOrEmpty(accesToken))
        {
            return;
        }

        var inboxMailFolders = GetExistingMailFolders(accesToken, username, outlookInboxFolderId, $"displayName eq '{FolderNameAfgeslotenOrders}'");

        if (inboxMailFolders == null)
        {
            return; //Something wrong
        }

        var mailFolderAfgeslotenOrders = inboxMailFolders.FirstOrDefault(x => x.displayName.Equals(FolderNameAfgeslotenOrders));

        if(mailFolderAfgeslotenOrders == null)
        {
            Log.Error("Error", $"Verplaatsen mail folder bij Microsoft API is mislukt. Geen outlook folder 'Afgesloten orders' gevonden.", "");
            return;
        }

        var order = GetRecordset("R_ORDER", "ORDERNUMBER, DESCRIPTION, ORDERDATE", $"PK_R_ORDER = {id}", "")
            .DataTable.AsEnumerable().First();

        var yearMailFolder = GetOrCreateYearMailFolder(accesToken, username, outlookInboxFolderId, order.Field<DateTime>("ORDERDATE").Year.ToString());

        if (yearMailFolder == null)
        {
            return; //Something wrong
        }

        if (Parameters["OrderWordtAfgesloten"] != null && (bool)Parameters["OrderWordtAfgesloten"].Value)
        {
            //Order wordt afgesloten, verplaats de order naar de map Afgesloten orders
            var getMailFoldersErrored = false;
            var orderFolder = GetOrderFolder(accesToken, username, yearMailFolder.id, order, ref getMailFoldersErrored);

            if(getMailFoldersErrored || orderFolder == null)
            {
                return;
            }

            var yearMailFolderAfgeslotenOrders = GetOrCreateYearMailFolder(accesToken, username, mailFolderAfgeslotenOrders.id, order.Field<DateTime>("ORDERDATE").Year.ToString());

            MoveOutlookFolder(accesToken, username, orderFolder, yearMailFolderAfgeslotenOrders);
        }
        else if(Parameters["OrderAfsluitenWordtTeruggedraaid"] != null && (bool)Parameters["OrderAfsluitenWordtTeruggedraaid"].Value)
        {
            //Order afsluiten wordt teruggedraaid, verplaats de order weer terug
            var getMailFoldersErrored = false;
            var orderFolder = GetOrderFolder(accesToken, username, yearMailFolder.id, order, ref getMailFoldersErrored);

            if (getMailFoldersErrored || orderFolder == null)
            {
                return;
            }

            MoveOutlookFolder(accesToken, username, orderFolder, yearMailFolder);
        }
        else
        {
            CreateOrUpdateOrderOutlookFolder(accesToken, username, yearMailFolder.id, order);
        }
    }

    private void MoveOutlookFolder(string accesToken, string username, MailFolder orderFolder, MailFolder destinationFolder)
    {
        using (var httpClient = new HttpClient())
        {
            var url = $"https://graph.microsoft.com/v1.0/users/{username}/mailFolders/{orderFolder.id}/move";
            
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accesToken);

            var moveFolderObject = new MoveFolderObject()
            {
                DestinationId = destinationFolder.id,
            };

            HttpResponseMessage response;
            try
            {
                response = httpClient.PostAsJsonAsync(url, moveFolderObject).Result;
            }
            catch (Exception e)
            {
                Log.Error("Error", $"Verplaatsen mail folder bij Microsoft API is mislukt, oorzaak: {e.Message}", "");
                return;
            }

            if (!response.IsSuccessStatusCode)
            {
                var resultString = response.Content.ReadAsStringAsync().Result;
                Log.Error("Error", $"Verplaatsen mail folders bij Microsoft API is mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase} {resultString}", "");
                return;
            }
        }
    }

    private void CreateOrUpdateOrderOutlookFolder(string accesToken, string username, string parentFolderId, DataRow order)
    {
        var getMailFoldersErrored = false;
        var existingOrderMailFolder = GetOrderFolder(accesToken, username, parentFolderId, order, ref getMailFoldersErrored);

        if(getMailFoldersErrored)
        {
            return;
        }

        if (existingOrderMailFolder == null)
        {
            var folderName = $"{order.Field<int>("ORDERNUMBER")} - {order.Field<string>("DESCRIPTION")}";

            if (folderName.Length > 40)
            {
                folderName = folderName.Substring(0, 40);
            }

            CreateOutlookFolder(accesToken, username, parentFolderId, folderName);
        }
        else
        {
            var newFolderName = $"{order.Field<int>("ORDERNUMBER")} - {order.Field<string>("DESCRIPTION")}";

            if (newFolderName.Length > 40)
            {
                newFolderName = newFolderName.Substring(0, 40);
            }

            UpdateNameOutlookFolder(accesToken, username, existingOrderMailFolder, newFolderName);
        }
    }

    private MailFolder GetOrderFolder(string accesToken, string username, string parentFolderId, DataRow order, ref bool getMailFoldersErrored)
    {
        var mailFoldersThisOrderNumber = GetExistingMailFolders(accesToken, username, parentFolderId, $"startsWith(displayName, '{order.Field<int>("ORDERNUMBER")}')");

        if (mailFoldersThisOrderNumber == null)
        {
            getMailFoldersErrored = true;
            return null;
        }

        return mailFoldersThisOrderNumber.FirstOrDefault(x => x.displayName.StartsWith(order.Field<int>("ORDERNUMBER").ToString()));
    }

    private MailFolder GetOrCreateYearMailFolder(string accesToken, string username, string parentFolderId, string year)
    {
        var mailFoldersThisYear = GetExistingMailFolders(accesToken, username, parentFolderId, $"displayName eq '{year}'");

        if (mailFoldersThisYear == null)
        {
            return null; //Something wrong
        }

        var existingYearMailFolder = mailFoldersThisYear.FirstOrDefault(x => x.displayName.Equals(year));

        if (existingYearMailFolder != null)
        {
            return existingYearMailFolder;
        }
        else
        {
            return CreateOutlookFolder(accesToken, username, parentFolderId, year);
        }
    }

    private void UpdateNameOutlookFolder(string accesToken, string username, MailFolder existingOrderMailFolder, string newFolderName)
    {
        if (existingOrderMailFolder.displayName.Equals(newFolderName))
        {
            return; //Omschrijving is al gelijk.
        }

        existingOrderMailFolder.displayName = newFolderName;

        using (var httpClient = new HttpClient())
        {
            var url = $"https://graph.microsoft.com/v1.0/users/{username}/mailfolders/{existingOrderMailFolder.id}";
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accesToken);

            var request = new HttpRequestMessage(new HttpMethod("PATCH"), url);
            request.Content = new StringContent(JsonConvert.SerializeObject(existingOrderMailFolder), Encoding.UTF8, "application/json");

            HttpResponseMessage response;
            try
            {
                response = httpClient.SendAsync(request).Result;
            }
            catch (Exception e)
            {
                Log.Error("Error", $"Bijwerken mail folder bij Microsoft API is mislukt, oorzaak: {e.Message}", "");
                return;
            }

            if (!response.IsSuccessStatusCode)
            {
                var resultString = response.Content.ReadAsStringAsync().Result;
                Log.Error("Error", $"Bijwerken mail folders bij Microsoft API is mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase} {resultString}", "");
                return;
            }
        }
    }

    private MailFolder CreateOutlookFolder(string accesToken, string username, string outlookParentFolderId, string folderName)
    {
        var newMailFolder = new MailFolder()
        {
            displayName = folderName,
            isHidden = false,
        };

        using (var httpClient = new HttpClient())
        {
            var url = $"https://graph.microsoft.com/v1.0/users/{username}/mailfolders/{outlookParentFolderId}/childfolders";
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accesToken);

            HttpResponseMessage response;
            try
            {
                response = httpClient.PostAsJsonAsync(url, newMailFolder).Result;
            }
            catch (Exception e)
            {
                Log.Error("Error", $"Aanmaken nieuwe mail folder bij Microsoft API is mislukt, oorzaak: {e.Message}", "");
                return null;
            }

            if (!response.IsSuccessStatusCode)
            {
                var resultString = response.Content.ReadAsStringAsync().Result;
                Log.Error("Error", $"Aanmaken nieuwe mail folders bij Microsoft API is mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase} {resultString}", "");
                return null;
            }

            var responseString = response.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<MailFolder>(responseString);
        }
    }

    private List<MailFolder> GetExistingMailFolders(string accesToken, string mailbox, string outlookParentFolderId, string filterString)
    {
        //Let op, geen paging ingebouwd omdat we met een filterstring werken

        var result = new List<MailFolder>();

        using (var httpClient = new HttpClient())
        {
            var url = $"https://graph.microsoft.com/v1.0/users/{mailbox}/mailfolders/{outlookParentFolderId}/childfolders?$filter={filterString}";
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accesToken);

            HttpResponseMessage response;
            try
            {
                response = httpClient.GetAsync(url).Result;
            }
            catch (Exception e)
            {
                Log.Error("Error", $"Opvragen mail folders bij Microsoft API is mislukt, oorzaak: {e.Message}", "");
                return null;
            }

            if (!response.IsSuccessStatusCode)
            {
                var resultString = response.Content.ReadAsStringAsync().Result;
                Log.Error("Error", $"Opvragen mail folders bij Microsoft API is mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase} {resultString}", "");
                return null;
            }

            var responseString = response.Content.ReadAsStringAsync().Result;
            var rootobjectGetChildFolders = JsonConvert.DeserializeObject<RootobjectGetChildFolders>(responseString);

            result = rootobjectGetChildFolders.MailFolders.ToList();
        }

        return result;
    }

    private string GetAccessTokenAtMicrosoftGraphApi()
    {
        var result = string.Empty;

        using (var httpClient = new HttpClient())
        {
            var values = new Dictionary<string, string>
              {
                  { "client_id", "8cf29eff-4b46-4deb-92db-0b9b3d6e18d1" },
                  { "scope", "https://graph.microsoft.com/.default" },
                  { "client_secret", "pVh8Q~DBk72ZHbe78hzePHgjy7KemosxdrjexbYQ" },
                  { "grant_type", "client_credentials" },
              };

            var content = new FormUrlEncodedContent(values);

            var url = "https://login.microsoftonline.com/ffb434bb-2d1f-4a1d-bb91-166558f09c98/oauth2/v2.0/token";

            HttpResponseMessage response;
            try
            {
                response = httpClient.PostAsync(url, content).Result;
            }
            catch (Exception e)
            {
                Log.Error("Error", $"Verkrijgen acces token bij Microsoft API is mislukt, oorzaak: {e.Message}", "");
                return string.Empty;
            }

            if (!response.IsSuccessStatusCode)
            {
                var resultString = response.Content.ReadAsStringAsync().Result;
                Log.Error("Error", $"Verkrijgen acces token bij Microsoft API is mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase} {resultString}", "");
                return string.Empty;
            }

            var responseString = response.Content.ReadAsStringAsync().Result;
            var rootobjectGetAccessToken = JsonConvert.DeserializeObject<RootobjectGetAccessToken>(responseString);

            result = rootobjectGetAccessToken.access_token;
        }

        return result;
    }


    public class RootobjectGetAccessToken
    {
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public int ext_expires_in { get; set; }
        public string access_token { get; set; }
    }


    public class RootobjectGetChildFolders
    {
        public string odatacontext { get; set; }
        [JsonProperty("value")]
        public MailFolder[] MailFolders { get; set; }
        public string odatanextLink { get; set; }
    }

    public class MailFolder
    {
        public string id { get; set; }
        public string displayName { get; set; }
        public string parentFolderId { get; set; }
        public int childFolderCount { get; set; }
        public int unreadItemCount { get; set; }
        public int totalItemCount { get; set; }
        public int sizeInBytes { get; set; }
        public bool isHidden { get; set; }
    }

    public class MoveFolderObject
    {
        public string DestinationId { get; set; }
    }
}
