﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class GenerateResultLog : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;
        var date = Convert.ToDateTime(Parameters["Date"].Value);

        var order = GetRecordset("R_ORDER", "ORDERNUMBER, EXPECTEDTOTALCOSTS, EXPECTEDMARGINPERC, EXPECTEDMARGINAMOUNT, DATEFINANCIALFINISHED, TOTALNETAMOUNT",
            $"PK_R_ORDER = {id}", "").DataTable.AsEnumerable().First();
        var expectedMarginPerc = order.Field<double>("EXPECTEDMARGINPERC");
        var totalNetAmount = order.Field<double>("TOTALNETAMOUNT");
        var dateFinDone = order.Field<DateTime?>("DATEFINANCIALFINISHED");


        var rsOrderWipDetails = GetRecordset("R_ORDERWIPALLDETAIL", "",
            $"FK_ORDER = {id} AND ISREVERSED <> 1 " +
            $"AND FK_REVERSEDORDERWIPDETAILITEM IS NULL AND FK_REVERSEDORDERWIPDETAILMISC IS NULL " +
            $"AND FK_REVERSEDORDERWIPDETAILOUTSOURCED IS NULL AND FK_REVERSEDORDERWIPDETAILWORKACTIVITY IS NULL " +
            $"AND (SOURCE_TABLENAME = 'R_ORDERWIPDETAILITEM' OR SOURCE_TABLENAME = 'R_ORDERWIPDETAILITEMCOR' " +
            $"OR SOURCE_TABLENAME = 'R_ORDERWIPDETAILMISC' OR SOURCE_TABLENAME = 'R_ORDERWIPDETAILMISCCOR'" +
            $"OR SOURCE_TABLENAME = 'R_ORDERWIPDETAILOUTSOURCED' OR SOURCE_TABLENAME = 'R_ORDERWIPDETAILOUTSOURCEDCOR'" +
            $"OR SOURCE_TABLENAME = 'R_ORDERWIPDETAILWORKACTIVITY' OR SOURCE_TABLENAME = 'R_PURCHASEINVOICEDETAILINSTALLMENTITEM'" +
            $"OR SOURCE_TABLENAME = 'R_PURCHASEINVOICEDETAILINSTALLMENTMISC' OR SOURCE_TABLENAME = 'R_PURCHASEINVOICEDETAILINSTALLMENTOURSOURCED')", "");

        if(rsOrderWipDetails.RecordCount == 0)
        {
            return;
        }

        var orderWipAllDetailIds = rsOrderWipDetails.DataTable.AsEnumerable().Select(x => x.Field<Guid>("PK_R_ORDERWIPALLDETAIL"));

        double totalCostPrice = 0.0;

        foreach (var orderWipAllDetailId in orderWipAllDetailIds)
        {
            var orderWipAllDetail = rsOrderWipDetails.DataTable.AsEnumerable().Where(x => x.Field<Guid>("PK_R_ORDERWIPALLDETAIL") == orderWipAllDetailId);

            var sourceTable = orderWipAllDetail.First().Field<string>("SOURCE_TABLENAME");
            var sourceType = sourceTable.Replace("R_ORDERWIPDETAIL", "");
            var trimmedSourceType = sourceType.Replace("COR", "");
            var dateInWip = orderWipAllDetail.First().Field<DateTime?>("DATEINWIP");
            var dateOutWip = orderWipAllDetail.First().Field<DateTime?>("DATEOUTWIP");
            var costPrice = orderWipAllDetail.First().Field<double>("COSTPRICE");

            if (sourceType.Contains("R_PURCHASEINVOICEDETAILINSTALLMENT"))
            {
                if (dateInWip <= date && (dateFinDone > date || !dateFinDone.HasValue))
                {
                    totalCostPrice += costPrice;
                }
            }
            else
            {
                var orderWipDetailId = orderWipAllDetail.First().Field<int>($"FK_ORDERWIPDETAIL{sourceType}");

                var orderwipDetail = GetRecordset($"{sourceTable}", "",
                    $"PK_R_ORDERWIPDETAIL{sourceType} = {orderWipDetailId}", "").DataTable.AsEnumerable();

                bool manualWip = false;
                bool directToWip = false;
                if (!sourceType.Contains("COR"))
                {
                    manualWip = orderwipDetail.First().Field<bool>("ISMANUALWIP");

                    var joborderDetailId = orderwipDetail.FirstOrDefault().Field<int?>($"FK_JOBORDERDETAIL{sourceType}") ?? 0;

                    if(joborderDetailId > 0)
                    {
                        var regPath = GetRecordset($"R_JOBORDERDETAIL{sourceType}", "REGISTRATIONPATH",
                        $"PK_R_JOBORDERDETAIL{sourceType} = {joborderDetailId}", "")
                        .DataTable.AsEnumerable().First().Field<RegistrationPath>("REGISTRATIONPATH");

                        if (regPath == RegistrationPath.NormativeWIPEntry)
                        {
                            directToWip = true;
                        }
                    }                   
                }

                bool labour = false;
                if (sourceType.Contains("WORKACTIVITY"))
                {
                    labour = true;
                }

                if (manualWip || labour || directToWip)
                {
                    if (dateInWip <= date && (dateOutWip > date || !dateOutWip.HasValue))
                    {
                        totalCostPrice += costPrice;
                    }
                }
                else
                {
                    var purchaseInvoiceDetailId = orderwipDetail.FirstOrDefault().Field<int?>($"FK_PURCHASEINVOICEDETAIL{trimmedSourceType}") ?? 0;

                    int goodsReceiptDetailId = 0;
                    if (!sourceType.Contains("COR"))
                    {
                        goodsReceiptDetailId = orderwipDetail.FirstOrDefault().Field<int?>($"FK_GOODSRECEIPTDETAIL{sourceType}") ?? 0;
                    }

                    if (goodsReceiptDetailId > 0)//Wanneer er een goederenontvangst is, alleen meenemen als deze gekoppeld is aan factuur en factuurdatum eerder is dan peildatum
                    {
                        var rsPurchaseInvoice = GetRecordset($"R_PURCHASEINVOICEDETAIL{sourceType}", "FK_PURCHASEINVOICE",
                            $"FK_GOODSRECEIPTDETAIL{sourceType} = {goodsReceiptDetailId}", "");

                        if (rsPurchaseInvoice.RecordCount > 0)
                        {
                            var purchaseInvoiceId = rsPurchaseInvoice.DataTable.AsEnumerable().FirstOrDefault().Field<int?>("FK_PURCHASEINVOICE") ?? 0;

                            var purchaseInvoiceDate = GetRecordset("R_PURCHASEINVOICE", "INVOICEDATE",
                            $"PK_R_PURCHASEINVOICE = {purchaseInvoiceId}", "").DataTable.AsEnumerable().First().Field<DateTime>("INVOICEDATE");

                            if (purchaseInvoiceDate.Date <= date.Date)
                            {
                                totalCostPrice += costPrice;
                            }

                        }
                    }
                    else if (purchaseInvoiceDetailId > 0)//Alleen meenemen als factuurdatum eerder is dan peildatum
                    {
                        var purchaseInvoiceId = GetRecordset($"R_PURCHASEINVOICEDETAIL{trimmedSourceType}", "FK_PURCHASEINVOICE",
                           $"PK_R_PURCHASEINVOICEDETAIL{trimmedSourceType} = {purchaseInvoiceDetailId}", "").DataTable.AsEnumerable().FirstOrDefault().Field<int>("FK_PURCHASEINVOICE");

                        var purchaseInvoiceDate = GetRecordset("R_PURCHASEINVOICE", "INVOICEDATE",
                            $"PK_R_PURCHASEINVOICE = {purchaseInvoiceId}", "").DataTable.AsEnumerable().First().Field<DateTime>("INVOICEDATE");

                        if (purchaseInvoiceDate.Date <= date.Date)
                        {
                            totalCostPrice += costPrice;
                        }
                    }
                }
            }
        }

        double previousResultLogAmount = 0.0;
        var rsPreviousResultLogs = GetRecordset("C_RESULTAATNEMING", "",
            $"FK_ORDER = {id}", "");
        if(rsPreviousResultLogs.RecordCount > 0)
        {
            previousResultLogAmount = rsPreviousResultLogs.DataTable.AsEnumerable().Sum(x => x.Field<double>("AMOUNT"));
        }

        if (expectedMarginPerc == 0.0)
        {
            Log.Error("Error", $"Berekenen verwacht rendement bij order {order.Field<int>("ORDERNUMBER")} over verwachte kosten mislukt, omdat verwacht rendement % nul is. Controleer het tabje 'Financieel' onder de opdrachtomschrijving.", "");
            return;
        }

        double amountToLog = 0.0;
        double expectedMarginOverCosts = 0.0;

        expectedMarginOverCosts = (expectedMarginPerc * 100) / (100 - (expectedMarginPerc * 100));

        amountToLog = (totalCostPrice * expectedMarginOverCosts) - previousResultLogAmount;

        var rsNewResultToLog = GetRecordset("C_RESULTAATNEMING", "",
            $"PK_C_RESULTAATNEMING = -1", "");
        rsNewResultToLog.UseDataChanges = false;
        rsNewResultToLog.UpdateWhenMoveRecord = false;

        rsNewResultToLog.AddNew();
        rsNewResultToLog.Fields["FK_ORDER"].Value = id;
        if(expectedMarginPerc < 0)
        {
            if(rsPreviousResultLogs.RecordCount == 0)
            {
                rsNewResultToLog.Fields["AMOUNT"].Value = expectedMarginPerc * totalNetAmount;
                rsNewResultToLog.Fields["EXPECTEDLOSS"].Value = expectedMarginPerc * totalNetAmount;
            }
            else
            {
                rsNewResultToLog.Fields["AMOUNT"].Value = ((expectedMarginPerc * totalNetAmount) + previousResultLogAmount * -1);
                rsNewResultToLog.Fields["EXPECTEDLOSS"].Value = expectedMarginPerc * totalNetAmount;
            }
        }
        else
        {
            rsNewResultToLog.Fields["AMOUNT"].Value = amountToLog;
        }
        
        rsNewResultToLog.Fields["DATE"].Value = date;
        rsNewResultToLog.Fields["EXPECTEDMARGINOVERCOSTS"].Value = expectedMarginOverCosts;
        rsNewResultToLog.Fields["COSTPRICEONREFERENCEDATE"].Value = totalCostPrice;
        rsNewResultToLog.Fields["PREVIOUSRESULTLOGS"].Value = previousResultLogAmount;

        rsNewResultToLog.Fields["TOTALNETAMOUNT"].Value = totalNetAmount;
        rsNewResultToLog.Fields["EXPECTEDMARGINPERC"].Value = expectedMarginPerc;
        rsNewResultToLog.Fields["EXPECTEDMARGINAMOUNT"].Value = order.Field<double>("EXPECTEDMARGINAMOUNT");
        rsNewResultToLog.Fields["EXPECTEDTOTALCOSTS"].Value = order.Field<double>("EXPECTEDTOTALCOSTS");

        var updateResult = rsNewResultToLog.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            Log.Error("Error", $"Aanmaken resultaatneming mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "");
            return;
        }

    }
}
