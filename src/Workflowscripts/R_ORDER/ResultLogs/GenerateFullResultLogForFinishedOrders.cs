﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class GenerateFullResultLogForFinishedOrders : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;
        var date = Convert.ToDateTime(Parameters["Date"].Value);

        Guid wfFinished = new Guid("eb0e5af0-52b1-4d0e-b203-4a4fb2f089bf");

        var order = GetRecordset("R_ORDER", "REALTOTALMATERIAL, REALTOTALMISC, REALTOTALOUTSOURCED, REALTOTALWORK, INVOICEDAMOUNT, FK_WORKFLOWSTATE",
            $"PK_R_ORDER = {id}", "").DataTable.AsEnumerable().First();

        var wf = order.Field<Guid>("FK_WORKFLOWSTATE");

        if (wf != wfFinished)
        {
            throw new Exception($"Workflowstatus van de order is niet gelijk aan 'Afgesloten'. Genereren volledige resultaatneming niet toegestaan.");
        }

        var totalNetAmount = order.Field<double>("INVOICEDAMOUNT");
        var totalCostPrice = order.Field<double>("REALTOTALMATERIAL") + order.Field<double>("REALTOTALMISC")
            + order.Field<double>("REALTOTALOUTSOURCED") + order.Field<double>("REALTOTALWORK");

        var purchaseinstallmentsNotReceivedDeclarationAccord = GetRecordset("U_PURCHASEINSTALLMENTSPERPURCHASEORDERPERORDER", "PURCHASEINSTALLMENTNETAMOUNTTHISORDER",
            $"FK_ORDER = {id} AND DECLARATIONOFPERFORMANCE = 1", "").DataTable.AsEnumerable().Sum(x => x.Field<double>("PURCHASEINSTALLMENTNETAMOUNTTHISORDER"));

        double previousResultLogAmount = 0.0;
        var rsPreviousResultLogs = GetRecordset("C_RESULTAATNEMING", "",
            $"FK_ORDER = {id}", "");
        if (rsPreviousResultLogs.RecordCount > 0)
        {
            previousResultLogAmount = rsPreviousResultLogs.DataTable.AsEnumerable().Sum(x => x.Field<double>("AMOUNT"));
        }

        var amountToLog = totalCostPrice + purchaseinstallmentsNotReceivedDeclarationAccord - totalNetAmount + previousResultLogAmount;

        var expectedMarginPerc = 0.0;
        double expectedMarginOverCosts = 0.0;
        if (totalCostPrice == 0.0)
        {
            expectedMarginOverCosts = 1.0;
        }
        else if (totalNetAmount == 0.0)
        {
            expectedMarginOverCosts = -1.0;
        }
        else
        {
            expectedMarginPerc = (totalNetAmount - totalCostPrice) / totalNetAmount;
            expectedMarginOverCosts = (expectedMarginPerc * 100) / (100 - (expectedMarginPerc * 100));
        }

        var rsNewResultToLog = GetRecordset("C_RESULTAATNEMING", "",
            $"PK_C_RESULTAATNEMING = -1", "");
        rsNewResultToLog.UseDataChanges = false;
        rsNewResultToLog.UpdateWhenMoveRecord = false;

        rsNewResultToLog.AddNew();
        rsNewResultToLog.Fields["FK_ORDER"].Value = id;

        rsNewResultToLog.Fields["DATE"].Value = date;
        rsNewResultToLog.Fields["EXPECTEDMARGINOVERCOSTS"].Value = expectedMarginOverCosts;
        rsNewResultToLog.Fields["COSTPRICEONREFERENCEDATE"].Value = totalCostPrice;
        rsNewResultToLog.Fields["PREVIOUSRESULTLOGS"].Value = previousResultLogAmount;
        rsNewResultToLog.Fields["AMOUNT"].Value = amountToLog*-1;

        if(amountToLog > 0)
        {
            rsNewResultToLog.Fields["EXPECTEDLOSS"].Value = amountToLog*-1;
        }


        rsNewResultToLog.Fields["TOTALNETAMOUNT"].Value = totalNetAmount;
        rsNewResultToLog.Fields["EXPECTEDMARGINPERC"].Value = expectedMarginPerc;
        rsNewResultToLog.Fields["EXPECTEDMARGINAMOUNT"].Value = totalNetAmount - totalCostPrice;
        rsNewResultToLog.Fields["EXPECTEDTOTALCOSTS"].Value = totalCostPrice;


        var updateResult = rsNewResultToLog.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            Log.Error("Error", $"Aanmaken resultaatneming mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "");
            return;
        }

    }
}
