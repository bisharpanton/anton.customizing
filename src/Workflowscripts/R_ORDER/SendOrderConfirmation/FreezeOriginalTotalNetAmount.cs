﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class FreezeOriginalTotalNetAmount : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //SP
        //13-2-2025
        //Bevries het huidige netto verkoopbedrag in een nieuwe kolom

        var rsOrder = GetRecordset("R_ORDER", "ORIGINALTOTALNETAMOUNT",
            $"PK_R_ORDER = {id}", "");
        rsOrder.UpdateWhenMoveRecord = false;
        rsOrder.UseDataChanges = false;

        rsOrder.MoveFirst();

        bool alreadyFrozen = (double)rsOrder.Fields["ORIGINALTOTALNETAMOUNT"].Value != 0.0;

        if(!alreadyFrozen)
        {
            rsOrder.SetFieldValue("ORIGINALTOTALNETAMOUNT", GetTotalNetAmountExAdditionalWork(id));

            var updateResult = rsOrder.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                Log.Error("Error", $"Bevriezen van netto verkoopbedrag mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "");
                return;
            }
        }
    }

    private double GetTotalNetAmountExAdditionalWork(int id)
    {
        return GetRecordset("R_SALESORDERALLDETAIL", "NETSALESAMOUNT",
            $"FK_ORDER = {id} AND ADDITIONALWORK = 0", "").DataTable.AsEnumerable().Sum(x => x.Field<double>("NETSALESAMOUNT"));
    }
}
