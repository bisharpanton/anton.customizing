﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;
using Ridder.Common;

public class GenerateMeetingNote : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //6-5-2015
        //Genereer vergaderpunt

        //Haal ordergegevens op
        var rsOrder = GetRecordset("R_ORDER",
            "PK_R_ORDER, FK_WORKFLOWSTATE, TOTALNETAMOUNT, REALTOTALMATERIAL, REALTOTALMISC, REALTOTALOUTSOURCED, REALTOTALWORK, INVOICEDAMOUNT",
            $"PK_R_ORDER = {RecordId}", "");
        rsOrder.MoveFirst();

        int orderID = (int)rsOrder.Fields["PK_R_ORDER"].Value;

        //Alleen nieuw vergaderpunt maken als de status niet gelijk is aan 'Afgesloten' of 'Financieel gereed'
        Guid afgesloten = new Guid("eb0e5af0-52b1-4d0e-b203-4a4fb2f089bf");
        Guid finGereed = new Guid("b2cb1324-a085-4823-8cc2-e0c372edfd87");

        Guid status = (Guid)rsOrder.Fields["FK_WORKFLOWSTATE"].Value;

        if (status != afgesloten && status != finGereed)
        {
            //Maak nieuw vergaderpunt
            var rsVergaderpunt = GetRecordset("C_VERGADERPUNTEN", "", "PK_C_VERGADERPUNTEN = -1", "");

            rsVergaderpunt.AddNew();
            rsVergaderpunt.Fields["FK_ORDER"].Value = (int)RecordId;
            rsVergaderpunt.Fields["DATUM"].Value = DateTime.Now;
            rsVergaderpunt.Fields["DESCRIPTION"].Value = "";

            rsVergaderpunt.Fields["OPDRACHTWAARDE"].Value = (double)rsOrder.Fields["TOTALNETAMOUNT"].Value;

            var totaleKosten = (double)rsOrder.Fields["REALTOTALMATERIAL"].Value +
                                  (double)rsOrder.Fields["REALTOTALMISC"].Value +
                                  (double)rsOrder.Fields["REALTOTALOUTSOURCED"].Value +
                                  (double)rsOrder.Fields["REALTOTALWORK"].Value;

            rsVergaderpunt.Fields["KOSTEN"].Value = totaleKosten;
            rsVergaderpunt.Fields["NOGTEONTVINKOOP"].Value = bepaalNogTeOntvInkoop((int)RecordId);
            rsVergaderpunt.Fields["GEFACTUREERD"].Value = (double)rsOrder.Fields["INVOICEDAMOUNT"].Value;
            rsVergaderpunt.Fields["OHW"].Value = totaleKosten - (double)rsOrder.Fields["INVOICEDAMOUNT"].Value;

            rsVergaderpunt.Update();
        }
    }


    public double bepaalNogTeOntvInkoop(int order)
    {
        double result = 0.0;

        var allReservations = GetRecordset("R_ALLRESERVATION",
                "FK_PURCHASEORDERDETAILITEM, FK_PURCHASEORDERDETAILMISC, FK_PURCHASEORDERDETAILOUTSOURCED",
                $"FK_ORDER = {order}", "")
            .DataTable.AsEnumerable();

        if (allReservations.Any(x => x.Field<int?>("FK_PURCHASEORDERDETAILITEM").HasValue))
        {
            var purchaseOrderDetailIds = allReservations
                .Where(x => x.Field<int?>("FK_PURCHASEORDERDETAILITEM").HasValue)
                .Select(x => x.Field<int>("FK_PURCHASEORDERDETAILITEM")).Distinct().ToList();

            result += GetRecordset("R_PURCHASEORDERDETAILITEM",
                    "QUANTITY, QUANTITYTORECEIVE, NETPURCHASEPRICE",
                    $"PK_R_PURCHASEORDERDETAILITEM IN ({string.Join(",", purchaseOrderDetailIds)}) AND QUANTITY <> 0.0",
                    "")
                .DataTable.AsEnumerable().Sum(x =>
                    (x.Field<double>("QUANTITYTORECEIVE") * x.Field<double>("NETPURCHASEPRICE") /
                     x.Field<double>("QUANTITY")));
        }


        if (allReservations.Any(x => x.Field<int?>("FK_PURCHASEORDERDETAILMISC").HasValue))
        {
            var purchaseOrderDetailIds = allReservations
                .Where(x => x.Field<int?>("FK_PURCHASEORDERDETAILMISC").HasValue)
                .Select(x => x.Field<int>("FK_PURCHASEORDERDETAILMISC")).Distinct().ToList();

            result += GetRecordset("R_PURCHASEORDERDETAILMISC",
                    "QUANTITY, QUANTITYTORECEIVE, NETPURCHASEPRICE",
                    $"PK_R_PURCHASEORDERDETAILMISC IN ({string.Join(",", purchaseOrderDetailIds)}) AND QUANTITY <> 0.0",
                    "")
                .DataTable.AsEnumerable().Sum(x =>
                    (x.Field<double>("QUANTITYTORECEIVE") * x.Field<double>("NETPURCHASEPRICE") /
                     x.Field<double>("QUANTITY")));
        }

        if (allReservations.Any(x => x.Field<int?>("FK_PURCHASEORDERDETAILOUTSOURCED").HasValue))
        {
            var purchaseOrderDetailIds = allReservations
                .Where(x => x.Field<int?>("FK_PURCHASEORDERDETAILOUTSOURCED").HasValue)
                .Select(x => x.Field<int>("FK_PURCHASEORDERDETAILOUTSOURCED")).Distinct().ToList();

            result += GetRecordset("R_PURCHASEORDERDETAILOUTSOURCED",
                    "QUANTITY, QUANTITYTORECEIVE, NETPURCHASEPRICE",
                    $"PK_R_PURCHASEORDERDETAILOUTSOURCED IN ({string.Join(",", purchaseOrderDetailIds)}) AND QUANTITY <> 0.0",
                    "")
                .DataTable.AsEnumerable().Sum(x =>
                    (x.Field<double>("QUANTITYTORECEIVE") * x.Field<double>("NETPURCHASEPRICE") /
                     x.Field<double>("QUANTITY")));
        }

        return result;
    }
}
