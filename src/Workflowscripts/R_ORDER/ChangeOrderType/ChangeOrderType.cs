﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;
using Ridder.Common.ItemManagement;

public class ChangeOrderType : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //15-9-2020
        //Wijzig ordertype, dit is alleen toegestaan bij vaste prijsafspraak, regie en garantie

        var orderType = ConvertParameterToCorrectOrdertype(Convert.ToInt32(Parameters["Ordertype"].Value));

        if (orderType == 0)
        {
            throw new Exception("Bepaling ordertype mislukt.");
        }

        var rsOrder = GetRecordset("R_ORDER", "ORDERTYPE, FK_INVOICESCHEDULE",
            $"PK_R_ORDER = {id}", "");
        rsOrder.UseDataChanges = false; //Trigger geen business logica
        rsOrder.UpdateWhenMoveRecord = false;

        rsOrder.MoveFirst();

        if ((OrderType)rsOrder.GetField("ORDERTYPE").Value == OrderType.FixedPrice &&
            ((OrderType)orderType == OrderType.Time_Material || (OrderType)orderType == OrderType.Warranty))
        {
            /*Log.Info("Info",
                $"Bij het wijzigen van Vaste prijsafspraak naar Regie is de termijncode niet meer verplicht. Verwijder de termijncode eventueel handmatig indien deze niet meer van toepassing is.",
                "");*/
            var salesInstallmentPresent = GetRecordset("R_SALESINSTALLMENT", "PK_R_SALESINSTALLMENT",
                $"FK_ORDER = {id}", "").RecordCount > 0;

            if(salesInstallmentPresent)
            {
                Log.Info("Error",
                $"Het wijzigen van Vaste prijsafspraak naar Regie of garantie is niet toegstaan indien er betaaltermijnen aanwezig zijn. Verwijder eerst de betaaltermijnen.",
                "");
                return;
            }

            var shippingOrderPresent = GetRecordset("R_SHIPPINGORDER", "PK_R_SHIPPINGORDER",
                $"FK_ORDER = {id}", "").RecordCount > 0;

            if (!shippingOrderPresent)
            {
                rsOrder.SetFieldValue("FK_INVOICESCHEDULE", DBNull.Value);
            }      
        }

        rsOrder.SetFieldValue("ORDERTYPE", orderType);
        rsOrder.Update();
    }

    private int ConvertParameterToCorrectOrdertype(int parameterValue)
    {
        switch (parameterValue)
        {
            case 1:
                return (int)OrderType.FixedPrice;
            case 2:
                return (int)OrderType.Time_Material;
            case 3:
                return (int)OrderType.Warranty;
            default:
                return 0;
        }
    }
}
