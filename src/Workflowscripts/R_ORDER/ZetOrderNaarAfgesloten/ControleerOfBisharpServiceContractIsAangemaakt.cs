﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class ControleerOfBisharpServiceContractIsAangemaakt : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //9-10-2024
        //Controleer of service contract is aangemaakt bij Bisharp

        if (!GetUserInfo().CompanyName.Equals("Bisharp"))
        {
            return;
        }

        var order = GetRecordset("R_ORDER", "DESCRIPTION, SERVICECONTRACTCREATED", $"PK_R_ORDER = {id}", "")
            .DataTable.AsEnumerable().First();

        if(order.Field<string>("DESCRIPTION").ToLower().Contains("diensten"))
        {
            return;
        }

        if(!order.Field<bool>("SERVICECONTRACTCREATED"))
        {
            Log.Error("Error", "Er is nog geen service contract aangemaakt. Recurring Revenue moeten we hebben ;)", "");
            return;
        }
    }
}
