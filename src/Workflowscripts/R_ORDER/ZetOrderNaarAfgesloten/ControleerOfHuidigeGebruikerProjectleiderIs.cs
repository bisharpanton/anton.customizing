﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class ControleerOfHuidigeGebruikerProjectleiderIs : WorkflowScriptInfo
{
    public Boolean Execute()
    {
        var id = (int)RecordId;

        //DB
        //31-8-2015
        //Controleer of de huidige gebruiker gelijk is aan de projectleider van de order. Anders foutmelding teruggeven. 

        //Check niet nodig indien de huidige gebruiker gekoppeld is aan de rol 'Administrator', 'Controller' of 'Assistent controller'
        if (UserLinkedToFinancialRole(CurrentUser.UserID))
        {
            return true;
        }

        var rsOrder = GetRecordset("R_ORDER", "FK_PLANNER",
            $"PK_R_ORDER = '{id}'", "");
        rsOrder.MoveFirst();

        int projectleider = (int)rsOrder.Fields["FK_PLANNER"].Value;

        return (projectleider == CurrentUser.EmployeeID) ? true : false;
    }

    private bool UserLinkedToFinancialRole(int currentUserUserId)
    {
        var roleIds = GetRecordset("R_ROLEUSER", "FK_R_ROLE",
                $"FK_R_USER = {currentUserUserId}", "")
            .DataTable.AsEnumerable().Select(x => x.Field<int>("FK_R_ROLE")).ToList();

        var roleNames = GetRecordset("R_ROLE", "NAME",
                $"PK_R_ROLE IN ({string.Join(",", roleIds)})", "")
            .DataTable.AsEnumerable().Select(x => x.Field<string>("NAME")).ToList();

        return roleNames.Contains("Administrator") || roleNames.Contains("Controller") ||
               roleNames.Contains("Assistent Controller");
    }
}
