﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class ControleerOfAlleVerkoopregelsGeleverdZijn : WorkflowScriptInfo
{
    public Boolean Execute()
    {
        var id = (int)RecordId;

        //DB
        //Controleer of alles is uitgeleverd

        //Er moet wel minstens één verkoopregel aanwezig zijn. 
        var rsVerkoopregels = GetRecordset("R_SALESORDERALLDETAIL",
            "PK_R_SALESORDERALLDETAIL, DELIVERYCOMPLETE" , 
            $"FK_ORDER = {id}", "");

        if (rsVerkoopregels.RecordCount == 0)
        {
            return true; //Uitlevering is niet van toepassing
        }

        return rsVerkoopregels.DataTable.AsEnumerable().All(x => x.Field<bool>("DELIVERYCOMPLETE"));
    }
}
