﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class ControleerSlottermijnAanwezig : WorkflowScriptInfo
{
    public Boolean Execute()
    {
        var id = (int)RecordId;

        //Controleer of slottermijn aanwezig is.

        var rsOrder = GetRecordset("R_ORDER", "FK_INVOICESCHEDULE, ORDERTYPE", 
            $"PK_R_ORDER = {id}", "");
        rsOrder.MoveFirst();

        if(!(rsOrder.Fields["FK_INVOICESCHEDULE"].Value as int?).HasValue)
        {
            return true;
        }

        return GetRecordset("R_SALESINSTALLMENT", "PK_R_SALESINSTALLMENT",
                $"FK_ORDER = {id} AND FINALINSTALLMENT = 1 AND FK_SALESINVOICE IS NOT NULL", "")
            .DataTable.AsEnumerable().Any();
    }
}
