﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class ControleerOfVerkoopregelsIngekochtZijn : WorkflowScriptInfo
{
    public Boolean Execute()
    {
        var id = (int)RecordId;

        //DB
        //14-05-2019
        //Controleer of verkoopregels ingekocht zijn

        //Arrived = 2 = Onwaar
        var rsSalesMaterialState = GetRecordset("R_SALESMATERIALSTATE", "PK_R_SALESMATERIALSTATE",
            $"FK_ORDER = {id} AND REGISTRATIONPATH = 3 AND ARRIVED = 2", "");

        return rsSalesMaterialState.RecordCount == 0;
    }
}
