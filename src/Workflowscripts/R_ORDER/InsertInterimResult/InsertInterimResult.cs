﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;
using Ridder.Common;

public class InsertInterimResult : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //29-7-2019
        //Voeg een resultaatneming in

        var amount = Convert.ToDouble(Parameters["Amount"].Value);
        var date = Convert.ToDateTime(Parameters["Date"].Value);

        var rsResultaatneming = GetRecordset("C_RESULTAATNEMING", "",
            $"PK_C_RESULTAATNEMING = -1", "");
        rsResultaatneming.UseDataChanges = false;
        rsResultaatneming.AddNew();
        rsResultaatneming.Fields["FK_ORDER"].Value = id;
        rsResultaatneming.Fields["AMOUNT"].Value = amount;
        rsResultaatneming.Fields["DATE"].Value = date;
        rsResultaatneming.Update();

    }
}
