﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class RecalculateRemainingCost : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //07-05-2019
        //De standaard houdt geen rekening met 'Bedrag inkooporders nieuw'. Hercalculeer het veld 'Te gaan' zodat 
        //wel met dit bedrag rekening wordt gehouden.

        var rsBudgetDetails = GetRecordset("R_PROJECTBUDGETDETAIL", "", $"FK_MAINPROJECT = {id} AND AMOUNTPURCHASEORDERSNEW <> 0.0", "");

        if (rsBudgetDetails.RecordCount == 0)
        {
            return;
        }

        rsBudgetDetails.UpdateWhenMoveRecord = false;
        rsBudgetDetails.UseDataChanges = true;

        rsBudgetDetails.MoveFirst();
        while (!rsBudgetDetails.EOF)
        {
            /* DB - 2024-02-01 - Onderstaande heeft jaren goed gewerkt, maar er is nu één situatie waarbij het OHW + aangegane verplichten zijn én negatieve inkoopbedrag nieuw
             * en nu gaat dit mis. Ridder gaat namelijk EXPECTEDCOSTPRICE vullen met PURCHASEDCOSTPRICE + WIPCOSTPRICE en zet dan Te gaan op nul. 
             * Hierdoor zijn we de oorspronkelijke waarden EXPECTEDCOSTPRICE en Te gaan kwijt. We hebben nu een FREEZEEXPECTEDCOSTPRICE ingericht om de oorspronkelijke waarde 
             * inclusief Inkooporders Nieuw te onthouden en terug te zetten
             */

            /*
            var newRemainingCost = (double)rsBudgetDetails.Fields["EXPECTEDCOSTPRICE"].Value -
                                   ((double)rsBudgetDetails.Fields["AMOUNTPURCHASEORDERSNEW"].Value +
                                    (double)rsBudgetDetails.Fields["PURCHASEDCOSTPRICE"].Value +
                                    (double)rsBudgetDetails.Fields["WIPCOSTPRICE"].Value);

            if (newRemainingCost >= 0.0)
            {
                rsBudgetDetails.Fields["REMAININGCOST"].Value = newRemainingCost;
            }
            else
            {
                rsBudgetDetails.Fields["REMAININGCOST"].Value = 0.0;
                rsBudgetDetails.Fields["EXPECTEDCOSTPRICE"].Value =
                    (double)rsBudgetDetails.Fields["EXPECTEDCOSTPRICE"].Value + (newRemainingCost * -1);
            }*/

            var newExpectedCostprice = (double)rsBudgetDetails.Fields["AMOUNTPURCHASEORDERSNEW"].Value + (double)rsBudgetDetails.Fields["PURCHASEDCOSTPRICE"].Value + (double)rsBudgetDetails.Fields["WIPCOSTPRICE"].Value;

            rsBudgetDetails.Fields["EXPECTEDCOSTPRICE"].Value = newExpectedCostprice > (double)rsBudgetDetails.Fields["FREEZEEXPECTEDCOSTPRICE"].Value
                ? newExpectedCostprice
                : (double)rsBudgetDetails.Fields["FREEZEEXPECTEDCOSTPRICE"].Value; //Datachange staat aan en zorgt voor hercalculatie 'Te gaan' inclusief inkooporder nieuw

            rsBudgetDetails.MoveNext();
        }

        rsBudgetDetails.MoveFirst();
        rsBudgetDetails.Update();
    }
}
