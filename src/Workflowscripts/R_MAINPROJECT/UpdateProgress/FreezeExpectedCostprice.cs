﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class FreezeExpectedCostprice : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //1-2-2024
        //Ridder gaat de EXPECTEDCOSTPRICE vullen met PURCHASEDCOSTPRICE + WIPCOSTPRICE en zet dan Te gaan op nul. 
        //Hierdoor zijn we de oorspronkelijke waarden EXPECTEDCOSTPRICE en Te gaan kwijt. Sla de EXPECTEDCOSTPRICE op in FREEZEEXPECTEDCOSTPRICE en na voortgang bijwerken zetten we hem weer terug

        var rsBudgetDetails = GetRecordset("R_PROJECTBUDGETDETAIL", "EXPECTEDCOSTPRICE, FREEZEEXPECTEDCOSTPRICE", $"FK_MAINPROJECT = {id}", "");

        if (rsBudgetDetails.RecordCount == 0)
        {
            return;
        }

        rsBudgetDetails.UpdateWhenMoveRecord = false;
        rsBudgetDetails.UseDataChanges = false;

        rsBudgetDetails.MoveFirst();
        while (!rsBudgetDetails.EOF)
        {
            rsBudgetDetails.Fields["FREEZEEXPECTEDCOSTPRICE"].Value = (double)rsBudgetDetails.Fields["EXPECTEDCOSTPRICE"].Value;

            rsBudgetDetails.MoveNext();
        }

        rsBudgetDetails.MoveFirst();
        rsBudgetDetails.Update();
    }
}
