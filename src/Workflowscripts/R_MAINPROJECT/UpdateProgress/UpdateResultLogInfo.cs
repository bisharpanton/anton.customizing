﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class UpdateResultLogInfo : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        var orderId = GetRecordset("R_MAINPROJECT", "FK_ORDER",
            $"PK_R_MAINPROJECT = {id}", "").DataTable.AsEnumerable().FirstOrDefault().Field<int?>("FK_ORDER") ?? 0;

        if(orderId == 0)
        {
            return;//Geen hoofdorder gekoppeld aan het project
        }

        var rsOrder = GetRecordset("R_ORDER", "TOTALNETAMOUNT, EXPECTEDMARGINAMOUNT, EXPECTEDTOTALCOSTS, EXPECTEDMARGINPERC, ADDITIONALWORKPROJECT",
                $"PK_R_ORDER = {orderId}", "");
        rsOrder.MoveFirst();

        var totalNetAmount = (double)rsOrder.GetField("TOTALNETAMOUNT").Value;

        if(totalNetAmount == 0.0)
        {
            return;//Wanneer er geen opdrachtwaarde is, is dit een intern project
        }

        double addWorkAmount = 0.0;

        var offerIds = GetRecordset("R_OFFER", "PK_R_OFFER",
            $"FK_MAINPROJECT = {id} AND FK_WORKFLOWSTATE = '5b0811e8-171f-49fd-a4bf-8bbd4a088421'", "").DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_OFFER")).ToList();

        if(offerIds.Any())
        {
            foreach(var offerId in offerIds)
            {
                var probabilityOfSale = GetRecordset("R_OFFER", "PROBABILITYOFSALE",
                    $"PK_R_OFFER = {offerId}", "").DataTable.AsEnumerable().First().Field<double>("PROBABILITYOFSALE");

                var offerDetailAmount = GetRecordset("R_OFFERDETAILMISC", "NETSALESAMOUNT",
                    $"FK_OFFER = {offerId} AND KOSTENINPROJECT = 1", "")
                    .DataTable.AsEnumerable().Sum(x => x.Field<double>("NETSALESAMOUNT"));

                var amountToAdd = offerDetailAmount * probabilityOfSale;

                addWorkAmount += amountToAdd;
            }
        }

        double expectedCosts = 0.0;

        var projectBudgetDetailIds = GetRecordset("R_PROJECTBUDGETDETAIL", "PK_R_PROJECTBUDGETDETAIL",
            $"FK_MAINPROJECT = {id}", "").DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_PROJECTBUDGETDETAIL")).ToList();

        if(projectBudgetDetailIds.Any())
        {
            foreach(var projectBudgetDetailId in projectBudgetDetailIds)
            {
                var expectedCost = GetRecordset("R_PROJECTBUDGETDETAIL", "EXPECTEDCOSTPRICE",
                    $"PK_R_PROJECTBUDGETDETAIL = {projectBudgetDetailId}", "").DataTable.AsEnumerable().First().Field<double>("EXPECTEDCOSTPRICE");

                var expectedCostAddWork = GetRecordset("U_ADDITIONALWORKLOGS", "AMOUNT",
                    $"FK_PROJECTBUDGETDETAIL = {projectBudgetDetailId} AND TYPE = 1", "").DataTable.AsEnumerable().Sum(x => x.Field<double>("AMOUNT"));

                var amountToAdd = expectedCost + expectedCostAddWork;

                expectedCosts += amountToAdd;
            }
        }

        var totalAmountInclAddWork = totalNetAmount + addWorkAmount;

        var expectedResult = totalAmountInclAddWork - expectedCosts;

        var margin = expectedResult / totalAmountInclAddWork;

        rsOrder.UseDataChanges = false;
        rsOrder.UpdateWhenMoveRecord = false;

        rsOrder.SetFieldValue("EXPECTEDMARGINAMOUNT", expectedResult);
        rsOrder.SetFieldValue("EXPECTEDTOTALCOSTS", expectedCosts);
        rsOrder.SetFieldValue("EXPECTEDMARGINPERC", margin);
        rsOrder.SetFieldValue("ADDITIONALWORKPROJECT", addWorkAmount);

        var updateResult = rsOrder.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            Log.Error("Error", $"Updaten van invulvelden t.b.v. resultaatneming mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "");
        }

    }
}
