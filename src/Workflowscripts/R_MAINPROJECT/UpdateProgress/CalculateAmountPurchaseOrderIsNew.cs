﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;
using Ridder.Common.Stock;

public class CalculateAmountPurchaseOrderIsNew : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //30-04-2019
        //Bij ARI wil men bij de aangegane verplichten ook de inkooporders met status Nieuw meenemen.
        //Daarom rekenen we deze uit op dit moment en slaan we die op in een custom-veld

        var rsCRM = GetRecordset("R_CRMSETTINGS", "PURCHASEORDERSNEWINPURCHASECOMMITMENT", "", "");
        rsCRM.MoveFirst();

        if (!(bool)rsCRM.Fields["PURCHASEORDERSNEWINPURCHASECOMMITMENT"].Value)
        {
            return;
        }

        var budgetDetails = GetBudgetDetailsWithLinkedJoborders(id);

        if (!budgetDetails.Any())
        {
            return;
        }

        CalculateAmountPurchaseOrdersNew(budgetDetails);
    }

    private void CalculateAmountPurchaseOrdersNew(List<BudgetDetailWithLinkedJoborders> budgetDetails)
    {
        var allReservations = GetRecordset("R_ALLRESERVATION",
                "FK_PRODUCTIONJOBORDER, FK_PURCHASEORDER, FK_PURCHASEORDERDETAILITEM, FK_PURCHASEORDERDETAILMISC, FK_PURCHASEORDERDETAILOUTSOURCED",
                $"FK_PRODUCTIONJOBORDER IN ({string.Join(",", budgetDetails.SelectMany(x => x.Joborders))}) AND FK_PURCHASEORDER IS NOT NULL",
                "")
            .DataTable.AsEnumerable();

        if (!allReservations.Any())
        {
            ResetAmountPurchaseOrdersNew(budgetDetails);

            return;
        }

        var wfStateNew = new Guid("0fc61ed2-0da1-4832-9644-635ef6e5bea6");
        var purchaseOrdersNew = GetRecordset("R_PURCHASEORDER", "PK_R_PURCHASEORDER",
                $"FK_WORKFLOWSTATE = '{wfStateNew}'", "")
            .DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_PURCHASEORDER")).ToList();

        if (!purchaseOrdersNew.Any())
        {
            ResetAmountPurchaseOrdersNew(budgetDetails);

            return;
        }

        var allReservationsWithPurchaseOrderNew = allReservations
            .Where(x => purchaseOrdersNew.Contains(x.Field<int>("FK_PURCHASEORDER"))).ToList();

        var purchaseOrderDetailItemIds =
            allReservationsWithPurchaseOrderNew.Where(x => x.Field<int?>("FK_PURCHASEORDERDETAILITEM").HasValue)
                .Select(x => x.Field<int>("FK_PURCHASEORDERDETAILITEM")).ToList();
        purchaseOrderDetailItemIds.Add(0); //Avoid empty list

        var purchaseOrderDetailsItem = GetRecordset("R_PURCHASEORDERDETAILITEM",
                "PK_R_PURCHASEORDERDETAILITEM, FK_PURCHASEORDER, NETPURCHASEPRICE",
                $"PK_R_PURCHASEORDERDETAILITEM IN ({string.Join(",", purchaseOrderDetailItemIds)})", "")
            .DataTable.AsEnumerable();

        var purchaseOrderDetailMiscIds =
            allReservationsWithPurchaseOrderNew.Where(x => x.Field<int?>("FK_PURCHASEORDERDETAILMISC").HasValue)
                .Select(x => x.Field<int>("FK_PURCHASEORDERDETAILMISC")).ToList();
        purchaseOrderDetailMiscIds.Add(0); //Avoid empty list

        var purchaseOrderDetailsMisc = GetRecordset("R_PURCHASEORDERDETAILMISC",
                "PK_R_PURCHASEORDERDETAILMISC, FK_PURCHASEORDER, NETPURCHASEPRICE",
                $"PK_R_PURCHASEORDERDETAILMISC IN ({string.Join(",", purchaseOrderDetailMiscIds)})", "")
            .DataTable.AsEnumerable();

        var purchaseOrderDetailOutsourcedIds =
            allReservationsWithPurchaseOrderNew.Where(x => x.Field<int?>("FK_PURCHASEORDERDETAILOUTSOURCED").HasValue)
                .Select(x => x.Field<int>("FK_PURCHASEORDERDETAILOUTSOURCED")).ToList();
        purchaseOrderDetailOutsourcedIds.Add(0); //Avoid empty list

        var purchaseOrderDetailsOutsourced = GetRecordset("R_PURCHASEORDERDETAILOUTSOURCED",
                "PK_R_PURCHASEORDERDETAILOUTSOURCED, FK_PURCHASEORDER, NETPURCHASEPRICE",
                $"PK_R_PURCHASEORDERDETAILOUTSOURCED IN ({string.Join(",", purchaseOrderDetailOutsourcedIds)})", "")
            .DataTable.AsEnumerable();


        //Loop door de budgetregels en bereken het totale inkoopbedrag met status Nieuw
        foreach (var budgetDetail in budgetDetails)
        {
            var totalAmount = 0.0;

            if (budgetDetail.BudgetDetail.Material)
            {
                var purchaseOrderDetailsItemThisBudgetDetail = allReservationsWithPurchaseOrderNew
                    .Where(x => budgetDetail.Joborders.Contains(x.Field<int>("FK_PRODUCTIONJOBORDER"))
                    && x.Field<int?>("FK_PURCHASEORDERDETAILITEM").HasValue)
                    .Select(x => x.Field<int>("FK_PURCHASEORDERDETAILITEM")).ToList();

                totalAmount += purchaseOrderDetailsItem
                    .Where(x =>
                        purchaseOrderDetailsItemThisBudgetDetail.Contains(x.Field<int>("PK_R_PURCHASEORDERDETAILITEM")))
                    .Sum(y => y.Field<double>("NETPURCHASEPRICE"));
            }

            if (budgetDetail.BudgetDetail.Misc)
            {
                var purchaseOrderDetailsMiscThisBudgetDetail = allReservationsWithPurchaseOrderNew
                    .Where(x => budgetDetail.Joborders.Contains(x.Field<int>("FK_PRODUCTIONJOBORDER"))
                                && x.Field<int?>("FK_PURCHASEORDERDETAILMISC").HasValue)
                    .Select(x => x.Field<int>("FK_PURCHASEORDERDETAILMISC")).ToList();

                totalAmount += purchaseOrderDetailsMisc
                    .Where(x => purchaseOrderDetailsMiscThisBudgetDetail.Contains(x.Field<int>("PK_R_PURCHASEORDERDETAILMISC")))
                    .Sum(y => y.Field<double>("NETPURCHASEPRICE"));
            }

            if (budgetDetail.BudgetDetail.Outsourced)
            {
                var purchaseOrderDetailsOutsourcedThisBudgetDetail = allReservationsWithPurchaseOrderNew
                    .Where(x => budgetDetail.Joborders.Contains(x.Field<int>("FK_PRODUCTIONJOBORDER"))
                                && x.Field<int?>("FK_PURCHASEORDERDETAILOUTSOURCED").HasValue)
                    .Select(x => x.Field<int>("FK_PURCHASEORDERDETAILOUTSOURCED")).ToList();

                totalAmount += purchaseOrderDetailsOutsourced
                    .Where(x => purchaseOrderDetailsOutsourcedThisBudgetDetail.Contains(
                        x.Field<int>("FK_PURCHASEORDERDETAILOUTSOURCED")))
                    .Sum(y => y.Field<double>("NETPURCHASEPRICE"));
            }

            if (Math.Abs(totalAmount - budgetDetail.BudgetDetail.AmountPurchaseOrdersNew) > 0.001)
            {
                budgetDetail.BudgetDetail.AmountPurchaseOrdersNew = totalAmount;
                budgetDetail.BudgetDetail.UpdateBudgetDetail = true;
            }
        }

        if (budgetDetails.Any(x => x.BudgetDetail.UpdateBudgetDetail))
        {
            var budgetDetailIdsToUpdate = budgetDetails
                .Where(x => x.BudgetDetail.UpdateBudgetDetail)
                .Select(x => x.BudgetDetail.BudgetDetailId).ToList();

            var rsBudgetDetailsToUpdate = GetRecordset("R_PROJECTBUDGETDETAIL",
                "PK_R_PROJECTBUDGETDETAIL, AMOUNTPURCHASEORDERSNEW",
                $"PK_R_PROJECTBUDGETDETAIL IN ({string.Join(",", budgetDetailIdsToUpdate)})",
                "");
 
            rsBudgetDetailsToUpdate.UseDataChanges = false;
            rsBudgetDetailsToUpdate.UpdateWhenMoveRecord = false;

            rsBudgetDetailsToUpdate.MoveFirst();
            while (!rsBudgetDetailsToUpdate.EOF)
            {
                var budgetDetail = budgetDetails
                    .First(x => x.BudgetDetail.BudgetDetailId == (int)rsBudgetDetailsToUpdate.Fields["PK_R_PROJECTBUDGETDETAIL"].Value);

                rsBudgetDetailsToUpdate.Fields["AMOUNTPURCHASEORDERSNEW"].Value = budgetDetail.BudgetDetail.AmountPurchaseOrdersNew;


                rsBudgetDetailsToUpdate.MoveNext();
            }

            rsBudgetDetailsToUpdate.MoveFirst();
            rsBudgetDetailsToUpdate.Update();
        }
    }

    private void ResetAmountPurchaseOrdersNew(List<BudgetDetailWithLinkedJoborders> budgetDetails)
    {
        var budgetDetailIdsToReset = budgetDetails
            .Where(x => Math.Abs(x.BudgetDetail.AmountPurchaseOrdersNew) > 0.001)
            .Select(x => x.BudgetDetail.BudgetDetailId).ToList();

        if (!budgetDetailIdsToReset.Any())
        {
            return;
        }

        var rsBudgetDetailsToUpdate = GetRecordset("R_PROJECTBUDGETDETAIL",
            "PK_R_PROJECTBUDGETDETAIL, AMOUNTPURCHASEORDERSNEW",
            $"PK_R_PROJECTBUDGETDETAIL IN ({string.Join(",", budgetDetailIdsToReset)})",
            "");

        rsBudgetDetailsToUpdate.UseDataChanges = false;
        rsBudgetDetailsToUpdate.UpdateWhenMoveRecord = false;

        rsBudgetDetailsToUpdate.MoveFirst();
        while (!rsBudgetDetailsToUpdate.EOF)
        {
            rsBudgetDetailsToUpdate.Fields["AMOUNTPURCHASEORDERSNEW"].Value = 0.0;

            rsBudgetDetailsToUpdate.MoveNext();
        }

        rsBudgetDetailsToUpdate.MoveFirst();
        rsBudgetDetailsToUpdate.Update();
    }

    private List<BudgetDetailWithLinkedJoborders> GetBudgetDetailsWithLinkedJoborders(int project)
    {
        //ARI budgetteert altijd 'Alle materiaal' / 'Alle divers' / 'Alle uitbesteed werk'.
        //We gaan er voor het gemak vanuit dat er geen specifieke bron wordt ingegeven.
        //Budgettype Arbeid hoef niet nagelopen te worden
        var budgetDetails = GetRecordset("R_PROJECTBUDGETDETAIL",
            "PK_R_PROJECTBUDGETDETAIL, MATERIAL, MISC, OUTSOURCED, AMOUNTPURCHASEORDERSNEW",
            $"FK_MAINPROJECT = {project} AND (MATERIAL = 1 OR MISC = 1 OR OUTSOURCED =1)", "").DataTable.AsEnumerable()
            .Select(x => new BudgetDetail()
            {
                BudgetDetailId = x.Field<int>("PK_R_PROJECTBUDGETDETAIL"),
                Material = x.Field<bool>("MATERIAL"),
                Misc = x.Field<bool>("MISC"),
                Outsourced = x.Field<bool>("OUTSOURCED"),
                AmountPurchaseOrdersNew = x.Field<double>("AMOUNTPURCHASEORDERSNEW"),
                UpdateBudgetDetail = false,
            });

        var budgetDetailIds = budgetDetails.Select(x => x.BudgetDetailId).ToList();

        if (!budgetDetails.Any())
        {
            return new List<BudgetDetailWithLinkedJoborders>();
        }

        var tableInfoBudgetDetails = new Guid("51e8daf8-b92c-4852-9518-433679420946");
        var tableInfoPhases = new Guid("7db2fca6-1cb4-4a2c-9f96-3b4376a5d37b");
        var tableInfoJoborders = new Guid("7de0ac3e-9dfa-47e0-b113-63d3cac55f90");

        var phases = GetRecordset("R_WORKBREAKDOWNINFO", "PARENTRECORDID, RECORDID",
            $"FK_MAINPROJECT = {project} AND FK_PARENTTABLEINFO = '{tableInfoPhases}' AND FK_TABLEINFO = '{tableInfoBudgetDetails}' AND RECORDID IN ({string.Join(",", budgetDetailIds)})",
            "").DataTable.AsEnumerable().Select(x => new
            {
                Phase = x.Field<int>("PARENTRECORDID"),
                BudgetDetail = x.Field<int>("RECORDID")
            }).ToList();

        if (!phases.Any())
        {
            return new List<BudgetDetailWithLinkedJoborders>();
        }

        var phaseIds = phases.Select(x => x.Phase).Distinct().ToList();
        var jobordersUnderPhases = GetRecordset("R_WORKBREAKDOWNINFO", "PARENTRECORDID, RECORDID",
            $"FK_MAINPROJECT = {project} AND FK_PARENTTABLEINFO = '{tableInfoPhases}' AND FK_TABLEINFO = '{tableInfoJoborders}' AND PARENTRECORDID IN ({string.Join(",", phaseIds)})",
            "").DataTable.AsEnumerable().Select(x => new
            {
                Phase = x.Field<int>("PARENTRECORDID"),
                Joborder = x.Field<int>("RECORDID"),
            }).ToList();

        var result = new List<BudgetDetailWithLinkedJoborders>();

        foreach (var budgetDetail in budgetDetails)
        {
            var phase = phases
                .FirstOrDefault(x => x.BudgetDetail == budgetDetail.BudgetDetailId);

            if (phase == null)
            {
                result.Add(new BudgetDetailWithLinkedJoborders()
                {
                    BudgetDetail = budgetDetail,
                    Joborders = new List<int>(),
                });

                continue;
            }

            var linkedJoborders = jobordersUnderPhases.Where(x => x.Phase == phase.Phase);

            if (!linkedJoborders.Any())
            {
                result.Add(new BudgetDetailWithLinkedJoborders()
                {
                    BudgetDetail = budgetDetail,
                    Joborders = new List<int>(),
                });

                continue;
            }

            result.Add(new BudgetDetailWithLinkedJoborders()
            {
                BudgetDetail = budgetDetail,
                Joborders = linkedJoborders.Select(x => x.Joborder).ToList(),
            });
        }

        return result;
    }

    public class BudgetDetailWithLinkedJoborders
    {
        public BudgetDetail BudgetDetail { get; set; }
        public List<int> Joborders { get; set; }
    }

    public class BudgetDetail
    {
        public int BudgetDetailId { get; set; }
        public bool Material { get; set; }
        public bool Misc { get; set; }
        public bool Outsourced { get; set; }
        public double AmountPurchaseOrdersNew { get; set; }
        public bool UpdateBudgetDetail { get; set; }
    }
}
