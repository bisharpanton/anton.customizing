﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class UpdateLastDateUpdateProgress : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //30-04-2019
        //Opslaan datum

        var rsProject = GetRecordset("R_MAINPROJECT", "LASTDATEUPDATEPROGRESS",
            $"PK_R_MAINPROJECT = {id}", "");
        rsProject.MoveFirst();

        rsProject.Fields["LASTDATEUPDATEPROGRESS"].Value = DateTime.Now;
        rsProject.Update();
    }
}
