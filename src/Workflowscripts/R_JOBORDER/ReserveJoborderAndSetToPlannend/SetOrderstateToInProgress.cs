﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class SetOrderstateToInProgress : WorkflowScriptInfo
{
    public void Execute()
    {
        // HVE
        // 4-8-2020
        // Set order to In Progress

        var joborderId = (int)RecordId;

        var orderId = GetOrderId(joborderId);
        if (CheckWorkflowstateIsNew(orderId))
        {
            var wfSetStateToInprogress = new Guid("811f0af2-261e-47f6-9e53-1f1213770aee");
            ExecuteWorkflowEvent("R_ORDER", orderId, wfSetStateToInprogress, null);
        } 
        

    }

    private bool CheckWorkflowstateIsNew(int orderId)
    {
        var wfStateNew = new Guid("6205d9d2-72d7-49df-8b4b-6e5c1c3d4927");
        var wfSateOrder = GetWfStateOrder(orderId);

        return wfStateNew == wfSateOrder ? true : false;

    }

    private Guid GetWfStateOrder(int orderId)
    {
        return GetRecordset("R_ORDER", "FK_WORKFLOWSTATE", $"PK_R_ORDER = {orderId}", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<Guid>("FK_WORKFLOWSTATE"))
            .First();
    }

    private int GetOrderId(int joborderId)
    {
        return GetRecordset("R_JOBORDER", "FK_ORDER", $"PK_R_JOBORDER = {joborderId}", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int>("FK_ORDER"))
            .First();
    }
}
