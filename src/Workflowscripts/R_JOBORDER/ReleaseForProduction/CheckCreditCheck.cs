﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;
using Ridder.Client.SDK.SDKParameters;

public class CheckCreditCheckJobOrder : WorkflowScriptInfo
{
    public Boolean Execute()
    {
        var id = (int)RecordId;

        //DB
        //23-11-2017
        //Controleer of de gekoppelde verkoopregel voldoet aan de kredietcheck

        var rsCRM = GetRecordset("R_CRMSETTINGS", "CREDITCHECKAPPLICABLE", "", "");
        rsCRM.MoveFirst();

        if (!(bool)rsCRM.Fields["CREDITCHECKAPPLICABLE"].Value)
        {
            return true;
        }

        var rsBon = GetRecordset("R_JOBORDER", "FK_ORDERBASE", $"PK_R_JOBORDER = {RecordId}", "");
        rsBon.MoveFirst();

        if (!(rsBon.Fields["FK_ORDERBASE"].Value as int?).HasValue)
        {
            return true;
        }

        var rsOrderBase = GetRecordset("R_ORDERBASE", "FK_SALESORDERDETAILASSEMBLY",
            $"PK_R_ORDERBASE = {rsBon.Fields["FK_ORDERBASE"].Value}", "");
        rsOrderBase.MoveFirst();

        if (!(rsOrderBase.Fields["FK_SALESORDERDETAILASSEMBLY"].Value as int?).HasValue)
        {
            return true;
        }

        var rsVerkoopregel = GetRecordset("R_SALESORDERDETAILASSEMBLY", "CREDITCHECK",
            $"PK_R_SALESORDERDETAILASSEMBLY = {rsOrderBase.Fields["FK_SALESORDERDETAILASSEMBLY"].Value}", "");
        rsVerkoopregel.MoveFirst();

        return (int)rsVerkoopregel.Fields["CREDITCHECK"].Value == 2;
    }
}
