﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class CheckMandayDateFieldsVatShifted : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //SP
        //1-10-2024
        //Bij verkoopfacturen zonder BTW moeten altijd de mandagstaten velden gevuld worden

        if(GetUserInfo().DatabaseName != "Spanberg")
        {
            return;//Alleen van toepassing bij Spanberg
        }

        if (CalculateVatAmount(id) != 0.0)
        {
            return;//Alleen doorgaan als er geen BTW op de factuur staat
        }

        var mandayFields = GetRecordset("R_SALESINVOICE", "MANDAYREGISTERFROM, MANDAYREGISTERUNTILL",
            $"PK_R_SALESINVOICE = {id}", "").DataTable.AsEnumerable();

        var mandayFrom = mandayFields.FirstOrDefault()?.Field<DateTime?>("MANDAYREGISTERFROM");
        var mandayUntill = mandayFields.FirstOrDefault()?.Field<DateTime?>("MANDAYREGISTERUNTILL");

        if(!(mandayFrom).HasValue || !(mandayUntill).HasValue)
        {
            throw new Exception("Velden 'Mandagstatenregister vanaf' en 'Mandagstatenregister t/m' zijn verplicht bij facturen met BTW verlegd.");
        }
    }

    private double CalculateVatAmount(int id)
    {
        return GetRecordset("R_SALESINVOICEALLDETAIL", "VATAMOUNT",
            $"FK_SALESINVOICE = {id}", "").DataTable.AsEnumerable().Sum(x => x.Field<double>("VATAMOUNT"));
    }
}
