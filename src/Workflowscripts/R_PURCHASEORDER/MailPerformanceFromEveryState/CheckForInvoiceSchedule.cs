﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class CheckForInvoiceSchedule : WorkflowScriptInfo
{
    public Boolean Execute()
    {
        var purchaseOrderId = (int)RecordId;
        var purchaseOrder = GetRecordset("R_PURCHASEORDER", "FK_INVOICESCHEDULE", $"PK_R_PURCHASEORDER = {purchaseOrderId}", "")
            .DataTable
            .AsEnumerable()
            .First();

        var invoiceScheduleId = purchaseOrder.Field<int?>("FK_INVOICESCHEDULE") ?? 0;
        return invoiceScheduleId > 0;

    }
}
