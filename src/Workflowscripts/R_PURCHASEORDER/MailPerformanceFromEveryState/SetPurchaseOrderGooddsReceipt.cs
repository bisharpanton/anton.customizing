﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Client.SDK.SDKParameters;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class SetPurchaseOrderGooddsReceipt : WorkflowScriptInfo
{
    public Boolean Execute()
    {
        var purchaseId = (int)RecordId;
        var purchaseOrder = GetRecordset("R_PURCHASEORDER", "RECEIPTSTATE, FK_WORKFLOWSTATE, PK_R_PURCHASEORDER",
                $"PK_R_PURCHASEORDER = {purchaseId}", "")
            .DataTable
            .AsEnumerable()
            .First();

        if ( purchaseOrder.Field<Guid>("FK_WORKFLOWSTATE") == PurchaseOrderWfState.StockPrinted 
            && purchaseOrder.Field<int>("RECEIPTSTATE") == (int)ReceiptState.WaitingForGoods)
        {
            var result = EventsAndActions.Purchase.Events.CreateGoodsReceiptFromPurchaseOrder(purchaseId, 0, 0,
                BlockOrSplitParameter.BlockJournalize, true, DateTime.Now, string.Empty);
            if (result != null && result.HasError)
            {
                Log.Error(GetRecordTag("R_PURCHASEORDER", purchaseId),
                    $"Inkooporder ontvangen melden mislukt, oorzaak: {result.GetResult()}",
                    "Mail Prestatieverklaring");
                return false;
            }
        }

        return true;

    }
    public static class PurchaseOrderWfState
    {
        public static Guid New = new Guid("0fc61ed2-0da1-4832-9644-635ef6e5bea6");
        public static Guid StockPrinted = new Guid("16a957a0-c449-43c4-9396-87c425d35556");
        public static Guid FiatRequested = new Guid("0ce76758-c02d-42d8-934a-50b8d22b40c4");
        public static Guid FiatApproved = new Guid("4d45523a-92b3-4ef1-813d-34fa15f56c15");

    }
    public enum ReceiptState : int
    {
        WaitingForGoods = 1,
        GoodsReceived = 2,
        GoodsReceiptProcessed = 3,
        Cancelled = 4
    }
}
