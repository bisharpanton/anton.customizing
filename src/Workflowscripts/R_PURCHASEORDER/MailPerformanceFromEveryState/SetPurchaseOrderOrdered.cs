﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class SetPurchaseOrderOrdered : WorkflowScriptInfo
{
    public Boolean Execute()
    {
        var purchaseId = (int)RecordId;
        var purchaseOrder = GetRecordset("R_PURCHASEORDER", "FK_PURCHASER, FK_WORKFLOWSTATE, PK_R_PURCHASEORDER", 
                $"PK_R_PURCHASEORDER = {purchaseId}", "")
            .DataTable
            .AsEnumerable()
            .First();

        var wfState = purchaseOrder.Field<Guid>("FK_WORKFLOWSTATE");
        if (wfState == PurchaseOrderWfState.StockPrinted)
        {
            return true;
        }

        var fiatRequired = DetermineFiatNeeded(purchaseOrder);

        // set purchaseorder ordered
        if (wfState == PurchaseOrderWfState.FiatApproved
            || (wfState == PurchaseOrderWfState.New && !fiatRequired))
        {
            var resultPurchaseOrdered = ExecuteWorkflowEvent("R_PURCHASEORDER", purchaseId,
                PurchaseOrderWorkflowEvents.SetPurchaseOrdered, null);
            if (resultPurchaseOrdered != null && resultPurchaseOrdered.HasError)
            {
                Log.Error(GetRecordTag("R_PURCHASEORDER", purchaseId),
                    $"Inkooporder ontvangen melden mislukt, oorzaak: {resultPurchaseOrdered.GetResult()}",
                    "Mail Prestatieverklaring");
                return false;
            }
        }
        else if (wfState == PurchaseOrderWfState.New && fiatRequired)
        {
            ExecuteWorkflowEvent("R_PURCHASEORDER", purchaseId, PurchaseOrderWorkflowEvents.SetReadyForFiat, null);
            Log.Info(GetRecordTag("R_PURCHASEORDER", purchaseId),
                "Inkooporder moet eerst gefiatteerd worden, inkooporder klaar gezet voor fiat.",
                "Mail Prestatieverklaring");
            return false;
        }

        return true;
    }

    private bool DetermineFiatNeeded(DataRow purchaseOrder)
    {
        var purchaseId = purchaseOrder.Field<int>("PK_R_PURCHASEORDER");
        var purchaserId = purchaseOrder.Field<int>("FK_PURCHASER");
        var buyLimit = GetRecordset("R_EMPLOYEE", "C_BESTELLIMIET", $"PK_R_EMPLOYEE = {purchaserId}", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<double>("C_BESTELLIMIET"))
            .First();

        var purchaseAmount = GetRecordset("R_PURCHASEORDERALLDETAIL", "NETPURCHASEPRICE",
                $"FK_PURCHASEORDER = {purchaseId}", "")
            .DataTable
            .AsEnumerable()
            .Sum(x => x.Field<double>("NETPURCHASEPRICE"));

        return buyLimit < purchaseAmount;
    }

    public static class PurchaseOrderWfState
    {
        public static Guid New = new Guid("0fc61ed2-0da1-4832-9644-635ef6e5bea6");
        public static Guid StockPrinted = new Guid("16a957a0-c449-43c4-9396-87c425d35556");
        public static Guid FiatRequested = new Guid("0ce76758-c02d-42d8-934a-50b8d22b40c4");
        public static Guid FiatApproved  = new Guid("4d45523a-92b3-4ef1-813d-34fa15f56c15");

    }

    public static class PurchaseOrderWorkflowEvents
    {
        public static Guid SetReadyForFiat = new Guid("e0fa383d-bd75-4049-a7e3-3ed50e157bce");
        public static Guid SetPurchaseOrdered  = new Guid("324de220-9561-40ce-8d50-9ff207dee26a");
    }
}
