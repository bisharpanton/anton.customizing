﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class MailPerformanceFromInstallment : WorkflowScriptInfo
{
    public static Guid MailPerformanceOfDeclaration = new Guid("5b703cd7-b52d-4b1f-bae0-206fc939ef05");
    public void Execute()
    {
        var purchaseId = (int)RecordId;
        var invoiceScheduleId = GetRecordset("R_PURCHASEINSTALLMENT", "PK_R_PURCHASEINSTALLMENT",
                $"FK_PURCHASEORDER = {purchaseId} AND DECLARATIONOFPERFORMANCE == 0", "PK_R_PURCHASEINSTALLMENT")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int?>("PK_R_PURCHASEINSTALLMENT") ?? 0)
            .FirstOrDefault();
        if (invoiceScheduleId == 0)
        {
            Log.Error(GetRecordTag("R_PURCHASEORDER", purchaseId),
                $"Inkooporder heeft geen inkoopfacturatieschemaregel waarvan de Prestatieverklaring gemaild kan worden." +
                $" Controleer de inkoopfacturatieschemaregels.",
                "Mail Prestatieverklaring");
            return;
        }

        ExecuteWorkflowEvent("R_PURCHASEINSTALLMENT", invoiceScheduleId,
            MailPerformanceOfDeclaration, null);
    }
}
