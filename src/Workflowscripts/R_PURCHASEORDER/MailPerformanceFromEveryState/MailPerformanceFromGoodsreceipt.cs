﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class MailPerformanceFromGoodsreceipt : WorkflowScriptInfo
{
    public static Guid MailPerformanceOfDeclaration = new Guid("2af8ccc1-33c9-40db-880f-1839fe0e2a0a");

    public void Execute()
    {
        // HVE 30-11-2021
        // VERVANGEN MET WORKFLOWCOMMANDSCRIPT.
        var purchaseId = (int)RecordId;

        var purchaseAllDetails = GetRecordset("R_PURCHASEORDERALLDETAIL", "", $"FK_PURCHASEORDER = {purchaseId}", "")
            .DataTable
            .AsEnumerable()
            .ToList();

        var goodsDetail = new GoodsDetail();

        var columns = new List<string>{ "R_PURCHASEORDERDETAILITEM", "R_PURCHASEORDERDETAILMISC", "R_PURCHASEORDERDETAILOUTSOURCED" };
        foreach (var column in columns)
        {
            var purchaseDetails = purchaseAllDetails
                .Where(y => y.Field<string>("SOURCE_TABLENAME") == column)
                .ToList();

            goodsDetail = DetermineGoodsDetail(purchaseDetails);
            if (goodsDetail != null 
                && goodsDetail.Id > 0)
            {
                break;
            }
        }

        if (goodsDetail == null
            || goodsDetail.Id == 0)
        {
            Log.Error(GetRecordTag("R_PURCHASEORDER", purchaseId),
                $"Geen ontvangsten beschikbaar voor inkooporder {GetRecordTag("R_PURCHASEORDER", purchaseId)}",
                "Mail Prestatieverklaring");
            return;
        }

        ExecuteWorkflowEvent("R_GOODSRECEIPT", goodsDetail.Id, MailPerformanceOfDeclaration, null);
    }

    private GoodsDetail DetermineGoodsDetail(List<DataRow> purchaseDetails)
    {
        if (!purchaseDetails.Any())
        {
            return null;
        }

        var purchaseAllDetail = purchaseDetails.First();
        var tableName = purchaseAllDetail.Field<string>("SOURCE_TABLENAME");
        var column = tableName.Replace("R_", "FK_");
        var purchaseDetailIds = purchaseDetails.Select(x => x.Field<int>(column))
            .ToList();

        var tableGoodsreceipt = DetermineTableGoodsReceipt(tableName);
        
        var goodsreceiptDetails = GetRecordset(tableGoodsreceipt, "FK_GOODSRECEIPT", 
                $"{column} IN ({string.Join(",", purchaseDetailIds)})", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int?>("FK_GOODSRECEIPT") ?? 0)
            .ToList();

        if (goodsreceiptDetails.Any(y => y > 0))
        {
            return new GoodsDetail()
            {
                Id = goodsreceiptDetails.First(y => y > 0),
                TableName = tableName
            };
        }

        return null;
    }

    private string DetermineTableGoodsReceipt(string tableName)
    {
        switch (tableName)
        {
            case "R_PURCHASEORDERDETAILITEM":
                return "R_GOODSRECEIPTDETAILITEM";
            case "R_PURCHASEORDERDETAILMISC":
                return "R_GOODSRECEIPTDETAILMISC";
            case "R_PURCHASEORDERDETAILOUTSOURCED":
                return "R_GOODSRECEIPTDETAILOUTSOURCED";
        }

        return null;
    }

    public class GoodsDetail
    {
        public int Id { get; set; }
        public string TableName { get; set; }
    }
}
