﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Formatting;

public class GeneratePurchaseInstallment : WorkflowScriptInfo
{
    public Boolean Execute()
    {
        var purchaseOrderId = (int)RecordId;

        var invoiceSchedule = GetRecordset("R_PURCHASEORDER", "PK_R_PURCHASEORDER",
            $"PK_R_PURCHASEORDER = {purchaseOrderId} AND FK_INVOICESCHEDULE IS NOT NULL", "");

        if (invoiceSchedule.RecordCount == 0)
        {
            return false;
        }
        else
        {
            return true;
        }

    }
}
