﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class CreateRevisionZero : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //22-6-2023
        //Maak bij de eerste keer mailen of printen de eerste revisie aan

        var purchaseOrder = GetRecordset("R_PURCHASEORDER", "FK_SUPPLIER, PURCHASEORDERNUMBER, SEQUENCENUMBER, DELIVERYDATE", $"PK_R_PURCHASEORDER = {id}", "")
            .DataTable.AsEnumerable().First();

        if(purchaseOrder.Field<int>("SEQUENCENUMBER") != 0)
        {
            return;
        }

        if (!CreatePurchaseOrderRevisionLog(id, purchaseOrder, 0, null))
        {
            return;
        }
    }

    private bool CreatePurchaseOrderRevisionLog(int id, DataRow purchaseOrder, int newRevision, int? reasonRevision)
    {
        var rsPurchaseOrderRevision = GetRecordset("U_PURCHASEORDERREVISION", "", "PK_U_PURCHASEORDERREVISION IS NULL", "");
        rsPurchaseOrderRevision.AddNew();
        rsPurchaseOrderRevision.SetFieldValue("FK_PURCHASEORDER", id);
        rsPurchaseOrderRevision.SetFieldValue("SEQUENCENUMBER", newRevision);

        rsPurchaseOrderRevision.SetFieldValue("TOTALNETPURCHASEAMOUNT", GetTotalNetPurchaseAmount(id));

        if (reasonRevision.HasValue)
        {
            rsPurchaseOrderRevision.SetFieldValue("FK_PURCHASEREASONREVISION", reasonRevision);
        }

        if (purchaseOrder.Field<DateTime?>("DELIVERYDATE").HasValue)
        {
            rsPurchaseOrderRevision.SetFieldValue("DELIVERYDATE", purchaseOrder.Field<DateTime>("DELIVERYDATE"));
        }

        var joborderStartDateInfo = GetJoborderStartDate(id);
        if (joborderStartDateInfo != null && joborderStartDateInfo.StartDate.HasValue)
        {
            rsPurchaseOrderRevision.SetFieldValue("JOBORDERSTARTDATE", joborderStartDateInfo.StartDate);
        }

        if (joborderStartDateInfo != null && joborderStartDateInfo.StartTime.HasValue)
        {
            rsPurchaseOrderRevision.SetFieldValue("JOBORDERSTARTTIME", joborderStartDateInfo.StartTime);
        }

        var updateResult = rsPurchaseOrderRevision.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            Log.Error("Error", $"Aanmaken inkooporder revisie is mislukt, oorzaak:{updateResult.First(x => x.HasError).GetResult()}", "");
            return false;
        }

        return true;
    }

    private double GetTotalNetPurchaseAmount(int id)
    {
        return GetRecordset("R_PURCHASEORDERALLDETAIL", "NETPURCHASEPRICE", $"FK_PURCHASEORDER = {id}", "")
            .DataTable.AsEnumerable().Sum(x => x.Field<double>("NETPURCHASEPRICE"));
    }

    private JoborderStartDateInfo GetJoborderStartDate(int id)
    {
        var joborderId = GetRecordset("R_ALLRESERVATION", "FK_PRODUCTIONJOBORDER", $"FK_PURCHASEORDER = {id} AND FK_PRODUCTIONJOBORDER IS NOT NULL", "")
            .DataTable.AsEnumerable().FirstOrDefault()?.Field<int?>("FK_PRODUCTIONJOBORDER") ?? 0;

        if (joborderId == 0)
        {
            return null;
        }

        var joborder = GetRecordset("R_JOBORDER", "DELIVERYDATE, STARTTIJD", $"PK_R_JOBORDER = {joborderId}", "")
            .DataTable.AsEnumerable().First();

        return new JoborderStartDateInfo()
        {
            StartDate = joborder.Field<DateTime?>("DELIVERYDATE"),
            StartTime = joborder.Field<DateTime?>("STARTTIJD"),
        };
    }

    class JoborderStartDateInfo
    {
        public DateTime? StartDate { get; set; }
        public DateTime? StartTime { get; set; }
    }
}
