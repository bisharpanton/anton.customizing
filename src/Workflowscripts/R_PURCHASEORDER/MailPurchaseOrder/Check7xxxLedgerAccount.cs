﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.CRM;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class Check7xxxLedgerAccount : WorkflowScriptInfo
{
    public void Execute()
    {
        /* DB - Deze code staat nog niet live. Sven gaat eerst overleggen met Joris en Jacco of de inkopers wel de keuze kunnen maken voor het juiste grootboek.
         * 
        var id = (int)RecordId;

        var generalLedgerAccounts = GetRecordset("R_GENERALLEDGERACCOUNT", "GENERALLEDGERACCOUNTNUMBER",
            $"", "").DataTable.AsEnumerable().Select(x => new GeneralLedgerAccount()
            {
                Id = x.Field<int>("PK_R_GENERALLEDGERACCOUNT"),
                Number = x.Field<string>("GENERALLEDGERACCOUNTNUMBER")
            }).ToList();


        var sources = new string[3] { "ITEM", "MISC", "OUTSOURCED" };

        foreach (var source in sources)
        {
            var purchaseOrderDetails = GetRecordset($"R_PURCHASEORDERDETAIL{source}", "FK_GENERALLEDGERACCOUNT, DIRECTTOORDER",
                $"FK_PURCHASEORDER = {id}", "").DataTable.AsEnumerable().ToList();

            if (!purchaseOrderDetails.Any())
            {
                continue;
            }

            foreach (var purchaseOrderDetail in purchaseOrderDetails)
            {
                if (purchaseOrderDetail.Field<bool>("DIRECTTOORDER") && !purchaseOrderDetail.Field<int?>("FK_GENERALLEDGERACCOUNT").HasValue)
                {
                    var recordTag = GetRecordTag($"R_PURCHASEORDERDETAIL{source}", purchaseOrderDetail.Field<int>($"PK_R_PURCHASEORDERDETAIL{source}"));
                    Log.Error("Error", $"Inkoopregel {recordTag} is rechtstreeks op order en moet daarom op een 7xxx grootboekrekening worden gekoppeld.", "");
                    return;
                }

                if (purchaseOrderDetail.Field<int?>("FK_GENERALLEDGERACCOUNT").HasValue)
                {
                    var generalLedgerAccount = generalLedgerAccounts.First(x => x.Id == purchaseOrderDetail.Field<int>("FK_GENERALLEDGERACCOUNT"));

                    if (purchaseOrderDetail.Field<bool>("DIRECTTOORDER") && !generalLedgerAccount.Number.StartsWith("7"))
                    {
                        var recordTag = GetRecordTag($"R_PURCHASEORDERDETAIL{source}", purchaseOrderDetail.Field<int>($"PK_R_PURCHASEORDERDETAIL{source}"));
                        Log.Error("Error", $"Inkoopregel {recordTag} is rechtstreeks op order en moet daarom op een 7xxx grootboekrekening worden gekoppeld.", "");
                        return;
                    }

                    if (!purchaseOrderDetail.Field<bool>("DIRECTTOORDER") && generalLedgerAccount.Number.StartsWith("7"))
                    {
                        var recordTag = GetRecordTag($"R_PURCHASEORDERDETAIL{source}", purchaseOrderDetail.Field<int>($"PK_R_PURCHASEORDERDETAIL{source}"));
                        Log.Error("Error", $"Foutieve grootboekrekening bij inkoopregel {recordTag}. Een 7xxx grootboekrekening mag alleen worden gebruikt bij rechtstreeks op order.", "");
                        return;
                    }
                }
            }
        }*/
    }



    public class GeneralLedgerAccount
    {
        public int Id { get; set; }
        public string Number { get; set; }
    }
}
