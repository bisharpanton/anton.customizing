﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Client;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class CopyPUrchaseOrderWithParameters : WorkflowScriptInfo
{
    public void Execute()
    {
        // HVE
        // 4-5-2021
        // COpy Purchaseorder with Parameter
        var purchaseOrderId = (int)RecordId;
        var recalculate = (bool)Parameters["Recalculate"].Value;
        var copyDestination = (bool)Parameters["CopyOrderAndJoborder"].Value;

        var result = EventsAndActions.Purchase.Actions.CopyPurchaseOrder(purchaseOrderId, recalculate);

        if (result != null && result.HasError)
        {
            Log.Error(GetRecordTag("R_PURCHASEORDER", purchaseOrderId),
                $"Kopieren van de inkooporder mislukt, oorzaak: {result.GetResult()}",
                "Kopieer inkooporder");
            return;
        }

        var newPurchaseOrderId = (int)result.PrimaryKey;

        var relationId = GetRecordset("R_PURCHASEORDER", "FK_SUPPLIER",
            $"PK_R_PURCHASEORDER = {newPurchaseOrderId}", "").DataTable.AsEnumerable().First().Field<int>("FK_SUPPLIER");

        var relation = GetRecordset("R_RELATION", "FK_PURCHASEPAYMENTTERM, FK_DEFAULTPURCHASELEDGERACCOUNT, FK_VATCOMPANYGROUP",
            $"PK_R_RELATION = {relationId}", "").DataTable.AsEnumerable();

        var paymentTermId = relation.FirstOrDefault().Field<int?>("FK_PURCHASEPAYMENTTERM") ?? 0;
        var defaultPurchaseLedgerId = relation.FirstOrDefault().Field<int?>("FK_DEFAULTPURCHASELEDGERACCOUNT") ?? 0;
        var vatCompanyGroupId = relation.FirstOrDefault().Field<int?>("FK_VATCOMPANYGROUP") ?? 0;

        var currentDescription = GetRecordset("R_PURCHASEORDER", "REFERENCESUPPLIER",
            $"PK_R_PURCHASEORDER = {purchaseOrderId}", "").DataTable.AsEnumerable().First().Field<string>("REFERENCESUPPLIER");


        var rsNewPurchaseOrder = GetRecordset("R_PURCHASEORDER", "", $"PK_R_PURCHASEORDER = {newPurchaseOrderId}", "");
        rsNewPurchaseOrder.MoveFirst();
        rsNewPurchaseOrder.SetFieldValue("FK_ORIGNALPURCHASEORDER", purchaseOrderId);
        rsNewPurchaseOrder.SetFieldValue("FK_PURCHASER", CurrentUser.EmployeeID);
        rsNewPurchaseOrder.SetFieldValue("REFERENCESUPPLIER", currentDescription);

        if (paymentTermId > 0)
        {
            rsNewPurchaseOrder.SetFieldValue("FK_PAYMENTTERM", paymentTermId);
        }
        if (vatCompanyGroupId > 0)//SP 5-12-24 - BTW code ophalen uit de relatie
        {
            rsNewPurchaseOrder.SetFieldValue("FK_VATCOMPANYGROUP", vatCompanyGroupId);
        }
        rsNewPurchaseOrder.Update();


        if(copyDestination)
        {
            CopyOrderAndJoborderToNewPurchaseOrderDetails(purchaseOrderId, newPurchaseOrderId, defaultPurchaseLedgerId);
        }


    }

    private void CopyOrderAndJoborderToNewPurchaseOrderDetails(int purchaseOrderId, int newPurchaseOrderId, int defaultPurchaseLedgerId)
    {
        string[] tables = new string[3] { "R_PURCHASEORDERDETAILITEM", "R_PURCHASEORDERDETAILMISC", "R_PURCHASEORDERDETAILOUTSOURCED" };

        foreach (string table in tables)
        {
            var rsNewPurchaseOrderDetails = GetRecordset(table, "", $"FK_PURCHASEORDER = {newPurchaseOrderId}", "");

            if(rsNewPurchaseOrderDetails.RecordCount == 0)
            {
                continue;
            }

            rsNewPurchaseOrderDetails.UseDataChanges = true;

            var oldPurchaseOrderDetails = GetRecordset(table, "", $"FK_PURCHASEORDER = {purchaseOrderId}", "")
                .DataTable.AsEnumerable().ToList();

            var orderIds = oldPurchaseOrderDetails.Select(x => x.Field<int?>("FK_ORDER") ?? 0).ToList();

            var orders = GetRecordset("R_ORDER", "FK_WORKFLOWSTATE", $"PK_R_ORDER IN ({string.Join(",", orderIds)})", "")
                .DataTable.AsEnumerable() .ToList();

            var recordsChanged = false;

            Guid wfStatusAfgesloten = new Guid("eb0e5af0-52b1-4d0e-b203-4a4fb2f089bf");
            Guid wfStatusFinGereed = new Guid("b2cb1324-a085-4823-8cc2-e0c372edfd87");

            rsNewPurchaseOrderDetails.MoveFirst();
            while(!rsNewPurchaseOrderDetails.EOF)
            {
                if (defaultPurchaseLedgerId > 0)//SP 5-12-24 - Voor elke regel grootboek ophalen uit de relatie
                {
                    rsNewPurchaseOrderDetails.Fields["FK_GENERALLEDGERACCOUNT"].Value = defaultPurchaseLedgerId;
                }

                var oldPurchaseOrderDetail = oldPurchaseOrderDetails
                    .FirstOrDefault(x => x.Field<string>("DESCRIPTION").Equals(rsNewPurchaseOrderDetails.GetField("DESCRIPTION").Value.ToString())
                                            && Math.Abs(x.Field<double>("QUANTITY") - (double)rsNewPurchaseOrderDetails.GetField("QUANTITY").Value) < 0.001);

                if(oldPurchaseOrderDetail == null)
                {
                    //Geen oorspronkelijke inkoopregel gevonden
                    rsNewPurchaseOrderDetails.MoveNext();
                    continue;
                }

                if(!oldPurchaseOrderDetail.Field<int?>("FK_ORDER").HasValue)
                {
                    //Bestemming niet van toepassing
                    rsNewPurchaseOrderDetails.MoveNext();
                    continue;
                }

                var order = orders.First(x => x.Field<int>("PK_R_ORDER") == oldPurchaseOrderDetail.Field<int>("FK_ORDER"));

                if(order.Field<Guid>("FK_WORKFLOWSTATE") ==  wfStatusAfgesloten || order.Field<Guid>("FK_WORKFLOWSTATE") == wfStatusFinGereed)
                {
                    //Controle of order al afgesloten of fin gereed is
                    rsNewPurchaseOrderDetails.MoveNext();
                    continue;
                }


                rsNewPurchaseOrderDetails.Fields["DIRECTTOORDER"].Value = true;
                rsNewPurchaseOrderDetails.Fields["FK_ORDER"].Value = oldPurchaseOrderDetail.Field<int>("FK_ORDER");

                if(oldPurchaseOrderDetail.Field<int?>("FK_JOBORDER").HasValue)
                {
                    rsNewPurchaseOrderDetails.Fields["FK_JOBORDER"].Value = oldPurchaseOrderDetail.Field<int>("FK_JOBORDER");
                }

                recordsChanged = true;


                rsNewPurchaseOrderDetails.MoveNext();
            }

            if(recordsChanged)
            {
                rsNewPurchaseOrderDetails.MoveFirst();
                var updateResult = rsNewPurchaseOrderDetails.Update2();

                if(updateResult.Any(x => x.HasError))
                {
                    Log.Error("Error", $"Wijzigen inkoopregels is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "");
                }
            }

        }

    }
}
