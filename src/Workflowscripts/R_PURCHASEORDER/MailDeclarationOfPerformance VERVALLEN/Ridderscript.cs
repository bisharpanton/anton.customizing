﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using ADODB;
using Ridder.Client;
using Ridder.Client.SDK.SDKParameters;
using Ridder.Common;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;
using Ridder.Communication.PurchaseAdministration;
using Ridder.Recordset.Extensions;
using Workflowscripts.R_PURCHASEORDER.MailDeclarationOfPerformance.Models;
using ReceiptState = Workflowscripts.R_PURCHASEORDER.MailDeclarationOfPerformance.Models.ReceiptState;

public class MailDeclarationOfPerformance : WorkflowScriptInfo
{
    public void Execute()
    {
        // HVE
        // 4-5-2021
        // Mail performance of declaration
        var purchaseId = (int)RecordId;
        var wfStates = new WorkflowState();
        var wfPurchaseOrderEvents = new PurchaseOrderWorkflowEvents();
        var wfGoodsreceiptEvents = new GoodsReceiptWorkflowEvents();

        var purchaseOrder = GetRecordset("R_PURCHASEORDER", "", $"PK_R_PURCHASEORDER = {purchaseId}", "")
            .As<PurchaseOrder>()
            .First();

        if (!purchaseOrder.FK_INVOICESCHEDULE.HasValue
        && purchaseOrder.RECEIPTSTATE == ReceiptState.Goods_receipt_processed)
        {
            var goodsReceiptIds = GetRecordset("R_GOODSRECEIPT", "PK_R_GOODSRECEIPT",
                    $"FK_ORIGINALPURCHASEORDER = {purchaseId} AND DECLARATIONOFPERFORMANCE == 0", "")
                .DataTable
                .AsEnumerable()
                .Select(x => x.Field<int?>("PK_R_GOODSRECEIPT") ?? 0)
                .ToList();

            if (goodsReceiptIds.Count == 1)
            {
                Thread.Sleep(new TimeSpan(0, 0, 0, 2));

                ExecuteWorkflowEvent("R_GOODSRECEIPT", (int)goodsReceiptIds.First(),
                    wfGoodsreceiptEvents.MailPerformanceOfDeclaration, null);
            }
            else
            {
                Log.Info(GetRecordTag("R_PURCHASEORDER", purchaseId),
                    "Er zijn meerdere goederenontvangsten beschikbaar. De workflow dient handmatig per goederenontvangst uitgevoerd te worden.",
                    "Mail Prestatieverklaring");
            }
            return;
        } 

        if (purchaseOrder.FK_WORKFLOWSTATE == wfStates.FiatRequested)
        {
            Log.Info(GetRecordTag("R_PURCHASEORDER", purchaseId),
                "Inkooporder moet eerst gefiatteerd worden",
                "Mail Prestatieverklaring");
            return;
        }


        if (purchaseOrder.FK_WORKFLOWSTATE == wfStates.New
            && DetermineFiatNeeded(purchaseOrder))
        {
            ExecuteWorkflowEvent("R_PURCHASEORDER", purchaseId, wfPurchaseOrderEvents.SetReadyForFiat, null);
            Log.Info(GetRecordTag("R_PURCHASEORDER", purchaseId),
                "Inkooporder moet eerst gefiatteerd worden, inkooporder klaar gezet voor fiat.",
                "Mail Prestatieverklaring");
            return;
        }

        if (purchaseOrder.FK_WORKFLOWSTATE == wfStates.New
            || purchaseOrder.FK_WORKFLOWSTATE == wfStates.FiatApproved)
        {
            var resultPurchaseOrdered = ExecuteWorkflowEvent("R_PURCHASEORDER", purchaseId,
                wfPurchaseOrderEvents.SetPurchaseOrdered, null);
            if (resultPurchaseOrdered != null && resultPurchaseOrdered.HasError)
            {
                Log.Error(GetRecordTag("R_PURCHASEORDER", purchaseId),
                    $"Inkooporder ontvangen melden mislukt, oorzaak: {resultPurchaseOrdered.GetResult()}",
                    "Mail Prestatieverklaring");
                return;
            }
        }

        if (!purchaseOrder.FK_INVOICESCHEDULE.HasValue)
        {
            var result = EventsAndActions.Purchase.Events.CreateGoodsReceiptFromPurchaseOrder(purchaseId, 0, 0,
                BlockOrSplitParameter.BlockJournalize, true, DateTime.Now, string.Empty);
            if (result != null && result.HasError)
            {
                Log.Error(GetRecordTag("R_PURCHASEORDER", purchaseId),
                    $"Inkooporder ontvangen melden mislukt, oorzaak: {result.GetResult()}",
                    "Mail Prestatieverklaring");
                return;
            }


            Thread.Sleep(new TimeSpan(0,0,0,2));
            var goodsReceiptId = (int)result.PrimaryKey;
            ExecuteWorkflowEvent("R_GOODSRECEIPT", goodsReceiptId, wfGoodsreceiptEvents.MailPerformanceOfDeclaration, null);
        }
        else
        {
            var invoiceScheduleId = GetRecordset("R_PURCHASEINSTALLMENT", "PK_R_PURCHASEINSTALLMENT",
                    $"FK_PURCHASEORDER = {purchaseId} AND DECLARATIONOFPERFORMANCE == 0", "PK_R_PURCHASEINSTALLMENT")
                .DataTable
                .AsEnumerable()
                .Select(x => x.Field<int?>("PK_R_PURCHASEINSTALLMENT") ?? 0)
                .FirstOrDefault();
            if (invoiceScheduleId == 0)
            {
                Log.Error(GetRecordTag("R_PURCHASEORDER", purchaseId),
                    $"Inkooporder heeft geen inkoopfacturatieschemaregel waarvan de Prestatieverklaring gemaild kan worden." +
                    $" Controleer de inkoopfacturatieschemaregels.",
                    "Mail Prestatieverklaring");
                return;
            }


            Thread.Sleep(new TimeSpan(0, 0, 0, 2));
            var purchaseInstallmentWfs = new PurchaseInstallmentWorkflowEvents();
            ExecuteWorkflowEvent("R_PURCHASEINSTALLMENT", invoiceScheduleId,
                purchaseInstallmentWfs.MailPerformanceOfDeclaration, null);
        }
    }

    private bool DetermineFiatNeeded(PurchaseOrder purchaseOrder)
    {
        var purchaseId = purchaseOrder.FK_PURCHASER;
        var buyLimit = GetRecordset("R_EMPLOYEE", "C_BESTELLIMIET", $"PK_R_EMPLOYEE = {purchaseId}", "")
            .As<Employee>()
            .Select(x => x.C_BESTELLIMIET)
            .First();

        var purchaseAmount = GetRecordset("R_PURCHASEORDERALLDETAIL", "NETPURCHASEPRICE",
                $"FK_PURCHASEORDER = {purchaseOrder.PK_R_PURCHASEORDER}", "")
            .DataTable
            .AsEnumerable()
            .Sum(x => x.Field<double>("NETPURCHASEPRICE"));

        return buyLimit < purchaseAmount;

    }


    public class PurchaseInstallmentWorkflowEvents
    {
        public Guid MailPerformanceOfDeclaration { get; set; } = new Guid("5b703cd7-b52d-4b1f-bae0-206fc939ef05");
    }
    public class PurchaseOrderWorkflowEvents
    {
        public Guid SetReadyForFiat { get; set; } = new Guid("e0fa383d-bd75-4049-a7e3-3ed50e157bce");
        public Guid SetPurchaseOrdered { get; set; } = new Guid("324de220-9561-40ce-8d50-9ff207dee26a");
    }
    public class GoodsReceiptWorkflowEvents
    {
        public Guid MailPerformanceOfDeclaration { get; set; } = new Guid("2af8ccc1-33c9-40db-880f-1839fe0e2a0a");

    }
    public class WorkflowState
    {
        public Guid New { get; set; } = new Guid("0fc61ed2-0da1-4832-9644-635ef6e5bea6");
        public Guid StockPrinted { get; set; } = new Guid("16a957a0-c449-43c4-9396-87c425d35556");
        public Guid FiatRequested { get; set; } = new Guid("0ce76758-c02d-42d8-934a-50b8d22b40c4");
        public Guid FiatApproved { get; set; } = new Guid("4d45523a-92b3-4ef1-813d-34fa15f56c15");

    }

}
