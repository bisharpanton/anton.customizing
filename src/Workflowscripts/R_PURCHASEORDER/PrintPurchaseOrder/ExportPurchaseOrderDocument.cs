﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class ExportPurchaseOrderDocument : WorkflowScriptInfo
{
    public String Execute()
    {
        var id = (int)RecordId;

        var tableName = "R_PURCHASEORDER";

        //DB
        //10-6-2019
        //Haal de bestandlocatie op uit het mailsjabloon en archiveer daar het inkooporder document
        /*
        var pkTableInfo = GetRecordset("M_TABLEINFO", "PK_M_TABLEINFO",
                $"TABLENAME = '{tableName}'", "").DataTable.AsEnumerable().First()
            .Field<Guid>("PK_M_TABLEINFO");

        var rsMailReportSetting = GetRecordset("R_MAILREPORTSETTING",
            "PK_R_MAILREPORTSETTING, FILELOCATION, FILENAME",
            $"FK_TABLEINFO = '{pkTableInfo}' AND [DEFAULT] = 1", "");

        if (rsMailReportSetting.RecordCount == 0)
        {
            throw new Exception("Mailsjabloon niet gevonden");
        }

        rsMailReportSetting.MoveFirst();

        //Get file path
        var fileLocation = rsMailReportSetting.GetField("FILELOCATION").Value as byte[];

        if (fileLocation == null)
        {
            throw new Exception("Bestandslocatie niet ingevuld in mailsjabloon.");
        }

        var fileName = rsMailReportSetting.GetField("FILENAME").Value as byte[];

        if (fileName == null)
        {
            throw new Exception("Bestandsnaam niet ingevuld in mailsjabloon.");
        }

        var fileLocationCalc = GetCalculatedColumnOutput(fileLocation, "R_PURCHASEORDER", id) as string;
        var fileNameCalc = GetCalculatedColumnOutput(fileName, "R_PURCHASEORDER", id) as string;
        var filePathCalc = System.IO.Path.Combine(new string[] { fileLocationCalc, fileNameCalc });

        return filePathCalc;*/

        return string.Empty;
    }
}
