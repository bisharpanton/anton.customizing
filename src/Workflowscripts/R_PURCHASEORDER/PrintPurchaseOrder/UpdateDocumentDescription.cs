﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class UpdateDocumentDescription : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //10-6-2019
        //Bij het archiveren wordt de omschrijving van het document niet opgeslagen.
        /*
        //get the most recently added document id for this purchase order
        var rsPurchaseOrderDocument = GetRecordset("R_PURCHASEORDERDOCUMENT", "FK_DOCUMENT",
            $"FK_PURCHASEORDER = {id}", "DATECREATED DESC");
        rsPurchaseOrderDocument.MoveFirst();

        if (rsPurchaseOrderDocument.RecordCount == 0)
        {
            return;
        }

        //get the the path of the file
        var rsDocument = GetRecordset("R_DOCUMENT", "DESCRIPTION, DOCUMENTLOCATION",
            $"PK_R_DOCUMENT = {rsPurchaseOrderDocument.Fields["FK_DOCUMENT"].Value}", "");
        rsDocument.MoveFirst();

        var documentPath = rsDocument.GetField("DOCUMENTLOCATION").Value as string;
        var fileName = System.IO.Path.GetFileNameWithoutExtension(documentPath);

        rsDocument.Fields["DESCRIPTION"].Value = fileName;
        rsDocument.Update();*/
    }
}
