﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class ChangePurchaser : WorkflowScriptInfo
{
    public void Execute()
    {
        // HVE
        // 8-7-2021
        // Wf to change the purchaser. So that the planner can create on purchaseorder and the projectmanager can make the purchase.
        
        var purchaseOrderId = (int)RecordId;
        var purchaserId = Parameters["newpurchaser"].Value as int? ?? 0;

        var rsPurchaseOrder = GetRecordset("R_PURCHASEORDER", "", $"PK_R_PURCHASEORDER = {purchaseOrderId}", "");
        rsPurchaseOrder.MoveFirst();

        if (purchaserId != (int)rsPurchaseOrder.Fields["FK_PURCHASER"].Value)
        {
            rsPurchaseOrder.SetFieldValue("FK_PURCHASER", purchaserId);
            rsPurchaseOrder.Update();
        }
    }
}
