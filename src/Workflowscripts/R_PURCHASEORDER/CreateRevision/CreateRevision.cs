﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class CreateRevision : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //15-6-2023
        //Maak revisie inkooporder

        var reasonRevision = Parameters["ReasonRevision"].Value as int?;

        var rsPuchaseOrder = GetRecordset("R_PURCHASEORDER", "FK_SUPPLIER, PURCHASEORDERNUMBER, SEQUENCENUMBER, DELIVERYDATE", $"PK_R_PURCHASEORDER = {id}", "");
        var purchaseOrder = rsPuchaseOrder.DataTable.AsEnumerable().First();

        var currentRevision = purchaseOrder.Field<int>("SEQUENCENUMBER");
        var newRevision = currentRevision + 1;

        if(!CreatePurchaseOrderRevisionLog(id, purchaseOrder, newRevision, reasonRevision))
        {
            return;
        }

        UpdateRevisionNumber(rsPuchaseOrder, newRevision);

        ExecuteWorkflowSetToNew(id);

    }

    private void ExecuteWorkflowSetToNew(int id)
    {
        var wfSetToNew = new Guid("1bf20f41-3a58-494e-9e1e-291decba3dc0");

        var result = ExecuteWorkflowEvent("R_PURCHASEORDER", id, wfSetToNew, null);

        if(result.HasError)
        {
            throw new Exception($"Terug naar Nieuw zetten is mislukt, oorzaak: {result.GetResult()}");
        }
    }

    private bool CreatePurchaseOrderRevisionLog(int id, DataRow purchaseOrder, int newRevision, int? reasonRevision)
    {
        var rsPurchaseOrderRevision = GetRecordset("U_PURCHASEORDERREVISION", "", "PK_U_PURCHASEORDERREVISION IS NULL", "");
        rsPurchaseOrderRevision.AddNew();
        rsPurchaseOrderRevision.SetFieldValue("FK_PURCHASEORDER", id);
        rsPurchaseOrderRevision.SetFieldValue("SEQUENCENUMBER", newRevision);

        rsPurchaseOrderRevision.SetFieldValue("TOTALNETPURCHASEAMOUNT", GetTotalNetPurchaseAmount(id));

        if (reasonRevision.HasValue)
        {
            rsPurchaseOrderRevision.SetFieldValue("FK_PURCHASEREASONREVISION", reasonRevision);
        }

        if (purchaseOrder.Field<DateTime?>("DELIVERYDATE").HasValue)
        {
            rsPurchaseOrderRevision.SetFieldValue("DELIVERYDATE", purchaseOrder.Field<DateTime>("DELIVERYDATE"));
        }

        var joborderStartDateInfo = GetJoborderStartDate(id);
        if (joborderStartDateInfo != null && joborderStartDateInfo.StartDate.HasValue)
        {
            rsPurchaseOrderRevision.SetFieldValue("JOBORDERSTARTDATE", joborderStartDateInfo.StartDate);
        }

        if (joborderStartDateInfo != null && joborderStartDateInfo.StartTime.HasValue)
        {
            rsPurchaseOrderRevision.SetFieldValue("JOBORDERSTARTTIME", joborderStartDateInfo.StartTime);
        }

        var updateResult = rsPurchaseOrderRevision.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            Log.Error("Error", $"Aanmaken inkooporder revisie is mislukt, oorzaak:{updateResult.First(x => x.HasError).GetResult()}", "");
            return false;
        }

        return true;
    }

    private double GetTotalNetPurchaseAmount(int id)
    {
        return GetRecordset("R_PURCHASEORDERALLDETAIL", "NETPURCHASEPRICE", $"FK_PURCHASEORDER = {id}", "")
            .DataTable.AsEnumerable().Sum(x => x.Field<double>("NETPURCHASEPRICE"));
    }

    private JoborderStartDateInfo GetJoborderStartDate(int id)
    {
        var joborderId = GetRecordset("R_ALLRESERVATION", "FK_PRODUCTIONJOBORDER", $"FK_PURCHASEORDER = {id} AND FK_PRODUCTIONJOBORDER IS NOT NULL", "")
            .DataTable.AsEnumerable().FirstOrDefault()?.Field<int?>("FK_PRODUCTIONJOBORDER") ?? 0;

        if(joborderId == 0)
        {
            return null;
        }

        var joborder = GetRecordset("R_JOBORDER", "DELIVERYDATE, STARTTIJD", $"PK_R_JOBORDER = {joborderId}", "")
            .DataTable.AsEnumerable().First();

        return new JoborderStartDateInfo()
        {
            StartDate = joborder.Field<DateTime?>("DELIVERYDATE"),
            StartTime = joborder.Field<DateTime?>("STARTTIJD"),
        };
    }

    private void UpdateRevisionNumber(ScriptRecordset rsPuchaseOrder, int newRevision)
    {
        rsPuchaseOrder.MoveFirst();
        rsPuchaseOrder.Fields["SEQUENCENUMBER"].Value = newRevision;
        rsPuchaseOrder.Update();
    }

    class JoborderStartDateInfo
    {
        public DateTime? StartDate { get; set; }
        public DateTime? StartTime { get; set; }
    }
}
