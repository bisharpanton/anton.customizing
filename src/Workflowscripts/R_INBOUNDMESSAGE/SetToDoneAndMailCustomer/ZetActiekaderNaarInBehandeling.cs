﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Formatting;

public class ZetActiekaderNaarInBehandeling : WorkflowScriptInfo
{
    public void Execute()
    {
        //DB
        //26-8-2024
        //Zet actiekader naar in behandeling bij eerste uitgaande bericht

        var inboundMessage = GetRecordset("R_INBOUNDMESSAGE", "FK_INBOUNDMESSAGETYPE, FK_ACTIONSCOPE", string.Format("PK_R_INBOUNDMESSAGE = {0}", this.RecordId), "")
                    .DataTable.AsEnumerable().First();


        if (inboundMessage.Field<int>("FK_INBOUNDMESSAGETYPE") != 4)//4 = uitgaand portal bericht
        {
            return;
        }

        var actionScopeId = inboundMessage.Field<int?>("FK_ACTIONSCOPE") ?? 0;
        if (actionScopeId == 0)
        {
            return;
        }

        var wfStateNieuw = new Guid("32e74003-9d63-4ab6-a9fe-e1404c6d4355");

        var wfStateActionScope = GetRecordset("R_ACTIONSCOPE", "FK_WORKFLOWSTATE", string.Format("PK_R_ACTIONSCOPE = {0}", actionScopeId), "")
                    .DataTable.AsEnumerable().First().Field<Guid>("FK_WORKFLOWSTATE");

        if (wfStateActionScope != wfStateNieuw)
        {
            return;
        }

        var wfInBehandeling = new Guid("026fb036-6fa6-494a-b6cc-5e53b04cd541");
        var wfResult = ExecuteWorkflowEvent("R_ACTIONSCOPE", actionScopeId, wfInBehandeling, null);

        if (wfResult.HasError)
        {
            Log.Error("Error", "In behandeling zetten actiekader is mislukt, oorzaak:" + wfResult.GetResult(), "");
            return;
        }

    }
}
