﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Formatting;
using Ridder.Common.Login;

public class SendSupportReactionMailByBrevo : WorkflowScriptInfo
{
    private const string BrevoApiKey = "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";

    public void Execute()
    {
        //DB
        //26-8-2024
        //Stuur support reactie mail via Breveo bij uitgaande portal uit bericht

        var inboundMessage = GetRecordset("R_INBOUNDMESSAGE", "FK_ACTIONSCOPE, FK_INBOUNDMESSAGETYPE, FK_EMPLOYEE", $"PK_R_INBOUNDMESSAGE = {(int)RecordId}", "")
            .DataTable.AsEnumerable().First();

        if (inboundMessage.Field<int>("FK_INBOUNDMESSAGETYPE") != 4)//4 = uitgaand portal bericht
        {
            return;
        }

        if (!inboundMessage.Field<int?>("FK_ACTIONSCOPE").HasValue)
        {
            Log.Error("Error", "Geen actiekader gevonden. Support mail kan niet verstuurd worden.", "");
            return;
        }

        var actionScope = GetRecordset("R_ACTIONSCOPE", "CODE, FK_BISHARPPORTALUSER", $"PK_R_ACTIONSCOPE = {inboundMessage.Field<int>("FK_ACTIONSCOPE")}", "")
            .DataTable.AsEnumerable().First();

        var bisharpPortalUserId = actionScope.Field<int?>("FK_BISHARPPORTALUSER") ?? 0;

        if (bisharpPortalUserId == 0)
        {
            Log.Error("Error", "Geen Bisharp portal user gevonden. Support mail kan niet verstuurd worden.", "");
            return;
        }

        var bisharpPortalUser = GetRecordset("U_BISHARPPORTALUSER", "FK_CONTACT, EMAIL",
            $"PK_U_BISHARPPORTALUSER = {bisharpPortalUserId}", "")
            .DataTable.AsEnumerable().Select(x => new User()
            {
                ContactId = x.Field<int?>("FK_CONTACT"),
                Email = x.Field<string>("EMAIL"),
            }).First();

        if (!bisharpPortalUser.ContactId.HasValue)
        {
            Log.Error("Error", "Geen contactpersoon gevonden bij de Bisharp portal user. Support mail kan niet verstuurd worden.", "");
            return;
        }

        if (string.IsNullOrEmpty(bisharpPortalUser.Email))
        {
            Log.Error("Error", "Geen mail adres gevonden bij de Bisharp portal user. Support mail kan niet verstuurd worden.", "");
            return;
        }

        var personId = GetRecordset("R_CONTACT", "FK_PERSON",
            $"PK_R_CONTACT = {bisharpPortalUser.ContactId}", "").DataTable.AsEnumerable().First().Field<int>("FK_PERSON");

        var person = GetRecordset("R_PERSON", "FIRSTNAME, NAMEPREFIX, LASTNAME",
            $"PK_R_PERSON = {personId}", "")
            .DataTable.AsEnumerable().Select(x => new Person()
            {
                FirstName = x.Field<string>("FIRSTNAME"),
                NamePrefix = x.Field<string>("NAMEPREFIX"),
                LastName = x.Field<string>("LASTNAME"),

            }).First();

        var employeeFirstName = GetEmployeeFirstName(inboundMessage.Field<int?>("FK_EMPLOYEE") ?? 0);

        if(string.IsNullOrEmpty(employeeFirstName))
        {
            Log.Error("Error", "Bepaling werknemer bij huidig bericht is mislukt. Support mail kan niet verstuurd worden.", "");
            return;
        }

        SendMailRequestModel requestModel = CreateRequestModel(bisharpPortalUser, person, employeeFirstName, actionScope.Field<string>("CODE"));

        var errorMessage = string.Empty;
        SendMailRequest(requestModel, ref errorMessage);
        if (!string.IsNullOrEmpty(errorMessage))
        {
            Log.Error("Error", $"Versturen welkomsmail is mislukt. Oorzaak: {errorMessage}.", "");
            return;
        }


    }

    private string GetEmployeeFirstName(int employeeId)
    {
        if(employeeId == 0)
        {
            return string.Empty;
        }

        var personId = GetRecordset("R_EMPLOYEE", "FK_PERSON", $"PK_R_EMPLOYEE = {employeeId}", "")
            .DataTable.AsEnumerable().First().Field<int?>("FK_PERSON") ?? 0;

        if(personId == 0)
        {
            return string.Empty;
        }

        return GetRecordset("R_PERSON", "FIRSTNAME", $"PK_R_PERSON = {personId}", "")
            .DataTable.AsEnumerable().First().Field<string>("FIRSTNAME");
    }

    private SendMailRequestModel CreateRequestModel(User bisharpPortalUser, Person person, string employeeFirstName, string supportTicketNumber)
    {
        return new SendMailRequestModel
        {
            Sender = new Sender() { },

            To = new Recipient[]
            {
                new Recipient()
                {
                    Email = bisharpPortalUser.Email,
                    Name = person.TotalName
                }
            },

            TemplateId = 18,
            Params = new Dictionary<string, string>()
            {
                { "userFirstName", person.FirstName },
                { "employeeFirstName", employeeFirstName },
                { "supportTicketNumber", supportTicketNumber },
                { "urlsupportticket", $"biportal.bisharp.nl/Support/Tickets/{supportTicketNumber}" }
            }
        };
    }

    private void SendMailRequest(SendMailRequestModel requestModel, ref string errorMessage)
    {
        HttpClient client = new HttpClient();

        client.DefaultRequestHeaders.Add("api-key", BrevoApiKey);

        HttpResponseMessage response;

        try
        {
            response = client.PostAsJsonAsync("https://api.brevo.com/v3/smtp/email", requestModel).Result;

            if (!response.IsSuccessStatusCode && response.StatusCode != System.Net.HttpStatusCode.Accepted && response.StatusCode != System.Net.HttpStatusCode.Created)
            {
                var message = response.Content.ReadAsStringAsync().Result;
                errorMessage = $"Verbinden Brevo API is mislukt, oorzaak: {(int)response.StatusCode} - {response.ReasonPhrase} - {message}";
            }
        }
        catch (Exception e)
        {
            errorMessage = $"Brevo API niet bereikbaar, error: {e}";
        }
    }

    class User
    {
        public int? ContactId { get; set; }
        public string Email { get; set; }
        public bool WelcomeMailSent { get; set; }
    }

    public class Person
    {
        public string FirstName { get; set; }
        public string NamePrefix { get; set; }
        public string LastName { get; set; }
        public string TotalName => string.IsNullOrEmpty(NamePrefix)
            ? $"{FirstName} {LastName}"
            : $"{FirstName} {NamePrefix} {LastName}";
    }

    public class SendMailRequestModel
    {
        [JsonProperty("sender")]
        public Sender Sender { get; set; }

        [JsonProperty("to")]
        public Recipient[] To { get; set; }

        [JsonProperty("cc")]
        public Recipient[] CC { get; set; }

        [JsonProperty("attachment")]
        public Attachment[] Attachments { get; set; }

        [JsonProperty("templateId")]
        public int TemplateId { get; set; }

        public Dictionary<string, string> Params { get; set; }
    }

    public class Sender
    {
        [JsonProperty("email")]
        public string Email { get; set; } = "no-reply@bisharp.nl";

        [JsonProperty("name")]
        public string Name { get; set; } = "Bisharp";
    }

    public class Recipient
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class Attachment
    {
        [JsonProperty("content")]
        public string Content { get; set; }

        [JsonProperty("name")]
        public string Filename { get; set; }
    }

}
