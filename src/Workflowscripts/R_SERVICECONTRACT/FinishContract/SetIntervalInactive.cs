﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class SetIntervalInactive : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //SP
        //5-1-2022
        //Bij beëindigen servicecontract de serviceinterval op inactief zetten

        var serviceObjectIds = GetRecordset("R_SERVICECONTRACTDETAIL", "FK_SERVICEOBJECT",
            $"FK_SERVICECONTRACT = {id}", "").DataTable.AsEnumerable().Select(x => x.Field<int>("FK_SERVICEOBJECT")).ToList();

        if (!serviceObjectIds.Any())
        {
            return; //Niet van toepassing
        }

        var serviceTypeIds = GetRecordset("R_SERVICECONTRACTSERVICETYPE", "FK_SERVICETYPE",
            $"FK_SERVICECONTRACT = {id}", "").DataTable.AsEnumerable().Select(x => x.Field<int>("FK_SERVICETYPE")).ToList();

        if (!serviceTypeIds.Any())
        {
            return; //Niet van toepassing
        }

        var serviceIntervalIds = GetRecordset("R_SERVICEINTERVAL", "PK_R_SERVICEINTERVAL",
            $"FK_SERVICEOBJECT IN ({string.Join(",", serviceObjectIds)}) AND FK_SERVICETYPE IN ({string.Join(",", serviceTypeIds)})", "")
            .DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_SERVICEINTERVAL")).ToList();

        foreach (var serviceIntervalId in serviceIntervalIds)
        {
            var rsServiceinterval = GetRecordset("R_SERVICEINTERVAL", "INACTIVE",
                $"PK_R_SERVICEINTERVAL = {serviceIntervalId}", "");
            rsServiceinterval.MoveFirst();

            rsServiceinterval.SetFieldValue("INACTIVE", true);

            var updateResult = rsServiceinterval.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                Log.Error("Error", $"Interval op inactief zetten mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "");
                return;
            }

        }

    }
}
