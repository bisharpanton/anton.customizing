﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class AddDiscountDetail : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //28-12-2021
        //Haal het kortingsbedrag op uit het contract en maak hier een factuurregel van

        if(!GetUserInfo().CompanyName.Equals("Bisharp"))
        {
            return;
        }

        var serviceContract = GetRecordset("R_SERVICECONTRACT", "TOTALDISCOUNTPERMONTH, DISCOUNTDESCRIPTION, CUSTOMERORDERNUMBER",
            $"PK_R_SERVICECONTRACT = {id}", "").DataTable.AsEnumerable().First();


        var lastCreatedInvoiceDetail = GetRecordset("R_SALESINVOICEDETAILSERVICECONTRACT", "FK_SALESINVOICE, DATECREATED",
            $"FK_SERVICECONTRACT = {id}", "DATECREATED DESC").DataTable.AsEnumerable().First();

        if(lastCreatedInvoiceDetail.Field<DateTime>("DATECREATED") < DateTime.Now.AddMinutes(-1))
        {
            return; //Regel is ouder dan 1 minuut. Dit kan niet.
        }
        
        if(!string.IsNullOrEmpty(serviceContract.Field<string>("CUSTOMERORDERNUMBER")))
        {
            var rsSalesInvoice = GetRecordset("R_SALESINVOICE", "CUSTOMERORDERNUMBER", $"PK_R_SALESINVOICE = {lastCreatedInvoiceDetail.Field<int>("FK_SALESINVOICE")}", "");
            rsSalesInvoice.MoveFirst();
            rsSalesInvoice.SetFieldValue("CUSTOMERORDERNUMBER", serviceContract.Field<string>("CUSTOMERORDERNUMBER"));
            rsSalesInvoice.Update();
        }


        if (serviceContract.Field<double>("TOTALDISCOUNTPERMONTH") == 0.0)
        {
            return; //Niet van toepassing
        }

        var rsSalesInvoiceDetailsMisc = GetRecordset("R_SALESINVOICEDETAILMISC", "", "PK_R_SALESINVOICEDETAILMISC IS NULL", "");
        rsSalesInvoiceDetailsMisc.UseDataChanges = true;

        rsSalesInvoiceDetailsMisc.AddNew();
        rsSalesInvoiceDetailsMisc.SetFieldValue("FK_SALESINVOICE", lastCreatedInvoiceDetail.Field<int>("FK_SALESINVOICE"));
        rsSalesInvoiceDetailsMisc.SetFieldValue("FK_MISCELLANEOUS", 9);
        rsSalesInvoiceDetailsMisc.SetFieldValue("QUANTITY", 1.0);

        if (!string.IsNullOrEmpty(serviceContract.Field<string>("DISCOUNTDESCRIPTION")))
        {
            rsSalesInvoiceDetailsMisc.SetFieldValue("DESCRIPTION", serviceContract.Field<string>("DISCOUNTDESCRIPTION"));
        }

        rsSalesInvoiceDetailsMisc.SetFieldValue("GROSSSALESPRICE", serviceContract.Field<double>("TOTALDISCOUNTPERMONTH"));

        var updateResult = rsSalesInvoiceDetailsMisc.Update2();

        if(updateResult.Any(x => x.HasError))
        {
            Log.Error("Error", $"Aanmaken kortingsregel mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "");
            return;
        }
    }
}
