﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;
using Ridder.Common.ItemManagement;
using System.Runtime.InteropServices.WindowsRuntime;

public class CheckForOutstandingPurchaseInvoiceDetails : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //26-6-2024
        //Als er een inkoopfactuur eerder wordt ontvangen en de inkoop wordt daarna ontvangen gemeld dan wordt de ontvangst automatisch gekoppeld als het aantal overeenkomt.
        //Als het aantal niet overeenkomt dan willen we de gebruiker een melding geven dat de inkoopfactuur en de ontvangst niet overeenkomen.

        var sources = new string[] { "ITEM", "MISC", "OUTSOURCED" };

        foreach (var source in sources)
        {
            var goodsReceiptDetails = GetRecordset($"R_GOODSRECEIPTDETAIL{source}", $"FK_PURCHASEORDERDETAIL{source}, QUANTITY, DESCRIPTION",
                $"FK_GOODSRECEIPT = {id} AND FK_PURCHASEORDERDETAIL{source} IS NOT NULL", "")
                .DataTable.AsEnumerable().ToList();

            if (!goodsReceiptDetails.Any())
            {
                continue;
            }

            var purchaseOrderDetailIds = goodsReceiptDetails.Select(x => x.Field<int>($"FK_PURCHASEORDERDETAIL{source}")).ToList();

            var purchaseInvoiceDetails = GetRecordset($"R_PURCHASEINVOICEDETAIL{source}", $"FK_PURCHASEORDERDETAIL{source}, QUANTITY",
                $"FK_PURCHASEORDERDETAIL{source} IN ({string.Join(",", purchaseOrderDetailIds)}) AND FK_GOODSRECEIPTDETAIL{source} IS NULL", "")
                .DataTable.AsEnumerable().ToList();

            if (!purchaseInvoiceDetails.Any())
            {
                continue;
            }

            foreach (var purchaseInvoiceDetail in purchaseInvoiceDetails)
            {
                var quantityReceived = goodsReceiptDetails.Where(x => x.Field<int>($"FK_PURCHASEORDERDETAIL{source}") == purchaseInvoiceDetail.Field<int>($"FK_PURCHASEORDERDETAIL{source}"))
                    .Sum(x => x.Field<double>("QUANTITY"));
                Log.Info("Info", $"LET OP: Aantal ontvangst komt niet overeen met aantal inkoopfactuur. Er wordt {quantityReceived:n2} ontvangen en er staat een inkoopfactuur klaar met aantal {purchaseInvoiceDetail.Field<double>("QUANTITY"):n2}. Deze situatie levert problemen op bij de administratie.", "");
            }
        }
    }
}
