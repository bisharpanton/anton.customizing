﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;
using Ridder.Common.ItemManagement;

public class CheckMandagenstaatRegister : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        var wkaActive = GetRecordset("R_CRMSETTINGS", "WKAACTIVE",
            "", "").DataTable.AsEnumerable().First().Field<bool>("WKAACTIVE");

        if (!wkaActive)
        {
            return; //Alleen van toepassing indien vinkje 'WKA functionaliteit actief' aanstaat
        }


        //DB
        //27-3-2023
        //Blokkeer ontvangen melden als mandagenstaatregister niet volledig is

        var originalPurchaseOrderId = GetRecordset("R_GOODSRECEIPT", "FK_ORIGINALPURCHASEORDER", $"PK_R_GOODSRECEIPT = {id}", "")
            .DataTable.AsEnumerable().First().Field<int?>("FK_ORIGINALPURCHASEORDER") ?? 0;

        if(originalPurchaseOrderId == 0)
        {
            return;
        }

        var wkaApplicable = GetRecordset("R_PURCHASEORDER", "WKA", $"PK_R_PURCHASEORDER = {originalPurchaseOrderId}", "")
            .DataTable.AsEnumerable().First().Field<bool>("WKA");

        if(!wkaApplicable)
        {
            return;
        }

        var purchaseOrderDetailsOutsourced = GetRecordset("R_PURCHASEORDERDETAILOUTSOURCED",
            "FK_OUTSOURCEDACTIVITY, QUANTITY", $"FK_PURCHASEORDER = {originalPurchaseOrderId}", "")
            .DataTable.AsEnumerable().ToList();

        if(!purchaseOrderDetailsOutsourced.Any())
        {
            return;
        }

        var outsourcedActivities = GetRecordset("R_OUTSOURCEDACTIVITY", "FK_UNIT, MANDAYNOTAPPLICABLE",
            $"PK_R_OUTSOURCEDACTIVITY IN ({string.Join(",", purchaseOrderDetailsOutsourced.Select(x => x.Field<int>("FK_OUTSOURCEDACTIVITY")).Distinct())})", "")
            .DataTable.AsEnumerable().ToList();

        var units = GetRecordset("R_UNIT", "FACTOR, QUANTITY",
            $"PK_R_UNIT IN ({string.Join(",", outsourcedActivities.Select(x => x.Field<int>("FK_UNIT")).Distinct())})", "")
            .DataTable.AsEnumerable().ToList();

        var supplierMandayRegisterDetails = GetRecordset("U_SUPPLIERMANDAYREGISTERDETAILS", "FK_PURCHASEORDERDETAILOUTSOURCED, TIMEWORKED",
            $"FK_PURCHASEORDERDETAILOUTSOURCED IN ({string.Join(",", purchaseOrderDetailsOutsourced.Select(x => x.Field<int>("PK_R_PURCHASEORDERDETAILOUTSOURCED")))})", "")
            .DataTable.AsEnumerable().ToList();

        var zzpProjectTimes = GetRecordset("R_PROJECTTIME", "FK_WKAPURCHASEORDERDETAILOUTSOURCED, TIMEEMPLOYEE",
            $"FK_WKAPURCHASEORDERDETAILOUTSOURCED IN ({string.Join(",", purchaseOrderDetailsOutsourced.Select(x => x.Field<int>("PK_R_PURCHASEORDERDETAILOUTSOURCED")))})", "")
            .DataTable.AsEnumerable().ToList();

        foreach (var purchaseOrderDetailOutsourced in purchaseOrderDetailsOutsourced)
        {
            var purchaseOrderDetailOutsourcedId = purchaseOrderDetailOutsourced.Field<int>("PK_R_PURCHASEORDERDETAILOUTSOURCED");
            var outsourcedActivity = outsourcedActivities.First(x => x.Field<int>("PK_R_OUTSOURCEDACTIVITY") == purchaseOrderDetailOutsourced.Field<int>("FK_OUTSOURCEDACTIVITY"));
            var unit = units.First(x => x.Field<int>("PK_R_UNIT") == outsourcedActivity.Field<int>("FK_UNIT"));

            if (unit.Field<UnitQuantity>("QUANTITY") != UnitQuantity.Time)
            {
                return;//Alleen UW met eenheid tijd controleren
            }

            if(outsourcedActivity.Field<bool>("MANDAYNOTAPPLICABLE") == true)
            {
                return;//Alleen UW controleren waarbij vinkje mandagstaten n.v.t. uitstaat
            }

            var quantityPurchased = purchaseOrderDetailOutsourced.Field<double>("QUANTITY") / unit.Field<double>("FACTOR");

            var tsQuantityPurchase = TimeSpan.FromHours(quantityPurchased);

            var supplierMandayRegisterDetailsThisPurchaseOrderDetailOutsourced = supplierMandayRegisterDetails
                .Where(x => x.Field<int>("FK_PURCHASEORDERDETAILOUTSOURCED") == purchaseOrderDetailOutsourcedId).ToList();

            var zzpProjectTimesThisPurchaseOrderDetailOutsourced = zzpProjectTimes
                .Where(x => x.Field<int>("FK_WKAPURCHASEORDERDETAILOUTSOURCED") == purchaseOrderDetailOutsourcedId).ToList();

            var totalTimeWka = supplierMandayRegisterDetailsThisPurchaseOrderDetailOutsourced.Any()
                ? supplierMandayRegisterDetailsThisPurchaseOrderDetailOutsourced.Sum(x => x.Field<long>("TIMEWORKED"))
                : zzpProjectTimesThisPurchaseOrderDetailOutsourced.Sum(x => x.Field<long>("TIMEEMPLOYEE"));

            var tsTotalTimeWka = TimeSpan.FromTicks(totalTimeWka);

            if (Math.Floor(tsQuantityPurchase.TotalHours) != Math.Floor(tsTotalTimeWka.TotalHours) || tsQuantityPurchase.Minutes != tsTotalTimeWka.Minutes)
            {
                Log.Error("Error",
                    $"WKA mandagenregister niet compleet bij inkooporderregel {GetRecordTag("R_PURCHASEORDERDETAILOUTSOURCED", purchaseOrderDetailOutsourcedId)}. Totale tijd mandagenregister ({Math.Floor(tsTotalTimeWka.TotalHours):n0}u {tsTotalTimeWka.Minutes}m) komt niet overeen met aantal uur uit inkoopregel ({Math.Floor(tsQuantityPurchase.TotalHours):n0}u {tsQuantityPurchase.Minutes}m).", "");
                return;
            }
        }
    }
}
