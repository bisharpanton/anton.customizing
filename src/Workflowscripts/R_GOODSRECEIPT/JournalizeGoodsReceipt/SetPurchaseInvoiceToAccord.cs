﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;
using Ridder.Common.ItemManagement;
using System.Runtime.InteropServices.WindowsRuntime;

public class SetPurchaseInvoiceToAccord : WorkflowScriptInfo
{
    public void Execute()
    {
        var id = (int)RecordId;

        //DB
        //30-5-2024
        //Check of ontvangst direct is gekoppeld aan een factuur en of de factuur al akkoord is vanuit IC. Zet dan de factuur naar status 'Akkoord'

        var purchaseInvoiceIds = GetPurchaseInvoiceIds(id);

        if(!purchaseInvoiceIds.Any())
        {
            return;
        }

        var purchaseInvoiceIdsToSetAccord = FilterPurchaseInvoiceWhereAllDetailsAreLinkedToGoodsReceiptDetail(purchaseInvoiceIds);

        if (!purchaseInvoiceIdsToSetAccord.Any())
        {
            return;
        }

        foreach (var purchaseInvoiceId in purchaseInvoiceIdsToSetAccord)
        {
            SetPurchaseInvoiceAccord(purchaseInvoiceId);
        }
    }

    private void SetPurchaseInvoiceAccord(int purchaseInvoiceId)
    {
        Guid wfSetToAccord = new Guid("0075174d-1a1a-4525-b8f4-06c267d803d2");

        var parameters = new Dictionary<string, object>();
        parameters.Add("ExecutedFromSetDeclarationTodoDone", false);

        var result = ExecuteWorkflowEvent("R_PURCHASEINVOICE", purchaseInvoiceId, wfSetToAccord, parameters);

        if (result.HasError)
        {
            Log.Error("Error", $"Accorderen inkoopfactuur is mislukt, oorzaak: {result.GetResult()}", "");
        }
    }

    private List<int> FilterPurchaseInvoiceWhereAllDetailsAreLinkedToGoodsReceiptDetail(List<int> purchaseInvoiceIds)
    {
        var result = new List<int>();
        
        foreach (var purchaseInvoiceId in purchaseInvoiceIds)
        {
            if (AllAllDetailsAreLinkedToGoodsReceiptDetailCore(purchaseInvoiceId))
            {
                result.Add(purchaseInvoiceId);
            }
        }


        return result;
    }

    private bool AllAllDetailsAreLinkedToGoodsReceiptDetailCore(int purchaseInvoiceId)
    {
        var sources = new string[] { "ITEM", "MISC", "OUTSOURCED" };

        foreach (var source in sources)
        {
            var purchaseInvoiceDetails = GetRecordset($"R_PURCHASEINVOICEDETAIL{source}", $"FK_GOODSRECEIPTDETAIL{source}",
                $"FK_PURCHASEINVOICE = {purchaseInvoiceId}", "").DataTable.AsEnumerable().ToList();

            if(!purchaseInvoiceDetails.Any())
            {
                continue;
            }

            if (purchaseInvoiceDetails.Any(x => !x.Field<int?>($"FK_GOODSRECEIPTDETAIL{source}").HasValue))
            {
                return false; //Er is een factuurregel nog niet gekoppeld aan een ontvangstregel.
            }
        }

        return true;
    }

    private List<int> GetPurchaseInvoiceIds(int goodsReceiptId)
    {
        var result = new List<int>();

        var sources = new string[] { "ITEM", "MISC", "OUTSOURCED" };

        foreach (var source in sources)
        {
            var goodsReceiptDetailIds = GetRecordset($"R_GOODSRECEIPTDETAIL{source}", $"PK_R_GOODSRECEIPTDETAIL{source}", $"FK_GOODSRECEIPT = {goodsReceiptId}", "")
                .DataTable.AsEnumerable().Select(x => x.Field<int>($"PK_R_GOODSRECEIPTDETAIL{source}")).ToList();

            if (!goodsReceiptDetailIds.Any())
            {
                continue;
            }

            var purchaseInvoiceDetails = GetRecordset($"R_PURCHASEINVOICEDETAIL{source}", $"FK_PURCHASEINVOICE",
                $"FK_GOODSRECEIPTDETAIL{source} IN ({string.Join(",", goodsReceiptDetailIds)})", "").DataTable.AsEnumerable().ToList();

            if (!purchaseInvoiceDetails.Any())
            {
                continue;
            }

            result.AddRange(purchaseInvoiceDetails.Select(x => x.Field<int>("FK_PURCHASEINVOICE")));
        }

        return result.Distinct().ToList();
    }
}
