﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.WorkflowModel.Activities;

public class SetDeclarationOfPerformance : WorkflowScriptInfo
{
    public void Execute()
    {
        // HVE
        // 23-3-2021
        // Set Declaration of performance true
        var goodsreceiptId = (int)RecordId;

        var goodsreceipt = GetRecordset("R_GOODSRECEIPT", "DECLARATIONOFPERFORMANCE", 
            $"PK_R_GOODSRECEIPT = {goodsreceiptId}", "");
        goodsreceipt.UseDataChanges = true;

        goodsreceipt.MoveFirst();
        goodsreceipt.SetFieldValue("DECLARATIONOFPERFORMANCE", true);
        goodsreceipt.Update();

    }
}
