﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;

public class CreateProjectStructure_RidderScript : WorkflowCommandScript
{
    public void Execute()
    {
        //SP
        //07-06-2022: Aanmaken van projectstructuur (bonnen, fases en budgetregels) op basis van gekopieerde tekst uit Excel
        //Opbouw: Bonnummer/ Som totaal budget/ Omschrijving / Type (arbeid, inkoop of gecombineerd)

        if(TableName == null || TableName != "R_MAINPROJECT")
        {
            MessageBox.Show("Dit script werkt alleen op de tabel 'Projecten'.", "Ridder iQ", MessageBoxButtons.OK,
                MessageBoxIcon.Error);
            return;
        }

        if (RecordIds == null || !RecordIds.Any())
        {
            return;
        }

        var projectId = RecordIds[0];

        var orderId = GetRecordset("R_MAINPROJECT", "FK_ORDER",
            $"PK_R_MAINPROJECT = {projectId}", "").DataTable.AsEnumerable().FirstOrDefault().Field<int?>("FK_ORDER") as int? ?? 0;

        if(orderId == 0)
        {
            MessageBox.Show("Geen hoofdorder gekoppeld aan project.", "Ridder iQ", MessageBoxButtons.OK,
                MessageBoxIcon.Error);
            return;
        }

        var rsProjectPhase = GetRecordset("R_PROJECTPHASE", "", "PK_R_PROJECTPHASE = -1", "");
        rsProjectPhase.MoveFirst();
        rsProjectPhase.UseDataChanges = true;

        var rsJoborder = GetRecordset("R_JOBORDER", "", "PK_R_JOBORDER = -1", "");
        rsJoborder.MoveFirst();
        rsJoborder.UseDataChanges = true;

        var rsProjectBudgetDetails = GetRecordset("R_PROJECTBUDGETDETAIL", "", "PK_R_PROJECTBUDGETDETAIL = -1", "");
        rsProjectBudgetDetails.MoveFirst();
        rsProjectBudgetDetails.UseDataChanges = false;
        rsProjectBudgetDetails.UpdateWhenMoveRecord = false;

        string clipboardData = Clipboard.GetText();

        foreach (var myString in clipboardData.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
        {
            string[] myStrValues = myString.Split('\t');

            rsProjectPhase.AddNew();
            rsProjectPhase.Fields["FK_MAINPROJECT"].Value = projectId;
            rsProjectPhase.Fields["CODE"].Value = Convert.ToString(myStrValues[0]);
            rsProjectPhase.Fields["DESCRIPTION"].Value = Convert.ToString(myStrValues[2]);
            rsProjectPhase.Fields["EXTERNALKEY"].Value = $"{projectId}{Convert.ToString(myStrValues[0])}{(DateTime.Now).Date.ToString()}";

            rsJoborder.AddNew();
            rsJoborder.Fields["FK_ORDER"].Value = orderId;
            rsJoborder.Fields["JOBORDERNUMBER"].Value = Convert.ToInt32(myStrValues[0]);
            rsJoborder.Fields["DESCRIPTION"].Value = Convert.ToString(myStrValues[2]);
            rsJoborder.Fields["EXTERNALKEY"].Value = $"{projectId}{Convert.ToString(myStrValues[0])}{(DateTime.Now).Date.ToString()}";
            //rsJoborder.Fields["JOBORDERTYPECHOICE"].Value = Convert.ToString(myStrValues[2]);

            rsProjectBudgetDetails.AddNew();
            rsProjectBudgetDetails.Fields["FK_MAINPROJECT"].Value = projectId;
            rsProjectBudgetDetails.Fields["TOTALCOSTPRICE"].Value = Convert.ToDouble(myStrValues[1]);
            rsProjectBudgetDetails.Fields["REMAININGCOST"].Value = Convert.ToDouble(myStrValues[1]);
            rsProjectBudgetDetails.Fields["EXPECTEDCOSTPRICE"].Value = Convert.ToDouble(myStrValues[1]);
            rsProjectBudgetDetails.Fields["DESCRIPTION"].Value = Convert.ToString(myStrValues[2]);
            rsProjectBudgetDetails.Fields["EXTERNALKEY"].Value = $"{projectId}{Convert.ToString(myStrValues[0])}{(DateTime.Now).Date.ToString()}";

            if (Convert.ToString(myStrValues[3]) == "Arbeid")
            {
                var unitLabour = GetRecordset("R_CRMSETTINGS", "FK_UNITPROJECTLABOUR",
                    "", "").DataTable.AsEnumerable().First().Field<int?>("FK_UNITPROJECTLABOUR");

                if (!unitLabour.HasValue)
                {
                    MessageBox.Show("Geen standaard eenheid ingevuld voor arbeid. Controleer de CRM-instellingen.", "Ridder iQ", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                    return;
                }

                rsProjectBudgetDetails.Fields["BUDGETTYPE"].Value = 1;//Arbeid
                rsProjectBudgetDetails.Fields["LABOR"].Value = true;
                rsProjectBudgetDetails.Fields["EXPECTEDQUANTITY"].Value = 1.0;
                rsProjectBudgetDetails.Fields["QUANTITY"].Value = 3600.0;
                rsProjectBudgetDetails.Fields["FK_UNIT"].Value = unitLabour;

            }
            else if(Convert.ToString(myStrValues[3]) == "Inkoop")
            {
                var unitMaterial = GetRecordset("R_CRMSETTINGS", "FK_UNITPROJECTMATERIAL",
                    "", "").DataTable.AsEnumerable().First().Field<int?>("FK_UNITPROJECTMATERIAL");

                if (!unitMaterial.HasValue)
                {
                    MessageBox.Show("Geen standaard eenheid ingevuld voor materiaal. Controleer de CRM-instellingen.", "Ridder iQ", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                    return;
                }

                rsProjectBudgetDetails.Fields["BUDGETTYPE"].Value = 16;//Gecombineerd
                rsProjectBudgetDetails.Fields["LABOR"].Value = false;
                rsProjectBudgetDetails.Fields["MATERIAL"].Value = true;
                rsProjectBudgetDetails.Fields["MISC"].Value = true;
                rsProjectBudgetDetails.Fields["OUTSOURCED"].Value = true;
                rsProjectBudgetDetails.Fields["QUANTITY"].Value = 1.0;
                rsProjectBudgetDetails.Fields["EXPECTEDQUANTITY"].Value = 1.0;
                rsProjectBudgetDetails.Fields["FK_UNIT"].Value = unitMaterial;
            }
            else if (Convert.ToString(myStrValues[3]) == "Gecombineerd")
            {
                var unitMaterial = GetRecordset("R_CRMSETTINGS", "FK_UNITPROJECTMATERIAL",
                    "", "").DataTable.AsEnumerable().First().Field<int?>("FK_UNITPROJECTMATERIAL");

                if (!unitMaterial.HasValue)
                {
                    MessageBox.Show("Geen standaard eenheid ingevuld voor materiaal. Controleer de CRM-instellingen.", "Ridder iQ", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                    return;
                }

                rsProjectBudgetDetails.Fields["BUDGETTYPE"].Value = 16;//Gecombineerd
                rsProjectBudgetDetails.Fields["LABOR"].Value = true;
                rsProjectBudgetDetails.Fields["MATERIAL"].Value = true;
                rsProjectBudgetDetails.Fields["MISC"].Value = true;
                rsProjectBudgetDetails.Fields["OUTSOURCED"].Value = true;
                rsProjectBudgetDetails.Fields["QUANTITY"].Value = 1.0;
                rsProjectBudgetDetails.Fields["EXPECTEDQUANTITY"].Value = 1.0;
                rsProjectBudgetDetails.Fields["FK_UNIT"].Value = unitMaterial;
            }
            else
            {
                MessageBox.Show("Ingevoerde type niet een van 'Arbeid', 'Inkoop' of 'Gecombineerd'.", "Ridder iQ", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }
        }

        var updateResultProjectPhase = rsProjectPhase.Update2();
        if (updateResultProjectPhase.Any(x => x.HasError))
        {
            MessageBox.Show($"Het aanmaken van de projectfasen is mislukt, oorzaak: {updateResultProjectPhase.First(x => x.HasError).GetResult()}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        var updateResultJoborder = rsJoborder.Update2();
        if (updateResultJoborder.Any(x => x.HasError))
        {
            MessageBox.Show($"Het aanmaken van de bonnen is mislukt, oorzaak: {updateResultJoborder.First(x => x.HasError).GetResult()}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        var updateResultProjectBudgetDetails = rsProjectBudgetDetails.Update2();
        if (updateResultProjectBudgetDetails.Any(x => x.HasError))
        {
            MessageBox.Show($"Het aanmaken van de budgetregels is mislukt, oorzaak: {updateResultProjectBudgetDetails.First(x => x.HasError).GetResult()}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        var uniqueIndexes = rsProjectPhase.DataTable.AsEnumerable().Select(x => x.Field<string>("EXTERNALKEY")).ToList();
        var createdIds = new List<createdIds>();

        foreach (var uniqueIndex in uniqueIndexes)
        {
            var projectPhaseId = rsProjectPhase.DataTable.AsEnumerable().FirstOrDefault(x => x.Field<string>("EXTERNALKEY").Equals(uniqueIndex)).Field<int>("PK_R_PROJECTPHASE");
            var joborderId = rsJoborder.DataTable.AsEnumerable().FirstOrDefault(x => x.Field<string>("EXTERNALKEY").Equals(uniqueIndex)).Field<int>("PK_R_JOBORDER");

            var projectBudgetDetail = rsProjectBudgetDetails.DataTable.AsEnumerable().First(x => x.Field<string>("EXTERNALKEY").Equals(uniqueIndex));
            var projectBudgetId = projectBudgetDetail.Field<int>("PK_R_PROJECTBUDGETDETAIL");
            var isLabor = projectBudgetDetail.Field<int>("BUDGETTYPE") == 1; //Arbeid

            createdIds.Add(new createdIds
            {
                projectPhaseId = projectPhaseId,
                joborderId = joborderId,
                projectBudgetId = projectBudgetId,
                IsLabor = isLabor,
            });
        }

        Guid tableOrder = new Guid("abcc4e35-c70a-49e9-bd76-6c8a10c9fbb8");
        Guid tablePhase = new Guid("7db2fca6-1cb4-4a2c-9f96-3b4376a5d37b");
        Guid tableJoborder = new Guid("7de0ac3e-9dfa-47e0-b113-63d3cac55f90");
        Guid tableBudget = new Guid("51e8daf8-b92c-4852-9518-433679420946");

        var rsWorkBreakdownInfo = GetRecordset("R_WORKBREAKDOWNINFO", "", "PK_R_WORKBREAKDOWNINFO = -1", "");
        rsWorkBreakdownInfo.MoveFirst();
        rsWorkBreakdownInfo.UseDataChanges = true;

        foreach (createdIds createdId in createdIds)
        {
            rsWorkBreakdownInfo.AddNew();
            rsWorkBreakdownInfo.Fields["FK_MAINPROJECT"].Value = projectId;

            rsWorkBreakdownInfo.Fields["FK_PARENTTABLEINFO"].Value = tableOrder;
            rsWorkBreakdownInfo.Fields["PARENTRECORDID"].Value = orderId;

            rsWorkBreakdownInfo.Fields["FK_TABLEINFO"].Value = tablePhase;
            rsWorkBreakdownInfo.Fields["RECORDID"].Value = createdId.projectPhaseId;

            rsWorkBreakdownInfo.AddNew();
            rsWorkBreakdownInfo.Fields["FK_MAINPROJECT"].Value = projectId;

            rsWorkBreakdownInfo.Fields["FK_PARENTTABLEINFO"].Value = tablePhase;
            rsWorkBreakdownInfo.Fields["PARENTRECORDID"].Value = createdId.projectPhaseId;

            rsWorkBreakdownInfo.Fields["FK_TABLEINFO"].Value = tableJoborder;
            rsWorkBreakdownInfo.Fields["RECORDID"].Value = createdId.joborderId;

            rsWorkBreakdownInfo.AddNew();
            rsWorkBreakdownInfo.Fields["FK_MAINPROJECT"].Value = projectId;

            rsWorkBreakdownInfo.Fields["FK_PARENTTABLEINFO"].Value = tablePhase;
            rsWorkBreakdownInfo.Fields["PARENTRECORDID"].Value = createdId.projectPhaseId;

            rsWorkBreakdownInfo.Fields["FK_TABLEINFO"].Value = tableBudget;
            rsWorkBreakdownInfo.Fields["RECORDID"].Value = createdId.projectBudgetId;

        }

        var updateResultWorkBreakdownInfo = rsWorkBreakdownInfo.Update2();
        if (updateResultWorkBreakdownInfo.Any(x => x.HasError))
        {
            MessageBox.Show($"Het aanmaken van de WBS is mislukt, oorzaak: {updateResultWorkBreakdownInfo.First(x => x.HasError).GetResult()}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        CreateJoborderDetailsWorkactivity(orderId, createdIds);
    }

    private void CreateJoborderDetailsWorkactivity(int orderId, List<createdIds> createdIds)
    {
        var laborCreatedIds = createdIds.Where(x => x.IsLabor).ToList();

        if (!laborCreatedIds.Any())
        {
            return;
        }

        var workactivitiesToCreate = GetRecordset("R_WORKACTIVITY", "PK_R_WORKACTIVITY, DEFAULTTIME",
            $"PREPARATIONWORKACTIVITY = 1", "").DataTable.AsEnumerable().ToList();

        if (!workactivitiesToCreate.Any())
        {
            return;
        }

        var rsJoborderDetailWorkactivity =
            GetRecordset("R_JOBORDERDETAILWORKACTIVITY", "", "PK_R_JOBORDERDETAILWORKACTIVITY = 1", "");
        rsJoborderDetailWorkactivity.UpdateWhenMoveRecord = false;
        rsJoborderDetailWorkactivity.UseDataChanges = true;
        rsJoborderDetailWorkactivity.MoveFirst();

        foreach (var createdIdsRecord in laborCreatedIds)
        {
            foreach (var workactivityToCreate in workactivitiesToCreate)
            {
                rsJoborderDetailWorkactivity.AddNew();
                rsJoborderDetailWorkactivity.Fields["FK_ORDER"].Value = orderId;
                rsJoborderDetailWorkactivity.Fields["FK_JOBORDER"].Value = createdIdsRecord.joborderId;
                rsJoborderDetailWorkactivity.Fields["FK_WORKACTIVITY"].Value = workactivityToCreate.Field<int>("PK_R_WORKACTIVITY");

                if(!(workactivityToCreate.Field<int?>("DEFAULTTIME").HasValue))
                {
                    throw new Exception($"Aanmaken standaard bewerkingen mislukt. Geen standaard tijd ingevuld.");
                }

                rsJoborderDetailWorkactivity.Fields["OPERATIONTIME"].Value = workactivityToCreate.Field<long>("DEFAULTTIME");
            }
        }

        //rsJoborderDetailWorkactivity.MoveFirst();
        var updateResult = rsJoborderDetailWorkactivity.Update2();

        if (updateResult != null && updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Het aanmaken van de bewerkingen is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }
    }

    public class createdIds
    {
        public int projectPhaseId { get; set; }
        public int joborderId { get; set; }
        public int projectBudgetId { get; set; }
        public bool IsLabor { get; set; }
    }
}
