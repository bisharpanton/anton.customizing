//  <auto-generated />
namespace PurchaseContractAccepted.Models
{
    using System;
    
    
    ///  <summary>
    ///  	 Table: R_PURCHASESETTINGS
    ///  </summary>
    ///  <remarks>
    /// 	Available columns (for debugging, not necessarily available in this class):
    ///  	CREATOR, DATECHANGED, DATECREATED, DELIVERYTIMEINWORKDAYS, EXTERNALINVOICENUMBERUNIQUE, EXTERNALKEY, FK_DEFAULTMISC, FK_DEFAULTTYPEPURCHASE, FK_R_PURCHASESETTINGS, FK_WORKFLOWSTATE
    ///  	MARGINBEFORESTARTPRODUCTION, MARGINBEFORESTARTPRODUCTIONOUTSOURCED, MARGINBEFORESTARTSERVICE, PK_R_PURCHASESETTINGS, RECORDLINK, USERCHANGED
    ///  
    ///  	Filter used: FK_TABLEINFO = '57bb9388-569d-48f3-ae39-1046a24fd6d6' AND (ISSECRET = 0)
    ///  	Included columns: FK_DEFAULTTYPEPURCHASE, FK_DEFAULTMISC
    ///  	Excluded columns: 
    ///  </remarks>
    public partial class PurchaseSettings
    {
        
        private System.Nullable<int> _FK_DEFAULTMISC;
        
        private System.Nullable<int> _FK_DEFAULTTYPEPURCHASE;
        
        private int _PK_R_PURCHASESETTINGS;
        
        /// <summary>
        /// 	Default misc
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: R_PURCHASESETTINGS
        /// </remarks>
        public virtual System.Nullable<int> FK_DEFAULTMISC
        {
            get
            {
                return this._FK_DEFAULTMISC;
            }
            set
            {
                this._FK_DEFAULTMISC = value;
            }
        }
        
        /// <summary>
        /// 	Default type purchase
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: R_PURCHASESETTINGS
        /// </remarks>
        public virtual System.Nullable<int> FK_DEFAULTTYPEPURCHASE
        {
            get
            {
                return this._FK_DEFAULTTYPEPURCHASE;
            }
            set
            {
                this._FK_DEFAULTTYPEPURCHASE = value;
            }
        }
        
        /// <summary>
        /// 	Purchase settings id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        public virtual int PK_R_PURCHASESETTINGS
        {
            get
            {
                return this._PK_R_PURCHASESETTINGS;
            }
            set
            {
                this._PK_R_PURCHASESETTINGS = value;
            }
        }
    }
}
