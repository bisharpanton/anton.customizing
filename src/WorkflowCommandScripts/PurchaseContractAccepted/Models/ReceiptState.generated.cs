namespace PurchaseContractAccepted.Models
{
    using System;
    using System.ComponentModel;
    
    
    public enum ReceiptState : int
    {
        
        /// <summary>
        /// Waiting for goods
        /// </summary>
        [Description("Waiting for goods")]
        Waiting_for_goods = 1,
        
        /// <summary>
        /// Goods received
        /// </summary>
        [Description("Goods received")]
        Goods_received = 2,
        
        /// <summary>
        /// Goods receipt processed
        /// </summary>
        [Description("Goods receipt processed")]
        Goods_receipt_processed = 3,
        
        /// <summary>
        /// Cancelled
        /// </summary>
        [Description("Cancelled")]
        Cancelled = 4,
    }
}
