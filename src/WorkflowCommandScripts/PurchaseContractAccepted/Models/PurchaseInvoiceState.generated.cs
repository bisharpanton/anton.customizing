namespace PurchaseContractAccepted.Models
{
    using System;
    using System.ComponentModel;
    
    
    public enum PurchaseInvoiceState : int
    {
        
        /// <summary>
        /// Waiting for Invoices
        /// </summary>
        [Description("Waiting for Invoices")]
        Waiting_for_Invoices = 1,
        
        /// <summary>
        /// Invoices received
        /// </summary>
        [Description("Invoices received")]
        Invoices_received = 2,
        
        /// <summary>
        /// Invoices processed
        /// </summary>
        [Description("Invoices processed")]
        Invoices_processed = 3,
    }
}
