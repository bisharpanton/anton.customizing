﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using Ridder.Recordset.Extensions;
using MailDeclarationOfPerformance.Models;

public class MailDeclarationOfPerformance_RidderScript : WorkflowCommandScript
{
    public void Execute()
    {
        var goodsreCeiptId = (int)RecordIds.First();

        var goodsReceipt = GetRecordset("R_GOODSRECEIPT", "", $"PK_R_GOODSRECEIPT = {goodsreCeiptId}", "")
            .As<Goodsreceipt>().First();

        var purchaseOrderId = goodsReceipt.FK_ORIGINALPURCHASEORDER as int? ?? 0;

        var wf_ShowReport = new Guid("a0a77b42-d995-4172-9fa4-04bc208e75f2");
        var dictionary = new Dictionary<string, object>();

        dictionary.Add("GoederenOntvangst", goodsreCeiptId);

        ExecuteWorkflowEvent("R_PURCHASEORDER", purchaseOrderId, wf_ShowReport, dictionary);
    }
}
