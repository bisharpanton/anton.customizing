namespace MailDeclarationOfPerformance.Models
{
    using System;
    using System.ComponentModel;
    
    
    public enum GoodsReceiptIntegrationState : int
    {
        
        /// <summary>
        /// New
        /// </summary>
        [Description("New")]
        New = 1,
        
        /// <summary>
        /// Message processed
        /// </summary>
        [Description("Message processed")]
        Message_processed = 2,
        
        /// <summary>
        /// Message not processed
        /// </summary>
        [Description("Message not processed")]
        Message_not_processed = 3,
    }
}
