

namespace MailDeclarationOfPerformance.Models
{
    using System;
    using System.ComponentModel;
    
    
    public enum GoodsReceiptState : int
    {
        
        /// <summary>
        /// New
        /// </summary>
        [Description("New")]
        New = 1,
        
        /// <summary>
        /// Goods receipt processed
        /// </summary>
        [Description("Goods receipt processed")]
        Goods_receipt_processed = 2,
        
        /// <summary>
        /// Invoice
        /// </summary>
        [Description("Invoice")]
        Invoice = 3,
        
        /// <summary>
        /// Invoices processed
        /// </summary>
        [Description("Invoices processed")]
        Invoices_processed = 4,
    }
}
