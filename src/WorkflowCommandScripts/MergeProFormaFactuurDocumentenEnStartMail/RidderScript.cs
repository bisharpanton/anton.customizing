﻿using ADODB;
using iTextSharp.text;
using iTextSharp.text.pdf;
using MergeProFormaFactuurDocumentenEnStartMail;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class MergeProFormaFactuurDocumentenEnStartMail_RidderScript : WorkflowCommandScript
{
    private readonly OutlookMail.OutlookBodyFormat _mailBodyFormat = OutlookMail.OutlookBodyFormat.HTML;

    Guid _proFormaSalesInvoiceReport = new Guid("88c451c5-b2cb-4add-873a-28b7b3430854");

    private static Guid _salesInvoiceTableInfoId = new Guid("998abc1a-481c-4143-89fb-acdfc35e1e91");

    public void Execute()
    {
        //DB
        //8-6-2023
        //Bij het mailen van de verkoopfactuur is het gewenst om de gekoppelde documenten te mergen tot één factuur. Daar is iTextSharp voor nodig.
        //In dit script mergen we de documenten en starten een mail

        //Deze code is bijna gelijk aan MergeProFormaFactuurDocumentenEnStartMail

        if (!RecordIds.Any())
        {
            throw new Exception("Verkoopfactuur kan niet bepaald worden.");
        }

        var salesInvoiceId = (int)RecordIds.First();

        var salesInvoice = GetRecordset("R_SALESINVOICE", "FK_JOURNALENTRY, FK_REPORT, MANDAYREGISTERFROM, MANDAYREGISTERUNTILL, FK_WORKFLOWSTATE, FK_LANGUAGE, FK_ALTERNATIVEREPORTLAYOUT",
            $"PK_R_SALESINVOICE = {salesInvoiceId}", "").DataTable.AsEnumerable().First();

        var journalEntryId = salesInvoice.Field<int?>("FK_JOURNALENTRY") ?? 0;

        var locatieDigitaleFacturatie = GetRecordset("R_CRMSETTINGS", "LOCATIETIJDELIJKDIGITALEFACTURATIE", "", "")
            .DataTable.AsEnumerable().First().Field<string>("LOCATIETIJDELIJKDIGITALEFACTURATIE");

        if (string.IsNullOrEmpty(locatieDigitaleFacturatie))
        {
            throw new Exception("Geen locatie Digitale facturatie gevonden in de CRM instellingen.");
        }

        if (journalEntryId == 0)
        {
            /*
            Guid wfGenereerFactuurNr = new Guid("b3fc040a-d468-4e08-9c7e-d33833848824");
            var result = ExecuteWorkflowEvent("R_SALESINVOICE", salesInvoiceId, wfGenereerFactuurNr, null);*/

            var result = EventsAndActions.Sales.Actions.GenerateReservedSalesJournalEntry(salesInvoiceId);

            if (result.HasError)
            {
                throw new Exception($"Het factuurnummer is niet te genereren om de volgende reden: {result.GetResult()}");
            }

            journalEntryId = GetRecordset("R_SALESINVOICE", "FK_JOURNALENTRY",
                $"PK_R_SALESINVOICE = {salesInvoiceId}", "").DataTable.AsEnumerable().First().Field<int>("FK_JOURNALENTRY");
        }

        var journalEntryNumber = GetRecordset("R_JOURNALENTRY", "JOURNALENTRYNUMBER",
            $"PK_R_JOURNALENTRY = {journalEntryId}", "").DataTable.AsEnumerable().First().Field<int>("JOURNALENTRYNUMBER");

        //0) Controleer of map al bestaat, verwijder dan de map. 
        var directoryArchiveInvoiceDocuments = Path.Combine(locatieDigitaleFacturatie, salesInvoiceId.ToString());

        if (!Directory.Exists(directoryArchiveInvoiceDocuments))
        {
            Directory.CreateDirectory(directoryArchiveInvoiceDocuments);
        }

        Cursor.Current = Cursors.WaitCursor;
        try
        {
            var documentsToMerge = new List<string>();
            var databaseDocumentsCreated = new List<string>();

            var fileNameSalesInvoice = ExportSalesInvoice(salesInvoice, directoryArchiveInvoiceDocuments);
            documentsToMerge.Add(fileNameSalesInvoice);
            databaseDocumentsCreated.Add(fileNameSalesInvoice);

            if (MandayRegisterApplicable(salesInvoice))
            {
                var fileNameMandayRegister = ExportMandayRegister(salesInvoice, directoryArchiveInvoiceDocuments);
                documentsToMerge.Add(fileNameMandayRegister);
                databaseDocumentsCreated.Add(fileNameMandayRegister);
            }

            var outputFileName = $"Pro forma factuur {journalEntryNumber}";

            var documents = ArchiveLinkedDocuments(salesInvoiceId, directoryArchiveInvoiceDocuments, outputFileName, ref databaseDocumentsCreated);

            documentsToMerge.AddRange(documents);

            var totalFileNameOutputDocument = Path.Combine(directoryArchiveInvoiceDocuments, $"{outputFileName}.pdf");
            Merge(documentsToMerge, totalFileNameOutputDocument);

            SaveDocumentInRidder(salesInvoiceId, totalFileNameOutputDocument, outputFileName);

            DeleteDatabaseDocumentsCreated(databaseDocumentsCreated);

            StartMail(salesInvoiceId, salesInvoice.Field<int>("FK_LANGUAGE"), totalFileNameOutputDocument, salesInvoice.Field<int?>("FK_ALTERNATIVEREPORTLAYOUT"));

            SetDateProformaSend(salesInvoiceId);
        }
        finally
        {
            Cursor.Current = Cursors.Default;
        }
    }

    private void SetDateProformaSend(int salesInvoiceId)
    {
        var rsSalesInvoice = GetRecordset("R_SALESINVOICE", "DATEPROFORMASEND", $"PK_R_SALESINVOICE = {salesInvoiceId}", "");
        rsSalesInvoice.MoveFirst();
        rsSalesInvoice.SetFieldValue("DATEPROFORMASEND", DateTime.Now);
        rsSalesInvoice.Update();
    }

    private string ExportMandayRegister(DataRow salesInvoice, string directoryArchiveInvoiceDocuments)
    {
        var reportGuidMandayRegister = new Guid("2b791886-4fd7-447a-8b71-c8a443a5e735");
        Guid reportColumnID = Guid.Empty;

        var fileName = Path.Combine(directoryArchiveInvoiceDocuments, $"Mandagenregister {salesInvoice.Field<int>("PK_R_SALESINVOICE")}.pdf");

        var par = new Ridder.Client.SDK.ExportReportParameter();
        par.Id = salesInvoice.Field<int>("PK_R_SALESINVOICE");
        par.TableName = "R_SALESINVOICE";
        par.DesignerScope = Ridder.Client.SDK.SDKParameters.DesignerScope.User;
        par.Report = reportGuidMandayRegister;
        par.ReportColumnId = Guid.Empty;
        par.FileName = fileName;
        par.ExportType = Ridder.Client.SDK.SDKParameters.ExportType.Pdf;
        par.ImagesAsJpegForPDF = false;

        var result = ExportReportWithParameters(par, false);

        if (result.HasError)
        {
            throw new Exception($"Exporteren mandagenregister is mislukt, oorzaak: {result.GetResult()}");
        }

        return fileName;
    }

    private bool MandayRegisterApplicable(DataRow salesInvoice)
    {
        var wkaActive = GetRecordset("R_CRMSETTINGS", "WKAACTIVE", "", "")
            .DataTable.AsEnumerable().First().Field<bool>("WKAACTIVE");

        if (!wkaActive)
        {
            return false;
        }

        if (!salesInvoice.Field<DateTime?>("MANDAYREGISTERFROM").HasValue || !salesInvoice.Field<DateTime?>("MANDAYREGISTERUNTILL").HasValue)
        {
            return false;
        }

        return true;
    }

    private void DeleteDatabaseDocumentsCreated(List<string> databaseDocumentsCreated)
    {
        foreach (var document in databaseDocumentsCreated)
        {
            File.Delete(document);
        }
    }

    private void SaveDocumentInRidder(int salesInvoiceId, string totalFileNameOutputDocument, string outputFileName)
    {
        var documentIds = GetRecordset("R_SALESINVOICEDOCUMENT", "FK_DOCUMENT", string.Format("FK_SALESINVOICE = {0}", salesInvoiceId), "")
            .DataTable.AsEnumerable().Select(x => x.Field<int>("FK_DOCUMENT")).ToList();

        if (documentIds.Any())
        {
            var outputDocumentExist = GetRecordset("R_DOCUMENT", "STORAGESYSTEM, DOCUMENTLOCATION, DOCUMENTDATA, DESCRIPTION",
                $"PK_R_DOCUMENT IN ({string.Join(",", documentIds)}) AND EXTENSION IN ('.pdf', '.PDF') AND DESCRIPTION = '{outputFileName}'",
                "").RecordCount > 0;

            if (outputDocumentExist)
            {
                return;
            }
        }

        EventsAndActions.CRM.Actions.AddDocument(totalFileNameOutputDocument, "0", true, "R_SALESINVOICE", salesInvoiceId, outputFileName);
    }

    private void StartMail(int salesInvoiceId, int languageId, string invoiceDocumentLocation, int? alternativeReportLayout)
    {
        /*
        var wfMailSalesInvoice = new Guid("20bbd9bb-0182-4532-a76c-9f423d8abbe7");

        var wfResult = ExecuteWorkflowEvent("R_SALESINVOICE", salesInvoiceId, wfMailSalesInvoice, null);

        if (wfResult.HasError)
        {
            throw new Exception($"Mailen verkoopfactuur is mislukt, oorzaak: {wfResult.GetResult()}");
        }*/

        var rsMailReportSettings = GetRecordset("R_MAILREPORTSETTING", "",
            $"FK_TABLEINFO = '{_salesInvoiceTableInfoId}' AND DESCRIPTION = 'Mail pro forma factuur'", "");

        if (rsMailReportSettings.RecordCount == 0)
        {
            throw new Exception("No mailsetting found on table 'Verkoopfacturen' with description 'Mail pro forma factuur'.");
        }

        rsMailReportSettings.MoveFirst();

        var rsMailReportSettingDetail = GetRecordset("R_MAILREPORTSETTINGDETAIL", "",
            $"FK_MAILREPORTSETTING = {rsMailReportSettings.GetField("PK_R_MAILREPORTSETTING").Value} AND FK_LANGUAGE = {languageId}", "");

        if (rsMailReportSettingDetail.RecordCount == 0)
        {
            throw new Exception("No mailsetting in correct language found on table 'Verkoopfacturen' with description 'Mail pro forma factuur'.");
        }

        rsMailReportSettingDetail.MoveFirst();

        var calculatedColumnsMain = new[] { "TO", "FROM", "CC", "BCC", "FILELOCATION" };
        var calculatedColumnsLanguage = new[] { "SUBJECT", "SALUTATION", "FILENAME" };

        var calculatedColumnDataMain = GetCalculatedColumnOutput(rsMailReportSettings, calculatedColumnsMain, "R_SALESINVOICE", salesInvoiceId);
        var calculatedColumnDataLanguage = GetCalculatedColumnOutput(rsMailReportSettingDetail, calculatedColumnsLanguage, "R_SALESINVOICE", salesInvoiceId);


        using (var mail = new OutlookMail
        {
            To = (string)calculatedColumnDataMain["TO"],
            Subject = (string)calculatedColumnDataLanguage["SUBJECT"]
        })
        {
            var from = calculatedColumnDataMain["FROM"] as string;
            if (!string.IsNullOrEmpty(from))
            {
                mail.SentOnBehalfOfName = from;
            }

            var cc = calculatedColumnDataMain["CC"] as string;
            if (!string.IsNullOrEmpty(cc))
            {
                mail.CC = cc;
            }

            var bcc = calculatedColumnDataMain["BCC"] as string;
            if (!string.IsNullOrEmpty(bcc))
            {
                mail.BCC = bcc;
            }

            mail.BodyFormat = _mailBodyFormat;

            var salutation = calculatedColumnDataLanguage["SALUTATION"] as string;
            var signature = rsMailReportSettings.Fields["SIGNATURE"].Value as string;
            var body = rsMailReportSettingDetail.GetField("BODY").Value.ToString();

            var companyName = GetCompanyName(salesInvoiceId, alternativeReportLayout);
            body = body.Replace("<NaamEigenBedrijf>", companyName);



            var signaturekind = (int)rsMailReportSettings.Fields["SIGNATUREKIND"].Value;

            var bodyWithSignature = mail.HTMLBody ?? "";
            var bodyTagStart = bodyWithSignature.IndexOf("<o:p>&nbsp;", StringComparison.OrdinalIgnoreCase);
            if (bodyTagStart >= 0)
            {
                var preBodyString = bodyWithSignature.Substring(0, bodyTagStart);
                var postBodyString = bodyWithSignature.Substring(bodyTagStart);
                var salutationAndBody = salutation + "<BR><BR>" + body;
                if (signaturekind == 2) // 2 = Eigen tekst
                    salutationAndBody += "<BR><BR>" + signature;

                var newBody = preBodyString + salutationAndBody + postBodyString;
                mail.HTMLBody = newBody;
            }
            else
            {
                mail.HTMLBody = salutation + "<BR><BR>" + bodyWithSignature;
            }

            // Add default report
            mail.AddAttachment(invoiceDocumentLocation);

            try
            {
                mail.Display();
            }
            catch (Exception e)
            {
                throw new Exception($"Genereren e-mail is mislukt, oorzaak: {e.Message}");
            }
        }

    }

    private string GetCompanyName(int salesInvoiceId, int? alternativeReportLayout)
    {
        return alternativeReportLayout.HasValue
            ? GetAlternativeCompanyName(alternativeReportLayout.Value)
            : GetNameOwnCompany();
    }

    public string GetAlternativeCompanyName(int alternativeReportLayoutId)
    {
        return this.GetRecordset("C_ALTERNATIVEREPORTLAYOUT", "COMPANYNAME", $"PK_C_ALTERNATIVEREPORTLAYOUT = {alternativeReportLayoutId}", "")
        .DataTable.AsEnumerable().First().Field<string>("COMPANYNAME");
    }

    public string GetNameOwnCompany()
    {
        var ownCompanyId = this.GetRecordset("R_CRMSETTINGS", "FK_OWNCOMPANY", "", "")
            .DataTable.AsEnumerable().First().Field<int>("FK_OWNCOMPANY");

        var ownCompanyName = this.GetRecordset("R_RELATION", "NAME",
            string.Format("PK_R_RELATION = {0}", ownCompanyId), "")
            .DataTable.AsEnumerable().First().Field<string>("NAME");

        if (ownCompanyName.EndsWith("B.V.") || ownCompanyName.EndsWith("b.v."))
        {
            ownCompanyName = ownCompanyName.Substring(0, ownCompanyName.Length - 4);
        }
        else if (ownCompanyName.EndsWith("BV") || ownCompanyName.EndsWith("bv"))
        {
            ownCompanyName = ownCompanyName.Substring(0, ownCompanyName.Length - 2);
        }

        return ownCompanyName.TrimEnd();
    }

    private List<string> ArchiveLinkedDocuments(int salesInvoiceId, string directoryArchiveInvoiceDocuments, string outputFileName, ref List<string> databaseDocumentsCreated)
    {
        var result = new List<string>();

        var documentIds = GetRecordset("R_SALESINVOICEDOCUMENT", "FK_DOCUMENT", string.Format("FK_SALESINVOICE = {0}", salesInvoiceId), "")
            .DataTable.AsEnumerable().Select(x => x.Field<int>("FK_DOCUMENT")).ToList();

        if (!documentIds.Any())
        {
            return new List<string>();
        }

        var documents = GetRecordset("R_DOCUMENT", "STORAGESYSTEM, DOCUMENTLOCATION, DOCUMENTDATA, DESCRIPTION",
            $"PK_R_DOCUMENT IN ({string.Join(",", documentIds)}) AND EXTENSION IN ('.pdf', '.PDF') AND DESCRIPTION <> '{outputFileName}'",
            "").DataTable.AsEnumerable().ToList();

        foreach (var document in documents)
        {
            if (document.Field<string>("STORAGESYSTEM").Equals("Verwijzing"))  //Document is als verwijzing opgeslagen
            {
                result.Add(document.Field<string>("DOCUMENTLOCATION"));
            }
            else  //Document is in de database opgeslagen
            {
                var documentData = document.Field<byte[]>("DOCUMENTDATA");

                if (documentData != null)
                {
                    var totalFileName = Path.Combine(directoryArchiveInvoiceDocuments, $"{document.Field<string>("DESCRIPTION")}.pdf");

                    try
                    {
                        File.WriteAllBytes(totalFileName, documentData);
                    }
                    catch (Exception e)
                    {
                        throw new Exception($"Wegschrijven documentdata om document te creëren is mislukt, oorzaak: {e}");
                    }

                    databaseDocumentsCreated.Add(totalFileName);
                    result.Add(totalFileName);
                }
            }
        }

        return result;
    }

    private string ExportSalesInvoice(DataRow salesInvoice, string directoryArchiveInvoiceDocuments)
    {
        Guid reportColumnID = Guid.Empty;

        var fileName = Path.Combine(directoryArchiveInvoiceDocuments, $"FactuurLos {salesInvoice.Field<int>("PK_R_SALESINVOICE")}.pdf");

        var par = new Ridder.Client.SDK.ExportReportParameter();
        par.Id = salesInvoice.Field<int>("PK_R_SALESINVOICE");
        par.TableName = "R_SALESINVOICE";
        par.DesignerScope = Ridder.Client.SDK.SDKParameters.DesignerScope.User;
        par.Report = _proFormaSalesInvoiceReport;
        par.ReportColumnId = Guid.Empty;
        par.FileName = fileName;
        par.ExportType = Ridder.Client.SDK.SDKParameters.ExportType.Pdf;
        par.ImagesAsJpegForPDF = false;

        var result = ExportReportWithParameters(par, false);

        if (result.HasError)
        {
            throw new Exception($"Exporteren verkoopfactuur is mislukt, oorzaak: {result.GetResult()}");
        }

        return fileName;
    }

    public void Merge(List<string> InFiles, String OutFile)
    {
        using (FileStream stream = new FileStream(OutFile, FileMode.Create))
        using (Document doc = new Document())
        using (PdfCopy pdf = new PdfCopy(doc, stream))
        {
            doc.Open();

            PdfReader reader = null;
            PdfImportedPage page = null;

            foreach (var file in InFiles)
            {
                reader = new PdfReader(file);

                for (int i = 0; i < reader.NumberOfPages; i++)
                {
                    page = pdf.GetImportedPage(reader, i + 1);
                    pdf.AddPage(page);
                }

                pdf.FreeReader(reader);
                reader.Close();
            }
        }
    }

    private IDictionary<string, object> GetCalculatedColumnOutput(ScriptRecordset recordset, string[] fields,
        string tableName, object recordId)
    {
        var result = new Dictionary<string, object>();
        foreach (var field in fields)
        {
            var fieldData = recordset.Fields[field].Value != DBNull.Value
                ? recordset.Fields[field].Value as byte[]
                : null;
            if (fieldData == null)
            {
                result[field] = null;
                continue;
            }

            result[field] = GetCalculatedColumnOutput(fieldData, tableName, recordId);
        }

        return result;
    }
}
