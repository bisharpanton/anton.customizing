﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MergeProFormaFactuurDocumentenEnStartMail
{
    public sealed class OutlookMail : IDisposable
    {
        private readonly object _mailItem;
        private readonly Type _mailItemType;
        private readonly object _outlookApp;
        private readonly Type _outlookAppType;
        private bool _disposed;

        public OutlookMail()
        {
            _outlookApp = Activator.CreateInstance(Type.GetTypeFromProgID("Outlook.Application"));
            _outlookAppType = _outlookApp.GetType();
            _mailItem = _outlookAppType.InvokeMember("CreateItem", BindingFlags.InvokeMethod, null, _outlookApp, new object[] { null });
            _mailItemType = _mailItem.GetType();
        }

        public string To
        {
            get
            {
                return (string)_mailItemType.InvokeMember("To", BindingFlags.GetProperty, null, _mailItem, null);
            }
            set
            {
                _mailItemType.InvokeMember("To", BindingFlags.SetProperty, null, _mailItem, new object[] { value });
            }
        }

        public string CC
        {
            get
            {
                return (string)_mailItemType.InvokeMember("CC", BindingFlags.GetProperty, null, _mailItem, null);
            }
            set
            {
                _mailItemType.InvokeMember("CC", BindingFlags.SetProperty, null, _mailItem, new object[] { value });
            }
        }

        public string BCC
        {
            get
            {
                return (string)_mailItemType.InvokeMember("BCC", BindingFlags.GetProperty, null, _mailItem, null);
            }
            set
            {
                _mailItemType.InvokeMember("BCC", BindingFlags.SetProperty, null, _mailItem, new object[] { value });
            }
        }

        public string SentOnBehalfOfName
        {
            get
            {
                return (string)_mailItemType.InvokeMember("SentOnBehalfOfName", BindingFlags.GetProperty, null, _mailItem, null);
            }
            set
            {
                _mailItemType.InvokeMember("SentOnBehalfOfName", BindingFlags.SetProperty, null, _mailItem, new object[] { value });
            }
        }

        public string Subject
        {
            get
            {
                return (string)_mailItemType.InvokeMember("Subject", BindingFlags.GetProperty, null, _mailItem, null);
            }
            set
            {
                _mailItemType.InvokeMember("Subject", BindingFlags.SetProperty, null, _mailItem, new object[] { value });
            }
        }

        public string Body
        {
            get
            {
                // http://geekswithblogs.net/QuandaryPhase/archive/2009/08/07/outlook-automation-without-com-references.aspx
                // Outlook only fills the body with a signature when the 'GetInspector' is accessed.
                _mailItemType.InvokeMember("GetInspector", BindingFlags.GetProperty, null, _mailItem, null);

                return (string)_mailItemType.InvokeMember("Body", BindingFlags.GetProperty, null, _mailItem, null);
            }
            set
            {
                // http://geekswithblogs.net/QuandaryPhase/archive/2009/08/07/outlook-automation-without-com-references.aspx
                // Outlook only fills the body with a signature when the 'GetInspector' is accessed.
                _mailItemType.InvokeMember("GetInspector", BindingFlags.GetProperty, null, _mailItem, null);

                _mailItemType.InvokeMember("Body", BindingFlags.SetProperty, null, _mailItem, new object[] { value });
            }
        }

        public string HTMLBody
        {
            get
            {
                // http://geekswithblogs.net/QuandaryPhase/archive/2009/08/07/outlook-automation-without-com-references.aspx
                // Outlook only fills the body with a signature when the 'GetInspector' is accessed.
                _mailItemType.InvokeMember("GetInspector", BindingFlags.GetProperty, null, _mailItem, null);

                return (string)_mailItemType.InvokeMember("HTMLBody", BindingFlags.GetProperty, null, _mailItem, null);
            }
            set
            {
                // http://geekswithblogs.net/QuandaryPhase/archive/2009/08/07/outlook-automation-without-com-references.aspx
                // Outlook only fills the body with a signature when the 'GetInspector' is accessed.
                _mailItemType.InvokeMember("GetInspector", BindingFlags.GetProperty, null, _mailItem, null);

                _mailItemType.InvokeMember("HTMLBody", BindingFlags.SetProperty, null, _mailItem, new object[] { value });
            }
        }

        public bool NoAging
        {
            get
            {
                return (bool)_mailItemType.InvokeMember("NoAging", BindingFlags.GetProperty, null, _mailItem, null);
            }
            set
            {
                _mailItemType.InvokeMember("NoAging", BindingFlags.SetProperty, null, _mailItem, new object[] { value });
            }
        }

        public OutlookBodyFormat BodyFormat
        {
            get
            {
                return (OutlookBodyFormat)(int)_mailItemType.InvokeMember("BodyFormat", BindingFlags.GetProperty, null, _mailItem, null);
            }
            set
            {
                _mailItemType.InvokeMember("BodyFormat", BindingFlags.SetProperty, null, _mailItem, new object[] { (int)value });
            }
        }

        public string Categories
        {
            get
            {
                return (string)_mailItemType.InvokeMember("Categories", BindingFlags.GetProperty, null, _mailItem, null);
            }
            set
            {
                _mailItemType.InvokeMember("Categories", BindingFlags.SetProperty, null, _mailItem, new object[] { value });
            }
        }

        public DateTime DeferredDeliveryTime
        {
            get
            {
                return (DateTime)_mailItemType.InvokeMember("DeferredDeliveryTime", BindingFlags.GetProperty, null, _mailItem, null);
            }
            set
            {
                _mailItemType.InvokeMember("DeferredDeliveryTime", BindingFlags.SetProperty, null, _mailItem, new object[] { value });
            }
        }

        public void AddAttachment(string file)
        {
            var mailItemAttachments = _mailItemType.InvokeMember("Attachments", BindingFlags.GetProperty, null, _mailItem, new object[] { });
            mailItemAttachments.GetType().InvokeMember("Add", BindingFlags.InvokeMethod, null, mailItemAttachments, new object[] { file, 1, 1, Path.GetFileName(file) });
        }

        public void Send()
        {
            _mailItemType.InvokeMember("Send", BindingFlags.InvokeMethod, null, _mailItem, new object[] { });
        }

        public void Display(bool modal = false)
        {
            _mailItemType.InvokeMember("Display", BindingFlags.InvokeMethod, null, _mailItem, new object[] { modal });
        }

        public void SaveAsDraft()
        {
            _mailItemType.InvokeMember("Save", BindingFlags.InvokeMethod, null, _mailItem, new object[] { });
        }

        public void Dispose()
        {
            if (_disposed)
                return;

            _disposed = true;

            var mailItem = _mailItem;
            if (mailItem != null)
            {
                Marshal.ReleaseComObject(mailItem);
            }

            var outlookApp = _outlookApp;
            if (outlookApp != null)
            {
                Marshal.ReleaseComObject(outlookApp);
            }
        }

        public enum OutlookBodyFormat
        {
            Unspecified,
            Plain,
            HTML,
            RichText,
        }
    }
}
