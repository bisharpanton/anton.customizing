﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

public class MailPerformanceOfDeclarationFromGoodsreceipt_RidderScript : WorkflowCommandScript
{
	public void Execute()
	{
		// HVE
		// 4-5-2021
		// Mail Performance of declaration

		var purchaseId = (int)RecordIds.First();

        var goodsReceiptId = WaitAndSearchForGoodsReceipt(purchaseId);

		if (goodsReceiptId == 0)
        {
            throw new Exception("Geen goederenontvangst gevonden");
			MessageBox.Show("Geen goederenontvangst gevonden", "Ridder iQ", MessageBoxButtons.OK,
				MessageBoxIcon.Information);
			return;
		}

		var wf_MailPerformanceOfDelcaration = new Guid("2af8ccc1-33c9-40db-880f-1839fe0e2a0a");
		var result = ExecuteWorkflowEvent("R_GOODSRECEIPT", goodsReceiptId, wf_MailPerformanceOfDelcaration, null);
	}

    private int WaitAndSearchForGoodsReceipt(int purchaseId)
    {
        var timeOutInSeconds = 15;
        
        for (int i = 0; i < timeOutInSeconds; i++)
        {
            var rsGoodsReceipt = GetRecordset("R_GOODSRECEIPT", "PK_R_GOODSRECEIPT", $"FK_ORIGINALPURCHASEORDER = {purchaseId}", "");

            if(rsGoodsReceipt.RecordCount > 0)
            {
                return rsGoodsReceipt.DataTable.AsEnumerable().First().Field<int>("PK_R_GOODSRECEIPT");
            }

            Thread.Sleep(1000); //Wacht 1 seconden
        }

        return 0;
    }
}
