﻿using Ridder.Client.SDK.SDKParameters;

namespace OpenRevisedPurchaseContract.Models
{
    using ADODB;
    using Ridder.Common.ADO;
    using Ridder.Common.Choices;
    using Ridder.Common.Script;
    using Ridder.Common.Search;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Windows.Forms;
    using System.Xml;
    using Models;
    using Ridder.Recordset.Extensions;

    public class OpenRevisedPurchaseContract_RidderScript : WorkflowCommandScript
    {
        public void Execute()
        {
            // HVE
            // 6-4-2021
            // Revise purchase contract
            var originalPurchaseContractId = (int)RecordIds.First();
            var originalRecordTag = GetRecordTag("C_PURCHASECONTRACT", originalPurchaseContractId);
            var originalPurchaseContract = GetRecordset("C_PURCHASECONTRACT", "",
                    $"PK_C_PURCHASECONTRACT = {originalPurchaseContractId}", "")
                .As<PurchaseContract>()
                .First();

            originalPurchaseContract.REVISION = DetermineRevisionnumber(originalPurchaseContract.CONTRACTNUMBER);

            var rsNewPurchaseContract = GetRecordset("C_PURCHASECONTRACT", "",
                $"PK_C_PURCHASECONTRACT IS NULL", "");
            rsNewPurchaseContract.AddNew();
            rsNewPurchaseContract.Fill(originalPurchaseContract);
            var updateResult = rsNewPurchaseContract.Update2();
            if (updateResult != null && updateResult.Any(x => x.HasError))
            {
                MessageBox.Show(
                    $"Reviseren van {originalRecordTag} mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}",
                    "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var purchaseContractNew = rsNewPurchaseContract
                .As<PurchaseContract>()
                .First();

            var originalTexts = GetRecordset("C_CONTRACTTEXT", "",
                    $"FK_PURCHASECONTRACT = {originalPurchaseContractId}", "")
                .As<Text>()
                .ToList();
            if (originalTexts.Any())
            {
                ReviseContractText(originalTexts, purchaseContractNew.PK_C_PURCHASECONTRACT);
            }

            var docs = GetRecordset("C_PURCHASECONTRACTDOCUMENT", "",
                    $"FK_PURCHASECONTRACT = {originalPurchaseContractId}", "")
                .As<Doc>()
                .ToList();
            if (docs.Any())
            {
                ReviseDocuments(docs, purchaseContractNew.PK_C_PURCHASECONTRACT);
            }

            var editPar = new EditParameters()
            {
                TableName = "C_PURCHASECONTRACT", 
                Id = purchaseContractNew.PK_C_PURCHASECONTRACT,
            };

            OpenEdit(editPar, false);
        }

        private void ReviseDocuments(List<Doc> docs, int purchaseContractNewId)
        {
            var rsNewDocuments = GetRecordset("C_PURCHASECONTRACTDOCUMENT", "",
                $"PK_C_PURCHASECONTRACTDOCUMENT IS NULL", "");
            rsNewDocuments.UpdateWhenMoveRecord = false;

            foreach (var doc in docs)
            {
                doc.FK_PURCHASECONTRACT = purchaseContractNewId;
                rsNewDocuments.AddNew();
                rsNewDocuments.Fill(doc);
            }

            rsNewDocuments.MoveFirst();
            rsNewDocuments.Update();
        }

        private void ReviseContractText(List<Text> originalTexts, int purchaseContractNewId)
        {
            var rsNewText = GetRecordset("C_CONTRACTTEXT", "",
                $"PK_C_CONTRACTTEXT IS NULL", "");
            rsNewText.UpdateWhenMoveRecord = false;

            foreach (var text in originalTexts)
            {
                text.FK_PURCHASECONTRACT = purchaseContractNewId;
                rsNewText.AddNew();
                rsNewText.Fill(text);
            }

            rsNewText.MoveFirst();
            rsNewText.Update();
        }

        private int? DetermineRevisionnumber(string contractnumber)
        {
            var revision = GetRecordset("C_PURCHASECONTRACT", "",
                    $"CONTRACTNUMBER = '{contractnumber}'", "")
                .As<PurchaseContract>()
                .Max(x => x.REVISION);

            return revision + 1;
        }
    }
}

