﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class CreateContractText_RidderScript : WorkflowCommandScript
{
    public void Execute()
    {
        Guid tableInfoId = new Guid("65c3f1bd-5c93-492f-b299-43d3cf25bc44");

        if (TableName == null || TableName != "C_PURCHASECONTRACT")
        {
            MessageBox.Show("Dit script werkt alleen op de tabel 'Inkoopcontracten'.", "Ridder iQ", MessageBoxButtons.OK,
                MessageBoxIcon.Error);
            return;
        }

        if (RecordIds == null || !RecordIds.Any())
        {
            return;
        }

        var contractId = RecordIds[0];

        var rsContract = GetRecordset("C_PURCHASECONTRACT", "FK_CONTRACTTYPE, FK_SUPPLIER",
            $"PK_C_PURCHASECONTRACT = {contractId}", "").DataTable.AsEnumerable().First();

        var contractType = rsContract.Field<int>("FK_CONTRACTTYPE");
        var supplierId = rsContract.Field<int>("FK_SUPPLIER");

        var languageId = DetermineLanguageId(supplierId);

        var rsDefaultText = GetRecordset("C_CONTRACTDEFAULTTEXT", "", $"FK_CONTRACTTYPE = {contractType}", "");
        if (rsDefaultText.RecordCount == 0)
        {
            return;
        }

        var rsContractText = GetRecordset("C_CONTRACTTEXT", "", $"FK_PURCHASECONTRACT = {contractId}",
            "FK_REF_CONTRACTTEXT DESC");
        rsContractText.UseDataChanges = false;

        //Bepaal WordPluginVariabelen
        ScriptRecordset rsReportTextVariables = GetRecordset("U_REPORTTEXTVARIABLES", "",
            $"FK_TABLEINFO = '{tableInfoId}'", "");

        if (rsReportTextVariables.RecordCount == 0)
        {
            MessageBox.Show($"Geen rapport tekst variabelen voor tabel {TableName}.",
                "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        List<WordPluginVariable> listWordPluginVariables = new List<WordPluginVariable>();
        rsReportTextVariables.MoveFirst();

        while (!rsReportTextVariables.EOF)
        {
            string textToReplace = (string)rsReportTextVariables.Fields["TEXTTOREPLACE"].Value;

            if (textToReplace == "RIQ_TabelBetaalschema") //Dit is een uitzondering, hier wordt een tabel ingevoegd
            {
                listWordPluginVariables.Add(new WordPluginVariable() { TextToReplace = textToReplace, Value = null, });

                rsReportTextVariables.MoveNext();
                continue;
            }

            string valueVariable = "";
            try
            {
                if ((bool)rsReportTextVariables.Fields["DATEFIELD"].Value)
                {
                    DateTime outputCalcColumn = (DateTime)GetCalculatedColumnOutput(
                        (Byte[])rsReportTextVariables.Fields["VALUE"].Value, TableName, contractId);
                    valueVariable = $"{outputCalcColumn:dd-MM-yyyy}";
                }
                else if ((bool)rsReportTextVariables.Fields["MONEYFIELD"].Value)
                {
                    var outputCalcColumn = GetCalculatedColumnOutput((Byte[])rsReportTextVariables.Fields["VALUE"].Value,
                        TableName, contractId).ToString();

                    var amount = Convert.ToDouble(outputCalcColumn);
                    valueVariable = string.Format(new System.Globalization.CultureInfo("nl-NL"), "{0:C0}", amount);
                }
                else
                {
                    valueVariable = GetCalculatedColumnOutput((Byte[])rsReportTextVariables.Fields["VALUE"].Value,
                        TableName, contractId).ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    $"Uitrekenen waarde {textToReplace} is mislukt; controleer de Rapportinstellingen.",
                    "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            WordPluginVariable wpv = new WordPluginVariable { TextToReplace = textToReplace, Value = valueVariable };

            listWordPluginVariables.Add(wpv);

            rsReportTextVariables.MoveNext();
        }

        Guid purchaseContractTableId = new Guid("65c3f1bd-5c93-492f-b299-43d3cf25bc44");

        var rsDefaultChoiceText = GetRecordset("C_CONTRACTDEFAULTTEXT", "FK_CHOICEVALUE IS NOT NULL", $"FK_CONTRACTTYPE = {contractType}", "");

        List<int> listDefaultTextIdsToImport = new List<int>();

        var choiceColumnNames = GetRecordset("M_COLUMNINFO", "COLUMNNAME",
            $"FK_TABLEINFO = '{purchaseContractTableId}' AND FK_CHOICETYPE IS NOT NULL", "").DataTable.AsEnumerable().Select(x => x.Field<string>("COLUMNNAME")).ToList();

        foreach (var choiceColumnName in choiceColumnNames)
        {
            var chosenChoiceValue = GetRecordset("C_PURCHASECONTRACT", $"{choiceColumnName}",
            $"PK_C_PURCHASECONTRACT = {contractId}", "").DataTable.AsEnumerable().First().Field<int>($"{choiceColumnName}");

            var choiceId = GetRecordset("M_COLUMNINFO", "FK_CHOICETYPE",
            $"FK_TABLEINFO = '{purchaseContractTableId}' AND COLUMNNAME = '{choiceColumnName}'", "").DataTable.AsEnumerable().First().Field<Guid>("FK_CHOICETYPE");

            var choiceValueId = GetRecordset("M_CHOICEVALUE", "PK_M_CHOICEVALUE, CHOICENUMBER",
            $"FK_CHOICETYPE = '{choiceId}'", "").DataTable.AsEnumerable().Where(y => (y.Field<int>("CHOICENUMBER") == chosenChoiceValue)).First().Field<Guid>("PK_M_CHOICEVALUE");

            var defaultTextWithChoiceIds = rsDefaultChoiceText.DataTable.AsEnumerable().
                Where(y => y.Field<Guid?>("FK_CHOICEVALUE").HasValue && y.Field<Guid>("FK_CHOICEVALUE") == choiceValueId).Select(x => x.Field<int>("PK_C_CONTRACTDEFAULTTEXT")).ToList();

            if(defaultTextWithChoiceIds.Any())
            {
                foreach(var defaultTextWithChoiceId in defaultTextWithChoiceIds)
                {
                    listDefaultTextIdsToImport.Add(defaultTextWithChoiceId);
                }
            }  

        }

        var rsFilteredDefaultText = GetRecordset("C_CONTRACTDEFAULTTEXT", "",
            $"FK_CONTRACTTYPE = {contractType} AND (FK_CHOICEVALUE IS NULL OR PK_C_CONTRACTDEFAULTTEXT IN ({string.Join(", ", listDefaultTextIdsToImport)}))", "");

        var FilteredDefaultTexts = rsFilteredDefaultText.DataTable.AsEnumerable();

        rsFilteredDefaultText.MoveFirst();
        while (!rsFilteredDefaultText.EOF)
        {
            //var groupApplicable = rsDefaultText.Fields["FK_CONTRACTDETAULTTEXTGROUP"].Value as int? ?? 0;

            rsContractText.AddNew();
            rsContractText.SetFieldValue("FK_PURCHASECONTRACT", contractId);
            rsContractText.SetFieldValue("FK_CHAPTER", (int)rsFilteredDefaultText.Fields["FK_CONTRACTCHAPTER"].Value);

            var text = (string)rsFilteredDefaultText.Fields["PLAINTEXT_TEXT"].Value;
            /*if (languageId > 0)
            {
                var id = (int)rsDefaultText.Fields["PK_C_CONTRACTDEFAULTTEXT"].Value;
                var transText = GetTranslatableFieldValue("C_CONTRACTDEFAULTTEXT", "TEXT", languageId, id);
                if (!string.IsNullOrWhiteSpace(transText))
                {
                    text = transText;
                }
            }*/

            foreach (var variable in listWordPluginVariables)
            {
                if (variable.TextToReplace == "RIQ_TabelOfferteregels")
                {
                    //InsertTable(doc, FormDataAwareFunctions.TableName, id, taalId);  
                }
                else
                {
                    if (text.Contains($"{variable.TextToReplace}"))
                    {
                        text = text.Replace(variable.TextToReplace, variable.Value);
                    }
                }
            }

            rsContractText.SetFieldValue("MEMO", text);
            rsContractText.SetFieldValue("SEQUENCE", (int)rsFilteredDefaultText.Fields["SEQUENCE"].Value);
            rsContractText.SetFieldValue("CONTACTS", (bool)rsFilteredDefaultText.Fields["CONTACTS"].Value);
            rsContractText.SetFieldValue("FINANCIAL", (bool)rsFilteredDefaultText.Fields["FINANCIAL"].Value);

            var chapterNumber = GetRecordset("C_CONTRACTCHAPTER", "LINENUMBER",
                $"PK_C_CONTRACTCHAPTER = {(int)rsFilteredDefaultText.Fields["FK_CONTRACTCHAPTER"].Value}", "").DataTable.AsEnumerable().First().Field<int?>("LINENUMBER") ?? 0;
            rsContractText.SetFieldValue("CHAPTERNUMBER", chapterNumber);

            var refText = rsFilteredDefaultText.Fields["FK_REF_CONTRACTTEXT"].Value as int? ?? 0;
            if (refText > 0)
            {
                var chapterId = FilteredDefaultTexts
                    .Where(y => y.Field<int>("PK_C_CONTRACTDEFAULTTEXT") == refText)
                    .Select(x => x.Field<int>("FK_CONTRACTCHAPTER"))
                    .FirstOrDefault();
                var sequence = FilteredDefaultTexts
                    .Where(y => y.Field<int>("PK_C_CONTRACTDEFAULTTEXT") == refText)
                    .Select(x => x.Field<int>("SEQUENCE"))
                    .FirstOrDefault();
                var refTextId = rsContractText
                    .DataTable
                    .AsEnumerable()
                    .Where(y => (y.Field<int>("FK_CHAPTER") == chapterId)
                                && (y.Field<int>("SEQUENCE") == sequence))
                    .Select(x => x.Field<int?>("PK_C_CONTRACTTEXT") ?? 0)
                    .FirstOrDefault();
                if (refTextId > 0)
                {
                    rsContractText.SetFieldValue("FK_REF_CONTRACTTEXT", refTextId);
                }
            }

            rsContractText.Update();
            rsFilteredDefaultText.MoveNext();
        }


    }
    private int DetermineLanguageId(int supplierId)
    {
        return GetRecordset("R_RELATION", "FK_LANGUAGE",
                $"PK_R_RELATION = {supplierId}", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int?>("FK_LANGUAGE") ?? 0)
            .FirstOrDefault();
    }
    public class WordPluginVariable
    {
        public string TextToReplace { get; set; }
        public string Value { get; set; }
    }

}
