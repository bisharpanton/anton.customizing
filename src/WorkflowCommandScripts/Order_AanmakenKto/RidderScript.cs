﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using Ridder.Client.SDK.SDKParameters;

public class Order_AanmakenKto_RidderScript : WorkflowCommandScript
{
    public void Execute()
    {
        //DB
        //1-12-2020
        //Aanmaken KTO vanuit de order

        if(!TableName.Equals("R_ORDER"))
        {
            MessageBox.Show("Dit script mag alleen vanaf de order tabel aangeroepen worden.");
            return;
        }

        if ((RecordIds?.FirstOrDefault() as int? ?? 0) == 0)
        {
            MessageBox.Show("Order id kan niet bepaald worden.");
            return;
        }

        var orderId = (int)RecordIds.First();

        var rsKto = GetRecordset("C_KLANTTEVREDENHEIDSONDERZOEK", "", "PK_C_KLANTTEVREDENHEIDSONDERZOEK = -1", "");
        rsKto.UseDataChanges = true;
        
        rsKto.AddNew();
        rsKto.SetFieldValue("FK_ORDER", orderId);
        var updateResult = rsKto.Update2();

        if (updateResult != null && updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Het aanmaken van een nieuwe KTO is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        var createdKto = (int)updateResult.First().PrimaryKey;

        var editPar = new EditParameters()
        {
            TableName = "C_KLANTTEVREDENHEIDSONDERZOEK",
            Id = createdKto,
        };

        OpenEdit(editPar, false);
    }
}
