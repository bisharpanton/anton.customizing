﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class ImportReferenceTemplate_RidderScript : WorkflowCommandScript
{
	public void Execute()
	{
        Guid tableInfoId = new Guid("abcc4e35-c70a-49e9-bd76-6c8a10c9fbb8"); //R_ORDER

        if (TableName == null || TableName != "R_ORDER")
        {
            MessageBox.Show("Dit script werkt alleen op de tabel 'Orders'.", "Ridder iQ", MessageBoxButtons.OK,
                MessageBoxIcon.Error);
            return;
        }

        if (RecordIds == null || !RecordIds.Any())
        {
            return;
        }

        var orderId = RecordIds[0];

        //Bepaal RapportVariabelen
        ScriptRecordset rsReportTextVariables = GetRecordset("U_REPORTTEXTVARIABLES", "",
            $"FK_TABLEINFO = '{tableInfoId}'", "");

        if (rsReportTextVariables.RecordCount == 0)
        {
            MessageBox.Show($"Geen rapport tekst variabelen voor tabel {TableName}.",
                "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        List<ReportTextVariable> listReportTextVariables = GetReportTextVariables(rsReportTextVariables, orderId);

        ImportTemplateToOrder(orderId, listReportTextVariables);
    }

    private void ImportTemplateToOrder(object orderId, List<ReportTextVariable> listReportTextVariables)
    {
        int referenceTemplateId = CreatedReferenceTemplate(orderId);

        var rsReferenceStatementDetails = GetRecordset("U_REFERENCESTATEMENTDETAILS", "", $"PK_U_REFERENCESTATEMENTDETAILS = -1", "");
        rsReferenceStatementDetails.UpdateWhenMoveRecord = false;
        rsReferenceStatementDetails.UseDataChanges = false;

        var rsReferenceTemplate = GetRecordset("U_REFERENCETEMPLATE", "",
            $"", "");

        rsReferenceTemplate.MoveFirst();

        while (!rsReferenceTemplate.EOF)
        {
            rsReferenceStatementDetails.AddNew();
            rsReferenceStatementDetails.SetFieldValue("FK_REFERENCESTATEMENT", referenceTemplateId);
            rsReferenceStatementDetails.SetFieldValue("FK_CHAPTER", (int)rsReferenceTemplate.Fields["FK_CHAPTER"].Value);

            var text = (string)rsReferenceTemplate.Fields["PLAINTEXT_DEFAULTVALUE"].Value;

            foreach (var variable in listReportTextVariables)
            {
                if (text.Contains($"{variable.TextToReplace}"))
                {
                    text = text.Replace(variable.TextToReplace, variable.Value);
                }
            }

            rsReferenceStatementDetails.SetFieldValue("VALUE", text);
            rsReferenceStatementDetails.SetFieldValue("SEQUENCENUMBER", (float)rsReferenceTemplate.Fields["SEQUENCENUMBER"].Value);
            rsReferenceStatementDetails.SetFieldValue("FK_CHAPTER", (int)rsReferenceTemplate.Fields["FK_CHAPTER"].Value);
            rsReferenceStatementDetails.SetFieldValue("DESCRIPTION", (string)rsReferenceTemplate.Fields["DESCRIPTION"].Value);

            rsReferenceStatementDetails.Update();
            rsReferenceTemplate.MoveNext();
        }
    }

    private int CreatedReferenceTemplate(object orderId)
    {
        var order = GetRecordset("R_ORDER", "FK_RELATION, FK_CONTACT",
            $"PK_R_ORDER = {orderId}", "").DataTable.AsEnumerable();
            
        var relationId = order.First().Field<int?>("FK_RELATION") ?? 0;
        var contactId = order.First().Field<int?>("FK_CONTACT") ?? 0;

        var currentEmployeeId = GetRecordset("R_USER", "FK_EMPLOYEE",
            $"PK_R_USER = {CurrentUserId}", "").DataTable.AsEnumerable().First().Field<int?>("FK_EMPLOYEE") ?? 0;

        var rsReferenceStatement = GetRecordset("U_REFERENCESTATEMENT", "", $"PK_U_REFERENCESTATEMENT = -1", "");
        rsReferenceStatement.UpdateWhenMoveRecord = false;
        rsReferenceStatement.UseDataChanges = false;

        rsReferenceStatement.AddNew();
        rsReferenceStatement.SetFieldValue("FK_ORDER", orderId);
        rsReferenceStatement.SetFieldValue("FK_RELATION", relationId);
        rsReferenceStatement.SetFieldValue("FK_EMPLOYEE", currentEmployeeId);

        if(contactId > 0)
        {
            rsReferenceStatement.SetFieldValue("FK_CONTACT", contactId);
        }

        rsReferenceStatement.Update();

        return (int)rsReferenceStatement.GetField("PK_U_REFERENCESTATEMENT").Value;
    }

    private List<ReportTextVariable> GetReportTextVariables(ScriptRecordset rsReportTextVariables, object orderId)
    {
        List<ReportTextVariable> listReportTextVariables = new List<ReportTextVariable>();

        rsReportTextVariables.MoveFirst();

        while (!rsReportTextVariables.EOF)
        {
            string textToReplace = (string)rsReportTextVariables.Fields["TEXTTOREPLACE"].Value;

            string valueVariable = "";
            try
            {
                if ((bool)rsReportTextVariables.Fields["DATEFIELD"].Value)
                {
                    DateTime outputCalcColumn = (DateTime)GetCalculatedColumnOutput(
                        (Byte[])rsReportTextVariables.Fields["VALUE"].Value, TableName, orderId);
                    valueVariable = $"{outputCalcColumn:dd-MM-yyyy}";
                }
                else if ((bool)rsReportTextVariables.Fields["MONEYFIELD"].Value)
                {
                    var outputCalcColumn = GetCalculatedColumnOutput((Byte[])rsReportTextVariables.Fields["VALUE"].Value,
                        TableName, orderId).ToString();

                    var amount = Convert.ToDouble(outputCalcColumn);
                    valueVariable = string.Format(new System.Globalization.CultureInfo("nl-NL"), "{0:C0}", amount);
                }
                else
                {
                    valueVariable = GetCalculatedColumnOutput((Byte[])rsReportTextVariables.Fields["VALUE"].Value,
                        TableName, orderId).ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    $"Uitrekenen waarde {textToReplace} is mislukt; controleer de Rapportinstellingen.",
                    "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            ReportTextVariable rtv = new ReportTextVariable { TextToReplace = textToReplace, Value = valueVariable };

            listReportTextVariables.Add(rtv);

            rsReportTextVariables.MoveNext();
        }

        return listReportTextVariables;
    }

    public class ReportTextVariable
    {
        public string TextToReplace { get; set; }
        public string Value { get; set; }
    }
}
