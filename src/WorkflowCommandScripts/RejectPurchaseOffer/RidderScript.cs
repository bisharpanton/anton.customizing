﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using RejectPurchaseOffer.Models;
using Ridder.Client.SDK;
using Ridder.Client.SDK.SDKParameters;
using Ridder.Recordset.Extensions;

public class RejectPurchaseOffer_RidderScript : WorkflowCommandScript
{
    public void Execute()
    {
        // HVE
        // 14-12-2021
        // Reject purchaseoffer supplier. 
        var purchaseOfferRelationId = (int)RecordIds.First();
        var rsPurchaseOfferRelation = GetRecordset("R_PURCHASEOFFERRELATION", "",
            $"PK_R_PURCHASEOFFERRELATION = {purchaseOfferRelationId}", "");
        var purchaseOfferRelation = rsPurchaseOfferRelation
            .As<PurchaseOfferRelation>().FirstOrDefault();

        var purchaseOfferId = purchaseOfferRelation.FK_PURCHASEOFFER;
        var purchaseOffer = GetRecordset("R_PURCHASEOFFER", "", $"PK_R_PURCHASEOFFER = {purchaseOfferId}", "")
            .As<PurchaseOffer>().FirstOrDefault();

        var offerId = purchaseOffer.FK_OFFER ?? 0;
        if (offerId > 0)
        {
            var salesOffer = GetRecordset("R_OFFER", "", $"PK_R_OFFER = {offerId}", "")
                .As<SalesOffer>()
                .FirstOrDefault();
            var rejectionId = salesOffer.FK_REJECTIONCODE ?? 0;

            rsPurchaseOfferRelation.MoveFirst();
            rsPurchaseOfferRelation.SetFieldValue("FK_REJECTIONCODE", rejectionId);
            rsPurchaseOfferRelation.Update();
        }
   
        var editPar = new EditParameters()
        {
            TableName = "R_PURCHASEOFFERRELATION",
            Id = purchaseOfferRelation.PK_R_PURCHASEOFFERRELATION,
            Context = "SETREASONANDLOSTTO",
        };
        OpenEdit(editPar, true);

        // TODO: 
        // Workflow met mailsjabloon starten
        // in mailsjabloon tekst afhankelijk maken van de waardes in PurchaseOfferRelation
        // - reden afwijzing
        // - toon reden afwijzing
        // - toon verloren aan

    }
}
