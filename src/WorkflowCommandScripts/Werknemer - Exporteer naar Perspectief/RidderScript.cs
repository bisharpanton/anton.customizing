﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Drawing;
using System.IO;
using System.Xml.Schema;
using System.Xml.Serialization;
using Werknemer___Exporteer_naar_Perspectief.Models;
using WinSCP;

public class Werknemer___Exporteer_naar_Perspectief_RidderScript : WorkflowCommandScript
{
    public void Execute()
    {
        //DB
        //26-11-2019
        //We exporteren de huidige werknemergegevens in een XML naar een SFTP-server die Perspectief uitleest

        if (TableName == null || TableName != "R_EMPLOYEE")
        {
            MessageBox.Show("Dit script mag alleen vanaf de werknemers-tabel aangeroepen worden.");
            return;
        }

        if (RecordIds?[0] == null)
        {
            MessageBox.Show("Record kan niet bepaald worden.");
            return;
        }

        var employeeId = (int)RecordIds[0];
        var companyInfo = GetOwnCompanyInfo();

        if (string.IsNullOrEmpty(companyInfo.IdentificationCodePerspectief))
        {
            throw new Exception("Geen Identificatiecode Camas gevonden in de CRM-instellingen.");
        }

        var rootExport = GetEmployeeInfo(companyInfo, employeeId);

        var fileName = CreateXML(companyInfo, employeeId, rootExport);

        if (fileName == "" || !File.Exists(fileName))
        {
            return;
        }

        UploadFileToSFTP(fileName, rootExport);

        SaveExportDateInEmployee(employeeId);
    }

    private void SaveExportDateInEmployee(int employeeId)
    {
        var rsEmployee = GetRecordset("R_EMPLOYEE", "DATELASTEXPORTCAMAS",
            $"PK_R_EMPLOYEE = {employeeId}", "");
        rsEmployee.MoveFirst();
        rsEmployee.Fields["DATELASTEXPORTCAMAS"].Value = DateTime.Now;
        rsEmployee.Update();
    }

    private void UploadFileToSFTP(string fileName, RootExport rootExport)
    {
        SessionOptions sessionOptions = new SessionOptions
        {
            Protocol = Protocol.Sftp,
            HostName = @"traffic.camasit.nl",
            UserName = "RidderIQ",
            Password = "TDTgEr9Z",
            SshHostKeyFingerprint = "ssh-rsa 2048 U2zLlMIU2RPWiuIPoXPYjxBvDeH4yV7V445KGclKBfc=",
        };

        //SshHostKeyFingerprint = "ssh-rsa 2048 xxxxxxxxxxx...="
        using (Session session = new Session())
        {
            // Connect
            session.Open(sessionOptions);
 
            // Upload files
            TransferOptions transferOptions = new TransferOptions();
            transferOptions.TransferMode = TransferMode.Binary;
 
            TransferOperationResult transferResult;
            transferResult =
                session.PutFiles(fileName, session.HomePath + "/", false, transferOptions);
 
            // Throw on any error
            transferResult.Check();
 
            // Print results
            foreach (TransferEventArgs transfer in transferResult.Transfers)
            {
                /*var employeeName = $"{rootExport.Employer.Employee.Initials} {rootExport.Employer.Employee.LastName}";
                MessageBox.Show($"Upload van werknemersbestand {employeeName} is gelukt.", "Ridder iQ",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);*/
            }
        }
    }

    private string CreateXML(CompanyInfo companyInfo, int employeeId, RootExport rootExport)
    {
        if (companyInfo.TempExportLocationPerspectief == "")
        {
            MessageBox.Show("Geen export locatie ingesteld in de CRM-instellingen.", "Ridder iQ",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            return "";
        }

        //Delete older files
        var filesOlderThanOneWeek = Directory.GetFiles(companyInfo.TempExportLocationPerspectief)
            .Where(x => File.GetCreationTime(x) < DateTime.Now.AddDays(-7));

        foreach (var file in filesOlderThanOneWeek)
        {
            File.Delete(file);
        }

        var fileName =
            $"{companyInfo.TempExportLocationPerspectief}\\{companyInfo.Abbreviation}_{employeeId}_{DateTime.Now:yyyyMMdd HH.mm.ss}.xml";

        //using (StreamWriter myWriter = new StreamWriter(fileName, false))
        using (XmlWriter myWriter = XmlWriter.Create(fileName, new XmlWriterSettings { OmitXmlDeclaration = true}))
        {
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            
            ns.Add("xsi","http://w3.org/2001/XMLSchema-instance");
            
            XmlSerializer mySerializer = new XmlSerializer(typeof(RootExport), "http://www.sivi.org/Verzuimmanagement/WerknemersGegevens/2017");
            mySerializer.Serialize(myWriter, rootExport, ns);
        }
        

        return fileName;
    }

    public RootExport GetEmployeeInfo(CompanyInfo companyInfo, int employeeId)
    {
        //Get data from iQ
        var employeeInfo = GetRecordset("R_EMPLOYEE", "",
            $"PK_R_EMPLOYEE = {employeeId}", "").DataTable.AsEnumerable().First();

        var personInfo = GetRecordset("R_PERSON", "",
            $"PK_R_PERSON = {employeeInfo.Field<int>("FK_PERSON")}", "")
            .DataTable.AsEnumerable().First();

        var employeePrivateData = GetRecordset("R_EMPLOYEEPRIVATEDATA", "",
                $"FK_EMPLOYEE = {employeeId}", "")
            .DataTable.AsEnumerable().First();

        var functionInfo = GetFunctionInfo(employeeInfo.Field<int?>("FK_POSITION"));
        var department = GetDepartmentInfo(employeeInfo.Field<int?>("FK_DEPARTMENT"));

        var addressInfo = (!employeePrivateData.Field<int?>("FK_ADDRESS").HasValue)
            ? null
            : GetRecordset("R_ADDRESS", "ZIPCODE, CITY, STREET, HOUSENUMBER2, ADDITIONHOUSENUMBER",
                    $"PK_R_ADDRESS = {employeePrivateData.Field<int>("FK_ADDRESS")}", "")
                .DataTable.AsEnumerable().First();

        var cao = (!employeeInfo.Field<int?>("FK_CAO").HasValue)
            ? ""
            : GetRecordset("C_CAO", "DESCRIPTION",
                    $"PK_C_CAO = {employeeInfo.Field<int>("FK_CAO")}", "")
                .DataTable.AsEnumerable().First().Field<string>("DESCRIPTION").ToUpper();

        //Declare classes
        var messageInfo = new MessageInfo()
        {
            MessageCode = "00600",
            VersionMessageCode = "00001",
            CreationDate = $"{DateTime.Now.Date:yyyy-MM-dd}",
            CreationTime = $"{DateTime.Now.TimeOfDay.Hours}:{DateTime.Now.TimeOfDay.Minutes}:{DateTime.Now.TimeOfDay.Seconds}",
            Sender = "RidderIQ",
            Receiver = "Perspectief",
            Reference = "",
            TestMessage = "N",
            ConfimationOfReceipt = "N",
        };

        var employment = new Employment()
        {
            ProcessingCode = "04",
            EmploymentId = cao,
            EmploymentStartDate = $"{employeeInfo.Field<DateTime?>("EMPLOYMENTSTARTDATE"):yyyy-MM-dd}",
            EmploymentEndDate = $"{employeeInfo.Field<DateTime?>("EMPLOYMENTTERMINATIONDATE"):yyyy-MM-dd}",
            ReasonTermination = "99", //Onbekend
            EmployeeNumber = employeeInfo.Field<string>("CODE"),
            EmploymentType = "01", //Normale arbeidsovereenkomst
            FunctionDescription = functionInfo.Description,
            NameOrganisationUnit = department.Description,
            OrganisationUnitCode = department.Code,
            ContractHoursPerWeek = employeeInfo.Field<double>("UURWERKWEEK"),
            PartTimeFactor = employeeInfo.Field<double>("UURWERKWEEK") / 40,
        };

        var employee = new Employee()
        {
            ProcessingCode = "02",
            //NationalIdNumber = employeeInfo.Field<string>("NATIONALIDNUMBER"),
            DateOfBirth = $"{employeePrivateData.Field<DateTime?>("DATEOFBIRTH"):yyyy-MM-dd}",
            LastName = personInfo.Field<string>("LASTNAME"),
            Initials = personInfo.Field<string>("INITIALS").ToUpper().Replace(".", ""),
            NamePrefix = personInfo.Field<string>("NAMEPREFIX"),
            FirstName = personInfo.Field<string>("FIRSTNAME"),
            NameProcessingCode = "01",
            Gender = (personInfo.Field<Gender>("GENDER") == Gender.Female) ? "V" : "M",
            EmployeeId = $"{companyInfo.Abbreviation}_{employeeInfo.Field<int>("PK_R_EMPLOYEE")}",
            //Partner = new Partner(){NameProcessingCode = "04", LastName = ""}, //We houden in iQ geen partner bij. We maken deze leeg
            AddressInfo = addressInfo == null
                ? null
                : new AddressInfoPerspectief()
                {
                    AddressType = "01",
                    ZipCode = addressInfo.Field<string>("ZIPCODE"),
                    City = addressInfo.Field<string>("CITY"),
                    Street = addressInfo.Field<string>("STREET"),
                    HouseNumber = addressInfo.Field<string>("HOUSENUMBER2"),
                    HouseNumberAddition = addressInfo.Field<string>("ADDITIONHOUSENUMBER"),
                },
            ListCommunicationInfo = new List<Com>()
            {
                new Com() {CommunicationType = "01", Value = ""},
                new Com() {CommunicationType = "02", Value = employeePrivateData.Field<string>("PHONE1").Replace(" ", "")},
                new Com() {CommunicationType = "04", Value = employeeInfo.Field<string>("EMAIL")},
                new Com() {CommunicationType = "05", Value = employeePrivateData.Field<string>("EMAIL")},
                /*new Com(){CommunicationType = "06", Value = employeeInfo.Field<string>("EMAIL")},
                new Com(){CommunicationType = "07", Value = personInfo.Field<string>("PHONE1")},
                new Com(){CommunicationType = "08", Value = personInfo.Field<string>("SPEEDDIAL")},
                new Com(){CommunicationType = "09", Value = personInfo.Field<string>("PHONE1")},
                new Com(){CommunicationType = "10", Value = employeeInfo.Field<string>("MOBILE")},*/
            },
            Employment = employment,
        };

        var employer = new Employer()
        {
            CompanyName = companyInfo.CompanyName,
            IdentificationCodePerspectief = companyInfo.IdentificationCodePerspectief,
            IdentificationCodePerspectief2 = companyInfo.IdentificationCodePerspectief,
            IdentificationIncomeTax = companyInfo.IdentificationIncomeTax,
            AddressInfo = companyInfo.AddressInfo,
            Employee = employee
        };

        return new RootExport() {MessageInfo = messageInfo, Employer = employer};
    }

    public FunctionInfo GetFunctionInfo(int? function)
    {
        if (!function.HasValue)
        {
            return new FunctionInfo() {Code = "", Description = ""};
        }
        
        var position = GetRecordset("R_POSITION", "CODE, DESCRIPTION",
            $"PK_R_POSITION = {function.Value}", "").DataTable.AsEnumerable().First();

        return new FunctionInfo()
        {
            Code = position.Field<string>("CODE"), Description = position.Field<string>("DESCRIPTION")
        };
    }

    public Department GetDepartmentInfo(int? department)
    {
        if (!department.HasValue)
        {
            return new Department() {Code = "", Description = ""};
        }

        return GetRecordset("R_DEPARTMENT", "CODE, DESCRIPTION",
                $"PK_R_DEPARTMENT = {department.Value}", "")
            .DataTable.AsEnumerable().Select(x => new Department()
            {
                Code = x.Field<string>("CODE"), Description = x.Field<string>("DESCRIPTION")
            }).ToList().First();
    }

    public CompanyInfo GetOwnCompanyInfo()
    {
        var crm = GetRecordset("R_CRMSETTINGS",
                "FK_OWNCOMPANY, ABBREVIATIONCOMPANY, IDENTIFICATIONCODEPERSPECTIEF, TEMPLOCATIONEXPORTPERSPECTIEF",
                "", "")
            .DataTable.AsEnumerable().First();

        var ownCompany = GetRecordset("R_RELATION", "NAME, FK_VISITINGADDRESS",
                $"PK_R_RELATION = {crm.Field<int>("FK_OWNCOMPANY")}", "")
            .DataTable.AsEnumerable().First();

        var address = GetRecordset("R_ADDRESS", "ZIPCODE, CITY, STREET, HOUSENUMBER2, ADDITIONHOUSENUMBER",
                $"PK_R_ADDRESS = {ownCompany.Field<int>("FK_VISITINGADDRESS")}", "")
            .DataTable.AsEnumerable().First();

        return new CompanyInfo()
        {
            CompanyName = ownCompany.Field<string>("NAME"),
            Abbreviation = crm.Field<string>("ABBREVIATIONCOMPANY"),
            IdentificationCodePerspectief = crm.Field<string>("IDENTIFICATIONCODEPERSPECTIEF"),
            IdentificationIncomeTax = "",
            AddressInfo = new AddressInfoPerspectief()
            {
                AddressType = "01",
                ZipCode = address.Field<string>("ZIPCODE"),
                City = address.Field<string>("CITY"),
                Street = address.Field<string>("STREET"),
                HouseNumber = address.Field<string>("HOUSENUMBER2"),
                HouseNumberAddition = address.Field<string>("ADDITIONHOUSENUMBER"),
            },
            TempExportLocationPerspectief = crm.Field<string>("TEMPLOCATIONEXPORTPERSPECTIEF"),
        };
    }

    [XmlType(TypeName = "WerknemersGegevens")]
    public class RootExport
    {
        [XmlElement("BrAlg")] public MessageInfo MessageInfo { get; set; }
        [XmlElement("Wrkgvr")] public Employer Employer { get; set; }
    }
}
