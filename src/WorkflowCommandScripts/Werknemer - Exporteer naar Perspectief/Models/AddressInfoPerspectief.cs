﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Werknemer___Exporteer_naar_Perspectief.Models
{
    public class AddressInfoPerspectief
    {
        [XmlElement("SrtAdrsCd")] public string AddressType { get; set; }
        [XmlElement("Pc")] public string ZipCode { get; set; }
        [XmlElement("Wnpl")] public string City { get; set; }
        [XmlElement("StraatLang")] public string Street { get; set; }
        [XmlElement("Huisnr")] public string HouseNumber { get; set; }
        [XmlElement("HuisToev")] public string HouseNumberAddition { get; set; }

    }
}
