﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Werknemer___Exporteer_naar_Perspectief.Models
{
    public class Employee
    {
        [XmlElement("VrwrkCd")] public string ProcessingCode { get; set; }
        [XmlElement("SofiNr")] public string NationalIdNumber { get; set; }
        [XmlElement("Gebdat")] public string DateOfBirth { get; set; }
        [XmlElement("SignNm")] public string LastName { get; set; }
        [XmlElement("Voorl")] public string Initials { get; set; }
        [XmlElement("Voorv")] public string NamePrefix { get; set; }
        [XmlElement("Roepnaam")] public string FirstName { get; set; }
        [XmlElement("NmvrkrCd")] public string NameProcessingCode { get; set; }
        [XmlElement("GslchtCd")] public string Gender { get; set; }
        [XmlElement("IdWrknmr")] public string EmployeeId { get; set; }
        [XmlElement("Prtnr")] public Partner Partner { get; set; }
        [XmlElement("StrAdrNl")] public AddressInfoPerspectief AddressInfo { get; set; }
        [XmlElement("Com")] public List<Com> ListCommunicationInfo { get; set; }
        [XmlElement("Dnstvbnd")] public Employment Employment { get; set; }
    }
}
