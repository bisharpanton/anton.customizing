﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Werknemer___Exporteer_naar_Perspectief.Models
{
    public class Partner
    {
        [XmlElement("VrwrkCd")] public string NameProcessingCode { get; set; }
        [XmlElement("SignNm")] public string LastName { get; set; }
    }
}
