﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Werknemer___Exporteer_naar_Perspectief.Models
{
    public class Employment
    {
        [XmlElement("VrwrkCd")] public string ProcessingCode { get; set; }
        [XmlElement("IdDnstvbnd")] public string EmploymentId { get; set; }
        [XmlElement("Ingdat")] public string EmploymentStartDate { get; set; }
        [XmlElement("Enddat")] public string EmploymentEndDate { get; set; }
        [XmlElement("RdEndDnstvbndCd")] public string ReasonTermination { get; set; }
        [XmlElement("PersNr")] public string EmployeeNumber { get; set; }
        [XmlElement("SrtDnstvbndCd")] public string EmploymentType { get; set; }
        [XmlElement("NmFnct")] public string FunctionDescription { get; set; }
        [XmlElement("NmOrgeenh")] public string NameOrganisationUnit { get; set; }
        [XmlElement("OrgeenhCd")] public string OrganisationUnitCode { get; set; }
        [XmlElement("CntrctUrnWk")] public double ContractHoursPerWeek { get; set; }
        [XmlElement("DltfctNVS")] public double PartTimeFactor { get; set; }
    }
}
