﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Werknemer___Exporteer_naar_Perspectief.Models
{
    public class MessageInfo
    {
        [XmlElement("BrCd")] public string MessageCode { get; set; }
        [XmlElement("VnBrCd")] public string VersionMessageCode { get; set; }
        [XmlElement("AandatBr")] public string CreationDate { get; set; }
        [XmlElement("AantijdBr")] public string CreationTime { get; set; }
        [XmlElement("IdInzndr")] public string Sender { get; set; }
        [XmlElement("IdOntvngr")] public string Receiver { get; set; }
        [XmlElement("Berrefnr")] public string Reference { get; set; }
        [XmlElement("TestJN")] public string TestMessage { get; set; }
        [XmlElement("OntvngstbevJN")] public string ConfimationOfReceipt { get; set; }
    }
}
