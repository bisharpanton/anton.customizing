﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Werknemer___Exporteer_naar_Perspectief.Models
{
    public class FunctionInfo
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
