﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Werknemer___Exporteer_naar_Perspectief.Models
{
    public class Employer
    {
        [XmlElement("HndlsnmOrg")] public string CompanyName { get; set; }
        [XmlElement("IdWrkgvrArbdnst")]  public string IdentificationCodePerspectief { get; set; }
        [XmlElement("AansltnrGeguitwlngArbdnst")]   public string IdentificationCodePerspectief2 { get; set; }
        [XmlElement("Lhnr")] public string IdentificationIncomeTax { get; set; }
        [XmlElement("StrAdrNl")] public AddressInfoPerspectief AddressInfo { get; set; }
        [XmlElement("Wrknmr")] public Employee Employee { get; set; }
    }
}
