﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Werknemer___Exporteer_naar_Perspectief.Models
{
    public class Com
    {
        [XmlElement("SrtComCd")] public string CommunicationType { get; set; }
        [XmlElement("NrCom")] public string Value { get; set; }
    }
}
