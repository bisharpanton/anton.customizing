﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Werknemer___Exporteer_naar_Perspectief.Models
{
    public class CompanyInfo
    {
        public string CompanyName { get; set; }
        public string Abbreviation { get; set; }
        public string IdentificationCodePerspectief { get; set; }
        public string IdentificationIncomeTax { get; set; }
        [XmlElement("StrAdrNl")] public AddressInfoPerspectief AddressInfo { get; set; }
        public string TempExportLocationPerspectief { get; set; }
    }
}
