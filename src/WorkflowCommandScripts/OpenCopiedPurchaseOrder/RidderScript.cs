﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using Ridder.Client.SDK.SDKParameters;
using System.Threading;

public class OpenCopiedPurchaseOrder_RidderScript : WorkflowCommandScript
{
    public void Execute()
    {
        // HVE
        // 4-5-2021
        // Get copied purchaseorder
        var purchaseOrderId = (int)RecordIds.First();


        //Zoek purchase order op
        var newPurchaseOrderId = 0;

        for (int i = 0; i < 10; i++)
        {
            var rsPurchaseOrder = GetRecordset("R_PURCHASEORDER", "PK_R_PURCHASEORDER", $"FK_ORIGNALPURCHASEORDER = {purchaseOrderId}", "DATECREATED DESC");

            if(rsPurchaseOrder.RecordCount > 0)
            {
                newPurchaseOrderId = rsPurchaseOrder.DataTable.AsEnumerable().First().Field<int>("PK_R_PURCHASEORDER");

                break;
            }
            else
            {
                Thread.Sleep(1000);
            }
        }

        if(newPurchaseOrderId == 0)
        {
            MessageBox.Show("Opzoeken nieuw aangemaakte inkooporder is mislukt.", "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        var editPar = new EditParameters() {TableName = "R_PURCHASEORDER", Id = newPurchaseOrderId };
        OpenEdit(editPar, false);
    }
}
