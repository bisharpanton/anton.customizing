﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Drawing;
using System.IO;
using MailAndGeneratePaymentReminder;
using Ridder.Client.SDK.SDKParameters;

public class MailAndGeneratePaymentReminder_RidderScript : WorkflowCommandScript
{
    /* DB - 17-9-2019 - Vervangen door een commandscript zodat er een refresh gegeven kan worden
    public void Execute()
    {
        //DB
        //08-01-2019
        //Genereer aanmaning voor de verkoopfacturen uit het overzicht 'Openstaande posten'.

        if (TableName == null || TableName != "R_RELATION")
        {
            MessageBox.Show("Dit script mag alleen vanaf de relatie-tabel aangeroepen worden.");
            return;
        }

        if (RecordIds?[0] == null)
        {
            MessageBox.Show("Geen relatie gevonden.");
            return;
        }

        int id = (int)RecordIds[0];


        GenerateMail(id);
        CreatePaymentReminders(id);
    }

    private void GenerateMail(int relationId)
    {
        var result = new GenerateMailsResult();

        var rsSettings = GetRecordset("R_MAILREPORTSETTING", "",
            $"FK_TABLEINFO = '{_relationTableInfoId}' AND DESCRIPTION = 'Mail overzicht openstaande posten'", "");

        if (rsSettings.RecordCount == 0)
        {
            throw new Exception(
                "No mailsetting found on table 'Relations' with description 'Mail overzicht openstaande posten'.");
        }

        rsSettings.MoveFirst();

        var calculatedColumns = new[] {"FILELOCATION", "FILENAME", "TO", "SUBJECT", "FROM", "CC", "BCC", "SALUTATION"};
        var calculatedColumnData = GetCalculatedColumnOutput(rsSettings, calculatedColumns, "R_RELATION", relationId);

        var path = Path.GetTempPath();

        var fileName = (string)calculatedColumnData["FILENAME"];

        if (string.IsNullOrEmpty(fileName))
        {
            throw new Exception(
                "Fout bij het ophalen van gecalculeerde kolommen van mailsjabloon (Bestandsnaam is leeg).");
        }

        fileName = fileName.Replace("\\", "_").Replace("/", "_")
            .Replace(":", "_").Replace("?", "_").Replace("*", "_")
            .Replace("\"", "_").Replace("<", "_").Replace(">", "_")
            .Replace("|", "_");

        var fullFileName = Path.Combine(path, fileName);

        Guid userReportId = (Guid)rsSettings.Fields["FK_USERREPORT"].Value;
        ExportReport(relationId, "R_RELATION", DesignerScope.User, userReportId, Guid.Empty, fullFileName,
            ExportType.Pdf, false);

        using (var mail = new OutlookMail
        {
            To = (string)calculatedColumnData["TO"],
            Subject = (string)calculatedColumnData["SUBJECT"]
        })
        {
            var from = calculatedColumnData["FROM"] as string;
            if (!string.IsNullOrEmpty(from))
            {
                mail.SentOnBehalfOfName = from;
            }

            var cc = calculatedColumnData["CC"] as string;
            if (!string.IsNullOrEmpty(cc))
            {
                mail.CC = cc;
            }

            var bcc = calculatedColumnData["BCC"] as string;
            if (!string.IsNullOrEmpty(bcc))
            {
                mail.BCC = bcc;
            }

            mail.BodyFormat = _mailBodyFormat;

            var salutation = calculatedColumnData["SALUTATION"] as string;
            var signature = rsSettings.Fields["SIGNATURE"].Value as string;
            var body = rsSettings.Fields["BODY"].Value as string;
            var signaturekind = (int)rsSettings.Fields["SIGNATUREKIND"].Value;

            var bodyWithSignature = mail.HTMLBody ?? "";
            var bodyTagStart = bodyWithSignature.IndexOf("<o:p>&nbsp;", StringComparison.OrdinalIgnoreCase);
            if (bodyTagStart >= 0)
            {
                var preBodyString = bodyWithSignature.Substring(0, bodyTagStart);
                var postBodyString = bodyWithSignature.Substring(bodyTagStart);
                var salutationAndBody = salutation + "<BR><BR>" + body;
                if (signaturekind == 2) // 2 = Eigen tekst
                    salutationAndBody += "<BR><BR>" + signature;

                var newBody = preBodyString + salutationAndBody + postBodyString;
                mail.HTMLBody = newBody;
            }
            else
            {
                mail.HTMLBody = salutation + "<BR><BR>" + bodyWithSignature;
            }

            // Add default report
            mail.AddAttachment(fullFileName);

            switch (_mailAction)
            {
                case MailAction.Send:
                    if (_deferredDeliveryTime != null)
                    {
                        mail.DeferredDeliveryTime = _deferredDeliveryTime();
                    }

                    mail.Send();
                    break;
                case MailAction.SaveInDrafts:
                    mail.SaveAsDraft();
                    break;
                case MailAction.Display:
                    mail.Display();
                    break;
            }
        }
    }

    private void CreatePaymentReminders(int id)
    {
        var salesInvoicesToReminder = GetRecordset("C_SALESINVOICESTOREMINDER", "FK_SALESINVOICE",
                $"FK_INVOICERELATION = {id}", "").DataTable.AsEnumerable().Select(x => x.Field<int>("FK_SALESINVOICE"))
            .ToList();

        if (!salesInvoicesToReminder.Any())
        {
            return;
        }

        var rsPaymentReminder = GetRecordset("C_PAYMENTREMINDER", "", "PK_C_PAYMENTREMINDER = -1", "");
        rsPaymentReminder.UseDataChanges = false;
        rsPaymentReminder.UpdateWhenMoveRecord = false;

        foreach (var salesInvoice in salesInvoicesToReminder)
        {
            rsPaymentReminder.AddNew();
            rsPaymentReminder.Fields["FK_SALESINVOICE"].Value = salesInvoice;
            rsPaymentReminder.Fields["DATEREMINDER"].Value = DateTime.Now;
            rsPaymentReminder.Fields["FK_CREATEDBY"].Value = GetCurrentEmployee(CurrentUserId);
        }

        rsPaymentReminder.MoveFirst();
        rsPaymentReminder.Update();
    }

    private int? GetCurrentEmployee(int currentUserId)
    {
        var rsUser = GetRecordset("R_USER", "FK_EMPLOYEE",
            $"PK_R_USER = {currentUserId}", "");
        rsUser.MoveFirst();

        return rsUser.Fields["FK_EMPLOYEE"].Value as int?;
    }

    private IDictionary<string, object> GetCalculatedColumnOutput(ScriptRecordset recordset, string[] fields,
        string tableName, object recordId)
    {
        var result = new Dictionary<string, object>();
        foreach (var field in fields)
        {
            var fieldData = recordset.Fields[field].Value != DBNull.Value
                ? recordset.Fields[field].Value as byte[]
                : null;
            if (fieldData == null)
            {
                result[field] = null;
                continue;
            }

            result[field] = GetCalculatedColumnOutput(fieldData, tableName, recordId);
        }

        return result;
    }

    public enum MailAction
    {
        Send,
        SaveInDrafts,
        Display
    }

    internal class GenerateMailsResult
    {
        public GenerateMailsResult()
        {
            LogMessage = string.Empty;
        }

        public string LogMessage { get; set; }
    }*/
}
