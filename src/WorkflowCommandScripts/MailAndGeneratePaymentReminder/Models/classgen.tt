﻿<#@ template debug="true" hostSpecific="true" language="C#" #>
<#@ Assembly Name="System.Core" #>
<#@ Assembly Name="System.Windows.Forms" #>
<#@ Assembly Name="System.Xml" #>
<#@ Assembly Name="System" #>
<#@ Assembly Name="Microsoft.CSharp" #>
<#@ Assembly name="EnvDTE" #>
<#@ assembly name="Microsoft.VisualStudio.OLE.Interop" #>
<#@ assembly name="Microsoft.VisualStudio.Shell" #>
<#@ assembly name="Microsoft.VisualStudio.Shell.Interop" #>
<#@ assembly name="Microsoft.VisualStudio.Shell.Interop.8.0" #>
<#@ Assembly name="adodb" #>
<#@ Assembly name="C:\Program Files (x86)\Ridder iQ Client\Bin\Ridder.Client.SDK.dll" #>
<#@ import namespace="EnvDTE" #>
<#@ import namespace="System" #>
<#@ import namespace="System.Collections" #>
<#@ import namespace="System.IO" #>
<#@ import namespace="System.Linq" #>
<#@ import namespace="System.Text" #>
<#@ import namespace="System.Collections.Generic" #>
<#@ import namespace="System.Reflection" #>
<#@ import namespace="System.Xml" #>
<#@ import namespace="System.Xml.Serialization" #>
<#@ import namespace="System.Xml.Schema" #>
<#@ import namespace="System.CodeDom" #>
<#@ import namespace="System.CodeDom.Compiler" #>
<#@ import namespace="Microsoft.CSharp" #>
<#@ import namespace="System.Xml.Serialization.Advanced" #>
<#@ import namespace="Microsoft.VisualStudio" #>
<#@ import namespace="Microsoft.VisualStudio.Shell" #>
<#@ import namespace="Microsoft.VisualStudio.Shell.Interop" #>
<#@ import namespace="Microsoft.VisualStudio.TextTemplating" #>
<#@ import namespace="Ridder.Client.SDK" #>
<#+
	private Dictionary<string, bool> enumsToGenerate = new Dictionary<string, bool>();

    public class ClassGenOptions
    {
        // public string ClassName { get; set; }
        // public string TableName { get; set; }
        public string Filter { get; set; }
        public string Namespace { get; set; }
        public string[] NamespaceImports { get; set; }

        public string SdkCompanyName { get; set; }
        public string SdkUserName { get; set; }
        public string SdkPassword { get; set; }
        public string SdkClientPath { get; set; }
		public string BaseClassName { get; set; }

        public bool GenerateAttributes { get; set; }
		public bool SkipPrimaryKey { get; set; }
		public bool UseEnums { get; set; }

        public bool FixFKColumnNames { get; set; }
        public bool RenamePKColumnsToId { get; set; } = true;

        public ClassGenOptions()
        {
            GenerateAttributes = true;
            NamespaceImports = new string[] { "System", "Ridder.Client.SDK.Extensions" };
            Namespace = "";
            Filter = "ISSECRET = 0";
			UseEnums = true;
        }
    }

	public void Run(Action action)
	{
		action();
    }

	public System.Threading.Tasks.Task RunAsync(Action action)
	{
		return System.Threading.Tasks.Task.Run(action);
    }

    public void GenerateEnum(ClassGenOptions options, string className, string tableName)
    {
		if (string.IsNullOrEmpty(options.SdkCompanyName))
		{
			throw new Exception("SdkCompanyName (in options.tt) nog niet ingevuld.");
		}

        var sdk = new RidderIQSDK();
        sdk.SetRidderIQClientPath(options.SdkClientPath);
        sdk.ConnectToPersistedSession(options.SdkUserName, options.SdkPassword, options.SdkCompanyName);
        if (!sdk.LoggedinAndConnected)
        {
            sdk.Login(options.SdkUserName, options.SdkPassword, options.SdkCompanyName);
            sdk.PersistSession();
        }

        IList<ChoiceValue> choiceValues = new List<ChoiceValue>();
        var rsChoiceType = sdk.CreateRecordset("M_CHOICETYPE", "", string.Format("CHOICETYPENAME = '{0}'", tableName), "");
        rsChoiceType.MoveFirst();
        var choiceType = new ChoiceType()
                        {
                            Id = (Guid)rsChoiceType.Fields["PK_M_CHOICETYPE"].Value,
                            ChoiceTypeName = (string)rsChoiceType.Fields["CHOICETYPENAME"].Value,
                        };

        var rsColumns = sdk.CreateRecordset("M_CHOICEVALUE", "", string.Format("FK_CHOICETYPE = '{0}'", choiceType.Id), "CHOICENUMBER ASC");
        if (rsColumns.RecordCount == 0)
        {
            return;
        }

        rsColumns.MoveFirst();
        while (!rsColumns.EOF)
        {
			var choiceText = sdk.GetTranslatableFieldValue("M_CHOICEVALUE", "CHOICETEXT", 2, (Guid)rsColumns.Fields["PK_M_CHOICEVALUE"].Value);
			if (string.IsNullOrEmpty(choiceText))
			{
				choiceText = (string)rsColumns.Fields["CHOICETEXT"].Value;
			}
			
            choiceValues.Add(new ChoiceValue()
                                {
                                    Id = (Guid)rsColumns.Fields["PK_M_CHOICEVALUE"].Value,
                                    ChoiceNumber = (int)rsColumns.Fields["CHOICENUMBER"].Value,
                                    ChoiceText = choiceText,
                                    Fk_ChoiceType = (Guid)rsColumns.Fields["FK_CHOICETYPE"].Value
                                });
            rsColumns.MoveNext();
        }

        if (String.IsNullOrEmpty(options.Namespace))
        {
            var hostServiceProvider = (IServiceProvider)Host;
            var dte = (EnvDTE.DTE)hostServiceProvider.GetService(typeof(EnvDTE.DTE));
            var activeSolutionProjects = (Array)dte.ActiveSolutionProjects;
            var dteProject = (EnvDTE.Project)activeSolutionProjects.GetValue(0);
            var defaultNamespace = dteProject.Properties.Item("DefaultNamespace").Value;
            var templateDir = Path.GetDirectoryName(Host.TemplateFile);
            var fullPath = dteProject.Properties.Item("FullPath").Value.ToString();
            fullPath = fullPath.EndsWith("\\") ? fullPath.Substring(0, fullPath.Length - 1) : fullPath;
            var subNamespace = templateDir.Replace(fullPath, string.Empty).Replace("\\", ".");
            options.Namespace = string.Concat(defaultNamespace, subNamespace);
        }

        var codeNamespace = new System.CodeDom.CodeNamespace(options.Namespace);

        var type = new CodeTypeDeclaration(className);
        type.IsPartial = false;
        type.IsEnum = true;
        type.Attributes = MemberAttributes.Public;
        codeNamespace.Types.Add(type);
        type.BaseTypes.Add(typeof(System.Int32));

        foreach (var choiceValue in choiceValues)
        {
            var sb = new StringBuilder();
            char? last = null;
            for (int i = 0; i < choiceValue.ChoiceText.Length; i++)
            {
                var c = choiceValue.ChoiceText[i];
                if (i == 0 && !char.IsLetter(c))
                    sb.Append("N");
                if (char.IsLetterOrDigit(c))
                    sb.Append(c);
                else if (last != '_')
                    sb.Append("_");
                last = c;
            }

            CodeMemberField field = new CodeMemberField(String.Empty, sb.ToString());
            field.CustomAttributes.Add(new CodeAttributeDeclaration("Description", new CodeAttributeArgument(new CodePrimitiveExpression(choiceValue.ChoiceText))));
            field.InitExpression = new CodePrimitiveExpression(choiceValue.ChoiceNumber);
            type.Members.Add(field);
            field.Comments.Add(
                new CodeCommentStatement(
                    string.Format("<summary>\r\n {0}\r\n </summary>", choiceValue.ChoiceText),
                    true));
        }

        // Check for invalid characters in identifiers
        CodeGenerator.ValidateIdentifiers(codeNamespace);

        // output the C# code
        var codeProvider = new CSharpCodeProvider();

        AddDefaultNamespaces(codeNamespace, options);
        codeNamespace.Imports.Add(new CodeNamespaceImport("System.ComponentModel"));

        foreach (var import in options.NamespaceImports)
        {
            if (codeNamespace.Imports.Cast<CodeNamespaceImport>().All(x => x.Namespace != import))
            {
                codeNamespace.Imports.Add(new CodeNamespaceImport(import));
            }
        }

        using (var sb = new StringWriter())
        {
            codeProvider.GenerateCodeFromNamespace(
                codeNamespace,
                sb,
                new CodeGeneratorOptions() { BracingStyle = "C", });
            Write(sb.GetStringBuilder().ToString());
        }

        SaveOutput(className + ".cs");
    }

    public void GenerateClass(ClassGenOptions options, string className, string tableName, string include = null, string exclude = null, string filter = null, string baseClassName = null, bool skipPrimaryKey = false)
    {
		if (string.IsNullOrEmpty(options.SdkCompanyName))
		{
			throw new Exception("SdkCompanyName (in options.tt) nog niet ingevuld.");
		}

        var includedColumns = include == null
                                  ? new Dictionary<string, bool>()
                                  : include.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToDictionary(x=>x.Trim(), x=>true, StringComparer.OrdinalIgnoreCase);
        var excludedColumns = exclude == null
                                  ? new Dictionary<string, bool>()
                                  : exclude.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToDictionary(x=>x.Trim(), x=>true, StringComparer.OrdinalIgnoreCase);

		baseClassName = !string.IsNullOrEmpty(baseClassName) ? baseClassName : options.BaseClassName;
		skipPrimaryKey = skipPrimaryKey || options.SkipPrimaryKey;

        var sdk = new RidderIQSDK();
        sdk.SetRidderIQClientPath(options.SdkClientPath);
        sdk.ConnectToPersistedSession(options.SdkUserName, options.SdkPassword, options.SdkCompanyName);
        if (!sdk.LoggedinAndConnected)
        {
            var loginResult = sdk.Login(options.SdkUserName, options.SdkPassword, options.SdkCompanyName);
            if (loginResult.HasError)
            {
                throw new Exception(loginResult.GetResult());
            }

            sdk.PersistSession();
        }

		IList<ColumnInfo> columninfos = new List<ColumnInfo>();
        var rsTableInfo = sdk.CreateRecordset("M_TABLEINFO", "", string.Format("TABLENAME = '{0}'", tableName), "");
        rsTableInfo.MoveFirst();
        var tableInfo = new TableInfo()
                        {
                            Id = (Guid)rsTableInfo.Fields["PK_M_TABLEINFO"].Value,
                            TableName = (string)rsTableInfo.Fields["TABLENAME"].Value,
                        };

        var filterStr = string.Format("FK_TABLEINFO = '{0}'", tableInfo.Id);
        if (!String.IsNullOrEmpty(options.Filter))
        {
            filterStr += " AND (" + options.Filter + ")";
        }

        if (!String.IsNullOrEmpty(filter))
        {
            filterStr += " AND (" + filter + ")";
        }

        var rsColumns = sdk.CreateRecordset("M_COLUMNINFO", "", filterStr, "COLUMNNAME");
        if (rsColumns.RecordCount == 0)
        {
            return;
        }

        rsColumns.MoveFirst();
        while (!rsColumns.EOF)
        try
        {
            var columnName = (string)rsColumns.Fields["COLUMNNAME"].Value;
            var isPrimaryKey = columnName.Equals("PK_" + tableName, StringComparison.OrdinalIgnoreCase);

            var ci = new ColumnInfo()
                            {
                                ColumnName = columnName,
                                DataType = (string)rsColumns.Fields["DATATYPE"].Value,
                                Description = (string)rsColumns.Fields["DESCRIPTION"].Value,
                                FK_TableInfo = (Guid)rsColumns.Fields["FK_TABLEINFO"].Value,
                                FK_TableInfo_Parent = rsColumns.Fields["FK_TABLEINFO_PARENT"].Value != DBNull.Value ? (Guid?)rsColumns.Fields["FK_TABLEINFO_PARENT"].Value : null,
                                Id = (Guid)rsColumns.Fields["PK_M_COLUMNINFO"].Value,
                                Required = (bool)rsColumns.Fields["REQUIRED"].Value,
                                Size = (int)rsColumns.Fields["SIZE"].Value,
                                IsPrimaryKey = isPrimaryKey,
								FK_CHOICETYPE = rsColumns.Fields["FK_CHOICETYPE"].Value != DBNull.Value ? (Guid?)rsColumns.Fields["FK_CHOICETYPE"].Value : null,
								IsSystemColumn = (bool)rsColumns.Fields["ISSYSTEMCOLUMN"].Value
                            };
            if (ci.FK_TableInfo_Parent != null)
            {
                var rsParentTableInfo = sdk.CreateRecordset("M_TABLEINFO", "", string.Format("TABLENAME = '{0}'", tableName), "");
                rsParentTableInfo.MoveFirst();
                ci.ParentTableName = (string)rsParentTableInfo.Fields["TABLENAME"].Value;
            }

            if (ci.FK_CHOICETYPE != null)
            {
                var rsChoiceType = sdk.CreateRecordset("M_CHOICETYPE", "", string.Format("PK_M_CHOICETYPE = '{0}'", ci.FK_CHOICETYPE), "");
                rsChoiceType.MoveFirst();
                ci.ChoiceTypeName = (string)rsChoiceType.Fields["CHOICETYPENAME"].Value;
            }

            columninfos.Add(ci);
        }
        finally
        {
            rsColumns.MoveNext();
        }

        if (String.IsNullOrEmpty(options.Namespace))
        {
            var hostServiceProvider = (IServiceProvider)Host;
            var dte = (EnvDTE.DTE)hostServiceProvider.GetService(typeof(EnvDTE.DTE));
            var activeSolutionProjects = (Array)dte.ActiveSolutionProjects;
            var dteProject = (EnvDTE.Project)activeSolutionProjects.GetValue(0);
            var defaultNamespace = dteProject.Properties.Item("DefaultNamespace").Value;
            var templateDir = Path.GetDirectoryName(Host.TemplateFile);
            var fullPath = dteProject.Properties.Item("FullPath").Value.ToString();
            fullPath = fullPath.EndsWith("\\") ? fullPath.Substring(0, fullPath.Length - 1) : fullPath;
            var subNamespace = templateDir.Replace(fullPath, string.Empty).Replace("\\", ".");
            options.Namespace = string.Concat(defaultNamespace, subNamespace);
        }

        var codeNamespace = new System.CodeDom.CodeNamespace(options.Namespace);

        var type = new CodeTypeDeclaration(className);
        type.IsPartial = true;
        type.IsClass = true;
        type.Attributes = MemberAttributes.Public;

		if (!string.IsNullOrEmpty(baseClassName))
        {
			type.BaseTypes.Add(new CodeTypeReference(baseClassName));
        }

        if (options.GenerateAttributes)
        {
            type.CustomAttributes.Add(new CodeAttributeDeclaration("Table", new CodeAttributeArgument()
                                                                                {
                                                                                    Name = "",
                                                                                    Value = new CodePrimitiveExpression(tableInfo.TableName)
                                                                                }));
        }

        codeNamespace.Types.Add(type);

		codeNamespace.Comments.Add(new CodeCommentStatement(" <auto-generated />", false));

		// comments on class
		type.Comments.Add(new CodeCommentStatement(" <summary>", true));
		type.Comments.Add(new CodeCommentStatement(string.Format(" \t Table: {0}", tableInfo.TableName), true));
		type.Comments.Add(new CodeCommentStatement(" </summary>", true));
		type.Comments.Add(new CodeCommentStatement(" <remarks>\r\n \tAvailable columns (for debugging, not necessarily available in this class):", true));

		int index = 0;
		while (index < columninfos.Count)
        {
			type.Comments.Add(new CodeCommentStatement(" \t" + string.Join(", ", columninfos.Skip(index).Take(10).Select(x => x.ColumnName)), true));
			index += 10;
        }
		type.Comments.Add(new CodeCommentStatement(" ", true));
		type.Comments.Add(new CodeCommentStatement(string.Format(" \tFilter used: {0}", filterStr), true));
		type.Comments.Add(new CodeCommentStatement(string.Format(" \tIncluded columns: {0}", include), true));
		type.Comments.Add(new CodeCommentStatement(string.Format(" \tExcluded columns: {0}", exclude), true));
		type.Comments.Add(new CodeCommentStatement(" </remarks>", true));

        foreach (var column in columninfos)
        {
            if (includedColumns.Count > 0 && !column.IsPrimaryKey && !includedColumns.ContainsKey(column.ColumnName))
            {
                continue;
            }

            if (excludedColumns.Count > 0 && !column.IsPrimaryKey && excludedColumns.ContainsKey(column.ColumnName))
            {
                continue;
            }

			if (skipPrimaryKey && column.IsPrimaryKey)
            {
				continue;
            }

            Type propertytype = null;

            string datatype = column.DataType.Trim();
            propertytype = ColumnTypeConverter.GetNativeType(datatype);

            if (!column.Required
                && !(propertytype.IsGenericType && propertytype.GetGenericTypeDefinition() == typeof(Nullable<>))
                && !(propertytype == typeof(string)) && !(propertytype.IsArray))
            {
                propertytype = typeof(Nullable<>).MakeGenericType(propertytype);
            }

			string propertyTypeName = propertytype.FullName;

			if (options.UseEnums && !string.IsNullOrEmpty(column.ChoiceTypeName))
            {
				propertyTypeName = column.ChoiceTypeName;
				if (!enumsToGenerate.ContainsKey(propertyTypeName))
				{
					enumsToGenerate.Add(propertyTypeName, false);
                }
            }

            var propertyName = column.ColumnName;
            if (options.FixFKColumnNames)
            {
                if (column.FK_TableInfo_Parent != null
                    && !column.ColumnName.StartsWith("FK_", StringComparison.OrdinalIgnoreCase))
                {
                    propertyName = "FK_" + column.ColumnName;
                }
            }

            var field = new CodeMemberField
                            {
                                Attributes = MemberAttributes.Private,
                                Name = "_" + propertyName,
                                Type = new CodeTypeReference(propertyTypeName),
                            };
            var property = new CodeMemberProperty()
                               {
                                   Attributes = MemberAttributes.Public,
                                   Name = propertyName,
                                   Type = new CodeTypeReference(propertyTypeName),
                               };

            if (options.GenerateAttributes)
            {
                if (column.IsPrimaryKey)
                {
                    if (options.RenamePKColumnsToId)
                    {
                        property.Name = "Id";
                        field.Name = "_id";
                    }

                    property.CustomAttributes.Add(
                        new CodeAttributeDeclaration(
                            "PrimaryKey",
                            new CodeAttributeArgument()
                                {
                                    Name = "",
                                    Value = new CodePrimitiveExpression(column.ColumnName)
                                }));
                }
                else if (column.ColumnName.StartsWith("FK_"))
                {
                    property.CustomAttributes.Add(
                        new CodeAttributeDeclaration(
                            "Column",
                            new CodeAttributeArgument()
                                {
                                    Name = "",
                                    Value = new CodePrimitiveExpression(column.ColumnName)
                                }));
                }
                else
                {
                    property.CustomAttributes.Add(
                        new CodeAttributeDeclaration(
                            "Column",
                            new CodeAttributeArgument()
                                {
                                    Name = "",
                                    Value = new CodePrimitiveExpression(column.ColumnName)
                                }));
                }

                if (column.IsSystemColumn)
                {
                    property.CustomAttributes.Add(new CodeAttributeDeclaration("IgnoreWrite"));
                }
            }

            if (propertytype == typeof(string))
            {
                var fieldStatement = new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), field.Name);
                CodeConditionStatement emptyCheck = new CodeConditionStatement();
                CodeMethodReferenceExpression emptyCheckMethod =
                    new CodeMethodReferenceExpression(new CodeTypeReferenceExpression(typeof(String)), "IsNullOrEmpty");
                CodeMethodInvokeExpression emptyCheckExpr = new CodeMethodInvokeExpression(
                    emptyCheckMethod,
                    fieldStatement);
                emptyCheck.Condition = emptyCheckExpr;
                emptyCheck.TrueStatements.Add(new CodeMethodReturnStatement(new CodePrimitiveExpression(String.Empty)));
                property.GetStatements.Add(emptyCheck);
                property.GetStatements.Add(
                    new CodeMethodReturnStatement(
                        new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), field.Name)));
            }
            else
            {
                property.GetStatements.Add(
                    new CodeMethodReturnStatement(
                        new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), field.Name)));
            }

            property.SetStatements.Add(
                new CodeAssignStatement(
                    new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), field.Name),
                    new CodePropertySetValueReferenceExpression()));

            string summary = column.Description;
			summary = summary.Replace(Environment.NewLine, Environment.NewLine + "\t");

            property.Comments.Add(
                new CodeCommentStatement(string.Format("<summary>\r\n \t{0}\r\n </summary>", summary), true));

            var remarks = new StringBuilder();
            remarks.AppendLine("<remarks>");
            remarks.Append(" \tDataType: ");
            remarks.AppendLine(datatype);
            remarks.Append(" \tSize: ");
            remarks.AppendLine(column.Size.ToString());
            if (column.FK_TableInfo_Parent != null)
            {
                remarks.Append(" \tTable: ");
                remarks.AppendLine(column.ParentTableName);
            }
            remarks.Append(" </remarks>");
            property.Comments.Add(
                new CodeCommentStatement(remarks.ToString(), true));

            type.Members.Add(field);
            type.Members.Add(property);
        }

        // Check for invalid characters in identifiers
        CodeGenerator.ValidateIdentifiers(codeNamespace);

        // output the C# code
        var codeProvider = new CSharpCodeProvider();

        AddDefaultNamespaces(codeNamespace, options);

        foreach (var import in options.NamespaceImports)
        {
            if (codeNamespace.Imports.Cast<CodeNamespaceImport>().All(x => x.Namespace != import))
            {
                codeNamespace.Imports.Add(new CodeNamespaceImport(import));
            }
        }

		if (options.UseEnums)
        {
			var keys = enumsToGenerate.Keys.ToList();
			foreach(var key in keys)
			{
				if (enumsToGenerate[key] == false)
				{
					enumsToGenerate[key] = true;
					GenerateEnum(options, key, key);
                }
			}
        }

        using (var sb = new StringWriter())
        {
            codeProvider.GenerateCodeFromNamespace(
                codeNamespace,
                sb,
                new CodeGeneratorOptions() { BracingStyle = "C", });
            Write(sb.GetStringBuilder().ToString());
        }

        SaveOutput(className + ".cs");
    }

    private void AddDefaultNamespaces(System.CodeDom.CodeNamespace codeNamespace, ClassGenOptions options)
    {
        codeNamespace.Imports.Add(new CodeNamespaceImport("System"));
        if (options.GenerateAttributes)
        {
            codeNamespace.Imports.Add(new CodeNamespaceImport("Ridder.Client.SDK.Extensions"));
        }
    }

    List<string> __savedOutputs = new List<string>();
    Engine __engine = new Engine();

    void DeleteOldOutputs()
    {
        EnvDTE.ProjectItem templateProjectItem = __getTemplateProjectItem();
        foreach (EnvDTE.ProjectItem childProjectItem in templateProjectItem.ProjectItems)
        {
            if (!__savedOutputs.Contains(childProjectItem.Name))
                childProjectItem.Delete();
        }
    }

    void ProcessTemplate(string templateFileName, string outputFileName)
    {
        string templateDirectory = Path.GetDirectoryName(Host.TemplateFile);
        string outputFilePath = Path.Combine(templateDirectory, outputFileName);

        string template = File.ReadAllText(Host.ResolvePath(templateFileName));
        string output = __engine.ProcessTemplate(template, Host);
        File.WriteAllText(outputFilePath, output);

        EnvDTE.ProjectItem templateProjectItem = __getTemplateProjectItem();
        templateProjectItem.ProjectItems.AddFromFile(outputFilePath);

        __savedOutputs.Add(outputFileName);
    }

    void SaveOutput(string outputFileName)
    {
        string templateDirectory = Path.GetDirectoryName(Host.TemplateFile);
        string outputFilePath = Path.Combine(templateDirectory, outputFileName);

        File.WriteAllText(outputFilePath, this.GenerationEnvironment.ToString());
        this.GenerationEnvironment = new StringBuilder();

        EnvDTE.ProjectItem templateProjectItem = __getTemplateProjectItem();
        templateProjectItem.ProjectItems.AddFromFile(outputFilePath);

        __savedOutputs.Add(outputFileName);
    }

    EnvDTE.ProjectItem __getTemplateProjectItem()
    {
        EnvDTE.Project dteProject = __getTemplateProject();

        IVsProject vsProject = __dteProjectToVsProject(dteProject);

        int iFound = 0;
        uint itemId = 0;
        VSDOCUMENTPRIORITY[] pdwPriority = new VSDOCUMENTPRIORITY[1];
        int result = vsProject.IsDocumentInProject(Host.TemplateFile, out iFound, pdwPriority, out itemId);
        if (result != VSConstants.S_OK)
            throw new Exception("Unexpected error calling IVsProject.IsDocumentInProject");
        if (iFound == 0)
            throw new Exception("Cannot retrieve ProjectItem for template file");
        if (itemId == 0)
            throw new Exception("Cannot retrieve ProjectItem for template file");

        Microsoft.VisualStudio.OLE.Interop.IServiceProvider itemContext = null;
        result = vsProject.GetItemContext(itemId, out itemContext);
        if (result != VSConstants.S_OK)
            throw new Exception("Unexpected error calling IVsProject.GetItemContext");
        if (itemContext == null)
            throw new Exception("IVsProject.GetItemContext returned null");

        ServiceProvider itemContextService = new ServiceProvider(itemContext);
        EnvDTE.ProjectItem templateItem = (EnvDTE.ProjectItem)itemContextService.GetService(typeof(EnvDTE.ProjectItem));

        return templateItem;
    }

    EnvDTE.Project __getTemplateProject()
    {
        IServiceProvider hostServiceProvider = (IServiceProvider)Host;
        if (hostServiceProvider == null)
            throw new Exception("Host property returned unexpected value (null)");

        EnvDTE.DTE dte = (EnvDTE.DTE)hostServiceProvider.GetService(typeof(EnvDTE.DTE));
        if (dte == null)
            throw new Exception("Unable to retrieve EnvDTE.DTE");

        Array activeSolutionProjects = (Array)dte.ActiveSolutionProjects;
        if (activeSolutionProjects == null)
            throw new Exception("DTE.ActiveSolutionProjects returned null");

        EnvDTE.Project dteProject = (EnvDTE.Project)activeSolutionProjects.GetValue(0);
        if (dteProject == null)
            throw new Exception("DTE.ActiveSolutionProjects[0] returned null");

        return dteProject;
    }

    static IVsProject __dteProjectToVsProject(EnvDTE.Project project)
    {
        if (project == null)
            throw new ArgumentNullException("project");

        string projectGuid = null;

        // DTE does not expose the project GUID that exists at in the msbuild project file.
        // Cannot use MSBuild object model because it uses a static instance of the Engine,
        // and using the Project will cause it to be unloaded from the engine when the
        // GC collects the variable that we declare.
        using (XmlReader projectReader = XmlReader.Create(project.FileName))
        {
            projectReader.MoveToContent();
            object nodeName = projectReader.NameTable.Add("ProjectGuid");
            while (projectReader.Read())
            {
                if (Object.Equals(projectReader.LocalName, nodeName))
                {
                    projectGuid = (string)projectReader.ReadElementContentAsString();
                    break;
                }
            }
        }
        if (string.IsNullOrEmpty(projectGuid))
            throw new Exception("Unable to find ProjectGuid element in the project file");

        Microsoft.VisualStudio.OLE.Interop.IServiceProvider dteServiceProvider =
            (Microsoft.VisualStudio.OLE.Interop.IServiceProvider)project.DTE;
        IServiceProvider serviceProvider = new ServiceProvider(dteServiceProvider);
        IVsHierarchy vsHierarchy = VsShellUtilities.GetHierarchy(serviceProvider, new Guid(projectGuid));

        IVsProject vsProject = (IVsProject)vsHierarchy;
        if (vsProject == null)
            throw new ArgumentException("Project is not a VS project.");
        return vsProject;
    }

    public class ColumnInfo
    {
        public Guid Id { get; set; }

        public Guid FK_TableInfo { get; set; }

        public Guid? FK_TableInfo_Parent { get; set; }

		public Guid? FK_CHOICETYPE { get; set; }

		public string ChoiceTypeName { get; set; }

        public string ParentTableName { get; set; }

        public string ColumnName { get; set; }

        public string DataType { get; set; }

        public bool Required { get; set; }

        public string Description { get; set; }

        public int Size { get; set; }

        public bool IsPrimaryKey { get; set; }

        public bool IsSystemColumn { get; set; }
    }

    public class TableInfo
    {
        public Guid Id { get; set; }

        public string TableName { get; set; }
    }

    public class ChoiceValue
    {
        public Guid Id { get; set; }

        public int ChoiceNumber { get; set; }

        public string ChoiceText { get; set; }

        public Guid Fk_ChoiceType { get; set; }
    }

    public class ChoiceType
    {
        public Guid Id { get; set; }

        public string ChoiceTypeName { get; set; }
    }

    public static class ColumnTypeConverter
    {
        public static Type GetNativeType(string dataType)
        {
            switch (dataType.Trim())
            {
                case "Guid":
                    return typeof(Guid);
                case "Float":
                case "Money":
                    return typeof(double);
                case "Boolean":
                    return typeof(bool);
                case "DateTime":
                    return typeof(DateTime);
                case "Blob":
                case "Image":
                    return typeof(byte[]);
                case "String":
                    return typeof(string);
                case "TimeSpan":
                    return typeof(long);
                case "Integer":
                    return typeof(int);
                case "NullType":
                    return typeof(DBNull);
                case "State":
                    return typeof(Guid);
                default:
                    throw new ArgumentException(dataType + " is an unknown datatype.");
            }
        }

        public static string GetColumnDataType(Type type)
        {
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>)) type = type.GetGenericArguments()[0];
            switch (type.Name)
            {
                case "Guid":
                    return "Guid";
                case "Float":
                case "Decimal":
                case "Double":
                    return "Float";
                case "Boolean":
                    return "Boolean";
                case "DateTime":
                    return "DateTime";
                case "TimeSpan":
                    return "TimeSpan";
                case "Int32":
                    return "Integer";
                case "Byte[]":
                    return "Blob";
                case "String":
                    return "String";
                default:
                    throw new ArgumentException("No datatype found for " + type.Name);
            }
        }
    }

    #>
