﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using AddOrdersToOrderFinancialState.Models;
using Ridder.Client.SDK.SDKParameters;
using Ridder.Common.ItemManagement;
using Ridder.Common.ProductionManagement;
using Ridder.Common.PurchaseAdministration;
using Ridder.Recordset.Extensions;
using System.ComponentModel;
using Ridder.Client.SDK;
using Item = AddOrdersToOrderFinancialState.Models.Item;
using Order = AddOrdersToOrderFinancialState.Models.Order;
using PurchaseOrderDetailItem = AddOrdersToOrderFinancialState.Models.PurchaseOrderDetailItem;
using PurchaseOrderDetailMisc = AddOrdersToOrderFinancialState.Models.PurchaseOrderDetailMisc;
using PurchaseOrderDetailOutsourced = AddOrdersToOrderFinancialState.Models.PurchaseOrderDetailOutsourced;

public class AddOrdersToOrderFinancialState_RidderScript : CommandScript
{
    public void Execute()
    {
        // HVE
        // 27-7-2021
        // Replace report OHW kosten historie
        if (FormDataAwareFunctions == null || FormDataAwareFunctions.TableName != "R_ORDER")
        {
            MessageBox.Show("Dit script mag alleen vanaf de order-tabel aangeroepen worden.",
                "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        // Get peildatum
        var targetDate = DetermineTargetDate().Date;
        if (targetDate == DateTime.MinValue)
        {
            return;
        }

        try
        {
            Cursor.Current = Cursors.WaitCursor;

            var orderIds = new List<int>();
            var filter = string.Empty;

            if (FormDataAwareFunctions.GetSelectedRecords().Any())
            {
                orderIds.AddRange(FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue()).ToList());
                filter = $"PK_R_ORDER IN ({string.Join(",", orderIds)}) AND";
            }
            var orders = GetRecordset("R_ORDER", "",
                    $"{filter} DATECREATED <= #{targetDate.ToString("yyyy-MM-dd")}#" +
                         $" AND (FINANCIALDONE = 0 OR DATEFINANCIALFINISHED > #{targetDate.ToString("yyyy-MM-dd")}#)",
                    "")
                .As<Order>()
                .ToList();

            orderIds = orders
                .Select(x => x.PK_R_ORDER)
                .ToList();
            if (!orders.Any())
            {
                MessageBox.Show($"Geen orders beschikbaar met peildatum {targetDate}.", "Ridder iQ", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }

            var orderWipDetails = DetermineOrderWipDetails(orderIds, targetDate);
            var salesInvoiceDetails = DetermineSalesInvoiceDetails(orderIds, targetDate);
            var resultaatneming = DetermineResultaatneming(orderIds, targetDate);

            var firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            var date_TakeProfit = firstDayOfMonth.AddMonths(1).AddDays(-1);
             

            var orderFinancialStates = new List<OrderFinancialState>();

            foreach (var order in orders)
            {
                var thisOrderWipDetails = orderWipDetails
                    .Where(y => y.FK_ORDER == order.PK_R_ORDER)
                    .ToList();

                var orderFinancialState = new OrderFinancialState();

                var billed = salesInvoiceDetails
                    .Where(y => y.FK_ORDER == order.PK_R_ORDER)
                    .Sum(x => x.NETSALESAMOUNT);

                var ohwWorkactivity = thisOrderWipDetails
                                      .Where(y => y.FK_ORDERWIPDETAILWORKACTIVITY.HasValue
                                      && y.DATEINWIP < targetDate.AddDays(1))
                    .Sum(x => x.COSTPRICE);

                var inkoopwaardeExclOntvangst = DeterminePurchaseValueExclReceived(order, thisOrderWipDetails, targetDate);
                var costprice = ohwWorkactivity + inkoopwaardeExclOntvangst;

                var profit = 0d;
                var dateFinancialFinished = order.DATEFINANCIALFINISHED as DateTime? ?? DateTime.MinValue;
                if (dateFinancialFinished == DateTime.MinValue
                    || dateFinancialFinished < targetDate)
                {
                    profit = (double)resultaatneming
                        .Where(y => y.FK_ORDER == order.PK_R_ORDER)
                        .Sum(x => x.AMOUNT);
                }

                var ohwTotal = costprice - billed + profit;
                var toReceive = DetermineToReceiveInvoices(order, targetDate);

                if (order.RENDEMENT_PERC > 0d)
                {
                    orderFinancialState.RENDEMENT_PERC = order.RENDEMENT_PERC;
                    orderFinancialState.TAKE_PROFIT = (costprice * order.RENDEMENT_PERC) - profit;
                }
                orderFinancialState.REF_DATE = targetDate;
                orderFinancialState.FK_ORDER = order.PK_R_ORDER;
                orderFinancialState.BILLED = billed;
                orderFinancialState.OHW_WORKACTIVITY = ohwWorkactivity;
                orderFinancialState.PURCHASED = inkoopwaardeExclOntvangst;
                orderFinancialState.COSTPRICE = costprice;
                orderFinancialState.OHW = ohwTotal;
                orderFinancialState.TOTAL_TAKEPROFIT = profit;
                orderFinancialState.BILLSTORECEIVE = toReceive;
                orderFinancialState.DATE_TAKEPROFIT = date_TakeProfit;

                orderFinancialStates.Add(orderFinancialState);
            }

            var updatedOrderIds = orderFinancialStates
                .Select(x => x.FK_ORDER)
                .ToList();

            var rsOrderFinancialState = GetRecordset("C_ORDERFINANCIALSTATE", "",
                $"FK_ORDER IN ({string.Join(",", updatedOrderIds)}) AND PROCESSED = 0", "");
            rsOrderFinancialState.UpdateWhenMoveRecord = false;

            if (rsOrderFinancialState.RecordCount > 0)
            {
                rsOrderFinancialState.MoveFirst();
                while (!rsOrderFinancialState.EOF)
                {
                    rsOrderFinancialState.Delete();
                    rsOrderFinancialState.MoveNext();
                }
                rsOrderFinancialState.MoveFirst();
                rsOrderFinancialState.Update();
            }

            foreach (var orderFinancialState in orderFinancialStates)
            {
                rsOrderFinancialState.AddNew();
                rsOrderFinancialState.Fill(orderFinancialState);
            }

            rsOrderFinancialState.MoveFirst();
            rsOrderFinancialState.Update();

            orderFinancialStates = rsOrderFinancialState.As<OrderFinancialState>().ToList();
            var ids = orderFinancialStates.Select(x => x.PK_C_ORDERFINANCIALSTATE).ToList();

            var browsePar = new BrowseParameters()
            {
                TableName = "C_ORDERFINANCIALSTATE",
                Filter =
                    $"PK_C_ORDERFINANCIALSTATE IN ({string.Join(",", ids)})",

            };

            OpenBrowse(browsePar, false);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
        finally
        {
            Cursor.Current = Cursors.Default;
        }
      
    }

    private double? DetermineToReceiveInvoices(Order order, DateTime targetDate)
    {
        var toReceive = 0d;
        
        var purchaseOrderDetailItems = DeterminePurchaseOrderDetailItems(order);
        var purchaseOrderDetailMiscs = DeterminePurchaseOrderDetailMisc(order);
        var purchaseOrderDetailOutsourceds = DeterminePurchaseOrderDetailOutsourced(order);
        if (!purchaseOrderDetailItems.Any()
        && !purchaseOrderDetailMiscs.Any()
        && !purchaseOrderDetailOutsourceds.Any())
        {
            return toReceive;
        }

        foreach (var purchaseOrderDetailItem in purchaseOrderDetailItems)
        {
            var priceEach = 0d;

            if ((bool)purchaseOrderDetailItem.Item.INVENTORYITEM)
            {
                priceEach = purchaseOrderDetailItem.Item.STANDARDCOST as double? ?? 0d;
            }
            else
            {
                priceEach = (double)purchaseOrderDetailItem.NETPURCHASEPRICE / purchaseOrderDetailItem.QUANTITY;
            }

            var quantityPurchaseDetail = purchaseOrderDetailItem.QUANTITY;
            var quantityInvoiced = purchaseOrderDetailItem.QuantityInvoiced;

            toReceive += (priceEach * (quantityPurchaseDetail - quantityInvoiced));
        }

        foreach (var purchaseOrderDetailMisc in purchaseOrderDetailMiscs)
        {
            if (purchaseOrderDetailMisc.PurchaseInvoiceDate == null 
                || purchaseOrderDetailMisc.PurchaseInvoiceDate > targetDate )
            {
                continue;
            }
            
            var quantityPurchaseDetail = purchaseOrderDetailMisc.QUANTITY;
            if (quantityPurchaseDetail == 0d)
            {
                continue;
            }

            var priceEach = (double)purchaseOrderDetailMisc.NETPURCHASEPRICE / purchaseOrderDetailMisc.QUANTITY;
            var quantityInvoiced = purchaseOrderDetailMisc.QuantityInvoiced;

            toReceive += (priceEach * (quantityPurchaseDetail - quantityInvoiced));
        }

        foreach (var purchaseOrderDetailOutsourced in purchaseOrderDetailOutsourceds)
        {
            if (purchaseOrderDetailOutsourced.PurchaseInvoiceDate == null
                || purchaseOrderDetailOutsourced.PurchaseInvoiceDate > targetDate)
            {
                continue;
            }

            var quantityPurchaseDetail = purchaseOrderDetailOutsourced.QUANTITY;
            if (quantityPurchaseDetail == 0d)
            {
                continue;
            }

            var priceEach = (double)purchaseOrderDetailOutsourced.NETPURCHASEPRICE / purchaseOrderDetailOutsourced.QUANTITY;
            var quantityInvoiced = purchaseOrderDetailOutsourced.QuantityInvoiced;

            toReceive += (priceEach * (quantityPurchaseDetail - quantityInvoiced));
        }

        return toReceive;
    }

    private List<PurchaseOrderDetailOutsourced> DeterminePurchaseOrderDetailOutsourced(Order order)
    {
        var purchaseOrderDetailOutsourceds = GetRecordset("R_PURCHASEORDERDETAILOUTSOURCED", "",
                $"FK_ORDER = {order.PK_R_ORDER}", "")
            .As<PurchaseOrderDetailOutsourced>().ToList();

        if (!purchaseOrderDetailOutsourceds.Any())
        {
            return purchaseOrderDetailOutsourceds;
        }

        var purchaseInvoiceDetailOutsourceds = new List<PurchaseInvoiceDetailOutsourced>();
        var purchaseInvoices = new List<PurchaseInvoice>();

        var purchaseOrderDetailOutsourcedIds = purchaseOrderDetailOutsourceds.Select(x => x.PK_R_PURCHASEORDERDETAILOUTSOURCED).ToList();
        var goodsReceiptDetailOutsourceds = GetRecordset("R_GOODSRECEIPTDETAILOUTSOURCED",
                "FK_PURCHASEORDERDETAILOUTSOURCED, PK_R_GOODSRECEIPTDETAILOUTSOURCED",
                $"FK_PURCHASEORDERDETAILOUTSOURCED IN ({string.Join(",", purchaseOrderDetailOutsourcedIds)})", "")
            .As<GoodsReceiptDetailOutsourced>().ToList();

        if (goodsReceiptDetailOutsourceds.Any())
        {
            var goodsToReceiptDetailOutsourcedIds = goodsReceiptDetailOutsourceds.Select(x => x.Pk_R_GoodsReceiptDetailOutsourced).ToList();
            purchaseInvoiceDetailOutsourceds = GetRecordset("R_PURCHASEINVOICEDETAILOUTSOURCED", "FK_PURCHASEINVOICE, FK_GOODSRECEIPTDETAILOUTSOURCED, QUANTITY",
                    $"FK_GOODSRECEIPTDETAILOUTSOURCED IN ({string.Join(",", goodsToReceiptDetailOutsourcedIds)})", "")
                .As<PurchaseInvoiceDetailOutsourced>().ToList();

            if (purchaseInvoiceDetailOutsourceds.Any())
            {
                var purchaseInvoiceIds = purchaseInvoiceDetailOutsourceds
                    .Select(x => x.FK_PURCHASEINVOICE)
                    .ToList();

                purchaseInvoices = GetRecordset("R_PURCHASEINVOICE", "",
                        $"PK_R_PURCHASEINVOICE IN ({string.Join(",", purchaseInvoiceIds)})", "")
                    .As<PurchaseInvoice>().ToList();
            }
        }

        foreach (var purchaseOrderDetailOutsourced in purchaseOrderDetailOutsourceds)
        {
            var goodsToReceiptDetailOutsourced = goodsReceiptDetailOutsourceds
                                               .Where(y =>
                                                   y.Fk_PurchaseOrderDetailOutsourced == purchaseOrderDetailOutsourced.PK_R_PURCHASEORDERDETAILOUTSOURCED)
                                               .ToList();

            if (!goodsToReceiptDetailOutsourced.Any())
            {
                continue;
            }

            var goodsToReceiptDetailIds = goodsToReceiptDetailOutsourced
                .Select(x => x.Pk_R_GoodsReceiptDetailOutsourced)
                .ToList();
            var purchaseInvoiceDetailOutsourced = purchaseInvoiceDetailOutsourceds
                .Where(y => goodsToReceiptDetailIds.Contains(y.FK_GOODSRECEIPTDETAILOUTSOURCED as int? ?? 0))
                .ToList();

            if (!purchaseInvoiceDetailOutsourced.Any())
            {
                continue;
            }

            purchaseOrderDetailOutsourced.QuantityInvoiced = purchaseInvoiceDetailOutsourced.Sum(x => x.QUANTITY);
            var purchaseInvoiceIds = purchaseInvoiceDetailOutsourced
                .Select(x => x.FK_PURCHASEINVOICE)
                .ToList();
            purchaseOrderDetailOutsourced.PurchaseInvoiceDate = purchaseInvoices
                .Where(y =>  purchaseInvoiceIds.Contains(y.PK_R_PURCHASEINVOICE))
                .Max(x => x.INVOICEDATE);
        }

        return purchaseOrderDetailOutsourceds;
    }



private List<PurchaseOrderDetailMisc> DeterminePurchaseOrderDetailMisc(Order order)
    {
        var purchaseOrderDetailMiscs = GetRecordset("R_PURCHASEORDERDETAILMISC", "",
                $"FK_ORDER = {order.PK_R_ORDER}", "")
            .As<PurchaseOrderDetailMisc>().ToList();
        
        if (!purchaseOrderDetailMiscs.Any())
        {
            return purchaseOrderDetailMiscs;
        }

        var purchaseInvoiceDetailMiscs = new List<PurchaseInvoiceDetailMisc>();
        var purchaseInvoices = new List<PurchaseInvoice>();

        var purchaseOrderDetailMiscIds = purchaseOrderDetailMiscs.Select(x => x.PK_R_PURCHASEORDERDETAILMISC).ToList();
        var goodsReceiptDetailMiscs = GetRecordset("R_GOODSRECEIPTDETAILMISC", "FK_PURCHASEORDERDETAILMISC, PK_R_GOODSRECEIPTDETAILMISC",
                $"FK_PURCHASEORDERDETAILMISC IN ({string.Join(",", purchaseOrderDetailMiscIds)})", "")
            .As<GoodsReceiptDetailMisc>().ToList();

        if (goodsReceiptDetailMiscs.Any())
        {
            var goodsToReceiptDetailMiscIds = goodsReceiptDetailMiscs.Select(x => x.Pk_R_GoodsReceiptDetailMisc).ToList();
            purchaseInvoiceDetailMiscs = GetRecordset("R_PURCHASEINVOICEDETAILMISC", "FK_PURCHASEINVOICE, FK_GOODSRECEIPTDETAILMISC, QUANTITY",
                    $"FK_GOODSRECEIPTDETAILMISC IN ({string.Join(",", goodsToReceiptDetailMiscIds)})", "")
                .As<PurchaseInvoiceDetailMisc>().ToList();

            if (purchaseInvoiceDetailMiscs.Any())
            {
                var purchaseInvoiceIds = purchaseInvoiceDetailMiscs
                    .Select(x => x.FK_PURCHASEINVOICE)
                    .ToList();

                purchaseInvoices = GetRecordset("R_PURCHASEINVOICE", "",
                        $"PK_R_PURCHASEINVOICE IN ({string.Join(",", purchaseInvoiceIds)})", "")
                    .As<PurchaseInvoice>().ToList();
            }
        }

        foreach (var purchaseOrderDetailMisc in purchaseOrderDetailMiscs)
        {
            var goodsToReceiptDetailMiscs = goodsReceiptDetailMiscs
                                               .Where(y =>
                                                   y.Fk_PurchaseOrderDetailMisc == purchaseOrderDetailMisc.PK_R_PURCHASEORDERDETAILMISC)
                                               .ToList();

            if (!goodsToReceiptDetailMiscs.Any())
            {
                continue;
            }

            var goodsToReceiptDetailMiscIds =
                goodsToReceiptDetailMiscs.Select(x => x.Pk_R_GoodsReceiptDetailMisc).ToList();
            var thisPurchaseInvoiceDetailMiscs = purchaseInvoiceDetailMiscs
                .Where(y => goodsToReceiptDetailMiscIds.Contains(y.FK_GOODSRECEIPTDETAILMISC as int? ?? 0))
                .ToList();

            if (!thisPurchaseInvoiceDetailMiscs.Any())
            {
                continue;
            }

            purchaseOrderDetailMisc.QuantityInvoiced = thisPurchaseInvoiceDetailMiscs.Sum(x => x.QUANTITY);
            var puchaseInvoiceIds = thisPurchaseInvoiceDetailMiscs.Select(x => x.FK_PURCHASEINVOICE).ToList();
            purchaseOrderDetailMisc.PurchaseInvoiceDate = purchaseInvoices
                .Where(y => puchaseInvoiceIds.Contains(y.PK_R_PURCHASEINVOICE))
                .Max(x => x.INVOICEDATE);
        }

        return purchaseOrderDetailMiscs;
    }


    private List<PurchaseOrderDetailItem> DeterminePurchaseOrderDetailItems(Order order)
    {
        var purchaseOrderDetailItems = GetRecordset("R_PURCHASEORDERDETAILITEM", "",
                $"FK_ORDER = {order.PK_R_ORDER}", "")
            .As<PurchaseOrderDetailItem>().ToList();
        if (!purchaseOrderDetailItems.Any())
        {
            return purchaseOrderDetailItems;
        }

        var itemIds = purchaseOrderDetailItems
            .Select(x => x.FK_ITEM)
            .Distinct()
            .ToList();
        var items = DetermineItems(itemIds);
        var purchaseInvoiceDetailItems = new List<PurchaseInvoiceDetailItem>();
        var purchaseInvoices = new List<PurchaseInvoice>();

        var purchaseOrderDetailItemIds = purchaseOrderDetailItems.Select(x => x.PK_R_PURCHASEORDERDETAILITEM).ToList();
        var goodsReceiptDetailItems = GetRecordset("R_GOODSRECEIPTDETAILITEM", "FK_PURCHASEORDERDETAILITEM, PK_R_GOODSRECEIPTDETAILITEM",
                $"FK_PURCHASEORDERDETAILITEM IN ({string.Join(",", purchaseOrderDetailItemIds)})", "")
            .As<GoodsReceiptDetailItem>().ToList();

        if (goodsReceiptDetailItems.Any())
        {
            var goodsToReceiptDetailItemIds = goodsReceiptDetailItems.Select(x => x.Pk_R_GoodsReceiptDetailItem).ToList();
            purchaseInvoiceDetailItems = GetRecordset("R_PURCHASEINVOICEDETAILITEM", "FK_PURCHASEINVOICE, FK_GOODSRECEIPTDETAILITEM, QUANTITY",
                    $"FK_GOODSRECEIPTDETAILITEM IN ({string.Join(",", goodsToReceiptDetailItemIds)})", "")
                .As<PurchaseInvoiceDetailItem>().ToList();

            if (purchaseInvoiceDetailItems.Any())
            {
                var purchaseInvoiceIds = purchaseInvoiceDetailItems
                    .Select(x => x.FK_PURCHASEINVOICE)
                    .ToList();

                purchaseInvoices = GetRecordset("R_PURCHASEINVOICE", "",
                        $"PK_R_PURCHASEINVOICE IN ({string.Join(",", purchaseInvoiceIds)})", "")
                    .As<PurchaseInvoice>().ToList();
            }
        }

        foreach (var purchaseOrderDetailItem in purchaseOrderDetailItems)
        {
            var item = items.FirstOrDefault(y => y.PK_R_ITEM == purchaseOrderDetailItem.FK_ITEM);
            if (item != null)
            {
                purchaseOrderDetailItem.Item = item;
            }

            var goodsToReceiptDetailItem = goodsReceiptDetailItems
                                               .Where(y => y.Fk_PurchaseOrderDetailItem ==
                                                           purchaseOrderDetailItem.PK_R_PURCHASEORDERDETAILITEM)
                                               .ToList();

            if (!goodsToReceiptDetailItem.Any())
            {
                continue;
            }

            var goodsToReceiptIds = goodsToReceiptDetailItem
                .Select(x => x.Pk_R_GoodsReceiptDetailItem)
                .ToList();
            var purchaseInvoiceDetailItem = purchaseInvoiceDetailItems
                .Where(
                    y => goodsToReceiptIds.Contains(y.FK_GOODSRECEIPTDETAILITEM as int? ?? 0))
                .ToList();

            if (!purchaseInvoiceDetailItem.Any())
            {
                continue;
            }

            purchaseOrderDetailItem.QuantityInvoiced = purchaseInvoiceDetailItem.Sum(x => x.QUANTITY);
            var purchaseInvoiceIds = purchaseInvoiceDetailItem
                .Select(x => x.FK_PURCHASEINVOICE)
                .ToList();
            purchaseOrderDetailItem.PurchaseInvoiceDate = purchaseInvoices
                .Where(y => purchaseInvoiceIds.Contains(y.PK_R_PURCHASEINVOICE))
                .Max(x => x.INVOICEDATE);
        }

        return purchaseOrderDetailItems;
    }


    private List<Item> DetermineItems(List<int> itemIds)
    {
        if (!itemIds.Any())
        {
            return null;
        }
        
        return GetRecordset("R_ITEM", "", $"PK_R_ITEM IN ({string.Join(",", itemIds)})", "")
            .As<Item>()
            .ToList();
    }

    private double DeterminePurchaseValueExclReceived(Order order, List<OrderWipDetail> orderWipDetails, DateTime targetDate)
    {
        return orderWipDetails
            .Where(y => !y.FK_ORDERWIPDETAILWORKACTIVITY.HasValue
            && y.PurchaseInvoice != null 
            && (y.PurchaseInvoice.INVOICEDATE == null || y.PurchaseInvoice.INVOICEDATE <= targetDate))
            .Sum(x => x.COSTPRICE) ?? 0d;
    }

    private OrderWipDetail DeterminePurchaseInvoiceDetails(OrderWipDetail orderWipDetail, KeyValuePair<string, int> tableAndId)
    {
        var column = $"PURCHASEINVOICEDETAIL{tableAndId.Key}";
        var purchaseInvoiceDetailId = DetermineDetailId(orderWipDetail, tableAndId, column);
        if (purchaseInvoiceDetailId > 0)
        {
            var rsPurchaseInvoiceDetail = GetRecordset($"R_{column}", "FK_PURCHASEINVOICE",
                $"PK_R_{column} = {purchaseInvoiceDetailId}", "");

            if (rsPurchaseInvoiceDetail.RecordCount > 0)
            {
                orderWipDetail = DeterminePurchaseInvoiceDetailAndId(orderWipDetail, rsPurchaseInvoiceDetail, tableAndId.Key);
            }
        }
        else
        {
            column = $"GOODSRECEIPTDETAIL{tableAndId.Key}";
            var goodsReceiptDetailId = DetermineDetailId(orderWipDetail, tableAndId, column);
            if (goodsReceiptDetailId > 0)
            {
                var columnInvoice = $"PURCHASEINVOICEDETAIL{tableAndId.Key}";
                var rsPurchaseInvoiceDetail = GetRecordset($"R_{columnInvoice}", "FK_PURCHASEINVOICE",
                    $"FK_{column} = {goodsReceiptDetailId}", "");

                if (rsPurchaseInvoiceDetail.RecordCount > 0)
                {
                    orderWipDetail =
                        DeterminePurchaseInvoiceDetailAndId(orderWipDetail, rsPurchaseInvoiceDetail, tableAndId.Key);
                }
            }
        }

        return orderWipDetail;
    }

    private OrderWipDetail DeterminePurchaseInvoiceDetailAndId(OrderWipDetail orderWipDetail, ScriptRecordset rsPurchaseInvoiceDetail, string key)
    {

        switch (key)
        {
            case "ITEM":
                orderWipDetail.PurchaseInvoiceDetailItem =
                    rsPurchaseInvoiceDetail.As<PurchaseInvoiceDetailItem>().First();
                orderWipDetail.PurchaseInvoiceId = orderWipDetail.PurchaseInvoiceDetailItem.FK_PURCHASEINVOICE;
                break;
            case "MISC":
                orderWipDetail.PurchaseInvoiceDetailMisc =
                    rsPurchaseInvoiceDetail.As<PurchaseInvoiceDetailMisc>().First();
                orderWipDetail.PurchaseInvoiceId = orderWipDetail.PurchaseInvoiceDetailMisc.FK_PURCHASEINVOICE;
                break;
            case "OUTSOURCED":
                orderWipDetail.PurchaseInvoiceDetailOutsourced =
                    rsPurchaseInvoiceDetail.As<PurchaseInvoiceDetailOutsourced>().First();
                orderWipDetail.PurchaseInvoiceId = orderWipDetail.PurchaseInvoiceDetailOutsourced.FK_PURCHASEINVOICE;
                break;
        }

        return orderWipDetail;
    }

    private int DetermineDetailId(OrderWipDetail orderAllWipDetail, KeyValuePair<string, int> tableAndId, string column)
    {
        var columnOHWDetail = $"OrderWipDetail{tableAndId.Key}";
        var propInfo = typeof(OrderWipDetail)
            .GetProperties()
            .FirstOrDefault(y => y.Name == columnOHWDetail);
        if (propInfo == null)
        {
            return 0;
        }

        var orderWipDetail = propInfo.GetValue(orderAllWipDetail);

        PropertyInfo propInfo2 = null;
        switch (tableAndId.Key)
        {
            case "ITEM":
                propInfo2 = typeof(OrderWipDetailItem)
                    .GetProperties()
                    .FirstOrDefault(y => y.Name == $"FK_{column}");
                break;
            case "MISC":
                propInfo2 = typeof(OrderWipDetailMisc)
                    .GetProperties()
                    .FirstOrDefault(y => y.Name == $"FK_{column}");
                break;
            case "OUTSOURCED":
                propInfo2 = typeof(OrderWipDetailOutsourced)
                    .GetProperties()
                    .FirstOrDefault(y => y.Name == $"FK_{column}");
                break;
        }
         
        if (propInfo2 == null)
        {
            return 0;
        }

        return propInfo2.GetValue(orderWipDetail) as int? ?? 0;
    }

    private bool DetermineItemIsStock(int itemId)
    {
        return GetRecordset("R_ITEM", "INVENTORYITEM", $"PK_R_ITEM = {itemId}", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<bool>("INVENTORYITEM"))
            .First();
    }

    private int DetermineRegistrationPath(OrderWipDetail orderWipAllDetail, KeyValuePair<string, int> tableAndId)
    {
        var columnOHWDetail = $"orderWipDetail{tableAndId.Key}";
        var propInfo = typeof(OrderWipDetail)
            .GetProperties()
            .FirstOrDefault(y => y.Name == columnOHWDetail);
        if (propInfo == null)
        {
            return 0;
        }

        var orderWipDetail = propInfo.GetValue(orderWipAllDetail);
        
        var column = $"JOBORDERDETAIL{tableAndId.Key}";
        var propInfo2 = typeof(OrderWipDetail)
            .GetProperties()
            .FirstOrDefault(y => y.Name == $"FK_{column}");
        if (propInfo2 == null)
        {
            return 0;
        }

        var id = propInfo2.GetValue(orderWipDetail);
        return GetRecordset($"R_{column}", "REGISTRATIONPATH", $"PK_R_{column} = {id}", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int?>("REGISTRATIONPATH") ?? 0)
            .FirstOrDefault();
    }



    private KeyValuePair<string, int> DetermineOHWTable(OrderWipDetail orderWipDetail)
    {
        var result = new Dictionary<string, int>();
        if ((orderWipDetail.FK_ORDERWIPDETAILITEM as int? ?? 0) > 0)
        {
            result.Add("ITEM", (int)orderWipDetail.FK_ORDERWIPDETAILITEM);
        }
        else if ((orderWipDetail.FK_ORDERWIPDETAILMISC as int? ?? 0) > 0)
        {
            result.Add("MISC", (int)orderWipDetail.FK_ORDERWIPDETAILMISC);
        }
        else if ((orderWipDetail.FK_ORDERWIPDETAILWORKACTIVITY as int? ?? 0) > 0)
        {
            result.Add("WORKACTIVITY", (int)orderWipDetail.FK_ORDERWIPDETAILWORKACTIVITY);
        }
        else if ((orderWipDetail.FK_ORDERWIPDETAILOUTSOURCED as int? ?? 0) > 0)
        {
            result.Add("OUTSOURCED", (int)orderWipDetail.FK_ORDERWIPDETAILOUTSOURCED);
        }

        return result.FirstOrDefault();
    }
   
    private List<Resultaatneming> DetermineResultaatneming(List<int> orderIds, DateTime targetDate)
    {
        return GetRecordset("C_RESULTAATNEMING", "", $"FK_ORDER IN ({string.Join(",", orderIds)}) " +
                                                     $"AND DATE <= #{targetDate.ToString("yyyy-MM-dd")}#", "")
            .As<Resultaatneming>()
            .ToList();
    }

    private List<SalesInvoiceDetail> DetermineSalesInvoiceDetails(List<int> orderIds, DateTime targetDate)
    {
        var salesInvoiceDetails =
            GetRecordset("R_SALESINVOICEALLDETAIL", "", $"FK_ORDER IN ({string.Join(",", orderIds)}) " +
                                                        $"AND (FINANCIALDONEDATE IS NULL OR FINANCIALDONEDATE > #{targetDate.ToString("yyyy-MM-dd")}#)",
                    "")
                .As<SalesInvoiceDetail>()
                .ToList();

        if (!salesInvoiceDetails.Any())
        {
            return salesInvoiceDetails;
        }

        var invoiceIds = salesInvoiceDetails.Select(x => x.FK_SALESINVOICE).ToList();
        var wfStateNew = new Guid("79b4b558-9815-4ff6-8c7f-a789052b75f3");
        var invoiceIdsFiltered = GetRecordset("R_SALESINVOICE", "PK_R_SALESINVOICE",
            $"PK_R_SALESINVOICE IN ({string.Join(",", invoiceIds)}) " +
            $"AND INVOICEDATE <= #{targetDate.ToString("yyyy-MM-dd")}# " +
            $"AND FK_WORKFLOWSTATE != '{wfStateNew}'", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int>("PK_R_SALESINVOICE"))
            .ToList();

        return salesInvoiceDetails
            .Where(y => invoiceIdsFiltered.Contains((int)y.FK_SALESINVOICE)).ToList();
    }

    private List<OrderWipDetail> DetermineOrderWipDetails(List<int> orderIds, DateTime targetDate)
    {
        var orderWipAllDetail = GetRecordset("R_ORDERWIPALLDETAIL", "", $"FK_ORDER IN ({string.Join(",", orderIds)})" +
                                                       $" AND DATEINWIP <= #{targetDate.ToString("yyyy-MM-dd")}#" +
                                                       $" AND ISREVERSED = 0" +
                                                       $" AND FK_REVERSEDORDERWIPDETAILITEM IS NULL" +
                                                       $" AND FK_REVERSEDORDERWIPDETAILMISC IS NULL" +
                                                       $" AND FK_REVERSEDORDERWIPDETAILOUTSOURCED IS NULL" +
                                                       $" AND FK_REVERSEDORDERWIPDETAILWORKACTIVITY IS NULL", "")
            .As<OrderWipDetail>()
            .ToList();

        var orderWipItemIds = orderWipAllDetail
            .Where(y => y.FK_ORDERWIPDETAILITEM.HasValue)
            .Select(x => x.FK_ORDERWIPDETAILITEM)
            .ToList();

        var orderwipDetailItems = new List<OrderWipDetailItem>();
        if (orderWipItemIds.Any())
        {
            orderwipDetailItems = GetRecordset("R_ORDERWIPDETAILITEM", "",
                    $"PK_R_ORDERWIPDETAILITEM IN ({string.Join(",", orderWipItemIds)})", "")
                .As<OrderWipDetailItem>().ToList();
        }
        
        var orderWipMiscIds = orderWipAllDetail
            .Where(y => y.FK_ORDERWIPDETAILMISC.HasValue)
            .Select(x => x.FK_ORDERWIPDETAILMISC)
            .ToList();
        var orderwipDetailMiscs = new List<OrderWipDetailMisc>();
        if (orderWipMiscIds.Any())
        {
            orderwipDetailMiscs = GetRecordset("R_ORDERWIPDETAILMISC", "",
                    $"PK_R_ORDERWIPDETAILMISC IN ({string.Join(",", orderWipMiscIds)})", "")
                .As<OrderWipDetailMisc>().ToList();
        }

        var orderWipOutsourcedIds = orderWipAllDetail
            .Where(y => y.FK_ORDERWIPDETAILOUTSOURCED.HasValue)
            .Select(x => x.FK_ORDERWIPDETAILOUTSOURCED)
            .ToList();
        var orderwipDetailOutsourceds = new List<OrderWipDetailOutsourced>();
        

        if (orderWipOutsourcedIds.Any())
        {
            orderwipDetailOutsourceds = GetRecordset("R_ORDERWIPDETAILOUTSOURCED", "",
                    $"PK_R_ORDERWIPDETAILOUTSOURCED IN ({string.Join(",", orderWipOutsourcedIds)})", "")
                .As<OrderWipDetailOutsourced>().ToList();
        }

        var newListOrderWipAllDetail = new List<OrderWipDetail>();
        foreach (var orderWipDetail in orderWipAllDetail)
        {
            orderWipDetail.TableAndId = DetermineOHWTable(orderWipDetail);

            var add = false;
            while (!add)
            {
                if (orderWipDetail.FK_ORDERWIPDETAILITEM.HasValue)
                {
                    orderWipDetail.OrderWipDetailITEM = orderwipDetailItems.First(x =>
                        x.PK_R_ORDERWIPDETAILITEM == orderWipDetail.FK_ORDERWIPDETAILITEM);
                    
                    add = true;
                    break;
                }

                if (orderWipDetail.FK_ORDERWIPDETAILMISC.HasValue)
                {
                    orderWipDetail.OrderWipDetailMISC = orderwipDetailMiscs.First(x =>
                        x.PK_R_ORDERWIPDETAILMISC == orderWipDetail.FK_ORDERWIPDETAILMISC);

                    add = true;
                        break;
                }

                if (orderWipDetail.FK_ORDERWIPDETAILWORKACTIVITY.HasValue)
                {
                    add = true;
                    break;
                }

                if (orderWipDetail.FK_ORDERWIPDETAILOUTSOURCED.HasValue)
                {
                    orderWipDetail.OrderWipDetailOUTSOURCED = orderwipDetailOutsourceds.First(x =>
                        x.PK_R_ORDERWIPDETAILOUTSOURCED == (int)orderWipDetail.FK_ORDERWIPDETAILOUTSOURCED);
                    add = true;
                    break;
                }

            }


            var orderWipDetailWithPurchaseInfo = DeterminePurchaseInvoiceDetails(orderWipDetail, orderWipDetail.TableAndId);

            if (add)
            {
                newListOrderWipAllDetail.Add(orderWipDetailWithPurchaseInfo);
            }
        }

        newListOrderWipAllDetail = SetPurchaseInvoice(newListOrderWipAllDetail);

        return newListOrderWipAllDetail;
    }

    private List<OrderWipDetail> SetPurchaseInvoice(List<OrderWipDetail> newListOrderWipAllDetail)
    {
        var orderWipAllDetails = newListOrderWipAllDetail;
        var purchaseInvoiceIds = newListOrderWipAllDetail
            .Where(y => y.PurchaseInvoiceId > 0)
            .Select(x => x.PurchaseInvoiceId)
            .ToList()
            .Distinct();

        if (!purchaseInvoiceIds.Any())
        {
            return orderWipAllDetails;
        }

        var purchaseInvoices = GetRecordset($"R_PURCHASEINVOICE", "",
                $"PK_R_PURCHASEINVOICE IN ({string.Join(",", purchaseInvoiceIds)})", "")
            .As<PurchaseInvoice>()
            .ToList();

        foreach (var orderWipAllDetail in orderWipAllDetails)
        {
            orderWipAllDetail.PurchaseInvoice = purchaseInvoices
                .FirstOrDefault(y => y.PK_R_PURCHASEINVOICE == orderWipAllDetail.PurchaseInvoiceId);
        }

        return orderWipAllDetails;
    }

    private DateTime DetermineTargetDate()
    {
        var sdkDefaultValues = new SDKDefaultValues();

        var now = DateTime.Now;
        var firstDayCurrentMonth = new DateTime(now.Year, now.Month,1);
        var lastDayLastMonth = firstDayCurrentMonth.AddDays(-1);
        
        sdkDefaultValues.AddColumn("REF_DATE", lastDayLastMonth);

        var editPar = new EditParameters() {TableName = "C_ORDERFINANCIALSTATE", Context = "SELECTDATE", SdkDefaultValues = sdkDefaultValues};
        var id = OpenEdit(editPar, true);

        if (id == null)
        {
            return DateTime.MinValue;;
        }


        var rsFilledOrderFinancialState = GetRecordset("C_ORDERFINANCIALSTATE", "PK_C_ORDERFINANCIALSTATE, REF_DATE",
            $"PK_C_ORDERFINANCIALSTATE = {id}", "");

        var date = rsFilledOrderFinancialState
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<DateTime>("REF_DATE"))
            .First();

        rsFilledOrderFinancialState.MoveFirst();
        rsFilledOrderFinancialState.Delete();

        return date;
    }
}
