namespace AddOrdersToOrderFinancialState.Models
{
    using System;
    using System.ComponentModel;
    
    
    public enum WkaState : int
    {
        
        /// <summary>
        /// N.v.t.
        /// </summary>
        [Description("N.v.t.")]
        N_v_t_ = 1,
        
        /// <summary>
        /// Nog te controleren
        /// </summary>
        [Description("Nog te controleren")]
        Nog_te_controleren = 2,
        
        /// <summary>
        /// Akkoord
        /// </summary>
        [Description("Akkoord")]
        Akkoord = 3,
        
        /// <summary>
        /// Niet akkoord
        /// </summary>
        [Description("Niet akkoord")]
        Niet_akkoord = 4,
    }
}
