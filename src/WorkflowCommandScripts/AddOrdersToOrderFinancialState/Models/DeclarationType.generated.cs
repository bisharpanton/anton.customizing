namespace AddOrdersToOrderFinancialState.Models
{
    using System;
    using System.ComponentModel;
    
    
    public enum DeclarationType : int
    {
        
        /// <summary>
        /// N.v.t.
        /// </summary>
        [Description("N.v.t.")]
        N_v_t_ = 1,
        
        /// <summary>
        /// Parkeergelden
        /// </summary>
        [Description("Parkeergelden")]
        Parkeergelden = 2,
        
        /// <summary>
        /// Eten
        /// </summary>
        [Description("Eten")]
        Eten = 3,
        
        /// <summary>
        /// Overig
        /// </summary>
        [Description("Overig")]
        Overig = 4,
        
        /// <summary>
        /// Felicitatie / ziekte/ oid
        /// </summary>
        [Description("Felicitatie / ziekte/ oid")]
        Felicitatie___ziekte__oid = 5,
    }
}
