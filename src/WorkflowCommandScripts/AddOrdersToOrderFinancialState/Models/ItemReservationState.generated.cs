namespace AddOrdersToOrderFinancialState.Models
{
    using System;
    using System.ComponentModel;
    
    
    public enum ItemReservationState : int
    {
        
        /// <summary>
        /// New
        /// </summary>
        [Description("New")]
        New = 1,
        
        /// <summary>
        /// Done
        /// </summary>
        [Description("Done")]
        Done = 2,
        
        /// <summary>
        /// Direct to order
        /// </summary>
        [Description("Direct to order")]
        Direct_to_order = 3,
    }
}
