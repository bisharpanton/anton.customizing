namespace AddOrdersToOrderFinancialState.Models
{
    using System;
    using System.ComponentModel;
    
    
    public enum PurchaseInvoiceReceiptState : int
    {
        
        /// <summary>
        /// Link goods receipts
        /// </summary>
        [Description("Link goods receipts")]
        Link_goods_receipts = 1,
        
        /// <summary>
        /// Journalize goods receipts
        /// </summary>
        [Description("Journalize goods receipts")]
        Journalize_goods_receipts = 2,
        
        /// <summary>
        /// Approve invoice
        /// </summary>
        [Description("Approve invoice")]
        Approve_invoice = 3,
        
        /// <summary>
        /// Done
        /// </summary>
        [Description("Done")]
        Done = 4,
    }
}
