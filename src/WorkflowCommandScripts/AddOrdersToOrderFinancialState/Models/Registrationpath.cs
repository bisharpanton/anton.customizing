﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddOrdersToOrderFinancialState.Models
{
    public enum Registrationpath : int
    {
        ManualWipEntry = 1,
        NormativeWipEntry = 2,
        PurchaseWhenNeeded = 3,
        AutomaticInventory = 4,
        ManualStockToWip = 5,
        OnlyAutomaticStockToWip= 6,
        ManualInventory= 7,
        DirectWipManualStock = 8
    }
 
}
