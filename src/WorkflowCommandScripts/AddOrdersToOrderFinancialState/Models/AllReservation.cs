﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ridder.Common;

namespace AddOrdersToOrderFinancialState.Models
{
    partial class AllReservation
    {
        public PurchaseInvoice PurchaseInvoice { get; set; }
        public double? PurchaseOrderAmount { get; set; }

    }
}
