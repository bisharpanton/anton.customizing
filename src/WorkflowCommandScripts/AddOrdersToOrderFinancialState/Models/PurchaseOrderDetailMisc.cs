﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddOrdersToOrderFinancialState.Models
{
    partial class PurchaseOrderDetailMisc
    {

        public double QuantityInvoiced { get; set; }
        public DateTime PurchaseInvoiceDate { get; set; }
    }
}
