namespace AddOrdersToOrderFinancialState.Models
{
    using System;
    using System.ComponentModel;
    
    
    public enum ReservationDestinationType : int
    {
        
        /// <summary>
        /// Job order detail item
        /// </summary>
        [Description("Job order detail item")]
        Job_order_detail_item = 1,
        
        /// <summary>
        /// Stock out
        /// </summary>
        [Description("Stock out")]
        Stock_out = 2,
        
        /// <summary>
        /// Job order detail miscellaneous
        /// </summary>
        [Description("Job order detail miscellaneous")]
        Job_order_detail_miscellaneous = 3,
        
        /// <summary>
        /// Job order detail outsourced
        /// </summary>
        [Description("Job order detail outsourced")]
        Job_order_detail_outsourced = 4,
        
        /// <summary>
        /// Sales order detail item
        /// </summary>
        [Description("Sales order detail item")]
        Sales_order_detail_item = 5,
        
        /// <summary>
        /// Sales order detail miscellaneous
        /// </summary>
        [Description("Sales order detail miscellaneous")]
        Sales_order_detail_miscellaneous = 6,
        
        /// <summary>
        /// Sales order detail outsourced
        /// </summary>
        [Description("Sales order detail outsourced")]
        Sales_order_detail_outsourced = 7,
        
        /// <summary>
        /// Return purchase order detail item
        /// </summary>
        [Description("Return purchase order detail item")]
        Return_purchase_order_detail_item = 8,
        
        /// <summary>
        /// Warehouse transfer supplying
        /// </summary>
        [Description("Warehouse transfer supplying")]
        Warehouse_transfer_supplying = 9,
        
        /// <summary>
        /// Negative goods receipt detail item
        /// </summary>
        [Description("Negative goods receipt detail item")]
        Negative_goods_receipt_detail_item = 10,
        
        /// <summary>
        /// Shipping order detail item
        /// </summary>
        [Description("Shipping order detail item")]
        Shipping_order_detail_item = 11,
        
        /// <summary>
        /// Noted need
        /// </summary>
        [Description("Noted need")]
        Noted_need = 12,
        
        /// <summary>
        /// Unknown
        /// </summary>
        [Description("Unknown")]
        Unknown = 13,
        
        /// <summary>
        /// Warehouse
        /// </summary>
        [Description("Warehouse")]
        Warehouse = 14,
        
        /// <summary>
        /// Order
        /// </summary>
        [Description("Order")]
        Order = 15,
        
        /// <summary>
        /// Job order
        /// </summary>
        [Description("Job order")]
        Job_order = 16,
    }
}
