﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ridder.Common.ProductionManagement;
using Ridder.Common.PurchaseAdministration;

namespace AddOrdersToOrderFinancialState.Models
{
    partial class OrderWipDetail
    {
        public OrderWipDetailItem OrderWipDetailITEM { get; set; }
        public PurchaseInvoiceDetailItem PurchaseInvoiceDetailItem { get; set; }
        public OrderWipDetailMisc OrderWipDetailMISC { get; set; }
        public PurchaseInvoiceDetailMisc PurchaseInvoiceDetailMisc { get; set; }
        public OrderWipDetailOutsourced OrderWipDetailOUTSOURCED { get; set; }
        public PurchaseInvoiceDetailOutsourced PurchaseInvoiceDetailOutsourced { get; set; }
        public int PurchaseInvoiceId { get; set; }
        public PurchaseInvoice PurchaseInvoice { get; set; }
        public KeyValuePair<string, int> TableAndId { get; set; }
    }
}
