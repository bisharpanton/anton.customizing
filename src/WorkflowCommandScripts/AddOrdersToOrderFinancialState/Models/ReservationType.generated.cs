namespace AddOrdersToOrderFinancialState.Models
{
    using System;
    using System.ComponentModel;
    
    
    public enum ReservationType : int
    {
        
        /// <summary>
        /// Item
        /// </summary>
        [Description("Item")]
        Item = 1,
        
        /// <summary>
        /// Miscellaneous
        /// </summary>
        [Description("Miscellaneous")]
        Miscellaneous = 2,
        
        /// <summary>
        /// Outsourced activity
        /// </summary>
        [Description("Outsourced activity")]
        Outsourced_activity = 3,
    }
}
