namespace AddOrdersToOrderFinancialState.Models
{
    using System;
    using System.ComponentModel;
    
    
    public enum ReservationSourceType : int
    {
        
        /// <summary>
        /// Stock in
        /// </summary>
        [Description("Stock in")]
        Stock_in = 1,
        
        /// <summary>
        /// Purchase order detail item
        /// </summary>
        [Description("Purchase order detail item")]
        Purchase_order_detail_item = 2,
        
        /// <summary>
        /// Purchaseoffer detail item
        /// </summary>
        [Description("Purchaseoffer detail item")]
        Purchaseoffer_detail_item = 3,
        
        /// <summary>
        /// Warehouse transfer destination
        /// </summary>
        [Description("Warehouse transfer destination")]
        Warehouse_transfer_destination = 4,
        
        /// <summary>
        /// Return shippingorder detail item
        /// </summary>
        [Description("Return shippingorder detail item")]
        Return_shippingorder_detail_item = 5,
        
        /// <summary>
        /// Return sales order detail item
        /// </summary>
        [Description("Return sales order detail item")]
        Return_sales_order_detail_item = 6,
        
        /// <summary>
        /// Return job order detail item
        /// </summary>
        [Description("Return job order detail item")]
        Return_job_order_detail_item = 7,
        
        /// <summary>
        /// Purchase order detail miscellaneous
        /// </summary>
        [Description("Purchase order detail miscellaneous")]
        Purchase_order_detail_miscellaneous = 8,
        
        /// <summary>
        /// Purchase order detail outsourced
        /// </summary>
        [Description("Purchase order detail outsourced")]
        Purchase_order_detail_outsourced = 9,
        
        /// <summary>
        /// Purchaseoffer detail miscellaneous
        /// </summary>
        [Description("Purchaseoffer detail miscellaneous")]
        Purchaseoffer_detail_miscellaneous = 10,
        
        /// <summary>
        /// Purchaseoffer detail outsourced
        /// </summary>
        [Description("Purchaseoffer detail outsourced")]
        Purchaseoffer_detail_outsourced = 11,
        
        /// <summary>
        /// Goods receipt detail item
        /// </summary>
        [Description("Goods receipt detail item")]
        Goods_receipt_detail_item = 12,
        
        /// <summary>
        /// Goods receipt detail miscellaneous
        /// </summary>
        [Description("Goods receipt detail miscellaneous")]
        Goods_receipt_detail_miscellaneous = 13,
        
        /// <summary>
        /// Goods receipt detail outsourced
        /// </summary>
        [Description("Goods receipt detail outsourced")]
        Goods_receipt_detail_outsourced = 14,
        
        /// <summary>
        /// Internal job order
        /// </summary>
        [Description("Internal job order")]
        Internal_job_order = 15,
        
        /// <summary>
        /// Purchase invoice detail item
        /// </summary>
        [Description("Purchase invoice detail item")]
        Purchase_invoice_detail_item = 16,
        
        /// <summary>
        /// Purchase invoice detail miscellaneous
        /// </summary>
        [Description("Purchase invoice detail miscellaneous")]
        Purchase_invoice_detail_miscellaneous = 17,
        
        /// <summary>
        /// Purchase invoice detail outsourced
        /// </summary>
        [Description("Purchase invoice detail outsourced")]
        Purchase_invoice_detail_outsourced = 18,
    }
}
