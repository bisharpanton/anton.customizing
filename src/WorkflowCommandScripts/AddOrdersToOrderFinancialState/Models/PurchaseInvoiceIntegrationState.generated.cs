namespace AddOrdersToOrderFinancialState.Models
{
    using System;
    using System.ComponentModel;
    
    
    public enum PurchaseInvoiceIntegrationState : int
    {
        
        /// <summary>
        /// New
        /// </summary>
        [Description("New")]
        New = 1,
        
        /// <summary>
        /// Message processed
        /// </summary>
        [Description("Message processed")]
        Message_processed = 2,
        
        /// <summary>
        /// Next message not processed
        /// </summary>
        [Description("Next message not processed")]
        Next_message_not_processed = 3,
    }
}
