namespace AddOrdersToOrderFinancialState.Models
{
    using System;
    using System.ComponentModel;
    
    
    public enum SawingCode : int
    {
        
        /// <summary>
        /// None
        /// </summary>
        [Description("None")]
        None = 1,
        
        /// <summary>
        /// No mitre
        /// </summary>
        [Description("No mitre")]
        No_mitre = 2,
        
        /// <summary>
        /// One * mitre
        /// </summary>
        [Description("One * mitre")]
        One___mitre = 3,
        
        /// <summary>
        /// Two * mitre (same angle)
        /// </summary>
        [Description("Two * mitre (same angle)")]
        Two___mitre__same_angle_ = 4,
        
        /// <summary>
        /// Two * mitre (different angle)
        /// </summary>
        [Description("Two * mitre (different angle)")]
        Two___mitre__different_angle_ = 5,
    }
}
