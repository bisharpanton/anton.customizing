﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

public class CreateBisharpSalesFunnelAfterConnection_RidderScript : WorkflowCommandScript
{
    public void Execute()
    {
        if (TableName == null || TableName != "R_TODO")
        {
            MessageBox.Show("Dit script werkt alleen op de tabel 'Taken'.", "Ridder iQ", MessageBoxButtons.OK,
                MessageBoxIcon.Error);
            return;
        }

        if (RecordIds == null || !RecordIds.Any())
        {
            return;
        }

        var todoId = (int)RecordIds[0];

        var todo = GetRecordset("R_TODO", "FK_ACTIONSCOPE, FK_CONTACT, FK_RELATION, ISCONNECTIONTODO",
            $"PK_R_TODO = {todoId}", "")
            .DataTable.AsEnumerable().Select(x => new Todo()
            {
                ActionScopeId = x.Field<int>("FK_ACTIONSCOPE"),
                ContactId = x.Field<int?>("FK_CONTACT") ?? 0,
                RelationId = x.Field<int?>("FK_RELATION") ?? 0,
                IsConnectionTodo = x.Field<bool>("ISCONNECTIONTODO")

            }).First();

        if(todo.IsConnectionTodo == false)
        {
            return;
        }

        if(todo.RelationId == 0 || todo.ContactId == 0)
        {
            throw new Exception("Relatie of contactpersoon ontbreekt in deze taak");
        }

        var currentUserId = GetUserInfo().CurrentUserId;

        var currentEmployeeId = GetRecordset("R_USER", "FK_EMPLOYEE",
            $"PK_R_USER = {currentUserId}", "").DataTable.AsEnumerable().First().Field<int>("FK_EMPLOYEE");

        var formatTodos = GetRecordset("U_BISHARPTODOFORMAT", "DESCRIPTION, MEMO, FK_TODOTYPE, NUMBEROFDAYSFROMSTART, ASSIGNTOCOLLEGUE, ISCONNECTIONTODO",
                      "CREATEAFTERCONNECTION = 1", "")
                  .DataTable.AsEnumerable().Select(x => new FormatTodo()
                  {
                      FormatId = x.Field<int>("PK_U_BISHARPTODOFORMAT"),
                      Description = x.Field<string>("DESCRIPTION"),
                      Memo = x.Field<string>("MEMO"),
                      TodoType = x.Field<int?>("FK_TODOTYPE") ?? 0,
                      NumberOfDays = x.Field<int>("NUMBEROFDAYSFROMSTART"),
                      AssignToCollegue = x.Field<bool>("ASSIGNTOCOLLEGUE")

                  }).ToList();

        var rsTodo = GetRecordset("R_TODO", "", "PK_R_TODO = -1", "");
        rsTodo.MoveFirst();
        rsTodo.UseDataChanges = false;
        rsTodo.UpdateWhenMoveRecord = false;

        foreach (FormatTodo formatTodo in formatTodos)
        {
            rsTodo.AddNew();
            rsTodo.Fields["FK_RELATION"].Value = todo.RelationId;
            rsTodo.Fields["FK_ACTIONSCOPE"].Value = todo.ActionScopeId;
            rsTodo.Fields["DESCRIPTION"].Value = formatTodo.Description;
            rsTodo.Fields["INTERNALMEMO"].Value = formatTodo.Memo;
            rsTodo.Fields["FK_TODOTYPE"].Value = formatTodo.TodoType;
            rsTodo.Fields["DUEDATE"].Value = DateTime.Now.AddDays(formatTodo.NumberOfDays);
            rsTodo.Fields["FK_CONTACT"].Value = todo.ContactId;

            if (formatTodo.AssignToCollegue == false)
            {
                rsTodo.Fields["FK_ASSIGNEDTO"].Value = currentEmployeeId;
            }
            else
            {
                MessageBox.Show($"De taak '{formatTodo.Description}' moet worden toegewezen aan een collega. Selecteer in de volgende pop-up je collega.", "Bisharp SalesFunnel® Generator", MessageBoxButtons.OK, MessageBoxIcon.Information);

                var resultCollegue = OpenBrowseFormWithFilter("R_EMPLOYEE", "PK_R_EMPLOYEE",
                    "NIETDECLARABEL = 0", true);

                if (resultCollegue == null)
                {
                    return;
                }

                rsTodo.Fields["FK_ASSIGNEDTO"].Value = resultCollegue;
            }
        }

        var updateResult = rsTodo.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            throw new Exception($"Het aanmaken van de sales funnel taken is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}");
        }

        MessageBox.Show($"Bisharp SalesFunnel® Configuratie afgerond. Rammen maar!", "Bisharp SalesFunnel® Generator", MessageBoxButtons.OK, MessageBoxIcon.Information);
    }

    class Todo
    {
        public int ActionScopeId { get; set; }
        public int? ContactId { get; set; }
        public int? RelationId { get; set; }
        public bool IsConnectionTodo { get; set; }
    }
    class FormatTodo
    {
        public int FormatId { get; set; }
        public string Description { get; set; }
        public string Memo { get; set; }
        public int? TodoType { get; set; }
        public int NumberOfDays { get; set; }
        public bool AssignToCollegue { get; set; }
    }


}

