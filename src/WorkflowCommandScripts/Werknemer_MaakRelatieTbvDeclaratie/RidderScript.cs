﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Drawing;
using Ridder.Client.SDK.SDKParameters;
using Ridder.Common.CRM.Parameters;
using Ridder.Common.ItemManagement;
using Ridder.Recordset.Extensions;
using System.Threading;

public class Werknemer_MaakRelatieTbvDeclaratie_RidderScript : WorkflowCommandScript
{
    public void Execute()
    {
        //DB
        //12-5-2020
        //Maak van de werknemer een relatie tbv verwerking declaraties

        if (TableName == null || TableName != "R_EMPLOYEE")
        {
            MessageBox.Show("Dit script mag alleen vanaf de werknemer aangeroepen worden.", "Ridder iQ",
                MessageBoxButtons.OK, MessageBoxIcon.Information);

            return;
        }

        var employeeId = RecordIds?.FirstOrDefault();

        if (employeeId == null)
        {
            MessageBox.Show("Werknemer kan niet bepaald worden.");
            return;
        }

        var wfCreateDeclarationRelation = new Guid("0c808ff2-5052-41a1-aed4-8f84b6f968ee");
        var wfResult = ExecuteWorkflowEvent("R_EMPLOYEE", (int)employeeId, wfCreateDeclarationRelation, null);

        if(wfResult.HasError)
        {
            MessageBox.Show(
               $"Genereren relatie is mislukt, oorzaak: {wfResult.GetResult()}",
               "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);

            return;
        }

        var createdRelation = GetRecordset("R_EMPLOYEE", "FK_PERSON, FK_RELATION",
            $"PK_R_EMPLOYEE = {employeeId}", "").DataTable.AsEnumerable().First().Field<int?>("FK_RELATION") ?? 0;

        if (createdRelation == 0)
        {
            MessageBox.Show(
               $"Geen relatie gevonden bij de werknemer.",
               "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);

            return;
        }

        OpenRelation(createdRelation);
    }


    private void OpenRelation(int createdRelation)
    {
        var editPar = new EditParameters() { TableName = "R_RELATION", Id = createdRelation, };

        OpenEdit(editPar, false);
    }

}
