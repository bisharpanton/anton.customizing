﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using CreatePurchaesorderFromPurchaseContract.Models;
using Ridder.Client.SDK.SDKParameters;
using Ridder.Recordset.Extensions;

public class CreatePurchaesorderFromPurchaseContract_RidderScript : WorkflowCommandScript
{
    public void Execute()
    {
        var purchaseContractId = (int)RecordIds.First();
        var purchaseContract = GetRecordset("C_PURCHASECONTRACT", "",
                $"PK_C_PURCHASECONTRACT = {purchaseContractId}", "")
            .As<PurchaseContract>()
            .First();

        var supplierId = purchaseContract.FK_SUPPLIER;
        var wka = DetermineWKA(purchaseContract.FK_CONTRACTTYPE);
        var description = purchaseContract.DESCRIPTION;

        var rsPurchaseOrder = GetRecordset("R_PURCHASEORDER", "",
            $"PK_R_PURCHASEORDER IS NULL", "");
        rsPurchaseOrder.UseDataChanges = true;

        rsPurchaseOrder.AddNew();
        rsPurchaseOrder.SetFieldValue("FK_SUPPLIER", supplierId);
        rsPurchaseOrder.SetFieldValue("WKA", wka);
        rsPurchaseOrder.SetFieldValue("REFERENCESUPPLIER", description);
        rsPurchaseOrder.SetFieldValue("FK_DELIVERYRELATION", GetOwnCompany());
        rsPurchaseOrder.Update();

        rsPurchaseOrder.MoveFirst();
        var purchaseorderId = (int)rsPurchaseOrder.Fields["PK_R_PURCHASEORDER"].Value;

        var editPar = new EditParameters()
        {
            TableName = "R_PURCHASEORDER", 
            Id = purchaseorderId
        };

        OpenEdit(editPar, false);
    }
    private int GetOwnCompany()
    {
        return GetRecordset("R_CRMSETTINGS", "FK_OWNCOMPANY", "", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int>("FK_OWNCOMPANY"))
            .First();

    }
    private bool DetermineWKA(int? contractTypeId)
    {
        return GetRecordset("C_CONTRACTTYPE", "WKA", $"PK_C_CONTRACTTYPE = {contractTypeId}", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<bool>("WKA"))
            .FirstOrDefault();


    }
}
