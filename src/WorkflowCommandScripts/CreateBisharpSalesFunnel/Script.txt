﻿/*
 * 7/25/2022 1:39:15 PM
 * Stefan Putman
 * Created CreateBisharpSalesFunnel
 * 
 * Changelog: 
 *   
 * 
 * References:
 * 
 *   Newtonsoft.Json (C:\Users\Stefan Putman\.nuget\packages\newtonsoft.json\12.0.1\lib\net45\Newtonsoft.Json.dll)
 *   Ridder.iQ.Common.Recordset.Extensions (C:\Users\Stefan Putman\.nuget\packages\ridder.clientdevelopment\1.8.251.16\lib\Ridder.iQ.Common.Recordset.Extensions.dll)
 * 
 * Generated on: 2022-10-19 17:15 by Stefan Putman
 *   
 */

/*
 * RidderScript.cs
 */
using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using Ridder.Client.SDK.SDKParameters;

public class CreateBisharpSalesFunnel_RidderScript : WorkflowCommandScript
{
    private System.Windows.Forms.Form Form1;
    private System.Windows.Forms.DateTimePicker dateTimePicker1;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.Label label1;

    private int _objectId;
    IFormDataAwareFunctions ridderForm;

    int relationId = 0;
    int createdActionscope = 0;
    int currentEmployeeId = 0;

    public void Execute()
    {
        if (TableName == null || TableName != "R_RELATION")
        {
            MessageBox.Show("Dit script werkt alleen op de tabel 'Relaties'.", "Ridder iQ", MessageBoxButtons.OK,
                MessageBoxIcon.Error);
            return;
        }

        if (RecordIds == null || !RecordIds.Any())
        {
            return;
        }

        relationId = (int)RecordIds[0];

        var rsContact = GetRecordset("R_CONTACT", "PK_R_CONTACT",
            $"FK_RELATION = {relationId}", "");

        if (rsContact.RecordCount == 0)
        {
            MessageBox.Show("Geen contactpersonen gevonden onder deze relatie. Maak eerst een contactpersoon aan.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        var currentUserId = GetUserInfo().CurrentUserId;

        currentEmployeeId = GetRecordset("R_USER", "FK_EMPLOYEE",
            $"PK_R_USER = {currentUserId}", "").DataTable.AsEnumerable().First().Field<int>("FK_EMPLOYEE");

        createdActionscope = CreateActionScope(relationId);

        MessageBox.Show("Selecteer in de volgende pop-up de startdatum.", "Bisharp SalesFunnel® Generator", MessageBoxButtons.OK, MessageBoxIcon.Information);

        DateTime currentDate = DateTime.Now;

        InitializeComponent(currentDate);
    }

    private void button1_Click(object sender, EventArgs e)
    {
        var startDate = (DateTime)dateTimePicker1.Value;

        Form1.Close();

        var formatTodos = GetRecordset("U_BISHARPTODOFORMAT", "DESCRIPTION, MEMO, FK_TODOTYPE, NUMBEROFDAYSFROMSTART, ASSIGNTOCOLLEGUE, ISCONNECTIONTODO",
               "CREATEAFTERCONNECTION = 0", "")
           .DataTable.AsEnumerable().Select(x => new FormatTodo()
           {
               FormatId = x.Field<int>("PK_U_BISHARPTODOFORMAT"),
               Description = x.Field<string>("DESCRIPTION"),
               Memo = x.Field<string>("MEMO"),
               TodoType = x.Field<int?>("FK_TODOTYPE") ?? 0,
               NumberOfDays = x.Field<int>("NUMBEROFDAYSFROMSTART"),
               AssignToCollegue = x.Field<bool>("ASSIGNTOCOLLEGUE"),
               IsConnectionTodo = x.Field<bool>("ISCONNECTIONTODO")

           }).ToList();

        var contactIds = GetRecordset("R_CONTACT", "PK_R_CONTACT",
            $"FK_RELATION = {relationId}", "").DataTable.AsEnumerable().Select(x => x.Field<int>("PK_R_CONTACT")).ToList();

        foreach (var contactId in contactIds)
        {
            var rsTodo = GetRecordset("R_TODO", "", "PK_R_TODO = -1", "");
            rsTodo.MoveFirst();
            rsTodo.UseDataChanges = false;
            rsTodo.UpdateWhenMoveRecord = false;

            foreach (FormatTodo formatTodo in formatTodos)
            {
                rsTodo.AddNew();
                rsTodo.Fields["FK_RELATION"].Value = relationId;
                rsTodo.Fields["FK_ACTIONSCOPE"].Value = createdActionscope;
                rsTodo.Fields["DESCRIPTION"].Value = formatTodo.Description;
                rsTodo.Fields["INTERNALMEMO"].Value = formatTodo.Memo;
                rsTodo.Fields["FK_TODOTYPE"].Value = formatTodo.TodoType;
                rsTodo.Fields["DUEDATE"].Value = (DateTime)startDate.AddDays(formatTodo.NumberOfDays);
                rsTodo.Fields["FK_CONTACT"].Value = contactId;
                rsTodo.Fields["ISCONNECTIONTODO"].Value = formatTodo.IsConnectionTodo;

                if (formatTodo.AssignToCollegue == false)
                {
                    rsTodo.Fields["FK_ASSIGNEDTO"].Value = currentEmployeeId;
                }
                else
                {
                    MessageBox.Show($"De taak '{formatTodo.Description}' moet worden toegewezen aan een collega. Selecteer in de volgende pop-up je collega.", "Bisharp SalesFunnel® Generator", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    var resultCollegue = OpenBrowseFormWithFilter("R_EMPLOYEE", "PK_R_EMPLOYEE",
                        "NIETDECLARABEL = 0", true);

                    if (resultCollegue == null)
                    {
                        return;
                    }

                    rsTodo.Fields["FK_ASSIGNEDTO"].Value = resultCollegue;
                }
            }
            var updateResult = rsTodo.Update2();

            if (updateResult.Any(x => x.HasError))
            {
                MessageBox.Show($"Het aanmaken van de sales funnel taken is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        MessageBox.Show($"Bisharp SalesFunnel® Configuratie afgerond. Rammen maar!", "Bisharp SalesFunnel® Generator", MessageBoxButtons.OK, MessageBoxIcon.Information);
    }

    private int CreateActionScope(object relationId)
    {
        var rsActionScope = GetRecordset("R_ACTIONSCOPE", "", "PK_R_ACTIONSCOPE = -1", "");
        rsActionScope.MoveFirst();
        rsActionScope.UseDataChanges = true;
        rsActionScope.UpdateWhenMoveRecord = false;

        rsActionScope.AddNew();
        rsActionScope.Fields["FK_RELATION"].Value = relationId;
        rsActionScope.Fields["DESCRIPTION"].Value = $"Sales funnel {GetRecordTag("R_RELATION", relationId)}";

        var updateResult = rsActionScope.Update2();

        if (updateResult.Any(x => x.HasError))
        {
            MessageBox.Show($"Het aanmaken van een actiekader is mislukt, oorzaak: {updateResult.First(x => x.HasError).GetResult()}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        return (int)updateResult.First().PrimaryKey;
    }

    class FormatTodo
    {
        public int FormatId { get; set; }
        public string Description { get; set; }
        public string Memo { get; set; }
        public int? TodoType { get; set; }
        public int NumberOfDays { get; set; }
        public bool AssignToCollegue { get; set; }
        public bool IsConnectionTodo { get; set; }
    }

    private void InitializeComponent(DateTime currentDate)
    {
        Form1 = new System.Windows.Forms.Form();
        dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
        button1 = new System.Windows.Forms.Button();
        label1 = new System.Windows.Forms.Label();
        Form1.SuspendLayout();
        // 
        // dateTimePicker1
        // 
        dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
        dateTimePicker1.Location = new System.Drawing.Point(161, 20);
        dateTimePicker1.Name = "dateTimePicker1";
        dateTimePicker1.Size = new System.Drawing.Size(125, 20);
        dateTimePicker1.TabIndex = 0;
        dateTimePicker1.Value = currentDate;
        // 
        // button1
        // 
        button1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular,
            System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        button1.Location = new System.Drawing.Point(161, 68);
        button1.Name = "button1";
        button1.Size = new System.Drawing.Size(125, 23);
        button1.TabIndex = 1;
        button1.Text = "Opslaan";
        button1.UseVisualStyleBackColor = true;
        button1.Click += new System.EventHandler(this.button1_Click);
        // 
        // label1
        // 
        label1.AutoSize = true;
        label1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular,
            System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        label1.Location = new System.Drawing.Point(12, 20);
        label1.Name = "label1";
        label1.Size = new System.Drawing.Size(122, 14);
        label1.TabIndex = 2;
        label1.Text = "Datum";
        // 
        // Form1
        // 
        Form1.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        Form1.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        Form1.ClientSize = new System.Drawing.Size(328, 122);
        Form1.Controls.Add(this.label1);
        Form1.Controls.Add(this.button1);
        Form1.Controls.Add(this.dateTimePicker1);
        Form1.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
        Form1.StartPosition = FormStartPosition.CenterScreen;
        Form1.Name = "Form1";
        Form1.Text = "Kies datum";
        Form1.ResumeLayout(false);
        Form1.PerformLayout();

        Form1.Show();
    }
}

