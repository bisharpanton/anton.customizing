﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using Ridder.Client.SDK.SDKParameters;

public class CloseProjecttimeEmployeeWeekCommand_RidderScript : WorkflowCommandScript
{
	public void Execute()
	{
        if (TableName == null || TableName != "U_PROJECTTIMEEMPLOYEEWEEK")
        {
            MessageBox.Show("Dit script werkt alleen op de tabel 'Uren werknemer per week'.", "Ridder iQ", MessageBoxButtons.OK,
                MessageBoxIcon.Error);
            return;
        }

        if (RecordIds == null || !RecordIds.Any())
        {
            return;
        }

        var id = (int)RecordIds[0];

        var rsProjectTime = GetRecordset("R_PROJECTTIME", "PK_R_PROJECTTIME, CLOSED, FK_OVERTIMECODE, TIMEEMPLOYEE",
            $"FK_PROJECTTIMEEMPLOYEEWEEK = {id}", "");

        var sumTotalHours = rsProjectTime.DataTable.AsEnumerable().Sum(x => x.Field<long>("TIMEEMPLOYEE"));

        var hours = TimeSpan.FromTicks(sumTotalHours).TotalHours;
        var minutes = TimeSpan.FromTicks(sumTotalHours).TotalMinutes % 60;

        var sumOvertime = rsProjectTime.DataTable.AsEnumerable().Where(y => (y.Field<int?>("FK_OVERTIMECODE") ?? 0) != 0).Sum(x => x.Field<long>("TIMEEMPLOYEE"));

        var neededHours = GetRecordset("U_PROJECTTIMEEMPLOYEEDAY", "TOTALHOURSNEEDED",
            $"FK_PROJECTTIMEEMPLOYEEWEEK = {id}", "").DataTable.AsEnumerable().Sum(x => x.Field<long>("TOTALHOURSNEEDED"));

        if(sumTotalHours < neededHours)
        {
            MessageBox.Show($"Minder uren geboekt ({Math.Floor(TimeSpan.FromTicks(sumTotalHours).TotalMinutes / 60)}u {(TimeSpan.FromTicks(sumTotalHours).TotalMinutes % 60).ToString("00")}m) " +
                $"dan benodigd ({Math.Floor(TimeSpan.FromTicks(neededHours).TotalMinutes / 60)}u {(TimeSpan.FromTicks(neededHours).TotalMinutes % 60).ToString("00")}m). " +
                $"Afsluiten niet mogelijk.", "Ridder iQ", MessageBoxButtons.OK,
                MessageBoxIcon.Error);
            return;
        }

        if (sumOvertime > 0)
        {
            var result = MessageBox.Show($"Er zijn overuren geboekt ({ Math.Floor(TimeSpan.FromTicks(sumOvertime).TotalMinutes / 60)}u {(TimeSpan.FromTicks(sumOvertime).TotalMinutes % 60).ToString("00")}m). " +
                $"Weet je zeker dat je deze week wil afsluiten?", "Ridder iQ", MessageBoxButtons.OKCancel,
                MessageBoxIcon.Warning);

            if (result == DialogResult.Cancel)
            {
                return;
            }
        }
        else
        {
            var result = MessageBox.Show($"Weet je zeker dat je deze week wil afsluiten?", "Ridder iQ", MessageBoxButtons.OKCancel,
                MessageBoxIcon.Warning);

            if (result == DialogResult.Cancel)
            {
                return;
            }
        }


        var rsProjectTimesToClose = GetRecordset("R_PROJECTTIME", "PK_R_PROJECTTIME, CLOSED, FK_OVERTIMECODE, TIMEEMPLOYEE",
            $"FK_PROJECTTIMEEMPLOYEEWEEK = {id} AND CLOSED = 0", "");

        if (rsProjectTimesToClose.RecordCount > 0)
        {
            CloseOpenProjecttimes(rsProjectTimesToClose);
        }

        UpdateCloseInfo(id);
    }

    private void UpdateCloseInfo(int id)
    {
        var rsProjecttimeEmployeeWeek = GetRecordset("U_PROJECTTIMEEMPLOYEEWEEK", "DATECLOSED, CLOSEDBY",
            $"PK_U_PROJECTTIMEEMPLOYEEWEEK = {id}", "");
        rsProjecttimeEmployeeWeek.MoveFirst();

        rsProjecttimeEmployeeWeek.SetFieldValue("DATECLOSED", DateTime.Now);
        rsProjecttimeEmployeeWeek.SetFieldValue("CLOSEDBY", GetUserInfo().CurrentUserName);

        rsProjecttimeEmployeeWeek.Update();

        Guid setStateClosed = new Guid("9dcfeed4-a703-4b88-b060-73bef6896ed6");

        ExecuteWorkflowEvent("U_PROJECTTIMEEMPLOYEEWEEK", id, setStateClosed, null);

    }

    private void CloseOpenProjecttimes(ScriptRecordset rsProjectTime)
    {
        //Zet vinkje 'Closed' aan bij geselecteerde uren
        rsProjectTime.MoveFirst();

        rsProjectTime.UpdateWhenMoveRecord = false;
        rsProjectTime.UseDataChanges = false;

        while (!rsProjectTime.EOF)
        {
            rsProjectTime.Fields["CLOSED"].Value = true;

            rsProjectTime.MoveNext();
        }

        rsProjectTime.MoveFirst();
        rsProjectTime.Update();
    }
}
