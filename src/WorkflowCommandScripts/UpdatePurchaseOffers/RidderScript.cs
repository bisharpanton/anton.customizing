﻿using ADODB;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Script;
using Ridder.Common.Search;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using Ridder.Client.SDK.SDKParameters;

public class UpdatePurchaseOffers_RidderScript : WorkflowCommandScript
{
    public void Execute()
    {
        // HVE
        // 14-12-2021
        // Open linked purchaseoffers
        
        var offerId = (int)RecordIds.First();
        var purchaseOffers = GetRecordset("R_PURCHASEOFFER", "", $"FK_OFFER = {offerId}", "")
            .DataTable
            .AsEnumerable()
            .ToList();

        if (!purchaseOffers.Any())
        {
            return;
        }


        var openBrowse = new BrowseParameters()
        {
            TableName = "R_PURCHASEOFFER", 
            Filter = $"FK_OFFER = {offerId}"
        };

        OpenBrowse(openBrowse, false);
    }

}
