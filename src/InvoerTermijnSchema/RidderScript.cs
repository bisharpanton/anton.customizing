﻿using System;
using System.Xml;
using System.Xml.Linq;
using ADODB;
using System.Data;
using System.Linq;
using Ridder.Client.SDK.SDKParameters;

namespace FieldTemplatePlugin
{
    public class Script : PluginScript
    {
        public override void Run()
        {

            //DB
            //27-10-2016
            //Klant specifieke manier om termijnschema te creëeren

            //Stap 1 - Haal configuratie record op van huidige gebruiker, indien niet aanwezig
            if (this.Data.CurrentRecord.State == Ridder.Common.DatabaseDesigner.ObjectState.Added
                ) //Bij toevoegen offerte is dit nog niet mogelijk
            {
                System.Windows.Forms.MessageBox.Show(
                    "Het genereren van een termijnschema kan pas als het record ten minste éénmaal is opgeslagen.");
                return;
            }

            var tableName = Data.TableName;
            var id = (int)Data.GetPrimaryKeyValue();
            var columnName = $"FK_{tableName.Substring(2, tableName.Length - 2)}";

            var rs = Data.GetRecordset("C_DUMMYINVOERTERMIJNSCHEMA", "", $"{columnName}={id}", "");

            if (rs.RecordCount > 0)
            {
                rs.MoveFirst();
                while (!rs.EOF)
                {
                    rs.Delete();
                }
            }

            rs.AddNew();
            rs.Fields[columnName].Value = id;
            rs.Fields["SEQUENCENUMBER"].Value = 0;
            rs.Update();

            var editPar = new EditParameters()
            {
                TableName = tableName,
                Id = id,
                Context = "INVOERTERMIJN",

            };
            this.OpenEdit(editPar, true);


           // this.OpenEditForm("C_DUMMYINVOERTERMIJNSCHEMA", (int)rs.Fields["PK_C_DUMMYINVOERTERMIJNSCHEMA"].Value, null,
             //   true);

            DateTime vandaag = System.DateTime.Now;
            DateTime TienSecondenGeleden = vandaag.AddSeconds(-10);

            string maand = (TienSecondenGeleden.Month < 10)
                ? "0" + TienSecondenGeleden.Month.ToString()
                : TienSecondenGeleden.Month.ToString();
            string dag = (TienSecondenGeleden.Day < 10)
                ? "0" + TienSecondenGeleden.Day.ToString()
                : TienSecondenGeleden.Day.ToString();

            string minuutGeleden = string.Format("{0}{1}{2} {3}:{4}:{5}", TienSecondenGeleden.Year, maand, dag,
                TienSecondenGeleden.Hour, TienSecondenGeleden.Minute, TienSecondenGeleden.Second);

            var rsTermijnschemas = Data.GetRecordset("R_INVOICESCHEDULE", "PK_R_INVOICESCHEDULE",
                string.Format("DATECREATED > '{0}'", minuutGeleden), "DATECREATED DESC");

            if (rsTermijnschemas.RecordCount > 0)
            {
                rsTermijnschemas.MoveFirst();

                Data.CurrentRecord.SetCurrentRecordValue("FK_INVOICESCHEDULE",
                    rsTermijnschemas.Fields["PK_R_INVOICESCHEDULE"].Value);
            }
        }
    }
}
