﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;

public class FK_SERVICECONTRACTTYPE : ServiceContractScript
{
    public void Execute()
    {
        // HVE
        // 4-8-2020
        // Neem dekking over vanuit servicecontractsoort
        var serviceContractTypeId = Row["FK_SERVICECONTRACTTYPE"] as int? ?? 0;
        if (serviceContractTypeId == 0)
        {
            return;
        }

        var rsServiceContractType = GetRecordset("R_SERVICECONTRACTTYPE", "", 
            $"PK_R_SERVICECONTRACTTYPE = {serviceContractTypeId}", "")
            .DataTable
            .AsEnumerable();

        var labourCoverage = GetCoverage(rsServiceContractType, "LABOURCOVERAGE");
        var materialCoverage = GetCoverage(rsServiceContractType, "MATERIALCOVERAGE");
        var outsourcedCoverage = GetCoverage(rsServiceContractType, "OUTSOURCEDACTIVITYCOVERAGE");

        Row["LABOURCOVERAGE"] = labourCoverage;
        Row["MATERIALCOVERAGE"] = materialCoverage;
        Row["OUTSOURCEDACTIVITYCOVERAGE"] = outsourcedCoverage;
        
    }

    private double GetCoverage(EnumerableRowCollection<DataRow> rsServiceContractType, string columnName)
    {
        return rsServiceContractType
            .Select(x => x.Field<double>(columnName))
            .FirstOrDefault();
    }
}
