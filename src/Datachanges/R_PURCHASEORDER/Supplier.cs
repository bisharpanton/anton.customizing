﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;

public class RidderScript : PurchaseOrderScript
{
    public void Execute()
    {
        // HVE
        // 23-6-2020
        // Set Manual Created 
        Row["MANUALCREATED"] = true;

        IRelationInfo relationInfo = this.GetPurchaseOrderSupplierInfo(this.Row);
        if (relationInfo == null)
            return;

        if (relationInfo.Fk_PurchaseIncoterm != Guid.Empty)
            this.Row[this.Columns.Fk_Incoterm.ColumnName] = relationInfo.Fk_PurchaseIncoterm;
        else
            this.Row[this.Columns.Fk_Incoterm.ColumnName] = Convert.DBNull;

        this.Row[this.Columns.Fk_PaymentMethod.ColumnName] = relationInfo.Fk_PurchasePaymentMethod;

        this.Row[this.Columns.Fk_PaymentTerm.ColumnName] = relationInfo.Fk_PurchasePaymentTerm;

        this.Row[this.Columns.Fk_Currency.ColumnName] = relationInfo.Fk_Currency;
        this.Row[this.Columns.ExchangeRate.ColumnName] = relationInfo.ExchangeRate;
        if (relationInfo.ExchangeRateDate != DateTime.MinValue)
            this.Row[this.Columns.ExchangeRateDate.ColumnName] = relationInfo.ExchangeRateDate;
        this.Row[this.Columns.BookInCurrency.ColumnName] = this.GetCurrencyBookInCurrency(relationInfo.Fk_Currency);
        this.Row[this.Columns.Fk_VatCompanyGroup.ColumnName] = relationInfo.Fk_VatCompanyGroup;
        this.Row[this.Columns.Fk_Contact.ColumnName] = relationInfo.MainContactCreditor == 0 ? relationInfo.MainContact : relationInfo.MainContactCreditor;

        // HVE
        // 23-6-2020
        // Set Default tradingname
        var supplierId = (int)Row["FK_SUPPLIER"];
        var tradingNameId = GetTradingNameId(supplierId);
        if (tradingNameId > 0)
        {
            Row["FK_DIVERGENTTRADINGNAME"] = tradingNameId;
        }

        // HVE
        // 18-5-2021
        // Set WKA from supplier
        var wkaSupplier = GetWKASupplier(supplierId);
        Row["WKA"] = wkaSupplier;
    }

    private bool GetWKASupplier(int supplierId)
    {
        return GetRecordset("R_RELATION", "WKA",
                $"PK_R_RELATION = {supplierId}", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<bool>("WKA"))
            .FirstOrDefault();
    }

    private int GetTradingNameId(int relationId)
    {
        return GetRecordset("C_DIVERGENTTRADINGNAMES", "",
                $"FK_RELATION = {relationId} AND DEFAULTTRADINGNAME = TRUE", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int?>("PK_C_DIVERGENTTRADINGNAMES") ?? 0)
            .FirstOrDefault();
    }
}
