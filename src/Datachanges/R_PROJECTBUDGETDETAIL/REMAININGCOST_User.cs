﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;

public class REMAININGCOST_User : ProjectBudgetDetailScript
{
    public void Execute()
    {
        ProjectBudgetInfo projectBudgetInfo = this.CalculatePrices(Ridder.Common.Script.ProjectBudgetDetailFieldChanged.RemainingCostPrice, Row);

        //DB
        //07-05-2019
        //Trek het custom veld 'Bedrag inkooporders nieuw' af
        this.Row[ProjectBudgetDetailColumns.ExpectedCostprice.ColumnName] = projectBudgetInfo.ExpectedCostPrice
                                                                            + (double)Row["AMOUNTPURCHASEORDERSNEW"];
            
        this.Row[ProjectBudgetDetailColumns.ExpectedSalesPrice.ColumnName] = projectBudgetInfo.ExpectedSalesPrice;
        this.Row[ProjectBudgetDetailColumns.ExpectedQuantity.ColumnName] = projectBudgetInfo.ExpectedQuantity;

        this.Row[ProjectBudgetDetailColumns.RemainingSalesPrice.ColumnName] = projectBudgetInfo.RemainingSalesPrice;
        this.Row[ProjectBudgetDetailColumns.RemainingQuantity.ColumnName] = projectBudgetInfo.RemainingQuantity;

        this.Row[ProjectBudgetDetailColumns.Progress.ColumnName] = projectBudgetInfo.Progress;

    }
}
