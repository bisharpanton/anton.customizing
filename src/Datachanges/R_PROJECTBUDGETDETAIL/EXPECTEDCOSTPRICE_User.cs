﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;

public class EXPECTEDCOSTPRICE_User : ProjectBudgetDetailScript
{
    public void Execute()
    {
        ProjectBudgetInfo projectBudgetInfo = this.CalculatePrices(Ridder.Common.Script.ProjectBudgetDetailFieldChanged.ExpectedCostPrice, Row);

        this.Row[ProjectBudgetDetailColumns.ExpectedQuantity.ColumnName] = projectBudgetInfo.ExpectedQuantity;
        this.Row[ProjectBudgetDetailColumns.ExpectedSalesPrice.ColumnName] = projectBudgetInfo.ExpectedSalesPrice;

        //DB
        //07-05-2019
        //Voeg het custom veld 'Bedrag inkooporders nieuw' toe
        this.Row[ProjectBudgetDetailColumns.RemainingCost.ColumnName] = projectBudgetInfo.RemainingCostPrice
                                                                        - (double)Row["AMOUNTPURCHASEORDERSNEW"];
        this.Row[ProjectBudgetDetailColumns.RemainingSalesPrice.ColumnName] = projectBudgetInfo.RemainingSalesPrice;
        this.Row[ProjectBudgetDetailColumns.RemainingQuantity.ColumnName] = projectBudgetInfo.RemainingQuantity;
        this.Row[ProjectBudgetDetailColumns.Progress.ColumnName] = projectBudgetInfo.Progress;

    }
}
