﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;

public class FK_SUPPLIER3 : PurchaseOfferRelationScript
{
    public void Execute()
    {
        IRelationInfo relationInfo = this.GetSupplierInfo(this.Row);
        if (relationInfo == null)
            return;

        if (relationInfo.Fk_PurchaseIncoterm != Guid.Empty)
            this.Row[this.Columns.Fk_Incoterms.ColumnName] = relationInfo.Fk_PurchaseIncoterm;
        else
            this.Row[this.Columns.Fk_Incoterms.ColumnName] = Convert.DBNull;

        this.Row[this.Columns.Fk_PaymentMethod.ColumnName] = relationInfo.Fk_PurchasePaymentMethod;


        if (this.Row[this.Columns.Fk_InvoiceSchedule.ColumnName] == DBNull.Value
            || this.Row[this.Columns.Fk_InvoiceSchedule.ColumnName] == null
            || System.Convert.ToInt32(Row[this.Columns.Fk_InvoiceSchedule.ColumnName]) == 0)
            this.Row[this.Columns.Fk_PaymentTerm.ColumnName] = relationInfo.Fk_PurchasePaymentTerm;
        else
            this.Row[this.Columns.Fk_PaymentTerm.ColumnName] = Convert.DBNull;

        this.Row[this.Columns.Fk_Currency.ColumnName] = relationInfo.Fk_Currency;
        this.Row[this.Columns.ExchangeRate.ColumnName] = relationInfo.ExchangeRate;
        if (relationInfo.ExchangeRateDate != DateTime.MinValue)
            this.Row[this.Columns.ExchangeRateDate.ColumnName] = relationInfo.ExchangeRateDate;
        this.Row[this.Columns.Fk_Contact.ColumnName] = relationInfo.MainContactCreditor == 0
            ? relationInfo.MainContact
            : relationInfo.MainContactCreditor;

        // HVE
        // 30-11-2021
        // Set Default tradingname
        Row["MANUALCREATED"] = true;

        var supplierId = (int)Row["FK_SUPPLIER"];
        var tradingNameId = GetTradingNameId(supplierId);
        if (tradingNameId > 0)
        {
            Row["FK_DIVERGENTTRADINGNAME"] = tradingNameId;
        }

    }

    private int GetTradingNameId(int relationId)
    {
        return GetRecordset("C_DIVERGENTTRADINGNAMES", "",
                $"FK_RELATION = {relationId} AND DEFAULTTRADINGNAME = TRUE", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int?>("PK_C_DIVERGENTTRADINGNAMES") ?? 0)
            .FirstOrDefault();
    }
}
