﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;

public class KTO_FK_ORDER_User : ScriptDataChange
{
    public void Execute()
    {
		//DB
        //1-12-2020
        //Neem relatie en contactpersoon over vanuit de order

        var orderId = Row["FK_ORDER"] as int? ?? 0;

        if (orderId == 0)
        {
            Row["FK_RELATION"] = DBNull.Value;
            Row["FK_CONTACT"] = DBNull.Value;

            return;
        }

        var order = GetRecordset("R_ORDER", "FK_RELATION, FK_CONTACT", $"PK_R_ORDER = {orderId}", "")
            .DataTable.AsEnumerable().First();

        Row["FK_RELATION"] = order.Field<int>("FK_RELATION");
        Row["FK_CONTACT"] = order.Field<int?>("FK_CONTACT");
    }
}
