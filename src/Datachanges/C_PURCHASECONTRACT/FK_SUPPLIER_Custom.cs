﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;

public class FK_SUPPLIER2 : ScriptDataChange
{
    public void Execute()
    {
        // HVE
        // 23-2-2021
        // Hoofdcontactpersoon leverancier overnemen
        var supplierId = (int)Row["FK_SUPPLIER"];

        var contactId = GetRecordset("R_CONTACT", "PK_R_CONTACT", $"FK_RELATION = {supplierId} AND MAINCONTACT == 1", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int?>("PK_R_CONTACT") ?? 0)
            .FirstOrDefault();

        if (contactId == 0)
        {
            return;
        }

        Row["FK_CONTACT"] = contactId;

    }
}
