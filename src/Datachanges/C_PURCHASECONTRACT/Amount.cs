﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;


namespace Datachanges.C_PURCHASECONTRACT
{
    public class Amount : ScriptDataChange
    {
        public void Execute()
        {
            // Roundup the amount
            var amount = (double)Row["AMOUNT"];

            var round = Math.Round(amount);

            Row["AMOUNT"] = round;

        }
    }
}
