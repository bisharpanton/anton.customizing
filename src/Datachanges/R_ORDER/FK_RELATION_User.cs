﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;

public class FK_RELATION_User_Order : OrderScript
{
    public void Execute()
    {

        //DB
        //21-05-2019
        //Vul verkoper met vertegenwoordiger uit relatie indien gevuld, anders huidige wn
        if ((Row["FK_RELATION"] as int?).HasValue)
        {
            int? salesPersonRelation = GetSalesPersonFromRelation((int)Row["FK_RELATION"]);
            Row["FK_SALESPERSON"] = salesPersonRelation.HasValue ? salesPersonRelation.Value : GetCurrentEmployee(CurrentUserId);
        }

        // HVE
        // 7-7-2020
        // Set Default tradingname
        var tradingNameId = GetDefaultTradingNameId((int)Row["FK_RELATION"]);
        if (tradingNameId > 0)
        {
            Row["FK_DIVERGENTTRADINGNAME"] = tradingNameId;
        }

        //DB
        //Get sector from relation
        var sector = GetSectorFromRelation(Row["FK_RELATION"] as int?);
        if(sector > 0)
        {
            Row["FK_INDUSTRY"] = sector;
        }
    }

    private int GetSectorFromRelation(int? relationId)
    {
        var copySector = GetRecordset("R_CRMSETTINGS", "COPYSECTORTOOFFERANDORDER", "", "")
            .DataTable.AsEnumerable().First().Field<bool>("COPYSECTORTOOFFERANDORDER");

        if(!copySector)
        {
            return 0;
        }

        if (!relationId.HasValue)
        {
            return 0;
        }

        var rsRelatie = GetRecordset("R_RELATION", "FK_INDUSTRY", 
            $"PK_R_RELATION = {relationId.Value}", "");
        rsRelatie.MoveFirst();

        return rsRelatie.GetField("FK_INDUSTRY").Value as int? ?? 0;
    }


    private int GetDefaultTradingNameId(int relationId)
    {
        return GetRecordset("C_DIVERGENTTRADINGNAMES", "",
                $"FK_RELATION = {relationId} AND DEFAULTTRADINGNAME = TRUE", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int?>("PK_C_DIVERGENTTRADINGNAMES") ?? 0)
            .FirstOrDefault();
    }

    private int GetCurrentEmployee(int currentUserId)
    {
        var employee = GetRecordset("R_USER", "FK_EMPLOYEE", $"PK_R_USER = {currentUserId}", "")
            .DataTable.AsEnumerable().First().Field<int?>("FK_EMPLOYEE");

        if (!employee.HasValue)
        {
            throw new Exception(
                "Huidige gebruiker is niet aan een werknemer gekoppeld. Verkoper kan niet bepaald worden.");
        }

        return employee.Value;
    }

    private int? GetSalesPersonFromRelation(int relation)
    {
        return GetRecordset("R_RELATION", "FK_SALESPERSON", $"PK_R_RELATION = {relation}", "")
            .DataTable.AsEnumerable().First().Field<int?>("FK_SALESPERSON");
    }
}
