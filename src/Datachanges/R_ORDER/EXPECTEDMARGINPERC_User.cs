﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;

public class EXPECTEDMARGINPERC_User : OrderScript
{
    public void Execute()
    {
        //SP 17-1-2023 Berekenen verwacht rendement en kosten

        if ((Row["FK_MAINPROJECT"] as int? ?? 0) != 0)
        {
            return;//Indien er een project is gekoppeld worden de velden hier vandaan gevuld.
        }
        else
        {
            double perc = (double)Row["EXPECTEDMARGINPERC"];
            double totalAmount = (double)Row["TOTALNETAMOUNT"];

            if (totalAmount == 0.0)
            {
                return;
            }

            double margin = perc * totalAmount;

            Row["EXPECTEDMARGINAMOUNT"] = margin;
            Row["EXPECTEDTOTALCOSTS"] = totalAmount - margin;

            var orderId = (int)Row["PK_R_ORDER"];
            var purchaseCosts = DeterminePurchaseCosts(orderId);
            var wipCosts = DetermineTotalWip(orderId);

            Row["REMAININGCOST"] = (double)Row["EXPECTEDTOTALCOSTS"] - (purchaseCosts + wipCosts);
        }
    }

    private double DetermineTotalWip(int orderId)
    {
        var result = 0.0;

        var inkooptermijnenNietOntvangenWelPrestatieverklaringAkkoord = GetRecordset("U_PURCHASEINSTALLMENTSPERPURCHASEORDERPERORDER", "PURCHASEINSTALLMENTNETAMOUNTTHISORDER",
            $"FK_ORDER = {orderId} AND DECLARATIONOFPERFORMANCE = 1", "").DataTable.AsEnumerable().Sum(x => x.Field<double>("PURCHASEINSTALLMENTNETAMOUNTTHISORDER"));

        return (double)Row["REALTOTALMATERIAL"] + (double)Row["REALTOTALMISC"] + (double)Row["REALTOTALOUTSOURCED"]
            + (double)Row["REALTOTALWORK"] + inkooptermijnenNietOntvangenWelPrestatieverklaringAkkoord;
    }

    private double DeterminePurchaseCosts(int orderId)
    {
        //SourceType / Herkomstsoort = inkooporderregel artikel/divers/ubw

        var allReservations = GetRecordset("R_ALLRESERVATION", "FK_PURCHASEORDERDETAILITEM, FK_PURCHASEORDERDETAILMISC, FK_PURCHASEORDERDETAILOUTSOURCED",
            $"FK_ORDER = {orderId} AND FK_PURCHASEORDER IS NOT NULL AND SOURCETYPE IN (2,8,9)", "").DataTable.AsEnumerable().ToList();

        if (!allReservations.Any())
        {
            return 0.0;
        }

        var result = 0.0;
        var sources = new string[3] { "ITEM", "MISC", "OUTSOURCED" };

        foreach (var source in sources)
        {
            var purchaseOrderDetailIds = allReservations.Where(x => x.Field<int?>($"FK_PURCHASEORDERDETAIL{source}").HasValue)
                .Select(x => x.Field<int>($"FK_PURCHASEORDERDETAIL{source}")).ToList();

            if (!purchaseOrderDetailIds.Any())
            {
                continue;
            }

            var purchaseOrderDetails = GetRecordset($"R_PURCHASEORDERDETAIL{source}", "QUANTITY, QUANTITYTORECEIVE, NETPURCHASEPRICE",
                $"PK_R_PURCHASEORDERDETAIL{source} IN ({string.Join(",", purchaseOrderDetailIds)})", "").DataTable.AsEnumerable().ToList();

            result += purchaseOrderDetails.Sum(x => x.Field<double>("QUANTITYTORECEIVE") * (x.Field<double>("NETPURCHASEPRICE") / x.Field<double>("QUANTITY")));
        }

        var inkooptermijnenNietOntvangenWelPrestatieverklaringAkkoord = GetRecordset("U_PURCHASEINSTALLMENTSPERPURCHASEORDERPERORDER", "PURCHASEINSTALLMENTNETAMOUNTTHISORDER",
            $"FK_ORDER = {orderId} AND DECLARATIONOFPERFORMANCE = 1", "").DataTable.AsEnumerable().Sum(x => x.Field<double>("PURCHASEINSTALLMENTNETAMOUNTTHISORDER"));

        result -= inkooptermijnenNietOntvangenWelPrestatieverklaringAkkoord;

        return result;
    }
}
