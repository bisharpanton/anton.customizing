﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;

public class CODE_User : ProjectTimeScript
{
    public void Execute()
    {

        //DB
        //16-12-2014
        //Als de code ingevuld wordt, vul dan automatisch de werknemer in

        if (Row["WERKNEMERSCODE"].ToString() == "")
        {
            return;
        }

        var rsEmployee = GetRecordset("R_EMPLOYEE", "PK_R_EMPLOYEE, FK_WAGEGROUP",
            $"CODE = '{Row["WERKNEMERSCODE"]}'", "");

        if (rsEmployee.RecordCount == 0)
        {
            return;
        }

        rsEmployee.MoveFirst();
        Row["FK_EMPLOYEE"] = (int)rsEmployee.Fields["PK_R_EMPLOYEE"].Value;
        Row["FK_WAGEGROUP"] = (int)rsEmployee.Fields["FK_WAGEGROUP"].Value;

        SystemDatachangeEmployee();
    }

    public void SystemDatachangeEmployee()
    {
        if (Row.State == Ridder.Common.DatabaseDesigner.ObjectState.Added)
        {
            long ticks = GetRemainingHours(Row).Ticks;
            Row[ProjectTimeColumns.TimeDevice.ColumnName] = ticks;
            Row[ProjectTimeColumns.TimeEmployee.ColumnName] = ticks;
            Row[ProjectTimeColumns.TimeOfInLieu.ColumnName] = GetTOIL(Row).Ticks;

            if (ticks != 0)
                Row[ProjectTimeColumns.Fk_OverTimeCode.ColumnName] = 0;
            else
                Row[ProjectTimeColumns.Fk_OverTimeCode.ColumnName] = GetOverTimeCode(Row);
        }
        Row[ProjectTimeColumns.Fk_CostCenter.ColumnName] = GetCostCenter(Row);
        Row[ProjectTimeColumns.Fk_WageGroup.ColumnName] = GetWageGroupFromEmployee(Row);
        ProjectTimePrices prices = CalculatePrices(Row);
        Row[ProjectTimeColumns.CostPriceDevice.ColumnName] = prices.CostpriceDevice;
        Row[ProjectTimeColumns.CostPriceEmployee.ColumnName] = prices.CostpriceEmployee;
        Row[ProjectTimeColumns.SalesPriceDevice.ColumnName] = prices.SalesPriceDevice;
        Row[ProjectTimeColumns.SalesPriceEmployee.ColumnName] = prices.SalesPriceEmployee;
    }
}
