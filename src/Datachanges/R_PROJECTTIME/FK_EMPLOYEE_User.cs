﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;

public class FK_EMPLOYEE_User : ProjectTimeScript
{
    public void Execute()
    {

        if (Row.State == Ridder.Common.DatabaseDesigner.ObjectState.Added)
        {
            long ticks = GetRemainingHours(Row).Ticks;
            Row[ProjectTimeColumns.TimeDevice.ColumnName] = ticks;
            Row[ProjectTimeColumns.TimeEmployee.ColumnName] = ticks;
            Row[ProjectTimeColumns.TimeOfInLieu.ColumnName] = GetTOIL(Row).Ticks;

            if (ticks != 0)
                Row[ProjectTimeColumns.Fk_OverTimeCode.ColumnName] = 0;
            else
                Row[ProjectTimeColumns.Fk_OverTimeCode.ColumnName] = GetOverTimeCode(Row);
        }
        Row[ProjectTimeColumns.Fk_CostCenter.ColumnName] = GetCostCenter(Row);
        Row[ProjectTimeColumns.Fk_WageGroup.ColumnName] = GetWageGroupFromEmployee(Row);
        ProjectTimePrices prices = CalculatePrices(Row);
        Row[ProjectTimeColumns.CostPriceDevice.ColumnName] = prices.CostpriceDevice;
        Row[ProjectTimeColumns.CostPriceEmployee.ColumnName] = prices.CostpriceEmployee;
        Row[ProjectTimeColumns.SalesPriceDevice.ColumnName] = prices.SalesPriceDevice;
        Row[ProjectTimeColumns.SalesPriceEmployee.ColumnName] = prices.SalesPriceEmployee;


        //DB
        //16-12-2014
        //Als de werknemer ingevuld wordt, vul dan automatisch de code in
        if ((Row["FK_EMPLOYEE"] as int?).HasValue)
        {
            var rsEmployee = GetRecordset("R_EMPLOYEE", "CODE, FK_CAO",
                $"PK_R_EMPLOYEE = {Row["FK_EMPLOYEE"]}", "");
            rsEmployee.MoveFirst();

            Row["WERKNEMERSCODE"] = rsEmployee.Fields["CODE"].Value.ToString();
            Row["FK_CAO"] = rsEmployee.Fields["FK_CAO"].Value;
        }
    }
}
