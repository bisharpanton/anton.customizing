﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;

public class ENDTIME_User : ProjectTimeScript
{
    public void Execute()
    {
        // HVE
        // 7-7-2020
        // Bij wijzigen eindtijd de Tijd werknemer update (Eindtijd - starttijd)
        var startTime = Convert.ToDateTime(Row["STARTTIME"]).TimeOfDay;
        if (startTime == TimeSpan.Zero)
        {
            return;
        }
        var endTime = Convert.ToDateTime(Row["ENDTIME"]).TimeOfDay;

        Row["TIMEEMPLOYEE"] = endTime.Ticks - startTime.Ticks;
        Row["TIMEDEVICE"] = endTime.Ticks - startTime.Ticks;
    }
}
