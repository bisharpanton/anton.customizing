﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;

public class FK_ORDERRELATION_User_SALESINVOICE : SalesInvoiceScript
{
    public void Execute()
    {
        // HVE
        // 7-7-2020
        // Set Default tradingname
        var tradingNameId = GetDefaultTradingNameId((int)Row["FK_ORDERRELATION"]);
        if (tradingNameId > 0)
        {
            Row["FK_DIVERGENTTRADINGNAME"] = tradingNameId;
        }

    }
    private int GetDefaultTradingNameId(int relationId)
    {
        return GetRecordset("C_DIVERGENTTRADINGNAMES", "",
                $"FK_RELATION = {relationId} AND DEFAULTTRADINGNAME = TRUE", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int?>("PK_C_DIVERGENTTRADINGNAMES") ?? 0)
            .FirstOrDefault();
    }
}
