﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;

public class FK_DIPLOMAGROEP_User : ScriptDataChange
{
    public void Execute()
    {

        if(!(Row["FK_DIPLOMAGROEP"] as int?).HasValue)
        {
            return;
        }

        Row["DESCRIPTION"] = GetRecordset("U_DIPLOMAGROEP", "DESCRIPTION",
                $"PK_U_DIPLOMAGROEP = {Row["FK_DIPLOMAGROEP"]}", "").DataTable.AsEnumerable().First()
            .Field<string>("DESCRIPTION");

    }
}
