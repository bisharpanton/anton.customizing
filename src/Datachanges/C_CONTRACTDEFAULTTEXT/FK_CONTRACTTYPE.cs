﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;

public class FK_CONTRACTTYPE : ScriptDataChange
{
    public void Execute()
    {
        // HVE
        // 9-3-2021
        // Determine Sequence
        var chapterId = (int?)Row["FK_CONTRACTCHAPTER"] ?? 0;
        var contractType = (int?)Row["FK_CONTRACTTYPE"] ?? 0;

        if (chapterId == 0 || contractType == 0)
        {
            return;
        }

        var contraxtTexts = GetRecordset("C_CONTRACTDEFAULTTEXT", "SEQUENCE",
                $"FK_CONTRACTCHAPTER = {chapterId} AND FK_CONTRACTTYPE = {contractType}", "")
            .DataTable
            .AsEnumerable();

        if (!contraxtTexts.Any())
        {
            return;
        }

        var sequence = contraxtTexts.Max(x => x.Field<int>("SEQUENCE"));

        if (sequence > 0)
        {
            Row["SEQUENCE"] = sequence + 1;
        }

    }
}
