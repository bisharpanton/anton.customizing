﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;

public class FK_OFFER_User : TodoScript
{
    public void Execute()
    {
		//Neem relatie over uit offerte

        var offerId = Row["FK_OFFER"] as int? ?? 0;

        if(offerId == 0)
        {
            return;
        }

        Row["FK_RELATION"] = GetRecordset("R_OFFER", "FK_RELATION",
            $"PK_R_OFFER = {offerId}", "")
            .DataTable.AsEnumerable().First().Field<int>("FK_RELATION");

    }
}
