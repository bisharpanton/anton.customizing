﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;

public class FK_SALESINVOICE_User : TodoScript
{
    public void Execute()
    {
        //Neem relatie over uit offerte

        var salesInvoiceId = Row["FK_SALESINVOICE"] as int? ?? 0;

        if(salesInvoiceId == 0)
        {
            return;
        }

        Row["FK_RELATION"] = GetRecordset("R_SALESINVOICE", "FK_INVOICERELATION",
                $"PK_R_SALESINVOICE = {salesInvoiceId}", "")
            .DataTable.AsEnumerable().First().Field<int>("FK_INVOICERELATION");

    }
}
