﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;

public class FK_ORDER_User : TodoScript
{
    public void Execute()
    {
        //Neem relatie over uit offerte

        var orderId = Row["FK_ORDER"] as int? ?? 0;

        if(orderId == 0)
        {
            return;
        }

        Row["FK_RELATION"] = GetRecordset("R_ORDER", "FK_RELATION",
                $"PK_R_ORDER = {orderId}", "")
            .DataTable.AsEnumerable().First().Field<int>("FK_RELATION");

    }
}
