﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;

public class FK_PURCHASECONTRACT : PurchaseOrderDetailItemScript
{
    public void Execute()
    {
        // HVE
        // 4-5-2021
        // Set order from purchasecontract
        var manually = (bool)this.Row["ORDERSETMANUAL"];
        var purchaseContractId = this.Row["FK_PURCHASECONTRACT"] as int? ?? 0;
        if (manually)
        {
            return;
        }

        if (purchaseContractId == 0)
        {
            this.Row["DIRECTTOORDER"] = false;
            this.Row["FK_ORDER"] = DBNull.Value;
            return;
        }

        var orderId = GetOrderFromPurchaseContract(purchaseContractId);
        if (orderId == 0)
        {
            return;
        }

        this.Row["DIRECTTOORDER"] = true;
        this.Row["FK_ORDER"] = orderId;
    }

    private int GetOrderFromPurchaseContract(int purchaseContractId)
    {
        return GetRecordset("C_PURCHASECONTRACT", "", $"PK_C_PURCHASECONTRACT = {purchaseContractId}", "")
            .DataTable
            .AsEnumerable()
            .Select(x => x.Field<int?>("FK_ORDER") ?? 0)
            .FirstOrDefault();
    }
}
