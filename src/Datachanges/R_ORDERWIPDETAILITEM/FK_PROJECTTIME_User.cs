﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;

public class FK_PROJECTTIME_User : OrderWIPDetailItemScript
{
    public void Execute()
    {
        //DB
        //03-09-2019
        //Vul order en bon vanuit urenboeking

        var projecttimeId = Row["FK_PROJECTTIME"] as int?;

        if (!projecttimeId.HasValue)
        {
            return;
        }

        var projectTime = GetRecordset("R_PROJECTTIME", "FK_ORDER, FK_JOBORDER, DATE",
            $"PK_R_PROJECTTIME = {projecttimeId}", "").DataTable.AsEnumerable().First();

        if (!projectTime.Field<int?>("FK_ORDER").HasValue)
        {
            return;
        }

        Row["FK_ORDER"] = projectTime.Field<int>("FK_ORDER");
        Row["FK_JOBORDER"] = projectTime.Field<int>("FK_JOBORDER");
        Row["DATEINWIP"] = projectTime.Field<DateTime>("DATE");
    }
}
