﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;

public class RENDEMENT_PERC : ScriptDataChange
{
    public void Execute()
    {
        // HVE
        // 19-10-2021
        // Recalculate Take profit
        var rendement = Row["RENDEMENT_PERC"] as double? ?? 0d;
        if (rendement == 0d)
        {
            return;
        }

        var costs = (double)Row["COSTPRICE"];
        var totalTakeProfit = (double)Row["TOTAL_TAKEPROFIT"];
        Row["TAKE_PROFIT"] = (costs * rendement) - totalTakeProfit;
    }
}
