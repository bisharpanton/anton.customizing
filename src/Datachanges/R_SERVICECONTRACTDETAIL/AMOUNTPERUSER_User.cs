﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;

namespace Datachanges.R_SERVICECONTRACTDETAIL
{
    class AMOUNTPERUSER_User : ServiceContractDetailScript
    {
        public void Execute()
        {
            //DB
            //27-12-2021
            //Bereken totaal bedrag

            var helper = new ScriptHelper();

            if (!helper.GetUserInfo().CompanyName.Equals("Bisharp"))
            {
                return;
            }

            var serviceContract = Row["FK_SERVICECONTRACT"] as int? ?? 0;

            if(serviceContract == 0)
            {
                return;
            }

            var numberOfUsers = GetRecordset("R_SERVICECONTRACT", "NUMBEROFUSERS", $"PK_R_SERVICECONTRACT = {Row["FK_SERVICECONTRACT"]}", "")
                .DataTable.AsEnumerable().First().Field<int>("NUMBEROFUSERS");

            Row["AMOUNT"] = (double)Row["AMOUNTPERUSER"] * Convert.ToDouble(numberOfUsers);


        }
    }
}
