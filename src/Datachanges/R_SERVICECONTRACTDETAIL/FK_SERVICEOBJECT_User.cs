﻿using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;
using System;

namespace Datachanges.R_SERVICECONTRACTDETAIL
{
    class FK_SERVICEOBJECT_User : ServiceContractDetailScript
    {
        public void Execute()
        {
            //DB
            //27-12-2021
            //Neem default bedrag over


            var helper = new ScriptHelper();

            if (!helper.GetUserInfo().CompanyName.Equals("Bisharp"))
            {
                return;
            }

            var serviceObject = Row["FK_SERVICEOBJECT"] as int? ?? 0;
            var serviceContract = Row["FK_SERVICECONTRACT"] as int? ?? 0;

            if (serviceObject == 0 || serviceContract == 0)
            {
                return;
            }

            var numberOfUsers = GetRecordset("R_SERVICECONTRACT", "NUMBEROFUSERS", $"PK_R_SERVICECONTRACT = {Row["FK_SERVICECONTRACT"]}", "")
                .DataTable.AsEnumerable().First().Field<int>("NUMBEROFUSERS");

            var serviceObjectTypeId = GetRecordset("R_SERVICEOBJECT", "FK_SERVICEOBJECTTYPE", $"PK_R_SERVICEOBJECT = {serviceObject}", "")
                .DataTable.AsEnumerable().First().Field<int>("FK_SERVICEOBJECTTYPE");

            var defaultPricePerUser = GetRecordset("R_SERVICEOBJECTTYPE", "BISHARPPORTALPRICEPERUSER", $"PK_R_SERVICEOBJECTTYPE = {serviceObjectTypeId}", "")
                .DataTable.AsEnumerable().First().Field<double>("BISHARPPORTALPRICEPERUSER");

            Row["AMOUNTPERUSER"] = defaultPricePerUser;
            Row["AMOUNT"] = defaultPricePerUser * Convert.ToDouble(numberOfUsers);
        }
    }
}
