﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;

public class R_RELATION_FK_VISITINGADDRESS : ScriptDataChange
{
    public void Execute()
    {
        if (this.Row["FK_VISITINGADDRESS"] != DBNull.Value)
        {
            ScriptRecordset rsAddress = this.GetRecordset("R_ADDRESS", "FK_COUNTRY", "PK_R_ADDRESS = " + (int)this.Row["FK_VISITINGADDRESS"], "");
            rsAddress.MoveFirst();

            var countryCode = GetRecordset("R_COUNTRY", "CODE", $"PK_R_COUNTRY = {rsAddress.GetField("FK_COUNTRY").Value}", "")
                .DataTable.AsEnumerable().First().Field<string>("CODE");

            //Maak BTW-Bedrijfsgroep leeg als land is ongelijk aan NL
            if (!countryCode.Equals("NL"))
            {
                this.Row["FK_VATCOMPANYGROUP"] = DBNull.Value;
            }
            else
            {
                this.Row["FK_VATCOMPANYGROUP"] = 103;
            }
        }
    }
}
