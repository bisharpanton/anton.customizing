﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using System.Diagnostics.Eventing.Reader;
using Ridder.Common.Script;

public class DESCRIPTION : ScriptDataChange
{
    public void Execute()
    {
        // HVE
        // 6-4-2021
        // Set Sequence number

        if ((int)this.Row["SEQUENCENUMBER"] > 0)
        {
            return;
        }

        var offerId = this.Row["FK_OFFER"] as int? ?? 0;
        var purchaseOrderId = this.Row["FK_PURCHASEORDER"] as int? ?? 0;
        var purchaseContractId = this.Row["FK_PURCHASECONTRACT"] as int? ?? 0;

        var filter = "";
        if (offerId > 0)
        {
            filter = $"FK_OFFER = {offerId}";
        }
        else if (purchaseOrderId > 0)
        {
            filter = $"FK_PURCHASEORDER = {purchaseOrderId}";
        }
        else if (purchaseContractId > 0)
        {
            filter = $"FK_PURCHASECONTRACT = {purchaseContractId}";
        }
        else
        {
            return;
        }

        var max = GetRecordset("C_DUMMYINVOERTERMIJNSCHEMA", "SEQUENCENUMBER", $"{filter}", "")
            .DataTable
            .AsEnumerable()
            .Max(x => x.Field<int>("SEQUENCENUMBER"));

        this.Row["SEQUENCENUMBER"] = max + 1;
    }
}
