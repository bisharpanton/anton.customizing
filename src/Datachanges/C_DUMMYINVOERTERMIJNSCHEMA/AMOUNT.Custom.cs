﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using System.Diagnostics.Eventing.Reader;
using Ridder.Common.Script;

public class AMOUNT : ScriptDataChange
{
    public void Execute()
    {

        //BMW
        //07-11-2016
        //Bereken kolom 'Bedrag' obv totaal offertebedrag in record 1
        Ridder.Common.Script.ScriptHelper helper = new Ridder.Common.Script.ScriptHelper();

        var totaalBedrag = 0.0;

        var offerId = this.Row["FK_OFFER"] as int? ?? 0;
        var purchaseOrderId = this.Row["FK_PURCHASEORDER"] as int? ?? 0;
        var purchaseContractId = this.Row["FK_PURCHASECONTRACT"] as int? ?? 0;

        if (offerId > 0)
        {
            ScriptRecordset rsOfferteregels = this.GetRecordset("R_OFFERALLDETAIL", "NETSALESAMOUNT",
                $"FK_OFFER = {offerId}", "");
            totaalBedrag = rsOfferteregels.DataTable.AsEnumerable().Sum(x => (double)x["NETSALESAMOUNT"]);
        }
        else if (purchaseOrderId > 0)
        {
            totaalBedrag = this.GetRecordset("R_PURCHASEORDERALLDETAIL", "NETPURCHASEPRICE",
                    $"FK_PURCHASEORDER = {purchaseOrderId}", "")
                .DataTable
                .AsEnumerable()
                .Sum(x => (double)x["NETPURCHASEPRICE"]);
        }
        else if (purchaseContractId > 0)
        {
            totaalBedrag = this.GetRecordset("C_PURCHASECONTRACT", "AMOUNT",
                    $"PK_C_PURCHASECONTRACT = {purchaseContractId}", "")
                .DataTable
                .AsEnumerable()
                .Sum(x => (double)x["AMOUNT"]);
        }
        else
        {
            return;
        }

        this.Row["PERCENTAGE"] = (Math.Round(totaalBedrag, 1) == 0.0) ? 0.0 : (double)this.Row["AMOUNT"] / totaalBedrag;

    }
}
