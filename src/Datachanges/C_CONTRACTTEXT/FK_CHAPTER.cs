﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Data;
using Ridder.Common.Script;

public class FK_CHAPTER : ScriptDataChange
{
    public void Execute()
    {
        // HVE
        // 9-3-2021
        // Determine Sequence
        var chapterId = (int?)Row["FK_CHAPTER"] ?? 0;
        var contractId = (int?)Row["FK_PURCHASECONTRACT"] ?? 0;

        if (chapterId == 0 || contractId == 0)
        {
            return;
        }

        var contraxtTexts = GetRecordset("C_CONTRACTTEXT", "SEQUENCE",
                $"FK_CHAPTER = {chapterId} AND FK_PURCHASECONTRACT = {contractId}", "")
            .DataTable
            .AsEnumerable();

        if (!contraxtTexts.Any())
        {
            return;
        }
        
        var sequence = contraxtTexts.Max(x => x.Field<int>("SEQUENCE"));
        Row["SEQUENCE"] = sequence + 1;
    }
}
