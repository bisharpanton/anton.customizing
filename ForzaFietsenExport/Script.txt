﻿/*
 * <Add a description.txt to your project> 
 * 
 * Changelog: 
 *   
 *   <add a changelog.txt to your project>
 * 
 * References:
 * 
 *   Newtonsoft.Json (C:\Users\remko\.nuget\packages\newtonsoft.json\13.0.2\lib\net45\Newtonsoft.Json.dll)
 * 
 * Generated on: 2024-04-09 13:17 by remko
 *   
 */

/*
 * RidderScript.cs
 */
namespace Forza___Relaties_naar_Brevo
{
    using ADODB;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text.RegularExpressions;
    using System.Xml;
    using Ridder.Common.ADO;
    using Ridder.Common.Choices;
    using Ridder.Common.Search;
    using System.Linq;
    using System.Windows.Forms;
    using System.Data;
    using Ridder.Common.Script;
    using Ridder.Client.SDK.SDKParameters;
    using System.Net.Http;
    using Newtonsoft.Json;
    using System.Text;
    using System.Threading.Tasks;
    using ForzaFietsenExport;
    using System.Threading;
    
    public class RidderScript : CommandScript
    {
        private readonly string _apikey = "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";
        private readonly HttpClient _client = new HttpClient();

        public async void Execute()
        {
            // RK
            // 25-01-2024
            // Toevoegen bijhouden van synchronisatie tijd/datum
            // Verwijderen uit oude lijsten wanneer niet meer in aanmerking komend

            // RK
            // 19-02-2024
            // Optimalisatie en bugfixes

            // RK
            // 15-03-2024
            // Verbeteringen en bugfixes

            // RK
            // 03-04-2024
            // Progressbar toevoegen
            if (System.Diagnostics.Debugger.IsAttached)
                System.Diagnostics.Debugger.Break();

            if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("R_RELATION"))
            {
                MessageBox.Show("Dit script mag alleen vanaf de relaties tabel aangeroepen worden.");
                return;
            }

            List<int> relationIds = FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue()).ToList();


            if (relationIds.Count == 0)
            {
                MessageBox.Show("Geen geldige relaties gekozen voor synchronisatie, maak alstublieft een andere keuze");
                return;
            }

            var filter = FormDataAwareFunctions.GetSelectedRecords().Any()
                    ? $"PK_R_RELATION IN ({string.Join(", ", relationIds)}) AND FK_RELATIONTYPE = 3"
                    : "FK_RELATIONTYPE = 3";

            var relations = GetRecordset("R_RELATION", "PK_R_RELATION, NAME, EMAIL, PHONE1, BLACKLISTEDFROMNEWSLETTERBYFORZA, UNSUBSCRIBEDINBREVO, FK_VISITINGADDRESS", filter, "").DataTable.AsEnumerable().ToList();

            DialogResult result = MessageBox.Show($"Aantal gekozen relaties voor synchronisatie: {relations.Count}. \n\nDoorgaan met synchroniseren?", "Bevestigen", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            // Niet doorgaan
            if (result == DialogResult.No)
                return;

            // Progressbar en taak starten starten
            FormExportProgress exportProgress = new FormExportProgress(this, relationIds, relations);
            exportProgress.ShowDialog();

            // Synchronisatie voltooid
            MessageBox.Show($"Synchronisatie is voltooid", "Voortgang export", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }

    public class ContactsResponse
    {
        public List<BrevoAccount> Contacts { get; set; }
        public int Count { get; set; }
    }

    public class BrevoAccount
    {
        public string email { get; set; }
        public int id { get; set; }
        public bool emailBlacklisted { get; set; }
        public bool smsBlacklisted { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime modifiedAt { get; set; }
        public int[] listIds { get; set; }
    }

    public class ExportBody
    {
        public string fileBody { get; set; }
        public int[] listIds { get; set; }
        public bool updateExistingContacts { get; set; }
        public bool emptyContactsAttributes { get; set; }
    }

    public class ContactListResponse
    {
        public List<BrevoAccount> Contacts { get; set; }
        public int Count { get; set; }
    }
}

/*
 * FormExportProgress.cs
 */
namespace ForzaFietsenExport
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using Forza___Relaties_naar_Brevo;
    using Ridder.Common.Explain;
    using Ridder.Common.Script;
    using ADODB;
    using System.Text.RegularExpressions;
    using System.Xml;
    using Ridder.Common.ADO;
    using Ridder.Common.Choices;
    using Ridder.Common.Search;
    using Ridder.Client.SDK.SDKParameters;
    using System.Net.Http;
    using Newtonsoft.Json;
    using ForzaFietsenExport;
    using System.Threading;
    using Ridder.Common.Email;
    
    public partial class FormExportProgress : Form
    {
        private readonly string _apikey = "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";
        private readonly HttpClient _client = new HttpClient();

        public RidderScript _ridderScript { get; set; }
        public List<int> _relationIds { get; set; }
        public List<DataRow> _relations { get; set; }

        public FormExportProgress(RidderScript ridderScript, List<int> relationIds, List<DataRow> relations)
        {
            InitializeComponent();

            _client.DefaultRequestHeaders.Add("api-key", _apikey);

            _ridderScript = ridderScript;
            _relationIds = relationIds;
            _relations = relations;
        }

        private async void FormExportProgress_Load(object sender, EventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached)
                System.Diagnostics.Debugger.Break();

            //Disable close
            this.ControlBox = false;

            // Maak een Progress<T> instantie
            var progress = new Progress<ProgressInfo>(progressInfo =>
            {
                progressBar1.Value = Convert.ToInt32(Math.Round(progressInfo.Percent * 100));
                label1.Text = progressInfo.Description;
            });

            try
            {
                // Start de asynchrone taak
                await Task.Run(() => DoWork(progress));
            }
            finally
            {
                this.Close();
            }
        }

        private async void DoWork(IProgress<ProgressInfo> progress)
        {
            progress.Report(new ProgressInfo()
            {
                Percent = 0.0,
                Description = $"Ophalen bestaande Brevo accounts."
            });

            // Alle brevo accounts ophalen
            List<BrevoAccount> brevoAccounts = GetAllBrevoAccounts(progress);
            
            progress.Report(new ProgressInfo()
            {
                Percent = 0.1,
                Description = $"Bestaande Brevo accounts opgehaald."
            });

            // Filter brevo accounts op email
            List<BrevoAccount> filteredBrevoAccounts = brevoAccounts.Where(x => _relations.Select(y => y.Field<string>("EMAIL")).Contains(x.email)).ToList();
            SetUnsubscribeStatus(_relationIds, filteredBrevoAccounts, progress);

            progress.Report(new ProgressInfo()
            {
                Percent = 0.2,
                Description = $"Uitschrijfstatus in Ridder bijgewerkt."
            });

            // Bepalen wie klant is
            List<DataRow> relationNotCustomers = new List<DataRow>();
            List<DataRow> relationCustomers = new List<DataRow>();

            var orders = _ridderScript.GetRecordset("R_ORDER", "PK_R_ORDER, FK_RELATION, ORDERNUMBER, ORDERDATE, ORDERTYPE", $"FK_RELATION IN ({string.Join(", ", _relationIds)})", "").DataTable.AsEnumerable().ToList();
            foreach (DataRow relation in _relations)
            {
                var relationOrders = orders.Where(x => x.Field<int>("FK_RELATION") == relation.Field<int>("PK_R_RELATION")).OrderBy(x => x.Field<DateTime>("ORDERDATE"));
                if (relationOrders.Count() != 0 && relationOrders.Any(x => x.Field<int>("ORDERTYPE") == 4))
                {
                    relationCustomers.Add(relation);
                }
                else
                {
                    relationNotCustomers.Add(relation);
                }
            }

            progress.Report(new ProgressInfo()
            {
                Percent = 0.3,
                Description = $"Relatie gesplits in wel/niet klanten."
            });

            // Klanten ophalen die in oude lijsten zitten
            List<string> emails44 = brevoAccounts.Where(x => x.listIds.Contains(44)).Select(x => x.email).ToList();
            List<string> emails45 = brevoAccounts.Where(x => x.listIds.Contains(45)).Select(x => x.email).ToList();
            List<string> emails47 = brevoAccounts.Where(x => x.listIds.Contains(47)).Select(x => x.email).ToList();

            // Niet klanten exporteren
            if (relationNotCustomers.Count != 0)
            {
                List<DataRow> notCustomerWithoutDemo = new List<DataRow>();
                List<DataRow> notCustomerWithDemo = new List<DataRow>();

                // Geen klanten splitsen op basis van demo
                (notCustomerWithoutDemo, notCustomerWithDemo) = await SplitNotCustomerBasedOnDemo(relationNotCustomers);
                
                progress.Report(new ProgressInfo()
                {
                    Percent = 0.4,
                    Description = $"Niet klanten gesplitst op basis van demo."
                });

                // Filteren op emails die met de sync meegaan
                emails44 = emails44.Where(x => relationNotCustomers.Select(y => y.Field<string>("EMAIL")).Contains(x)).ToList();
                emails45 = emails45.Where(x => relationNotCustomers.Select(y => y.Field<string>("EMAIL")).Contains(x)).ToList();
                emails47 = emails47.Where(x => relationNotCustomers.Select(y => y.Field<string>("EMAIL")).Contains(x)).ToList();

                // Klanten verwijderen uit oude lijsten
                await RemoveRelationsFromBrevoList(emails44, 44);
                await RemoveRelationsFromBrevoList(emails45, 45);
                await RemoveRelationsFromBrevoList(emails47, 47);

                progress.Report(new ProgressInfo()
                {
                    Percent = 0.5,
                    Description = $"Niet klanten verwijderd uit oude lijsten."
                });

                // Zonder demo
                if (notCustomerWithoutDemo.Count != 0)
                {
                    List<DataRow> notCustomerWithoutDemoBlackListed = notCustomerWithoutDemo.Where(x => x.Field<bool>("BLACKLISTEDFROMNEWSLETTERBYFORZA") || x.Field<bool>("UNSUBSCRIBEDINBREVO")).ToList();
                    if (notCustomerWithoutDemoBlackListed.Count != 0)
                    {
                        List<string> emails = notCustomerWithoutDemoBlackListed.Select(x => x.Field<string>("EMAIL")).ToList();

                        if(emails.Exists(x => !string.IsNullOrEmpty(x) && IsValidEmail(x)))
                        {
                            var csvNoCustomersWithoutDemoBlackListed = GenerateExportCsvWithRelations(notCustomerWithoutDemoBlackListed);
                            await ExportContactCsvToBrevo(csvNoCustomersWithoutDemoBlackListed, new int[] { 48 });
                        }
                    }

                    List<DataRow> notCustomerWithoutDemoNotBlackListed = notCustomerWithoutDemo.Where(x => !x.Field<bool>("BLACKLISTEDFROMNEWSLETTERBYFORZA") && !x.Field<bool>("UNSUBSCRIBEDINBREVO")).ToList();
                    if (notCustomerWithoutDemoNotBlackListed.Count != 0)
                    {
                        List<string> emails = notCustomerWithoutDemoNotBlackListed.Select(x => x.Field<string>("EMAIL")).ToList();

                        if(emails.Exists(x => !string.IsNullOrEmpty(x) && IsValidEmail(x)))
                        {
                            var csvNoCustomersWithoutDemoNotBlackListed = GenerateExportCsvWithRelations(notCustomerWithoutDemoNotBlackListed);
                            await ExportContactCsvToBrevo(csvNoCustomersWithoutDemoNotBlackListed, new int[] { 43, 45 });
                        }
                    }

                    progress.Report(new ProgressInfo()
                    {
                        Percent = 0.6,
                        Description = $"Niet klanten zonder demo geëxporteerd."
                    });
                }

                // Met demo
                if (notCustomerWithDemo.Count != 0)
                {
                    List<DataRow> notCustomerWithDemoBlackListed = notCustomerWithDemo.Where(x => x.Field<bool>("BLACKLISTEDFROMNEWSLETTERBYFORZA") || x.Field<bool>("UNSUBSCRIBEDINBREVO")).ToList();
                    if (notCustomerWithDemoBlackListed.Count != 0)
                    {
                        List<string> emails = notCustomerWithDemoBlackListed.Select(x => x.Field<string>("EMAIL")).ToList();

                        if(emails.Exists(x => !string.IsNullOrEmpty(x) && IsValidEmail(x)))
                        {
                            var csvNoCustomersWithDemoBlackListed = GenerateExportCsvWithRelations(notCustomerWithDemoBlackListed);
                            await ExportContactCsvToBrevo(csvNoCustomersWithDemoBlackListed, new int[] { 48 });
                        }
                    }

                    List<DataRow> notCustomerWithDemoNotBlackListed = notCustomerWithDemo.Where(x => !x.Field<bool>("BLACKLISTEDFROMNEWSLETTERBYFORZA") && !x.Field<bool>("UNSUBSCRIBEDINBREVO")).ToList();
                    if (notCustomerWithDemoNotBlackListed.Count != 0)
                    {
                        List<string> emails = notCustomerWithDemoNotBlackListed.Select(x => x.Field<string>("EMAIL")).ToList();

                        if(emails.Exists(x => !string.IsNullOrEmpty(x) && IsValidEmail(x)))
                        {
                            var csvNoCustomersWithDemoNotBlackListed = GenerateExportCsvWithRelations(notCustomerWithDemoNotBlackListed);
                            await ExportContactCsvToBrevo(csvNoCustomersWithDemoNotBlackListed, new int[] { 43, 47 });
                        }
                    }

                    progress.Report(new ProgressInfo()
                    {
                        Percent = 0.7,
                        Description = $"Niet klanten met demo geëxporteerd."
                    });
                }
            }

            // Wel klanten exporteren
            if (relationCustomers.Count != 0)
            {
                // Filteren op emails die met de sync meegaan
                emails44 = emails44.Where(x => relationCustomers.Select(y => y.Field<string>("EMAIL")).Contains(x)).ToList();
                emails45 = emails45.Where(x => relationCustomers.Select(y => y.Field<string>("EMAIL")).Contains(x)).ToList();
                emails47 = emails47.Where(x => relationCustomers.Select(y => y.Field<string>("EMAIL")).Contains(x)).ToList();

                // Klanten verwijderen uit oude lijsten
                await RemoveRelationsFromBrevoList(emails44, 44);
                await RemoveRelationsFromBrevoList(emails45, 45);
                await RemoveRelationsFromBrevoList(emails47, 47);
                
                progress.Report(new ProgressInfo()
                {
                    Percent = 0.8,
                    Description = $"Wel klanten verwijderd uit oude lijsten."
                });

                List<DataRow> relationCustomersBlackListed = relationCustomers.Where(x => x.Field<bool>("BLACKLISTEDFROMNEWSLETTERBYFORZA") || x.Field<bool>("UNSUBSCRIBEDINBREVO")).ToList();
                if(relationCustomersBlackListed.Count != 0)
                {
                    List<string> emails = relationCustomersBlackListed.Select(x => x.Field<string>("EMAIL")).ToList();

                    if (emails.Exists(x => !string.IsNullOrEmpty(x) && IsValidEmail(x)))
                    {
                        var csvCustomersBlackListed = GenerateExportCsvWithRelations(relationCustomersBlackListed);
                        await ExportContactCsvToBrevo(csvCustomersBlackListed, new int[] { 48 });
                    }
                }


                List<DataRow> relationCustomersNotBlackListed = relationCustomers.Where(x => !x.Field<bool>("BLACKLISTEDFROMNEWSLETTERBYFORZA") && !x.Field<bool>("UNSUBSCRIBEDINBREVO")).ToList();
                if(relationCustomersNotBlackListed.Count != 0)
                {
                    List<string> emails = relationCustomersNotBlackListed.Select(x => x.Field<string>("EMAIL")).ToList();

                    if (emails.Exists(x => !string.IsNullOrEmpty(x) && IsValidEmail(x)))
                    {
                        var csvCustomersNotBlackListed = GenerateExportCsvWithRelations(relationCustomersNotBlackListed);
                        await ExportContactCsvToBrevo(csvCustomersNotBlackListed, new int[] { 43, 44 });
                    }
                }
                
                progress.Report(new ProgressInfo()
                {
                    Percent = 0.9,
                    Description = $"Wel klanten geëxporteerd."
                });
            }

            progress.Report(new ProgressInfo()
            {
                Percent = 0.95,
                Description = $"Klaar met exporteren, laatste sync datum(s) bijwerken."
            });

            // Updaten laatse sync datum
            UpdateLastBrevoSyncDate(_relationIds, progress);

            progress.Report(new ProgressInfo()
            {
                Percent = 1.0,
                Description = $"Taak voltooid."
            });
        }

        private List<BrevoAccount> GetAllBrevoAccounts(IProgress<ProgressInfo> progress)
        {
            List<BrevoAccount> brevoAccounts = new List<BrevoAccount>();

            int totalContacts = int.MaxValue;
            int batchSize = 1000;

            for (int i = 0; i < totalContacts; i += batchSize)
            {
                HttpResponseMessage response = _client.GetAsync($"https://api.brevo.com/v3/contacts?limit={batchSize}&offset={i}").Result;

                if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    throw new Exception("Fout bij het ophalen van contacten in Brevo: " + response.Content.ReadAsStringAsync().Result);
                }

                var responseObj = JsonConvert.DeserializeObject<ContactsResponse>(response.Content.ReadAsStringAsync().Result);
                brevoAccounts.AddRange(responseObj.Contacts);

                if (totalContacts == int.MaxValue)
                {
                    totalContacts = responseObj.Count;
                } else
                {
                    progress.Report(new ProgressInfo()
                    {
                        Percent = (double)i / totalContacts / 10,
                        Description = $"Ophalen bestaande Brevo accounts. ({i:n0}/{totalContacts:n0})"
                    });
                }
            }

            return brevoAccounts;
        }

        private void SetUnsubscribeStatus(List<int> relationIds, List<BrevoAccount> brevoAccounts, IProgress<ProgressInfo> progress)
        {
            List<string> unsubscribedBrevoEmails = brevoAccounts.Where(x => x.emailBlacklisted || x.smsBlacklisted)
                .Select(x => x.email).ToList();

            var rsRelationToUpdate = _ridderScript.GetRecordset("R_RELATION", "PK_R_RELATION, EMAIL, UNSUBSCRIBEDINBREVO",
                    $"EMAIL IN ('{string.Join("','", unsubscribedBrevoEmails)}') AND UNSUBSCRIBEDINBREVO = 0", "");

            rsRelationToUpdate.UpdateWhenMoveRecord = false;

            int index = 0;
            while (!rsRelationToUpdate.EOF)
            {
                index++;

                rsRelationToUpdate.SetFieldValue("UNSUBSCRIBEDINBREVO", true);
                rsRelationToUpdate.MoveNext();

                progress.Report(new ProgressInfo()
                {
                    Percent = (double)progressBar1.Value / 100,
                    Description = $"Bijwerken uitschrijfstatus in Ridder {index}/{rsRelationToUpdate.RecordCount}"
                });
            }

            rsRelationToUpdate.MoveFirst();
            var result = rsRelationToUpdate.Update2();

            if(result.Any(x => x.HasError))
            {
                MessageBox.Show($"Er is iets fouts gegaan bij het bijwerken van de uitschrijfstatus in Ridder: {result.First(x => x.HasError).GetResult()}");
            }
        }

        private string GenerateExportCsvWithRelations(List<DataRow> relations)
        {
            List<int> relationIds = relations.Select(x => x.Field<int>("PK_R_RELATION")).ToList();
            
            // Prevent empty list
            if(relationIds.Count == 0)
                relationIds.Add(0);

            var orders = _ridderScript.GetRecordset("R_ORDER", "PK_R_ORDER, FK_RELATION, ORDERNUMBER, ORDERDATE, ORDERTYPE", $"FK_RELATION IN ({string.Join(", ", relationIds)})", "").DataTable.AsEnumerable().OrderBy(x => x.Field<DateTime>("ORDERDATE"));
            
            List<int> visitingAddressIds = relations.Select(x => x.Field<int?>("FK_VISITINGADDRESS") ?? 0).ToList();
            
            // Prevent empty list
            if(visitingAddressIds.Count == 0)
                visitingAddressIds.Add(0);
            
            var addressses = _ridderScript.GetRecordset("R_ADDRESS", "PK_R_ADDRESS, ZIPCODE", $"PK_R_ADDRESS IN ({string.Join(", ", visitingAddressIds)})", "").DataTable.AsEnumerable();

            var csv = new StringBuilder();
            csv.AppendLine("EMAIL;FIRSTNAME;ADDED;ORDERNUMMER;POSTCODE;TELEFOON;EMAILBLACKLISTED;SMSBLACKLISTED"); // Header
            foreach (DataRow relation in relations)
            {
                try
                {
                    var relationOrders = orders.Where(x => x.Field<int>("FK_RELATION") == relation.Field<int>("PK_R_RELATION"));
                    var relationAddress = addressses.Where(x => x.Field<int>("PK_R_ADDRESS") == (relation.Field<int?>("FK_VISITINGADDRESS") ?? 0));

                    var email = relation.Field<string>("EMAIL");

                    if (string.IsNullOrEmpty(email) && !IsValidEmail(email))
                    {
                        continue;
                    }

                    var emailBlacklisted = relation.Field<bool>("BLACKLISTEDFROMNEWSLETTERBYFORZA") || relation.Field<bool>("UNSUBSCRIBEDINBREVO");
                    var smsBlacklisted = false;

                    var firstName = relation.Field<string>("NAME");
                    var added = DateTime.Now.ToString("dd-MM-yyyy");
                    var telefoon = relation.Field<string>("PHONE1");

                    int? orderNummer = null;
                    if (relationOrders.Count() != 0)
                    {
                        orderNummer = relationOrders.First().Field<int>("ORDERNUMBER");
                    }

                    string postcode = null;
                    if (relationAddress.Count() != 0)
                    {
                        postcode = relationAddress.First().Field<string>("ZIPCODE");
                    }

                    csv.AppendLine($"{email};{firstName};{added};{orderNummer};{postcode};{telefoon};{emailBlacklisted};{smsBlacklisted}");
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Fout bij het exporteren van relatie: {relation.Field<string>("NAME")} - melding: {ex.Message} \n\n Probeer de synchronisatie opnieuw te starten.");
                    break;
                }
            }

            return csv.ToString();
        }

        private bool IsValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            // Regular expression for validating an email address
            string pattern = @"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$";
            Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);

            return regex.IsMatch(email);
        }

        private async Task<List<string>> GetEmailsFromBrevoList(int listId)
        {
            List<string> emails = new List<string>();

            int totalCount = int.MaxValue;
            int batchSize = 500;
            int index = 0;

            while (index < totalCount)
            {
                HttpResponseMessage resp = await _client.GetAsync($"https://api.brevo.com/v3/contacts/lists/{listId}/contacts?limit={batchSize}&offset={index}");

                if (!resp.IsSuccessStatusCode)
                {
                    throw new Exception("Error while fetching contacts from Brevo lists: " + await resp.Content.ReadAsStringAsync());
                }

                ContactListResponse content = JsonConvert.DeserializeObject<ContactListResponse>(await resp.Content.ReadAsStringAsync());
                emails.AddRange(content.Contacts.Select(x => x.email));

                totalCount = content.Count;
                index += batchSize; // Increment index by batchSize after processing each batch
            }

            return emails;
        }

        private async Task<bool> RemoveRelationsFromBrevoList(List<string> emails, int listId)
        {
            // Deze endpoint kan maar 150 emails per keer aan
            int totalRel = emails.Count;
            int batchSize = 150;
            bool success = true;

            for (int i = 0; i < totalRel; i += batchSize)
            {
                Dictionary<string, string[]> body = new Dictionary<string, string[]>();
                body["emails"] = emails.Skip(i).Take(batchSize).ToArray();

                HttpResponseMessage response = _client.PostAsJsonAsync($"https://api.brevo.com/v3/contacts/lists/{listId}/contacts/remove", body).Result;
            }

            return success;
        }

        private async Task<(List<DataRow>, List<DataRow>)> SplitNotCustomerBasedOnDemo(List<DataRow> relations)
        {
            List<DataRow> withoutDemo = new List<DataRow>();
            List<DataRow> withDemo = new List<DataRow>();

            var actionScopes = _ridderScript.GetRecordset(
                "R_ACTIONSCOPE",
                "FK_RELATION",
                $"FK_RELATION IN ({string.Join(", ", relations.Select(x => x.Field<int>("PK_R_RELATION")))})",
                "").DataTable.AsEnumerable().ToList();
            var actionScopeIds = actionScopes.Select(x => x.Field<int>("PK_R_ACTIONSCOPE")).ToList();

            var appointmentActions = _ridderScript.GetRecordset(
                "R_APPOINTMENTACTION",
                "DESCRIPTION, FK_ACTIONSCOPE",
                $"FK_ACTIONSCOPE IN ({string.Join(", ", actionScopeIds)})",
                "").DataTable.AsEnumerable().ToList();

            foreach (DataRow relation in relations)
            {
                var relationActionScopesIds = actionScopes.Where(x => x.Field<int>("FK_RELATION") == relation.Field<int>("PK_R_RELATION"))
                    .Select(x => x.Field<int>("PK_R_ACTIONSCOPE")).ToList();

                if (relationActionScopesIds.Count() == 0)
                {
                    withoutDemo.Add(relation);
                    continue;
                }

                var relationAppointmentActions = appointmentActions.Where(x => relationActionScopesIds.Contains(x.Field<int>("FK_ACTIONSCOPE")));
                if (relationAppointmentActions.Any(x => x.Field<string>("DESCRIPTION").Contains("THUISDEMO") || x.Field<string>("DESCRIPTION").Contains("AFSH")))
                {
                    withDemo.Add(relation);
                }
                else
                {
                    withoutDemo.Add(relation);
                }
            }

            return (withoutDemo, withDemo);
        }

        private async Task<bool> ExportContactCsvToBrevo(string csv, int[] listIds)
        {
            ExportBody body = new ExportBody()
            {
                fileBody = csv,
                listIds = listIds,
                updateExistingContacts = true,
                emptyContactsAttributes = true
            };

            HttpResponseMessage response = _client.PostAsJsonAsync("https://api.brevo.com/v3/contacts/import", body).Result;

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception("Fout bij het importeren van contacten in Brevo: " + response.Content.ReadAsStringAsync().Result);
            }

            return response.IsSuccessStatusCode;
        }

        private void UpdateLastBrevoSyncDate(List<int> relationIds, IProgress<ProgressInfo> progress)
        {
            var rsRelations = _ridderScript.GetRecordset("R_RELATION", "PK_R_RELATION, LASTSYNCDATEBREVO", $"PK_R_RELATION IN ({string.Join(", ", relationIds)})", "");
            rsRelations.UpdateWhenMoveRecord = false;
            rsRelations.MoveFirst();

            int index = 0;
            // Loop over de relaties en update de lastbrevo sync datum
            while (!rsRelations.EOF)
            {
                index++;

                rsRelations.SetFieldValue("LASTSYNCDATEBREVO", DateTime.Now);
                rsRelations.MoveNext();

                progress.Report(new ProgressInfo()
                {
                    Percent = (double)progressBar1.Value / 100,
                    Description = $"Bijwerken laatste sync datum in Ridder {index}/{rsRelations.RecordCount}"
                });
            }

            rsRelations.MoveFirst();
            var result = rsRelations.Update2();

            if (result.Any(x => x.HasError))
            {
                MessageBox.Show($"Er is iets fouts gegaan bij het bijwerken van de laatste sync datum in Ridder: {result.First(x => x.HasError).GetResult()}");
            }
        }
    }

    public class ProgressInfo
    {
        public double Percent { get; set; }
        public string Description { get; set; }
    }
}

/*
 * FormExportProgress.Designer.cs
 */
namespace ForzaFietsenExport
{
    partial class FormExportProgress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(47, 33);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(433, 23);
            this.progressBar1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "label1";
            // 
            // FormExportProgress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(535, 123);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.progressBar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormExportProgress";
            this.Text = "Voortgang export";
            this.Load += new System.EventHandler(this.FormExportProgress_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label1;
    }
}

