﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using Ridder.Client.SDK.SDKParameters;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;
using ForzaFietsenExport;
using System.Threading;

namespace Forza___Relaties_naar_Brevo
{
    public class RidderScript : CommandScript
    {
        private readonly string _apikey = "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";
        private readonly HttpClient _client = new HttpClient();

        public async void Execute()
        {
            // RK
            // 25-01-2024
            // Toevoegen bijhouden van synchronisatie tijd/datum
            // Verwijderen uit oude lijsten wanneer niet meer in aanmerking komend

            // RK
            // 19-02-2024
            // Optimalisatie en bugfixes

            // RK
            // 15-03-2024
            // Verbeteringen en bugfixes

            // RK
            // 03-04-2024
            // Progressbar toevoegen
            if (System.Diagnostics.Debugger.IsAttached)
                System.Diagnostics.Debugger.Break();

            if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("R_RELATION"))
            {
                MessageBox.Show("Dit script mag alleen vanaf de relaties tabel aangeroepen worden.");
                return;
            }

            List<int> relationIds = FormDataAwareFunctions.GetSelectedRecords().Select(x => (int)x.GetPrimaryKeyValue()).ToList();


            if (relationIds.Count == 0)
            {
                MessageBox.Show("Geen geldige relaties gekozen voor synchronisatie, maak alstublieft een andere keuze");
                return;
            }

            var filter = FormDataAwareFunctions.GetSelectedRecords().Any()
                    ? $"PK_R_RELATION IN ({string.Join(", ", relationIds)}) AND FK_RELATIONTYPE = 3"
                    : "FK_RELATIONTYPE = 3";

            var relations = GetRecordset("R_RELATION", "PK_R_RELATION, NAME, EMAIL, PHONE1, BLACKLISTEDFROMNEWSLETTERBYFORZA, UNSUBSCRIBEDINBREVO, FK_VISITINGADDRESS", filter, "").DataTable.AsEnumerable().ToList();

            DialogResult result = MessageBox.Show($"Aantal gekozen relaties voor synchronisatie: {relations.Count}. \n\nDoorgaan met synchroniseren?", "Bevestigen", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            // Niet doorgaan
            if (result == DialogResult.No)
                return;

            // Progressbar en taak starten starten
            FormExportProgress exportProgress = new FormExportProgress(this, relationIds, relations);
            exportProgress.ShowDialog();

            // Synchronisatie voltooid
            MessageBox.Show($"Synchronisatie is voltooid", "Voortgang export", MessageBoxButtons.OK, MessageBoxIcon.Information);

            MessageBox.Show("test", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }

    public class ContactsResponse
    {
        public List<BrevoAccount> Contacts { get; set; }
        public int Count { get; set; }
    }

    public class BrevoAccount
    {
        public string email { get; set; }
        public int id { get; set; }
        public bool emailBlacklisted { get; set; }
        public bool smsBlacklisted { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime modifiedAt { get; set; }
        public int[] listIds { get; set; }
    }

    public class ExportBody
    {
        public string fileBody { get; set; }
        public int[] listIds { get; set; }
        public bool updateExistingContacts { get; set; }
        public bool emptyContactsAttributes { get; set; }
    }

    public class ContactListResponse
    {
        public List<BrevoAccount> Contacts { get; set; }
        public int Count { get; set; }
    }
}
