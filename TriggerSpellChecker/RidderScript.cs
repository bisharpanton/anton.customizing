﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using Ridder.Recordset.Extensions;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using Newtonsoft.Json.Linq;

public class RidderScript : CommandScript
{
    public async void Execute()
    {
        // RK
        // 01-08-2024
        // Tekst controleren op spelfouten m.b.v. Claude AI

        if (FormDataAwareFunctions == null || !FormDataAwareFunctions.TableName.Equals("U_SPELLCHECKTEST", StringComparison.OrdinalIgnoreCase))
        {
            MessageBox.Show("Dit script mag alleen vanaf een spellcheck test tabel aangeroepen worden.", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        IRecord[] records = FormDataAwareFunctions.GetSelectedRecords();
        if (records.Length > 1)
        {
            MessageBox.Show("Dit script is voor één Bisharp portal user tegelijk aan te roepen.", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        int id = (int)records[0].GetPrimaryKeyValue();
        var rsSpellCheckTest = GetRecordset("U_SPELLCHECKTEST", "PLAINTEXT_ORIGINALTEXT, MODIFIEDTEXT", $"PK_U_SPELLCHECKTEST = {id}", "");

        string originalText = rsSpellCheckTest.DataTable.AsEnumerable().First().Field<string>("PLAINTEXT_ORIGINALTEXT");
        string spellCheckedText = await SpellCheckText(originalText);

        rsSpellCheckTest.MoveFirst();
        rsSpellCheckTest.SetFieldValue("MODIFIEDTEXT", spellCheckedText);

        var result = rsSpellCheckTest.Update2();

        if (result.Any(x => x.HasError))
        {
            MessageBox.Show("Er is een fout opgetreden bij het opslaan van de gecorrigeerde tekst.", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }
    }

    public async Task<string> SpellCheckText(string text)
    {
        HttpClient client = new HttpClient();

        client.BaseAddress = new Uri("https://api.anthropic.com/v1/messages");
        client.DefaultRequestHeaders.Add("anthropic-version", "2023-06-01");
        client.DefaultRequestHeaders.Add("x-api-key", "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");

        var body = new
        {
            model = "claude-3-5-sonnet-20240620",
            stream = false,
            max_tokens = 1024,
            system = "Controleer de volgende tekst op spelfouten en corrigeer deze. Geef alleen de gecorrigeerde tekst terug, zonder verdere uitleg. Als de tekst geen fouten bevat, geef de originele tekst terug.",
            messages = new Message[]
            {
                new Message
                {
                    Role = "user",
                    Content = text
                }
            }
        };

        string json = JsonConvert.SerializeObject(body);
        HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");

        var response = await client.PostAsync("", content);

        if (!response.IsSuccessStatusCode)
        {
            // get error message
            var error = await response.Content.ReadAsStringAsync();
            throw new Exception($"Error: {error}");
        }

        var responseContent = await response.Content.ReadAsStringAsync();

        // Parse the JSON response using Newtonsoft.Json
        JObject parsedResponse = JObject.Parse(responseContent);

        // Extract the content array
        JArray contentArray = (JArray)parsedResponse["content"];

        // Loop through each content object
        foreach (JObject contentObj in contentArray)
        {
            // Check if the type is "text"
            if (contentObj["type"]?.ToString() == "text")
            {
                // Get the text field
                string respText = contentObj["text"]?.ToString();
                return respText;
            }
        }

        return null;
    }
}

public class Message
{
    [JsonProperty("role")]
    public string Role { get; set; }
    [JsonProperty("content")]
    public string Content { get; set; }
}
